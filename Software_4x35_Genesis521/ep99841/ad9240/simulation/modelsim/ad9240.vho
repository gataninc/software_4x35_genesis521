-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 13:07:44"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	AD9240 IS
    PORT (
	IMR : IN std_logic;
	CLK20 : IN std_logic;
	OTR : IN std_logic;
	ADout : IN std_logic_vector(13 DOWNTO 0);
	raw_ad : OUT std_logic_vector(15 DOWNTO 0);
	CLK_FAD : OUT std_logic;
	Dout : OUT std_logic_vector(15 DOWNTO 0);
	Max : OUT std_logic_vector(15 DOWNTO 0);
	Min : OUT std_logic_vector(15 DOWNTO 0)
	);
END AD9240;

ARCHITECTURE structure OF AD9240 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_CLK20 : std_logic;
SIGNAL ww_OTR : std_logic;
SIGNAL ww_ADout : std_logic_vector(13 DOWNTO 0);
SIGNAL ww_raw_ad : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_CLK_FAD : std_logic;
SIGNAL ww_Dout : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Max : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Min : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a_a980 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a_a1144 : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a_a981 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a_a1145 : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a_a982 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a_a1146 : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a_a983 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a_a1147 : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a_a984 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a_a1148 : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a_a985 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a_a1149 : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a_a986 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a_a1150 : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a_a987 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a_a1151 : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a_a988 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a_a1152 : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a_a989 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a_a1153 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a5_a_a329COMB : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a_a990 : std_logic;
SIGNAL U_ADMM_aMIN_SUM_ADD_SUB_aadder_aresult_node_acout_a4_a : std_logic;
SIGNAL U_ADMM_aMIN_SUM_ADD_SUB_aadder_aresult_node_acs_buffer_a4_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a_a1154 : std_logic;
SIGNAL U_ADMM_aMAX_SUM_ADD_SUB_aadder_aresult_node_acout_a4_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a4_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a_a991 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a4_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a_a1155 : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a_a992 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a_a1156 : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a_a993 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a_a1157 : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a_a994 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a_a1158 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a15_a_a1159 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a14_a_a1160 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a13_a_a1161 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a12_a_a1162 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a11_a_a1163 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a10_a_a1164 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a9_a_a1165 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a8_a_a1166 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a7_a_a1167 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a6_a_a1168 : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a5_a_a1169 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a5_a_a411 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a5_a_a333 : std_logic;
SIGNAL ADout_a13_a_acombout : std_logic;
SIGNAL ADout_a0_a_acombout : std_logic;
SIGNAL OTR_acombout : std_logic;
SIGNAL CLK20_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL CLK_FAD_areg0 : std_logic;
SIGNAL dout_del_ff_adffs_a0_a : std_logic;
SIGNAL ADout_a1_a_acombout : std_logic;
SIGNAL dout_del_ff_adffs_a1_a : std_logic;
SIGNAL ADout_a2_a_acombout : std_logic;
SIGNAL dout_del_ff_adffs_a2_a : std_logic;
SIGNAL ADout_a3_a_acombout : std_logic;
SIGNAL dout_del_ff_adffs_a3_a : std_logic;
SIGNAL ADout_a4_a_acombout : std_logic;
SIGNAL dout_del_ff_adffs_a4_a : std_logic;
SIGNAL ADout_a5_a_acombout : std_logic;
SIGNAL dout_del_ff_adffs_a5_a : std_logic;
SIGNAL ADout_a6_a_acombout : std_logic;
SIGNAL dout_del_ff_adffs_a6_a : std_logic;
SIGNAL ADout_a7_a_acombout : std_logic;
SIGNAL dout_del_ff_adffs_a7_a : std_logic;
SIGNAL ADout_a8_a_acombout : std_logic;
SIGNAL dout_del_ff_adffs_a8_a : std_logic;
SIGNAL ADout_a9_a_acombout : std_logic;
SIGNAL dout_del_ff_adffs_a9_a : std_logic;
SIGNAL ADout_a10_a_acombout : std_logic;
SIGNAL dout_del_ff_adffs_a10_a : std_logic;
SIGNAL ADout_a11_a_acombout : std_logic;
SIGNAL dout_del_ff_adffs_a11_a : std_logic;
SIGNAL ADout_a12_a_acombout : std_logic;
SIGNAL dout_del_ff_adffs_a12_a : std_logic;
SIGNAL dout_del_ff_adffs_a13_a : std_logic;
SIGNAL dout_del1_ff_adffs_a0_a : std_logic;
SIGNAL Dout_FF_adffs_a0_a : std_logic;
SIGNAL dout_del1_ff_adffs_a1_a : std_logic;
SIGNAL Dout_FF_adffs_a0_a_a94 : std_logic;
SIGNAL Dout_FF_adffs_a1_a : std_logic;
SIGNAL dout_del1_ff_adffs_a2_a : std_logic;
SIGNAL Dout_FF_adffs_a1_a_a97 : std_logic;
SIGNAL Dout_FF_adffs_a2_a : std_logic;
SIGNAL dout_del1_ff_adffs_a3_a : std_logic;
SIGNAL Dout_FF_adffs_a2_a_a100 : std_logic;
SIGNAL Dout_FF_adffs_a3_a : std_logic;
SIGNAL dout_del1_ff_adffs_a4_a : std_logic;
SIGNAL Dout_FF_adffs_a3_a_a103 : std_logic;
SIGNAL Dout_FF_adffs_a4_a : std_logic;
SIGNAL dout_del1_ff_adffs_a5_a : std_logic;
SIGNAL Dout_FF_adffs_a4_a_a106 : std_logic;
SIGNAL Dout_FF_adffs_a5_a : std_logic;
SIGNAL dout_del1_ff_adffs_a6_a : std_logic;
SIGNAL Dout_FF_adffs_a5_a_a109 : std_logic;
SIGNAL Dout_FF_adffs_a6_a : std_logic;
SIGNAL dout_del1_ff_adffs_a7_a : std_logic;
SIGNAL Dout_FF_adffs_a6_a_a112 : std_logic;
SIGNAL Dout_FF_adffs_a7_a : std_logic;
SIGNAL dout_del1_ff_adffs_a8_a : std_logic;
SIGNAL Dout_FF_adffs_a7_a_a115 : std_logic;
SIGNAL Dout_FF_adffs_a8_a : std_logic;
SIGNAL dout_del1_ff_adffs_a9_a : std_logic;
SIGNAL Dout_FF_adffs_a8_a_a118 : std_logic;
SIGNAL Dout_FF_adffs_a9_a : std_logic;
SIGNAL dout_del1_ff_adffs_a10_a : std_logic;
SIGNAL Dout_FF_adffs_a9_a_a121 : std_logic;
SIGNAL Dout_FF_adffs_a10_a : std_logic;
SIGNAL dout_del1_ff_adffs_a11_a : std_logic;
SIGNAL Dout_FF_adffs_a10_a_a124 : std_logic;
SIGNAL Dout_FF_adffs_a11_a : std_logic;
SIGNAL dout_del1_ff_adffs_a12_a : std_logic;
SIGNAL Dout_FF_adffs_a11_a_a127 : std_logic;
SIGNAL Dout_FF_adffs_a12_a : std_logic;
SIGNAL dout_del1_ff_adffs_a13_a : std_logic;
SIGNAL Dout_FF_adffs_a12_a_a130 : std_logic;
SIGNAL Dout_FF_adffs_a13_a : std_logic;
SIGNAL Dout_FF_adffs_a13_a_a133 : std_logic;
SIGNAL Dout_FF_adffs_a14_a : std_logic;
SIGNAL Dout_FF_adffs_a15_a : std_logic;
SIGNAL a_aGND : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a5_a_a331 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a16_a_a249 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a17_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a17_a_a252 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a18_a_a255 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a19_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a19_a_a258 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a20_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a20_a_a261 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a21_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a21_a_a264 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a22_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a22_a_a267 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a23_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a23_a_a270 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a24_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a24_a_a273 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a25_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a25_a_a276 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a26_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a26_a_a279 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a27_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a27_a_a282 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a28_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a28_a_a285 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a29_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a29_a_a288 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a30_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a30_a_a291 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a31_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a : std_logic;
SIGNAL U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a5_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a5_a_a327 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a6_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a6_a_a324 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a7_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a7_a_a321 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a8_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a8_a_a318 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a9_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a9_a_a315 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a10_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a10_a_a312 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a11_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a11_a_a309 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a12_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a12_a_a306 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a13_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a13_a_a303 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a14_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a14_a_a300 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a15_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a15_a_a297 : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a16_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a0_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a1_a : std_logic;
SIGNAL U_ADMM_aMAX_val_ff_adffs_a18_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a2_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a3_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a4_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a5_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a6_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a7_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a8_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a9_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a10_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a11_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a12_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a13_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a14_a : std_logic;
SIGNAL U_ADMM_aMax_ff_adffs_a15_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a16_a_a331 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a17_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a17_a_a334 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a18_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a18_a_a337 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a19_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a19_a_a340 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a20_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a20_a_a343 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a21_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a21_a_a346 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a22_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a22_a_a349 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a23_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a13_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a : std_logic;
SIGNAL U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a5_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a5_a_a409 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a6_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a6_a_a406 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a7_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a7_a_a403 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a8_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a8_a_a400 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a9_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a9_a_a397 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a10_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a10_a_a394 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a11_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a11_a_a391 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a12_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a12_a_a388 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a13_a_a385 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a14_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a14_a_a382 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a15_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a15_a_a379 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a16_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a0_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a1_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a2_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a3_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a4_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a5_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a6_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a7_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a23_a_a352 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a24_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a8_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a24_a_a355 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a25_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a9_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a25_a_a358 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a26_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a10_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a26_a_a361 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a27_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a11_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a27_a_a364 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a28_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a12_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a28_a_a367 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a29_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a13_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a29_a_a370 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a30_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a14_a : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a30_a_a373 : std_logic;
SIGNAL U_ADMM_aMin_val_ff_adffs_a31_a : std_logic;
SIGNAL U_ADMM_aMin_ff_adffs_a15_a : std_logic;
SIGNAL ALT_INV_CLK_FAD_areg0 : std_logic;
SIGNAL ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out : std_logic;

BEGIN

ww_IMR <= IMR;
ww_CLK20 <= CLK20;
ww_OTR <= OTR;
ww_ADout <= ADout;
raw_ad <= ww_raw_ad;
CLK_FAD <= ww_CLK_FAD;
Dout <= ww_Dout;
Max <= ww_Max;
Min <= ww_Min;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
ALT_INV_CLK_FAD_areg0 <= NOT CLK_FAD_areg0;
ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out <= NOT U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out;
ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out <= NOT U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out;

U_ADMM_aMIN_SUM_ADD_SUB_aadder_aresult_node_acs_buffer_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_SUM_ADD_SUB_aadder_aresult_node_acs_buffer_a4_a = U_ADMM_aMin_val_ff_adffs_a4_a
-- U_ADMM_aMIN_SUM_ADD_SUB_aadder_aresult_node_acout_a4_a = CARRY(U_ADMM_aMin_val_ff_adffs_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCCC",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_SUM_ADD_SUB_aadder_aresult_node_acs_buffer_a4_a,
	cout => U_ADMM_aMIN_SUM_ADD_SUB_aadder_aresult_node_acout_a4_a);

U_ADMM_aMAX_val_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_SUM_ADD_SUB_aadder_aresult_node_acout_a4_a = CARRY()

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0FF0",
	operation_mode => "qfbk_counter",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a4_a,
	cout => U_ADMM_aMAX_SUM_ADD_SUB_aadder_aresult_node_acout_a4_a);

U_ADMM_aMin_val_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a4_a = DFFE(GLOBAL(U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out) & !U_ADMM_aMIN_SUM_ADD_SUB_aadder_aresult_node_acs_buffer_a4_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a4_a = CARRY(U_ADMM_aMin_val_ff_adffs_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "55F0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMIN_SUM_ADD_SUB_aadder_aresult_node_acs_buffer_a4_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a4_a,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a4_a);

U_ADMM_aMin_val_ff_adffs_a5_a_a411_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a5_a_a411 = U_ADMM_aMIN_SUM_ADD_SUB_aadder_aresult_node_acout_a4_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => U_ADMM_aMIN_SUM_ADD_SUB_aadder_aresult_node_acout_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMin_val_ff_adffs_a5_a_a411);

U_ADMM_aMAX_val_ff_adffs_a5_a_a333_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a5_a_a333 = U_ADMM_aMAX_SUM_ADD_SUB_aadder_aresult_node_acout_a4_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => U_ADMM_aMAX_SUM_ADD_SUB_aadder_aresult_node_acout_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_val_ff_adffs_a5_a_a333);

ADout_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ADout(13),
	combout => ADout_a13_a_acombout);

ADout_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ADout(0),
	combout => ADout_a0_a_acombout);

OTR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_OTR,
	combout => OTR_acombout);

CLK20_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK20,
	combout => CLK20_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

CLK_FAD_areg0_I : apex20ke_lcell
-- Equation(s):
-- CLK_FAD_areg0 = DFFE(!CLK_FAD_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => CLK_FAD_areg0,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CLK_FAD_areg0);

dout_del_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a0_a = DFFE(OTR_acombout & ADout_a13_a_acombout # !OTR_acombout & (ADout_a0_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !CLK_FAD_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AACC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ADout_a13_a_acombout,
	datab => ADout_a0_a_acombout,
	datad => OTR_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_CLK_FAD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a0_a);

ADout_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ADout(1),
	combout => ADout_a1_a_acombout);

dout_del_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a1_a = DFFE(OTR_acombout & ADout_a13_a_acombout # !OTR_acombout & (ADout_a1_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !CLK_FAD_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => OTR_acombout,
	datac => ADout_a13_a_acombout,
	datad => ADout_a1_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_CLK_FAD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a1_a);

ADout_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ADout(2),
	combout => ADout_a2_a_acombout);

dout_del_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a2_a = DFFE(OTR_acombout & ADout_a13_a_acombout # !OTR_acombout & (ADout_a2_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !CLK_FAD_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => OTR_acombout,
	datac => ADout_a13_a_acombout,
	datad => ADout_a2_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_CLK_FAD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a2_a);

ADout_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ADout(3),
	combout => ADout_a3_a_acombout);

dout_del_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a3_a = DFFE(OTR_acombout & ADout_a13_a_acombout # !OTR_acombout & (ADout_a3_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !CLK_FAD_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => OTR_acombout,
	datac => ADout_a13_a_acombout,
	datad => ADout_a3_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_CLK_FAD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a3_a);

ADout_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ADout(4),
	combout => ADout_a4_a_acombout);

dout_del_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a4_a = DFFE(OTR_acombout & ADout_a13_a_acombout # !OTR_acombout & (ADout_a4_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !CLK_FAD_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ADout_a13_a_acombout,
	datac => OTR_acombout,
	datad => ADout_a4_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_CLK_FAD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a4_a);

ADout_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ADout(5),
	combout => ADout_a5_a_acombout);

dout_del_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a5_a = DFFE(OTR_acombout & ADout_a13_a_acombout # !OTR_acombout & (ADout_a5_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !CLK_FAD_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AACC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ADout_a13_a_acombout,
	datab => ADout_a5_a_acombout,
	datad => OTR_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_CLK_FAD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a5_a);

ADout_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ADout(6),
	combout => ADout_a6_a_acombout);

dout_del_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a6_a = DFFE(OTR_acombout & ADout_a13_a_acombout # !OTR_acombout & (ADout_a6_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !CLK_FAD_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ADout_a13_a_acombout,
	datac => OTR_acombout,
	datad => ADout_a6_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_CLK_FAD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a6_a);

ADout_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ADout(7),
	combout => ADout_a7_a_acombout);

dout_del_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a7_a = DFFE(OTR_acombout & ADout_a13_a_acombout # !OTR_acombout & (ADout_a7_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !CLK_FAD_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AACC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ADout_a13_a_acombout,
	datab => ADout_a7_a_acombout,
	datad => OTR_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_CLK_FAD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a7_a);

ADout_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ADout(8),
	combout => ADout_a8_a_acombout);

dout_del_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a8_a = DFFE(OTR_acombout & ADout_a13_a_acombout # !OTR_acombout & (ADout_a8_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !CLK_FAD_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => OTR_acombout,
	datac => ADout_a13_a_acombout,
	datad => ADout_a8_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_CLK_FAD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a8_a);

ADout_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ADout(9),
	combout => ADout_a9_a_acombout);

dout_del_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a9_a = DFFE(OTR_acombout & ADout_a13_a_acombout # !OTR_acombout & (ADout_a9_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !CLK_FAD_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ADout_a13_a_acombout,
	datac => OTR_acombout,
	datad => ADout_a9_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_CLK_FAD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a9_a);

ADout_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ADout(10),
	combout => ADout_a10_a_acombout);

dout_del_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a10_a = DFFE(OTR_acombout & ADout_a13_a_acombout # !OTR_acombout & (ADout_a10_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !CLK_FAD_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ADout_a13_a_acombout,
	datac => OTR_acombout,
	datad => ADout_a10_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_CLK_FAD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a10_a);

ADout_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ADout(11),
	combout => ADout_a11_a_acombout);

dout_del_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a11_a = DFFE(OTR_acombout & ADout_a13_a_acombout # !OTR_acombout & (ADout_a11_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !CLK_FAD_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ADout_a13_a_acombout,
	datac => OTR_acombout,
	datad => ADout_a11_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_CLK_FAD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a11_a);

ADout_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ADout(12),
	combout => ADout_a12_a_acombout);

dout_del_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a12_a = DFFE(OTR_acombout & ADout_a13_a_acombout # !OTR_acombout & (ADout_a12_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !CLK_FAD_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => OTR_acombout,
	datac => ADout_a13_a_acombout,
	datad => ADout_a12_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_CLK_FAD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a12_a);

dout_del_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a13_a = DFFE(ADout_a13_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !CLK_FAD_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => ADout_a13_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_CLK_FAD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a13_a);

dout_del1_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del1_ff_adffs_a0_a = DFFE(dout_del_ff_adffs_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => dout_del_ff_adffs_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del1_ff_adffs_a0_a);

Dout_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a0_a = DFFE(dout_del_ff_adffs_a0_a $ dout_del1_ff_adffs_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_FF_adffs_a0_a_a94 = CARRY(dout_del_ff_adffs_a0_a & dout_del1_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a0_a,
	datab => dout_del1_ff_adffs_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a0_a,
	cout => Dout_FF_adffs_a0_a_a94);

dout_del1_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del1_ff_adffs_a1_a = DFFE(dout_del_ff_adffs_a1_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => dout_del_ff_adffs_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del1_ff_adffs_a1_a);

Dout_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a1_a = DFFE(dout_del1_ff_adffs_a1_a $ dout_del_ff_adffs_a1_a $ Dout_FF_adffs_a0_a_a94, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_FF_adffs_a1_a_a97 = CARRY(dout_del1_ff_adffs_a1_a & !dout_del_ff_adffs_a1_a & !Dout_FF_adffs_a0_a_a94 # !dout_del1_ff_adffs_a1_a & (!Dout_FF_adffs_a0_a_a94 # !dout_del_ff_adffs_a1_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del1_ff_adffs_a1_a,
	datab => dout_del_ff_adffs_a1_a,
	cin => Dout_FF_adffs_a0_a_a94,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a1_a,
	cout => Dout_FF_adffs_a1_a_a97);

dout_del1_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del1_ff_adffs_a2_a = DFFE(dout_del_ff_adffs_a2_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => dout_del_ff_adffs_a2_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del1_ff_adffs_a2_a);

Dout_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a2_a = DFFE(dout_del1_ff_adffs_a2_a $ dout_del_ff_adffs_a2_a $ !Dout_FF_adffs_a1_a_a97, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_FF_adffs_a2_a_a100 = CARRY(dout_del1_ff_adffs_a2_a & (dout_del_ff_adffs_a2_a # !Dout_FF_adffs_a1_a_a97) # !dout_del1_ff_adffs_a2_a & dout_del_ff_adffs_a2_a & !Dout_FF_adffs_a1_a_a97)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del1_ff_adffs_a2_a,
	datab => dout_del_ff_adffs_a2_a,
	cin => Dout_FF_adffs_a1_a_a97,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a2_a,
	cout => Dout_FF_adffs_a2_a_a100);

dout_del1_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del1_ff_adffs_a3_a = DFFE(dout_del_ff_adffs_a3_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => dout_del_ff_adffs_a3_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del1_ff_adffs_a3_a);

Dout_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a3_a = DFFE(dout_del_ff_adffs_a3_a $ dout_del1_ff_adffs_a3_a $ Dout_FF_adffs_a2_a_a100, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_FF_adffs_a3_a_a103 = CARRY(dout_del_ff_adffs_a3_a & !dout_del1_ff_adffs_a3_a & !Dout_FF_adffs_a2_a_a100 # !dout_del_ff_adffs_a3_a & (!Dout_FF_adffs_a2_a_a100 # !dout_del1_ff_adffs_a3_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a3_a,
	datab => dout_del1_ff_adffs_a3_a,
	cin => Dout_FF_adffs_a2_a_a100,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a3_a,
	cout => Dout_FF_adffs_a3_a_a103);

dout_del1_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del1_ff_adffs_a4_a = DFFE(dout_del_ff_adffs_a4_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => dout_del_ff_adffs_a4_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del1_ff_adffs_a4_a);

Dout_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a4_a = DFFE(dout_del_ff_adffs_a4_a $ dout_del1_ff_adffs_a4_a $ !Dout_FF_adffs_a3_a_a103, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_FF_adffs_a4_a_a106 = CARRY(dout_del_ff_adffs_a4_a & (dout_del1_ff_adffs_a4_a # !Dout_FF_adffs_a3_a_a103) # !dout_del_ff_adffs_a4_a & dout_del1_ff_adffs_a4_a & !Dout_FF_adffs_a3_a_a103)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a4_a,
	datab => dout_del1_ff_adffs_a4_a,
	cin => Dout_FF_adffs_a3_a_a103,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a4_a,
	cout => Dout_FF_adffs_a4_a_a106);

dout_del1_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del1_ff_adffs_a5_a = DFFE(dout_del_ff_adffs_a5_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => dout_del_ff_adffs_a5_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del1_ff_adffs_a5_a);

Dout_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a5_a = DFFE(dout_del1_ff_adffs_a5_a $ dout_del_ff_adffs_a5_a $ Dout_FF_adffs_a4_a_a106, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_FF_adffs_a5_a_a109 = CARRY(dout_del1_ff_adffs_a5_a & !dout_del_ff_adffs_a5_a & !Dout_FF_adffs_a4_a_a106 # !dout_del1_ff_adffs_a5_a & (!Dout_FF_adffs_a4_a_a106 # !dout_del_ff_adffs_a5_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del1_ff_adffs_a5_a,
	datab => dout_del_ff_adffs_a5_a,
	cin => Dout_FF_adffs_a4_a_a106,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a5_a,
	cout => Dout_FF_adffs_a5_a_a109);

dout_del1_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del1_ff_adffs_a6_a = DFFE(dout_del_ff_adffs_a6_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => dout_del_ff_adffs_a6_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del1_ff_adffs_a6_a);

Dout_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a6_a = DFFE(dout_del_ff_adffs_a6_a $ dout_del1_ff_adffs_a6_a $ !Dout_FF_adffs_a5_a_a109, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_FF_adffs_a6_a_a112 = CARRY(dout_del_ff_adffs_a6_a & (dout_del1_ff_adffs_a6_a # !Dout_FF_adffs_a5_a_a109) # !dout_del_ff_adffs_a6_a & dout_del1_ff_adffs_a6_a & !Dout_FF_adffs_a5_a_a109)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a6_a,
	datab => dout_del1_ff_adffs_a6_a,
	cin => Dout_FF_adffs_a5_a_a109,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a6_a,
	cout => Dout_FF_adffs_a6_a_a112);

dout_del1_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del1_ff_adffs_a7_a = DFFE(dout_del_ff_adffs_a7_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => dout_del_ff_adffs_a7_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del1_ff_adffs_a7_a);

Dout_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a7_a = DFFE(dout_del_ff_adffs_a7_a $ dout_del1_ff_adffs_a7_a $ Dout_FF_adffs_a6_a_a112, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_FF_adffs_a7_a_a115 = CARRY(dout_del_ff_adffs_a7_a & !dout_del1_ff_adffs_a7_a & !Dout_FF_adffs_a6_a_a112 # !dout_del_ff_adffs_a7_a & (!Dout_FF_adffs_a6_a_a112 # !dout_del1_ff_adffs_a7_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a7_a,
	datab => dout_del1_ff_adffs_a7_a,
	cin => Dout_FF_adffs_a6_a_a112,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a7_a,
	cout => Dout_FF_adffs_a7_a_a115);

dout_del1_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del1_ff_adffs_a8_a = DFFE(dout_del_ff_adffs_a8_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => dout_del_ff_adffs_a8_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del1_ff_adffs_a8_a);

Dout_FF_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a8_a = DFFE(dout_del_ff_adffs_a8_a $ dout_del1_ff_adffs_a8_a $ !Dout_FF_adffs_a7_a_a115, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_FF_adffs_a8_a_a118 = CARRY(dout_del_ff_adffs_a8_a & (dout_del1_ff_adffs_a8_a # !Dout_FF_adffs_a7_a_a115) # !dout_del_ff_adffs_a8_a & dout_del1_ff_adffs_a8_a & !Dout_FF_adffs_a7_a_a115)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a8_a,
	datab => dout_del1_ff_adffs_a8_a,
	cin => Dout_FF_adffs_a7_a_a115,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a8_a,
	cout => Dout_FF_adffs_a8_a_a118);

dout_del1_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del1_ff_adffs_a9_a = DFFE(dout_del_ff_adffs_a9_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => dout_del_ff_adffs_a9_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del1_ff_adffs_a9_a);

Dout_FF_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a9_a = DFFE(dout_del_ff_adffs_a9_a $ dout_del1_ff_adffs_a9_a $ Dout_FF_adffs_a8_a_a118, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_FF_adffs_a9_a_a121 = CARRY(dout_del_ff_adffs_a9_a & !dout_del1_ff_adffs_a9_a & !Dout_FF_adffs_a8_a_a118 # !dout_del_ff_adffs_a9_a & (!Dout_FF_adffs_a8_a_a118 # !dout_del1_ff_adffs_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a9_a,
	datab => dout_del1_ff_adffs_a9_a,
	cin => Dout_FF_adffs_a8_a_a118,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a9_a,
	cout => Dout_FF_adffs_a9_a_a121);

dout_del1_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del1_ff_adffs_a10_a = DFFE(dout_del_ff_adffs_a10_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => dout_del_ff_adffs_a10_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del1_ff_adffs_a10_a);

Dout_FF_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a10_a = DFFE(dout_del_ff_adffs_a10_a $ dout_del1_ff_adffs_a10_a $ !Dout_FF_adffs_a9_a_a121, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_FF_adffs_a10_a_a124 = CARRY(dout_del_ff_adffs_a10_a & (dout_del1_ff_adffs_a10_a # !Dout_FF_adffs_a9_a_a121) # !dout_del_ff_adffs_a10_a & dout_del1_ff_adffs_a10_a & !Dout_FF_adffs_a9_a_a121)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a10_a,
	datab => dout_del1_ff_adffs_a10_a,
	cin => Dout_FF_adffs_a9_a_a121,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a10_a,
	cout => Dout_FF_adffs_a10_a_a124);

dout_del1_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del1_ff_adffs_a11_a = DFFE(dout_del_ff_adffs_a11_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => dout_del_ff_adffs_a11_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del1_ff_adffs_a11_a);

Dout_FF_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a11_a = DFFE(dout_del_ff_adffs_a11_a $ dout_del1_ff_adffs_a11_a $ Dout_FF_adffs_a10_a_a124, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_FF_adffs_a11_a_a127 = CARRY(dout_del_ff_adffs_a11_a & !dout_del1_ff_adffs_a11_a & !Dout_FF_adffs_a10_a_a124 # !dout_del_ff_adffs_a11_a & (!Dout_FF_adffs_a10_a_a124 # !dout_del1_ff_adffs_a11_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a11_a,
	datab => dout_del1_ff_adffs_a11_a,
	cin => Dout_FF_adffs_a10_a_a124,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a11_a,
	cout => Dout_FF_adffs_a11_a_a127);

dout_del1_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del1_ff_adffs_a12_a = DFFE(dout_del_ff_adffs_a12_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => dout_del_ff_adffs_a12_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del1_ff_adffs_a12_a);

Dout_FF_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a12_a = DFFE(dout_del_ff_adffs_a12_a $ dout_del1_ff_adffs_a12_a $ !Dout_FF_adffs_a11_a_a127, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_FF_adffs_a12_a_a130 = CARRY(dout_del_ff_adffs_a12_a & (dout_del1_ff_adffs_a12_a # !Dout_FF_adffs_a11_a_a127) # !dout_del_ff_adffs_a12_a & dout_del1_ff_adffs_a12_a & !Dout_FF_adffs_a11_a_a127)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a12_a,
	datab => dout_del1_ff_adffs_a12_a,
	cin => Dout_FF_adffs_a11_a_a127,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a12_a,
	cout => Dout_FF_adffs_a12_a_a130);

dout_del1_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del1_ff_adffs_a13_a = DFFE(dout_del_ff_adffs_a13_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => dout_del_ff_adffs_a13_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del1_ff_adffs_a13_a);

Dout_FF_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a13_a = DFFE(dout_del_ff_adffs_a13_a $ dout_del1_ff_adffs_a13_a $ Dout_FF_adffs_a12_a_a130, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_FF_adffs_a13_a_a133 = CARRY(dout_del_ff_adffs_a13_a & !dout_del1_ff_adffs_a13_a & !Dout_FF_adffs_a12_a_a130 # !dout_del_ff_adffs_a13_a & (!Dout_FF_adffs_a12_a_a130 # !dout_del1_ff_adffs_a13_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a13_a,
	datab => dout_del1_ff_adffs_a13_a,
	cin => Dout_FF_adffs_a12_a_a130,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a13_a,
	cout => Dout_FF_adffs_a13_a_a133);

Dout_FF_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a14_a = DFFE(!Dout_FF_adffs_a13_a_a133, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0F0F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => Dout_FF_adffs_a13_a_a133,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a14_a);

Dout_FF_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_FF_adffs_a15_a = DFFE(VCC, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_FF_adffs_a15_a);

a_aGND_aI : apex20ke_lcell
-- Equation(s):
-- a_aGND = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aGND);

U_ADMM_aMAX_val_ff_adffs_a5_a_a331_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a5_a_a331 = CARRY(U_ADMM_aMAX_val_ff_adffs_a5_a_a333)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00AA",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMAX_val_ff_adffs_a5_a_a333,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_val_ff_adffs_a5_a_a329COMB,
	cout => U_ADMM_aMAX_val_ff_adffs_a5_a_a331);

U_ADMM_aMAX_val_ff_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a16_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a0_a) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a16_a $ U_ADMM_aMAX_val_ff_adffs_a15_a_a297), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a16_a_a249 = CARRY(U_ADMM_aMAX_val_ff_adffs_a16_a # !U_ADMM_aMAX_val_ff_adffs_a15_a_a297)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CCF",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a16_a,
	datac => dout_del1_ff_adffs_a0_a,
	cin => U_ADMM_aMAX_val_ff_adffs_a15_a_a297,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a16_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a16_a_a249);

U_ADMM_aMAX_val_ff_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a17_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a1_a) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a17_a $ !U_ADMM_aMAX_val_ff_adffs_a16_a_a249), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a17_a_a252 = CARRY(!U_ADMM_aMAX_val_ff_adffs_a17_a & !U_ADMM_aMAX_val_ff_adffs_a16_a_a249)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C303",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a17_a,
	datac => dout_del1_ff_adffs_a1_a,
	cin => U_ADMM_aMAX_val_ff_adffs_a16_a_a249,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a17_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a17_a_a252);

U_ADMM_aMAX_val_ff_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a18_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a2_a) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a18_a $ (U_ADMM_aMAX_val_ff_adffs_a17_a_a252)), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a18_a_a255 = CARRY(U_ADMM_aMAX_val_ff_adffs_a18_a # !U_ADMM_aMAX_val_ff_adffs_a17_a_a252)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AAF",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMAX_val_ff_adffs_a18_a,
	datac => dout_del1_ff_adffs_a2_a,
	cin => U_ADMM_aMAX_val_ff_adffs_a17_a_a252,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a18_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a18_a_a255);

U_ADMM_aMAX_val_ff_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a19_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a3_a) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a19_a $ !U_ADMM_aMAX_val_ff_adffs_a18_a_a255), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a19_a_a258 = CARRY(!U_ADMM_aMAX_val_ff_adffs_a19_a & !U_ADMM_aMAX_val_ff_adffs_a18_a_a255)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C303",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a19_a,
	datac => dout_del1_ff_adffs_a3_a,
	cin => U_ADMM_aMAX_val_ff_adffs_a18_a_a255,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a19_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a19_a_a258);

U_ADMM_aMAX_val_ff_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a20_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a4_a) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a20_a $ U_ADMM_aMAX_val_ff_adffs_a19_a_a258), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a20_a_a261 = CARRY(U_ADMM_aMAX_val_ff_adffs_a20_a # !U_ADMM_aMAX_val_ff_adffs_a19_a_a258)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CCF",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a20_a,
	datac => dout_del1_ff_adffs_a4_a,
	cin => U_ADMM_aMAX_val_ff_adffs_a19_a_a258,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a20_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a20_a_a261);

U_ADMM_aMAX_val_ff_adffs_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a21_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a5_a) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a21_a $ !U_ADMM_aMAX_val_ff_adffs_a20_a_a261), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a21_a_a264 = CARRY(!U_ADMM_aMAX_val_ff_adffs_a21_a & !U_ADMM_aMAX_val_ff_adffs_a20_a_a261)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C303",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a21_a,
	datac => dout_del1_ff_adffs_a5_a,
	cin => U_ADMM_aMAX_val_ff_adffs_a20_a_a261,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a21_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a21_a_a264);

U_ADMM_aMAX_val_ff_adffs_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a22_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a6_a) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a22_a $ U_ADMM_aMAX_val_ff_adffs_a21_a_a264), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a22_a_a267 = CARRY(U_ADMM_aMAX_val_ff_adffs_a22_a # !U_ADMM_aMAX_val_ff_adffs_a21_a_a264)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CCF",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a22_a,
	datac => dout_del1_ff_adffs_a6_a,
	cin => U_ADMM_aMAX_val_ff_adffs_a21_a_a264,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a22_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a22_a_a267);

U_ADMM_aMAX_val_ff_adffs_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a23_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a7_a) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a23_a $ !U_ADMM_aMAX_val_ff_adffs_a22_a_a267), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a23_a_a270 = CARRY(!U_ADMM_aMAX_val_ff_adffs_a23_a & !U_ADMM_aMAX_val_ff_adffs_a22_a_a267)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C303",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a23_a,
	datac => dout_del1_ff_adffs_a7_a,
	cin => U_ADMM_aMAX_val_ff_adffs_a22_a_a267,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a23_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a23_a_a270);

U_ADMM_aMAX_val_ff_adffs_a24_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a24_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a8_a) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a24_a $ U_ADMM_aMAX_val_ff_adffs_a23_a_a270), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a24_a_a273 = CARRY(U_ADMM_aMAX_val_ff_adffs_a24_a # !U_ADMM_aMAX_val_ff_adffs_a23_a_a270)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CCF",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a24_a,
	datac => dout_del1_ff_adffs_a8_a,
	cin => U_ADMM_aMAX_val_ff_adffs_a23_a_a270,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a24_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a24_a_a273);

U_ADMM_aMAX_val_ff_adffs_a25_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a25_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a9_a) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a25_a $ !U_ADMM_aMAX_val_ff_adffs_a24_a_a273), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a25_a_a276 = CARRY(!U_ADMM_aMAX_val_ff_adffs_a25_a & !U_ADMM_aMAX_val_ff_adffs_a24_a_a273)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C303",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a25_a,
	datac => dout_del1_ff_adffs_a9_a,
	cin => U_ADMM_aMAX_val_ff_adffs_a24_a_a273,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a25_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a25_a_a276);

U_ADMM_aMAX_val_ff_adffs_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a26_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a10_a) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a26_a $ U_ADMM_aMAX_val_ff_adffs_a25_a_a276), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a26_a_a279 = CARRY(U_ADMM_aMAX_val_ff_adffs_a26_a # !U_ADMM_aMAX_val_ff_adffs_a25_a_a276)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CCF",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a26_a,
	datac => dout_del1_ff_adffs_a10_a,
	cin => U_ADMM_aMAX_val_ff_adffs_a25_a_a276,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a26_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a26_a_a279);

U_ADMM_aMAX_val_ff_adffs_a27_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a27_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a11_a) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a27_a $ !U_ADMM_aMAX_val_ff_adffs_a26_a_a279), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a27_a_a282 = CARRY(!U_ADMM_aMAX_val_ff_adffs_a27_a & !U_ADMM_aMAX_val_ff_adffs_a26_a_a279)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C303",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a27_a,
	datac => dout_del1_ff_adffs_a11_a,
	cin => U_ADMM_aMAX_val_ff_adffs_a26_a_a279,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a27_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a27_a_a282);

U_ADMM_aMAX_val_ff_adffs_a28_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a28_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a12_a) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a28_a $ U_ADMM_aMAX_val_ff_adffs_a27_a_a282), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a28_a_a285 = CARRY(U_ADMM_aMAX_val_ff_adffs_a28_a # !U_ADMM_aMAX_val_ff_adffs_a27_a_a282)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CCF",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a28_a,
	datac => dout_del1_ff_adffs_a12_a,
	cin => U_ADMM_aMAX_val_ff_adffs_a27_a_a282,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a28_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a28_a_a285);

U_ADMM_aMAX_val_ff_adffs_a29_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a29_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a13_a) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a29_a $ !U_ADMM_aMAX_val_ff_adffs_a28_a_a285), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a29_a_a288 = CARRY(!U_ADMM_aMAX_val_ff_adffs_a29_a & !U_ADMM_aMAX_val_ff_adffs_a28_a_a285)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C303",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a29_a,
	datac => dout_del1_ff_adffs_a13_a,
	cin => U_ADMM_aMAX_val_ff_adffs_a28_a_a285,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a29_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a29_a_a288);

U_ADMM_aMAX_val_ff_adffs_a30_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a30_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a30_a $ U_ADMM_aMAX_val_ff_adffs_a29_a_a288), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a30_a_a291 = CARRY(U_ADMM_aMAX_val_ff_adffs_a30_a # !U_ADMM_aMAX_val_ff_adffs_a29_a_a288)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CCF",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a30_a,
	datac => a_aGND,
	cin => U_ADMM_aMAX_val_ff_adffs_a29_a_a288,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a30_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a30_a_a291);

U_ADMM_aMAX_val_ff_adffs_a31_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a31_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a30_a_a291 $ !U_ADMM_aMAX_val_ff_adffs_a31_a), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aGND,
	datad => U_ADMM_aMAX_val_ff_adffs_a31_a,
	cin => U_ADMM_aMAX_val_ff_adffs_a30_a_a291,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a31_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a_a994_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a = CARRY(dout_del1_ff_adffs_a0_a & !U_ADMM_aMAX_val_ff_adffs_a16_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0022",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del1_ff_adffs_a0_a,
	datab => U_ADMM_aMAX_val_ff_adffs_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a_a994,
	cout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a_a993_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a = CARRY(dout_del1_ff_adffs_a1_a & U_ADMM_aMAX_val_ff_adffs_a17_a & !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a # !dout_del1_ff_adffs_a1_a & (U_ADMM_aMAX_val_ff_adffs_a17_a # 
-- !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del1_ff_adffs_a1_a,
	datab => U_ADMM_aMAX_val_ff_adffs_a17_a,
	cin => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a_a993,
	cout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a_a992_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a = CARRY(U_ADMM_aMAX_val_ff_adffs_a18_a & dout_del1_ff_adffs_a2_a & !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a # !U_ADMM_aMAX_val_ff_adffs_a18_a & (dout_del1_ff_adffs_a2_a # 
-- !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMAX_val_ff_adffs_a18_a,
	datab => dout_del1_ff_adffs_a2_a,
	cin => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a_a992,
	cout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a_a991_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a = CARRY(dout_del1_ff_adffs_a3_a & U_ADMM_aMAX_val_ff_adffs_a19_a & !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a # !dout_del1_ff_adffs_a3_a & (U_ADMM_aMAX_val_ff_adffs_a19_a # 
-- !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del1_ff_adffs_a3_a,
	datab => U_ADMM_aMAX_val_ff_adffs_a19_a,
	cin => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a_a991,
	cout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a_a990_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a = CARRY(U_ADMM_aMAX_val_ff_adffs_a20_a & dout_del1_ff_adffs_a4_a & !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a # !U_ADMM_aMAX_val_ff_adffs_a20_a & (dout_del1_ff_adffs_a4_a # 
-- !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMAX_val_ff_adffs_a20_a,
	datab => dout_del1_ff_adffs_a4_a,
	cin => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a_a990,
	cout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a_a989_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a = CARRY(dout_del1_ff_adffs_a5_a & U_ADMM_aMAX_val_ff_adffs_a21_a & !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a # !dout_del1_ff_adffs_a5_a & (U_ADMM_aMAX_val_ff_adffs_a21_a # 
-- !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del1_ff_adffs_a5_a,
	datab => U_ADMM_aMAX_val_ff_adffs_a21_a,
	cin => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a_a989,
	cout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a_a988_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a = CARRY(dout_del1_ff_adffs_a6_a & (!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a # !U_ADMM_aMAX_val_ff_adffs_a22_a) # !dout_del1_ff_adffs_a6_a & !U_ADMM_aMAX_val_ff_adffs_a22_a & 
-- !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del1_ff_adffs_a6_a,
	datab => U_ADMM_aMAX_val_ff_adffs_a22_a,
	cin => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a_a988,
	cout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a_a987_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a = CARRY(dout_del1_ff_adffs_a7_a & U_ADMM_aMAX_val_ff_adffs_a23_a & !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a # !dout_del1_ff_adffs_a7_a & (U_ADMM_aMAX_val_ff_adffs_a23_a # 
-- !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del1_ff_adffs_a7_a,
	datab => U_ADMM_aMAX_val_ff_adffs_a23_a,
	cin => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a_a987,
	cout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a_a986_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a = CARRY(dout_del1_ff_adffs_a8_a & (!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a # !U_ADMM_aMAX_val_ff_adffs_a24_a) # !dout_del1_ff_adffs_a8_a & !U_ADMM_aMAX_val_ff_adffs_a24_a & 
-- !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del1_ff_adffs_a8_a,
	datab => U_ADMM_aMAX_val_ff_adffs_a24_a,
	cin => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a_a986,
	cout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a_a985_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a = CARRY(dout_del1_ff_adffs_a9_a & U_ADMM_aMAX_val_ff_adffs_a25_a & !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a # !dout_del1_ff_adffs_a9_a & (U_ADMM_aMAX_val_ff_adffs_a25_a # 
-- !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del1_ff_adffs_a9_a,
	datab => U_ADMM_aMAX_val_ff_adffs_a25_a,
	cin => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a_a985,
	cout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a_a984_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a = CARRY(dout_del1_ff_adffs_a10_a & (!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a # !U_ADMM_aMAX_val_ff_adffs_a26_a) # !dout_del1_ff_adffs_a10_a & !U_ADMM_aMAX_val_ff_adffs_a26_a & 
-- !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del1_ff_adffs_a10_a,
	datab => U_ADMM_aMAX_val_ff_adffs_a26_a,
	cin => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a_a984,
	cout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a_a983_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a = CARRY(dout_del1_ff_adffs_a11_a & U_ADMM_aMAX_val_ff_adffs_a27_a & !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a # !dout_del1_ff_adffs_a11_a & (U_ADMM_aMAX_val_ff_adffs_a27_a # 
-- !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del1_ff_adffs_a11_a,
	datab => U_ADMM_aMAX_val_ff_adffs_a27_a,
	cin => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a_a983,
	cout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a_a982_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a = CARRY(dout_del1_ff_adffs_a12_a & (!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a # !U_ADMM_aMAX_val_ff_adffs_a28_a) # !dout_del1_ff_adffs_a12_a & !U_ADMM_aMAX_val_ff_adffs_a28_a & 
-- !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del1_ff_adffs_a12_a,
	datab => U_ADMM_aMAX_val_ff_adffs_a28_a,
	cin => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a_a982,
	cout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a_a981_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a = CARRY(U_ADMM_aMAX_val_ff_adffs_a29_a & (!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a # !dout_del1_ff_adffs_a13_a) # !U_ADMM_aMAX_val_ff_adffs_a29_a & !dout_del1_ff_adffs_a13_a & 
-- !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMAX_val_ff_adffs_a29_a,
	datab => dout_del1_ff_adffs_a13_a,
	cin => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a_a981,
	cout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a_a980_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a = CARRY(!U_ADMM_aMAX_val_ff_adffs_a30_a & !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a30_a,
	cin => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a_a980,
	cout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a);

U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out_node_a1 : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out = U_ADMM_aMAX_val_ff_adffs_a31_a # !U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "FF0F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMAX_val_ff_adffs_a31_a,
	cin => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out);

U_ADMM_aMAX_val_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a5_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a5_a $ !U_ADMM_aMAX_val_ff_adffs_a5_a_a331), GLOBAL(CLK20_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a5_a_a327 = CARRY(!U_ADMM_aMAX_val_ff_adffs_a5_a & !U_ADMM_aMAX_val_ff_adffs_a5_a_a331)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C303",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a5_a,
	datac => a_aGND,
	cin => U_ADMM_aMAX_val_ff_adffs_a5_a_a331,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a5_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a5_a_a327);

U_ADMM_aMAX_val_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a6_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a6_a $ U_ADMM_aMAX_val_ff_adffs_a5_a_a327), GLOBAL(CLK20_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a6_a_a324 = CARRY(U_ADMM_aMAX_val_ff_adffs_a6_a # !U_ADMM_aMAX_val_ff_adffs_a5_a_a327)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CCF",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a6_a,
	datac => a_aGND,
	cin => U_ADMM_aMAX_val_ff_adffs_a5_a_a327,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a6_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a6_a_a324);

U_ADMM_aMAX_val_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a7_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a7_a $ !U_ADMM_aMAX_val_ff_adffs_a6_a_a324), GLOBAL(CLK20_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a7_a_a321 = CARRY(!U_ADMM_aMAX_val_ff_adffs_a7_a & !U_ADMM_aMAX_val_ff_adffs_a6_a_a324)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C303",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a7_a,
	datac => a_aGND,
	cin => U_ADMM_aMAX_val_ff_adffs_a6_a_a324,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a7_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a7_a_a321);

U_ADMM_aMAX_val_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a8_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a8_a $ U_ADMM_aMAX_val_ff_adffs_a7_a_a321), GLOBAL(CLK20_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a8_a_a318 = CARRY(U_ADMM_aMAX_val_ff_adffs_a8_a # !U_ADMM_aMAX_val_ff_adffs_a7_a_a321)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CCF",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a8_a,
	datac => a_aGND,
	cin => U_ADMM_aMAX_val_ff_adffs_a7_a_a321,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a8_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a8_a_a318);

U_ADMM_aMAX_val_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a9_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a9_a $ !U_ADMM_aMAX_val_ff_adffs_a8_a_a318), GLOBAL(CLK20_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a9_a_a315 = CARRY(!U_ADMM_aMAX_val_ff_adffs_a9_a & !U_ADMM_aMAX_val_ff_adffs_a8_a_a318)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C303",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a9_a,
	datac => a_aGND,
	cin => U_ADMM_aMAX_val_ff_adffs_a8_a_a318,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a9_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a9_a_a315);

U_ADMM_aMAX_val_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a10_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a10_a $ U_ADMM_aMAX_val_ff_adffs_a9_a_a315), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a10_a_a312 = CARRY(U_ADMM_aMAX_val_ff_adffs_a10_a # !U_ADMM_aMAX_val_ff_adffs_a9_a_a315)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CCF",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a10_a,
	datac => a_aGND,
	cin => U_ADMM_aMAX_val_ff_adffs_a9_a_a315,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a10_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a10_a_a312);

U_ADMM_aMAX_val_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a11_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a11_a $ !U_ADMM_aMAX_val_ff_adffs_a10_a_a312), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a11_a_a309 = CARRY(!U_ADMM_aMAX_val_ff_adffs_a11_a & !U_ADMM_aMAX_val_ff_adffs_a10_a_a312)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C303",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a11_a,
	datac => a_aGND,
	cin => U_ADMM_aMAX_val_ff_adffs_a10_a_a312,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a11_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a11_a_a309);

U_ADMM_aMAX_val_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a12_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a12_a $ U_ADMM_aMAX_val_ff_adffs_a11_a_a309), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a12_a_a306 = CARRY(U_ADMM_aMAX_val_ff_adffs_a12_a # !U_ADMM_aMAX_val_ff_adffs_a11_a_a309)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CCF",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a12_a,
	datac => a_aGND,
	cin => U_ADMM_aMAX_val_ff_adffs_a11_a_a309,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a12_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a12_a_a306);

U_ADMM_aMAX_val_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a13_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a13_a $ !U_ADMM_aMAX_val_ff_adffs_a12_a_a306), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a13_a_a303 = CARRY(!U_ADMM_aMAX_val_ff_adffs_a13_a & !U_ADMM_aMAX_val_ff_adffs_a12_a_a306)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C303",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a13_a,
	datac => a_aGND,
	cin => U_ADMM_aMAX_val_ff_adffs_a12_a_a306,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a13_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a13_a_a303);

U_ADMM_aMAX_val_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a14_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a14_a $ U_ADMM_aMAX_val_ff_adffs_a13_a_a303), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a14_a_a300 = CARRY(U_ADMM_aMAX_val_ff_adffs_a14_a # !U_ADMM_aMAX_val_ff_adffs_a13_a_a303)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CCF",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a14_a,
	datac => a_aGND,
	cin => U_ADMM_aMAX_val_ff_adffs_a13_a_a303,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a14_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a14_a_a300);

U_ADMM_aMAX_val_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMAX_val_ff_adffs_a15_a = DFFE((!U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMAX_val_ff_adffs_a15_a $ !U_ADMM_aMAX_val_ff_adffs_a14_a_a300), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMAX_val_ff_adffs_a15_a_a297 = CARRY(!U_ADMM_aMAX_val_ff_adffs_a15_a & !U_ADMM_aMAX_val_ff_adffs_a14_a_a300)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C303",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMAX_val_ff_adffs_a15_a,
	datac => a_aGND,
	cin => U_ADMM_aMAX_val_ff_adffs_a14_a_a300,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMAX_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMAX_val_ff_adffs_a15_a,
	cout => U_ADMM_aMAX_val_ff_adffs_a15_a_a297);

U_ADMM_aMax_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a0_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a16_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => U_ADMM_aMAX_val_ff_adffs_a16_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a0_a);

U_ADMM_aMax_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a1_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a17_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => U_ADMM_aMAX_val_ff_adffs_a17_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a1_a);

U_ADMM_aMax_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a2_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a18_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMAX_val_ff_adffs_a18_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a2_a);

U_ADMM_aMax_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a3_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a19_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => U_ADMM_aMAX_val_ff_adffs_a19_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a3_a);

U_ADMM_aMax_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a4_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a20_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => U_ADMM_aMAX_val_ff_adffs_a20_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a4_a);

U_ADMM_aMax_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a5_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a21_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMAX_val_ff_adffs_a21_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a5_a);

U_ADMM_aMax_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a6_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a22_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => U_ADMM_aMAX_val_ff_adffs_a22_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a6_a);

U_ADMM_aMax_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a7_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a23_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => U_ADMM_aMAX_val_ff_adffs_a23_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a7_a);

U_ADMM_aMax_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a8_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a24_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMAX_val_ff_adffs_a24_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a8_a);

U_ADMM_aMax_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a9_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a25_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMAX_val_ff_adffs_a25_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a9_a);

U_ADMM_aMax_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a10_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a26_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMAX_val_ff_adffs_a26_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a10_a);

U_ADMM_aMax_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a11_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a27_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMAX_val_ff_adffs_a27_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a11_a);

U_ADMM_aMax_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a12_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a28_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMAX_val_ff_adffs_a28_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a12_a);

U_ADMM_aMax_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a13_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a29_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMAX_val_ff_adffs_a29_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a13_a);

U_ADMM_aMax_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a14_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a30_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMAX_val_ff_adffs_a30_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a14_a);

U_ADMM_aMax_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMax_ff_adffs_a15_a = DFFE(U_ADMM_aMAX_val_ff_adffs_a31_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMAX_val_ff_adffs_a31_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMax_ff_adffs_a15_a);

U_ADMM_aMin_val_ff_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a16_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a0_a) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a16_a $ U_ADMM_aMin_val_ff_adffs_a15_a_a379), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a16_a_a331 = CARRY(!U_ADMM_aMin_val_ff_adffs_a15_a_a379 # !U_ADMM_aMin_val_ff_adffs_a16_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a16_a,
	datac => dout_del1_ff_adffs_a0_a,
	cin => U_ADMM_aMin_val_ff_adffs_a15_a_a379,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a16_a,
	cout => U_ADMM_aMin_val_ff_adffs_a16_a_a331);

U_ADMM_aMin_val_ff_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a17_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a1_a) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a17_a $ !U_ADMM_aMin_val_ff_adffs_a16_a_a331), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a17_a_a334 = CARRY(U_ADMM_aMin_val_ff_adffs_a17_a & !U_ADMM_aMin_val_ff_adffs_a16_a_a331)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a17_a,
	datac => dout_del1_ff_adffs_a1_a,
	cin => U_ADMM_aMin_val_ff_adffs_a16_a_a331,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a17_a,
	cout => U_ADMM_aMin_val_ff_adffs_a17_a_a334);

U_ADMM_aMin_val_ff_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a18_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a2_a) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a18_a $ U_ADMM_aMin_val_ff_adffs_a17_a_a334), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a18_a_a337 = CARRY(!U_ADMM_aMin_val_ff_adffs_a17_a_a334 # !U_ADMM_aMin_val_ff_adffs_a18_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a18_a,
	datac => dout_del1_ff_adffs_a2_a,
	cin => U_ADMM_aMin_val_ff_adffs_a17_a_a334,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a18_a,
	cout => U_ADMM_aMin_val_ff_adffs_a18_a_a337);

U_ADMM_aMin_val_ff_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a19_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a3_a) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a19_a $ !U_ADMM_aMin_val_ff_adffs_a18_a_a337), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a19_a_a340 = CARRY(U_ADMM_aMin_val_ff_adffs_a19_a & !U_ADMM_aMin_val_ff_adffs_a18_a_a337)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a19_a,
	datac => dout_del1_ff_adffs_a3_a,
	cin => U_ADMM_aMin_val_ff_adffs_a18_a_a337,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a19_a,
	cout => U_ADMM_aMin_val_ff_adffs_a19_a_a340);

U_ADMM_aMin_val_ff_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a20_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a4_a) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a20_a $ U_ADMM_aMin_val_ff_adffs_a19_a_a340), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a20_a_a343 = CARRY(!U_ADMM_aMin_val_ff_adffs_a19_a_a340 # !U_ADMM_aMin_val_ff_adffs_a20_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a20_a,
	datac => dout_del1_ff_adffs_a4_a,
	cin => U_ADMM_aMin_val_ff_adffs_a19_a_a340,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a20_a,
	cout => U_ADMM_aMin_val_ff_adffs_a20_a_a343);

U_ADMM_aMin_val_ff_adffs_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a21_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a5_a) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a21_a $ !U_ADMM_aMin_val_ff_adffs_a20_a_a343), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a21_a_a346 = CARRY(U_ADMM_aMin_val_ff_adffs_a21_a & !U_ADMM_aMin_val_ff_adffs_a20_a_a343)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a21_a,
	datac => dout_del1_ff_adffs_a5_a,
	cin => U_ADMM_aMin_val_ff_adffs_a20_a_a343,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a21_a,
	cout => U_ADMM_aMin_val_ff_adffs_a21_a_a346);

U_ADMM_aMin_val_ff_adffs_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a22_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a6_a) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a22_a $ U_ADMM_aMin_val_ff_adffs_a21_a_a346), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a22_a_a349 = CARRY(!U_ADMM_aMin_val_ff_adffs_a21_a_a346 # !U_ADMM_aMin_val_ff_adffs_a22_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a22_a,
	datac => dout_del1_ff_adffs_a6_a,
	cin => U_ADMM_aMin_val_ff_adffs_a21_a_a346,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a22_a,
	cout => U_ADMM_aMin_val_ff_adffs_a22_a_a349);

U_ADMM_aMin_val_ff_adffs_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a23_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a7_a) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a23_a $ (!U_ADMM_aMin_val_ff_adffs_a22_a_a349)), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a23_a_a352 = CARRY(U_ADMM_aMin_val_ff_adffs_a23_a & (!U_ADMM_aMin_val_ff_adffs_a22_a_a349))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a23_a,
	datac => dout_del1_ff_adffs_a7_a,
	cin => U_ADMM_aMin_val_ff_adffs_a22_a_a349,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a23_a,
	cout => U_ADMM_aMin_val_ff_adffs_a23_a_a352);

U_ADMM_aMin_val_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a13_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a13_a $ (!U_ADMM_aMin_val_ff_adffs_a12_a_a388)), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a13_a_a385 = CARRY(U_ADMM_aMin_val_ff_adffs_a13_a & (!U_ADMM_aMin_val_ff_adffs_a12_a_a388))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a13_a,
	datac => a_aGND,
	cin => U_ADMM_aMin_val_ff_adffs_a12_a_a388,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a13_a,
	cout => U_ADMM_aMin_val_ff_adffs_a13_a_a385);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a5_a_a1169_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a5_a = CARRY(!U_ADMM_aMin_val_ff_adffs_a5_a & (!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a4_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a5_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a5_a_a1169,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a5_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a6_a_a1168_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a6_a = CARRY(U_ADMM_aMin_val_ff_adffs_a6_a # !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a6_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a6_a_a1168,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a6_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a7_a_a1167_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a7_a = CARRY(!U_ADMM_aMin_val_ff_adffs_a7_a & (!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a7_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a7_a_a1167,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a7_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a8_a_a1166_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a8_a = CARRY(U_ADMM_aMin_val_ff_adffs_a8_a # !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a8_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a8_a_a1166,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a8_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a9_a_a1165_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a9_a = CARRY(!U_ADMM_aMin_val_ff_adffs_a9_a & (!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a9_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a9_a_a1165,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a9_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a10_a_a1164_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a10_a = CARRY(U_ADMM_aMin_val_ff_adffs_a10_a # !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a10_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a10_a_a1164,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a10_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a11_a_a1163_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a11_a = CARRY(!U_ADMM_aMin_val_ff_adffs_a11_a & (!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a10_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a11_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a11_a_a1163,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a11_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a12_a_a1162_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a12_a = CARRY(U_ADMM_aMin_val_ff_adffs_a12_a # !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a12_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a12_a_a1162,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a12_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a13_a_a1161_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a13_a = CARRY(!U_ADMM_aMin_val_ff_adffs_a13_a & !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a13_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a13_a_a1161,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a13_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a14_a_a1160_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a14_a = CARRY(U_ADMM_aMin_val_ff_adffs_a14_a # !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a14_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a14_a_a1160,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a14_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a15_a_a1159_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a15_a = CARRY(!U_ADMM_aMin_val_ff_adffs_a15_a & (!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a14_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a15_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a15_a_a1159,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a15_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a_a1158_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a = CARRY(U_ADMM_aMin_val_ff_adffs_a16_a & (!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a15_a # !dout_del1_ff_adffs_a0_a) # !U_ADMM_aMin_val_ff_adffs_a16_a & !dout_del1_ff_adffs_a0_a & 
-- !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a16_a,
	datab => dout_del1_ff_adffs_a0_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a_a1158,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a_a1157_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a = CARRY(U_ADMM_aMin_val_ff_adffs_a17_a & dout_del1_ff_adffs_a1_a & !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a # !U_ADMM_aMin_val_ff_adffs_a17_a & (dout_del1_ff_adffs_a1_a # 
-- !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a17_a,
	datab => dout_del1_ff_adffs_a1_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a_a1157,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a_a1156_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a = CARRY(U_ADMM_aMin_val_ff_adffs_a18_a & (!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a # !dout_del1_ff_adffs_a2_a) # !U_ADMM_aMin_val_ff_adffs_a18_a & !dout_del1_ff_adffs_a2_a & 
-- !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a18_a,
	datab => dout_del1_ff_adffs_a2_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a_a1156,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a_a1155_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a = CARRY(U_ADMM_aMin_val_ff_adffs_a19_a & dout_del1_ff_adffs_a3_a & !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a # !U_ADMM_aMin_val_ff_adffs_a19_a & (dout_del1_ff_adffs_a3_a # 
-- !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a19_a,
	datab => dout_del1_ff_adffs_a3_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a_a1155,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a_a1154_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a = CARRY(U_ADMM_aMin_val_ff_adffs_a20_a & (!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a # !dout_del1_ff_adffs_a4_a) # !U_ADMM_aMin_val_ff_adffs_a20_a & !dout_del1_ff_adffs_a4_a & 
-- !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a20_a,
	datab => dout_del1_ff_adffs_a4_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a_a1154,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a_a1153_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a = CARRY(U_ADMM_aMin_val_ff_adffs_a21_a & dout_del1_ff_adffs_a5_a & !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a # !U_ADMM_aMin_val_ff_adffs_a21_a & (dout_del1_ff_adffs_a5_a # 
-- !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a21_a,
	datab => dout_del1_ff_adffs_a5_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a_a1153,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a_a1152_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a = CARRY(U_ADMM_aMin_val_ff_adffs_a22_a & (!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a # !dout_del1_ff_adffs_a6_a) # !U_ADMM_aMin_val_ff_adffs_a22_a & !dout_del1_ff_adffs_a6_a & 
-- !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a22_a,
	datab => dout_del1_ff_adffs_a6_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a21_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a_a1152,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a_a1151_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a = CARRY(dout_del1_ff_adffs_a7_a & (!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a # !U_ADMM_aMin_val_ff_adffs_a23_a) # !dout_del1_ff_adffs_a7_a & !U_ADMM_aMin_val_ff_adffs_a23_a & 
-- !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dout_del1_ff_adffs_a7_a,
	datab => U_ADMM_aMin_val_ff_adffs_a23_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a_a1151,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a_a1150_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a = CARRY(U_ADMM_aMin_val_ff_adffs_a24_a & (!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a # !dout_del1_ff_adffs_a8_a) # !U_ADMM_aMin_val_ff_adffs_a24_a & !dout_del1_ff_adffs_a8_a & 
-- !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a24_a,
	datab => dout_del1_ff_adffs_a8_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a23_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a_a1150,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a_a1149_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a = CARRY(U_ADMM_aMin_val_ff_adffs_a25_a & dout_del1_ff_adffs_a9_a & !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a # !U_ADMM_aMin_val_ff_adffs_a25_a & (dout_del1_ff_adffs_a9_a # 
-- !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a25_a,
	datab => dout_del1_ff_adffs_a9_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a24_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a_a1149,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a_a1148_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a = CARRY(U_ADMM_aMin_val_ff_adffs_a26_a & (!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a # !dout_del1_ff_adffs_a10_a) # !U_ADMM_aMin_val_ff_adffs_a26_a & !dout_del1_ff_adffs_a10_a & 
-- !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a26_a,
	datab => dout_del1_ff_adffs_a10_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a25_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a_a1148,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a_a1147_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a = CARRY(U_ADMM_aMin_val_ff_adffs_a27_a & dout_del1_ff_adffs_a11_a & !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a # !U_ADMM_aMin_val_ff_adffs_a27_a & (dout_del1_ff_adffs_a11_a # 
-- !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a27_a,
	datab => dout_del1_ff_adffs_a11_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a26_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a_a1147,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a_a1146_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a = CARRY(U_ADMM_aMin_val_ff_adffs_a28_a & (!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a # !dout_del1_ff_adffs_a12_a) # !U_ADMM_aMin_val_ff_adffs_a28_a & !dout_del1_ff_adffs_a12_a & 
-- !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a28_a,
	datab => dout_del1_ff_adffs_a12_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a27_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a_a1146,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a_a1145_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a = CARRY(U_ADMM_aMin_val_ff_adffs_a29_a & dout_del1_ff_adffs_a13_a & !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a # !U_ADMM_aMin_val_ff_adffs_a29_a & (dout_del1_ff_adffs_a13_a # 
-- !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a29_a,
	datab => dout_del1_ff_adffs_a13_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a28_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a_a1145,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a_a1144_I : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a = CARRY(U_ADMM_aMin_val_ff_adffs_a30_a # !U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a30_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a29_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a_a1144,
	cout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a);

U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out = !U_ADMM_aMin_val_ff_adffs_a31_a & (!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0505",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a31_a,
	cin => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_alcarry_a30_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out);

U_ADMM_aMin_val_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a5_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a5_a_a411 $ U_ADMM_aMin_val_ff_adffs_a5_a), GLOBAL(CLK20_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a5_a_a409 = CARRY(U_ADMM_aMin_val_ff_adffs_a5_a_a411 & U_ADMM_aMin_val_ff_adffs_a5_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ADMM_aMin_val_ff_adffs_a5_a_a411,
	datab => U_ADMM_aMin_val_ff_adffs_a5_a,
	datac => a_aGND,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a5_a,
	cout => U_ADMM_aMin_val_ff_adffs_a5_a_a409);

U_ADMM_aMin_val_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a6_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a6_a $ U_ADMM_aMin_val_ff_adffs_a5_a_a409), GLOBAL(CLK20_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a6_a_a406 = CARRY(!U_ADMM_aMin_val_ff_adffs_a5_a_a409 # !U_ADMM_aMin_val_ff_adffs_a6_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a6_a,
	datac => a_aGND,
	cin => U_ADMM_aMin_val_ff_adffs_a5_a_a409,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a6_a,
	cout => U_ADMM_aMin_val_ff_adffs_a6_a_a406);

U_ADMM_aMin_val_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a7_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a7_a $ !U_ADMM_aMin_val_ff_adffs_a6_a_a406), GLOBAL(CLK20_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a7_a_a403 = CARRY(U_ADMM_aMin_val_ff_adffs_a7_a & !U_ADMM_aMin_val_ff_adffs_a6_a_a406)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a7_a,
	datac => a_aGND,
	cin => U_ADMM_aMin_val_ff_adffs_a6_a_a406,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a7_a,
	cout => U_ADMM_aMin_val_ff_adffs_a7_a_a403);

U_ADMM_aMin_val_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a8_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a8_a $ U_ADMM_aMin_val_ff_adffs_a7_a_a403), GLOBAL(CLK20_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a8_a_a400 = CARRY(!U_ADMM_aMin_val_ff_adffs_a7_a_a403 # !U_ADMM_aMin_val_ff_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a8_a,
	datac => a_aGND,
	cin => U_ADMM_aMin_val_ff_adffs_a7_a_a403,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a8_a,
	cout => U_ADMM_aMin_val_ff_adffs_a8_a_a400);

U_ADMM_aMin_val_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a9_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a9_a $ !U_ADMM_aMin_val_ff_adffs_a8_a_a400), GLOBAL(CLK20_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a9_a_a397 = CARRY(U_ADMM_aMin_val_ff_adffs_a9_a & !U_ADMM_aMin_val_ff_adffs_a8_a_a400)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a9_a,
	datac => a_aGND,
	cin => U_ADMM_aMin_val_ff_adffs_a8_a_a400,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a9_a,
	cout => U_ADMM_aMin_val_ff_adffs_a9_a_a397);

U_ADMM_aMin_val_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a10_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a10_a $ U_ADMM_aMin_val_ff_adffs_a9_a_a397), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a10_a_a394 = CARRY(!U_ADMM_aMin_val_ff_adffs_a9_a_a397 # !U_ADMM_aMin_val_ff_adffs_a10_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a10_a,
	datac => a_aGND,
	cin => U_ADMM_aMin_val_ff_adffs_a9_a_a397,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a10_a,
	cout => U_ADMM_aMin_val_ff_adffs_a10_a_a394);

U_ADMM_aMin_val_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a11_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a11_a $ !U_ADMM_aMin_val_ff_adffs_a10_a_a394), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a11_a_a391 = CARRY(U_ADMM_aMin_val_ff_adffs_a11_a & !U_ADMM_aMin_val_ff_adffs_a10_a_a394)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a11_a,
	datac => a_aGND,
	cin => U_ADMM_aMin_val_ff_adffs_a10_a_a394,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a11_a,
	cout => U_ADMM_aMin_val_ff_adffs_a11_a_a391);

U_ADMM_aMin_val_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a12_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a12_a $ U_ADMM_aMin_val_ff_adffs_a11_a_a391), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a12_a_a388 = CARRY(!U_ADMM_aMin_val_ff_adffs_a11_a_a391 # !U_ADMM_aMin_val_ff_adffs_a12_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a12_a,
	datac => a_aGND,
	cin => U_ADMM_aMin_val_ff_adffs_a11_a_a391,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a12_a,
	cout => U_ADMM_aMin_val_ff_adffs_a12_a_a388);

U_ADMM_aMin_val_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a14_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a14_a $ U_ADMM_aMin_val_ff_adffs_a13_a_a385), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a14_a_a382 = CARRY(!U_ADMM_aMin_val_ff_adffs_a13_a_a385 # !U_ADMM_aMin_val_ff_adffs_a14_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a14_a,
	datac => a_aGND,
	cin => U_ADMM_aMin_val_ff_adffs_a13_a_a385,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a14_a,
	cout => U_ADMM_aMin_val_ff_adffs_a14_a_a382);

U_ADMM_aMin_val_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a15_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a15_a $ !U_ADMM_aMin_val_ff_adffs_a14_a_a382), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a15_a_a379 = CARRY(U_ADMM_aMin_val_ff_adffs_a15_a & !U_ADMM_aMin_val_ff_adffs_a14_a_a382)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a15_a,
	datac => a_aGND,
	cin => U_ADMM_aMin_val_ff_adffs_a14_a_a382,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a15_a,
	cout => U_ADMM_aMin_val_ff_adffs_a15_a_a379);

U_ADMM_aMin_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a0_a = DFFE(U_ADMM_aMin_val_ff_adffs_a16_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMin_val_ff_adffs_a16_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a0_a);

U_ADMM_aMin_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a1_a = DFFE(U_ADMM_aMin_val_ff_adffs_a17_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMin_val_ff_adffs_a17_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a1_a);

U_ADMM_aMin_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a2_a = DFFE(U_ADMM_aMin_val_ff_adffs_a18_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMin_val_ff_adffs_a18_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a2_a);

U_ADMM_aMin_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a3_a = DFFE(U_ADMM_aMin_val_ff_adffs_a19_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMin_val_ff_adffs_a19_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a3_a);

U_ADMM_aMin_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a4_a = DFFE(U_ADMM_aMin_val_ff_adffs_a20_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMin_val_ff_adffs_a20_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a4_a);

U_ADMM_aMin_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a5_a = DFFE(U_ADMM_aMin_val_ff_adffs_a21_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMin_val_ff_adffs_a21_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a5_a);

U_ADMM_aMin_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a6_a = DFFE(U_ADMM_aMin_val_ff_adffs_a22_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMin_val_ff_adffs_a22_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a6_a);

U_ADMM_aMin_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a7_a = DFFE(U_ADMM_aMin_val_ff_adffs_a23_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMin_val_ff_adffs_a23_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a7_a);

U_ADMM_aMin_val_ff_adffs_a24_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a24_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a8_a) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a24_a $ U_ADMM_aMin_val_ff_adffs_a23_a_a352), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a24_a_a355 = CARRY(!U_ADMM_aMin_val_ff_adffs_a23_a_a352 # !U_ADMM_aMin_val_ff_adffs_a24_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a24_a,
	datac => dout_del1_ff_adffs_a8_a,
	cin => U_ADMM_aMin_val_ff_adffs_a23_a_a352,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a24_a,
	cout => U_ADMM_aMin_val_ff_adffs_a24_a_a355);

U_ADMM_aMin_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a8_a = DFFE(U_ADMM_aMin_val_ff_adffs_a24_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMin_val_ff_adffs_a24_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a8_a);

U_ADMM_aMin_val_ff_adffs_a25_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a25_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a9_a) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a25_a $ !U_ADMM_aMin_val_ff_adffs_a24_a_a355), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a25_a_a358 = CARRY(U_ADMM_aMin_val_ff_adffs_a25_a & !U_ADMM_aMin_val_ff_adffs_a24_a_a355)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a25_a,
	datac => dout_del1_ff_adffs_a9_a,
	cin => U_ADMM_aMin_val_ff_adffs_a24_a_a355,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a25_a,
	cout => U_ADMM_aMin_val_ff_adffs_a25_a_a358);

U_ADMM_aMin_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a9_a = DFFE(U_ADMM_aMin_val_ff_adffs_a25_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => U_ADMM_aMin_val_ff_adffs_a25_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a9_a);

U_ADMM_aMin_val_ff_adffs_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a26_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a10_a) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a26_a $ U_ADMM_aMin_val_ff_adffs_a25_a_a358), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a26_a_a361 = CARRY(!U_ADMM_aMin_val_ff_adffs_a25_a_a358 # !U_ADMM_aMin_val_ff_adffs_a26_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a26_a,
	datac => dout_del1_ff_adffs_a10_a,
	cin => U_ADMM_aMin_val_ff_adffs_a25_a_a358,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a26_a,
	cout => U_ADMM_aMin_val_ff_adffs_a26_a_a361);

U_ADMM_aMin_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a10_a = DFFE(U_ADMM_aMin_val_ff_adffs_a26_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMin_val_ff_adffs_a26_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a10_a);

U_ADMM_aMin_val_ff_adffs_a27_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a27_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a11_a) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a27_a $ !U_ADMM_aMin_val_ff_adffs_a26_a_a361), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a27_a_a364 = CARRY(U_ADMM_aMin_val_ff_adffs_a27_a & !U_ADMM_aMin_val_ff_adffs_a26_a_a361)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a27_a,
	datac => dout_del1_ff_adffs_a11_a,
	cin => U_ADMM_aMin_val_ff_adffs_a26_a_a361,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a27_a,
	cout => U_ADMM_aMin_val_ff_adffs_a27_a_a364);

U_ADMM_aMin_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a11_a = DFFE(U_ADMM_aMin_val_ff_adffs_a27_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMin_val_ff_adffs_a27_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a11_a);

U_ADMM_aMin_val_ff_adffs_a28_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a28_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a12_a) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a28_a $ U_ADMM_aMin_val_ff_adffs_a27_a_a364), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a28_a_a367 = CARRY(!U_ADMM_aMin_val_ff_adffs_a27_a_a364 # !U_ADMM_aMin_val_ff_adffs_a28_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a28_a,
	datac => dout_del1_ff_adffs_a12_a,
	cin => U_ADMM_aMin_val_ff_adffs_a27_a_a364,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a28_a,
	cout => U_ADMM_aMin_val_ff_adffs_a28_a_a367);

U_ADMM_aMin_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a12_a = DFFE(U_ADMM_aMin_val_ff_adffs_a28_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMin_val_ff_adffs_a28_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a12_a);

U_ADMM_aMin_val_ff_adffs_a29_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a29_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & dout_del1_ff_adffs_a13_a) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a29_a $ !U_ADMM_aMin_val_ff_adffs_a28_a_a367), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a29_a_a370 = CARRY(U_ADMM_aMin_val_ff_adffs_a29_a & !U_ADMM_aMin_val_ff_adffs_a28_a_a367)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a29_a,
	datac => dout_del1_ff_adffs_a13_a,
	cin => U_ADMM_aMin_val_ff_adffs_a28_a_a367,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a29_a,
	cout => U_ADMM_aMin_val_ff_adffs_a29_a_a370);

U_ADMM_aMin_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a13_a = DFFE(U_ADMM_aMin_val_ff_adffs_a29_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMin_val_ff_adffs_a29_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a13_a);

U_ADMM_aMin_val_ff_adffs_a30_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a30_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a30_a $ U_ADMM_aMin_val_ff_adffs_a29_a_a370), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_ADMM_aMin_val_ff_adffs_a30_a_a373 = CARRY(!U_ADMM_aMin_val_ff_adffs_a29_a_a370 # !U_ADMM_aMin_val_ff_adffs_a30_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ADMM_aMin_val_ff_adffs_a30_a,
	datac => a_aGND,
	cin => U_ADMM_aMin_val_ff_adffs_a29_a_a370,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a30_a,
	cout => U_ADMM_aMin_val_ff_adffs_a30_a_a373);

U_ADMM_aMin_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a14_a = DFFE(U_ADMM_aMin_val_ff_adffs_a30_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMin_val_ff_adffs_a30_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a14_a);

U_ADMM_aMin_val_ff_adffs_a31_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_val_ff_adffs_a31_a = DFFE((!U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & a_aGND) # (U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out & U_ADMM_aMin_val_ff_adffs_a30_a_a373 $ !U_ADMM_aMin_val_ff_adffs_a31_a), 
-- GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aGND,
	datad => U_ADMM_aMin_val_ff_adffs_a31_a,
	cin => U_ADMM_aMin_val_ff_adffs_a30_a_a373,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sload => ALT_INV_U_ADMM_aMIN_CMP_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_val_ff_adffs_a31_a);

U_ADMM_aMin_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- U_ADMM_aMin_ff_adffs_a15_a = DFFE(U_ADMM_aMin_val_ff_adffs_a31_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_ADMM_aMin_val_ff_adffs_a31_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_ADMM_aMin_ff_adffs_a15_a);

raw_ad_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => dout_del_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(0));

raw_ad_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => dout_del_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(1));

raw_ad_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => dout_del_ff_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(2));

raw_ad_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => dout_del_ff_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(3));

raw_ad_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => dout_del_ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(4));

raw_ad_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => dout_del_ff_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(5));

raw_ad_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => dout_del_ff_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(6));

raw_ad_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => dout_del_ff_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(7));

raw_ad_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => dout_del_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(8));

raw_ad_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => dout_del_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(9));

raw_ad_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => dout_del_ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(10));

raw_ad_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => dout_del_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(11));

raw_ad_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => dout_del_ff_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(12));

raw_ad_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => dout_del_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(13));

raw_ad_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(14));

raw_ad_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_raw_ad(15));

CLK_FAD_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => CLK_FAD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CLK_FAD);

Dout_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(0));

Dout_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(1));

Dout_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(2));

Dout_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(3));

Dout_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(4));

Dout_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(5));

Dout_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(6));

Dout_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(7));

Dout_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(8));

Dout_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(9));

Dout_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(10));

Dout_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(11));

Dout_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(12));

Dout_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(13));

Dout_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(14));

Dout_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Dout_FF_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(15));

Max_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(0));

Max_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(1));

Max_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(2));

Max_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(3));

Max_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(4));

Max_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(5));

Max_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(6));

Max_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(7));

Max_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(8));

Max_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(9));

Max_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(10));

Max_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(11));

Max_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(12));

Max_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(13));

Max_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(14));

Max_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMax_ff_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Max(15));

Min_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(0));

Min_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(1));

Min_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(2));

Min_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(3));

Min_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(4));

Min_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(5));

Min_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(6));

Min_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(7));

Min_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(8));

Min_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(9));

Min_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(10));

Min_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(11));

Min_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(12));

Min_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(13));

Min_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(14));

Min_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_ADMM_aMin_ff_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Min(15));
END structure;


