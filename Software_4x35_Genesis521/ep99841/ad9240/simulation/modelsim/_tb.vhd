-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	FIR_MEM.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module contains the delay lines to be used for the DPP-II. The delay
--		lines are broken down into the individual sections to generate all 8 time
--		constant's FIRs
--
--		It outputs 10 different version of delays from the input data (adata)
--		data1a	1us from adata
--		data1b	1us from data1a
--		data2	2us from data1b
--		data4	4us from data2
--		data8	8us from data4
--		data16	16us from data8
--		data32	32us	from data16
--		data64	64us from data32
--		data128	128us from data128
--
--   History:
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is
	signal IMR		: std_Logic := '1';					-- Master Reset
	signal CLK_FAD_BUF	: std_logic;
	signal CLK20		: std_logic := '0';					-- 5Mhz Clock ( from 20Mhz )
	signal OTR		: std_logic;					-- Out of Range
	signal ADout		: std_logic_vector( 15 downto 0 );	-- A/D Data Output
	signal CLK_FAD		: std_logic;
	signal DAV		: std_logic;
	signal Dout		: std_logic_Vector( 15 downto 0 );	-- outed A/D output
	signal Dout_Reg	: std_logic_Vector( 15 downto 0 );	-- outed A/D output
	signal adata		: std_logic_Vector( 13 downto 0 );
	signal debug_ad	: std_logic_vector( 15 downto 0 );
	signal reset_Gen	: std_Logic;
	signal AD_SEL		: std_logic_vector( 3 downto 0 );
	signal fgain		: std_logic_Vector( 15 downto 0 );
-------------------------------------------------------------------------------
	component AD9240 is 
		port( 
			IMR			: in		std_Logic;					-- Master Reset
			CLK20		: in		std_logic;					-- 5Mhz Clock ( from 20Mhz )
			PA_Reset		: in		std_logic;
			Ramp_Mode		: in		std_logic;
			OTR			: in		std_logic;					-- Out of Range
			ADout		: in		std_logic_vector( 13 downto 0 );	-- A/D Data Output
			fgain		: in		std_logic_Vector( 15 downto 0 );
			raw_ad		: buffer	std_Logic_Vector( 15 downto 0 );
			CLK_FAD		: buffer	std_logic;					-- Clock to A/D Converter
			Reset_Gen		: buffer	std_Logic;
			Dout			: buffer	std_logic_Vector( 15 downto 0 );	-- Buffered A/D output
			Max			: buffer	std_logic_Vector( 15 downto 0 );
			Min			: buffer	std_logic_vector( 15 downto 0 ) );
	end component AD9240;
	------------------------------------------------------------------------------

	-------------------------------------------------------------------------------

begin
	IMR 		<= '0' after 800 ns;
	CLK20	<= not( CLK20 ) after 25 ns;
	AD_SEL	<= x"1";
	fgain	<= x"4000";

	U : AD9240
		port map(
			IMR			=> IMR,
			CLK20		=> CLK20,
			PA_Reset		=> PA_Reset,
			Ramp_Mode		=> Ramp_Mode,
			OTR			=> OTR,
			ADout		=> adata( 13 downto 0 ),
			fgain		=> fgain,
			CLK_FAD		=> CLK_FAD,
			Raw_AD		=> debug_AD,
			Reset_Gen		=> Reset_Gen,
			Dout			=> Dout,
			Max			=> Max,
			Min			=> Min );

	otr <= '1' when ( conv_integer( adata ) > 16384 ) else
		  '1' when ( conv_integer( adata ) < 0     ) else
		  '0';	

	clk20_proc : process( clk20 )
	begin
		if(( clk20'event ) and ( clk20 = '1' )) then
			dout_Reg	<= dout;
		end if;
	end process;
     --------------------------------------------------------------------------
	AD9240_PRoc : process
	begin
		adata	<= "00" & x"000";
		wait until(( CLK_FAD'Event ) and ( CLK_FAD = '1' ));
		wait for 19 ns;
		adata <= "11" & x"FFF";
		wait until(( CLK_FAD'Event ) and ( CLK_FAD = '1' ));
		wait until(( CLK_FAD'Event ) and ( CLK_FAD = '1' ));
		wait for 19 ns;		
		adata <= "10" & x"000";
		wait until(( CLK_FAD'Event ) and ( CLK_FAD = '1' ));
		wait until(( CLK_FAD'Event ) and ( CLK_FAD = '1' ));
		wait for 19 ns;
		adata <= "00" & x"000";

		for j in 0 to 1 loop
			for i in 0 to 16383 loop
				wait until(( CLK_FAD'Event ) and ( CLK_FAD = '1' ));
				wait for 19 ns;
				adata <= conv_Std_logic_vector( i, 14 );
--				wait for 800 ns;
			end loop;
		end loop;

		wait for 1 us;
		assert false
			report "End of Simulations"
			severity failure;
	end process;
     --------------------------------------------------------------------------

	
-------------------------------------------------------------------------------
end test_bench;
-------------------------------------------------------------------------------

