onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk20
add wave -noupdate -format Logic -radix binary /tb/otr
add wave -noupdate -format Logic -radix binary /tb/clk_fad
add wave -noupdate -format Logic -radix binary /tb/reset_gen
add wave -noupdate -format Literal -radix hexadecimal -scale 0.0050000000000000001 /tb/adata
add wave -noupdate -format Literal -radix hexadecimal -scale 0.0050000000000000001 /tb/dout
add wave -noupdate -format Literal -radix hexadecimal -scale 0.0050000000000000001 /tb/dout_reg
add wave -noupdate -format Literal -radix hexadecimal -scale 0.0050000000000000001 /tb/debug_ad
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 4} {437479 ns} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
update
WaveRestoreZoom {0 ns} {42 us}
