-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 13:09:09"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	bit_dac IS
    PORT (
	IMR : IN std_logic;
	clk20 : IN std_logic;
	peak1 : IN std_logic_vector(11 DOWNTO 0);
	peak2 : IN std_logic_vector(11 DOWNTO 0);
	cps : IN std_logic_vector(15 DOWNTO 0);
	pk2dly : IN std_logic_vector(15 DOWNTO 0);
	BIT_DATA : OUT std_logic_vector(15 DOWNTO 0)
	);
END bit_dac;

ARCHITECTURE structure OF bit_dac IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_clk20 : std_logic;
SIGNAL ww_peak1 : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_peak2 : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_cps : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_pk2dly : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_BIT_DATA : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a15_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a15_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a15_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1_asram_asegment_a0_a_a15_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_ROM_asrom_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL U_ROM_asrom_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL DLY1_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_ROM_asrom_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL DLY1_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_ROM_asrom_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL DLY1_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_ROM_asrom_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL DLY1_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_ROM_asrom_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL DLY1_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_ROM_asrom_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL DLY1_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_ROM_asrom_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL U_ROM_asrom_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL DLY1_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_ROM_asrom_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL DLY1_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_ROM_asrom_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL U_ROM_asrom_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL U_ROM_asrom_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL DLY1_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_ROM_asrom_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL U_ROM_asrom_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL U_ROM_asrom_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "100100000100010000";
SIGNAL DLY1_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_ROM_asrom_aq_a8_a : std_logic;
SIGNAL ROM_DATA_FF_adffs_a9_a : std_logic;
SIGNAL ROM_DATA_FF_adffs_a10_a : std_logic;
SIGNAL ROM_DATA_FF_adffs_a11_a : std_logic;
SIGNAL U_ROM_asrom_aq_a12_a : std_logic;
SIGNAL ROM_DATA_FF_adffs_a13_a : std_logic;
SIGNAL ROM_DATA_FF_adffs_a14_a : std_logic;
SIGNAL DLY1_asram_aq_a9_a : std_logic;
SIGNAL DLY1_asram_aq_a10_a : std_logic;
SIGNAL DLY1_asram_aq_a11_a : std_logic;
SIGNAL DLY1_asram_aq_a13_a : std_logic;
SIGNAL DLY1_asram_aq_a14_a : std_logic;
SIGNAL Equal_a447 : std_logic;
SIGNAL Equal_a465 : std_logic;
SIGNAL Equal_a476 : std_logic;
SIGNAL CPS_REG_FF_adffs_a9_a : std_logic;
SIGNAL CPS_REG_FF_adffs_a8_a : std_logic;
SIGNAL CPS_CNTR_a8_a_a129 : std_logic;
SIGNAL CPS_CNTR_a8_a : std_logic;
SIGNAL CPS_CNTR_a9_a_a132 : std_logic;
SIGNAL CPS_CNTR_a9_a : std_logic;
SIGNAL Equal_a489 : std_logic;
SIGNAL Equal_a467 : std_logic;
SIGNAL CPS_REG_FF_adffs_a4_a : std_logic;
SIGNAL CPS_REG_FF_adffs_a1_a : std_logic;
SIGNAL CPS_CNTR_a1_a_a135 : std_logic;
SIGNAL CPS_CNTR_a1_a : std_logic;
SIGNAL CPS_CNTR_a4_a_a138 : std_logic;
SIGNAL CPS_CNTR_a4_a : std_logic;
SIGNAL Equal_a477 : std_logic;
SIGNAL CPS_REG_FF_adffs_a11_a : std_logic;
SIGNAL CPS_REG_FF_adffs_a10_a : std_logic;
SIGNAL CPS_CNTR_a10_a_a141 : std_logic;
SIGNAL CPS_CNTR_a10_a : std_logic;
SIGNAL CPS_CNTR_a11_a_a144 : std_logic;
SIGNAL CPS_CNTR_a11_a : std_logic;
SIGNAL Equal_a490 : std_logic;
SIGNAL Equal_a469 : std_logic;
SIGNAL CPS_REG_FF_adffs_a2_a : std_logic;
SIGNAL CPS_REG_FF_adffs_a0_a : std_logic;
SIGNAL CPS_CNTR_a0_a_a147 : std_logic;
SIGNAL CPS_CNTR_a0_a : std_logic;
SIGNAL CPS_CNTR_a2_a_a150 : std_logic;
SIGNAL CPS_CNTR_a2_a : std_logic;
SIGNAL Equal_a478 : std_logic;
SIGNAL CPS_REG_FF_adffs_a3_a : std_logic;
SIGNAL CPS_REG_FF_adffs_a15_a : std_logic;
SIGNAL CPS_CNTR_a15_a : std_logic;
SIGNAL CPS_CNTR_a3_a_a156 : std_logic;
SIGNAL CPS_CNTR_a3_a : std_logic;
SIGNAL Equal_a491 : std_logic;
SIGNAL Equal_a471 : std_logic;
SIGNAL CPS_REG_FF_adffs_a12_a : std_logic;
SIGNAL CPS_REG_FF_adffs_a5_a : std_logic;
SIGNAL CPS_CNTR_a5_a_a159 : std_logic;
SIGNAL CPS_CNTR_a5_a : std_logic;
SIGNAL CPS_CNTR_a12_a_a162 : std_logic;
SIGNAL CPS_CNTR_a12_a : std_logic;
SIGNAL Equal_a479 : std_logic;
SIGNAL CPS_REG_FF_adffs_a14_a : std_logic;
SIGNAL CPS_REG_FF_adffs_a6_a : std_logic;
SIGNAL CPS_CNTR_a6_a_a165 : std_logic;
SIGNAL CPS_CNTR_a6_a : std_logic;
SIGNAL CPS_CNTR_a14_a_a168 : std_logic;
SIGNAL CPS_CNTR_a14_a : std_logic;
SIGNAL Equal_a492 : std_logic;
SIGNAL Equal_a473 : std_logic;
SIGNAL CPS_REG_FF_adffs_a13_a : std_logic;
SIGNAL CPS_REG_FF_adffs_a7_a : std_logic;
SIGNAL CPS_CNTR_a7_a_a171 : std_logic;
SIGNAL CPS_CNTR_a7_a : std_logic;
SIGNAL CPS_CNTR_a13_a_a174 : std_logic;
SIGNAL CPS_CNTR_a13_a : std_logic;
SIGNAL Equal_a480 : std_logic;
SIGNAL Bit_Disable_a82 : std_logic;
SIGNAL UPD_PROC_a0 : std_logic;
SIGNAL Bit_Disable_a91 : std_logic;
SIGNAL Bit_Disable_a87 : std_logic;
SIGNAL Bit_Disable_a89 : std_logic;
SIGNAL peak2_a6_a_acombout : std_logic;
SIGNAL peak2_a3_a_acombout : std_logic;
SIGNAL peak2_a7_a_acombout : std_logic;
SIGNAL cps_a9_a_acombout : std_logic;
SIGNAL cps_a8_a_acombout : std_logic;
SIGNAL peak1_a4_a_acombout : std_logic;
SIGNAL peak1_a0_a_acombout : std_logic;
SIGNAL peak1_a6_a_acombout : std_logic;
SIGNAL peak1_a2_a_acombout : std_logic;
SIGNAL cps_a4_a_acombout : std_logic;
SIGNAL cps_a1_a_acombout : std_logic;
SIGNAL cps_a11_a_acombout : std_logic;
SIGNAL cps_a10_a_acombout : std_logic;
SIGNAL cps_a2_a_acombout : std_logic;
SIGNAL cps_a0_a_acombout : std_logic;
SIGNAL cps_a3_a_acombout : std_logic;
SIGNAL cps_a15_a_acombout : std_logic;
SIGNAL cps_a12_a_acombout : std_logic;
SIGNAL cps_a5_a_acombout : std_logic;
SIGNAL cps_a14_a_acombout : std_logic;
SIGNAL cps_a6_a_acombout : std_logic;
SIGNAL cps_a13_a_acombout : std_logic;
SIGNAL cps_a7_a_acombout : std_logic;
SIGNAL peak1_a9_a_acombout : std_logic;
SIGNAL peak1_a3_a_acombout : std_logic;
SIGNAL peak1_a8_a_acombout : std_logic;
SIGNAL peak1_a7_a_acombout : std_logic;
SIGNAL peak1_a11_a_acombout : std_logic;
SIGNAL peak1_a10_a_acombout : std_logic;
SIGNAL peak1_a5_a_acombout : std_logic;
SIGNAL peak1_a1_a_acombout : std_logic;
SIGNAL clk20_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL ROM_ADR_a0_a_a65 : std_logic;
SIGNAL ROM_ADR_a1_a : std_logic;
SIGNAL ROM_ADR_a1_a_a68 : std_logic;
SIGNAL ROM_ADR_a2_a : std_logic;
SIGNAL Equal_a448 : std_logic;
SIGNAL ROM_ADR_a2_a_a71 : std_logic;
SIGNAL ROM_ADR_a3_a_a74 : std_logic;
SIGNAL ROM_ADR_a4_a : std_logic;
SIGNAL ROM_ADR_a4_a_a77 : std_logic;
SIGNAL ROM_ADR_a5_a : std_logic;
SIGNAL ROM_ADR_a5_a_a80 : std_logic;
SIGNAL ROM_ADR_a6_a : std_logic;
SIGNAL ROM_ADR_a6_a_a83 : std_logic;
SIGNAL ROM_ADR_a7_a : std_logic;
SIGNAL Equal_a449 : std_logic;
SIGNAL ROM_EN : std_logic;
SIGNAL ROM_ADR_a0_a : std_logic;
SIGNAL ROM_ADR_a3_a : std_logic;
SIGNAL U_ROM_asrom_aq_a0_a : std_logic;
SIGNAL WADR_CNTR_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL WADR_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL WADR_CNTR_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL WADR_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL WADR_CNTR_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL WADR_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL WADR_CNTR_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL WADR_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL WADR_CNTR_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL WADR_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL WADR_CNTR_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL WADR_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL WADR_CNTR_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL WADR_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL WADR_CNTR_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL WADR_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL WADR_CNTR_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL WADR_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL WADR_CNTR_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL pk2dly_a0_a_acombout : std_logic;
SIGNAL PK2_DELAY_REG_FF_adffs_a0_a : std_logic;
SIGNAL add_a593 : std_logic;
SIGNAL pk2dly_a1_a_acombout : std_logic;
SIGNAL PK2_DELAY_REG_FF_adffs_a1_a : std_logic;
SIGNAL add_a595 : std_logic;
SIGNAL add_a597 : std_logic;
SIGNAL pk2dly_a2_a_acombout : std_logic;
SIGNAL PK2_DELAY_REG_FF_adffs_a2_a : std_logic;
SIGNAL add_a599 : std_logic;
SIGNAL add_a601 : std_logic;
SIGNAL pk2dly_a3_a_acombout : std_logic;
SIGNAL PK2_DELAY_REG_FF_adffs_a3_a : std_logic;
SIGNAL add_a603 : std_logic;
SIGNAL add_a605 : std_logic;
SIGNAL pk2dly_a4_a_acombout : std_logic;
SIGNAL PK2_DELAY_REG_FF_adffs_a4_a : std_logic;
SIGNAL add_a607 : std_logic;
SIGNAL add_a609 : std_logic;
SIGNAL pk2dly_a5_a_acombout : std_logic;
SIGNAL PK2_DELAY_REG_FF_adffs_a5_a : std_logic;
SIGNAL add_a611 : std_logic;
SIGNAL add_a613 : std_logic;
SIGNAL pk2dly_a6_a_acombout : std_logic;
SIGNAL PK2_DELAY_REG_FF_adffs_a6_a : std_logic;
SIGNAL add_a615 : std_logic;
SIGNAL add_a617 : std_logic;
SIGNAL pk2dly_a7_a_acombout : std_logic;
SIGNAL PK2_DELAY_REG_FF_adffs_a7_a : std_logic;
SIGNAL add_a619 : std_logic;
SIGNAL add_a621 : std_logic;
SIGNAL pk2dly_a8_a_acombout : std_logic;
SIGNAL PK2_DELAY_REG_FF_adffs_a8_a : std_logic;
SIGNAL add_a623 : std_logic;
SIGNAL add_a625 : std_logic;
SIGNAL pk2dly_a9_a_acombout : std_logic;
SIGNAL PK2_DELAY_REG_FF_adffs_a9_a : std_logic;
SIGNAL add_a627 : std_logic;
SIGNAL add_a629 : std_logic;
SIGNAL DLY1_asram_aq_a0_a : std_logic;
SIGNAL peak2_a0_a_acombout : std_logic;
SIGNAL peak2_a5_a_acombout : std_logic;
SIGNAL peak2_a2_a_acombout : std_logic;
SIGNAL peak2_a4_a_acombout : std_logic;
SIGNAL peak2_a8_a_acombout : std_logic;
SIGNAL peak2_a10_a_acombout : std_logic;
SIGNAL peak2_a1_a_acombout : std_logic;
SIGNAL peak2_a11_a_acombout : std_logic;
SIGNAL peak2_a9_a_acombout : std_logic;
SIGNAL Equal_a487 : std_logic;
SIGNAL Equal_a488 : std_logic;
SIGNAL Equal_a475 : std_logic;
SIGNAL ROM_DATA_FF_adffs_a0_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a0_a : std_logic;
SIGNAL U_ROM_asrom_aq_a1_a : std_logic;
SIGNAL DLY1_asram_aq_a1_a : std_logic;
SIGNAL ROM_DATA_FF_adffs_a1_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a0_a_a55 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a1_a : std_logic;
SIGNAL U_ROM_asrom_aq_a2_a : std_logic;
SIGNAL DLY1_asram_aq_a2_a : std_logic;
SIGNAL ROM_DATA_FF_adffs_a2_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a1_a_a58 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a2_a : std_logic;
SIGNAL U_ROM_asrom_aq_a3_a : std_logic;
SIGNAL DLY1_asram_aq_a3_a : std_logic;
SIGNAL ROM_DATA_FF_adffs_a3_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a2_a_a61 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a3_a : std_logic;
SIGNAL U_ROM_asrom_aq_a4_a : std_logic;
SIGNAL DLY1_asram_aq_a4_a : std_logic;
SIGNAL ROM_DATA_FF_adffs_a4_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a3_a_a64 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a4_a : std_logic;
SIGNAL DLY1_asram_aq_a5_a : std_logic;
SIGNAL ROM_DATA_FF_adffs_a5_a : std_logic;
SIGNAL U_ROM_asrom_aq_a5_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a4_a_a67 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a5_a : std_logic;
SIGNAL U_ROM_asrom_aq_a6_a : std_logic;
SIGNAL DLY1_asram_aq_a6_a : std_logic;
SIGNAL ROM_DATA_FF_adffs_a6_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a5_a_a70 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a6_a : std_logic;
SIGNAL DLY1_asram_aq_a7_a : std_logic;
SIGNAL ROM_DATA_FF_adffs_a7_a : std_logic;
SIGNAL U_ROM_asrom_aq_a7_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a6_a_a73 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a7_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a7_a_a76 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a8_a : std_logic;
SIGNAL DLY1_asram_aq_a8_a : std_logic;
SIGNAL ROM_DATA_FF_adffs_a8_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a0_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a166 : std_logic;
SIGNAL U_ROM_asrom_aq_a9_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a0_a_a49 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a1_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a168 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a170 : std_logic;
SIGNAL U_ROM_asrom_aq_a10_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a1_a_a52 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a2_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a172 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a174 : std_logic;
SIGNAL U_ROM_asrom_aq_a11_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a2_a_a55 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a3_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a176 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a178 : std_logic;
SIGNAL DLY1_asram_aq_a12_a : std_logic;
SIGNAL ROM_DATA_FF_adffs_a12_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a3_a_a58 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a4_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a180 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a182 : std_logic;
SIGNAL U_ROM_asrom_aq_a13_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a4_a_a61 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a5_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a184 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a186 : std_logic;
SIGNAL U_ROM_asrom_aq_a14_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a5_a_a64 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a6_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a188 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a190 : std_logic;
SIGNAL U_ROM_asrom_aq_a15_a : std_logic;
SIGNAL DLY1_asram_aq_a15_a : std_logic;
SIGNAL ROM_DATA_FF_adffs_a15_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a6_a_a67 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a7_a : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a192 : std_logic;
SIGNAL BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a194 : std_logic;
SIGNAL ALT_INV_ROM_EN : std_logic;

BEGIN

ww_IMR <= IMR;
ww_clk20 <= clk20;
ww_peak1 <= peak1;
ww_peak2 <= peak2;
ww_cps <= cps;
ww_pk2dly <= pk2dly;
BIT_DATA <= ww_BIT_DATA;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

U_ROM_asrom_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

U_ROM_asrom_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

U_ROM_asrom_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

U_ROM_asrom_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

U_ROM_asrom_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

U_ROM_asrom_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

U_ROM_asrom_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

U_ROM_asrom_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

DLY1_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);

DLY1_asram_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);

DLY1_asram_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);

DLY1_asram_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);

DLY1_asram_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);

DLY1_asram_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);

DLY1_asram_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);

DLY1_asram_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);

U_ROM_asrom_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

U_ROM_asrom_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

U_ROM_asrom_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

U_ROM_asrom_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

U_ROM_asrom_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

U_ROM_asrom_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

U_ROM_asrom_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

U_ROM_asrom_asegment_a0_a_a15_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd);

U_ROM_asrom_asegment_a0_a_a15_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROM_ADR_a7_a & ROM_ADR_a6_a & ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a3_a & ROM_ADR_a2_a & ROM_ADR_a1_a & ROM_ADR_a0_a);

DLY1_asram_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);

DLY1_asram_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);

DLY1_asram_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);

DLY1_asram_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);

DLY1_asram_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);

DLY1_asram_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);

DLY1_asram_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);

DLY1_asram_asegment_a0_a_a15_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & WADR_CNTR_awysi_counter_asload_path_a9_a & WADR_CNTR_awysi_counter_asload_path_a8_a & WADR_CNTR_awysi_counter_asload_path_a7_a & WADR_CNTR_awysi_counter_asload_path_a6_a & 
WADR_CNTR_awysi_counter_asload_path_a5_a & WADR_CNTR_awysi_counter_asload_path_a4_a & WADR_CNTR_awysi_counter_asload_path_a3_a & WADR_CNTR_awysi_counter_asload_path_a2_a & WADR_CNTR_awysi_counter_asload_path_a1_a & WADR_CNTR_awysi_counter_asload_path_a0_a
);

DLY1_asram_asegment_a0_a_a15_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & add_a629 & add_a625 & add_a621 & add_a617 & add_a613 & add_a609 & add_a605 & add_a601 & add_a597 & add_a593);
ALT_INV_ROM_EN <= NOT ROM_EN;

U_ROM_asrom_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111110000000000000000011111111000001111000011100011100001111111110010101010111000000000000000000000000000000000000000000000",
	address_width => 8,
	bit_number => 8,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a8_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a8_a_modesel,
	dataout => U_ROM_asrom_aq_a8_a);

ROM_DATA_FF_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a9_a = DFFE(DLY1_asram_aq_a9_a & !Equal_a475, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DLY1_asram_aq_a9_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a9_a);

ROM_DATA_FF_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a10_a = DFFE(DLY1_asram_aq_a10_a & !Equal_a475, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DLY1_asram_aq_a10_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a10_a);

ROM_DATA_FF_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a11_a = DFFE(DLY1_asram_aq_a11_a & !Equal_a475, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DLY1_asram_aq_a11_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a11_a);

U_ROM_asrom_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
	address_width => 8,
	bit_number => 12,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a12_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a12_a_modesel,
	dataout => U_ROM_asrom_aq_a12_a);

ROM_DATA_FF_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a13_a = DFFE(DLY1_asram_aq_a13_a & !Equal_a475, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DLY1_asram_aq_a13_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a13_a);

ROM_DATA_FF_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a14_a = DFFE(DLY1_asram_aq_a14_a & !Equal_a475, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DLY1_asram_aq_a14_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a14_a);

DLY1_asram_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 9,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a9_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a9_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a9_a_modesel,
	dataout => DLY1_asram_aq_a9_a);

DLY1_asram_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 10,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a10_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a10_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a10_a_modesel,
	dataout => DLY1_asram_aq_a10_a);

DLY1_asram_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 11,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a11_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a11_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a11_a_modesel,
	dataout => DLY1_asram_aq_a11_a);

DLY1_asram_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 13,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a13_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a13_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a13_a_modesel,
	dataout => DLY1_asram_aq_a13_a);

DLY1_asram_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 14,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a14_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a14_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a14_a_modesel,
	dataout => DLY1_asram_aq_a14_a);

peak2_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(6),
	combout => peak2_a6_a_acombout);

Equal_a447_I : apex20ke_lcell
-- Equation(s):
-- Equal_a447 = Equal_a477 & Equal_a480 & Equal_a479 & Equal_a478

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a477,
	datab => Equal_a480,
	datac => Equal_a479,
	datad => Equal_a478,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a447);

peak2_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(3),
	combout => peak2_a3_a_acombout);

peak2_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(7),
	combout => peak2_a7_a_acombout);

CPS_REG_FF_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a9_a = DFFE(cps_a9_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a9_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a9_a);

CPS_REG_FF_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a8_a = DFFE(cps_a8_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a8_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a8_a);

CPS_CNTR_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a8_a = DFFE(!GLOBAL(UPD_PROC_a0) & CPS_CNTR_a8_a $ (!CPS_CNTR_a7_a_a171), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- CPS_CNTR_a8_a_a129 = CARRY(CPS_CNTR_a8_a & (!CPS_CNTR_a7_a_a171))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a8_a,
	cin => CPS_CNTR_a7_a_a171,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a8_a,
	cout => CPS_CNTR_a8_a_a129);

CPS_CNTR_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a9_a = DFFE(!GLOBAL(UPD_PROC_a0) & CPS_CNTR_a9_a $ (CPS_CNTR_a8_a_a129), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- CPS_CNTR_a9_a_a132 = CARRY(!CPS_CNTR_a8_a_a129 # !CPS_CNTR_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a9_a,
	cin => CPS_CNTR_a8_a_a129,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a9_a,
	cout => CPS_CNTR_a9_a_a132);

Equal_a467_I : apex20ke_lcell
-- Equation(s):
-- Equal_a489 = CPS_CNTR_a8_a & CPS_REG_FF_adffs_a8_a & (CPS_REG_FF_adffs_a9_a $ !CPS_CNTR_a9_a) # !CPS_CNTR_a8_a & !CPS_REG_FF_adffs_a8_a & (CPS_REG_FF_adffs_a9_a $ !CPS_CNTR_a9_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a8_a,
	datab => CPS_REG_FF_adffs_a9_a,
	datac => CPS_CNTR_a9_a,
	datad => CPS_REG_FF_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a467,
	cascout => Equal_a489);

CPS_REG_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a4_a = DFFE(cps_a4_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a4_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a4_a);

CPS_REG_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a1_a = DFFE(cps_a1_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a1_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a1_a);

CPS_CNTR_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a1_a = DFFE(!GLOBAL(UPD_PROC_a0) & CPS_CNTR_a1_a $ (CPS_CNTR_a0_a_a147), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- CPS_CNTR_a1_a_a135 = CARRY(!CPS_CNTR_a0_a_a147 # !CPS_CNTR_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a1_a,
	cin => CPS_CNTR_a0_a_a147,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a1_a,
	cout => CPS_CNTR_a1_a_a135);

CPS_CNTR_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a4_a = DFFE(!GLOBAL(UPD_PROC_a0) & CPS_CNTR_a4_a $ (!CPS_CNTR_a3_a_a156), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- CPS_CNTR_a4_a_a138 = CARRY(CPS_CNTR_a4_a & (!CPS_CNTR_a3_a_a156))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a4_a,
	cin => CPS_CNTR_a3_a_a156,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a4_a,
	cout => CPS_CNTR_a4_a_a138);

Equal_a477_I : apex20ke_lcell
-- Equation(s):
-- Equal_a477 = (CPS_CNTR_a1_a & CPS_REG_FF_adffs_a1_a & (CPS_REG_FF_adffs_a4_a $ !CPS_CNTR_a4_a) # !CPS_CNTR_a1_a & !CPS_REG_FF_adffs_a1_a & (CPS_REG_FF_adffs_a4_a $ !CPS_CNTR_a4_a)) & CASCADE(Equal_a489)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a1_a,
	datab => CPS_REG_FF_adffs_a1_a,
	datac => CPS_REG_FF_adffs_a4_a,
	datad => CPS_CNTR_a4_a,
	cascin => Equal_a489,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a477);

CPS_REG_FF_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a11_a = DFFE(cps_a11_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a11_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a11_a);

CPS_REG_FF_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a10_a = DFFE(cps_a10_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a10_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a10_a);

CPS_CNTR_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a10_a = DFFE(!GLOBAL(UPD_PROC_a0) & CPS_CNTR_a10_a $ !CPS_CNTR_a9_a_a132, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- CPS_CNTR_a10_a_a141 = CARRY(CPS_CNTR_a10_a & !CPS_CNTR_a9_a_a132)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => CPS_CNTR_a10_a,
	cin => CPS_CNTR_a9_a_a132,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a10_a,
	cout => CPS_CNTR_a10_a_a141);

CPS_CNTR_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a11_a = DFFE(!GLOBAL(UPD_PROC_a0) & CPS_CNTR_a11_a $ (CPS_CNTR_a10_a_a141), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- CPS_CNTR_a11_a_a144 = CARRY(!CPS_CNTR_a10_a_a141 # !CPS_CNTR_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a11_a,
	cin => CPS_CNTR_a10_a_a141,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a11_a,
	cout => CPS_CNTR_a11_a_a144);

Equal_a469_I : apex20ke_lcell
-- Equation(s):
-- Equal_a490 = CPS_CNTR_a11_a & CPS_REG_FF_adffs_a11_a & (CPS_CNTR_a10_a $ !CPS_REG_FF_adffs_a10_a) # !CPS_CNTR_a11_a & !CPS_REG_FF_adffs_a11_a & (CPS_CNTR_a10_a $ !CPS_REG_FF_adffs_a10_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a11_a,
	datab => CPS_CNTR_a10_a,
	datac => CPS_REG_FF_adffs_a11_a,
	datad => CPS_REG_FF_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a469,
	cascout => Equal_a490);

CPS_REG_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a2_a = DFFE(cps_a2_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a2_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a2_a);

CPS_REG_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a0_a = DFFE(cps_a0_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a0_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a0_a);

CPS_CNTR_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a0_a = DFFE(!GLOBAL(UPD_PROC_a0) & !CPS_CNTR_a0_a, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- CPS_CNTR_a0_a_a147 = CARRY(CPS_CNTR_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33CC",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => CPS_CNTR_a0_a,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a0_a,
	cout => CPS_CNTR_a0_a_a147);

CPS_CNTR_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a2_a = DFFE(!GLOBAL(UPD_PROC_a0) & CPS_CNTR_a2_a $ (!CPS_CNTR_a1_a_a135), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- CPS_CNTR_a2_a_a150 = CARRY(CPS_CNTR_a2_a & (!CPS_CNTR_a1_a_a135))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a2_a,
	cin => CPS_CNTR_a1_a_a135,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a2_a,
	cout => CPS_CNTR_a2_a_a150);

Equal_a478_I : apex20ke_lcell
-- Equation(s):
-- Equal_a478 = (CPS_REG_FF_adffs_a0_a & CPS_CNTR_a0_a & (CPS_CNTR_a2_a $ !CPS_REG_FF_adffs_a2_a) # !CPS_REG_FF_adffs_a0_a & !CPS_CNTR_a0_a & (CPS_CNTR_a2_a $ !CPS_REG_FF_adffs_a2_a)) & CASCADE(Equal_a490)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_REG_FF_adffs_a0_a,
	datab => CPS_CNTR_a2_a,
	datac => CPS_CNTR_a0_a,
	datad => CPS_REG_FF_adffs_a2_a,
	cascin => Equal_a490,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a478);

CPS_REG_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a3_a = DFFE(cps_a3_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a3_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a3_a);

CPS_REG_FF_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a15_a = DFFE(cps_a15_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a15_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a15_a);

CPS_CNTR_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a15_a = DFFE(!GLOBAL(UPD_PROC_a0) & CPS_CNTR_a14_a_a168 $ CPS_CNTR_a15_a, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => CPS_CNTR_a15_a,
	cin => CPS_CNTR_a14_a_a168,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a15_a);

CPS_CNTR_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a3_a = DFFE(!GLOBAL(UPD_PROC_a0) & CPS_CNTR_a3_a $ (CPS_CNTR_a2_a_a150), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- CPS_CNTR_a3_a_a156 = CARRY(!CPS_CNTR_a2_a_a150 # !CPS_CNTR_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a3_a,
	cin => CPS_CNTR_a2_a_a150,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a3_a,
	cout => CPS_CNTR_a3_a_a156);

Equal_a471_I : apex20ke_lcell
-- Equation(s):
-- Equal_a491 = CPS_CNTR_a15_a & CPS_REG_FF_adffs_a15_a & (CPS_REG_FF_adffs_a3_a $ !CPS_CNTR_a3_a) # !CPS_CNTR_a15_a & !CPS_REG_FF_adffs_a15_a & (CPS_REG_FF_adffs_a3_a $ !CPS_CNTR_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a15_a,
	datab => CPS_REG_FF_adffs_a3_a,
	datac => CPS_CNTR_a3_a,
	datad => CPS_REG_FF_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a471,
	cascout => Equal_a491);

CPS_REG_FF_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a12_a = DFFE(cps_a12_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a12_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a12_a);

CPS_REG_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a5_a = DFFE(cps_a5_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a5_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a5_a);

CPS_CNTR_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a5_a = DFFE(!GLOBAL(UPD_PROC_a0) & CPS_CNTR_a5_a $ (CPS_CNTR_a4_a_a138), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- CPS_CNTR_a5_a_a159 = CARRY(!CPS_CNTR_a4_a_a138 # !CPS_CNTR_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a5_a,
	cin => CPS_CNTR_a4_a_a138,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a5_a,
	cout => CPS_CNTR_a5_a_a159);

CPS_CNTR_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a12_a = DFFE(!GLOBAL(UPD_PROC_a0) & CPS_CNTR_a12_a $ !CPS_CNTR_a11_a_a144, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- CPS_CNTR_a12_a_a162 = CARRY(CPS_CNTR_a12_a & !CPS_CNTR_a11_a_a144)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => CPS_CNTR_a12_a,
	cin => CPS_CNTR_a11_a_a144,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a12_a,
	cout => CPS_CNTR_a12_a_a162);

Equal_a479_I : apex20ke_lcell
-- Equation(s):
-- Equal_a479 = (CPS_CNTR_a12_a & CPS_REG_FF_adffs_a12_a & (CPS_REG_FF_adffs_a5_a $ !CPS_CNTR_a5_a) # !CPS_CNTR_a12_a & !CPS_REG_FF_adffs_a12_a & (CPS_REG_FF_adffs_a5_a $ !CPS_CNTR_a5_a)) & CASCADE(Equal_a491)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a12_a,
	datab => CPS_REG_FF_adffs_a5_a,
	datac => CPS_CNTR_a5_a,
	datad => CPS_REG_FF_adffs_a12_a,
	cascin => Equal_a491,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a479);

CPS_REG_FF_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a14_a = DFFE(cps_a14_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a14_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a14_a);

CPS_REG_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a6_a = DFFE(cps_a6_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a6_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a6_a);

CPS_CNTR_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a6_a = DFFE(!GLOBAL(UPD_PROC_a0) & CPS_CNTR_a6_a $ !CPS_CNTR_a5_a_a159, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- CPS_CNTR_a6_a_a165 = CARRY(CPS_CNTR_a6_a & !CPS_CNTR_a5_a_a159)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => CPS_CNTR_a6_a,
	cin => CPS_CNTR_a5_a_a159,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a6_a,
	cout => CPS_CNTR_a6_a_a165);

CPS_CNTR_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a14_a = DFFE(!GLOBAL(UPD_PROC_a0) & CPS_CNTR_a14_a $ (!CPS_CNTR_a13_a_a174), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- CPS_CNTR_a14_a_a168 = CARRY(CPS_CNTR_a14_a & (!CPS_CNTR_a13_a_a174))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a14_a,
	cin => CPS_CNTR_a13_a_a174,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a14_a,
	cout => CPS_CNTR_a14_a_a168);

Equal_a473_I : apex20ke_lcell
-- Equation(s):
-- Equal_a492 = CPS_CNTR_a6_a & CPS_REG_FF_adffs_a6_a & (CPS_REG_FF_adffs_a14_a $ !CPS_CNTR_a14_a) # !CPS_CNTR_a6_a & !CPS_REG_FF_adffs_a6_a & (CPS_REG_FF_adffs_a14_a $ !CPS_CNTR_a14_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a6_a,
	datab => CPS_REG_FF_adffs_a6_a,
	datac => CPS_REG_FF_adffs_a14_a,
	datad => CPS_CNTR_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a473,
	cascout => Equal_a492);

CPS_REG_FF_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a13_a = DFFE(cps_a13_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a13_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a13_a);

CPS_REG_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_REG_FF_adffs_a7_a = DFFE(cps_a7_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => cps_a7_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_REG_FF_adffs_a7_a);

CPS_CNTR_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a7_a = DFFE(!GLOBAL(UPD_PROC_a0) & CPS_CNTR_a7_a $ CPS_CNTR_a6_a_a165, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- CPS_CNTR_a7_a_a171 = CARRY(!CPS_CNTR_a6_a_a165 # !CPS_CNTR_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => CPS_CNTR_a7_a,
	cin => CPS_CNTR_a6_a_a165,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a7_a,
	cout => CPS_CNTR_a7_a_a171);

CPS_CNTR_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- CPS_CNTR_a13_a = DFFE(!GLOBAL(UPD_PROC_a0) & CPS_CNTR_a13_a $ (CPS_CNTR_a12_a_a162), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- CPS_CNTR_a13_a_a174 = CARRY(!CPS_CNTR_a12_a_a162 # !CPS_CNTR_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a13_a,
	cin => CPS_CNTR_a12_a_a162,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CPS_CNTR_a13_a,
	cout => CPS_CNTR_a13_a_a174);

Equal_a480_I : apex20ke_lcell
-- Equation(s):
-- Equal_a480 = (CPS_CNTR_a7_a & CPS_REG_FF_adffs_a7_a & (CPS_REG_FF_adffs_a13_a $ !CPS_CNTR_a13_a) # !CPS_CNTR_a7_a & !CPS_REG_FF_adffs_a7_a & (CPS_REG_FF_adffs_a13_a $ !CPS_CNTR_a13_a)) & CASCADE(Equal_a492)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_CNTR_a7_a,
	datab => CPS_REG_FF_adffs_a7_a,
	datac => CPS_REG_FF_adffs_a13_a,
	datad => CPS_CNTR_a13_a,
	cascin => Equal_a492,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a480);

cps_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(9),
	combout => cps_a9_a_acombout);

cps_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(8),
	combout => cps_a8_a_acombout);

peak1_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(4),
	combout => peak1_a4_a_acombout);

peak1_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(0),
	combout => peak1_a0_a_acombout);

peak1_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(6),
	combout => peak1_a6_a_acombout);

peak1_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(2),
	combout => peak1_a2_a_acombout);

Bit_Disable_a82_I : apex20ke_lcell
-- Equation(s):
-- Bit_Disable_a82 = !peak1_a0_a_acombout & !peak1_a6_a_acombout & !peak1_a2_a_acombout & !peak1_a4_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak1_a0_a_acombout,
	datab => peak1_a6_a_acombout,
	datac => peak1_a2_a_acombout,
	datad => peak1_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Bit_Disable_a82);

UPD_PROC_a0_I : apex20ke_lcell
-- Equation(s):
-- UPD_PROC_a0 = Equal_a447 # Bit_Disable_a82 & Equal_a475 & Bit_Disable_a89

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F8F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Bit_Disable_a82,
	datab => Equal_a475,
	datac => Equal_a447,
	datad => Bit_Disable_a89,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => UPD_PROC_a0);

cps_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(4),
	combout => cps_a4_a_acombout);

cps_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(1),
	combout => cps_a1_a_acombout);

cps_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(11),
	combout => cps_a11_a_acombout);

cps_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(10),
	combout => cps_a10_a_acombout);

cps_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(2),
	combout => cps_a2_a_acombout);

cps_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(0),
	combout => cps_a0_a_acombout);

cps_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(3),
	combout => cps_a3_a_acombout);

cps_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(15),
	combout => cps_a15_a_acombout);

cps_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(12),
	combout => cps_a12_a_acombout);

cps_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(5),
	combout => cps_a5_a_acombout);

cps_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(14),
	combout => cps_a14_a_acombout);

cps_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(6),
	combout => cps_a6_a_acombout);

cps_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(13),
	combout => cps_a13_a_acombout);

cps_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(7),
	combout => cps_a7_a_acombout);

peak1_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(9),
	combout => peak1_a9_a_acombout);

peak1_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(3),
	combout => peak1_a3_a_acombout);

peak1_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(8),
	combout => peak1_a8_a_acombout);

peak1_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(7),
	combout => peak1_a7_a_acombout);

Bit_Disable_a87_I : apex20ke_lcell
-- Equation(s):
-- Bit_Disable_a91 = !peak1_a9_a_acombout & !peak1_a7_a_acombout & !peak1_a8_a_acombout & !peak1_a3_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak1_a9_a_acombout,
	datab => peak1_a7_a_acombout,
	datac => peak1_a8_a_acombout,
	datad => peak1_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Bit_Disable_a87,
	cascout => Bit_Disable_a91);

peak1_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(11),
	combout => peak1_a11_a_acombout);

peak1_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(10),
	combout => peak1_a10_a_acombout);

peak1_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(5),
	combout => peak1_a5_a_acombout);

peak1_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(1),
	combout => peak1_a1_a_acombout);

Bit_Disable_a89_I : apex20ke_lcell
-- Equation(s):
-- Bit_Disable_a89 = (!peak1_a10_a_acombout & !peak1_a5_a_acombout & !peak1_a11_a_acombout & !peak1_a1_a_acombout) & CASCADE(Bit_Disable_a91)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak1_a10_a_acombout,
	datab => peak1_a5_a_acombout,
	datac => peak1_a11_a_acombout,
	datad => peak1_a1_a_acombout,
	cascin => Bit_Disable_a91,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Bit_Disable_a89);

clk20_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk20,
	combout => clk20_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

ROM_ADR_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_ADR_a0_a = DFFE(GLOBAL(ROM_EN) & !ROM_ADR_a0_a, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- ROM_ADR_a0_a_a65 = CARRY(ROM_ADR_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "55AA",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROM_ADR_a0_a,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_ROM_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_ADR_a0_a,
	cout => ROM_ADR_a0_a_a65);

ROM_ADR_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_ADR_a1_a = DFFE(GLOBAL(ROM_EN) & ROM_ADR_a1_a $ (ROM_ADR_a0_a_a65), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- ROM_ADR_a1_a_a68 = CARRY(!ROM_ADR_a0_a_a65 # !ROM_ADR_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROM_ADR_a1_a,
	cin => ROM_ADR_a0_a_a65,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_ROM_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_ADR_a1_a,
	cout => ROM_ADR_a1_a_a68);

ROM_ADR_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_ADR_a2_a = DFFE(GLOBAL(ROM_EN) & ROM_ADR_a2_a $ (!ROM_ADR_a1_a_a68), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- ROM_ADR_a2_a_a71 = CARRY(ROM_ADR_a2_a & (!ROM_ADR_a1_a_a68))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROM_ADR_a2_a,
	cin => ROM_ADR_a1_a_a68,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_ROM_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_ADR_a2_a,
	cout => ROM_ADR_a2_a_a71);

Equal_a448_I : apex20ke_lcell
-- Equation(s):
-- Equal_a448 = ROM_ADR_a3_a & ROM_ADR_a1_a & ROM_ADR_a2_a & ROM_ADR_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROM_ADR_a3_a,
	datab => ROM_ADR_a1_a,
	datac => ROM_ADR_a2_a,
	datad => ROM_ADR_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a448);

ROM_ADR_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_ADR_a3_a = DFFE(GLOBAL(ROM_EN) & ROM_ADR_a3_a $ (ROM_ADR_a2_a_a71), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- ROM_ADR_a3_a_a74 = CARRY(!ROM_ADR_a2_a_a71 # !ROM_ADR_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROM_ADR_a3_a,
	cin => ROM_ADR_a2_a_a71,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_ROM_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_ADR_a3_a,
	cout => ROM_ADR_a3_a_a74);

ROM_ADR_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_ADR_a4_a = DFFE(GLOBAL(ROM_EN) & ROM_ADR_a4_a $ (!ROM_ADR_a3_a_a74), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- ROM_ADR_a4_a_a77 = CARRY(ROM_ADR_a4_a & (!ROM_ADR_a3_a_a74))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROM_ADR_a4_a,
	cin => ROM_ADR_a3_a_a74,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_ROM_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_ADR_a4_a,
	cout => ROM_ADR_a4_a_a77);

ROM_ADR_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_ADR_a5_a = DFFE(GLOBAL(ROM_EN) & ROM_ADR_a5_a $ ROM_ADR_a4_a_a77, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- ROM_ADR_a5_a_a80 = CARRY(!ROM_ADR_a4_a_a77 # !ROM_ADR_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ROM_ADR_a5_a,
	cin => ROM_ADR_a4_a_a77,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_ROM_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_ADR_a5_a,
	cout => ROM_ADR_a5_a_a80);

ROM_ADR_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_ADR_a6_a = DFFE(GLOBAL(ROM_EN) & ROM_ADR_a6_a $ !ROM_ADR_a5_a_a80, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- ROM_ADR_a6_a_a83 = CARRY(ROM_ADR_a6_a & !ROM_ADR_a5_a_a80)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ROM_ADR_a6_a,
	cin => ROM_ADR_a5_a_a80,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_ROM_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_ADR_a6_a,
	cout => ROM_ADR_a6_a_a83);

ROM_ADR_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_ADR_a7_a = DFFE(GLOBAL(ROM_EN) & ROM_ADR_a6_a_a83 $ ROM_ADR_a7_a, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => ROM_ADR_a7_a,
	cin => ROM_ADR_a6_a_a83,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_ROM_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_ADR_a7_a);

Equal_a449_I : apex20ke_lcell
-- Equation(s):
-- Equal_a449 = ROM_ADR_a5_a & ROM_ADR_a4_a & ROM_ADR_a7_a & ROM_ADR_a6_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROM_ADR_a5_a,
	datab => ROM_ADR_a4_a,
	datac => ROM_ADR_a7_a,
	datad => ROM_ADR_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a449);

ROM_EN_aI : apex20ke_lcell
-- Equation(s):
-- ROM_EN = DFFE(Equal_a447 # ROM_EN & (!Equal_a449 # !Equal_a448), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "BAFA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a447,
	datab => Equal_a448,
	datac => ROM_EN,
	datad => Equal_a449,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_EN);

U_ROM_asrom_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000101000100000001010001000101010001000100010001010000010101010000010101000001000101010101000100000001000000000101000000000001010101010001000101010001010100000101010001010001010101010100010101010001010000000101000100010000010000010001010101010001001010100010",
	address_width => 8,
	bit_number => 0,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a0_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a0_a_modesel,
	dataout => U_ROM_asrom_aq_a0_a);

WADR_CNTR_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- WADR_CNTR_awysi_counter_asload_path_a0_a = DFFE(!WADR_CNTR_awysi_counter_asload_path_a0_a, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- WADR_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(WADR_CNTR_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0FF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WADR_CNTR_awysi_counter_asload_path_a0_a,
	cout => WADR_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT);

WADR_CNTR_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- WADR_CNTR_awysi_counter_asload_path_a1_a = DFFE(WADR_CNTR_awysi_counter_asload_path_a1_a $ (WADR_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- WADR_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!WADR_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT # !WADR_CNTR_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a1_a,
	cin => WADR_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WADR_CNTR_awysi_counter_asload_path_a1_a,
	cout => WADR_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT);

WADR_CNTR_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- WADR_CNTR_awysi_counter_asload_path_a2_a = DFFE(WADR_CNTR_awysi_counter_asload_path_a2_a $ (!WADR_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- WADR_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(WADR_CNTR_awysi_counter_asload_path_a2_a & (!WADR_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a2_a,
	cin => WADR_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WADR_CNTR_awysi_counter_asload_path_a2_a,
	cout => WADR_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT);

WADR_CNTR_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- WADR_CNTR_awysi_counter_asload_path_a3_a = DFFE(WADR_CNTR_awysi_counter_asload_path_a3_a $ (WADR_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- WADR_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!WADR_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT # !WADR_CNTR_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a3_a,
	cin => WADR_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WADR_CNTR_awysi_counter_asload_path_a3_a,
	cout => WADR_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT);

WADR_CNTR_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- WADR_CNTR_awysi_counter_asload_path_a4_a = DFFE(WADR_CNTR_awysi_counter_asload_path_a4_a $ (!WADR_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- WADR_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(WADR_CNTR_awysi_counter_asload_path_a4_a & (!WADR_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a4_a,
	cin => WADR_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WADR_CNTR_awysi_counter_asload_path_a4_a,
	cout => WADR_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT);

WADR_CNTR_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- WADR_CNTR_awysi_counter_asload_path_a5_a = DFFE(WADR_CNTR_awysi_counter_asload_path_a5_a $ (WADR_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- WADR_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!WADR_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT # !WADR_CNTR_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a5_a,
	cin => WADR_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WADR_CNTR_awysi_counter_asload_path_a5_a,
	cout => WADR_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT);

WADR_CNTR_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- WADR_CNTR_awysi_counter_asload_path_a6_a = DFFE(WADR_CNTR_awysi_counter_asload_path_a6_a $ (!WADR_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- WADR_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(WADR_CNTR_awysi_counter_asload_path_a6_a & (!WADR_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a6_a,
	cin => WADR_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WADR_CNTR_awysi_counter_asload_path_a6_a,
	cout => WADR_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT);

WADR_CNTR_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- WADR_CNTR_awysi_counter_asload_path_a7_a = DFFE(WADR_CNTR_awysi_counter_asload_path_a7_a $ (WADR_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- WADR_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!WADR_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT # !WADR_CNTR_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a7_a,
	cin => WADR_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WADR_CNTR_awysi_counter_asload_path_a7_a,
	cout => WADR_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT);

WADR_CNTR_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- WADR_CNTR_awysi_counter_asload_path_a8_a = DFFE(WADR_CNTR_awysi_counter_asload_path_a8_a $ (!WADR_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- WADR_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(WADR_CNTR_awysi_counter_asload_path_a8_a & (!WADR_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a8_a,
	cin => WADR_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WADR_CNTR_awysi_counter_asload_path_a8_a,
	cout => WADR_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT);

WADR_CNTR_awysi_counter_acounter_cell_a9_a : apex20ke_lcell
-- Equation(s):
-- WADR_CNTR_awysi_counter_asload_path_a9_a = DFFE(WADR_CNTR_awysi_counter_asload_path_a9_a $ (WADR_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a9_a,
	cin => WADR_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WADR_CNTR_awysi_counter_asload_path_a9_a);

pk2dly_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(0),
	combout => pk2dly_a0_a_acombout);

PK2_DELAY_REG_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- PK2_DELAY_REG_FF_adffs_a0_a = DFFE(pk2dly_a0_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pk2dly_a0_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PK2_DELAY_REG_FF_adffs_a0_a);

add_a593_I : apex20ke_lcell
-- Equation(s):
-- add_a593 = WADR_CNTR_awysi_counter_asload_path_a0_a $ PK2_DELAY_REG_FF_adffs_a0_a
-- add_a595 = CARRY(WADR_CNTR_awysi_counter_asload_path_a0_a # !PK2_DELAY_REG_FF_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66BB",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a0_a,
	datab => PK2_DELAY_REG_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a593,
	cout => add_a595);

pk2dly_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(1),
	combout => pk2dly_a1_a_acombout);

PK2_DELAY_REG_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- PK2_DELAY_REG_FF_adffs_a1_a = DFFE(pk2dly_a1_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pk2dly_a1_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PK2_DELAY_REG_FF_adffs_a1_a);

add_a597_I : apex20ke_lcell
-- Equation(s):
-- add_a597 = WADR_CNTR_awysi_counter_asload_path_a1_a $ PK2_DELAY_REG_FF_adffs_a1_a $ !add_a595
-- add_a599 = CARRY(WADR_CNTR_awysi_counter_asload_path_a1_a & PK2_DELAY_REG_FF_adffs_a1_a & !add_a595 # !WADR_CNTR_awysi_counter_asload_path_a1_a & (PK2_DELAY_REG_FF_adffs_a1_a # !add_a595))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a1_a,
	datab => PK2_DELAY_REG_FF_adffs_a1_a,
	cin => add_a595,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a597,
	cout => add_a599);

pk2dly_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(2),
	combout => pk2dly_a2_a_acombout);

PK2_DELAY_REG_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- PK2_DELAY_REG_FF_adffs_a2_a = DFFE(pk2dly_a2_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pk2dly_a2_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PK2_DELAY_REG_FF_adffs_a2_a);

add_a601_I : apex20ke_lcell
-- Equation(s):
-- add_a601 = WADR_CNTR_awysi_counter_asload_path_a2_a $ PK2_DELAY_REG_FF_adffs_a2_a $ add_a599
-- add_a603 = CARRY(WADR_CNTR_awysi_counter_asload_path_a2_a & (!add_a599 # !PK2_DELAY_REG_FF_adffs_a2_a) # !WADR_CNTR_awysi_counter_asload_path_a2_a & !PK2_DELAY_REG_FF_adffs_a2_a & !add_a599)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a2_a,
	datab => PK2_DELAY_REG_FF_adffs_a2_a,
	cin => add_a599,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a601,
	cout => add_a603);

pk2dly_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(3),
	combout => pk2dly_a3_a_acombout);

PK2_DELAY_REG_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- PK2_DELAY_REG_FF_adffs_a3_a = DFFE(pk2dly_a3_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pk2dly_a3_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PK2_DELAY_REG_FF_adffs_a3_a);

add_a605_I : apex20ke_lcell
-- Equation(s):
-- add_a605 = WADR_CNTR_awysi_counter_asload_path_a3_a $ PK2_DELAY_REG_FF_adffs_a3_a $ !add_a603
-- add_a607 = CARRY(WADR_CNTR_awysi_counter_asload_path_a3_a & PK2_DELAY_REG_FF_adffs_a3_a & !add_a603 # !WADR_CNTR_awysi_counter_asload_path_a3_a & (PK2_DELAY_REG_FF_adffs_a3_a # !add_a603))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a3_a,
	datab => PK2_DELAY_REG_FF_adffs_a3_a,
	cin => add_a603,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a605,
	cout => add_a607);

pk2dly_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(4),
	combout => pk2dly_a4_a_acombout);

PK2_DELAY_REG_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- PK2_DELAY_REG_FF_adffs_a4_a = DFFE(pk2dly_a4_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pk2dly_a4_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PK2_DELAY_REG_FF_adffs_a4_a);

add_a609_I : apex20ke_lcell
-- Equation(s):
-- add_a609 = WADR_CNTR_awysi_counter_asload_path_a4_a $ PK2_DELAY_REG_FF_adffs_a4_a $ add_a607
-- add_a611 = CARRY(WADR_CNTR_awysi_counter_asload_path_a4_a & (!add_a607 # !PK2_DELAY_REG_FF_adffs_a4_a) # !WADR_CNTR_awysi_counter_asload_path_a4_a & !PK2_DELAY_REG_FF_adffs_a4_a & !add_a607)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a4_a,
	datab => PK2_DELAY_REG_FF_adffs_a4_a,
	cin => add_a607,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a609,
	cout => add_a611);

pk2dly_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(5),
	combout => pk2dly_a5_a_acombout);

PK2_DELAY_REG_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- PK2_DELAY_REG_FF_adffs_a5_a = DFFE(pk2dly_a5_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pk2dly_a5_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PK2_DELAY_REG_FF_adffs_a5_a);

add_a613_I : apex20ke_lcell
-- Equation(s):
-- add_a613 = WADR_CNTR_awysi_counter_asload_path_a5_a $ PK2_DELAY_REG_FF_adffs_a5_a $ !add_a611
-- add_a615 = CARRY(WADR_CNTR_awysi_counter_asload_path_a5_a & PK2_DELAY_REG_FF_adffs_a5_a & !add_a611 # !WADR_CNTR_awysi_counter_asload_path_a5_a & (PK2_DELAY_REG_FF_adffs_a5_a # !add_a611))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a5_a,
	datab => PK2_DELAY_REG_FF_adffs_a5_a,
	cin => add_a611,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a613,
	cout => add_a615);

pk2dly_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(6),
	combout => pk2dly_a6_a_acombout);

PK2_DELAY_REG_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- PK2_DELAY_REG_FF_adffs_a6_a = DFFE(pk2dly_a6_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pk2dly_a6_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PK2_DELAY_REG_FF_adffs_a6_a);

add_a617_I : apex20ke_lcell
-- Equation(s):
-- add_a617 = WADR_CNTR_awysi_counter_asload_path_a6_a $ PK2_DELAY_REG_FF_adffs_a6_a $ add_a615
-- add_a619 = CARRY(WADR_CNTR_awysi_counter_asload_path_a6_a & (!add_a615 # !PK2_DELAY_REG_FF_adffs_a6_a) # !WADR_CNTR_awysi_counter_asload_path_a6_a & !PK2_DELAY_REG_FF_adffs_a6_a & !add_a615)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a6_a,
	datab => PK2_DELAY_REG_FF_adffs_a6_a,
	cin => add_a615,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a617,
	cout => add_a619);

pk2dly_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(7),
	combout => pk2dly_a7_a_acombout);

PK2_DELAY_REG_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- PK2_DELAY_REG_FF_adffs_a7_a = DFFE(pk2dly_a7_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pk2dly_a7_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PK2_DELAY_REG_FF_adffs_a7_a);

add_a621_I : apex20ke_lcell
-- Equation(s):
-- add_a621 = WADR_CNTR_awysi_counter_asload_path_a7_a $ PK2_DELAY_REG_FF_adffs_a7_a $ !add_a619
-- add_a623 = CARRY(WADR_CNTR_awysi_counter_asload_path_a7_a & PK2_DELAY_REG_FF_adffs_a7_a & !add_a619 # !WADR_CNTR_awysi_counter_asload_path_a7_a & (PK2_DELAY_REG_FF_adffs_a7_a # !add_a619))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a7_a,
	datab => PK2_DELAY_REG_FF_adffs_a7_a,
	cin => add_a619,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a621,
	cout => add_a623);

pk2dly_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(8),
	combout => pk2dly_a8_a_acombout);

PK2_DELAY_REG_FF_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- PK2_DELAY_REG_FF_adffs_a8_a = DFFE(pk2dly_a8_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pk2dly_a8_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PK2_DELAY_REG_FF_adffs_a8_a);

add_a625_I : apex20ke_lcell
-- Equation(s):
-- add_a625 = WADR_CNTR_awysi_counter_asload_path_a8_a $ PK2_DELAY_REG_FF_adffs_a8_a $ add_a623
-- add_a627 = CARRY(WADR_CNTR_awysi_counter_asload_path_a8_a & (!add_a623 # !PK2_DELAY_REG_FF_adffs_a8_a) # !WADR_CNTR_awysi_counter_asload_path_a8_a & !PK2_DELAY_REG_FF_adffs_a8_a & !add_a623)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WADR_CNTR_awysi_counter_asload_path_a8_a,
	datab => PK2_DELAY_REG_FF_adffs_a8_a,
	cin => add_a623,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a625,
	cout => add_a627);

pk2dly_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(9),
	combout => pk2dly_a9_a_acombout);

PK2_DELAY_REG_FF_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- PK2_DELAY_REG_FF_adffs_a9_a = DFFE(pk2dly_a9_a_acombout, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pk2dly_a9_a_acombout,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PK2_DELAY_REG_FF_adffs_a9_a);

add_a629_I : apex20ke_lcell
-- Equation(s):
-- add_a629 = WADR_CNTR_awysi_counter_asload_path_a9_a $ add_a627 $ !PK2_DELAY_REG_FF_adffs_a9_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CC3",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => WADR_CNTR_awysi_counter_asload_path_a9_a,
	datad => PK2_DELAY_REG_FF_adffs_a9_a,
	cin => add_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a629);

DLY1_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 0,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a0_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a0_a_modesel,
	dataout => DLY1_asram_aq_a0_a);

peak2_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(0),
	combout => peak2_a0_a_acombout);

peak2_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(5),
	combout => peak2_a5_a_acombout);

peak2_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(2),
	combout => peak2_a2_a_acombout);

peak2_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(4),
	combout => peak2_a4_a_acombout);

peak2_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(8),
	combout => peak2_a8_a_acombout);

peak2_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(10),
	combout => peak2_a10_a_acombout);

peak2_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(1),
	combout => peak2_a1_a_acombout);

peak2_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(11),
	combout => peak2_a11_a_acombout);

peak2_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(9),
	combout => peak2_a9_a_acombout);

Equal_a465_I : apex20ke_lcell
-- Equation(s):
-- Equal_a487 = !peak2_a3_a_acombout & !peak2_a1_a_acombout & !peak2_a11_a_acombout & !peak2_a9_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak2_a3_a_acombout,
	datab => peak2_a1_a_acombout,
	datac => peak2_a11_a_acombout,
	datad => peak2_a9_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a465,
	cascout => Equal_a487);

Equal_a476_I : apex20ke_lcell
-- Equation(s):
-- Equal_a488 = (!peak2_a7_a_acombout & !peak2_a4_a_acombout & !peak2_a8_a_acombout & !peak2_a10_a_acombout) & CASCADE(Equal_a487)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak2_a7_a_acombout,
	datab => peak2_a4_a_acombout,
	datac => peak2_a8_a_acombout,
	datad => peak2_a10_a_acombout,
	cascin => Equal_a487,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a476,
	cascout => Equal_a488);

Equal_a475_I : apex20ke_lcell
-- Equation(s):
-- Equal_a475 = (!peak2_a6_a_acombout & !peak2_a0_a_acombout & !peak2_a5_a_acombout & !peak2_a2_a_acombout) & CASCADE(Equal_a488)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak2_a6_a_acombout,
	datab => peak2_a0_a_acombout,
	datac => peak2_a5_a_acombout,
	datad => peak2_a2_a_acombout,
	cascin => Equal_a488,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a475);

ROM_DATA_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a0_a = DFFE(DLY1_asram_aq_a0_a & (!Equal_a475), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DLY1_asram_aq_a0_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a0_a);

BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a0_a = DFFE(U_ROM_asrom_aq_a0_a $ ROM_DATA_FF_adffs_a0_a, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a0_a_a55 = CARRY(U_ROM_asrom_aq_a0_a & ROM_DATA_FF_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ROM_asrom_aq_a0_a,
	datab => ROM_DATA_FF_adffs_a0_a,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a0_a,
	cout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a0_a_a55);

U_ROM_asrom_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000111010111101110011110010110001111000110110100110101011100100001011100111110000110001100111100010100101010111100111111111111001000110001111000100101110110111100100001100100100110011001001001110101100100010010010111100000001011110100100110011111001011101011",
	address_width => 8,
	bit_number => 1,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a1_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a1_a_modesel,
	dataout => U_ROM_asrom_aq_a1_a);

DLY1_asram_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 1,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a1_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a1_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a1_a_modesel,
	dataout => DLY1_asram_aq_a1_a);

ROM_DATA_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a1_a = DFFE(DLY1_asram_aq_a1_a & !Equal_a475, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DLY1_asram_aq_a1_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a1_a);

BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a1_a = DFFE(U_ROM_asrom_aq_a1_a $ ROM_DATA_FF_adffs_a1_a $ BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a0_a_a55, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a1_a_a58 = CARRY(U_ROM_asrom_aq_a1_a & !ROM_DATA_FF_adffs_a1_a & !BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a0_a_a55 # !U_ROM_asrom_aq_a1_a & 
-- (!BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a0_a_a55 # !ROM_DATA_FF_adffs_a1_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ROM_asrom_aq_a1_a,
	datab => ROM_DATA_FF_adffs_a1_a,
	cin => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a0_a_a55,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a1_a,
	cout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a1_a_a58);

U_ROM_asrom_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001001100010110000001111100110111111000110011101011101100111111011001100010100010110111001010110110011110110010110101010111010000101001011000100010111011000111001000100111101010010100001111011001000111001011011100110111111101100011101010011100101111100110001",
	address_width => 8,
	bit_number => 2,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a2_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a2_a_modesel,
	dataout => U_ROM_asrom_aq_a2_a);

DLY1_asram_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 2,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a2_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a2_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a2_a_modesel,
	dataout => DLY1_asram_aq_a2_a);

ROM_DATA_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a2_a = DFFE(DLY1_asram_aq_a2_a & (!Equal_a475), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DLY1_asram_aq_a2_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a2_a);

BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a2_a = DFFE(U_ROM_asrom_aq_a2_a $ ROM_DATA_FF_adffs_a2_a $ !BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a1_a_a58, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a2_a_a61 = CARRY(U_ROM_asrom_aq_a2_a & (ROM_DATA_FF_adffs_a2_a # !BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a1_a_a58) # !U_ROM_asrom_aq_a2_a & ROM_DATA_FF_adffs_a2_a & 
-- !BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a1_a_a58)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ROM_asrom_aq_a2_a,
	datab => ROM_DATA_FF_adffs_a2_a,
	cin => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a1_a_a58,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a2_a,
	cout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a2_a_a61);

U_ROM_asrom_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001111000000111111111110101000111111010011001100110110010111111000101000001101011001111011011001110000000001110011001101101001000111010010010110100110001010000001101001010110011110000101111001111011101010011101010111000100011111100011001110111101110111100000",
	address_width => 8,
	bit_number => 3,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a3_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a3_a_modesel,
	dataout => U_ROM_asrom_aq_a3_a);

DLY1_asram_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 3,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a3_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a3_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a3_a_modesel,
	dataout => DLY1_asram_aq_a3_a);

ROM_DATA_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a3_a = DFFE(DLY1_asram_aq_a3_a & (!Equal_a475), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DLY1_asram_aq_a3_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a3_a);

BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a3_a = DFFE(U_ROM_asrom_aq_a3_a $ ROM_DATA_FF_adffs_a3_a $ BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a2_a_a61, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a3_a_a64 = CARRY(U_ROM_asrom_aq_a3_a & !ROM_DATA_FF_adffs_a3_a & !BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a2_a_a61 # !U_ROM_asrom_aq_a3_a & 
-- (!BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a2_a_a61 # !ROM_DATA_FF_adffs_a3_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ROM_asrom_aq_a3_a,
	datab => ROM_DATA_FF_adffs_a3_a,
	cin => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a2_a_a61,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a3_a,
	cout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a3_a_a64);

U_ROM_asrom_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001111000000111111111110011111000000110001000011110111100111111000001011111101100000000111011111110000000000001111110000011100111100110110110010010010101100111110011011011000001011010111010110010111100010100100110111111000010000000000111110000010001111100000",
	address_width => 8,
	bit_number => 4,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a4_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a4_a_modesel,
	dataout => U_ROM_asrom_aq_a4_a);

DLY1_asram_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 4,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a4_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a4_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a4_a_modesel,
	dataout => DLY1_asram_aq_a4_a);

ROM_DATA_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a4_a = DFFE(DLY1_asram_aq_a4_a & (!Equal_a475), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DLY1_asram_aq_a4_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a4_a);

BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a4_a = DFFE(U_ROM_asrom_aq_a4_a $ ROM_DATA_FF_adffs_a4_a $ !BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a3_a_a64, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a4_a_a67 = CARRY(U_ROM_asrom_aq_a4_a & (ROM_DATA_FF_adffs_a4_a # !BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a3_a_a64) # !U_ROM_asrom_aq_a4_a & ROM_DATA_FF_adffs_a4_a & 
-- !BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a3_a_a64)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ROM_asrom_aq_a4_a,
	datab => ROM_DATA_FF_adffs_a4_a,
	cin => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a3_a_a64,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a4_a,
	cout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a4_a_a67);

DLY1_asram_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 5,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a5_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a5_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a5_a_modesel,
	dataout => DLY1_asram_aq_a5_a);

ROM_DATA_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a5_a = DFFE(DLY1_asram_aq_a5_a & !Equal_a475, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DLY1_asram_aq_a5_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a5_a);

U_ROM_asrom_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000111111000000000001111111111111110000111111110111111000000111110111111101111111111111011111110000000000000000000000000100000011110001110001110001100101101010101101100000000110110011001111100001001001101000011100000000011111111111111110000000000000011111",
	address_width => 8,
	bit_number => 5,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a5_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a5_a_modesel,
	dataout => U_ROM_asrom_aq_a5_a);

BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a5_a = DFFE(ROM_DATA_FF_adffs_a5_a $ U_ROM_asrom_aq_a5_a $ BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a4_a_a67, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a5_a_a70 = CARRY(ROM_DATA_FF_adffs_a5_a & !U_ROM_asrom_aq_a5_a & !BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a4_a_a67 # !ROM_DATA_FF_adffs_a5_a & 
-- (!BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a4_a_a67 # !U_ROM_asrom_aq_a5_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROM_DATA_FF_adffs_a5_a,
	datab => U_ROM_asrom_aq_a5_a,
	cin => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a4_a_a67,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a5_a,
	cout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a5_a_a70);

U_ROM_asrom_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001111111111111111111111111111111111110000000000001000000000000000000000000010000000000000100000001111111111111111111111111100000000001111110000001111100011100110011011010101010100100101101010101110011100001110110111111111100000000000000001111111111111111111",
	address_width => 8,
	bit_number => 6,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a6_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a6_a_modesel,
	dataout => U_ROM_asrom_aq_a6_a);

DLY1_asram_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 6,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a6_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a6_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a6_a_modesel,
	dataout => DLY1_asram_aq_a6_a);

ROM_DATA_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a6_a = DFFE(DLY1_asram_aq_a6_a & (!Equal_a475), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DLY1_asram_aq_a6_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a6_a);

BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a6_a = DFFE(U_ROM_asrom_aq_a6_a $ ROM_DATA_FF_adffs_a6_a $ !BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a5_a_a70, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a6_a_a73 = CARRY(U_ROM_asrom_aq_a6_a & (ROM_DATA_FF_adffs_a6_a # !BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a5_a_a70) # !U_ROM_asrom_aq_a6_a & ROM_DATA_FF_adffs_a6_a & 
-- !BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a5_a_a70)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ROM_asrom_aq_a6_a,
	datab => ROM_DATA_FF_adffs_a6_a,
	cin => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a5_a_a70,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a6_a,
	cout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a6_a_a73);

DLY1_asram_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 7,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a7_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a7_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a7_a_modesel,
	dataout => DLY1_asram_aq_a7_a);

ROM_DATA_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a7_a = DFFE(DLY1_asram_aq_a7_a & !Equal_a475, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DLY1_asram_aq_a7_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a7_a);

U_ROM_asrom_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001111111111111111111111111111111111110000000000000000000000000000000000000000000000000000000000000000000000000000000000000011111111111111110000000000011111100001111000110011001101101100100110011111101011110001100111111111111111111111111111111111111111111111",
	address_width => 8,
	bit_number => 7,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a7_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a7_a_modesel,
	dataout => U_ROM_asrom_aq_a7_a);

BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a7_a = DFFE(ROM_DATA_FF_adffs_a7_a $ U_ROM_asrom_aq_a7_a $ BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a6_a_a73, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a7_a_a76 = CARRY(ROM_DATA_FF_adffs_a7_a & !U_ROM_asrom_aq_a7_a & !BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a6_a_a73 # !ROM_DATA_FF_adffs_a7_a & 
-- (!BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a6_a_a73 # !U_ROM_asrom_aq_a7_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROM_DATA_FF_adffs_a7_a,
	datab => U_ROM_asrom_aq_a7_a,
	cin => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a6_a_a73,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a7_a,
	cout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a7_a_a76);

BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a8_a = DFFE(!BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a7_a_a76, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0F0F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a7_a_a76,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a8_a);

DLY1_asram_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 8,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a8_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a8_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a8_a_modesel,
	dataout => DLY1_asram_aq_a8_a);

ROM_DATA_FF_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a8_a = DFFE(DLY1_asram_aq_a8_a & !Equal_a475, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DLY1_asram_aq_a8_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a8_a);

BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a0_a = DFFE(U_ROM_asrom_aq_a8_a $ ROM_DATA_FF_adffs_a8_a, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a0_a_a49 = CARRY(U_ROM_asrom_aq_a8_a & ROM_DATA_FF_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ROM_asrom_aq_a8_a,
	datab => ROM_DATA_FF_adffs_a8_a,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a0_a,
	cout => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a0_a_a49);

BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a166_I : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a166 = BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a8_a $ BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a0_a
-- BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a168 = CARRY(BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a8_a & BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a8_a,
	datab => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a166,
	cout => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a168);

U_ROM_asrom_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001111111111111111111111111000000000111111100000011111111111111100110011000000000000000000000000000000000000000000000000",
	address_width => 8,
	bit_number => 9,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a9_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a9_a_modesel,
	dataout => U_ROM_asrom_aq_a9_a);

BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a1_a = DFFE(ROM_DATA_FF_adffs_a9_a $ U_ROM_asrom_aq_a9_a $ BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a0_a_a49, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a1_a_a52 = CARRY(ROM_DATA_FF_adffs_a9_a & !U_ROM_asrom_aq_a9_a & !BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a0_a_a49 # !ROM_DATA_FF_adffs_a9_a & 
-- (!BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a0_a_a49 # !U_ROM_asrom_aq_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROM_DATA_FF_adffs_a9_a,
	datab => U_ROM_asrom_aq_a9_a,
	cin => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a0_a_a49,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a1_a,
	cout => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a1_a_a52);

BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a170_I : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a170 = BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a1_a $ BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a168
-- BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a172 = CARRY(!BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a168 # !BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a1_a,
	cin => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a168,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a170,
	cout => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a172);

U_ROM_asrom_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000000000000000011111111111111111111111000011111111111111111111111111111111111111111111111111",
	address_width => 8,
	bit_number => 10,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a10_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a10_a_modesel,
	dataout => U_ROM_asrom_aq_a10_a);

BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a2_a = DFFE(ROM_DATA_FF_adffs_a10_a $ U_ROM_asrom_aq_a10_a $ !BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a1_a_a52, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a2_a_a55 = CARRY(ROM_DATA_FF_adffs_a10_a & (U_ROM_asrom_aq_a10_a # !BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a1_a_a52) # !ROM_DATA_FF_adffs_a10_a & U_ROM_asrom_aq_a10_a & 
-- !BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a1_a_a52)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROM_DATA_FF_adffs_a10_a,
	datab => U_ROM_asrom_aq_a10_a,
	cin => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a1_a_a52,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a2_a,
	cout => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a2_a_a55);

BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a174_I : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a174 = BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a2_a $ !BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a172
-- BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a176 = CARRY(BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a2_a & !BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a172)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a2_a,
	cin => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a172,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a174,
	cout => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a176);

U_ROM_asrom_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000111111111111111111111111111111111111111111100000000000000000000000000000000000000000000000000",
	address_width => 8,
	bit_number => 11,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a11_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a11_a_modesel,
	dataout => U_ROM_asrom_aq_a11_a);

BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a3_a = DFFE(ROM_DATA_FF_adffs_a11_a $ U_ROM_asrom_aq_a11_a $ BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a2_a_a55, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a3_a_a58 = CARRY(ROM_DATA_FF_adffs_a11_a & !U_ROM_asrom_aq_a11_a & !BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a2_a_a55 # !ROM_DATA_FF_adffs_a11_a & 
-- (!BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a2_a_a55 # !U_ROM_asrom_aq_a11_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROM_DATA_FF_adffs_a11_a,
	datab => U_ROM_asrom_aq_a11_a,
	cin => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a2_a_a55,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a3_a,
	cout => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a3_a_a58);

BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a178_I : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a178 = BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a3_a $ BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a176
-- BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a180 = CARRY(!BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a176 # !BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a3_a,
	cin => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a176,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a178,
	cout => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a180);

DLY1_asram_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 12,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a12_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a12_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a12_a_modesel,
	dataout => DLY1_asram_aq_a12_a);

ROM_DATA_FF_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a12_a = DFFE(DLY1_asram_aq_a12_a & !Equal_a475, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DLY1_asram_aq_a12_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a12_a);

BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a4_a = DFFE(U_ROM_asrom_aq_a12_a $ ROM_DATA_FF_adffs_a12_a $ !BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a3_a_a58, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a4_a_a61 = CARRY(U_ROM_asrom_aq_a12_a & (ROM_DATA_FF_adffs_a12_a # !BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a3_a_a58) # !U_ROM_asrom_aq_a12_a & ROM_DATA_FF_adffs_a12_a & 
-- !BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a3_a_a58)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_ROM_asrom_aq_a12_a,
	datab => ROM_DATA_FF_adffs_a12_a,
	cin => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a3_a_a58,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a4_a,
	cout => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a4_a_a61);

BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a182_I : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a182 = BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a4_a $ !BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a180
-- BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a184 = CARRY(BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a4_a & !BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a180)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a4_a,
	cin => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a180,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a182,
	cout => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a184);

U_ROM_asrom_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
	address_width => 8,
	bit_number => 13,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a13_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a13_a_modesel,
	dataout => U_ROM_asrom_aq_a13_a);

BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a5_a = DFFE(ROM_DATA_FF_adffs_a13_a $ U_ROM_asrom_aq_a13_a $ BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a4_a_a61, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a5_a_a64 = CARRY(ROM_DATA_FF_adffs_a13_a & !U_ROM_asrom_aq_a13_a & !BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a4_a_a61 # !ROM_DATA_FF_adffs_a13_a & 
-- (!BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a4_a_a61 # !U_ROM_asrom_aq_a13_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROM_DATA_FF_adffs_a13_a,
	datab => U_ROM_asrom_aq_a13_a,
	cin => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a4_a_a61,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a5_a,
	cout => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a5_a_a64);

BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a186_I : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a186 = BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a5_a $ BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a184
-- BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a188 = CARRY(!BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a184 # !BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a5_a,
	cin => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a184,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a186,
	cout => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a188);

U_ROM_asrom_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111",
	address_width => 8,
	bit_number => 14,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a14_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a14_a_modesel,
	dataout => U_ROM_asrom_aq_a14_a);

BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a6_a = DFFE(ROM_DATA_FF_adffs_a14_a $ U_ROM_asrom_aq_a14_a $ !BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a5_a_a64, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a6_a_a67 = CARRY(ROM_DATA_FF_adffs_a14_a & (U_ROM_asrom_aq_a14_a # !BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a5_a_a64) # !ROM_DATA_FF_adffs_a14_a & U_ROM_asrom_aq_a14_a & 
-- !BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a5_a_a64)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROM_DATA_FF_adffs_a14_a,
	datab => U_ROM_asrom_aq_a14_a,
	cin => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a5_a_a64,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a6_a,
	cout => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a6_a_a67);

BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a190_I : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a190 = BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a6_a $ !BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a188
-- BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a192 = CARRY(BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a6_a & !BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a188)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a6_a,
	cin => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a188,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a190,
	cout => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a192);

U_ROM_asrom_asegment_a0_a_a15_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	mem1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111",
	address_width => 8,
	bit_number => 15,
	data_in_clear => "none",
	data_in_clock => "none",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "WDSPULSE.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_rom:U_ROM|altrom:srom|content",
	logical_ram_width => 16,
	operation_mode => "rom",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "none")
-- pragma translate_on
PORT MAP (
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	waddr => U_ROM_asrom_asegment_a0_a_a15_a_WADDR_bus,
	raddr => U_ROM_asrom_asegment_a0_a_a15_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_ROM_asrom_asegment_a0_a_a15_a_modesel,
	dataout => U_ROM_asrom_aq_a15_a);

DLY1_asram_asegment_a0_a_a15_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 10,
	bit_number => 15,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 1023,
	logical_ram_depth => 1024,
	logical_ram_name => "lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => U_ROM_asrom_aq_a15_a,
	clk0 => clk20_acombout,
	clk1 => clk20_acombout,
	we => VCC,
	re => VCC,
	waddr => DLY1_asram_asegment_a0_a_a15_a_WADDR_bus,
	raddr => DLY1_asram_asegment_a0_a_a15_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => DLY1_asram_asegment_a0_a_a15_a_modesel,
	dataout => DLY1_asram_aq_a15_a);

ROM_DATA_FF_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- ROM_DATA_FF_adffs_a15_a = DFFE(DLY1_asram_aq_a15_a & !Equal_a475, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DLY1_asram_aq_a15_a,
	datad => Equal_a475,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROM_DATA_FF_adffs_a15_a);

BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a7_a = DFFE(U_ROM_asrom_aq_a15_a $ BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a6_a_a67 $ ROM_DATA_FF_adffs_a15_a, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C33C",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_ROM_asrom_aq_a15_a,
	datad => ROM_DATA_FF_adffs_a15_a,
	cin => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a6_a_a67,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a7_a);

BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a194_I : apex20ke_lcell
-- Equation(s):
-- BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a194 = BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a192 $ BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a7_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => BIT_DATA_ADDER_aadder1_0_a1_a_aresult_node_asout_node_a7_a,
	cin => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a192,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a194);

pk2dly_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(10));

pk2dly_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(11));

pk2dly_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(12));

pk2dly_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(13));

pk2dly_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(14));

pk2dly_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(15));

BIT_DATA_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(0));

BIT_DATA_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(1));

BIT_DATA_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(2));

BIT_DATA_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(3));

BIT_DATA_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(4));

BIT_DATA_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(5));

BIT_DATA_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(6));

BIT_DATA_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a0_a_aresult_node_asout_node_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(7));

BIT_DATA_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a166,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(8));

BIT_DATA_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a170,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(9));

BIT_DATA_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a174,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(10));

BIT_DATA_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a178,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(11));

BIT_DATA_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a182,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(12));

BIT_DATA_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a186,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(13));

BIT_DATA_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a190,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(14));

BIT_DATA_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BIT_DATA_ADDER_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a194,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BIT_DATA(15));
END structure;


