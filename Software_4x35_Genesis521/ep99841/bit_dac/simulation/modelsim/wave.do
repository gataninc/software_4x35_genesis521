onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /tb/imr
add wave -noupdate -format Logic -radix hexadecimal /tb/clk20
add wave -noupdate -format Logic -radix hexadecimal /tb/bit_en
add wave -noupdate -format Literal -radix hexadecimal /tb/peak1
add wave -noupdate -format Literal -radix hexadecimal /tb/peak2
add wave -noupdate -format Literal -radix hexadecimal /tb/cps
add wave -noupdate -format Literal -radix hexadecimal /tb/pk2dly
add wave -noupdate -format Analog-Step -height 200 -offset -25000.0 -radix unsigned -scale 0.0050000000000003002 /tb/bit_data
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {15323 ns} 0} {{Cursor 2} {215634 ns} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
update
WaveRestoreZoom {34770 ns} {305278 ns}
