---------------------------------------------------------------------------------------------------

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity tb is
end tb;

architecture test_Bench of tb is
	signal IMR 	: std_logic := '1';
	signal CLK20	: std_logic := '0';
	signal Peak1	: std_logic_Vector( 11 downto 0 );
	signal Peak2	: std_logic_Vector( 11 downto 0 );
	signal cps	: std_logic_Vector( 15 downto 0 );
	signal pk2dly	: std_logic_Vector( 15 downto 0 );
	signal bit_data : std_logic_Vector( 15 downto 0 );
	
	
	component bit_dac 
		port( 
		     IMR            : in      std_logic;				 	-- Master Reset
     		clk20          : in      std_logic;                     	-- Master Clock
		     peak1     	: in      std_logic_vector( 11 downto 0 ); 	-- Peak 1 Value
     		peak2     	: in      std_logic_vector( 11 downto 0 ); 	-- Peak 2 Value
		     cps       	: in      std_logic_vector( 15 downto 0 );	-- CPS Value
     		pk2dly    	: in      std_logic_vector( 15 downto 0 );   -- Peak2 Delay Ver 523
			BIT_DATA		: buffer	std_logic_vector( 15 downto 0 ) );	-- BIT DAC Data
	end component bit_dac;
---------------------------------------------------------------------------------------------------

begin
	U : BIT_DAC
		port map(
		     IMR            => IMR,
	     	clk20          => CLK20,
		     peak1     	=> Peak1,
	     	peak2     	=> Peak2, 
		     cps       	=> CPS,
	     	pk2dly    	=> Pk2Dly,
			BIT_DATA		=> bit_data );

	IMR <= '0' after 350 ns;
	CLK20 <= not( CLK20 ) after 25 ns;

	main_proc : process
	begin
		Peak1	<= x"000";
		Peak2	<= x"000";
		cps		<= x"0000";
		pk2dly	<= x"0000";
		wait until(( imr'event ) and ( imr = '0' ));
		wait for 2 us;
		Peak1	<= x"800";
		Peak2	<= x"400";
		cps		<= x"0100";
		pk2dly	<= x"0040";
		wait ;
	end process;	
	
---------------------------------------------------------------------------------------------------
end test_bench;
---------------------------------------------------------------------------------------------------

