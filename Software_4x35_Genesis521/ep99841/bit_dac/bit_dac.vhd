---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	BITDAC.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 16, 1999
--   Board          : FAD FPGA
--   Schematic      : 4035.007.20700
--
--   FPGA Code P/N  : 4035.009.99880 - SOFTWARE, FA-D FPGA
--   FPGA ROM  P/N  : 9335.078.00727 - ALTERA EPC2LC20
--   FPGA      P/N  : 9335.078.00735 - FPF20K100AQC240-3
--
--   Description:	
--		Module to generate the BIT DAC outputs based on 4 parameters
--			CPS, PK2D, PK1 and PK2
--
--
--
--
--   History:       <date> - <Author>
--		12/05/05 - MCS
--			Ver 357D: Created BIT_Disable which is set when PEak1 AND peak2 are 0
--			which prevents the sequencer from running.
--		06/15/05 - MCS
--			Updated for Quartus II Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		01/19/01 - Ver 0.95
--			Full USB Read/Write Support
--			LAst Time Constant in NVRam Support
--		11/20/00 - Ver 0.70
--			Polarity of SW_PSLOPE was reversed 
--				OFF = Negative/ ON= Positive Slope
--		11/16/00 - Ver 0.69 
--			Initial Code that appears to be fully operational
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
library LPM;
     use LPM.LPM_COMPONENTS.ALL;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity bit_dac is 
	port( 
	     IMR            : in      std_logic;				 	-- Master Reset
     	clk20          : in      std_logic;                     	-- Master Clock
	     peak1     	: in      std_logic_vector( 11 downto 0 ); 	-- Peak 1 Value
     	peak2     	: in      std_logic_vector( 11 downto 0 ); 	-- Peak 2 Value
	     cps       	: in      std_logic_vector( 15 downto 0 );	-- CPS Value
     	pk2dly    	: in      std_logic_vector( 15 downto 0 );   -- Peak2 Delay Ver 523
		BIT_DATA		: buffer	std_logic_vector( 15 downto 0 ) );	-- BIT DAC Data
	end bit_dac;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of bit_dac is

     -- Registered Signals
	signal PK2_DISABLE		: std_Logic;
	signal Bit_Disable		: std_logic;
	
	signal CPS_CNTR		: std_Logic_Vector( 15 downto 0 );
	signal CPS_CNTR_TC		: std_logic;
	signal CPS_REG			: std_logic_Vector( 15 downto 0 );

	signal PK2DLY_REG		: std_logic_Vector(  9 downto 0 );
	
	signal RADR			: std_logic_Vector(  9 downto 0 );
	signal ROM_ADR			: std_Logic_Vector( 7 downto 0 );
	signal ROM_EN			: std_logic;
	signal ROM_EN_TC		: std_logic;
	signal ROM_DATA		: std_logic_Vector( 15 downto 0 );
	signal ROM_DATA_DEL		: std_logic_Vector( 15 downto 0 );
	signal ROM_DATA_OUT		: std_logic_Vector( 15 downto 0 );

	signal WADR			: std_logic_Vector(  9 downto 0 );

begin

	CPS_REG_FF : lpm_ff
		generic map(
			LPM_WIDTH				=> 16 )
		port map(
			aclr					=> imr,
			clock				=> CLk20,
			data					=> CPS,
			q					=> CPS_REG );

	PK2_DELAY_REG_FF : lpm_ff
		generic map(
			LPM_WIDTH				=> 10 )
		port map(
			aclr					=> imr,
			clock				=> CLk20,
			data					=> PK2DLY( 9 downto 0 ),
			q					=> PK2DLY_REG );

     -------------------------------------------------------------------------
	-- Look-UP ROM For Pulse Information ------------------------------------
	U_ROM : lpm_rom
		generic map(	
			LPM_WIDTH				=> 16,
			LPM_WIDTHAD			=> 8,
			LPM_ADDRESS_CONTROL 	=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_FILE				=> "WDSPULSE.MIF" )
		port map(
			inclock				=> clk20,
			outclock				=> clk20,
			address				=> ROM_ADR,
			q					=> ROM_DATA );
	-- Look-UP ROM For Pulse Information ------------------------------------
     -------------------------------------------------------------------------

     -------------------------------------------------------------------------
	-- Delay Memory Elements
	WADR_CNTR : LPM_COUNTER
		generic map(
			LPM_WIDTH				=> 10,
			LPM_DIRECTION			=> "UP" )
		port map(
			aclr					=> imr,
			clock				=> clk20,
			q					=> WADR );
	
	RADR <= WADR - PK2DLY_REG;
	
	DLY1 : lpm_ram_dp
		generic map(
			LPM_WIDTH				=> 16,	-- Data Width
			LPM_WIDTHAD			=> 10,
			LPM_INDATA			=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_RDADDRESS_CONTROL	=> "REGISTERED",
			LPM_WRADDRESS_CONTROL	=> "REGISTERED" )
		PORT map(
			Wren					=> '1',
			wrclock				=> clk20,
			rdclock 				=> clk20,
			rdaddress 			=> radr, 
			wraddress				=> wadr,
			data					=> ROM_DATA,
			q					=> ROM_DATA_DEL );
	
	ROM_DATA_FF : LPM_FF
		generic map(
			LPM_WIDTH				=> 16 )
		port map(
			aclr					=> imr,
			clock				=> clk20,
			sclr					=> PK2_DISABLE,
			data					=> ROM_DATA_DEL,
			q					=> ROM_DATA_OUT );
	-- Delay Memory Element
     -------------------------------------------------------------------------
	
     -------------------------------------------------------------------------
	-- DATA Output
	BIT_DATA_ADDER : LPM_ADD_SUB
		generic map(
			LPM_WIDTH		=> 16,
			LPM_DIRECTION			=> "ADD",
			LPM_REPRESENTATION		=> "SIGNED",
			LPM_PIPELINE			=> 1 )
		port map(
			aclr					=> imr,
			clock				=> clk20,
			cin					=> '0',
			dataa				=> ROM_DATA,
			datab				=> ROM_DATA_OUT,
			result				=> BIT_DATA );
			
     -------------------------------------------------------------------------
		
	Bit_Disable 	<= '1' when (( conv_integer( Peak1 ) = 0 ) and ( conv_integer( peak2 ) = 0 )) else '0';
	PK2_DISABLE	<= '1' when ( PEAK2 = 0 ) else '0';
	CPS_CNTR_TC	<= '1' when ( CPS_CNTR = CPS_REG ) else '0';	
	ROM_EN_TC		<= '1' when ( ROM_ADR = x"FF" ) else '0';
	-- BIT Counter Terminal Count based on the State and the Max Value
	-------------------------------------------------------------------------------------------------
     ----------------------------------------------------------------------------------------------
     UPD_PROC : process( clk20, IMR )
     begin
          if( IMR = '1' ) then
			CPS_CNTR  <= x"0000";
			ROM_EN	<= '0';
			ROM_ADR	<= x"00";
          elsif(( clk20'Event ) and ( clk20 = '1' )) then

			if(( Bit_Disable = '1' ) or ( CPS_CNTR_TC = '1' ))
				then CPS_CNTR <= x"0000";
				else CPS_CNTR <= CPS_CNTR + 1;
			end if;				

			if( CPS_CNTR_TC = '1' )
				then ROM_EN <= '1';
			elsif( ROM_EN_TC = '1' )
				then ROM_EN <= '0';
			end if;
			
			if( ROM_EN = '0' )
				then ROM_ADR <= x"00";
				else ROM_ADR <= ROM_ADR + 1;
			end if;
		end if;
     end process;
     ----------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
end behavioral;			-- bit_dac.VHD
---------------------------------------------------------------------------------------------------

