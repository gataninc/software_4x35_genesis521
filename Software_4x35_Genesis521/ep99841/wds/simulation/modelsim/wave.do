onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /tb/imr
add wave -noupdate -format Logic /tb/clk
add wave -noupdate -format Logic /tb/fir_mr
add wave -noupdate -format Logic /tb/disc_en
add wave -noupdate -format Analog-Step -height 200 -offset 100.0 -radix decimal /tb/din
add wave -noupdate -format Literal -radix hexadecimal /tb/tlevel
add wave -noupdate -format Literal -radix hexadecimal /tb/blevel
add wave -noupdate -format Logic /tb/blevel_upd
add wave -noupdate -format Logic /tb/level_cmp
add wave -noupdate -format Logic /tb/pslope
add wave -noupdate -format Logic /tb/level_out
add wave -noupdate -format Logic /tb/pk_done
add wave -noupdate -format Logic /tb/pbusy
add wave -noupdate -format Analog-Step -height 100 -radix decimal -scale 0.10000000000000001 /tb/dout
add wave -noupdate -format Analog-Step -height 100 -radix decimal -scale 0.10000000000000001 /tb/dout_max
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {295455 ps} 0}
WaveRestoreZoom {0 ps} {42464789 ps}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
