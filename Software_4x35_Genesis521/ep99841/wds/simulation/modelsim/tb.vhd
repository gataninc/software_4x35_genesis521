------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_wds
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module performs peak-height analysis for the Ketek AXAS SDD Detector
--		The digital gain has been boosted high enough for an ASAS output of 1V gaussian pulses
--		for Mn55 (6KeV)
--
--
--	History
--		06/15/05 - MCS
--			Updated for QuartusII Version 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is
	signal imr		: std_logic := '1';
	signal clk		: std_logic := '0';
	signal FIR_MR		: std_logic := '1';
	signal Disc_en		: std_logic;
	signal Din		: std_logic_vector( 15 downto 0 );
	signal Tlevel		: std_logic_vector( 15 downto 0 );
	signal BLEVEL		: std_logic_vector(  7 downto 0 );
	signal BLEVEL_UPD	: std_logic;
	signal Level_Cmp	: std_logic;
	signal Pslope		: std_logic;
	signal Level_out	: std_logic;
	signal PK_DONE		: std_logic;
	signal PBusy		: std_Logic;
	signal Dout		: std_logic_vector( 15 downto 0 );
	signal Dout_Max	: std_Logic_Vector( 15 downto 0 );

	signal iDout		: std_logic_vector( 15 downto 0 );
	signal iDout_Max	: std_Logic_Vector( 15 downto 0 );
------------------------------------------------------------------------------------
	component tb_rom is
		port(
			clk	: in 	std_Logic;
			data	: out	std_logic_vector( 15 downto 0 ) );
	end component tb_rom;
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
	component fir_wds is 
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			FIR_MR		: in		std_logic;
			Disc_en		: in		std_logic;
			Din			: in		std_logic_vector( 15 downto 0 );
			Tlevel		: in		std_logic_vector( 15 downto 0 );
			BLEVEL		: out	std_logic_vector(  7 downto 0 );
			BLEVEL_UPD	: out	std_logic;
			Level_Cmp		: out	std_logic;
			Pslope		: out	std_logic;
			Level_out		: out	std_logic;
			PK_DONE		: out	std_logic;
			PBusy		: out	std_Logic;
			Dout			: out	std_logic_vector( 15 downto 0 );
			Dout_Max		: out	std_Logic_Vector( 15 downto 0 ) );
	end component fir_wds;
------------------------------------------------------------------------------------
begin
	imr <= '0' after 555 ns;
	clk	<= not( clk ) after 25 ns;
	fir_mr	<= '0' after 1 us;
	disc_en	<= '1';
	tlevel	<= x"0010";

	UR : tb_rom
		port map(
			clk	=> clk,
			data	=> din );
	
	U : fir_wds
		port map(
			imr			=> imr,
			clk			=> clk,
			FIR_MR		=> fir_mr,
			Disc_en		=> disc_en,
			Din			=> Din,
			Tlevel		=> tlevel,
			BLEVEL		=> blevel,
			BLEVEL_UPD	=> blevel_upd,
			Level_Cmp		=> level_Cmp,
			Pslope		=> Pslope,
			Level_out		=> Level_Out,
			PK_DONE		=> pk_done,
			PBusy		=> pbusy,
			Dout			=> iDout,
			Dout_Max		=> iDout_Max );
	
	clk_proc : process( clk )
	begin
		if(( clk'event ) and ( clk = '1' )) then
			dout <= idout;
			dout_max <= idout_max;
		end if;
	end process;
------------------------------------------------------------------------------------
     -------------------------------------------------------------------------------
	control_proc : process
	begin
		wait for 100 us;
		assert false
			report "End of Simulation"
			severity failure;
	end process;
		
------------------------------------------------------------------------------------
end test_Bench;               -- 
------------------------------------------------------------------------------------
			
	
