-- Copyright (C) 1991-2005 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 176 10/26/2005 Service Pack 0.15 SJ Full Version"

-- DATE "12/15/2005 10:24:22"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_wds IS
    PORT (
	imr : IN std_logic;
	clk : IN std_logic;
	Disc_en : IN std_logic;
	Din : IN std_logic_vector(15 DOWNTO 0);
	Tlevel : IN std_logic_vector(15 DOWNTO 0);
	BLEVEL : OUT std_logic_vector(7 DOWNTO 0);
	BLEVEL_UPD : OUT std_logic;
	Level_Cmp : OUT std_logic;
	Pslope : OUT std_logic;
	Level_out : OUT std_logic;
	PK_DONE : OUT std_logic;
	PBusy : OUT std_logic;
	Dout : OUT std_logic_vector(15 DOWNTO 0);
	Dout_Max : OUT std_logic_vector(15 DOWNTO 0)
	);
END fir_wds;

ARCHITECTURE structure OF fir_wds IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_Disc_en : std_logic;
SIGNAL ww_Din : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Tlevel : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_BLEVEL : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_BLEVEL_UPD : std_logic;
SIGNAL ww_Level_Cmp : std_logic;
SIGNAL ww_Pslope : std_logic;
SIGNAL ww_Level_out : std_logic;
SIGNAL ww_PK_DONE : std_logic;
SIGNAL ww_PBusy : std_logic;
SIGNAL ww_Dout : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Dout_Max : std_logic_vector(15 DOWNTO 0);
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a14_a_a1044 : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a13_a_a1045 : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a14_a_a1020 : std_logic;
SIGNAL rtl_a72 : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a12_a_a1046 : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a14_a_a1020 : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a13_a_a1021 : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a11_a_a1047 : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a13_a_a1021 : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a12_a_a1022 : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a10_a_a1048 : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a12_a_a1022 : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a11_a_a1023 : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a9_a_a1049 : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a11_a_a1023 : std_logic;
SIGNAL dout_del_ff_adffs_a12_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a10_a_a1024 : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a8_a_a1050 : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a10_a_a1024 : std_logic;
SIGNAL dout_del_ff_adffs_a11_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a9_a_a1025 : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a7_a_a1051 : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a9_a_a1025 : std_logic;
SIGNAL dout_del_ff_adffs_a10_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a8_a_a1026 : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a6_a_a1052 : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a8_a_a1026 : std_logic;
SIGNAL dout_del_ff_adffs_a9_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a7_a_a1027 : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a5_a_a1053 : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a7_a_a1027 : std_logic;
SIGNAL dout_del_ff_adffs_a8_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a6_a_a1028 : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a4_a_a1054 : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a6_a_a1028 : std_logic;
SIGNAL dout_del_ff_adffs_a7_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a5_a_a1029 : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a3_a_a1055 : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a5_a_a1029 : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a4_a_a1030 : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a2_a_a1056 : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a4_a_a1030 : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a3_a_a1031 : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a1_a_a1057 : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a3_a_a1031 : std_logic;
SIGNAL dout_del_ff_adffs_a4_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a2_a_a1032 : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a0_a_a1058 : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a2_a_a1032 : std_logic;
SIGNAL dout_del_ff_adffs_a3_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a1_a_a1033 : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a1_a_a1033 : std_logic;
SIGNAL dout_del_ff_adffs_a2_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a0_a_a1034 : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a0_a_a1034 : std_logic;
SIGNAL dout_del_ff_adffs_a1_a : std_logic;
SIGNAL Din_a0_a_acombout : std_logic;
SIGNAL clk_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL Tlevel_a15_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a15_a : std_logic;
SIGNAL Din_a15_a_acombout : std_logic;
SIGNAL Tlevel_a14_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a14_a : std_logic;
SIGNAL Tlevel_a13_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a13_a : std_logic;
SIGNAL Tlevel_a12_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a12_a : std_logic;
SIGNAL Tlevel_a11_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a11_a : std_logic;
SIGNAL Tlevel_a10_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a10_a : std_logic;
SIGNAL Tlevel_a9_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a9_a : std_logic;
SIGNAL Tlevel_a8_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a8_a : std_logic;
SIGNAL Tlevel_a7_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a7_a : std_logic;
SIGNAL Tlevel_a6_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a6_a : std_logic;
SIGNAL Tlevel_a5_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a5_a : std_logic;
SIGNAL Tlevel_a4_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a4_a : std_logic;
SIGNAL Tlevel_a3_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a3_a : std_logic;
SIGNAL Tlevel_a2_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a2_a : std_logic;
SIGNAL Tlevel_a1_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a1_a : std_logic;
SIGNAL Tlevel_a0_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a0_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL level_cmp_compare_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL PBusy_areg0 : std_logic;
SIGNAL BLEVEL_FF_adffs_a0_a : std_logic;
SIGNAL Din_a1_a_acombout : std_logic;
SIGNAL BLEVEL_FF_adffs_a1_a : std_logic;
SIGNAL Din_a2_a_acombout : std_logic;
SIGNAL BLEVEL_FF_adffs_a2_a : std_logic;
SIGNAL Din_a3_a_acombout : std_logic;
SIGNAL BLEVEL_FF_adffs_a3_a : std_logic;
SIGNAL Din_a4_a_acombout : std_logic;
SIGNAL BLEVEL_FF_adffs_a4_a : std_logic;
SIGNAL Din_a5_a_acombout : std_logic;
SIGNAL BLEVEL_FF_adffs_a5_a : std_logic;
SIGNAL Din_a6_a_acombout : std_logic;
SIGNAL BLEVEL_FF_adffs_a6_a : std_logic;
SIGNAL Din_a7_a_acombout : std_logic;
SIGNAL BLEVEL_FF_adffs_a7_a : std_logic;
SIGNAL add_a129 : std_logic;
SIGNAL blevel_cnt_a0_a : std_logic;
SIGNAL add_a131 : std_logic;
SIGNAL add_a133 : std_logic;
SIGNAL blevel_cnt_a1_a : std_logic;
SIGNAL add_a135 : std_logic;
SIGNAL add_a137 : std_logic;
SIGNAL blevel_cnt_a2_a : std_logic;
SIGNAL add_a139 : std_logic;
SIGNAL add_a141 : std_logic;
SIGNAL blevel_cnt_a3_a : std_logic;
SIGNAL add_a143 : std_logic;
SIGNAL add_a121 : std_logic;
SIGNAL blevel_cnt_a4_a : std_logic;
SIGNAL add_a123 : std_logic;
SIGNAL add_a127 : std_logic;
SIGNAL add_a113 : std_logic;
SIGNAL blevel_cnt_a6_a : std_logic;
SIGNAL add_a115 : std_logic;
SIGNAL add_a117 : std_logic;
SIGNAL blevel_cnt_a7_a : std_logic;
SIGNAL add_a125 : std_logic;
SIGNAL blevel_cnt_a5_a : std_logic;
SIGNAL rtl_a76 : std_logic;
SIGNAL rtl_a74 : std_logic;
SIGNAL BLEVEL_UPD_areg0 : std_logic;
SIGNAL dout_del_ff_adffs_a15_a : std_logic;
SIGNAL Din_a14_a_acombout : std_logic;
SIGNAL Din_a13_a_acombout : std_logic;
SIGNAL Din_a12_a_acombout : std_logic;
SIGNAL Din_a11_a_acombout : std_logic;
SIGNAL Din_a10_a_acombout : std_logic;
SIGNAL Din_a9_a_acombout : std_logic;
SIGNAL Din_a8_a_acombout : std_logic;
SIGNAL dout_del_ff_adffs_a6_a : std_logic;
SIGNAL dout_del_ff_adffs_a5_a : std_logic;
SIGNAL dout_del_ff_adffs_a0_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL Pslope_Comparee_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL PSlope_Vec_a0_a : std_logic;
SIGNAL PSlope_Vec_a1_a : std_logic;
SIGNAL PSlope_Vec_a2_a : std_logic;
SIGNAL Pslope_areg0 : std_logic;
SIGNAL PSlope_Del : std_logic;
SIGNAL Disc_en_acombout : std_logic;
SIGNAL Level_out_areg0 : std_logic;
SIGNAL Dout_Max_FF_adffs_a12_a : std_logic;
SIGNAL Dout_Max_FF_adffs_a11_a : std_logic;
SIGNAL Dout_Max_FF_adffs_a7_a : std_logic;
SIGNAL Dout_Max_FF_adffs_a6_a : std_logic;
SIGNAL Dout_Max_FF_adffs_a5_a : std_logic;
SIGNAL Dout_Max_FF_adffs_a4_a : std_logic;
SIGNAL Dout_Max_FF_adffs_a3_a : std_logic;
SIGNAL Dout_Max_FF_adffs_a1_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL dout_max_Compare_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL Dout_Max_En_a30 : std_logic;
SIGNAL Dout_Max_FF_adffs_a0_a : std_logic;
SIGNAL Dout_Max_FF_adffs_a2_a : std_logic;
SIGNAL Dout_Max_FF_adffs_a8_a : std_logic;
SIGNAL Dout_Max_FF_adffs_a9_a : std_logic;
SIGNAL Dout_Max_FF_adffs_a10_a : std_logic;
SIGNAL Dout_Max_FF_adffs_a15_a : std_logic;
SIGNAL ALT_INV_PBusy_areg0 : std_logic;

BEGIN

ww_imr <= imr;
ww_clk <= clk;
ww_Disc_en <= Disc_en;
ww_Din <= Din;
ww_Tlevel <= Tlevel;
BLEVEL <= ww_BLEVEL;
BLEVEL_UPD <= ww_BLEVEL_UPD;
Level_Cmp <= ww_Level_Cmp;
Pslope <= ww_Pslope;
Level_out <= ww_Level_out;
PK_DONE <= ww_PK_DONE;
PBusy <= ww_PBusy;
Dout <= ww_Dout;
Dout_Max <= ww_Dout_Max;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
ALT_INV_PBusy_areg0 <= NOT PBusy_areg0;

dout_del_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a12_a = DFFE(Din_a14_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a14_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a12_a);

dout_del_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a11_a = DFFE(Din_a13_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a13_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a11_a);

dout_del_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a10_a = DFFE(Din_a12_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a12_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a10_a);

dout_del_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a9_a = DFFE(Din_a11_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a11_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a9_a);

dout_del_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a8_a = DFFE(Din_a10_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a10_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a8_a);

dout_del_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a7_a = DFFE(Din_a9_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a9_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a7_a);

dout_del_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a4_a = DFFE(Din_a6_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a6_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a4_a);

dout_del_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a3_a = DFFE(Din_a5_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a5_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a3_a);

dout_del_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a2_a = DFFE(Din_a4_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a4_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a2_a);

dout_del_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a1_a = DFFE(Din_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a1_a);

Din_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(0),
	combout => Din_a0_a_acombout);

clk_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => clk_acombout);

imr_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

Tlevel_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(15),
	combout => Tlevel_a15_a_acombout);

tlevel_reg_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a15_a = DFFE(Tlevel_a15_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a15_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a15_a);

Din_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(15),
	combout => Din_a15_a_acombout);

Tlevel_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(14),
	combout => Tlevel_a14_a_acombout);

tlevel_reg_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a14_a = DFFE(Tlevel_a14_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a14_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a14_a);

Tlevel_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(13),
	combout => Tlevel_a13_a_acombout);

tlevel_reg_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a13_a = DFFE(Tlevel_a13_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a13_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a13_a);

Tlevel_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(12),
	combout => Tlevel_a12_a_acombout);

tlevel_reg_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a12_a = DFFE(Tlevel_a12_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a12_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a12_a);

Tlevel_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(11),
	combout => Tlevel_a11_a_acombout);

tlevel_reg_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a11_a = DFFE(Tlevel_a11_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a11_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a11_a);

Tlevel_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(10),
	combout => Tlevel_a10_a_acombout);

tlevel_reg_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a10_a = DFFE(Tlevel_a10_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a10_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a10_a);

Tlevel_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(9),
	combout => Tlevel_a9_a_acombout);

tlevel_reg_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a9_a = DFFE(Tlevel_a9_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a9_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a9_a);

Tlevel_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(8),
	combout => Tlevel_a8_a_acombout);

tlevel_reg_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a8_a = DFFE(Tlevel_a8_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a8_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a8_a);

Tlevel_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(7),
	combout => Tlevel_a7_a_acombout);

tlevel_reg_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a7_a = DFFE(Tlevel_a7_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a7_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a7_a);

Tlevel_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(6),
	combout => Tlevel_a6_a_acombout);

tlevel_reg_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a6_a = DFFE(Tlevel_a6_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a6_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a6_a);

Tlevel_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(5),
	combout => Tlevel_a5_a_acombout);

tlevel_reg_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a5_a = DFFE(Tlevel_a5_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a5_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a5_a);

Tlevel_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(4),
	combout => Tlevel_a4_a_acombout);

tlevel_reg_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a4_a = DFFE(Tlevel_a4_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a4_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a4_a);

Tlevel_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(3),
	combout => Tlevel_a3_a_acombout);

tlevel_reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a3_a = DFFE(Tlevel_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a3_a);

Tlevel_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(2),
	combout => Tlevel_a2_a_acombout);

tlevel_reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a2_a = DFFE(Tlevel_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a2_a);

Tlevel_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(1),
	combout => Tlevel_a1_a_acombout);

tlevel_reg_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a1_a = DFFE(Tlevel_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a1_a);

Tlevel_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(0),
	combout => Tlevel_a0_a_acombout);

tlevel_reg_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a0_a = DFFE(Tlevel_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Tlevel_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a0_a);

level_cmp_compare_acomparator_acmp_end_alcarry_a0_a_a1058_I : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_alcarry_a0_a = CARRY(Din_a2_a_acombout & !tlevel_reg_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "0022",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a2_a_acombout,
	datab => tlevel_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_alcarry_a0_a_a1058,
	cout => level_cmp_compare_acomparator_acmp_end_alcarry_a0_a);

level_cmp_compare_acomparator_acmp_end_alcarry_a1_a_a1057_I : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_alcarry_a1_a = CARRY(Din_a3_a_acombout & tlevel_reg_ff_adffs_a1_a & !level_cmp_compare_acomparator_acmp_end_alcarry_a0_a # !Din_a3_a_acombout & (tlevel_reg_ff_adffs_a1_a # 
-- !level_cmp_compare_acomparator_acmp_end_alcarry_a0_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a3_a_acombout,
	datab => tlevel_reg_ff_adffs_a1_a,
	cin => level_cmp_compare_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_alcarry_a1_a_a1057,
	cout => level_cmp_compare_acomparator_acmp_end_alcarry_a1_a);

level_cmp_compare_acomparator_acmp_end_alcarry_a2_a_a1056_I : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_alcarry_a2_a = CARRY(Din_a4_a_acombout & (!level_cmp_compare_acomparator_acmp_end_alcarry_a1_a # !tlevel_reg_ff_adffs_a2_a) # !Din_a4_a_acombout & !tlevel_reg_ff_adffs_a2_a & 
-- !level_cmp_compare_acomparator_acmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a4_a_acombout,
	datab => tlevel_reg_ff_adffs_a2_a,
	cin => level_cmp_compare_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_alcarry_a2_a_a1056,
	cout => level_cmp_compare_acomparator_acmp_end_alcarry_a2_a);

level_cmp_compare_acomparator_acmp_end_alcarry_a3_a_a1055_I : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_alcarry_a3_a = CARRY(Din_a5_a_acombout & tlevel_reg_ff_adffs_a3_a & !level_cmp_compare_acomparator_acmp_end_alcarry_a2_a # !Din_a5_a_acombout & (tlevel_reg_ff_adffs_a3_a # 
-- !level_cmp_compare_acomparator_acmp_end_alcarry_a2_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a5_a_acombout,
	datab => tlevel_reg_ff_adffs_a3_a,
	cin => level_cmp_compare_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_alcarry_a3_a_a1055,
	cout => level_cmp_compare_acomparator_acmp_end_alcarry_a3_a);

level_cmp_compare_acomparator_acmp_end_alcarry_a4_a_a1054_I : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_alcarry_a4_a = CARRY(Din_a6_a_acombout & (!level_cmp_compare_acomparator_acmp_end_alcarry_a3_a # !tlevel_reg_ff_adffs_a4_a) # !Din_a6_a_acombout & !tlevel_reg_ff_adffs_a4_a & 
-- !level_cmp_compare_acomparator_acmp_end_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a6_a_acombout,
	datab => tlevel_reg_ff_adffs_a4_a,
	cin => level_cmp_compare_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_alcarry_a4_a_a1054,
	cout => level_cmp_compare_acomparator_acmp_end_alcarry_a4_a);

level_cmp_compare_acomparator_acmp_end_alcarry_a5_a_a1053_I : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_alcarry_a5_a = CARRY(Din_a7_a_acombout & tlevel_reg_ff_adffs_a5_a & !level_cmp_compare_acomparator_acmp_end_alcarry_a4_a # !Din_a7_a_acombout & (tlevel_reg_ff_adffs_a5_a # 
-- !level_cmp_compare_acomparator_acmp_end_alcarry_a4_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a7_a_acombout,
	datab => tlevel_reg_ff_adffs_a5_a,
	cin => level_cmp_compare_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_alcarry_a5_a_a1053,
	cout => level_cmp_compare_acomparator_acmp_end_alcarry_a5_a);

level_cmp_compare_acomparator_acmp_end_alcarry_a6_a_a1052_I : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_alcarry_a6_a = CARRY(Din_a8_a_acombout & (!level_cmp_compare_acomparator_acmp_end_alcarry_a5_a # !tlevel_reg_ff_adffs_a6_a) # !Din_a8_a_acombout & !tlevel_reg_ff_adffs_a6_a & 
-- !level_cmp_compare_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a8_a_acombout,
	datab => tlevel_reg_ff_adffs_a6_a,
	cin => level_cmp_compare_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_alcarry_a6_a_a1052,
	cout => level_cmp_compare_acomparator_acmp_end_alcarry_a6_a);

level_cmp_compare_acomparator_acmp_end_alcarry_a7_a_a1051_I : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_alcarry_a7_a = CARRY(Din_a9_a_acombout & tlevel_reg_ff_adffs_a7_a & !level_cmp_compare_acomparator_acmp_end_alcarry_a6_a # !Din_a9_a_acombout & (tlevel_reg_ff_adffs_a7_a # 
-- !level_cmp_compare_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a9_a_acombout,
	datab => tlevel_reg_ff_adffs_a7_a,
	cin => level_cmp_compare_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_alcarry_a7_a_a1051,
	cout => level_cmp_compare_acomparator_acmp_end_alcarry_a7_a);

level_cmp_compare_acomparator_acmp_end_alcarry_a8_a_a1050_I : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_alcarry_a8_a = CARRY(Din_a10_a_acombout & (!level_cmp_compare_acomparator_acmp_end_alcarry_a7_a # !tlevel_reg_ff_adffs_a8_a) # !Din_a10_a_acombout & !tlevel_reg_ff_adffs_a8_a & 
-- !level_cmp_compare_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a10_a_acombout,
	datab => tlevel_reg_ff_adffs_a8_a,
	cin => level_cmp_compare_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_alcarry_a8_a_a1050,
	cout => level_cmp_compare_acomparator_acmp_end_alcarry_a8_a);

level_cmp_compare_acomparator_acmp_end_alcarry_a9_a_a1049_I : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_alcarry_a9_a = CARRY(Din_a11_a_acombout & tlevel_reg_ff_adffs_a9_a & !level_cmp_compare_acomparator_acmp_end_alcarry_a8_a # !Din_a11_a_acombout & (tlevel_reg_ff_adffs_a9_a # 
-- !level_cmp_compare_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a11_a_acombout,
	datab => tlevel_reg_ff_adffs_a9_a,
	cin => level_cmp_compare_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_alcarry_a9_a_a1049,
	cout => level_cmp_compare_acomparator_acmp_end_alcarry_a9_a);

level_cmp_compare_acomparator_acmp_end_alcarry_a10_a_a1048_I : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_alcarry_a10_a = CARRY(Din_a12_a_acombout & (!level_cmp_compare_acomparator_acmp_end_alcarry_a9_a # !tlevel_reg_ff_adffs_a10_a) # !Din_a12_a_acombout & !tlevel_reg_ff_adffs_a10_a & 
-- !level_cmp_compare_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a12_a_acombout,
	datab => tlevel_reg_ff_adffs_a10_a,
	cin => level_cmp_compare_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_alcarry_a10_a_a1048,
	cout => level_cmp_compare_acomparator_acmp_end_alcarry_a10_a);

level_cmp_compare_acomparator_acmp_end_alcarry_a11_a_a1047_I : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_alcarry_a11_a = CARRY(Din_a13_a_acombout & tlevel_reg_ff_adffs_a11_a & !level_cmp_compare_acomparator_acmp_end_alcarry_a10_a # !Din_a13_a_acombout & (tlevel_reg_ff_adffs_a11_a # 
-- !level_cmp_compare_acomparator_acmp_end_alcarry_a10_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a13_a_acombout,
	datab => tlevel_reg_ff_adffs_a11_a,
	cin => level_cmp_compare_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_alcarry_a11_a_a1047,
	cout => level_cmp_compare_acomparator_acmp_end_alcarry_a11_a);

level_cmp_compare_acomparator_acmp_end_alcarry_a12_a_a1046_I : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_alcarry_a12_a = CARRY(Din_a14_a_acombout & (!level_cmp_compare_acomparator_acmp_end_alcarry_a11_a # !tlevel_reg_ff_adffs_a12_a) # !Din_a14_a_acombout & !tlevel_reg_ff_adffs_a12_a & 
-- !level_cmp_compare_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a14_a_acombout,
	datab => tlevel_reg_ff_adffs_a12_a,
	cin => level_cmp_compare_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_alcarry_a12_a_a1046,
	cout => level_cmp_compare_acomparator_acmp_end_alcarry_a12_a);

level_cmp_compare_acomparator_acmp_end_alcarry_a13_a_a1045_I : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_alcarry_a13_a = CARRY(Din_a15_a_acombout & tlevel_reg_ff_adffs_a13_a & !level_cmp_compare_acomparator_acmp_end_alcarry_a12_a # !Din_a15_a_acombout & (tlevel_reg_ff_adffs_a13_a # 
-- !level_cmp_compare_acomparator_acmp_end_alcarry_a12_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a15_a_acombout,
	datab => tlevel_reg_ff_adffs_a13_a,
	cin => level_cmp_compare_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_alcarry_a13_a_a1045,
	cout => level_cmp_compare_acomparator_acmp_end_alcarry_a13_a);

level_cmp_compare_acomparator_acmp_end_alcarry_a14_a_a1044_I : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_alcarry_a14_a = CARRY(Din_a15_a_acombout & (!level_cmp_compare_acomparator_acmp_end_alcarry_a13_a # !tlevel_reg_ff_adffs_a14_a) # !Din_a15_a_acombout & !tlevel_reg_ff_adffs_a14_a & 
-- !level_cmp_compare_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a15_a_acombout,
	datab => tlevel_reg_ff_adffs_a14_a,
	cin => level_cmp_compare_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_alcarry_a14_a_a1044,
	cout => level_cmp_compare_acomparator_acmp_end_alcarry_a14_a);

level_cmp_compare_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- level_cmp_compare_acomparator_acmp_end_aagb_out = tlevel_reg_ff_adffs_a15_a & (level_cmp_compare_acomparator_acmp_end_alcarry_a14_a # !Din_a15_a_acombout) # !tlevel_reg_ff_adffs_a15_a & level_cmp_compare_acomparator_acmp_end_alcarry_a14_a & 
-- !Din_a15_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C0FC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => tlevel_reg_ff_adffs_a15_a,
	datad => Din_a15_a_acombout,
	cin => level_cmp_compare_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => level_cmp_compare_acomparator_acmp_end_aagb_out);

PBusy_areg0_I : apex20ke_lcell
-- Equation(s):
-- PBusy_areg0 = DFFE(level_cmp_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => level_cmp_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PBusy_areg0);

BLEVEL_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- BLEVEL_FF_adffs_a0_a = DFFE(Din_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLEVEL_FF_adffs_a0_a);

Din_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(1),
	combout => Din_a1_a_acombout);

BLEVEL_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- BLEVEL_FF_adffs_a1_a = DFFE(Din_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLEVEL_FF_adffs_a1_a);

Din_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(2),
	combout => Din_a2_a_acombout);

BLEVEL_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- BLEVEL_FF_adffs_a2_a = DFFE(Din_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLEVEL_FF_adffs_a2_a);

Din_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(3),
	combout => Din_a3_a_acombout);

BLEVEL_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- BLEVEL_FF_adffs_a3_a = DFFE(Din_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLEVEL_FF_adffs_a3_a);

Din_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(4),
	combout => Din_a4_a_acombout);

BLEVEL_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- BLEVEL_FF_adffs_a4_a = DFFE(Din_a4_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a4_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLEVEL_FF_adffs_a4_a);

Din_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(5),
	combout => Din_a5_a_acombout);

BLEVEL_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- BLEVEL_FF_adffs_a5_a = DFFE(Din_a5_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a5_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLEVEL_FF_adffs_a5_a);

Din_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(6),
	combout => Din_a6_a_acombout);

BLEVEL_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- BLEVEL_FF_adffs_a6_a = DFFE(Din_a6_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a6_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLEVEL_FF_adffs_a6_a);

Din_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(7),
	combout => Din_a7_a_acombout);

BLEVEL_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- BLEVEL_FF_adffs_a7_a = DFFE(Din_a7_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a7_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLEVEL_FF_adffs_a7_a);

add_a129_I : apex20ke_lcell
-- Equation(s):
-- add_a129 = !blevel_cnt_a0_a
-- add_a131 = CARRY(blevel_cnt_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "33CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => blevel_cnt_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a129,
	cout => add_a131);

blevel_cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- blevel_cnt_a0_a = DFFE(add_a129, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a129,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => blevel_cnt_a0_a);

add_a133_I : apex20ke_lcell
-- Equation(s):
-- add_a133 = blevel_cnt_a1_a $ add_a131
-- add_a135 = CARRY(!add_a131 # !blevel_cnt_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => blevel_cnt_a1_a,
	cin => add_a131,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a133,
	cout => add_a135);

blevel_cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- blevel_cnt_a1_a = DFFE(add_a133, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a133,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => blevel_cnt_a1_a);

add_a137_I : apex20ke_lcell
-- Equation(s):
-- add_a137 = blevel_cnt_a2_a $ !add_a135
-- add_a139 = CARRY(blevel_cnt_a2_a & !add_a135)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => blevel_cnt_a2_a,
	cin => add_a135,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a137,
	cout => add_a139);

blevel_cnt_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- blevel_cnt_a2_a = DFFE(add_a137, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => add_a137,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => blevel_cnt_a2_a);

add_a141_I : apex20ke_lcell
-- Equation(s):
-- add_a141 = blevel_cnt_a3_a $ add_a139
-- add_a143 = CARRY(!add_a139 # !blevel_cnt_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => blevel_cnt_a3_a,
	cin => add_a139,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a141,
	cout => add_a143);

blevel_cnt_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- blevel_cnt_a3_a = DFFE(add_a141 & !rtl_a74, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => add_a141,
	datad => rtl_a74,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => blevel_cnt_a3_a);

add_a121_I : apex20ke_lcell
-- Equation(s):
-- add_a121 = blevel_cnt_a4_a $ !add_a143
-- add_a123 = CARRY(blevel_cnt_a4_a & !add_a143)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => blevel_cnt_a4_a,
	cin => add_a143,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a121,
	cout => add_a123);

blevel_cnt_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- blevel_cnt_a4_a = DFFE(add_a121, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a121,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => blevel_cnt_a4_a);

add_a125_I : apex20ke_lcell
-- Equation(s):
-- add_a125 = blevel_cnt_a5_a $ (add_a123)
-- add_a127 = CARRY(!add_a123 # !blevel_cnt_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => blevel_cnt_a5_a,
	cin => add_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a125,
	cout => add_a127);

add_a113_I : apex20ke_lcell
-- Equation(s):
-- add_a113 = blevel_cnt_a6_a $ (!add_a127)
-- add_a115 = CARRY(blevel_cnt_a6_a & (!add_a127))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => blevel_cnt_a6_a,
	cin => add_a127,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a113,
	cout => add_a115);

blevel_cnt_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- blevel_cnt_a6_a = DFFE(add_a113 & !rtl_a74, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => add_a113,
	datad => rtl_a74,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => blevel_cnt_a6_a);

add_a117_I : apex20ke_lcell
-- Equation(s):
-- add_a117 = add_a115 $ blevel_cnt_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => blevel_cnt_a7_a,
	cin => add_a115,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a117);

blevel_cnt_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- blevel_cnt_a7_a = DFFE(add_a117 & !rtl_a74, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => add_a117,
	datad => rtl_a74,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => blevel_cnt_a7_a);

blevel_cnt_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- blevel_cnt_a5_a = DFFE(add_a125, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , !PBusy_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => add_a125,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => ALT_INV_PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => blevel_cnt_a5_a);

rtl_a72_I : apex20ke_lcell
-- Equation(s):
-- rtl_a76 = !blevel_cnt_a4_a & blevel_cnt_a6_a & blevel_cnt_a7_a & !blevel_cnt_a5_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0040",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => blevel_cnt_a4_a,
	datab => blevel_cnt_a6_a,
	datac => blevel_cnt_a7_a,
	datad => blevel_cnt_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a72,
	cascout => rtl_a76);

rtl_a74_I : apex20ke_lcell
-- Equation(s):
-- rtl_a74 = (blevel_cnt_a2_a & blevel_cnt_a1_a & blevel_cnt_a0_a & !blevel_cnt_a3_a) & CASCADE(rtl_a76)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => blevel_cnt_a2_a,
	datab => blevel_cnt_a1_a,
	datac => blevel_cnt_a0_a,
	datad => blevel_cnt_a3_a,
	cascin => rtl_a76,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a74);

BLEVEL_UPD_areg0_I : apex20ke_lcell
-- Equation(s):
-- BLEVEL_UPD_areg0 = DFFE(!PBusy_areg0 & (rtl_a74), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => PBusy_areg0,
	datad => rtl_a74,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLEVEL_UPD_areg0);

dout_del_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a15_a = DFFE(Din_a15_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Din_a15_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a15_a);

Din_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(14),
	combout => Din_a14_a_acombout);

Din_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(13),
	combout => Din_a13_a_acombout);

Din_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(12),
	combout => Din_a12_a_acombout);

Din_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(11),
	combout => Din_a11_a_acombout);

Din_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(10),
	combout => Din_a10_a_acombout);

Din_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(9),
	combout => Din_a9_a_acombout);

Din_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(8),
	combout => Din_a8_a_acombout);

dout_del_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a6_a = DFFE(Din_a8_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Din_a8_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a6_a);

dout_del_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a5_a = DFFE(Din_a7_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Din_a7_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a5_a);

dout_del_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- dout_del_ff_adffs_a0_a = DFFE(Din_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dout_del_ff_adffs_a0_a);

Pslope_Comparee_acomparator_acmp_end_alcarry_a0_a_a1034_I : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_alcarry_a0_a = CARRY(Din_a2_a_acombout & !dout_del_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "0022",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a2_a_acombout,
	datab => dout_del_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_alcarry_a0_a_a1034,
	cout => Pslope_Comparee_acomparator_acmp_end_alcarry_a0_a);

Pslope_Comparee_acomparator_acmp_end_alcarry_a1_a_a1033_I : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_alcarry_a1_a = CARRY(dout_del_ff_adffs_a1_a & (!Pslope_Comparee_acomparator_acmp_end_alcarry_a0_a # !Din_a3_a_acombout) # !dout_del_ff_adffs_a1_a & !Din_a3_a_acombout & 
-- !Pslope_Comparee_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a1_a,
	datab => Din_a3_a_acombout,
	cin => Pslope_Comparee_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_alcarry_a1_a_a1033,
	cout => Pslope_Comparee_acomparator_acmp_end_alcarry_a1_a);

Pslope_Comparee_acomparator_acmp_end_alcarry_a2_a_a1032_I : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_alcarry_a2_a = CARRY(dout_del_ff_adffs_a2_a & Din_a4_a_acombout & !Pslope_Comparee_acomparator_acmp_end_alcarry_a1_a # !dout_del_ff_adffs_a2_a & (Din_a4_a_acombout # !Pslope_Comparee_acomparator_acmp_end_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a2_a,
	datab => Din_a4_a_acombout,
	cin => Pslope_Comparee_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_alcarry_a2_a_a1032,
	cout => Pslope_Comparee_acomparator_acmp_end_alcarry_a2_a);

Pslope_Comparee_acomparator_acmp_end_alcarry_a3_a_a1031_I : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_alcarry_a3_a = CARRY(dout_del_ff_adffs_a3_a & (!Pslope_Comparee_acomparator_acmp_end_alcarry_a2_a # !Din_a5_a_acombout) # !dout_del_ff_adffs_a3_a & !Din_a5_a_acombout & 
-- !Pslope_Comparee_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a3_a,
	datab => Din_a5_a_acombout,
	cin => Pslope_Comparee_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_alcarry_a3_a_a1031,
	cout => Pslope_Comparee_acomparator_acmp_end_alcarry_a3_a);

Pslope_Comparee_acomparator_acmp_end_alcarry_a4_a_a1030_I : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_alcarry_a4_a = CARRY(dout_del_ff_adffs_a4_a & Din_a6_a_acombout & !Pslope_Comparee_acomparator_acmp_end_alcarry_a3_a # !dout_del_ff_adffs_a4_a & (Din_a6_a_acombout # !Pslope_Comparee_acomparator_acmp_end_alcarry_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a4_a,
	datab => Din_a6_a_acombout,
	cin => Pslope_Comparee_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_alcarry_a4_a_a1030,
	cout => Pslope_Comparee_acomparator_acmp_end_alcarry_a4_a);

Pslope_Comparee_acomparator_acmp_end_alcarry_a5_a_a1029_I : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_alcarry_a5_a = CARRY(Din_a7_a_acombout & dout_del_ff_adffs_a5_a & !Pslope_Comparee_acomparator_acmp_end_alcarry_a4_a # !Din_a7_a_acombout & (dout_del_ff_adffs_a5_a # !Pslope_Comparee_acomparator_acmp_end_alcarry_a4_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a7_a_acombout,
	datab => dout_del_ff_adffs_a5_a,
	cin => Pslope_Comparee_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_alcarry_a5_a_a1029,
	cout => Pslope_Comparee_acomparator_acmp_end_alcarry_a5_a);

Pslope_Comparee_acomparator_acmp_end_alcarry_a6_a_a1028_I : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_alcarry_a6_a = CARRY(Din_a8_a_acombout & (!Pslope_Comparee_acomparator_acmp_end_alcarry_a5_a # !dout_del_ff_adffs_a6_a) # !Din_a8_a_acombout & !dout_del_ff_adffs_a6_a & 
-- !Pslope_Comparee_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a8_a_acombout,
	datab => dout_del_ff_adffs_a6_a,
	cin => Pslope_Comparee_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_alcarry_a6_a_a1028,
	cout => Pslope_Comparee_acomparator_acmp_end_alcarry_a6_a);

Pslope_Comparee_acomparator_acmp_end_alcarry_a7_a_a1027_I : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_alcarry_a7_a = CARRY(dout_del_ff_adffs_a7_a & (!Pslope_Comparee_acomparator_acmp_end_alcarry_a6_a # !Din_a9_a_acombout) # !dout_del_ff_adffs_a7_a & !Din_a9_a_acombout & 
-- !Pslope_Comparee_acomparator_acmp_end_alcarry_a6_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a7_a,
	datab => Din_a9_a_acombout,
	cin => Pslope_Comparee_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_alcarry_a7_a_a1027,
	cout => Pslope_Comparee_acomparator_acmp_end_alcarry_a7_a);

Pslope_Comparee_acomparator_acmp_end_alcarry_a8_a_a1026_I : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_alcarry_a8_a = CARRY(dout_del_ff_adffs_a8_a & Din_a10_a_acombout & !Pslope_Comparee_acomparator_acmp_end_alcarry_a7_a # !dout_del_ff_adffs_a8_a & (Din_a10_a_acombout # 
-- !Pslope_Comparee_acomparator_acmp_end_alcarry_a7_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a8_a,
	datab => Din_a10_a_acombout,
	cin => Pslope_Comparee_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_alcarry_a8_a_a1026,
	cout => Pslope_Comparee_acomparator_acmp_end_alcarry_a8_a);

Pslope_Comparee_acomparator_acmp_end_alcarry_a9_a_a1025_I : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_alcarry_a9_a = CARRY(dout_del_ff_adffs_a9_a & (!Pslope_Comparee_acomparator_acmp_end_alcarry_a8_a # !Din_a11_a_acombout) # !dout_del_ff_adffs_a9_a & !Din_a11_a_acombout & 
-- !Pslope_Comparee_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a9_a,
	datab => Din_a11_a_acombout,
	cin => Pslope_Comparee_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_alcarry_a9_a_a1025,
	cout => Pslope_Comparee_acomparator_acmp_end_alcarry_a9_a);

Pslope_Comparee_acomparator_acmp_end_alcarry_a10_a_a1024_I : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_alcarry_a10_a = CARRY(dout_del_ff_adffs_a10_a & Din_a12_a_acombout & !Pslope_Comparee_acomparator_acmp_end_alcarry_a9_a # !dout_del_ff_adffs_a10_a & (Din_a12_a_acombout # 
-- !Pslope_Comparee_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a10_a,
	datab => Din_a12_a_acombout,
	cin => Pslope_Comparee_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_alcarry_a10_a_a1024,
	cout => Pslope_Comparee_acomparator_acmp_end_alcarry_a10_a);

Pslope_Comparee_acomparator_acmp_end_alcarry_a11_a_a1023_I : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_alcarry_a11_a = CARRY(dout_del_ff_adffs_a11_a & (!Pslope_Comparee_acomparator_acmp_end_alcarry_a10_a # !Din_a13_a_acombout) # !dout_del_ff_adffs_a11_a & !Din_a13_a_acombout & 
-- !Pslope_Comparee_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a11_a,
	datab => Din_a13_a_acombout,
	cin => Pslope_Comparee_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_alcarry_a11_a_a1023,
	cout => Pslope_Comparee_acomparator_acmp_end_alcarry_a11_a);

Pslope_Comparee_acomparator_acmp_end_alcarry_a12_a_a1022_I : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_alcarry_a12_a = CARRY(dout_del_ff_adffs_a12_a & Din_a14_a_acombout & !Pslope_Comparee_acomparator_acmp_end_alcarry_a11_a # !dout_del_ff_adffs_a12_a & (Din_a14_a_acombout # 
-- !Pslope_Comparee_acomparator_acmp_end_alcarry_a11_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => dout_del_ff_adffs_a12_a,
	datab => Din_a14_a_acombout,
	cin => Pslope_Comparee_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_alcarry_a12_a_a1022,
	cout => Pslope_Comparee_acomparator_acmp_end_alcarry_a12_a);

Pslope_Comparee_acomparator_acmp_end_alcarry_a13_a_a1021_I : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_alcarry_a13_a = CARRY(Din_a15_a_acombout & dout_del_ff_adffs_a15_a & !Pslope_Comparee_acomparator_acmp_end_alcarry_a12_a # !Din_a15_a_acombout & (dout_del_ff_adffs_a15_a # 
-- !Pslope_Comparee_acomparator_acmp_end_alcarry_a12_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a15_a_acombout,
	datab => dout_del_ff_adffs_a15_a,
	cin => Pslope_Comparee_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_alcarry_a13_a_a1021,
	cout => Pslope_Comparee_acomparator_acmp_end_alcarry_a13_a);

Pslope_Comparee_acomparator_acmp_end_alcarry_a14_a_a1020_I : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_alcarry_a14_a = CARRY(Din_a15_a_acombout & (!Pslope_Comparee_acomparator_acmp_end_alcarry_a13_a # !dout_del_ff_adffs_a15_a) # !Din_a15_a_acombout & !dout_del_ff_adffs_a15_a & 
-- !Pslope_Comparee_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a15_a_acombout,
	datab => dout_del_ff_adffs_a15_a,
	cin => Pslope_Comparee_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_alcarry_a14_a_a1020,
	cout => Pslope_Comparee_acomparator_acmp_end_alcarry_a14_a);

Pslope_Comparee_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- Pslope_Comparee_acomparator_acmp_end_aagb_out = Din_a15_a_acombout & (Pslope_Comparee_acomparator_acmp_end_alcarry_a14_a & dout_del_ff_adffs_a15_a) # !Din_a15_a_acombout & (Pslope_Comparee_acomparator_acmp_end_alcarry_a14_a # dout_del_ff_adffs_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F550",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Din_a15_a_acombout,
	datad => dout_del_ff_adffs_a15_a,
	cin => Pslope_Comparee_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pslope_Comparee_acomparator_acmp_end_aagb_out);

PSlope_Vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Vec_a0_a = DFFE(Pslope_Comparee_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Pslope_Comparee_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Vec_a0_a);

PSlope_Vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Vec_a1_a = DFFE(PSlope_Vec_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PSlope_Vec_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Vec_a1_a);

PSlope_Vec_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Vec_a2_a = DFFE(PSlope_Vec_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PSlope_Vec_a1_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Vec_a2_a);

Pslope_areg0_I : apex20ke_lcell
-- Equation(s):
-- Pslope_areg0 = DFFE(PSlope_Vec_a0_a & (Pslope_areg0 # PSlope_Vec_a1_a & PSlope_Vec_a2_a) # !PSlope_Vec_a0_a & Pslope_areg0 & (PSlope_Vec_a1_a # PSlope_Vec_a2_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F8E0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Vec_a0_a,
	datab => PSlope_Vec_a1_a,
	datac => Pslope_areg0,
	datad => PSlope_Vec_a2_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Pslope_areg0);

PSlope_Del_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Del = DFFE(Pslope_areg0, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Pslope_areg0,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Del);

Disc_en_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_en,
	combout => Disc_en_acombout);

Level_out_areg0_I : apex20ke_lcell
-- Equation(s):
-- Level_out_areg0 = DFFE(PSlope_Del & level_cmp_compare_acomparator_acmp_end_aagb_out & !Pslope_areg0 & Disc_en_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Del,
	datab => level_cmp_compare_acomparator_acmp_end_aagb_out,
	datac => Pslope_areg0,
	datad => Disc_en_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Level_out_areg0);

Dout_Max_FF_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Max_FF_adffs_a12_a = DFFE(Din_a14_a_acombout & (PSlope_Del # !Pslope_areg0), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , Dout_Max_En_a30)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => PSlope_Del,
	datac => Din_a14_a_acombout,
	datad => Pslope_areg0,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => Dout_Max_En_a30,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Max_FF_adffs_a12_a);

Dout_Max_FF_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Max_FF_adffs_a11_a = DFFE(Din_a13_a_acombout & (PSlope_Del # !Pslope_areg0), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , Dout_Max_En_a30)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "88AA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Din_a13_a_acombout,
	datab => PSlope_Del,
	datad => Pslope_areg0,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => Dout_Max_En_a30,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Max_FF_adffs_a11_a);

Dout_Max_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Max_FF_adffs_a7_a = DFFE(Din_a9_a_acombout & (PSlope_Del # !Pslope_areg0), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , Dout_Max_En_a30)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F500",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Pslope_areg0,
	datac => PSlope_Del,
	datad => Din_a9_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => Dout_Max_En_a30,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Max_FF_adffs_a7_a);

Dout_Max_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Max_FF_adffs_a6_a = DFFE(Din_a8_a_acombout & (PSlope_Del # !Pslope_areg0), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , Dout_Max_En_a30)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F030",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Pslope_areg0,
	datac => Din_a8_a_acombout,
	datad => PSlope_Del,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => Dout_Max_En_a30,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Max_FF_adffs_a6_a);

Dout_Max_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Max_FF_adffs_a5_a = DFFE(Din_a7_a_acombout & (PSlope_Del # !Pslope_areg0), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , Dout_Max_En_a30)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F500",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Pslope_areg0,
	datac => PSlope_Del,
	datad => Din_a7_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => Dout_Max_En_a30,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Max_FF_adffs_a5_a);

Dout_Max_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Max_FF_adffs_a4_a = DFFE(Din_a6_a_acombout & (PSlope_Del # !Pslope_areg0), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , Dout_Max_En_a30)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Del,
	datac => Pslope_areg0,
	datad => Din_a6_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => Dout_Max_En_a30,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Max_FF_adffs_a4_a);

Dout_Max_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Max_FF_adffs_a3_a = DFFE(Din_a5_a_acombout & (PSlope_Del # !Pslope_areg0), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , Dout_Max_En_a30)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F030",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Pslope_areg0,
	datac => Din_a5_a_acombout,
	datad => PSlope_Del,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => Dout_Max_En_a30,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Max_FF_adffs_a3_a);

Dout_Max_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Max_FF_adffs_a1_a = DFFE(Din_a3_a_acombout & (PSlope_Del # !Pslope_areg0), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , Dout_Max_En_a30)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F500",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Pslope_areg0,
	datac => PSlope_Del,
	datad => Din_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => Dout_Max_En_a30,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Max_FF_adffs_a1_a);

dout_max_Compare_acomparator_acmp_end_alcarry_a0_a_a1034_I : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_alcarry_a0_a = CARRY(Din_a2_a_acombout & !Dout_Max_FF_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "0022",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a2_a_acombout,
	datab => Dout_Max_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_alcarry_a0_a_a1034,
	cout => dout_max_Compare_acomparator_acmp_end_alcarry_a0_a);

dout_max_Compare_acomparator_acmp_end_alcarry_a1_a_a1033_I : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_alcarry_a1_a = CARRY(Din_a3_a_acombout & Dout_Max_FF_adffs_a1_a & !dout_max_Compare_acomparator_acmp_end_alcarry_a0_a # !Din_a3_a_acombout & (Dout_Max_FF_adffs_a1_a # 
-- !dout_max_Compare_acomparator_acmp_end_alcarry_a0_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a3_a_acombout,
	datab => Dout_Max_FF_adffs_a1_a,
	cin => dout_max_Compare_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_alcarry_a1_a_a1033,
	cout => dout_max_Compare_acomparator_acmp_end_alcarry_a1_a);

dout_max_Compare_acomparator_acmp_end_alcarry_a2_a_a1032_I : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_alcarry_a2_a = CARRY(Dout_Max_FF_adffs_a2_a & Din_a4_a_acombout & !dout_max_Compare_acomparator_acmp_end_alcarry_a1_a # !Dout_Max_FF_adffs_a2_a & (Din_a4_a_acombout # 
-- !dout_max_Compare_acomparator_acmp_end_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Max_FF_adffs_a2_a,
	datab => Din_a4_a_acombout,
	cin => dout_max_Compare_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_alcarry_a2_a_a1032,
	cout => dout_max_Compare_acomparator_acmp_end_alcarry_a2_a);

dout_max_Compare_acomparator_acmp_end_alcarry_a3_a_a1031_I : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_alcarry_a3_a = CARRY(Din_a5_a_acombout & Dout_Max_FF_adffs_a3_a & !dout_max_Compare_acomparator_acmp_end_alcarry_a2_a # !Din_a5_a_acombout & (Dout_Max_FF_adffs_a3_a # 
-- !dout_max_Compare_acomparator_acmp_end_alcarry_a2_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a5_a_acombout,
	datab => Dout_Max_FF_adffs_a3_a,
	cin => dout_max_Compare_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_alcarry_a3_a_a1031,
	cout => dout_max_Compare_acomparator_acmp_end_alcarry_a3_a);

dout_max_Compare_acomparator_acmp_end_alcarry_a4_a_a1030_I : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_alcarry_a4_a = CARRY(Din_a6_a_acombout & (!dout_max_Compare_acomparator_acmp_end_alcarry_a3_a # !Dout_Max_FF_adffs_a4_a) # !Din_a6_a_acombout & !Dout_Max_FF_adffs_a4_a & 
-- !dout_max_Compare_acomparator_acmp_end_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a6_a_acombout,
	datab => Dout_Max_FF_adffs_a4_a,
	cin => dout_max_Compare_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_alcarry_a4_a_a1030,
	cout => dout_max_Compare_acomparator_acmp_end_alcarry_a4_a);

dout_max_Compare_acomparator_acmp_end_alcarry_a5_a_a1029_I : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_alcarry_a5_a = CARRY(Din_a7_a_acombout & Dout_Max_FF_adffs_a5_a & !dout_max_Compare_acomparator_acmp_end_alcarry_a4_a # !Din_a7_a_acombout & (Dout_Max_FF_adffs_a5_a # 
-- !dout_max_Compare_acomparator_acmp_end_alcarry_a4_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a7_a_acombout,
	datab => Dout_Max_FF_adffs_a5_a,
	cin => dout_max_Compare_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_alcarry_a5_a_a1029,
	cout => dout_max_Compare_acomparator_acmp_end_alcarry_a5_a);

dout_max_Compare_acomparator_acmp_end_alcarry_a6_a_a1028_I : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_alcarry_a6_a = CARRY(Din_a8_a_acombout & (!dout_max_Compare_acomparator_acmp_end_alcarry_a5_a # !Dout_Max_FF_adffs_a6_a) # !Din_a8_a_acombout & !Dout_Max_FF_adffs_a6_a & 
-- !dout_max_Compare_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a8_a_acombout,
	datab => Dout_Max_FF_adffs_a6_a,
	cin => dout_max_Compare_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_alcarry_a6_a_a1028,
	cout => dout_max_Compare_acomparator_acmp_end_alcarry_a6_a);

dout_max_Compare_acomparator_acmp_end_alcarry_a7_a_a1027_I : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_alcarry_a7_a = CARRY(Din_a9_a_acombout & Dout_Max_FF_adffs_a7_a & !dout_max_Compare_acomparator_acmp_end_alcarry_a6_a # !Din_a9_a_acombout & (Dout_Max_FF_adffs_a7_a # 
-- !dout_max_Compare_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a9_a_acombout,
	datab => Dout_Max_FF_adffs_a7_a,
	cin => dout_max_Compare_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_alcarry_a7_a_a1027,
	cout => dout_max_Compare_acomparator_acmp_end_alcarry_a7_a);

dout_max_Compare_acomparator_acmp_end_alcarry_a8_a_a1026_I : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_alcarry_a8_a = CARRY(Dout_Max_FF_adffs_a8_a & Din_a10_a_acombout & !dout_max_Compare_acomparator_acmp_end_alcarry_a7_a # !Dout_Max_FF_adffs_a8_a & (Din_a10_a_acombout # 
-- !dout_max_Compare_acomparator_acmp_end_alcarry_a7_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Max_FF_adffs_a8_a,
	datab => Din_a10_a_acombout,
	cin => dout_max_Compare_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_alcarry_a8_a_a1026,
	cout => dout_max_Compare_acomparator_acmp_end_alcarry_a8_a);

dout_max_Compare_acomparator_acmp_end_alcarry_a9_a_a1025_I : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_alcarry_a9_a = CARRY(Dout_Max_FF_adffs_a9_a & (!dout_max_Compare_acomparator_acmp_end_alcarry_a8_a # !Din_a11_a_acombout) # !Dout_Max_FF_adffs_a9_a & !Din_a11_a_acombout & 
-- !dout_max_Compare_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Max_FF_adffs_a9_a,
	datab => Din_a11_a_acombout,
	cin => dout_max_Compare_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_alcarry_a9_a_a1025,
	cout => dout_max_Compare_acomparator_acmp_end_alcarry_a9_a);

dout_max_Compare_acomparator_acmp_end_alcarry_a10_a_a1024_I : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_alcarry_a10_a = CARRY(Dout_Max_FF_adffs_a10_a & Din_a12_a_acombout & !dout_max_Compare_acomparator_acmp_end_alcarry_a9_a # !Dout_Max_FF_adffs_a10_a & (Din_a12_a_acombout # 
-- !dout_max_Compare_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Max_FF_adffs_a10_a,
	datab => Din_a12_a_acombout,
	cin => dout_max_Compare_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_alcarry_a10_a_a1024,
	cout => dout_max_Compare_acomparator_acmp_end_alcarry_a10_a);

dout_max_Compare_acomparator_acmp_end_alcarry_a11_a_a1023_I : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_alcarry_a11_a = CARRY(Din_a13_a_acombout & Dout_Max_FF_adffs_a11_a & !dout_max_Compare_acomparator_acmp_end_alcarry_a10_a # !Din_a13_a_acombout & (Dout_Max_FF_adffs_a11_a # 
-- !dout_max_Compare_acomparator_acmp_end_alcarry_a10_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a13_a_acombout,
	datab => Dout_Max_FF_adffs_a11_a,
	cin => dout_max_Compare_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_alcarry_a11_a_a1023,
	cout => dout_max_Compare_acomparator_acmp_end_alcarry_a11_a);

dout_max_Compare_acomparator_acmp_end_alcarry_a12_a_a1022_I : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_alcarry_a12_a = CARRY(Din_a14_a_acombout & (!dout_max_Compare_acomparator_acmp_end_alcarry_a11_a # !Dout_Max_FF_adffs_a12_a) # !Din_a14_a_acombout & !Dout_Max_FF_adffs_a12_a & 
-- !dout_max_Compare_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Din_a14_a_acombout,
	datab => Dout_Max_FF_adffs_a12_a,
	cin => dout_max_Compare_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_alcarry_a12_a_a1022,
	cout => dout_max_Compare_acomparator_acmp_end_alcarry_a12_a);

dout_max_Compare_acomparator_acmp_end_alcarry_a13_a_a1021_I : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_alcarry_a13_a = CARRY(Dout_Max_FF_adffs_a15_a & (!dout_max_Compare_acomparator_acmp_end_alcarry_a12_a # !Din_a15_a_acombout) # !Dout_Max_FF_adffs_a15_a & !Din_a15_a_acombout & 
-- !dout_max_Compare_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Max_FF_adffs_a15_a,
	datab => Din_a15_a_acombout,
	cin => dout_max_Compare_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_alcarry_a13_a_a1021,
	cout => dout_max_Compare_acomparator_acmp_end_alcarry_a13_a);

dout_max_Compare_acomparator_acmp_end_alcarry_a14_a_a1020_I : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_alcarry_a14_a = CARRY(Dout_Max_FF_adffs_a15_a & Din_a15_a_acombout & !dout_max_Compare_acomparator_acmp_end_alcarry_a13_a # !Dout_Max_FF_adffs_a15_a & (Din_a15_a_acombout # 
-- !dout_max_Compare_acomparator_acmp_end_alcarry_a13_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Max_FF_adffs_a15_a,
	datab => Din_a15_a_acombout,
	cin => dout_max_Compare_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_alcarry_a14_a_a1020,
	cout => dout_max_Compare_acomparator_acmp_end_alcarry_a14_a);

dout_max_Compare_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- dout_max_Compare_acomparator_acmp_end_aagb_out = Dout_Max_FF_adffs_a15_a & (dout_max_Compare_acomparator_acmp_end_alcarry_a14_a # !Din_a15_a_acombout) # !Dout_Max_FF_adffs_a15_a & (dout_max_Compare_acomparator_acmp_end_alcarry_a14_a & !Din_a15_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A0FA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Max_FF_adffs_a15_a,
	datad => Din_a15_a_acombout,
	cin => dout_max_Compare_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dout_max_Compare_acomparator_acmp_end_aagb_out);

Dout_Max_En_a30_I : apex20ke_lcell
-- Equation(s):
-- Dout_Max_En_a30 = Pslope_areg0 & (dout_max_Compare_acomparator_acmp_end_aagb_out & level_cmp_compare_acomparator_acmp_end_aagb_out # !PSlope_Del)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "D050",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Del,
	datab => dout_max_Compare_acomparator_acmp_end_aagb_out,
	datac => Pslope_areg0,
	datad => level_cmp_compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Max_En_a30);

Dout_Max_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Max_FF_adffs_a0_a = DFFE(Din_a2_a_acombout & (PSlope_Del # !Pslope_areg0), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , Dout_Max_En_a30)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F500",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Pslope_areg0,
	datac => PSlope_Del,
	datad => Din_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => Dout_Max_En_a30,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Max_FF_adffs_a0_a);

Dout_Max_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Max_FF_adffs_a2_a = DFFE(Din_a4_a_acombout & (PSlope_Del # !Pslope_areg0), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , Dout_Max_En_a30)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F050",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Pslope_areg0,
	datac => Din_a4_a_acombout,
	datad => PSlope_Del,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => Dout_Max_En_a30,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Max_FF_adffs_a2_a);

Dout_Max_FF_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Max_FF_adffs_a8_a = DFFE(Din_a10_a_acombout & (PSlope_Del # !Pslope_areg0), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , Dout_Max_En_a30)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Del,
	datac => Pslope_areg0,
	datad => Din_a10_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => Dout_Max_En_a30,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Max_FF_adffs_a8_a);

Dout_Max_FF_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Max_FF_adffs_a9_a = DFFE(Din_a11_a_acombout & (PSlope_Del # !Pslope_areg0), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , Dout_Max_En_a30)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Del,
	datac => Pslope_areg0,
	datad => Din_a11_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => Dout_Max_En_a30,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Max_FF_adffs_a9_a);

Dout_Max_FF_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Max_FF_adffs_a10_a = DFFE(Din_a12_a_acombout & (PSlope_Del # !Pslope_areg0), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , Dout_Max_En_a30)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Del,
	datac => Pslope_areg0,
	datad => Din_a12_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => Dout_Max_En_a30,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Max_FF_adffs_a10_a);

Dout_Max_FF_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Max_FF_adffs_a15_a = DFFE(Din_a15_a_acombout & (PSlope_Del # !Pslope_areg0), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , Dout_Max_En_a30)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Del,
	datac => Pslope_areg0,
	datad => Din_a15_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => Dout_Max_En_a30,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Max_FF_adffs_a15_a);

BLEVEL_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => BLEVEL_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(0));

BLEVEL_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => BLEVEL_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(1));

BLEVEL_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => BLEVEL_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(2));

BLEVEL_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => BLEVEL_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(3));

BLEVEL_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => BLEVEL_FF_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(4));

BLEVEL_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => BLEVEL_FF_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(5));

BLEVEL_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => BLEVEL_FF_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(6));

BLEVEL_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => BLEVEL_FF_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(7));

BLEVEL_UPD_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => BLEVEL_UPD_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL_UPD);

Level_Cmp_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => level_cmp_compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Level_Cmp);

Pslope_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Pslope_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Pslope);

Level_out_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Level_out_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Level_out);

PK_DONE_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Level_out_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PK_DONE);

PBusy_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PBusy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PBusy);

Dout_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a2_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(0));

Dout_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(1));

Dout_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(2));

Dout_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a5_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(3));

Dout_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a6_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(4));

Dout_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a7_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(5));

Dout_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a8_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(6));

Dout_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a9_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(7));

Dout_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a10_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(8));

Dout_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a11_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(9));

Dout_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a12_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(10));

Dout_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a13_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(11));

Dout_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a14_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(12));

Dout_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a15_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(13));

Dout_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a15_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(14));

Dout_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Din_a15_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(15));

Dout_Max_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(0));

Dout_Max_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(1));

Dout_Max_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(2));

Dout_Max_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(3));

Dout_Max_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(4));

Dout_Max_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(5));

Dout_Max_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(6));

Dout_Max_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(7));

Dout_Max_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(8));

Dout_Max_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(9));

Dout_Max_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(10));

Dout_Max_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(11));

Dout_Max_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(12));

Dout_Max_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(13));

Dout_Max_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(14));

Dout_Max_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Dout_Max_FF_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout_Max(15));
END structure;


