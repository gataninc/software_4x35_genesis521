-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "05/01/2006 12:39:28"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	wds IS
    PORT (
	imr : IN std_logic;
	clk : IN std_logic;
	Time_Clr : IN std_logic;
	Time_Enable : IN std_logic;
	FIR_MR : IN std_logic;
	Memory_Access : IN std_logic;
	ROI_WE : IN std_logic;
	DSP_D : IN std_logic_vector(15 DOWNTO 0);
	DSP_RAMA : IN std_logic_vector(7 DOWNTO 0);
	Disc_en : IN std_logic;
	Din : IN std_logic_vector(15 DOWNTO 0);
	Tlevel : IN std_logic_vector(15 DOWNTO 0);
	BLEVEL : OUT std_logic_vector(7 DOWNTO 0);
	BLEVEL_UPD : OUT std_logic;
	FDISC : OUT std_logic;
	Meas_Done : OUT std_logic;
	PBusy : OUT std_logic;
	ROI_SUM : OUT std_logic_vector(31 DOWNTO 0);
	ROI_LU : OUT std_logic_vector(15 DOWNTO 0);
	dout : OUT std_logic_vector(15 DOWNTO 0);
	CPeak : OUT std_logic_vector(13 DOWNTO 0)
	);
END wds;

ARCHITECTURE structure OF wds IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_Time_Clr : std_logic;
SIGNAL ww_Time_Enable : std_logic;
SIGNAL ww_FIR_MR : std_logic;
SIGNAL ww_Memory_Access : std_logic;
SIGNAL ww_ROI_WE : std_logic;
SIGNAL ww_DSP_D : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DSP_RAMA : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_Disc_en : std_logic;
SIGNAL ww_Din : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Tlevel : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_BLEVEL : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_BLEVEL_UPD : std_logic;
SIGNAL ww_FDISC : std_logic;
SIGNAL ww_Meas_Done : std_logic;
SIGNAL ww_PBusy : std_logic;
SIGNAL ww_ROI_SUM : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_ROI_LU : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_dout : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_CPeak : std_logic_vector(13 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][0]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][0]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][1]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][1]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][2]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][2]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][3]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][3]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][4]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][4]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][5]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][5]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][6]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][6]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][7]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][7]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][8]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][8]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][9]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][9]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][10]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][10]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][11]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][11]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][12]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][12]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][13]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][13]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][14]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][14]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][15]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ROI_RAM|sram|segment[0][15]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][4]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][4]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][5]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][5]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][6]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][6]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][7]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][7]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][8]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][8]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][9]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][9]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][10]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][10]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][11]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][11]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][12]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][12]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][13]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][13]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][14]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][14]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][15]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][15]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][16]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][16]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][17]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][17]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][18]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][18]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][19]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][19]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][3]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][3]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][2]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][2]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][4]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][4]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][1]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][1]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][5]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][5]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][6]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][6]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][7]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][7]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][8]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][8]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][9]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][9]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][10]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][10]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][11]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][11]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][12]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][12]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][13]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][13]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][14]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][14]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][15]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][15]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][16]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][16]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][17]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][17]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][18]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][18]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][19]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][19]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][3]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][3]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][0]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][0]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][2]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][2]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][1]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][1]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][0]_WADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][0]_RADDR_bus\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][4]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][5]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][6]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][7]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][8]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][9]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][10]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][11]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][12]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][13]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][14]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][15]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][16]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][17]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][18]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][19]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][3]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][2]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][1]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS1|DLY1|sram|segment[0][0]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][18]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][17]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][16]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][15]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][14]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][13]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][12]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][11]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][10]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][9]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][8]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][7]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][6]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][5]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][4]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][3]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][2]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][1]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][0]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][19]_modesel\ : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL \ROI_RAM|sram|segment[0][15]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \ROI_RAM|sram|segment[0][12]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \ROI_RAM|sram|segment[0][14]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \ROI_RAM|sram|segment[0][5]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \ROI_RAM|sram|segment[0][6]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \ROI_RAM|sram|segment[0][4]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \ROI_RAM|sram|segment[0][2]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \ROI_RAM|sram|segment[0][1]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \ROI_RAM|sram|segment[0][0]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \ROI_RAM|sram|segment[0][3]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \ROI_RAM|sram|segment[0][7]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \ROI_RAM|sram|segment[0][8]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \ROI_RAM|sram|segment[0][9]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \ROI_RAM|sram|segment[0][10]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \ROI_RAM|sram|segment[0][11]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \ROI_RAM|sram|segment[0][13]_modesel\ : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL \TO_Cnt[0]\ : std_logic;
SIGNAL \ROI_DEF~55\ : std_logic;
SIGNAL \ROI_DEF~56\ : std_logic;
SIGNAL PSlope_Gen_Cmp_Del : std_logic;
SIGNAL \U_Filter|DS2|Acc_ff|dffs[3]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[14]~1044\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[5]\ : std_logic;
SIGNAL \blevel_cnt[1]\ : std_logic;
SIGNAL \Equal~124\ : std_logic;
SIGNAL \blevel_cnt[6]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[2]~2\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[3]|comp|sub_comptree|eq_cmp_end|aeb_out\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[14]~1044\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[4]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[4]\ : std_logic;
SIGNAL \U_Filter|DS2|Acc_ff|dffs[2]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[5]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[5]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[6]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[6]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[7]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[7]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[8]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[8]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[9]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[9]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[10]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[10]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[11]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[11]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[12]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[12]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[13]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[13]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[14]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[14]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[15]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[15]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[16]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[16]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[17]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[17]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[18]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[18]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[19]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[14]~50\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[14]~1044\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[13]~1045\ : std_logic;
SIGNAL \add~188\ : std_logic;
SIGNAL \add~204\ : std_logic;
SIGNAL \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[1]~3\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[2]|comp|sub_comptree|eq_cmp_end|aeb_out\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[13]~1045\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[14]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[3]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[3]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[4]~289\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[4]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[4]\ : std_logic;
SIGNAL \U_Filter|DS2|Acc_ff|dffs[1]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[5]~292\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[5]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[5]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[6]~295\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[6]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[6]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[7]~298\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[7]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[7]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[8]~301\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[8]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[8]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[9]~304\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[9]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[9]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[10]~307\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[10]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[10]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[11]~310\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[11]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[11]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[12]~313\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[12]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[12]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[13]~316\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[13]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[13]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[14]~319\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[14]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[14]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[15]~322\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[15]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[15]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[16]~325\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[16]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[16]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[17]~328\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[17]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[17]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[18]~331\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[18]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[18]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[19]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[19]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[13]~51\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[13]~1045\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[12]~1046\ : std_logic;
SIGNAL \clock_proc~2\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[14]\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[12]\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[13]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[3]|comp|cmp[0]|agb_out\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[0]|agb_out\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[1]|comp|sub_comptree|eq_cmp_end|aeb_out\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[3]|comp|cmp[0]|aeb_out\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[12]~1046\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[13]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[2]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[2]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[3]~337\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[3]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[3]\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~447\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~445\ : std_logic;
SIGNAL \U_Filter|DS2|Acc_ff|dffs[0]\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~451\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~449\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~455\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~453\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~459\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~457\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~463\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~461\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~467\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~465\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~471\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~469\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~475\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~473\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~479\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~477\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~483\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~481\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~487\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~485\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~491\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~489\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~495\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~493\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~499\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~497\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~503\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~501\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~505\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[12]~52\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[12]~1046\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[11]~1047\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[11]\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[9]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[2]|comp|cmp[0]|aeb_out\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[11]~1047\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[12]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[1]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[1]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[2]~340\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[2]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[2]\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~511\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~509\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[4]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[5]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[6]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[7]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[8]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[9]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[10]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[11]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[12]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[13]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[14]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[15]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[16]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[17]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[18]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[19]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[11]~53\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[11]~1047\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[10]~1048\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[7]\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[4]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[1]|comp|cmp[0]|aeb_out\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[10]~1048\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[0]\ : std_logic;
SIGNAL \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[0]\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[1]~343\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[1]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[1]\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~515\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~513\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[3]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[10]~54\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[10]~1048\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[9]~1049\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[0]|lcarry[2]~156\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[9]~1049\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[0]~346\ : std_logic;
SIGNAL \U_Filter|DS1|Dout_ff|dffs[0]\ : std_logic;
SIGNAL \U_Filter|acc1_del_ff|dffs[0]\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~519\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~517\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[2]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[9]~55\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[9]~1049\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[8]~1050\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[0]|lcarry[1]~157\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[2]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[8]~1050\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[9]\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~523\ : std_logic;
SIGNAL \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~521\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[1]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[8]~56\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[8]~1050\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[7]~1051\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[0]|lcarry[0]~158\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[7]~1051\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[8]\ : std_logic;
SIGNAL \U_Filter|DS1|DLY1|sram|q[0]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[7]~57\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[7]~1051\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[6]~1052\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[6]~1052\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[7]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[6]~58\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[6]~1052\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[5]~1053\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[5]~1053\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[5]~1053\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[4]~1054\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[4]~1054\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[5]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[4]~1054\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[3]~1055\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[3]~1055\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[4]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[3]~1055\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[2]~1056\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[2]~1056\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[3]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[2]~1056\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[1]~1057\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[1]~1057\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[2]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[1]~1057\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[0]~1058\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[0]~1058\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[1]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[0]~1058\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[0]\ : std_logic;
SIGNAL \DSP_D[0]~combout\ : std_logic;
SIGNAL \DSP_D[1]~combout\ : std_logic;
SIGNAL \DSP_D[2]~combout\ : std_logic;
SIGNAL \DSP_D[4]~combout\ : std_logic;
SIGNAL \DSP_D[5]~combout\ : std_logic;
SIGNAL \DSP_D[6]~combout\ : std_logic;
SIGNAL \DSP_D[12]~combout\ : std_logic;
SIGNAL \DSP_D[14]~combout\ : std_logic;
SIGNAL \DSP_D[15]~combout\ : std_logic;
SIGNAL \Tlevel[14]~combout\ : std_logic;
SIGNAL \Tlevel[13]~combout\ : std_logic;
SIGNAL \Din[4]~combout\ : std_logic;
SIGNAL \Din[5]~combout\ : std_logic;
SIGNAL \Din[6]~combout\ : std_logic;
SIGNAL \Din[7]~combout\ : std_logic;
SIGNAL \Din[8]~combout\ : std_logic;
SIGNAL \Din[9]~combout\ : std_logic;
SIGNAL \Din[10]~combout\ : std_logic;
SIGNAL \Din[11]~combout\ : std_logic;
SIGNAL \Din[12]~combout\ : std_logic;
SIGNAL \Din[13]~combout\ : std_logic;
SIGNAL \Din[14]~combout\ : std_logic;
SIGNAL \Din[15]~combout\ : std_logic;
SIGNAL \Tlevel[12]~combout\ : std_logic;
SIGNAL \Din[3]~combout\ : std_logic;
SIGNAL \Din[2]~combout\ : std_logic;
SIGNAL \Din[1]~combout\ : std_logic;
SIGNAL \Tlevel[9]~combout\ : std_logic;
SIGNAL \Din[0]~combout\ : std_logic;
SIGNAL \Tlevel[8]~combout\ : std_logic;
SIGNAL \Tlevel[7]~combout\ : std_logic;
SIGNAL \Tlevel[5]~combout\ : std_logic;
SIGNAL \Tlevel[4]~combout\ : std_logic;
SIGNAL \Tlevel[3]~combout\ : std_logic;
SIGNAL \Tlevel[2]~combout\ : std_logic;
SIGNAL \Tlevel[1]~combout\ : std_logic;
SIGNAL \Tlevel[0]~combout\ : std_logic;
SIGNAL \clk~combout\ : std_logic;
SIGNAL \imr~combout\ : std_logic;
SIGNAL \U_Filter|wadr_counter|wysi_counter|sload_path[0]\ : std_logic;
SIGNAL \U_Filter|wadr_counter|wysi_counter|counter_cell[0]~COUT\ : std_logic;
SIGNAL \U_Filter|wadr_counter|wysi_counter|sload_path[1]\ : std_logic;
SIGNAL \U_Filter|wadr_counter|wysi_counter|counter_cell[1]~COUT\ : std_logic;
SIGNAL \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ : std_logic;
SIGNAL \U_Filter|wadr_counter|wysi_counter|counter_cell[2]~COUT\ : std_logic;
SIGNAL \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ : std_logic;
SIGNAL \U_Filter|add~46\ : std_logic;
SIGNAL \U_Filter|add~47\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[18]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[17]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[16]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[15]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[14]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[13]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[12]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[11]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[10]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[9]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[8]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[7]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[6]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[5]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[4]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[3]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[2]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[1]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[0]\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~523\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~519\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~515\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~511\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~447\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~451\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~455\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~459\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~463\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~467\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~471\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~475\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~479\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~483\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~487\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~491\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~495\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~499\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~501\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~497\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~493\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~489\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~485\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~481\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~477\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~473\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~469\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~465\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~461\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~457\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~453\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~449\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~445\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~509\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~513\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~517\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~521\ : std_logic;
SIGNAL \FIR_MR~combout\ : std_logic;
SIGNAL \U_Filter|DS2|Acc_ff|dffs[0]~76\ : std_logic;
SIGNAL \U_Filter|DS2|Acc_ff|dffs[1]~73\ : std_logic;
SIGNAL \U_Filter|DS2|Acc_ff|dffs[2]~70\ : std_logic;
SIGNAL \U_Filter|DS2|Acc_ff|dffs[3]~67\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[4]~114\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[5]~117\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[6]~120\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[7]~123\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[8]~126\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[9]~129\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[10]~132\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[11]~135\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[12]~138\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[13]~141\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[14]~144\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[15]~147\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[16]~150\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[17]~153\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[18]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[14]\ : std_logic;
SIGNAL \U_Filter|DS2|DLY1|sram|q[19]\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~503\ : std_logic;
SIGNAL \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~505\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[18]~156\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[19]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[15]\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[15]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[3]|comp|cmp[1]|aeb_out\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[3]|comp|cmp[1]|agb_out\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[3]|comp|sub_comptree|gt_cmp_end|agb_out\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[15]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[11]\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[14]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[10]\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[10]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[2]|comp|cmp[1]|aeb_out\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[12]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[8]\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[13]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[9]\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[8]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[2]|comp|cmp[0]|agb_out\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[2]|comp|cmp[1]|agb_out\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[2]|comp|sub_comptree|gt_cmp_end|agb_out\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[11]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[7]\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[10]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[6]\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[6]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[1]|comp|cmp[1]|aeb_out\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[9]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[5]\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[8]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[4]\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[5]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[1]|comp|cmp[0]|agb_out\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[1]|comp|cmp[1]|agb_out\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[1]|comp|sub_comptree|gt_cmp_end|agb_out\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[7]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[3]\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[3]\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[6]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[2]\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[5]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[1]\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[1]\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[4]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[0]\ : std_logic;
SIGNAL \Dout_Del_ff|dffs[0]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[0]|lcarry[0]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[0]|lcarry[1]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[0]|lcarry[2]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|cmp[0]|$00006\ : std_logic;
SIGNAL \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[0]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[1]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[2]\ : std_logic;
SIGNAL \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|agb_out\ : std_logic;
SIGNAL \Tlevel[15]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[15]\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[17]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[13]\ : std_logic;
SIGNAL \U_Filter|DS2|Dout_ff|dffs[16]\ : std_logic;
SIGNAL \U_Filter|Dout_FF|dffs[12]\ : std_logic;
SIGNAL \Tlevel[11]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[11]\ : std_logic;
SIGNAL \Tlevel[10]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[10]\ : std_logic;
SIGNAL \Tlevel[6]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[6]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[0]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[1]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[2]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[3]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[4]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[5]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[6]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[7]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[8]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[9]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[10]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[11]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[12]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[13]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|lcarry[14]\ : std_logic;
SIGNAL \level_cmp_compare|comparator|cmp_end|agb_out\ : std_logic;
SIGNAL \Disc_en~combout\ : std_logic;
SIGNAL \clock_proc~45\ : std_logic;
SIGNAL PSlope_Gen : std_logic;
SIGNAL PSlope_Gen_Del : std_logic;
SIGNAL FD_STATE : std_logic;
SIGNAL \add~192\ : std_logic;
SIGNAL \blevel_cnt[0]\ : std_logic;
SIGNAL \add~194\ : std_logic;
SIGNAL \add~190\ : std_logic;
SIGNAL \add~186\ : std_logic;
SIGNAL \add~198\ : std_logic;
SIGNAL \add~214\ : std_logic;
SIGNAL \add~208\ : std_logic;
SIGNAL \blevel_cnt[5]\ : std_logic;
SIGNAL \add~210\ : std_logic;
SIGNAL \add~206\ : std_logic;
SIGNAL \add~200\ : std_logic;
SIGNAL \blevel_cnt[7]\ : std_logic;
SIGNAL \add~212\ : std_logic;
SIGNAL \blevel_cnt[4]\ : std_logic;
SIGNAL \add~196\ : std_logic;
SIGNAL \blevel_cnt[3]\ : std_logic;
SIGNAL \add~184\ : std_logic;
SIGNAL \blevel_cnt[2]\ : std_logic;
SIGNAL \Equal~128\ : std_logic;
SIGNAL \Equal~126\ : std_logic;
SIGNAL \BLEVEL_UPD~reg0\ : std_logic;
SIGNAL \FDISC~reg0\ : std_logic;
SIGNAL \TO_Cnt[0]~46\ : std_logic;
SIGNAL \TO_Cnt[1]\ : std_logic;
SIGNAL \TO_Cnt[1]~49\ : std_logic;
SIGNAL \TO_Cnt[2]\ : std_logic;
SIGNAL \TO_Cnt[2]~55\ : std_logic;
SIGNAL \TO_Cnt[3]~52\ : std_logic;
SIGNAL \TO_Cnt[4]\ : std_logic;
SIGNAL \TO_Cnt[3]\ : std_logic;
SIGNAL \Equal~119\ : std_logic;
SIGNAL \Equal~120\ : std_logic;
SIGNAL PK_DONE : std_logic;
SIGNAL \Meas_Done~reg0\ : std_logic;
SIGNAL \BLEVEL_FF|dffs[0]\ : std_logic;
SIGNAL \BLEVEL_FF|dffs[1]\ : std_logic;
SIGNAL \BLEVEL_FF|dffs[2]\ : std_logic;
SIGNAL \BLEVEL_FF|dffs[3]\ : std_logic;
SIGNAL \BLEVEL_FF|dffs[4]\ : std_logic;
SIGNAL \BLEVEL_FF|dffs[5]\ : std_logic;
SIGNAL \BLEVEL_FF|dffs[6]\ : std_logic;
SIGNAL \BLEVEL_FF|dffs[7]\ : std_logic;
SIGNAL \ROI_WE~combout\ : std_logic;
SIGNAL \DSP_RAMA[0]~combout\ : std_logic;
SIGNAL \~GND\ : std_logic;
SIGNAL \Dout_Max_Clr~13\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[14]\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[12]\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[11]\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[10]\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[9]\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[8]\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[7]\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[6]\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[5]\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[4]\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[3]\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[2]\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[1]\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[0]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[0]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[1]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[2]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[3]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[4]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[5]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[6]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[7]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[8]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[9]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[10]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[11]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[12]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[13]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|lcarry[14]\ : std_logic;
SIGNAL \dout_max_Compare|comparator|cmp_end|agb_out\ : std_logic;
SIGNAL \Dout_Max_En~6\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[15]\ : std_logic;
SIGNAL \Int_CPEak_FF|dffs[13]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[5]~59\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[4]~COUT\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[5]~COUT\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[6]\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[6]~COUT\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[7]\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[7]~COUT\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[8]\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[8]~COUT\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[9]\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[9]~COUT\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[10]\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[10]~COUT\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[11]\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[11]~COUT\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[12]\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[0]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[0]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[1]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[2]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[3]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[4]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[5]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[6]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[7]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[8]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[9]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[10]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[11]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[12]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[13]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|lcarry[14]\ : std_logic;
SIGNAL \Clamp_Dout_Compare|comparator|cmp_end|agb_out\ : std_logic;
SIGNAL \Max_Clamp_Inc~1\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[12]~COUT\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[13]\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[13]~COUT\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[14]\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[14]~COUT\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[15]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[5]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[6]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[7]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[8]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[9]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[10]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[11]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[12]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[13]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|lcarry[14]\ : std_logic;
SIGNAL \Max_Clamp_Compare|comparator|cmp_end|agb_out\ : std_logic;
SIGNAL \Time_Enable~combout\ : std_logic;
SIGNAL \Max_Clamp_Ld~4\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[0]~COUT\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[1]\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[1]~COUT\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[2]\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[2]~COUT\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[3]\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[3]~COUT\ : std_logic;
SIGNAL \Max_Clamp_Val_Cntr|wysi_counter|q[4]\ : std_logic;
SIGNAL \CPeak_FF|dffs[4]\ : std_logic;
SIGNAL \Memory_Access~combout\ : std_logic;
SIGNAL \ROI_ADDR[0]~56\ : std_logic;
SIGNAL \DSP_RAMA[1]~combout\ : std_logic;
SIGNAL \CPeak_FF|dffs[5]\ : std_logic;
SIGNAL \ROI_ADDR[1]~57\ : std_logic;
SIGNAL \DSP_RAMA[2]~combout\ : std_logic;
SIGNAL \CPeak_FF|dffs[6]\ : std_logic;
SIGNAL \ROI_ADDR[2]~58\ : std_logic;
SIGNAL \DSP_RAMA[3]~combout\ : std_logic;
SIGNAL \CPeak_FF|dffs[7]\ : std_logic;
SIGNAL \ROI_ADDR[3]~59\ : std_logic;
SIGNAL \CPeak_FF|dffs[8]\ : std_logic;
SIGNAL \DSP_RAMA[4]~combout\ : std_logic;
SIGNAL \ROI_ADDR[4]~60\ : std_logic;
SIGNAL \DSP_RAMA[5]~combout\ : std_logic;
SIGNAL \CPeak_FF|dffs[9]\ : std_logic;
SIGNAL \ROI_ADDR[5]~61\ : std_logic;
SIGNAL \CPeak_FF|dffs[10]\ : std_logic;
SIGNAL \DSP_RAMA[6]~combout\ : std_logic;
SIGNAL \ROI_ADDR[6]~62\ : std_logic;
SIGNAL \CPeak_FF|dffs[11]\ : std_logic;
SIGNAL \DSP_RAMA[7]~combout\ : std_logic;
SIGNAL \ROI_ADDR[7]~63\ : std_logic;
SIGNAL \ROI_RAM|sram|q[15]\ : std_logic;
SIGNAL \CPeak_FF|dffs[0]\ : std_logic;
SIGNAL \ROI_RAM|sram|q[12]\ : std_logic;
SIGNAL \ROI_RAM|sram|q[14]\ : std_logic;
SIGNAL \ROI_DEF~62\ : std_logic;
SIGNAL \ROI_DEF~63\ : std_logic;
SIGNAL \CPeak_FF|dffs[3]\ : std_logic;
SIGNAL \ROI_RAM|sram|q[5]\ : std_logic;
SIGNAL \ROI_RAM|sram|q[6]\ : std_logic;
SIGNAL \ROI_RAM|sram|q[4]\ : std_logic;
SIGNAL \ROI_DEF~57\ : std_logic;
SIGNAL \ROI_DEF~58\ : std_logic;
SIGNAL \ROI_RAM|sram|q[2]\ : std_logic;
SIGNAL \CPeak_FF|dffs[1]\ : std_logic;
SIGNAL \ROI_RAM|sram|q[1]\ : std_logic;
SIGNAL \ROI_RAM|sram|q[0]\ : std_logic;
SIGNAL \ROI_DEF~59\ : std_logic;
SIGNAL \ROI_DEF~60\ : std_logic;
SIGNAL \ROI_DEF~61\ : std_logic;
SIGNAL \ROI_DEF~64\ : std_logic;
SIGNAL \ROI_INC~22\ : std_logic;
SIGNAL \Time_Clr~combout\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[0]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[0]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[1]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[1]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[2]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[2]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[3]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[3]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[4]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[4]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[5]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[5]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[6]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[6]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[7]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[7]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[8]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[8]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[9]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[9]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[10]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[10]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[11]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[11]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[12]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[12]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[13]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[13]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[14]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[14]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[15]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[15]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[16]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[16]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[17]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[17]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[18]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[18]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[19]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[19]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[20]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[20]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[21]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[21]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[22]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[22]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[23]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[23]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[24]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[24]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[25]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[25]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[26]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[26]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[27]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[27]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[28]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[28]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[29]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[29]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[30]\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|counter_cell[30]~COUT\ : std_logic;
SIGNAL \ROI_SUM_CNTR|wysi_counter|sload_path[31]\ : std_logic;
SIGNAL \DSP_D[3]~combout\ : std_logic;
SIGNAL \ROI_RAM|sram|q[3]\ : std_logic;
SIGNAL \DSP_D[7]~combout\ : std_logic;
SIGNAL \ROI_RAM|sram|q[7]\ : std_logic;
SIGNAL \DSP_D[8]~combout\ : std_logic;
SIGNAL \ROI_RAM|sram|q[8]\ : std_logic;
SIGNAL \DSP_D[9]~combout\ : std_logic;
SIGNAL \ROI_RAM|sram|q[9]\ : std_logic;
SIGNAL \DSP_D[10]~combout\ : std_logic;
SIGNAL \ROI_RAM|sram|q[10]\ : std_logic;
SIGNAL \DSP_D[11]~combout\ : std_logic;
SIGNAL \ROI_RAM|sram|q[11]\ : std_logic;
SIGNAL \DSP_D[13]~combout\ : std_logic;
SIGNAL \ROI_RAM|sram|q[13]\ : std_logic;
SIGNAL \CPeak_FF|dffs[2]\ : std_logic;
SIGNAL \CPeak_FF|dffs[12]\ : std_logic;
SIGNAL \CPeak_FF|dffs[13]\ : std_logic;
SIGNAL ALT_INV_FD_STATE : std_logic;

BEGIN

ww_imr <= imr;
ww_clk <= clk;
ww_Time_Clr <= Time_Clr;
ww_Time_Enable <= Time_Enable;
ww_FIR_MR <= FIR_MR;
ww_Memory_Access <= Memory_Access;
ww_ROI_WE <= ROI_WE;
ww_DSP_D <= DSP_D;
ww_DSP_RAMA <= DSP_RAMA;
ww_Disc_en <= Disc_en;
ww_Din <= Din;
ww_Tlevel <= Tlevel;
BLEVEL <= ww_BLEVEL;
BLEVEL_UPD <= ww_BLEVEL_UPD;
FDISC <= ww_FDISC;
Meas_Done <= ww_Meas_Done;
PBusy <= ww_PBusy;
ROI_SUM <= ww_ROI_SUM;
ROI_LU <= ww_ROI_LU;
dout <= ww_dout;
CPeak <= ww_CPeak;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\ROI_RAM|sram|segment[0][0]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][0]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][1]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][1]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][2]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][2]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][3]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][3]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][4]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][4]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][5]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][5]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][6]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][6]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][7]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][7]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][8]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][8]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][9]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][9]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][10]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][10]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][11]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][11]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][12]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][12]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][13]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][13]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][14]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][14]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][15]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\ROI_RAM|sram|segment[0][15]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \ROI_ADDR[7]~63\ & \ROI_ADDR[6]~62\ & \ROI_ADDR[5]~61\ & \ROI_ADDR[4]~60\ & \ROI_ADDR[3]~59\ & \ROI_ADDR[2]~58\ & \ROI_ADDR[1]~57\ & \ROI_ADDR[0]~56\);

\U_Filter|DS2|DLY1|sram|segment[0][4]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][4]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][5]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][5]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][6]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][6]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][7]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][7]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][8]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][8]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][9]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][9]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][10]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][10]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][11]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][11]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][12]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][12]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][13]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][13]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][14]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][14]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][15]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][15]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][16]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][16]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][17]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][17]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][18]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][18]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][19]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][19]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][3]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][3]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][2]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][2]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][4]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][4]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][1]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][1]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][5]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][5]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][6]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][6]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][7]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][7]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][8]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][8]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][9]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][9]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][10]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][10]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][11]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][11]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][12]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][12]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][13]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][13]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][14]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][14]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][15]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][15]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][16]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][16]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][17]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][17]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][18]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][18]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][19]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][19]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][3]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][3]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][0]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS2|DLY1|sram|segment[0][0]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][2]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][2]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][1]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][1]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][0]_WADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ & \U_Filter|wadr_counter|wysi_counter|sload_path[0]\);

\U_Filter|DS1|DLY1|sram|segment[0][0]_RADDR_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \U_Filter|add~47\ & \U_Filter|add~46\ & \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ & 
\U_Filter|wadr_counter|wysi_counter|sload_path[0]\);
ALT_INV_FD_STATE <= NOT FD_STATE;

\TO_Cnt[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \TO_Cnt[0]\ = DFFE(GLOBAL(FD_STATE) & \TO_Cnt[0]\ $ !\Equal~120\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \TO_Cnt[0]~46\ = CARRY(\TO_Cnt[0]\ & !\Equal~120\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9922",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \TO_Cnt[0]\,
	datab => \Equal~120\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => ALT_INV_FD_STATE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TO_Cnt[0]\,
	cout => \TO_Cnt[0]~46\);

\ROI_DEF~55_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_DEF~55\ = \CPeak_FF|dffs[1]\ & (\CPeak_FF|dffs[0]\) # !\CPeak_FF|dffs[1]\ & (\CPeak_FF|dffs[0]\ & (\ROI_RAM|sram|q[9]\) # !\CPeak_FF|dffs[0]\ & \ROI_RAM|sram|q[8]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F2C2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_RAM|sram|q[8]\,
	datab => \CPeak_FF|dffs[1]\,
	datac => \CPeak_FF|dffs[0]\,
	datad => \ROI_RAM|sram|q[9]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_DEF~55\);

\ROI_DEF~56_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_DEF~56\ = \CPeak_FF|dffs[1]\ & (\ROI_DEF~55\ & \ROI_RAM|sram|q[11]\ # !\ROI_DEF~55\ & (\ROI_RAM|sram|q[10]\)) # !\CPeak_FF|dffs[1]\ & (\ROI_DEF~55\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DDA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \CPeak_FF|dffs[1]\,
	datab => \ROI_RAM|sram|q[11]\,
	datac => \ROI_RAM|sram|q[10]\,
	datad => \ROI_DEF~55\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_DEF~56\);

\PSlope_Gen_Cmp_Del~I\ : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Cmp_Del = DFFE(\PSlope_Compare|comparator|sub_comptree|gt_cmp_end|agb_out\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Gen_Cmp_Del);

\U_Filter|DS2|Acc_ff|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Acc_ff|dffs[3]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Acc_ff|dffs[3]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~509\ $ \U_Filter|DS2|Acc_ff|dffs[2]~70\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Acc_ff|dffs[3]~67\ = CARRY(\U_Filter|DS2|Acc_ff|dffs[3]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~509\ & !\U_Filter|DS2|Acc_ff|dffs[2]~70\ # !\U_Filter|DS2|Acc_ff|dffs[3]\ & (!\U_Filter|DS2|Acc_ff|dffs[2]~70\ # 
-- !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~509\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Acc_ff|dffs[3]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~509\,
	cin => \U_Filter|DS2|Acc_ff|dffs[2]~70\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Acc_ff|dffs[3]\,
	cout => \U_Filter|DS2|Acc_ff|dffs[3]~67\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[5]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[5]\ = DFFE((\Max_Clamp_Ld~4\ & \~GND\) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Compare|comparator|cmp_end|lcarry[5]~59\ $ (\Max_Clamp_Inc~1\ & \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[4]~COUT\)), GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )
-- \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[5]~COUT\ = CARRY(!\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[4]~COUT\ # !\Max_Clamp_Compare|comparator|cmp_end|lcarry[5]~59\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Inc~1\,
	datab => \Max_Clamp_Compare|comparator|cmp_end|lcarry[5]~59\,
	datac => \~GND\,
	cin => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[4]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[5]\,
	cout => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[5]~COUT\);

\blevel_cnt[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_cnt[1]\ = DFFE(\add~188\ & (FD_STATE # !\Equal~126\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \add~188\,
	datac => FD_STATE,
	datad => \Equal~126\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_cnt[1]\);

\blevel_cnt[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_cnt[6]\ = DFFE(\add~204\ & (FD_STATE # !\Equal~126\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \add~204\,
	datac => FD_STATE,
	datad => \Equal~126\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_cnt[6]\);

\PSlope_Compare|comparator|cmp[3]|comp|sub_comptree|eq_cmp_end|aeb_out~I\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[3]|comp|sub_comptree|eq_cmp_end|aeb_out\ = DFFE(\PSlope_Compare|comparator|cmp[3]|comp|cmp[0]|aeb_out\ & (\PSlope_Compare|comparator|cmp[3]|comp|cmp[1]|aeb_out\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \PSlope_Compare|comparator|cmp[3]|comp|cmp[0]|aeb_out\,
	datad => \PSlope_Compare|comparator|cmp[3]|comp|cmp[1]|aeb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \PSlope_Compare|comparator|cmp[3]|comp|sub_comptree|eq_cmp_end|aeb_out\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[4]\ = DFFE(\U_Filter|acc1_del_ff|dffs[4]\ $ \U_Filter|DS1|Dout_ff|dffs[4]\ $ \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[4]\ = CARRY(\U_Filter|acc1_del_ff|dffs[4]\ & \U_Filter|DS1|Dout_ff|dffs[4]\ & !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[3]\ # !\U_Filter|acc1_del_ff|dffs[4]\ & (\U_Filter|DS1|Dout_ff|dffs[4]\ # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[3]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|acc1_del_ff|dffs[4]\,
	datab => \U_Filter|DS1|Dout_ff|dffs[4]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[4]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[4]\);

\U_Filter|DS2|Acc_ff|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Acc_ff|dffs[2]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Acc_ff|dffs[2]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~513\ $ !\U_Filter|DS2|Acc_ff|dffs[1]~73\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Acc_ff|dffs[2]~70\ = CARRY(\U_Filter|DS2|Acc_ff|dffs[2]\ & (\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~513\ # !\U_Filter|DS2|Acc_ff|dffs[1]~73\) # !\U_Filter|DS2|Acc_ff|dffs[2]\ & 
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~513\ & !\U_Filter|DS2|Acc_ff|dffs[1]~73\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Acc_ff|dffs[2]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~513\,
	cin => \U_Filter|DS2|Acc_ff|dffs[1]~73\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Acc_ff|dffs[2]\,
	cout => \U_Filter|DS2|Acc_ff|dffs[2]~70\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[5]\ = DFFE(\U_Filter|acc1_del_ff|dffs[5]\ $ \U_Filter|DS1|Dout_ff|dffs[5]\ $ !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[5]\ = CARRY(\U_Filter|acc1_del_ff|dffs[5]\ & (!\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[4]\ # !\U_Filter|DS1|Dout_ff|dffs[5]\) # !\U_Filter|acc1_del_ff|dffs[5]\ & !\U_Filter|DS1|Dout_ff|dffs[5]\ & 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[4]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|acc1_del_ff|dffs[5]\,
	datab => \U_Filter|DS1|Dout_ff|dffs[5]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[5]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[5]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[6]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[6]\ $ \U_Filter|acc1_del_ff|dffs[6]\ $ \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[6]\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[6]\ & (!\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[5]\ # !\U_Filter|acc1_del_ff|dffs[6]\) # !\U_Filter|DS1|Dout_ff|dffs[6]\ & !\U_Filter|acc1_del_ff|dffs[6]\ & 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[5]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[6]\,
	datab => \U_Filter|acc1_del_ff|dffs[6]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[6]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[6]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[7]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[7]\ $ \U_Filter|acc1_del_ff|dffs[7]\ $ !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[7]\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[7]\ & \U_Filter|acc1_del_ff|dffs[7]\ & !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[6]\ # !\U_Filter|DS1|Dout_ff|dffs[7]\ & (\U_Filter|acc1_del_ff|dffs[7]\ # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[6]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[7]\,
	datab => \U_Filter|acc1_del_ff|dffs[7]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[7]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[7]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[8]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[8]\ $ \U_Filter|acc1_del_ff|dffs[8]\ $ \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[8]\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[8]\ & (!\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[7]\ # !\U_Filter|acc1_del_ff|dffs[8]\) # !\U_Filter|DS1|Dout_ff|dffs[8]\ & !\U_Filter|acc1_del_ff|dffs[8]\ & 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[7]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[8]\,
	datab => \U_Filter|acc1_del_ff|dffs[8]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[8]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[8]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[9]\ = DFFE(\U_Filter|acc1_del_ff|dffs[9]\ $ \U_Filter|DS1|Dout_ff|dffs[9]\ $ !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[9]\ = CARRY(\U_Filter|acc1_del_ff|dffs[9]\ & (!\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[8]\ # !\U_Filter|DS1|Dout_ff|dffs[9]\) # !\U_Filter|acc1_del_ff|dffs[9]\ & !\U_Filter|DS1|Dout_ff|dffs[9]\ & 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[8]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|acc1_del_ff|dffs[9]\,
	datab => \U_Filter|DS1|Dout_ff|dffs[9]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[8]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[9]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[9]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[10]\ = DFFE(\U_Filter|acc1_del_ff|dffs[10]\ $ \U_Filter|DS1|Dout_ff|dffs[10]\ $ \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[9]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[10]\ = CARRY(\U_Filter|acc1_del_ff|dffs[10]\ & \U_Filter|DS1|Dout_ff|dffs[10]\ & !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[9]\ # !\U_Filter|acc1_del_ff|dffs[10]\ & (\U_Filter|DS1|Dout_ff|dffs[10]\ # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[9]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|acc1_del_ff|dffs[10]\,
	datab => \U_Filter|DS1|Dout_ff|dffs[10]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[10]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[10]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[11]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[11]\ $ \U_Filter|acc1_del_ff|dffs[11]\ $ !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[10]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[11]\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[11]\ & \U_Filter|acc1_del_ff|dffs[11]\ & !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[10]\ # !\U_Filter|DS1|Dout_ff|dffs[11]\ & (\U_Filter|acc1_del_ff|dffs[11]\ 
-- # !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[10]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[11]\,
	datab => \U_Filter|acc1_del_ff|dffs[11]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[10]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[11]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[11]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[12]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[12]\ $ \U_Filter|acc1_del_ff|dffs[12]\ $ \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[12]\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[12]\ & (!\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[11]\ # !\U_Filter|acc1_del_ff|dffs[12]\) # !\U_Filter|DS1|Dout_ff|dffs[12]\ & 
-- !\U_Filter|acc1_del_ff|dffs[12]\ & !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[11]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[12]\,
	datab => \U_Filter|acc1_del_ff|dffs[12]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[11]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[12]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[12]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[13]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[13]\ $ \U_Filter|acc1_del_ff|dffs[13]\ $ !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[12]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[13]\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[13]\ & \U_Filter|acc1_del_ff|dffs[13]\ & !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[12]\ # !\U_Filter|DS1|Dout_ff|dffs[13]\ & (\U_Filter|acc1_del_ff|dffs[13]\ 
-- # !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[12]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[13]\,
	datab => \U_Filter|acc1_del_ff|dffs[13]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[12]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[13]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[13]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[14]\ = DFFE(\U_Filter|acc1_del_ff|dffs[14]\ $ \U_Filter|DS1|Dout_ff|dffs[14]\ $ \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[13]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[14]\ = CARRY(\U_Filter|acc1_del_ff|dffs[14]\ & \U_Filter|DS1|Dout_ff|dffs[14]\ & !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[13]\ # !\U_Filter|acc1_del_ff|dffs[14]\ & (\U_Filter|DS1|Dout_ff|dffs[14]\ 
-- # !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[13]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|acc1_del_ff|dffs[14]\,
	datab => \U_Filter|DS1|Dout_ff|dffs[14]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[13]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[14]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[14]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[15]\ = DFFE(\U_Filter|acc1_del_ff|dffs[15]\ $ \U_Filter|DS1|Dout_ff|dffs[15]\ $ !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[14]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[15]\ = CARRY(\U_Filter|acc1_del_ff|dffs[15]\ & (!\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[14]\ # !\U_Filter|DS1|Dout_ff|dffs[15]\) # !\U_Filter|acc1_del_ff|dffs[15]\ & 
-- !\U_Filter|DS1|Dout_ff|dffs[15]\ & !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[14]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|acc1_del_ff|dffs[15]\,
	datab => \U_Filter|DS1|Dout_ff|dffs[15]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[14]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[15]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[15]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[16]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[16]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[16]\ $ \U_Filter|acc1_del_ff|dffs[16]\ $ \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[15]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[16]\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[16]\ & (!\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[15]\ # !\U_Filter|acc1_del_ff|dffs[16]\) # !\U_Filter|DS1|Dout_ff|dffs[16]\ & 
-- !\U_Filter|acc1_del_ff|dffs[16]\ & !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[15]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[16]\,
	datab => \U_Filter|acc1_del_ff|dffs[16]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[15]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[16]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[16]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[17]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[17]\ = DFFE(\U_Filter|acc1_del_ff|dffs[17]\ $ \U_Filter|DS1|Dout_ff|dffs[17]\ $ !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[16]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[17]\ = CARRY(\U_Filter|acc1_del_ff|dffs[17]\ & (!\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[16]\ # !\U_Filter|DS1|Dout_ff|dffs[17]\) # !\U_Filter|acc1_del_ff|dffs[17]\ & 
-- !\U_Filter|DS1|Dout_ff|dffs[17]\ & !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[16]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|acc1_del_ff|dffs[17]\,
	datab => \U_Filter|DS1|Dout_ff|dffs[17]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[16]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[17]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[17]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[18]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[18]\ = DFFE(\U_Filter|acc1_del_ff|dffs[18]\ $ \U_Filter|DS1|Dout_ff|dffs[18]\ $ \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[17]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[18]\ = CARRY(\U_Filter|acc1_del_ff|dffs[18]\ & \U_Filter|DS1|Dout_ff|dffs[18]\ & !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[17]\ # !\U_Filter|acc1_del_ff|dffs[18]\ & (\U_Filter|DS1|Dout_ff|dffs[18]\ 
-- # !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[17]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|acc1_del_ff|dffs[18]\,
	datab => \U_Filter|DS1|Dout_ff|dffs[18]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[17]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[18]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[18]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[19]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[19]\ = DFFE(\U_Filter|acc1_del_ff|dffs[19]\ $ (\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[18]\ $ !\U_Filter|DS1|Dout_ff|dffs[19]\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AA5",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|acc1_del_ff|dffs[19]\,
	datad => \U_Filter|DS1|Dout_ff|dffs[19]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[18]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[19]\);

\add~188_I\ : apex20ke_lcell
-- Equation(s):
-- \add~188\ = \blevel_cnt[1]\ $ (\add~194\)
-- \add~190\ = CARRY(!\add~194\ # !\blevel_cnt[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \blevel_cnt[1]\,
	cin => \add~194\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~188\,
	cout => \add~190\);

\add~204_I\ : apex20ke_lcell
-- Equation(s):
-- \add~204\ = \blevel_cnt[6]\ $ (!\add~210\)
-- \add~206\ = CARRY(\blevel_cnt[6]\ & (!\add~210\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \blevel_cnt[6]\,
	cin => \add~210\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~204\,
	cout => \add~206\);

\PSlope_Compare|comparator|cmp[2]|comp|sub_comptree|eq_cmp_end|aeb_out~I\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[2]|comp|sub_comptree|eq_cmp_end|aeb_out\ = DFFE(\PSlope_Compare|comparator|cmp[2]|comp|cmp[0]|aeb_out\ & \PSlope_Compare|comparator|cmp[2]|comp|cmp[1]|aeb_out\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \PSlope_Compare|comparator|cmp[2]|comp|cmp[0]|aeb_out\,
	datad => \PSlope_Compare|comparator|cmp[2]|comp|cmp[1]|aeb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \PSlope_Compare|comparator|cmp[2]|comp|sub_comptree|eq_cmp_end|aeb_out\);

\tlevel_reg_ff|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[14]\ = DFFE(\Tlevel[14]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Tlevel[14]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[14]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[3]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[3]\ $ \U_Filter|acc1_del_ff|dffs[3]\ $ !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[3]\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[3]\ & \U_Filter|acc1_del_ff|dffs[3]\ & !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[2]\ # !\U_Filter|DS1|Dout_ff|dffs[3]\ & (\U_Filter|acc1_del_ff|dffs[3]\ # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[2]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[3]\,
	datab => \U_Filter|acc1_del_ff|dffs[3]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[3]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[3]\);

\U_Filter|DS1|Dout_ff|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[4]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[4]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~445\ $ !\U_Filter|DS1|Dout_ff|dffs[3]~337\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[4]~289\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[4]\ & (\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~445\ # !\U_Filter|DS1|Dout_ff|dffs[3]~337\) # !\U_Filter|DS1|Dout_ff|dffs[4]\ & 
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~445\ & !\U_Filter|DS1|Dout_ff|dffs[3]~337\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[4]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~445\,
	cin => \U_Filter|DS1|Dout_ff|dffs[3]~337\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[4]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[4]~289\);

\U_Filter|acc1_del_ff|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[4]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[4]\);

\U_Filter|DS2|Acc_ff|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Acc_ff|dffs[1]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Acc_ff|dffs[1]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~517\ $ \U_Filter|DS2|Acc_ff|dffs[0]~76\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Acc_ff|dffs[1]~73\ = CARRY(\U_Filter|DS2|Acc_ff|dffs[1]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~517\ & !\U_Filter|DS2|Acc_ff|dffs[0]~76\ # !\U_Filter|DS2|Acc_ff|dffs[1]\ & (!\U_Filter|DS2|Acc_ff|dffs[0]~76\ # 
-- !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~517\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Acc_ff|dffs[1]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~517\,
	cin => \U_Filter|DS2|Acc_ff|dffs[0]~76\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Acc_ff|dffs[1]\,
	cout => \U_Filter|DS2|Acc_ff|dffs[1]~73\);

\U_Filter|DS1|Dout_ff|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[5]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[5]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~449\ $ \U_Filter|DS1|Dout_ff|dffs[4]~289\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[5]~292\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[5]\ & !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~449\ & !\U_Filter|DS1|Dout_ff|dffs[4]~289\ # !\U_Filter|DS1|Dout_ff|dffs[5]\ & (!\U_Filter|DS1|Dout_ff|dffs[4]~289\ # 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~449\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[5]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~449\,
	cin => \U_Filter|DS1|Dout_ff|dffs[4]~289\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[5]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[5]~292\);

\U_Filter|acc1_del_ff|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[5]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[5]\);

\U_Filter|DS1|Dout_ff|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[6]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[6]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~453\ $ !\U_Filter|DS1|Dout_ff|dffs[5]~292\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[6]~295\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[6]\ & (\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~453\ # !\U_Filter|DS1|Dout_ff|dffs[5]~292\) # !\U_Filter|DS1|Dout_ff|dffs[6]\ & 
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~453\ & !\U_Filter|DS1|Dout_ff|dffs[5]~292\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[6]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~453\,
	cin => \U_Filter|DS1|Dout_ff|dffs[5]~292\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[6]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[6]~295\);

\U_Filter|acc1_del_ff|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[6]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[6]\);

\U_Filter|DS1|Dout_ff|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[7]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[7]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~457\ $ \U_Filter|DS1|Dout_ff|dffs[6]~295\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[7]~298\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[7]\ & !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~457\ & !\U_Filter|DS1|Dout_ff|dffs[6]~295\ # !\U_Filter|DS1|Dout_ff|dffs[7]\ & (!\U_Filter|DS1|Dout_ff|dffs[6]~295\ # 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~457\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[7]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~457\,
	cin => \U_Filter|DS1|Dout_ff|dffs[6]~295\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[7]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[7]~298\);

\U_Filter|acc1_del_ff|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[7]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[7]\);

\U_Filter|DS1|Dout_ff|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[8]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[8]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~461\ $ !\U_Filter|DS1|Dout_ff|dffs[7]~298\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[8]~301\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[8]\ & (\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~461\ # !\U_Filter|DS1|Dout_ff|dffs[7]~298\) # !\U_Filter|DS1|Dout_ff|dffs[8]\ & 
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~461\ & !\U_Filter|DS1|Dout_ff|dffs[7]~298\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[8]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~461\,
	cin => \U_Filter|DS1|Dout_ff|dffs[7]~298\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[8]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[8]~301\);

\U_Filter|acc1_del_ff|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[8]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[8]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[8]\);

\U_Filter|DS1|Dout_ff|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[9]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[9]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~465\ $ \U_Filter|DS1|Dout_ff|dffs[8]~301\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[9]~304\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[9]\ & !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~465\ & !\U_Filter|DS1|Dout_ff|dffs[8]~301\ # !\U_Filter|DS1|Dout_ff|dffs[9]\ & (!\U_Filter|DS1|Dout_ff|dffs[8]~301\ # 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~465\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[9]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~465\,
	cin => \U_Filter|DS1|Dout_ff|dffs[8]~301\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[9]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[9]~304\);

\U_Filter|acc1_del_ff|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[9]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[9]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[9]\);

\U_Filter|DS1|Dout_ff|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[10]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[10]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~469\ $ !\U_Filter|DS1|Dout_ff|dffs[9]~304\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[10]~307\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[10]\ & (\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~469\ # !\U_Filter|DS1|Dout_ff|dffs[9]~304\) # !\U_Filter|DS1|Dout_ff|dffs[10]\ & 
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~469\ & !\U_Filter|DS1|Dout_ff|dffs[9]~304\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[10]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~469\,
	cin => \U_Filter|DS1|Dout_ff|dffs[9]~304\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[10]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[10]~307\);

\U_Filter|acc1_del_ff|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[10]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[10]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[10]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[10]\);

\U_Filter|DS1|Dout_ff|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[11]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[11]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~473\ $ \U_Filter|DS1|Dout_ff|dffs[10]~307\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[11]~310\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[11]\ & !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~473\ & !\U_Filter|DS1|Dout_ff|dffs[10]~307\ # !\U_Filter|DS1|Dout_ff|dffs[11]\ & (!\U_Filter|DS1|Dout_ff|dffs[10]~307\ # 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~473\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[11]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~473\,
	cin => \U_Filter|DS1|Dout_ff|dffs[10]~307\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[11]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[11]~310\);

\U_Filter|acc1_del_ff|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[11]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[11]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[11]\);

\U_Filter|DS1|Dout_ff|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[12]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[12]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~477\ $ !\U_Filter|DS1|Dout_ff|dffs[11]~310\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[12]~313\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[12]\ & (\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~477\ # !\U_Filter|DS1|Dout_ff|dffs[11]~310\) # !\U_Filter|DS1|Dout_ff|dffs[12]\ & 
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~477\ & !\U_Filter|DS1|Dout_ff|dffs[11]~310\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[12]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~477\,
	cin => \U_Filter|DS1|Dout_ff|dffs[11]~310\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[12]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[12]~313\);

\U_Filter|acc1_del_ff|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[12]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[12]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[12]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[12]\);

\U_Filter|DS1|Dout_ff|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[13]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[13]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~481\ $ \U_Filter|DS1|Dout_ff|dffs[12]~313\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[13]~316\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[13]\ & !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~481\ & !\U_Filter|DS1|Dout_ff|dffs[12]~313\ # !\U_Filter|DS1|Dout_ff|dffs[13]\ & (!\U_Filter|DS1|Dout_ff|dffs[12]~313\ # 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~481\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[13]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~481\,
	cin => \U_Filter|DS1|Dout_ff|dffs[12]~313\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[13]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[13]~316\);

\U_Filter|acc1_del_ff|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[13]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[13]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[13]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[13]\);

\U_Filter|DS1|Dout_ff|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[14]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[14]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~485\ $ !\U_Filter|DS1|Dout_ff|dffs[13]~316\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[14]~319\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[14]\ & (\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~485\ # !\U_Filter|DS1|Dout_ff|dffs[13]~316\) # !\U_Filter|DS1|Dout_ff|dffs[14]\ & 
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~485\ & !\U_Filter|DS1|Dout_ff|dffs[13]~316\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[14]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~485\,
	cin => \U_Filter|DS1|Dout_ff|dffs[13]~316\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[14]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[14]~319\);

\U_Filter|acc1_del_ff|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[14]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[14]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[14]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[14]\);

\U_Filter|DS1|Dout_ff|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[15]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[15]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~489\ $ \U_Filter|DS1|Dout_ff|dffs[14]~319\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[15]~322\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[15]\ & !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~489\ & !\U_Filter|DS1|Dout_ff|dffs[14]~319\ # !\U_Filter|DS1|Dout_ff|dffs[15]\ & (!\U_Filter|DS1|Dout_ff|dffs[14]~319\ # 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~489\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[15]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~489\,
	cin => \U_Filter|DS1|Dout_ff|dffs[14]~319\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[15]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[15]~322\);

\U_Filter|acc1_del_ff|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[15]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[15]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[15]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[15]\);

\U_Filter|DS1|Dout_ff|dffs[16]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[16]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[16]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~493\ $ !\U_Filter|DS1|Dout_ff|dffs[15]~322\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[16]~325\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[16]\ & (\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~493\ # !\U_Filter|DS1|Dout_ff|dffs[15]~322\) # !\U_Filter|DS1|Dout_ff|dffs[16]\ & 
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~493\ & !\U_Filter|DS1|Dout_ff|dffs[15]~322\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[16]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~493\,
	cin => \U_Filter|DS1|Dout_ff|dffs[15]~322\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[16]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[16]~325\);

\U_Filter|acc1_del_ff|dffs[16]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[16]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[16]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[16]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[16]\);

\U_Filter|DS1|Dout_ff|dffs[17]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[17]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[17]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~497\ $ \U_Filter|DS1|Dout_ff|dffs[16]~325\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[17]~328\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[17]\ & !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~497\ & !\U_Filter|DS1|Dout_ff|dffs[16]~325\ # !\U_Filter|DS1|Dout_ff|dffs[17]\ & (!\U_Filter|DS1|Dout_ff|dffs[16]~325\ # 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~497\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[17]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~497\,
	cin => \U_Filter|DS1|Dout_ff|dffs[16]~325\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[17]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[17]~328\);

\U_Filter|acc1_del_ff|dffs[17]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[17]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[17]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[17]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[17]\);

\U_Filter|DS1|Dout_ff|dffs[18]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[18]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[18]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~501\ $ !\U_Filter|DS1|Dout_ff|dffs[17]~328\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[18]~331\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[18]\ & (\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~501\ # !\U_Filter|DS1|Dout_ff|dffs[17]~328\) # !\U_Filter|DS1|Dout_ff|dffs[18]\ & 
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~501\ & !\U_Filter|DS1|Dout_ff|dffs[17]~328\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[18]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~501\,
	cin => \U_Filter|DS1|Dout_ff|dffs[17]~328\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[18]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[18]~331\);

\U_Filter|acc1_del_ff|dffs[18]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[18]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[18]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[18]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[18]\);

\U_Filter|DS1|Dout_ff|dffs[19]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[19]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[19]\ $ (\U_Filter|DS1|Dout_ff|dffs[18]~331\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~505\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[19]\,
	datad => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~505\,
	cin => \U_Filter|DS1|Dout_ff|dffs[18]~331\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[19]\);

\U_Filter|acc1_del_ff|dffs[19]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[19]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[19]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[19]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[19]\);

\clock_proc~2_I\ : apex20ke_lcell
-- Equation(s):
-- \clock_proc~2\ = FD_STATE # \Equal~126\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => FD_STATE,
	datad => \Equal~126\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \clock_proc~2\);

\Dout_Del_ff|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[14]\ = DFFE(\U_Filter|Dout_FF|dffs[14]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|Dout_FF|dffs[14]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[14]\);

\Dout_Del_ff|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[12]\ = DFFE(\U_Filter|Dout_FF|dffs[12]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|Dout_FF|dffs[12]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[12]\);

\Dout_Del_ff|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[13]\ = DFFE(\U_Filter|Dout_FF|dffs[13]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|Dout_FF|dffs[13]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[13]\);

\PSlope_Compare|comparator|cmp[3]|comp|cmp[0]|agb_out~0\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[3]|comp|cmp[0]|agb_out\ = \U_Filter|Dout_FF|dffs[13]\ & (!\Dout_Del_ff|dffs[12]\ & \U_Filter|Dout_FF|dffs[12]\ # !\Dout_Del_ff|dffs[13]\) # !\U_Filter|Dout_FF|dffs[13]\ & !\Dout_Del_ff|dffs[12]\ & !\Dout_Del_ff|dffs[13]\ & 
-- \U_Filter|Dout_FF|dffs[12]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4D0C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Del_ff|dffs[12]\,
	datab => \U_Filter|Dout_FF|dffs[13]\,
	datac => \Dout_Del_ff|dffs[13]\,
	datad => \U_Filter|Dout_FF|dffs[12]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[3]|comp|cmp[0]|agb_out\);

\PSlope_Compare|comparator|cmp[1]|comp|sub_comptree|eq_cmp_end|aeb_out~I\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[1]|comp|sub_comptree|eq_cmp_end|aeb_out\ = DFFE(\PSlope_Compare|comparator|cmp[1]|comp|cmp[0]|aeb_out\ & \PSlope_Compare|comparator|cmp[1]|comp|cmp[1]|aeb_out\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \PSlope_Compare|comparator|cmp[1]|comp|cmp[0]|aeb_out\,
	datad => \PSlope_Compare|comparator|cmp[1]|comp|cmp[1]|aeb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \PSlope_Compare|comparator|cmp[1]|comp|sub_comptree|eq_cmp_end|aeb_out\);

\PSlope_Compare|comparator|cmp[3]|comp|cmp[0]|aeb_out~0\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[3]|comp|cmp[0]|aeb_out\ = \Dout_Del_ff|dffs[12]\ & \U_Filter|Dout_FF|dffs[12]\ & (\Dout_Del_ff|dffs[13]\ $ !\U_Filter|Dout_FF|dffs[13]\) # !\Dout_Del_ff|dffs[12]\ & !\U_Filter|Dout_FF|dffs[12]\ & (\Dout_Del_ff|dffs[13]\ $ 
-- !\U_Filter|Dout_FF|dffs[13]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Del_ff|dffs[12]\,
	datab => \U_Filter|Dout_FF|dffs[12]\,
	datac => \Dout_Del_ff|dffs[13]\,
	datad => \U_Filter|Dout_FF|dffs[13]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[3]|comp|cmp[0]|aeb_out\);

\tlevel_reg_ff|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[13]\ = DFFE(\Tlevel[13]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Tlevel[13]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[13]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[2]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[2]\ $ \U_Filter|acc1_del_ff|dffs[2]\ $ \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[2]\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[2]\ & (!\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[1]\ # !\U_Filter|acc1_del_ff|dffs[2]\) # !\U_Filter|DS1|Dout_ff|dffs[2]\ & !\U_Filter|acc1_del_ff|dffs[2]\ & 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[2]\,
	datab => \U_Filter|acc1_del_ff|dffs[2]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[2]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[2]\);

\U_Filter|DS1|Dout_ff|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[3]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[3]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~509\ $ \U_Filter|DS1|Dout_ff|dffs[2]~340\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[3]~337\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[3]\ & !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~509\ & !\U_Filter|DS1|Dout_ff|dffs[2]~340\ # !\U_Filter|DS1|Dout_ff|dffs[3]\ & (!\U_Filter|DS1|Dout_ff|dffs[2]~340\ # 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~509\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[3]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~509\,
	cin => \U_Filter|DS1|Dout_ff|dffs[2]~340\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[3]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[3]~337\);

\U_Filter|acc1_del_ff|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[3]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[3]\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~445_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~445\ = \Din[4]~combout\ $ \U_Filter|DS1|DLY1|sram|q[4]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~511\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~447\ = CARRY(\Din[4]~combout\ & (!\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~511\ # !\U_Filter|DS1|DLY1|sram|q[4]\) # !\Din[4]~combout\ & !\U_Filter|DS1|DLY1|sram|q[4]\ & 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~511\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din[4]~combout\,
	datab => \U_Filter|DS1|DLY1|sram|q[4]\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~511\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~445\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~447\);

\U_Filter|DS2|Acc_ff|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Acc_ff|dffs[0]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Acc_ff|dffs[0]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~521\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Acc_ff|dffs[0]~76\ = CARRY(\U_Filter|DS2|Acc_ff|dffs[0]\ & \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~521\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Acc_ff|dffs[0]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~521\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Acc_ff|dffs[0]\,
	cout => \U_Filter|DS2|Acc_ff|dffs[0]~76\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~449_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~449\ = \U_Filter|DS1|DLY1|sram|q[5]\ $ \Din[5]~combout\ $ !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~447\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~451\ = CARRY(\U_Filter|DS1|DLY1|sram|q[5]\ & (!\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~447\ # !\Din[5]~combout\) # !\U_Filter|DS1|DLY1|sram|q[5]\ & !\Din[5]~combout\ & 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~447\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|DLY1|sram|q[5]\,
	datab => \Din[5]~combout\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~447\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~449\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~451\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~453_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~453\ = \U_Filter|DS1|DLY1|sram|q[6]\ $ \Din[6]~combout\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~451\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~455\ = CARRY(\U_Filter|DS1|DLY1|sram|q[6]\ & \Din[6]~combout\ & !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~451\ # !\U_Filter|DS1|DLY1|sram|q[6]\ & (\Din[6]~combout\ # 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~451\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|DLY1|sram|q[6]\,
	datab => \Din[6]~combout\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~451\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~453\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~455\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~457_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~457\ = \U_Filter|DS1|DLY1|sram|q[7]\ $ \Din[7]~combout\ $ !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~455\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~459\ = CARRY(\U_Filter|DS1|DLY1|sram|q[7]\ & (!\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~455\ # !\Din[7]~combout\) # !\U_Filter|DS1|DLY1|sram|q[7]\ & !\Din[7]~combout\ & 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~455\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|DLY1|sram|q[7]\,
	datab => \Din[7]~combout\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~455\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~457\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~459\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~461_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~461\ = \Din[8]~combout\ $ \U_Filter|DS1|DLY1|sram|q[8]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~459\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~463\ = CARRY(\Din[8]~combout\ & (!\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~459\ # !\U_Filter|DS1|DLY1|sram|q[8]\) # !\Din[8]~combout\ & !\U_Filter|DS1|DLY1|sram|q[8]\ & 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~459\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din[8]~combout\,
	datab => \U_Filter|DS1|DLY1|sram|q[8]\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~459\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~461\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~463\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~465_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~465\ = \Din[9]~combout\ $ \U_Filter|DS1|DLY1|sram|q[9]\ $ !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~463\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~467\ = CARRY(\Din[9]~combout\ & \U_Filter|DS1|DLY1|sram|q[9]\ & !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~463\ # !\Din[9]~combout\ & (\U_Filter|DS1|DLY1|sram|q[9]\ # 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~463\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din[9]~combout\,
	datab => \U_Filter|DS1|DLY1|sram|q[9]\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~463\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~465\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~467\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~469_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~469\ = \Din[10]~combout\ $ \U_Filter|DS1|DLY1|sram|q[10]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~467\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~471\ = CARRY(\Din[10]~combout\ & (!\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~467\ # !\U_Filter|DS1|DLY1|sram|q[10]\) # !\Din[10]~combout\ & !\U_Filter|DS1|DLY1|sram|q[10]\ & 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~467\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din[10]~combout\,
	datab => \U_Filter|DS1|DLY1|sram|q[10]\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~467\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~469\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~471\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~473_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~473\ = \Din[11]~combout\ $ \U_Filter|DS1|DLY1|sram|q[11]\ $ !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~471\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~475\ = CARRY(\Din[11]~combout\ & \U_Filter|DS1|DLY1|sram|q[11]\ & !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~471\ # !\Din[11]~combout\ & (\U_Filter|DS1|DLY1|sram|q[11]\ # 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~471\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din[11]~combout\,
	datab => \U_Filter|DS1|DLY1|sram|q[11]\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~471\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~473\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~475\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~477_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~477\ = \Din[12]~combout\ $ \U_Filter|DS1|DLY1|sram|q[12]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~475\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~479\ = CARRY(\Din[12]~combout\ & (!\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~475\ # !\U_Filter|DS1|DLY1|sram|q[12]\) # !\Din[12]~combout\ & !\U_Filter|DS1|DLY1|sram|q[12]\ & 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~475\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din[12]~combout\,
	datab => \U_Filter|DS1|DLY1|sram|q[12]\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~475\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~477\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~479\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~481_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~481\ = \Din[13]~combout\ $ \U_Filter|DS1|DLY1|sram|q[13]\ $ !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~479\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~483\ = CARRY(\Din[13]~combout\ & \U_Filter|DS1|DLY1|sram|q[13]\ & !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~479\ # !\Din[13]~combout\ & (\U_Filter|DS1|DLY1|sram|q[13]\ # 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~479\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din[13]~combout\,
	datab => \U_Filter|DS1|DLY1|sram|q[13]\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~479\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~481\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~483\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~485_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~485\ = \Din[14]~combout\ $ \U_Filter|DS1|DLY1|sram|q[14]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~483\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~487\ = CARRY(\Din[14]~combout\ & (!\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~483\ # !\U_Filter|DS1|DLY1|sram|q[14]\) # !\Din[14]~combout\ & !\U_Filter|DS1|DLY1|sram|q[14]\ & 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~483\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din[14]~combout\,
	datab => \U_Filter|DS1|DLY1|sram|q[14]\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~483\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~485\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~487\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~489_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~489\ = \Din[15]~combout\ $ \U_Filter|DS1|DLY1|sram|q[15]\ $ !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~487\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~491\ = CARRY(\Din[15]~combout\ & \U_Filter|DS1|DLY1|sram|q[15]\ & !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~487\ # !\Din[15]~combout\ & (\U_Filter|DS1|DLY1|sram|q[15]\ # 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~487\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din[15]~combout\,
	datab => \U_Filter|DS1|DLY1|sram|q[15]\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~487\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~489\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~491\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~493_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~493\ = \Din[15]~combout\ $ \U_Filter|DS1|DLY1|sram|q[16]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~491\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~495\ = CARRY(\Din[15]~combout\ & (!\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~491\ # !\U_Filter|DS1|DLY1|sram|q[16]\) # !\Din[15]~combout\ & !\U_Filter|DS1|DLY1|sram|q[16]\ & 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~491\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din[15]~combout\,
	datab => \U_Filter|DS1|DLY1|sram|q[16]\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~491\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~493\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~495\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~497_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~497\ = \Din[15]~combout\ $ \U_Filter|DS1|DLY1|sram|q[17]\ $ !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~495\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~499\ = CARRY(\Din[15]~combout\ & \U_Filter|DS1|DLY1|sram|q[17]\ & !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~495\ # !\Din[15]~combout\ & (\U_Filter|DS1|DLY1|sram|q[17]\ # 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~495\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din[15]~combout\,
	datab => \U_Filter|DS1|DLY1|sram|q[17]\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~495\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~497\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~499\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~501_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~501\ = \Din[15]~combout\ $ \U_Filter|DS1|DLY1|sram|q[18]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~499\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~503\ = CARRY(\Din[15]~combout\ & (!\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~499\ # !\U_Filter|DS1|DLY1|sram|q[18]\) # !\Din[15]~combout\ & !\U_Filter|DS1|DLY1|sram|q[18]\ & 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~499\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din[15]~combout\,
	datab => \U_Filter|DS1|DLY1|sram|q[18]\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~499\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~501\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~503\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~505_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~505\ = \Din[15]~combout\ $ (\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~503\ $ !\U_Filter|DS1|DLY1|sram|q[19]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AA5",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din[15]~combout\,
	datad => \U_Filter|DS1|DLY1|sram|q[19]\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~503\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~505\);

\Dout_Del_ff|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[11]\ = DFFE(\U_Filter|Dout_FF|dffs[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|Dout_FF|dffs[11]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[11]\);

\Dout_Del_ff|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[9]\ = DFFE(\U_Filter|Dout_FF|dffs[9]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|Dout_FF|dffs[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[9]\);

\PSlope_Compare|comparator|cmp[2]|comp|cmp[0]|aeb_out~0\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[2]|comp|cmp[0]|aeb_out\ = \Dout_Del_ff|dffs[9]\ & \U_Filter|Dout_FF|dffs[9]\ & (\U_Filter|Dout_FF|dffs[8]\ $ !\Dout_Del_ff|dffs[8]\) # !\Dout_Del_ff|dffs[9]\ & !\U_Filter|Dout_FF|dffs[9]\ & (\U_Filter|Dout_FF|dffs[8]\ $ 
-- !\Dout_Del_ff|dffs[8]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Del_ff|dffs[9]\,
	datab => \U_Filter|Dout_FF|dffs[8]\,
	datac => \U_Filter|Dout_FF|dffs[9]\,
	datad => \Dout_Del_ff|dffs[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[2]|comp|cmp[0]|aeb_out\);

\tlevel_reg_ff|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[12]\ = DFFE(\Tlevel[12]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Tlevel[12]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[12]\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[1]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[1]\ $ \U_Filter|acc1_del_ff|dffs[1]\ $ !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[1]\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[1]\ & \U_Filter|acc1_del_ff|dffs[1]\ & !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[0]\ # !\U_Filter|DS1|Dout_ff|dffs[1]\ & (\U_Filter|acc1_del_ff|dffs[1]\ # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[0]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[1]\,
	datab => \U_Filter|acc1_del_ff|dffs[1]\,
	cin => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[1]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[1]\);

\U_Filter|DS1|Dout_ff|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[2]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[2]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~513\ $ !\U_Filter|DS1|Dout_ff|dffs[1]~343\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[2]~340\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[2]\ & (\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~513\ # !\U_Filter|DS1|Dout_ff|dffs[1]~343\) # !\U_Filter|DS1|Dout_ff|dffs[2]\ & 
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~513\ & !\U_Filter|DS1|Dout_ff|dffs[1]~343\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[2]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~513\,
	cin => \U_Filter|DS1|Dout_ff|dffs[1]~343\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[2]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[2]~340\);

\U_Filter|acc1_del_ff|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[2]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|DS1|Dout_ff|dffs[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[2]\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~509_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~509\ = \U_Filter|DS1|DLY1|sram|q[3]\ $ \Din[3]~combout\ $ !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~515\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~511\ = CARRY(\U_Filter|DS1|DLY1|sram|q[3]\ & (!\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~515\ # !\Din[3]~combout\) # !\U_Filter|DS1|DLY1|sram|q[3]\ & !\Din[3]~combout\ & 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~515\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|DLY1|sram|q[3]\,
	datab => \Din[3]~combout\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~515\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~509\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~511\);

\U_Filter|DS1|DLY1|sram|segment[0][4]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 4,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[4]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][4]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][4]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][4]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[4]\);

\U_Filter|DS1|DLY1|sram|segment[0][5]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 5,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[5]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][5]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][5]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][5]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[5]\);

\U_Filter|DS1|DLY1|sram|segment[0][6]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 6,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[6]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][6]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][6]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][6]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[6]\);

\U_Filter|DS1|DLY1|sram|segment[0][7]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 7,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[7]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][7]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][7]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][7]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[7]\);

\U_Filter|DS1|DLY1|sram|segment[0][8]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 8,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[8]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][8]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][8]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][8]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[8]\);

\U_Filter|DS1|DLY1|sram|segment[0][9]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 9,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[9]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][9]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][9]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][9]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[9]\);

\U_Filter|DS1|DLY1|sram|segment[0][10]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 10,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[10]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][10]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][10]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][10]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[10]\);

\U_Filter|DS1|DLY1|sram|segment[0][11]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 11,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[11]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][11]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][11]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][11]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[11]\);

\U_Filter|DS1|DLY1|sram|segment[0][12]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 12,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[12]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][12]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][12]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][12]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[12]\);

\U_Filter|DS1|DLY1|sram|segment[0][13]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 13,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[13]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][13]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][13]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][13]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[13]\);

\U_Filter|DS1|DLY1|sram|segment[0][14]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 14,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[14]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][14]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][14]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][14]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[14]\);

\U_Filter|DS1|DLY1|sram|segment[0][15]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 15,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[15]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][15]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][15]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][15]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[15]\);

\U_Filter|DS1|DLY1|sram|segment[0][16]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 16,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[15]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][16]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][16]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][16]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[16]\);

\U_Filter|DS1|DLY1|sram|segment[0][17]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 17,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[15]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][17]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][17]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][17]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[17]\);

\U_Filter|DS1|DLY1|sram|segment[0][18]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 18,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[15]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][18]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][18]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][18]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[18]\);

\U_Filter|DS1|DLY1|sram|segment[0][19]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 19,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[15]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][19]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][19]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][19]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[19]\);

\Dout_Del_ff|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[7]\ = DFFE(\U_Filter|Dout_FF|dffs[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|Dout_FF|dffs[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[7]\);

\Dout_Del_ff|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[4]\ = DFFE(\U_Filter|Dout_FF|dffs[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|Dout_FF|dffs[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[4]\);

\PSlope_Compare|comparator|cmp[1]|comp|cmp[0]|aeb_out~0\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[1]|comp|cmp[0]|aeb_out\ = \Dout_Del_ff|dffs[4]\ & \U_Filter|Dout_FF|dffs[4]\ & (\U_Filter|Dout_FF|dffs[5]\ $ !\Dout_Del_ff|dffs[5]\) # !\Dout_Del_ff|dffs[4]\ & !\U_Filter|Dout_FF|dffs[4]\ & (\U_Filter|Dout_FF|dffs[5]\ $ 
-- !\Dout_Del_ff|dffs[5]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Del_ff|dffs[4]\,
	datab => \U_Filter|Dout_FF|dffs[5]\,
	datac => \U_Filter|Dout_FF|dffs[4]\,
	datad => \Dout_Del_ff|dffs[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[1]|comp|cmp[0]|aeb_out\);

\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[0]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[0]\ $ \U_Filter|acc1_del_ff|dffs[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[0]\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[0]\ # !\U_Filter|acc1_del_ff|dffs[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66BB",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[0]\,
	datab => \U_Filter|acc1_del_ff|dffs[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[0]\,
	cout => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|cout[0]\);

\U_Filter|DS1|Dout_ff|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[1]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[1]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~517\ $ \U_Filter|DS1|Dout_ff|dffs[0]~346\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[1]~343\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[1]\ & !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~517\ & !\U_Filter|DS1|Dout_ff|dffs[0]~346\ # !\U_Filter|DS1|Dout_ff|dffs[1]\ & (!\U_Filter|DS1|Dout_ff|dffs[0]~346\ # 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~517\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[1]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~517\,
	cin => \U_Filter|DS1|Dout_ff|dffs[0]~346\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[1]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[1]~343\);

\U_Filter|acc1_del_ff|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[1]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[1]\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~513_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~513\ = \Din[2]~combout\ $ \U_Filter|DS1|DLY1|sram|q[2]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~519\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~515\ = CARRY(\Din[2]~combout\ & (!\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~519\ # !\U_Filter|DS1|DLY1|sram|q[2]\) # !\Din[2]~combout\ & !\U_Filter|DS1|DLY1|sram|q[2]\ & 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~519\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din[2]~combout\,
	datab => \U_Filter|DS1|DLY1|sram|q[2]\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~519\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~513\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~515\);

\U_Filter|DS1|DLY1|sram|segment[0][3]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 3,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[3]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][3]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][3]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][3]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[3]\);

\U_Filter|DS1|Dout_ff|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|Dout_ff|dffs[0]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS1|Dout_ff|dffs[0]\ $ \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~521\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS1|Dout_ff|dffs[0]~346\ = CARRY(\U_Filter|DS1|Dout_ff|dffs[0]\ & \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~521\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|Dout_ff|dffs[0]\,
	datab => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~521\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS1|Dout_ff|dffs[0]\,
	cout => \U_Filter|DS1|Dout_ff|dffs[0]~346\);

\U_Filter|acc1_del_ff|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|acc1_del_ff|dffs[0]\ = DFFE(\U_Filter|DS1|Dout_ff|dffs[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS1|Dout_ff|dffs[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|acc1_del_ff|dffs[0]\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~517_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~517\ = \U_Filter|DS1|DLY1|sram|q[1]\ $ \Din[1]~combout\ $ !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~523\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~519\ = CARRY(\U_Filter|DS1|DLY1|sram|q[1]\ & (!\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~523\ # !\Din[1]~combout\) # !\U_Filter|DS1|DLY1|sram|q[1]\ & !\Din[1]~combout\ & 
-- !\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~523\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|DLY1|sram|q[1]\,
	datab => \Din[1]~combout\,
	cin => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~523\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~517\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~519\);

\U_Filter|DS1|DLY1|sram|segment[0][2]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 2,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[2]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][2]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][2]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][2]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[2]\);

\Dout_Del_ff|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[2]\ = DFFE(\U_Filter|Dout_FF|dffs[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|Dout_FF|dffs[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[2]\);

\tlevel_reg_ff|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[9]\ = DFFE(\Tlevel[9]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Tlevel[9]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[9]\);

\U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~521_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~521\ = \U_Filter|DS1|DLY1|sram|q[0]\ $ \Din[0]~combout\
-- \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~523\ = CARRY(\Din[0]~combout\ # !\U_Filter|DS1|DLY1|sram|q[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66DD",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS1|DLY1|sram|q[0]\,
	datab => \Din[0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~521\,
	cout => \U_Filter|DS1|DS_Add_Sub|adder|result_node|cs_buffer[0]~523\);

\U_Filter|DS1|DLY1|sram|segment[0][1]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 1,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[1]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][1]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][1]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][1]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[1]\);

\tlevel_reg_ff|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[8]\ = DFFE(\Tlevel[8]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Tlevel[8]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[8]\);

\U_Filter|DS1|DLY1|sram|segment[0][0]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 0,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS1|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \Din[0]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS1|DLY1|sram|segment[0][0]_WADDR_bus\,
	raddr => \U_Filter|DS1|DLY1|sram|segment[0][0]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS1|DLY1|sram|segment[0][0]_modesel\,
	dataout => \U_Filter|DS1|DLY1|sram|q[0]\);

\tlevel_reg_ff|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[7]\ = DFFE(\Tlevel[7]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Tlevel[7]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[7]\);

\tlevel_reg_ff|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[5]\ = DFFE(\Tlevel[5]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Tlevel[5]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[5]\);

\tlevel_reg_ff|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[4]\ = DFFE(\Tlevel[4]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Tlevel[4]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[4]\);

\tlevel_reg_ff|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[3]\ = DFFE(\Tlevel[3]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Tlevel[3]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[3]\);

\tlevel_reg_ff|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[2]\ = DFFE(\Tlevel[2]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Tlevel[2]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[2]\);

\tlevel_reg_ff|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[1]\ = DFFE(\Tlevel[1]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Tlevel[1]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[1]\);

\tlevel_reg_ff|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[0]\ = DFFE(\Tlevel[0]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Tlevel[0]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[0]\);

\DSP_D[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(0),
	combout => \DSP_D[0]~combout\);

\DSP_D[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(1),
	combout => \DSP_D[1]~combout\);

\DSP_D[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(2),
	combout => \DSP_D[2]~combout\);

\DSP_D[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(4),
	combout => \DSP_D[4]~combout\);

\DSP_D[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(5),
	combout => \DSP_D[5]~combout\);

\DSP_D[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(6),
	combout => \DSP_D[6]~combout\);

\DSP_D[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(12),
	combout => \DSP_D[12]~combout\);

\DSP_D[14]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(14),
	combout => \DSP_D[14]~combout\);

\DSP_D[15]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(15),
	combout => \DSP_D[15]~combout\);

\Tlevel[14]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(14),
	combout => \Tlevel[14]~combout\);

\Tlevel[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(13),
	combout => \Tlevel[13]~combout\);

\Din[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(4),
	combout => \Din[4]~combout\);

\Din[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(5),
	combout => \Din[5]~combout\);

\Din[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(6),
	combout => \Din[6]~combout\);

\Din[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(7),
	combout => \Din[7]~combout\);

\Din[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(8),
	combout => \Din[8]~combout\);

\Din[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(9),
	combout => \Din[9]~combout\);

\Din[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(10),
	combout => \Din[10]~combout\);

\Din[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(11),
	combout => \Din[11]~combout\);

\Din[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(12),
	combout => \Din[12]~combout\);

\Din[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(13),
	combout => \Din[13]~combout\);

\Din[14]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(14),
	combout => \Din[14]~combout\);

\Din[15]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(15),
	combout => \Din[15]~combout\);

\Tlevel[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(12),
	combout => \Tlevel[12]~combout\);

\Din[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(3),
	combout => \Din[3]~combout\);

\Din[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(2),
	combout => \Din[2]~combout\);

\Din[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(1),
	combout => \Din[1]~combout\);

\Tlevel[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(9),
	combout => \Tlevel[9]~combout\);

\Din[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(0),
	combout => \Din[0]~combout\);

\Tlevel[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(8),
	combout => \Tlevel[8]~combout\);

\Tlevel[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(7),
	combout => \Tlevel[7]~combout\);

\Tlevel[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(5),
	combout => \Tlevel[5]~combout\);

\Tlevel[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(4),
	combout => \Tlevel[4]~combout\);

\Tlevel[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(3),
	combout => \Tlevel[3]~combout\);

\Tlevel[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(2),
	combout => \Tlevel[2]~combout\);

\Tlevel[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(1),
	combout => \Tlevel[1]~combout\);

\Tlevel[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(0),
	combout => \Tlevel[0]~combout\);

\clk~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => \clk~combout\);

\imr~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => \imr~combout\);

\U_Filter|wadr_counter|wysi_counter|counter_cell[0]\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|wadr_counter|wysi_counter|sload_path[0]\ = DFFE(!\U_Filter|wadr_counter|wysi_counter|sload_path[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|wadr_counter|wysi_counter|counter_cell[0]~COUT\ = CARRY(\U_Filter|wadr_counter|wysi_counter|sload_path[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0FF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|wadr_counter|wysi_counter|sload_path[0]\,
	cout => \U_Filter|wadr_counter|wysi_counter|counter_cell[0]~COUT\);

\U_Filter|wadr_counter|wysi_counter|counter_cell[1]\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|wadr_counter|wysi_counter|sload_path[1]\ = DFFE(\U_Filter|wadr_counter|wysi_counter|sload_path[1]\ $ (\U_Filter|wadr_counter|wysi_counter|counter_cell[0]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|wadr_counter|wysi_counter|counter_cell[1]~COUT\ = CARRY(!\U_Filter|wadr_counter|wysi_counter|counter_cell[0]~COUT\ # !\U_Filter|wadr_counter|wysi_counter|sload_path[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|wadr_counter|wysi_counter|sload_path[1]\,
	cin => \U_Filter|wadr_counter|wysi_counter|counter_cell[0]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|wadr_counter|wysi_counter|sload_path[1]\,
	cout => \U_Filter|wadr_counter|wysi_counter|counter_cell[1]~COUT\);

\U_Filter|wadr_counter|wysi_counter|counter_cell[2]\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|wadr_counter|wysi_counter|sload_path[2]\ = DFFE(\U_Filter|wadr_counter|wysi_counter|sload_path[2]\ $ (!\U_Filter|wadr_counter|wysi_counter|counter_cell[1]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|wadr_counter|wysi_counter|counter_cell[2]~COUT\ = CARRY(\U_Filter|wadr_counter|wysi_counter|sload_path[2]\ & (!\U_Filter|wadr_counter|wysi_counter|counter_cell[1]~COUT\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|wadr_counter|wysi_counter|sload_path[2]\,
	cin => \U_Filter|wadr_counter|wysi_counter|counter_cell[1]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|wadr_counter|wysi_counter|sload_path[2]\,
	cout => \U_Filter|wadr_counter|wysi_counter|counter_cell[2]~COUT\);

\U_Filter|wadr_counter|wysi_counter|counter_cell[3]\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ = DFFE(\U_Filter|wadr_counter|wysi_counter|counter_cell[2]~COUT\ $ \U_Filter|wadr_counter|wysi_counter|sload_path[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|wadr_counter|wysi_counter|sload_path[3]\,
	cin => \U_Filter|wadr_counter|wysi_counter|counter_cell[2]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|wadr_counter|wysi_counter|sload_path[3]\);

\U_Filter|DS2|DLY1|sram|segment[0][4]~19_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DLY1|sram|segment[0][4]~19\ = !\U_Filter|wadr_counter|wysi_counter|sload_path[1]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F0F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|wadr_counter|wysi_counter|sload_path[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DLY1|sram|segment[0][4]~19\);

\U_Filter|add~46_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|add~46\ = \U_Filter|wadr_counter|wysi_counter|sload_path[1]\ $ \U_Filter|wadr_counter|wysi_counter|sload_path[2]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|wadr_counter|wysi_counter|sload_path[1]\,
	datad => \U_Filter|wadr_counter|wysi_counter|sload_path[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|add~46\);

\U_Filter|add~47_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|add~47\ = \U_Filter|wadr_counter|wysi_counter|sload_path[3]\ $ (!\U_Filter|wadr_counter|wysi_counter|sload_path[2]\ # !\U_Filter|wadr_counter|wysi_counter|sload_path[1]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A50F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|wadr_counter|wysi_counter|sload_path[1]\,
	datac => \U_Filter|wadr_counter|wysi_counter|sload_path[3]\,
	datad => \U_Filter|wadr_counter|wysi_counter|sload_path[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|add~47\);

\U_Filter|DS2|DLY1|sram|segment[0][18]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 18,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[18]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][18]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][18]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][18]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[18]\);

\U_Filter|DS2|DLY1|sram|segment[0][17]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 17,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[17]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][17]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][17]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][17]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[17]\);

\U_Filter|DS2|DLY1|sram|segment[0][16]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 16,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[16]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][16]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][16]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][16]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[16]\);

\U_Filter|DS2|DLY1|sram|segment[0][15]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 15,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[15]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][15]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][15]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][15]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[15]\);

\U_Filter|DS2|DLY1|sram|segment[0][14]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 14,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[14]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][14]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][14]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][14]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[14]\);

\U_Filter|DS2|DLY1|sram|segment[0][13]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 13,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[13]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][13]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][13]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][13]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[13]\);

\U_Filter|DS2|DLY1|sram|segment[0][12]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 12,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[12]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][12]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][12]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][12]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[12]\);

\U_Filter|DS2|DLY1|sram|segment[0][11]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 11,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[11]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][11]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][11]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][11]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[11]\);

\U_Filter|DS2|DLY1|sram|segment[0][10]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 10,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[10]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][10]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][10]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][10]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[10]\);

\U_Filter|DS2|DLY1|sram|segment[0][9]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 9,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[9]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][9]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][9]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][9]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[9]\);

\U_Filter|DS2|DLY1|sram|segment[0][8]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 8,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[8]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][8]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][8]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][8]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[8]\);

\U_Filter|DS2|DLY1|sram|segment[0][7]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 7,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[7]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][7]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][7]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][7]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[7]\);

\U_Filter|DS2|DLY1|sram|segment[0][6]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 6,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[6]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][6]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][6]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][6]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[6]\);

\U_Filter|DS2|DLY1|sram|segment[0][5]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 5,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[5]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][5]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][5]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][5]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[5]\);

\U_Filter|DS2|DLY1|sram|segment[0][4]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 4,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[4]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][4]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][4]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][4]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[4]\);

\U_Filter|DS2|DLY1|sram|segment[0][3]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 3,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[3]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][3]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][3]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][3]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[3]\);

\U_Filter|DS2|DLY1|sram|segment[0][2]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 2,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[2]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][2]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][2]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][2]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[2]\);

\U_Filter|DS2|DLY1|sram|segment[0][1]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 1,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[1]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][1]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][1]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][1]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[1]\);

\U_Filter|DS2|DLY1|sram|segment[0][0]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 0,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[0]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][0]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][0]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][0]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[0]\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~521_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~521\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[0]\ $ \U_Filter|DS2|DLY1|sram|q[0]\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~523\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[0]\ # !\U_Filter|DS2|DLY1|sram|q[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66BB",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[0]\,
	datab => \U_Filter|DS2|DLY1|sram|q[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~521\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~523\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~517_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~517\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[1]\ $ \U_Filter|DS2|DLY1|sram|q[1]\ $ !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~523\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~519\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[1]\ & \U_Filter|DS2|DLY1|sram|q[1]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~523\ # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[1]\ & (\U_Filter|DS2|DLY1|sram|q[1]\ # !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~523\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[1]\,
	datab => \U_Filter|DS2|DLY1|sram|q[1]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~523\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~517\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~519\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~513_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~513\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[2]\ $ \U_Filter|DS2|DLY1|sram|q[2]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~519\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~515\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[2]\ & (!\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~519\ # !\U_Filter|DS2|DLY1|sram|q[2]\) # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[2]\ & !\U_Filter|DS2|DLY1|sram|q[2]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~519\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[2]\,
	datab => \U_Filter|DS2|DLY1|sram|q[2]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~519\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~513\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~515\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~509_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~509\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[3]\ $ \U_Filter|DS2|DLY1|sram|q[3]\ $ !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~515\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~511\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[3]\ & \U_Filter|DS2|DLY1|sram|q[3]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~515\ # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[3]\ & (\U_Filter|DS2|DLY1|sram|q[3]\ # !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~515\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[3]\,
	datab => \U_Filter|DS2|DLY1|sram|q[3]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~515\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~509\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~511\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~445_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~445\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[4]\ $ \U_Filter|DS2|DLY1|sram|q[4]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~511\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~447\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[4]\ & (!\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~511\ # !\U_Filter|DS2|DLY1|sram|q[4]\) # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[4]\ & !\U_Filter|DS2|DLY1|sram|q[4]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~511\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[4]\,
	datab => \U_Filter|DS2|DLY1|sram|q[4]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~511\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~445\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~447\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~449_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~449\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[5]\ $ \U_Filter|DS2|DLY1|sram|q[5]\ $ !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~447\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~451\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[5]\ & \U_Filter|DS2|DLY1|sram|q[5]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~447\ # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[5]\ & (\U_Filter|DS2|DLY1|sram|q[5]\ # !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~447\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[5]\,
	datab => \U_Filter|DS2|DLY1|sram|q[5]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~447\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~449\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~451\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~453_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~453\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[6]\ $ \U_Filter|DS2|DLY1|sram|q[6]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~451\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~455\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[6]\ & (!\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~451\ # !\U_Filter|DS2|DLY1|sram|q[6]\) # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[6]\ & !\U_Filter|DS2|DLY1|sram|q[6]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~451\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[6]\,
	datab => \U_Filter|DS2|DLY1|sram|q[6]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~451\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~453\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~455\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~457_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~457\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[7]\ $ \U_Filter|DS2|DLY1|sram|q[7]\ $ !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~455\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~459\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[7]\ & \U_Filter|DS2|DLY1|sram|q[7]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~455\ # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[7]\ & (\U_Filter|DS2|DLY1|sram|q[7]\ # !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~455\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[7]\,
	datab => \U_Filter|DS2|DLY1|sram|q[7]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~455\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~457\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~459\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~461_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~461\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[8]\ $ \U_Filter|DS2|DLY1|sram|q[8]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~459\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~463\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[8]\ & (!\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~459\ # !\U_Filter|DS2|DLY1|sram|q[8]\) # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[8]\ & !\U_Filter|DS2|DLY1|sram|q[8]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~459\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[8]\,
	datab => \U_Filter|DS2|DLY1|sram|q[8]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~459\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~461\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~463\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~465_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~465\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[9]\ $ \U_Filter|DS2|DLY1|sram|q[9]\ $ !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~463\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~467\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[9]\ & \U_Filter|DS2|DLY1|sram|q[9]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~463\ # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[9]\ & (\U_Filter|DS2|DLY1|sram|q[9]\ # !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~463\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[9]\,
	datab => \U_Filter|DS2|DLY1|sram|q[9]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~463\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~465\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~467\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~469_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~469\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[10]\ $ \U_Filter|DS2|DLY1|sram|q[10]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~467\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~471\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[10]\ & (!\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~467\ # !\U_Filter|DS2|DLY1|sram|q[10]\) # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[10]\ & !\U_Filter|DS2|DLY1|sram|q[10]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~467\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[10]\,
	datab => \U_Filter|DS2|DLY1|sram|q[10]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~467\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~469\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~471\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~473_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~473\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[11]\ $ \U_Filter|DS2|DLY1|sram|q[11]\ $ !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~471\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~475\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[11]\ & \U_Filter|DS2|DLY1|sram|q[11]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~471\ # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[11]\ & (\U_Filter|DS2|DLY1|sram|q[11]\ # !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~471\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[11]\,
	datab => \U_Filter|DS2|DLY1|sram|q[11]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~471\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~473\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~475\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~477_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~477\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[12]\ $ \U_Filter|DS2|DLY1|sram|q[12]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~475\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~479\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[12]\ & (!\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~475\ # !\U_Filter|DS2|DLY1|sram|q[12]\) # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[12]\ & !\U_Filter|DS2|DLY1|sram|q[12]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~475\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[12]\,
	datab => \U_Filter|DS2|DLY1|sram|q[12]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~475\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~477\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~479\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~481_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~481\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[13]\ $ \U_Filter|DS2|DLY1|sram|q[13]\ $ !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~479\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~483\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[13]\ & \U_Filter|DS2|DLY1|sram|q[13]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~479\ # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[13]\ & (\U_Filter|DS2|DLY1|sram|q[13]\ # !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~479\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[13]\,
	datab => \U_Filter|DS2|DLY1|sram|q[13]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~479\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~481\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~483\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~485_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~485\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[14]\ $ \U_Filter|DS2|DLY1|sram|q[14]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~483\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~487\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[14]\ & (!\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~483\ # !\U_Filter|DS2|DLY1|sram|q[14]\) # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[14]\ & !\U_Filter|DS2|DLY1|sram|q[14]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~483\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[14]\,
	datab => \U_Filter|DS2|DLY1|sram|q[14]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~483\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~485\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~487\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~489_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~489\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[15]\ $ \U_Filter|DS2|DLY1|sram|q[15]\ $ !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~487\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~491\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[15]\ & \U_Filter|DS2|DLY1|sram|q[15]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~487\ # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[15]\ & (\U_Filter|DS2|DLY1|sram|q[15]\ # !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~487\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[15]\,
	datab => \U_Filter|DS2|DLY1|sram|q[15]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~487\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~489\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~491\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~493_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~493\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[16]\ $ \U_Filter|DS2|DLY1|sram|q[16]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~491\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~495\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[16]\ & (!\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~491\ # !\U_Filter|DS2|DLY1|sram|q[16]\) # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[16]\ & !\U_Filter|DS2|DLY1|sram|q[16]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~491\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[16]\,
	datab => \U_Filter|DS2|DLY1|sram|q[16]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~491\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~493\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~495\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~497_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~497\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[17]\ $ \U_Filter|DS2|DLY1|sram|q[17]\ $ !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~495\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~499\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[17]\ & \U_Filter|DS2|DLY1|sram|q[17]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~495\ # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[17]\ & (\U_Filter|DS2|DLY1|sram|q[17]\ # !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~495\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[17]\,
	datab => \U_Filter|DS2|DLY1|sram|q[17]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~495\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~497\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~499\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~501_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~501\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[18]\ $ \U_Filter|DS2|DLY1|sram|q[18]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~499\
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~503\ = CARRY(\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[18]\ & (!\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~499\ # !\U_Filter|DS2|DLY1|sram|q[18]\) # 
-- !\U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[18]\ & !\U_Filter|DS2|DLY1|sram|q[18]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~499\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[18]\,
	datab => \U_Filter|DS2|DLY1|sram|q[18]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~499\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~501\,
	cout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~503\);

\FIR_MR~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_FIR_MR,
	combout => \FIR_MR~combout\);

\U_Filter|DS2|Dout_ff|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[4]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[4]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~445\ $ !\U_Filter|DS2|Acc_ff|dffs[3]~67\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Dout_ff|dffs[4]~114\ = CARRY(\U_Filter|DS2|Dout_ff|dffs[4]\ & (\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~445\ # !\U_Filter|DS2|Acc_ff|dffs[3]~67\) # !\U_Filter|DS2|Dout_ff|dffs[4]\ & 
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~445\ & !\U_Filter|DS2|Acc_ff|dffs[3]~67\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[4]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~445\,
	cin => \U_Filter|DS2|Acc_ff|dffs[3]~67\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[4]\,
	cout => \U_Filter|DS2|Dout_ff|dffs[4]~114\);

\U_Filter|DS2|Dout_ff|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[5]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[5]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~449\ $ \U_Filter|DS2|Dout_ff|dffs[4]~114\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Dout_ff|dffs[5]~117\ = CARRY(\U_Filter|DS2|Dout_ff|dffs[5]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~449\ & !\U_Filter|DS2|Dout_ff|dffs[4]~114\ # !\U_Filter|DS2|Dout_ff|dffs[5]\ & (!\U_Filter|DS2|Dout_ff|dffs[4]~114\ # 
-- !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~449\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[5]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~449\,
	cin => \U_Filter|DS2|Dout_ff|dffs[4]~114\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[5]\,
	cout => \U_Filter|DS2|Dout_ff|dffs[5]~117\);

\U_Filter|DS2|Dout_ff|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[6]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[6]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~453\ $ !\U_Filter|DS2|Dout_ff|dffs[5]~117\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Dout_ff|dffs[6]~120\ = CARRY(\U_Filter|DS2|Dout_ff|dffs[6]\ & (\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~453\ # !\U_Filter|DS2|Dout_ff|dffs[5]~117\) # !\U_Filter|DS2|Dout_ff|dffs[6]\ & 
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~453\ & !\U_Filter|DS2|Dout_ff|dffs[5]~117\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[6]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~453\,
	cin => \U_Filter|DS2|Dout_ff|dffs[5]~117\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[6]\,
	cout => \U_Filter|DS2|Dout_ff|dffs[6]~120\);

\U_Filter|DS2|Dout_ff|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[7]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[7]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~457\ $ \U_Filter|DS2|Dout_ff|dffs[6]~120\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Dout_ff|dffs[7]~123\ = CARRY(\U_Filter|DS2|Dout_ff|dffs[7]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~457\ & !\U_Filter|DS2|Dout_ff|dffs[6]~120\ # !\U_Filter|DS2|Dout_ff|dffs[7]\ & (!\U_Filter|DS2|Dout_ff|dffs[6]~120\ # 
-- !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~457\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[7]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~457\,
	cin => \U_Filter|DS2|Dout_ff|dffs[6]~120\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[7]\,
	cout => \U_Filter|DS2|Dout_ff|dffs[7]~123\);

\U_Filter|DS2|Dout_ff|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[8]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[8]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~461\ $ !\U_Filter|DS2|Dout_ff|dffs[7]~123\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Dout_ff|dffs[8]~126\ = CARRY(\U_Filter|DS2|Dout_ff|dffs[8]\ & (\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~461\ # !\U_Filter|DS2|Dout_ff|dffs[7]~123\) # !\U_Filter|DS2|Dout_ff|dffs[8]\ & 
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~461\ & !\U_Filter|DS2|Dout_ff|dffs[7]~123\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[8]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~461\,
	cin => \U_Filter|DS2|Dout_ff|dffs[7]~123\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[8]\,
	cout => \U_Filter|DS2|Dout_ff|dffs[8]~126\);

\U_Filter|DS2|Dout_ff|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[9]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[9]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~465\ $ \U_Filter|DS2|Dout_ff|dffs[8]~126\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Dout_ff|dffs[9]~129\ = CARRY(\U_Filter|DS2|Dout_ff|dffs[9]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~465\ & !\U_Filter|DS2|Dout_ff|dffs[8]~126\ # !\U_Filter|DS2|Dout_ff|dffs[9]\ & (!\U_Filter|DS2|Dout_ff|dffs[8]~126\ # 
-- !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~465\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[9]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~465\,
	cin => \U_Filter|DS2|Dout_ff|dffs[8]~126\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[9]\,
	cout => \U_Filter|DS2|Dout_ff|dffs[9]~129\);

\U_Filter|DS2|Dout_ff|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[10]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[10]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~469\ $ !\U_Filter|DS2|Dout_ff|dffs[9]~129\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Dout_ff|dffs[10]~132\ = CARRY(\U_Filter|DS2|Dout_ff|dffs[10]\ & (\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~469\ # !\U_Filter|DS2|Dout_ff|dffs[9]~129\) # !\U_Filter|DS2|Dout_ff|dffs[10]\ & 
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~469\ & !\U_Filter|DS2|Dout_ff|dffs[9]~129\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[10]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~469\,
	cin => \U_Filter|DS2|Dout_ff|dffs[9]~129\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[10]\,
	cout => \U_Filter|DS2|Dout_ff|dffs[10]~132\);

\U_Filter|DS2|Dout_ff|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[11]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[11]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~473\ $ \U_Filter|DS2|Dout_ff|dffs[10]~132\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Dout_ff|dffs[11]~135\ = CARRY(\U_Filter|DS2|Dout_ff|dffs[11]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~473\ & !\U_Filter|DS2|Dout_ff|dffs[10]~132\ # !\U_Filter|DS2|Dout_ff|dffs[11]\ & (!\U_Filter|DS2|Dout_ff|dffs[10]~132\ # 
-- !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~473\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[11]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~473\,
	cin => \U_Filter|DS2|Dout_ff|dffs[10]~132\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[11]\,
	cout => \U_Filter|DS2|Dout_ff|dffs[11]~135\);

\U_Filter|DS2|Dout_ff|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[12]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[12]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~477\ $ !\U_Filter|DS2|Dout_ff|dffs[11]~135\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Dout_ff|dffs[12]~138\ = CARRY(\U_Filter|DS2|Dout_ff|dffs[12]\ & (\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~477\ # !\U_Filter|DS2|Dout_ff|dffs[11]~135\) # !\U_Filter|DS2|Dout_ff|dffs[12]\ & 
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~477\ & !\U_Filter|DS2|Dout_ff|dffs[11]~135\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[12]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~477\,
	cin => \U_Filter|DS2|Dout_ff|dffs[11]~135\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[12]\,
	cout => \U_Filter|DS2|Dout_ff|dffs[12]~138\);

\U_Filter|DS2|Dout_ff|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[13]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[13]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~481\ $ \U_Filter|DS2|Dout_ff|dffs[12]~138\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Dout_ff|dffs[13]~141\ = CARRY(\U_Filter|DS2|Dout_ff|dffs[13]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~481\ & !\U_Filter|DS2|Dout_ff|dffs[12]~138\ # !\U_Filter|DS2|Dout_ff|dffs[13]\ & (!\U_Filter|DS2|Dout_ff|dffs[12]~138\ # 
-- !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~481\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[13]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~481\,
	cin => \U_Filter|DS2|Dout_ff|dffs[12]~138\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[13]\,
	cout => \U_Filter|DS2|Dout_ff|dffs[13]~141\);

\U_Filter|DS2|Dout_ff|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[14]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[14]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~485\ $ !\U_Filter|DS2|Dout_ff|dffs[13]~141\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Dout_ff|dffs[14]~144\ = CARRY(\U_Filter|DS2|Dout_ff|dffs[14]\ & (\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~485\ # !\U_Filter|DS2|Dout_ff|dffs[13]~141\) # !\U_Filter|DS2|Dout_ff|dffs[14]\ & 
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~485\ & !\U_Filter|DS2|Dout_ff|dffs[13]~141\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[14]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~485\,
	cin => \U_Filter|DS2|Dout_ff|dffs[13]~141\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[14]\,
	cout => \U_Filter|DS2|Dout_ff|dffs[14]~144\);

\U_Filter|DS2|Dout_ff|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[15]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[15]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~489\ $ \U_Filter|DS2|Dout_ff|dffs[14]~144\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Dout_ff|dffs[15]~147\ = CARRY(\U_Filter|DS2|Dout_ff|dffs[15]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~489\ & !\U_Filter|DS2|Dout_ff|dffs[14]~144\ # !\U_Filter|DS2|Dout_ff|dffs[15]\ & (!\U_Filter|DS2|Dout_ff|dffs[14]~144\ # 
-- !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~489\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[15]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~489\,
	cin => \U_Filter|DS2|Dout_ff|dffs[14]~144\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[15]\,
	cout => \U_Filter|DS2|Dout_ff|dffs[15]~147\);

\U_Filter|DS2|Dout_ff|dffs[16]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[16]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[16]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~493\ $ !\U_Filter|DS2|Dout_ff|dffs[15]~147\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Dout_ff|dffs[16]~150\ = CARRY(\U_Filter|DS2|Dout_ff|dffs[16]\ & (\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~493\ # !\U_Filter|DS2|Dout_ff|dffs[15]~147\) # !\U_Filter|DS2|Dout_ff|dffs[16]\ & 
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~493\ & !\U_Filter|DS2|Dout_ff|dffs[15]~147\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[16]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~493\,
	cin => \U_Filter|DS2|Dout_ff|dffs[15]~147\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[16]\,
	cout => \U_Filter|DS2|Dout_ff|dffs[16]~150\);

\U_Filter|DS2|Dout_ff|dffs[17]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[17]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[17]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~497\ $ \U_Filter|DS2|Dout_ff|dffs[16]~150\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Dout_ff|dffs[17]~153\ = CARRY(\U_Filter|DS2|Dout_ff|dffs[17]\ & !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~497\ & !\U_Filter|DS2|Dout_ff|dffs[16]~150\ # !\U_Filter|DS2|Dout_ff|dffs[17]\ & (!\U_Filter|DS2|Dout_ff|dffs[16]~150\ # 
-- !\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~497\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[17]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~497\,
	cin => \U_Filter|DS2|Dout_ff|dffs[16]~150\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[17]\,
	cout => \U_Filter|DS2|Dout_ff|dffs[17]~153\);

\U_Filter|DS2|Dout_ff|dffs[18]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[18]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[18]\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~501\ $ !\U_Filter|DS2|Dout_ff|dffs[17]~153\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \U_Filter|DS2|Dout_ff|dffs[18]~156\ = CARRY(\U_Filter|DS2|Dout_ff|dffs[18]\ & (\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~501\ # !\U_Filter|DS2|Dout_ff|dffs[17]~153\) # !\U_Filter|DS2|Dout_ff|dffs[18]\ & 
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~501\ & !\U_Filter|DS2|Dout_ff|dffs[17]~153\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[18]\,
	datab => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~501\,
	cin => \U_Filter|DS2|Dout_ff|dffs[17]~153\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[18]\,
	cout => \U_Filter|DS2|Dout_ff|dffs[18]~156\);

\U_Filter|Dout_FF|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[14]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[18]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[18]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[14]\);

\U_Filter|DS2|DLY1|sram|segment[0][19]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 19,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "none",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "wds_filter:U_Filter|wds_ds:DS2|lpm_ram_dp:DLY1|altdpram:sram|content",
	logical_ram_width => 40,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[19]\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => VCC,
	re => VCC,
	waddr => \U_Filter|DS2|DLY1|sram|segment[0][19]_WADDR_bus\,
	raddr => \U_Filter|DS2|DLY1|sram|segment[0][19]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \U_Filter|DS2|DLY1|sram|segment[0][19]_modesel\,
	dataout => \U_Filter|DS2|DLY1|sram|q[19]\);

\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~505_I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~505\ = \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[19]\ $ (\U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~503\ $ !\U_Filter|DS2|DLY1|sram|q[19]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AA5",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|PZ1_Add_Sub|adder1[0]|result_node|sout_node[19]\,
	datad => \U_Filter|DS2|DLY1|sram|q[19]\,
	cin => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~503\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~505\);

\U_Filter|DS2|Dout_ff|dffs[19]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|DS2|Dout_ff|dffs[19]\ = DFFE(!GLOBAL(\FIR_MR~combout\) & \U_Filter|DS2|Dout_ff|dffs[19]\ $ (\U_Filter|DS2|Dout_ff|dffs[18]~156\ $ \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~505\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|DS2|Dout_ff|dffs[19]\,
	datad => \U_Filter|DS2|DS_Add_Sub|adder|result_node|cs_buffer[0]~505\,
	cin => \U_Filter|DS2|Dout_ff|dffs[18]~156\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \FIR_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|DS2|Dout_ff|dffs[19]\);

\U_Filter|Dout_FF|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[15]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[19]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[19]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[15]\);

\Dout_Del_ff|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[15]\ = DFFE(\U_Filter|Dout_FF|dffs[15]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|Dout_FF|dffs[15]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[15]\);

\PSlope_Compare|comparator|cmp[3]|comp|cmp[1]|aeb_out~0\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[3]|comp|cmp[1]|aeb_out\ = \Dout_Del_ff|dffs[14]\ & \U_Filter|Dout_FF|dffs[14]\ & (\U_Filter|Dout_FF|dffs[15]\ $ !\Dout_Del_ff|dffs[15]\) # !\Dout_Del_ff|dffs[14]\ & !\U_Filter|Dout_FF|dffs[14]\ & (\U_Filter|Dout_FF|dffs[15]\ 
-- $ !\Dout_Del_ff|dffs[15]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Del_ff|dffs[14]\,
	datab => \U_Filter|Dout_FF|dffs[14]\,
	datac => \U_Filter|Dout_FF|dffs[15]\,
	datad => \Dout_Del_ff|dffs[15]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[3]|comp|cmp[1]|aeb_out\);

\PSlope_Compare|comparator|cmp[3]|comp|cmp[1]|agb_out~0\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[3]|comp|cmp[1]|agb_out\ = \U_Filter|Dout_FF|dffs[15]\ & !\Dout_Del_ff|dffs[14]\ & \U_Filter|Dout_FF|dffs[14]\ & \Dout_Del_ff|dffs[15]\ # !\U_Filter|Dout_FF|dffs[15]\ & (\Dout_Del_ff|dffs[15]\ # !\Dout_Del_ff|dffs[14]\ & 
-- \U_Filter|Dout_FF|dffs[14]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4F04",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Del_ff|dffs[14]\,
	datab => \U_Filter|Dout_FF|dffs[14]\,
	datac => \U_Filter|Dout_FF|dffs[15]\,
	datad => \Dout_Del_ff|dffs[15]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[3]|comp|cmp[1]|agb_out\);

\PSlope_Compare|comparator|cmp[3]|comp|sub_comptree|gt_cmp_end|agb_out~I\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[3]|comp|sub_comptree|gt_cmp_end|agb_out\ = DFFE(\PSlope_Compare|comparator|cmp[3]|comp|cmp[1]|agb_out\ # \PSlope_Compare|comparator|cmp[3]|comp|cmp[0]|agb_out\ & \PSlope_Compare|comparator|cmp[3]|comp|cmp[1]|aeb_out\, 
-- GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFA0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \PSlope_Compare|comparator|cmp[3]|comp|cmp[0]|agb_out\,
	datac => \PSlope_Compare|comparator|cmp[3]|comp|cmp[1]|aeb_out\,
	datad => \PSlope_Compare|comparator|cmp[3]|comp|cmp[1]|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \PSlope_Compare|comparator|cmp[3]|comp|sub_comptree|gt_cmp_end|agb_out\);

\U_Filter|Dout_FF|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[11]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[15]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[15]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[11]\);

\U_Filter|Dout_FF|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[10]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[14]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[14]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[10]\);

\Dout_Del_ff|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[10]\ = DFFE(\U_Filter|Dout_FF|dffs[10]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|Dout_FF|dffs[10]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[10]\);

\PSlope_Compare|comparator|cmp[2]|comp|cmp[1]|aeb_out~0\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[2]|comp|cmp[1]|aeb_out\ = \Dout_Del_ff|dffs[11]\ & \U_Filter|Dout_FF|dffs[11]\ & (\U_Filter|Dout_FF|dffs[10]\ $ !\Dout_Del_ff|dffs[10]\) # !\Dout_Del_ff|dffs[11]\ & !\U_Filter|Dout_FF|dffs[11]\ & (\U_Filter|Dout_FF|dffs[10]\ 
-- $ !\Dout_Del_ff|dffs[10]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Del_ff|dffs[11]\,
	datab => \U_Filter|Dout_FF|dffs[11]\,
	datac => \U_Filter|Dout_FF|dffs[10]\,
	datad => \Dout_Del_ff|dffs[10]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[2]|comp|cmp[1]|aeb_out\);

\U_Filter|Dout_FF|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[8]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[12]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[12]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[8]\);

\U_Filter|Dout_FF|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[9]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[13]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[13]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[9]\);

\Dout_Del_ff|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[8]\ = DFFE(\U_Filter|Dout_FF|dffs[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|Dout_FF|dffs[8]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[8]\);

\PSlope_Compare|comparator|cmp[2]|comp|cmp[0]|agb_out~0\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[2]|comp|cmp[0]|agb_out\ = \Dout_Del_ff|dffs[9]\ & \U_Filter|Dout_FF|dffs[8]\ & \U_Filter|Dout_FF|dffs[9]\ & !\Dout_Del_ff|dffs[8]\ # !\Dout_Del_ff|dffs[9]\ & (\U_Filter|Dout_FF|dffs[9]\ # \U_Filter|Dout_FF|dffs[8]\ & 
-- !\Dout_Del_ff|dffs[8]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "50D4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Del_ff|dffs[9]\,
	datab => \U_Filter|Dout_FF|dffs[8]\,
	datac => \U_Filter|Dout_FF|dffs[9]\,
	datad => \Dout_Del_ff|dffs[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[2]|comp|cmp[0]|agb_out\);

\PSlope_Compare|comparator|cmp[2]|comp|cmp[1]|agb_out~0\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[2]|comp|cmp[1]|agb_out\ = \Dout_Del_ff|dffs[11]\ & \U_Filter|Dout_FF|dffs[11]\ & \U_Filter|Dout_FF|dffs[10]\ & !\Dout_Del_ff|dffs[10]\ # !\Dout_Del_ff|dffs[11]\ & (\U_Filter|Dout_FF|dffs[11]\ # \U_Filter|Dout_FF|dffs[10]\ & 
-- !\Dout_Del_ff|dffs[10]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "44D4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Del_ff|dffs[11]\,
	datab => \U_Filter|Dout_FF|dffs[11]\,
	datac => \U_Filter|Dout_FF|dffs[10]\,
	datad => \Dout_Del_ff|dffs[10]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[2]|comp|cmp[1]|agb_out\);

\PSlope_Compare|comparator|cmp[2]|comp|sub_comptree|gt_cmp_end|agb_out~I\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[2]|comp|sub_comptree|gt_cmp_end|agb_out\ = DFFE(\PSlope_Compare|comparator|cmp[2]|comp|cmp[1]|agb_out\ # \PSlope_Compare|comparator|cmp[2]|comp|cmp[1]|aeb_out\ & \PSlope_Compare|comparator|cmp[2]|comp|cmp[0]|agb_out\, 
-- GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \PSlope_Compare|comparator|cmp[2]|comp|cmp[1]|aeb_out\,
	datac => \PSlope_Compare|comparator|cmp[2]|comp|cmp[0]|agb_out\,
	datad => \PSlope_Compare|comparator|cmp[2]|comp|cmp[1]|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \PSlope_Compare|comparator|cmp[2]|comp|sub_comptree|gt_cmp_end|agb_out\);

\U_Filter|Dout_FF|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[7]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[11]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[7]\);

\U_Filter|Dout_FF|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[6]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[10]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[10]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[6]\);

\Dout_Del_ff|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[6]\ = DFFE(\U_Filter|Dout_FF|dffs[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|Dout_FF|dffs[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[6]\);

\PSlope_Compare|comparator|cmp[1]|comp|cmp[1]|aeb_out~0\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[1]|comp|cmp[1]|aeb_out\ = \Dout_Del_ff|dffs[7]\ & \U_Filter|Dout_FF|dffs[7]\ & (\U_Filter|Dout_FF|dffs[6]\ $ !\Dout_Del_ff|dffs[6]\) # !\Dout_Del_ff|dffs[7]\ & !\U_Filter|Dout_FF|dffs[7]\ & (\U_Filter|Dout_FF|dffs[6]\ $ 
-- !\Dout_Del_ff|dffs[6]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Del_ff|dffs[7]\,
	datab => \U_Filter|Dout_FF|dffs[7]\,
	datac => \U_Filter|Dout_FF|dffs[6]\,
	datad => \Dout_Del_ff|dffs[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[1]|comp|cmp[1]|aeb_out\);

\U_Filter|Dout_FF|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[5]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[9]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[5]\);

\U_Filter|Dout_FF|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[4]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[8]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[4]\);

\Dout_Del_ff|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[5]\ = DFFE(\U_Filter|Dout_FF|dffs[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|Dout_FF|dffs[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[5]\);

\PSlope_Compare|comparator|cmp[1]|comp|cmp[0]|agb_out~0\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[1]|comp|cmp[0]|agb_out\ = \U_Filter|Dout_FF|dffs[5]\ & (!\Dout_Del_ff|dffs[4]\ & \U_Filter|Dout_FF|dffs[4]\ # !\Dout_Del_ff|dffs[5]\) # !\U_Filter|Dout_FF|dffs[5]\ & !\Dout_Del_ff|dffs[4]\ & \U_Filter|Dout_FF|dffs[4]\ & 
-- !\Dout_Del_ff|dffs[5]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "40DC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Del_ff|dffs[4]\,
	datab => \U_Filter|Dout_FF|dffs[5]\,
	datac => \U_Filter|Dout_FF|dffs[4]\,
	datad => \Dout_Del_ff|dffs[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[1]|comp|cmp[0]|agb_out\);

\PSlope_Compare|comparator|cmp[1]|comp|cmp[1]|agb_out~0\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[1]|comp|cmp[1]|agb_out\ = \Dout_Del_ff|dffs[7]\ & \U_Filter|Dout_FF|dffs[7]\ & \U_Filter|Dout_FF|dffs[6]\ & !\Dout_Del_ff|dffs[6]\ # !\Dout_Del_ff|dffs[7]\ & (\U_Filter|Dout_FF|dffs[7]\ # \U_Filter|Dout_FF|dffs[6]\ & 
-- !\Dout_Del_ff|dffs[6]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "44D4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Del_ff|dffs[7]\,
	datab => \U_Filter|Dout_FF|dffs[7]\,
	datac => \U_Filter|Dout_FF|dffs[6]\,
	datad => \Dout_Del_ff|dffs[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[1]|comp|cmp[1]|agb_out\);

\PSlope_Compare|comparator|cmp[1]|comp|sub_comptree|gt_cmp_end|agb_out~I\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[1]|comp|sub_comptree|gt_cmp_end|agb_out\ = DFFE(\PSlope_Compare|comparator|cmp[1]|comp|cmp[1]|agb_out\ # \PSlope_Compare|comparator|cmp[1]|comp|cmp[1]|aeb_out\ & \PSlope_Compare|comparator|cmp[1]|comp|cmp[0]|agb_out\, 
-- GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \PSlope_Compare|comparator|cmp[1]|comp|cmp[1]|aeb_out\,
	datac => \PSlope_Compare|comparator|cmp[1]|comp|cmp[0]|agb_out\,
	datad => \PSlope_Compare|comparator|cmp[1]|comp|cmp[1]|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \PSlope_Compare|comparator|cmp[1]|comp|sub_comptree|gt_cmp_end|agb_out\);

\U_Filter|Dout_FF|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[3]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[3]\);

\Dout_Del_ff|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[3]\ = DFFE(\U_Filter|Dout_FF|dffs[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|Dout_FF|dffs[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[3]\);

\U_Filter|Dout_FF|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[2]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[2]\);

\U_Filter|Dout_FF|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[1]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[1]\);

\Dout_Del_ff|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[1]\ = DFFE(\U_Filter|Dout_FF|dffs[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|Dout_FF|dffs[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[1]\);

\U_Filter|Dout_FF|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[0]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[0]\);

\Dout_Del_ff|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Del_ff|dffs[0]\ = DFFE(\U_Filter|Dout_FF|dffs[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|Dout_FF|dffs[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_Del_ff|dffs[0]\);

\PSlope_Compare|comparator|cmp[0]|lcarry[0]~158_I\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[0]|lcarry[0]\ = CARRY(\U_Filter|Dout_FF|dffs[0]\ & !\Dout_Del_ff|dffs[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0022",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[0]\,
	datab => \Dout_Del_ff|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[0]|lcarry[0]~158\,
	cout => \PSlope_Compare|comparator|cmp[0]|lcarry[0]\);

\PSlope_Compare|comparator|cmp[0]|lcarry[1]~157_I\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[0]|lcarry[1]\ = CARRY(\U_Filter|Dout_FF|dffs[1]\ & \Dout_Del_ff|dffs[1]\ & !\PSlope_Compare|comparator|cmp[0]|lcarry[0]\ # !\U_Filter|Dout_FF|dffs[1]\ & (\Dout_Del_ff|dffs[1]\ # 
-- !\PSlope_Compare|comparator|cmp[0]|lcarry[0]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[1]\,
	datab => \Dout_Del_ff|dffs[1]\,
	cin => \PSlope_Compare|comparator|cmp[0]|lcarry[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[0]|lcarry[1]~157\,
	cout => \PSlope_Compare|comparator|cmp[0]|lcarry[1]\);

\PSlope_Compare|comparator|cmp[0]|lcarry[2]~156_I\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[0]|lcarry[2]\ = CARRY(\Dout_Del_ff|dffs[2]\ & \U_Filter|Dout_FF|dffs[2]\ & !\PSlope_Compare|comparator|cmp[0]|lcarry[1]\ # !\Dout_Del_ff|dffs[2]\ & (\U_Filter|Dout_FF|dffs[2]\ # 
-- !\PSlope_Compare|comparator|cmp[0]|lcarry[1]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Del_ff|dffs[2]\,
	datab => \U_Filter|Dout_FF|dffs[2]\,
	cin => \PSlope_Compare|comparator|cmp[0]|lcarry[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[0]|lcarry[2]~156\,
	cout => \PSlope_Compare|comparator|cmp[0]|lcarry[2]\);

\PSlope_Compare|comparator|cmp[0]|agb_out_node\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|cmp[0]|$00006\ = \U_Filter|Dout_FF|dffs[3]\ & (\PSlope_Compare|comparator|cmp[0]|lcarry[2]\ # !\Dout_Del_ff|dffs[3]\) # !\U_Filter|Dout_FF|dffs[3]\ & (\PSlope_Compare|comparator|cmp[0]|lcarry[2]\ & !\Dout_Del_ff|dffs[3]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A0FA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[3]\,
	datad => \Dout_Del_ff|dffs[3]\,
	cin => \PSlope_Compare|comparator|cmp[0]|lcarry[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|cmp[0]|$00006\);

\PSlope_Compare|comparator|cmp[0]|agb_out~I\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[0]\ = CARRY()

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCF0",
	operation_mode => "qfbk_counter",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \PSlope_Compare|comparator|cmp[0]|$00006\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \PSlope_Compare|comparator|cmp[0]|agb_out\,
	cout => \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[0]\);

\PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[1]~3_I\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[1]\ = CARRY(!\PSlope_Compare|comparator|cmp[1]|comp|sub_comptree|gt_cmp_end|agb_out\ & (!\PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[0]\ # 
-- !\PSlope_Compare|comparator|cmp[1]|comp|sub_comptree|eq_cmp_end|aeb_out\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0013",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \PSlope_Compare|comparator|cmp[1]|comp|sub_comptree|eq_cmp_end|aeb_out\,
	datab => \PSlope_Compare|comparator|cmp[1]|comp|sub_comptree|gt_cmp_end|agb_out\,
	cin => \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[1]~3\,
	cout => \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[1]\);

\PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[2]~2_I\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[2]\ = CARRY(\PSlope_Compare|comparator|cmp[2]|comp|sub_comptree|gt_cmp_end|agb_out\ # \PSlope_Compare|comparator|cmp[2]|comp|sub_comptree|eq_cmp_end|aeb_out\ & 
-- !\PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CE",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \PSlope_Compare|comparator|cmp[2]|comp|sub_comptree|eq_cmp_end|aeb_out\,
	datab => \PSlope_Compare|comparator|cmp[2]|comp|sub_comptree|gt_cmp_end|agb_out\,
	cin => \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[2]~2\,
	cout => \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[2]\);

\PSlope_Compare|comparator|sub_comptree|gt_cmp_end|agb_out~0\ : apex20ke_lcell
-- Equation(s):
-- \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|agb_out\ = \PSlope_Compare|comparator|cmp[3]|comp|sub_comptree|gt_cmp_end|agb_out\ # \PSlope_Compare|comparator|cmp[3]|comp|sub_comptree|eq_cmp_end|aeb_out\ & 
-- \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[2]\

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "FFA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \PSlope_Compare|comparator|cmp[3]|comp|sub_comptree|eq_cmp_end|aeb_out\,
	datad => \PSlope_Compare|comparator|cmp[3]|comp|sub_comptree|gt_cmp_end|agb_out\,
	cin => \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|lcarry[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|agb_out\);

\Tlevel[15]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(15),
	combout => \Tlevel[15]~combout\);

\tlevel_reg_ff|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[15]\ = DFFE(\Tlevel[15]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Tlevel[15]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[15]\);

\U_Filter|Dout_FF|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[13]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[17]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[17]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[13]\);

\U_Filter|Dout_FF|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \U_Filter|Dout_FF|dffs[12]\ = DFFE(\U_Filter|DS2|Dout_ff|dffs[16]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|DS2|Dout_ff|dffs[16]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \U_Filter|Dout_FF|dffs[12]\);

\Tlevel[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(11),
	combout => \Tlevel[11]~combout\);

\tlevel_reg_ff|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[11]\ = DFFE(\Tlevel[11]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Tlevel[11]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[11]\);

\Tlevel[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(10),
	combout => \Tlevel[10]~combout\);

\tlevel_reg_ff|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[10]\ = DFFE(\Tlevel[10]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Tlevel[10]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[10]\);

\Tlevel[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Tlevel(6),
	combout => \Tlevel[6]~combout\);

\tlevel_reg_ff|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[6]\ = DFFE(\Tlevel[6]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Tlevel[6]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[6]\);

\level_cmp_compare|comparator|cmp_end|lcarry[0]~1058_I\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|lcarry[0]\ = CARRY(!\tlevel_reg_ff|dffs[0]\ & \U_Filter|Dout_FF|dffs[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0044",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[0]\,
	datab => \U_Filter|Dout_FF|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|lcarry[0]~1058\,
	cout => \level_cmp_compare|comparator|cmp_end|lcarry[0]\);

\level_cmp_compare|comparator|cmp_end|lcarry[1]~1057_I\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|lcarry[1]\ = CARRY(\tlevel_reg_ff|dffs[1]\ & (!\level_cmp_compare|comparator|cmp_end|lcarry[0]\ # !\U_Filter|Dout_FF|dffs[1]\) # !\tlevel_reg_ff|dffs[1]\ & !\U_Filter|Dout_FF|dffs[1]\ & 
-- !\level_cmp_compare|comparator|cmp_end|lcarry[0]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[1]\,
	datab => \U_Filter|Dout_FF|dffs[1]\,
	cin => \level_cmp_compare|comparator|cmp_end|lcarry[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|lcarry[1]~1057\,
	cout => \level_cmp_compare|comparator|cmp_end|lcarry[1]\);

\level_cmp_compare|comparator|cmp_end|lcarry[2]~1056_I\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|lcarry[2]\ = CARRY(\tlevel_reg_ff|dffs[2]\ & \U_Filter|Dout_FF|dffs[2]\ & !\level_cmp_compare|comparator|cmp_end|lcarry[1]\ # !\tlevel_reg_ff|dffs[2]\ & (\U_Filter|Dout_FF|dffs[2]\ # 
-- !\level_cmp_compare|comparator|cmp_end|lcarry[1]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[2]\,
	datab => \U_Filter|Dout_FF|dffs[2]\,
	cin => \level_cmp_compare|comparator|cmp_end|lcarry[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|lcarry[2]~1056\,
	cout => \level_cmp_compare|comparator|cmp_end|lcarry[2]\);

\level_cmp_compare|comparator|cmp_end|lcarry[3]~1055_I\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|lcarry[3]\ = CARRY(\tlevel_reg_ff|dffs[3]\ & (!\level_cmp_compare|comparator|cmp_end|lcarry[2]\ # !\U_Filter|Dout_FF|dffs[3]\) # !\tlevel_reg_ff|dffs[3]\ & !\U_Filter|Dout_FF|dffs[3]\ & 
-- !\level_cmp_compare|comparator|cmp_end|lcarry[2]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[3]\,
	datab => \U_Filter|Dout_FF|dffs[3]\,
	cin => \level_cmp_compare|comparator|cmp_end|lcarry[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|lcarry[3]~1055\,
	cout => \level_cmp_compare|comparator|cmp_end|lcarry[3]\);

\level_cmp_compare|comparator|cmp_end|lcarry[4]~1054_I\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|lcarry[4]\ = CARRY(\tlevel_reg_ff|dffs[4]\ & \U_Filter|Dout_FF|dffs[4]\ & !\level_cmp_compare|comparator|cmp_end|lcarry[3]\ # !\tlevel_reg_ff|dffs[4]\ & (\U_Filter|Dout_FF|dffs[4]\ # 
-- !\level_cmp_compare|comparator|cmp_end|lcarry[3]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[4]\,
	datab => \U_Filter|Dout_FF|dffs[4]\,
	cin => \level_cmp_compare|comparator|cmp_end|lcarry[3]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|lcarry[4]~1054\,
	cout => \level_cmp_compare|comparator|cmp_end|lcarry[4]\);

\level_cmp_compare|comparator|cmp_end|lcarry[5]~1053_I\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|lcarry[5]\ = CARRY(\tlevel_reg_ff|dffs[5]\ & (!\level_cmp_compare|comparator|cmp_end|lcarry[4]\ # !\U_Filter|Dout_FF|dffs[5]\) # !\tlevel_reg_ff|dffs[5]\ & !\U_Filter|Dout_FF|dffs[5]\ & 
-- !\level_cmp_compare|comparator|cmp_end|lcarry[4]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[5]\,
	datab => \U_Filter|Dout_FF|dffs[5]\,
	cin => \level_cmp_compare|comparator|cmp_end|lcarry[4]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|lcarry[5]~1053\,
	cout => \level_cmp_compare|comparator|cmp_end|lcarry[5]\);

\level_cmp_compare|comparator|cmp_end|lcarry[6]~1052_I\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|lcarry[6]\ = CARRY(\U_Filter|Dout_FF|dffs[6]\ & (!\level_cmp_compare|comparator|cmp_end|lcarry[5]\ # !\tlevel_reg_ff|dffs[6]\) # !\U_Filter|Dout_FF|dffs[6]\ & !\tlevel_reg_ff|dffs[6]\ & 
-- !\level_cmp_compare|comparator|cmp_end|lcarry[5]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[6]\,
	datab => \tlevel_reg_ff|dffs[6]\,
	cin => \level_cmp_compare|comparator|cmp_end|lcarry[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|lcarry[6]~1052\,
	cout => \level_cmp_compare|comparator|cmp_end|lcarry[6]\);

\level_cmp_compare|comparator|cmp_end|lcarry[7]~1051_I\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|lcarry[7]\ = CARRY(\tlevel_reg_ff|dffs[7]\ & (!\level_cmp_compare|comparator|cmp_end|lcarry[6]\ # !\U_Filter|Dout_FF|dffs[7]\) # !\tlevel_reg_ff|dffs[7]\ & !\U_Filter|Dout_FF|dffs[7]\ & 
-- !\level_cmp_compare|comparator|cmp_end|lcarry[6]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[7]\,
	datab => \U_Filter|Dout_FF|dffs[7]\,
	cin => \level_cmp_compare|comparator|cmp_end|lcarry[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|lcarry[7]~1051\,
	cout => \level_cmp_compare|comparator|cmp_end|lcarry[7]\);

\level_cmp_compare|comparator|cmp_end|lcarry[8]~1050_I\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|lcarry[8]\ = CARRY(\tlevel_reg_ff|dffs[8]\ & \U_Filter|Dout_FF|dffs[8]\ & !\level_cmp_compare|comparator|cmp_end|lcarry[7]\ # !\tlevel_reg_ff|dffs[8]\ & (\U_Filter|Dout_FF|dffs[8]\ # 
-- !\level_cmp_compare|comparator|cmp_end|lcarry[7]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[8]\,
	datab => \U_Filter|Dout_FF|dffs[8]\,
	cin => \level_cmp_compare|comparator|cmp_end|lcarry[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|lcarry[8]~1050\,
	cout => \level_cmp_compare|comparator|cmp_end|lcarry[8]\);

\level_cmp_compare|comparator|cmp_end|lcarry[9]~1049_I\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|lcarry[9]\ = CARRY(\tlevel_reg_ff|dffs[9]\ & (!\level_cmp_compare|comparator|cmp_end|lcarry[8]\ # !\U_Filter|Dout_FF|dffs[9]\) # !\tlevel_reg_ff|dffs[9]\ & !\U_Filter|Dout_FF|dffs[9]\ & 
-- !\level_cmp_compare|comparator|cmp_end|lcarry[8]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[9]\,
	datab => \U_Filter|Dout_FF|dffs[9]\,
	cin => \level_cmp_compare|comparator|cmp_end|lcarry[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|lcarry[9]~1049\,
	cout => \level_cmp_compare|comparator|cmp_end|lcarry[9]\);

\level_cmp_compare|comparator|cmp_end|lcarry[10]~1048_I\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|lcarry[10]\ = CARRY(\U_Filter|Dout_FF|dffs[10]\ & (!\level_cmp_compare|comparator|cmp_end|lcarry[9]\ # !\tlevel_reg_ff|dffs[10]\) # !\U_Filter|Dout_FF|dffs[10]\ & !\tlevel_reg_ff|dffs[10]\ & 
-- !\level_cmp_compare|comparator|cmp_end|lcarry[9]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[10]\,
	datab => \tlevel_reg_ff|dffs[10]\,
	cin => \level_cmp_compare|comparator|cmp_end|lcarry[9]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|lcarry[10]~1048\,
	cout => \level_cmp_compare|comparator|cmp_end|lcarry[10]\);

\level_cmp_compare|comparator|cmp_end|lcarry[11]~1047_I\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|lcarry[11]\ = CARRY(\U_Filter|Dout_FF|dffs[11]\ & \tlevel_reg_ff|dffs[11]\ & !\level_cmp_compare|comparator|cmp_end|lcarry[10]\ # !\U_Filter|Dout_FF|dffs[11]\ & (\tlevel_reg_ff|dffs[11]\ # 
-- !\level_cmp_compare|comparator|cmp_end|lcarry[10]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[11]\,
	datab => \tlevel_reg_ff|dffs[11]\,
	cin => \level_cmp_compare|comparator|cmp_end|lcarry[10]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|lcarry[11]~1047\,
	cout => \level_cmp_compare|comparator|cmp_end|lcarry[11]\);

\level_cmp_compare|comparator|cmp_end|lcarry[12]~1046_I\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|lcarry[12]\ = CARRY(\tlevel_reg_ff|dffs[12]\ & \U_Filter|Dout_FF|dffs[12]\ & !\level_cmp_compare|comparator|cmp_end|lcarry[11]\ # !\tlevel_reg_ff|dffs[12]\ & (\U_Filter|Dout_FF|dffs[12]\ # 
-- !\level_cmp_compare|comparator|cmp_end|lcarry[11]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[12]\,
	datab => \U_Filter|Dout_FF|dffs[12]\,
	cin => \level_cmp_compare|comparator|cmp_end|lcarry[11]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|lcarry[12]~1046\,
	cout => \level_cmp_compare|comparator|cmp_end|lcarry[12]\);

\level_cmp_compare|comparator|cmp_end|lcarry[13]~1045_I\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|lcarry[13]\ = CARRY(\tlevel_reg_ff|dffs[13]\ & (!\level_cmp_compare|comparator|cmp_end|lcarry[12]\ # !\U_Filter|Dout_FF|dffs[13]\) # !\tlevel_reg_ff|dffs[13]\ & !\U_Filter|Dout_FF|dffs[13]\ & 
-- !\level_cmp_compare|comparator|cmp_end|lcarry[12]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[13]\,
	datab => \U_Filter|Dout_FF|dffs[13]\,
	cin => \level_cmp_compare|comparator|cmp_end|lcarry[12]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|lcarry[13]~1045\,
	cout => \level_cmp_compare|comparator|cmp_end|lcarry[13]\);

\level_cmp_compare|comparator|cmp_end|lcarry[14]~1044_I\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|lcarry[14]\ = CARRY(\tlevel_reg_ff|dffs[14]\ & \U_Filter|Dout_FF|dffs[14]\ & !\level_cmp_compare|comparator|cmp_end|lcarry[13]\ # !\tlevel_reg_ff|dffs[14]\ & (\U_Filter|Dout_FF|dffs[14]\ # 
-- !\level_cmp_compare|comparator|cmp_end|lcarry[13]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[14]\,
	datab => \U_Filter|Dout_FF|dffs[14]\,
	cin => \level_cmp_compare|comparator|cmp_end|lcarry[13]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|lcarry[14]~1044\,
	cout => \level_cmp_compare|comparator|cmp_end|lcarry[14]\);

\level_cmp_compare|comparator|cmp_end|agb_out_node\ : apex20ke_lcell
-- Equation(s):
-- \level_cmp_compare|comparator|cmp_end|agb_out\ = \U_Filter|Dout_FF|dffs[15]\ & (\level_cmp_compare|comparator|cmp_end|lcarry[14]\ & \tlevel_reg_ff|dffs[15]\) # !\U_Filter|Dout_FF|dffs[15]\ & (\level_cmp_compare|comparator|cmp_end|lcarry[14]\ # 
-- \tlevel_reg_ff|dffs[15]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F550",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[15]\,
	datad => \tlevel_reg_ff|dffs[15]\,
	cin => \level_cmp_compare|comparator|cmp_end|lcarry[14]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \level_cmp_compare|comparator|cmp_end|agb_out\);

\Disc_en~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_en,
	combout => \Disc_en~combout\);

\clock_proc~45_I\ : apex20ke_lcell
-- Equation(s):
-- \clock_proc~45\ = \level_cmp_compare|comparator|cmp_end|agb_out\ & \Disc_en~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \level_cmp_compare|comparator|cmp_end|agb_out\,
	datad => \Disc_en~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \clock_proc~45\);

\PSlope_Gen~I\ : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen = DFFE(PSlope_Gen_Cmp_Del & (PSlope_Gen # \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|agb_out\ & \clock_proc~45\) # !PSlope_Gen_Cmp_Del & \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|agb_out\ & PSlope_Gen, GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E8E0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Gen_Cmp_Del,
	datab => \PSlope_Compare|comparator|sub_comptree|gt_cmp_end|agb_out\,
	datac => PSlope_Gen,
	datad => \clock_proc~45\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Gen);

\PSlope_Gen_Del~I\ : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Del = DFFE(PSlope_Gen, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PSlope_Gen,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Gen_Del);

\FD_STATE~I\ : apex20ke_lcell
-- Equation(s):
-- FD_STATE = DFFE(FD_STATE & !\Equal~120\ # !FD_STATE & (!PSlope_Gen_Del & PSlope_Gen), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5530",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Equal~120\,
	datab => PSlope_Gen_Del,
	datac => PSlope_Gen,
	datad => FD_STATE,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FD_STATE);

\add~192_I\ : apex20ke_lcell
-- Equation(s):
-- \add~192\ = \clock_proc~2\ $ !\blevel_cnt[0]\
-- \add~194\ = CARRY(!\clock_proc~2\ & \blevel_cnt[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9944",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \clock_proc~2\,
	datab => \blevel_cnt[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~192\,
	cout => \add~194\);

\blevel_cnt[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_cnt[0]\ = DFFE(\add~192\ & (FD_STATE # !\Equal~126\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \add~192\,
	datac => FD_STATE,
	datad => \Equal~126\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_cnt[0]\);

\add~184_I\ : apex20ke_lcell
-- Equation(s):
-- \add~184\ = \blevel_cnt[2]\ $ (!\add~190\)
-- \add~186\ = CARRY(\blevel_cnt[2]\ & (!\add~190\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \blevel_cnt[2]\,
	cin => \add~190\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~184\,
	cout => \add~186\);

\add~196_I\ : apex20ke_lcell
-- Equation(s):
-- \add~196\ = \blevel_cnt[3]\ $ (\add~186\)
-- \add~198\ = CARRY(!\add~186\ # !\blevel_cnt[3]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \blevel_cnt[3]\,
	cin => \add~186\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~196\,
	cout => \add~198\);

\add~212_I\ : apex20ke_lcell
-- Equation(s):
-- \add~212\ = \blevel_cnt[4]\ $ (!\add~198\)
-- \add~214\ = CARRY(\blevel_cnt[4]\ & (!\add~198\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \blevel_cnt[4]\,
	cin => \add~198\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~212\,
	cout => \add~214\);

\add~208_I\ : apex20ke_lcell
-- Equation(s):
-- \add~208\ = \blevel_cnt[5]\ $ (\add~214\)
-- \add~210\ = CARRY(!\add~214\ # !\blevel_cnt[5]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \blevel_cnt[5]\,
	cin => \add~214\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~208\,
	cout => \add~210\);

\blevel_cnt[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_cnt[5]\ = DFFE(\add~208\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \add~208\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_cnt[5]\);

\add~200_I\ : apex20ke_lcell
-- Equation(s):
-- \add~200\ = \add~206\ $ \blevel_cnt[7]\

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \blevel_cnt[7]\,
	cin => \add~206\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~200\);

\blevel_cnt[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_cnt[7]\ = DFFE(\add~200\ & (FD_STATE # !\Equal~126\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \add~200\,
	datac => FD_STATE,
	datad => \Equal~126\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_cnt[7]\);

\blevel_cnt[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_cnt[4]\ = DFFE(\add~212\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \add~212\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_cnt[4]\);

\blevel_cnt[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_cnt[3]\ = DFFE(\add~196\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \add~196\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_cnt[3]\);

\blevel_cnt[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_cnt[2]\ = DFFE(\add~184\ & (FD_STATE # !\Equal~126\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \add~184\,
	datac => FD_STATE,
	datad => \Equal~126\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_cnt[2]\);

\Equal~124_I\ : apex20ke_lcell
-- Equation(s):
-- \Equal~128\ = \blevel_cnt[1]\ & !\blevel_cnt[3]\ & \blevel_cnt[0]\ & \blevel_cnt[2]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2000",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \blevel_cnt[1]\,
	datab => \blevel_cnt[3]\,
	datac => \blevel_cnt[0]\,
	datad => \blevel_cnt[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal~124\,
	cascout => \Equal~128\);

\Equal~126_I\ : apex20ke_lcell
-- Equation(s):
-- \Equal~126\ = (\blevel_cnt[6]\ & !\blevel_cnt[5]\ & \blevel_cnt[7]\ & !\blevel_cnt[4]\) & CASCADE(\Equal~128\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \blevel_cnt[6]\,
	datab => \blevel_cnt[5]\,
	datac => \blevel_cnt[7]\,
	datad => \blevel_cnt[4]\,
	cascin => \Equal~128\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal~126\);

\BLEVEL_UPD~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \BLEVEL_UPD~reg0\ = DFFE(!FD_STATE & \Equal~126\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => FD_STATE,
	datad => \Equal~126\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \BLEVEL_UPD~reg0\);

\FDISC~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \FDISC~reg0\ = DFFE(PSlope_Gen & !PSlope_Gen_Del, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PSlope_Gen,
	datad => PSlope_Gen_Del,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \FDISC~reg0\);

\TO_Cnt[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \TO_Cnt[1]\ = DFFE(GLOBAL(FD_STATE) & \TO_Cnt[1]\ $ \TO_Cnt[0]~46\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \TO_Cnt[1]~49\ = CARRY(!\TO_Cnt[0]~46\ # !\TO_Cnt[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \TO_Cnt[1]\,
	cin => \TO_Cnt[0]~46\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => ALT_INV_FD_STATE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TO_Cnt[1]\,
	cout => \TO_Cnt[1]~49\);

\TO_Cnt[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \TO_Cnt[2]\ = DFFE(GLOBAL(FD_STATE) & \TO_Cnt[2]\ $ !\TO_Cnt[1]~49\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \TO_Cnt[2]~55\ = CARRY(\TO_Cnt[2]\ & !\TO_Cnt[1]~49\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \TO_Cnt[2]\,
	cin => \TO_Cnt[1]~49\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => ALT_INV_FD_STATE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TO_Cnt[2]\,
	cout => \TO_Cnt[2]~55\);

\TO_Cnt[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \TO_Cnt[3]\ = DFFE(GLOBAL(FD_STATE) & \TO_Cnt[3]\ $ (\TO_Cnt[2]~55\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \TO_Cnt[3]~52\ = CARRY(!\TO_Cnt[2]~55\ # !\TO_Cnt[3]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \TO_Cnt[3]\,
	cin => \TO_Cnt[2]~55\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => ALT_INV_FD_STATE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TO_Cnt[3]\,
	cout => \TO_Cnt[3]~52\);

\TO_Cnt[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \TO_Cnt[4]\ = DFFE(GLOBAL(FD_STATE) & \TO_Cnt[4]\ $ (!\TO_Cnt[3]~52\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A5A5",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \TO_Cnt[4]\,
	cin => \TO_Cnt[3]~52\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => ALT_INV_FD_STATE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TO_Cnt[4]\);

\Equal~119_I\ : apex20ke_lcell
-- Equation(s):
-- \Equal~119\ = \TO_Cnt[3]\ & !\TO_Cnt[2]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \TO_Cnt[3]\,
	datad => \TO_Cnt[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal~119\);

\Equal~120_I\ : apex20ke_lcell
-- Equation(s):
-- \Equal~120\ = \TO_Cnt[0]\ & \TO_Cnt[1]\ & !\TO_Cnt[4]\ & \Equal~119\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0800",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \TO_Cnt[0]\,
	datab => \TO_Cnt[1]\,
	datac => \TO_Cnt[4]\,
	datad => \Equal~119\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal~120\);

\PK_DONE~I\ : apex20ke_lcell
-- Equation(s):
-- PK_DONE = DFFE(\Equal~120\ & FD_STATE, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Equal~120\,
	datad => FD_STATE,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PK_DONE);

\Meas_Done~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \Meas_Done~reg0\ = DFFE(PK_DONE, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => PK_DONE,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Meas_Done~reg0\);

\BLEVEL_FF|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \BLEVEL_FF|dffs[0]\ = DFFE(\U_Filter|Dout_FF|dffs[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !FD_STATE)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|Dout_FF|dffs[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => ALT_INV_FD_STATE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \BLEVEL_FF|dffs[0]\);

\BLEVEL_FF|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \BLEVEL_FF|dffs[1]\ = DFFE(\U_Filter|Dout_FF|dffs[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !FD_STATE)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|Dout_FF|dffs[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => ALT_INV_FD_STATE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \BLEVEL_FF|dffs[1]\);

\BLEVEL_FF|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \BLEVEL_FF|dffs[2]\ = DFFE(\U_Filter|Dout_FF|dffs[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !FD_STATE)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|Dout_FF|dffs[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => ALT_INV_FD_STATE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \BLEVEL_FF|dffs[2]\);

\BLEVEL_FF|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \BLEVEL_FF|dffs[3]\ = DFFE(\U_Filter|Dout_FF|dffs[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !FD_STATE)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|Dout_FF|dffs[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => ALT_INV_FD_STATE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \BLEVEL_FF|dffs[3]\);

\BLEVEL_FF|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \BLEVEL_FF|dffs[4]\ = DFFE(\U_Filter|Dout_FF|dffs[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !FD_STATE)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \U_Filter|Dout_FF|dffs[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => ALT_INV_FD_STATE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \BLEVEL_FF|dffs[4]\);

\BLEVEL_FF|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \BLEVEL_FF|dffs[5]\ = DFFE(\U_Filter|Dout_FF|dffs[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !FD_STATE)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|Dout_FF|dffs[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => ALT_INV_FD_STATE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \BLEVEL_FF|dffs[5]\);

\BLEVEL_FF|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \BLEVEL_FF|dffs[6]\ = DFFE(\U_Filter|Dout_FF|dffs[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !FD_STATE)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|Dout_FF|dffs[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => ALT_INV_FD_STATE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \BLEVEL_FF|dffs[6]\);

\BLEVEL_FF|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \BLEVEL_FF|dffs[7]\ = DFFE(\U_Filter|Dout_FF|dffs[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !FD_STATE)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|Dout_FF|dffs[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => ALT_INV_FD_STATE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \BLEVEL_FF|dffs[7]\);

\ROI_WE~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ROI_WE,
	combout => \ROI_WE~combout\);

\DSP_RAMA[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(0),
	combout => \DSP_RAMA[0]~combout\);

\~GND~I\ : apex20ke_lcell
-- Equation(s):
-- \~GND\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \~GND\);

\Dout_Max_Clr~13_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Max_Clr~13\ = \Meas_Done~reg0\ # !PSlope_Gen_Del & !FD_STATE & PSlope_Gen

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ABAA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Meas_Done~reg0\,
	datab => PSlope_Gen_Del,
	datac => FD_STATE,
	datad => PSlope_Gen,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Max_Clr~13\);

\Int_CPEak_FF|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[14]\ = DFFE(\U_Filter|Dout_FF|dffs[14]\ & !\Dout_Max_Clr~13\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|Dout_FF|dffs[14]\,
	datad => \Dout_Max_Clr~13\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[14]\);

\Int_CPEak_FF|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[12]\ = DFFE(\U_Filter|Dout_FF|dffs[12]\ & (!\Dout_Max_Clr~13\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0A0A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[12]\,
	datac => \Dout_Max_Clr~13\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[12]\);

\Int_CPEak_FF|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[11]\ = DFFE(!\Dout_Max_Clr~13\ & \U_Filter|Dout_FF|dffs[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Dout_Max_Clr~13\,
	datad => \U_Filter|Dout_FF|dffs[11]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[11]\);

\Int_CPEak_FF|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[10]\ = DFFE(\U_Filter|Dout_FF|dffs[10]\ & !\Dout_Max_Clr~13\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|Dout_FF|dffs[10]\,
	datad => \Dout_Max_Clr~13\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[10]\);

\Int_CPEak_FF|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[9]\ = DFFE(!\Dout_Max_Clr~13\ & \U_Filter|Dout_FF|dffs[9]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Dout_Max_Clr~13\,
	datad => \U_Filter|Dout_FF|dffs[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[9]\);

\Int_CPEak_FF|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[8]\ = DFFE(\U_Filter|Dout_FF|dffs[8]\ & !\Dout_Max_Clr~13\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|Dout_FF|dffs[8]\,
	datad => \Dout_Max_Clr~13\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[8]\);

\Int_CPEak_FF|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[7]\ = DFFE(!\Dout_Max_Clr~13\ & \U_Filter|Dout_FF|dffs[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Dout_Max_Clr~13\,
	datad => \U_Filter|Dout_FF|dffs[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[7]\);

\Int_CPEak_FF|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[6]\ = DFFE(!\Dout_Max_Clr~13\ & \U_Filter|Dout_FF|dffs[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Dout_Max_Clr~13\,
	datad => \U_Filter|Dout_FF|dffs[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[6]\);

\Int_CPEak_FF|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[5]\ = DFFE(!\Dout_Max_Clr~13\ & \U_Filter|Dout_FF|dffs[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Dout_Max_Clr~13\,
	datad => \U_Filter|Dout_FF|dffs[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[5]\);

\Int_CPEak_FF|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[4]\ = DFFE(!\Dout_Max_Clr~13\ & (\U_Filter|Dout_FF|dffs[4]\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5050",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Max_Clr~13\,
	datac => \U_Filter|Dout_FF|dffs[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[4]\);

\Int_CPEak_FF|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[3]\ = DFFE(\U_Filter|Dout_FF|dffs[3]\ & !\Dout_Max_Clr~13\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|Dout_FF|dffs[3]\,
	datad => \Dout_Max_Clr~13\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[3]\);

\Int_CPEak_FF|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[2]\ = DFFE(!\Dout_Max_Clr~13\ & (\U_Filter|Dout_FF|dffs[2]\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5050",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Max_Clr~13\,
	datac => \U_Filter|Dout_FF|dffs[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[2]\);

\Int_CPEak_FF|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[1]\ = DFFE(!\Dout_Max_Clr~13\ & (\U_Filter|Dout_FF|dffs[1]\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5050",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Max_Clr~13\,
	datac => \U_Filter|Dout_FF|dffs[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[1]\);

\Int_CPEak_FF|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[0]\ = DFFE(!\Dout_Max_Clr~13\ & \U_Filter|Dout_FF|dffs[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Dout_Max_Clr~13\,
	datad => \U_Filter|Dout_FF|dffs[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[0]\);

\dout_max_Compare|comparator|cmp_end|lcarry[0]~1058_I\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|lcarry[0]\ = CARRY(\U_Filter|Dout_FF|dffs[0]\ & !\Int_CPEak_FF|dffs[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0022",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[0]\,
	datab => \Int_CPEak_FF|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|lcarry[0]~1058\,
	cout => \dout_max_Compare|comparator|cmp_end|lcarry[0]\);

\dout_max_Compare|comparator|cmp_end|lcarry[1]~1057_I\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|lcarry[1]\ = CARRY(\U_Filter|Dout_FF|dffs[1]\ & \Int_CPEak_FF|dffs[1]\ & !\dout_max_Compare|comparator|cmp_end|lcarry[0]\ # !\U_Filter|Dout_FF|dffs[1]\ & (\Int_CPEak_FF|dffs[1]\ # 
-- !\dout_max_Compare|comparator|cmp_end|lcarry[0]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[1]\,
	datab => \Int_CPEak_FF|dffs[1]\,
	cin => \dout_max_Compare|comparator|cmp_end|lcarry[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|lcarry[1]~1057\,
	cout => \dout_max_Compare|comparator|cmp_end|lcarry[1]\);

\dout_max_Compare|comparator|cmp_end|lcarry[2]~1056_I\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|lcarry[2]\ = CARRY(\U_Filter|Dout_FF|dffs[2]\ & (!\dout_max_Compare|comparator|cmp_end|lcarry[1]\ # !\Int_CPEak_FF|dffs[2]\) # !\U_Filter|Dout_FF|dffs[2]\ & !\Int_CPEak_FF|dffs[2]\ & 
-- !\dout_max_Compare|comparator|cmp_end|lcarry[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[2]\,
	datab => \Int_CPEak_FF|dffs[2]\,
	cin => \dout_max_Compare|comparator|cmp_end|lcarry[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|lcarry[2]~1056\,
	cout => \dout_max_Compare|comparator|cmp_end|lcarry[2]\);

\dout_max_Compare|comparator|cmp_end|lcarry[3]~1055_I\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|lcarry[3]\ = CARRY(\U_Filter|Dout_FF|dffs[3]\ & \Int_CPEak_FF|dffs[3]\ & !\dout_max_Compare|comparator|cmp_end|lcarry[2]\ # !\U_Filter|Dout_FF|dffs[3]\ & (\Int_CPEak_FF|dffs[3]\ # 
-- !\dout_max_Compare|comparator|cmp_end|lcarry[2]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[3]\,
	datab => \Int_CPEak_FF|dffs[3]\,
	cin => \dout_max_Compare|comparator|cmp_end|lcarry[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|lcarry[3]~1055\,
	cout => \dout_max_Compare|comparator|cmp_end|lcarry[3]\);

\dout_max_Compare|comparator|cmp_end|lcarry[4]~1054_I\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|lcarry[4]\ = CARRY(\U_Filter|Dout_FF|dffs[4]\ & (!\dout_max_Compare|comparator|cmp_end|lcarry[3]\ # !\Int_CPEak_FF|dffs[4]\) # !\U_Filter|Dout_FF|dffs[4]\ & !\Int_CPEak_FF|dffs[4]\ & 
-- !\dout_max_Compare|comparator|cmp_end|lcarry[3]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[4]\,
	datab => \Int_CPEak_FF|dffs[4]\,
	cin => \dout_max_Compare|comparator|cmp_end|lcarry[3]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|lcarry[4]~1054\,
	cout => \dout_max_Compare|comparator|cmp_end|lcarry[4]\);

\dout_max_Compare|comparator|cmp_end|lcarry[5]~1053_I\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|lcarry[5]\ = CARRY(\U_Filter|Dout_FF|dffs[5]\ & \Int_CPEak_FF|dffs[5]\ & !\dout_max_Compare|comparator|cmp_end|lcarry[4]\ # !\U_Filter|Dout_FF|dffs[5]\ & (\Int_CPEak_FF|dffs[5]\ # 
-- !\dout_max_Compare|comparator|cmp_end|lcarry[4]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[5]\,
	datab => \Int_CPEak_FF|dffs[5]\,
	cin => \dout_max_Compare|comparator|cmp_end|lcarry[4]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|lcarry[5]~1053\,
	cout => \dout_max_Compare|comparator|cmp_end|lcarry[5]\);

\dout_max_Compare|comparator|cmp_end|lcarry[6]~1052_I\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|lcarry[6]\ = CARRY(\U_Filter|Dout_FF|dffs[6]\ & (!\dout_max_Compare|comparator|cmp_end|lcarry[5]\ # !\Int_CPEak_FF|dffs[6]\) # !\U_Filter|Dout_FF|dffs[6]\ & !\Int_CPEak_FF|dffs[6]\ & 
-- !\dout_max_Compare|comparator|cmp_end|lcarry[5]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[6]\,
	datab => \Int_CPEak_FF|dffs[6]\,
	cin => \dout_max_Compare|comparator|cmp_end|lcarry[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|lcarry[6]~1052\,
	cout => \dout_max_Compare|comparator|cmp_end|lcarry[6]\);

\dout_max_Compare|comparator|cmp_end|lcarry[7]~1051_I\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|lcarry[7]\ = CARRY(\U_Filter|Dout_FF|dffs[7]\ & \Int_CPEak_FF|dffs[7]\ & !\dout_max_Compare|comparator|cmp_end|lcarry[6]\ # !\U_Filter|Dout_FF|dffs[7]\ & (\Int_CPEak_FF|dffs[7]\ # 
-- !\dout_max_Compare|comparator|cmp_end|lcarry[6]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[7]\,
	datab => \Int_CPEak_FF|dffs[7]\,
	cin => \dout_max_Compare|comparator|cmp_end|lcarry[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|lcarry[7]~1051\,
	cout => \dout_max_Compare|comparator|cmp_end|lcarry[7]\);

\dout_max_Compare|comparator|cmp_end|lcarry[8]~1050_I\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|lcarry[8]\ = CARRY(\U_Filter|Dout_FF|dffs[8]\ & (!\dout_max_Compare|comparator|cmp_end|lcarry[7]\ # !\Int_CPEak_FF|dffs[8]\) # !\U_Filter|Dout_FF|dffs[8]\ & !\Int_CPEak_FF|dffs[8]\ & 
-- !\dout_max_Compare|comparator|cmp_end|lcarry[7]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[8]\,
	datab => \Int_CPEak_FF|dffs[8]\,
	cin => \dout_max_Compare|comparator|cmp_end|lcarry[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|lcarry[8]~1050\,
	cout => \dout_max_Compare|comparator|cmp_end|lcarry[8]\);

\dout_max_Compare|comparator|cmp_end|lcarry[9]~1049_I\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|lcarry[9]\ = CARRY(\U_Filter|Dout_FF|dffs[9]\ & \Int_CPEak_FF|dffs[9]\ & !\dout_max_Compare|comparator|cmp_end|lcarry[8]\ # !\U_Filter|Dout_FF|dffs[9]\ & (\Int_CPEak_FF|dffs[9]\ # 
-- !\dout_max_Compare|comparator|cmp_end|lcarry[8]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[9]\,
	datab => \Int_CPEak_FF|dffs[9]\,
	cin => \dout_max_Compare|comparator|cmp_end|lcarry[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|lcarry[9]~1049\,
	cout => \dout_max_Compare|comparator|cmp_end|lcarry[9]\);

\dout_max_Compare|comparator|cmp_end|lcarry[10]~1048_I\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|lcarry[10]\ = CARRY(\U_Filter|Dout_FF|dffs[10]\ & (!\dout_max_Compare|comparator|cmp_end|lcarry[9]\ # !\Int_CPEak_FF|dffs[10]\) # !\U_Filter|Dout_FF|dffs[10]\ & !\Int_CPEak_FF|dffs[10]\ & 
-- !\dout_max_Compare|comparator|cmp_end|lcarry[9]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[10]\,
	datab => \Int_CPEak_FF|dffs[10]\,
	cin => \dout_max_Compare|comparator|cmp_end|lcarry[9]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|lcarry[10]~1048\,
	cout => \dout_max_Compare|comparator|cmp_end|lcarry[10]\);

\dout_max_Compare|comparator|cmp_end|lcarry[11]~1047_I\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|lcarry[11]\ = CARRY(\U_Filter|Dout_FF|dffs[11]\ & \Int_CPEak_FF|dffs[11]\ & !\dout_max_Compare|comparator|cmp_end|lcarry[10]\ # !\U_Filter|Dout_FF|dffs[11]\ & (\Int_CPEak_FF|dffs[11]\ # 
-- !\dout_max_Compare|comparator|cmp_end|lcarry[10]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[11]\,
	datab => \Int_CPEak_FF|dffs[11]\,
	cin => \dout_max_Compare|comparator|cmp_end|lcarry[10]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|lcarry[11]~1047\,
	cout => \dout_max_Compare|comparator|cmp_end|lcarry[11]\);

\dout_max_Compare|comparator|cmp_end|lcarry[12]~1046_I\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|lcarry[12]\ = CARRY(\U_Filter|Dout_FF|dffs[12]\ & (!\dout_max_Compare|comparator|cmp_end|lcarry[11]\ # !\Int_CPEak_FF|dffs[12]\) # !\U_Filter|Dout_FF|dffs[12]\ & !\Int_CPEak_FF|dffs[12]\ & 
-- !\dout_max_Compare|comparator|cmp_end|lcarry[11]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[12]\,
	datab => \Int_CPEak_FF|dffs[12]\,
	cin => \dout_max_Compare|comparator|cmp_end|lcarry[11]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|lcarry[12]~1046\,
	cout => \dout_max_Compare|comparator|cmp_end|lcarry[12]\);

\dout_max_Compare|comparator|cmp_end|lcarry[13]~1045_I\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|lcarry[13]\ = CARRY(\Int_CPEak_FF|dffs[13]\ & (!\dout_max_Compare|comparator|cmp_end|lcarry[12]\ # !\U_Filter|Dout_FF|dffs[13]\) # !\Int_CPEak_FF|dffs[13]\ & !\U_Filter|Dout_FF|dffs[13]\ & 
-- !\dout_max_Compare|comparator|cmp_end|lcarry[12]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[13]\,
	datab => \U_Filter|Dout_FF|dffs[13]\,
	cin => \dout_max_Compare|comparator|cmp_end|lcarry[12]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|lcarry[13]~1045\,
	cout => \dout_max_Compare|comparator|cmp_end|lcarry[13]\);

\dout_max_Compare|comparator|cmp_end|lcarry[14]~1044_I\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|lcarry[14]\ = CARRY(\U_Filter|Dout_FF|dffs[14]\ & (!\dout_max_Compare|comparator|cmp_end|lcarry[13]\ # !\Int_CPEak_FF|dffs[14]\) # !\U_Filter|Dout_FF|dffs[14]\ & !\Int_CPEak_FF|dffs[14]\ & 
-- !\dout_max_Compare|comparator|cmp_end|lcarry[13]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \U_Filter|Dout_FF|dffs[14]\,
	datab => \Int_CPEak_FF|dffs[14]\,
	cin => \dout_max_Compare|comparator|cmp_end|lcarry[13]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|lcarry[14]~1044\,
	cout => \dout_max_Compare|comparator|cmp_end|lcarry[14]\);

\dout_max_Compare|comparator|cmp_end|agb_out_node\ : apex20ke_lcell
-- Equation(s):
-- \dout_max_Compare|comparator|cmp_end|agb_out\ = \U_Filter|Dout_FF|dffs[15]\ & \dout_max_Compare|comparator|cmp_end|lcarry[14]\ & \Int_CPEak_FF|dffs[15]\ # !\U_Filter|Dout_FF|dffs[15]\ & (\dout_max_Compare|comparator|cmp_end|lcarry[14]\ # 
-- \Int_CPEak_FF|dffs[15]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F330",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \U_Filter|Dout_FF|dffs[15]\,
	datad => \Int_CPEak_FF|dffs[15]\,
	cin => \dout_max_Compare|comparator|cmp_end|lcarry[14]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dout_max_Compare|comparator|cmp_end|agb_out\);

\Dout_Max_En~6_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Max_En~6\ = \Dout_Max_Clr~13\ # \level_cmp_compare|comparator|cmp_end|agb_out\ & \dout_max_Compare|comparator|cmp_end|agb_out\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FCF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \level_cmp_compare|comparator|cmp_end|agb_out\,
	datac => \Dout_Max_Clr~13\,
	datad => \dout_max_Compare|comparator|cmp_end|agb_out\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Max_En~6\);

\Int_CPEak_FF|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[15]\ = DFFE(\U_Filter|Dout_FF|dffs[15]\ & !\Dout_Max_Clr~13\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \U_Filter|Dout_FF|dffs[15]\,
	datad => \Dout_Max_Clr~13\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[15]\);

\Int_CPEak_FF|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \Int_CPEak_FF|dffs[13]\ = DFFE(\U_Filter|Dout_FF|dffs[13]\ & (!\Dout_Max_Clr~13\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \Dout_Max_En~6\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \U_Filter|Dout_FF|dffs[13]\,
	datad => \Dout_Max_Clr~13\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \Dout_Max_En~6\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Int_CPEak_FF|dffs[13]\);

\Max_Clamp_Compare|comparator|cmp_end|lcarry[5]~59_I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Compare|comparator|cmp_end|lcarry[5]~59\ = \Max_Clamp_Val_Cntr|wysi_counter|q[5]\
-- \Max_Clamp_Compare|comparator|cmp_end|lcarry[5]\ = CARRY(!\Max_Clamp_Val_Cntr|wysi_counter|q[5]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AA55",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Val_Cntr|wysi_counter|q[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[5]~59\,
	cout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[5]\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[4]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[4]\ = DFFE((\Max_Clamp_Ld~4\ & VCC) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Val_Cntr|wysi_counter|q[4]\ $ (\Max_Clamp_Inc~1\ & !\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[3]~COUT\)), GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )
-- \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[4]~COUT\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[4]\ & !\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[3]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Inc~1\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[4]\,
	datac => VCC,
	cin => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[3]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[4]\,
	cout => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[4]~COUT\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[6]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[6]\ = DFFE((\Max_Clamp_Ld~4\ & \~GND\) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Val_Cntr|wysi_counter|q[6]\ $ (\Max_Clamp_Inc~1\ & !\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[5]~COUT\)), GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )
-- \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[6]~COUT\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[6]\ & !\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[5]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Inc~1\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[6]\,
	datac => \~GND\,
	cin => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[5]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[6]\,
	cout => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[6]~COUT\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[7]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[7]\ = DFFE((\Max_Clamp_Ld~4\ & VCC) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Val_Cntr|wysi_counter|q[7]\ $ (\Max_Clamp_Inc~1\ & \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[6]~COUT\)), GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )
-- \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[7]~COUT\ = CARRY(!\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[6]~COUT\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[7]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Inc~1\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[7]\,
	datac => VCC,
	cin => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[6]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[7]\,
	cout => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[7]~COUT\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[8]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[8]\ = DFFE((\Max_Clamp_Ld~4\ & VCC) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Val_Cntr|wysi_counter|q[8]\ $ (\Max_Clamp_Inc~1\ & !\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[7]~COUT\)), GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )
-- \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[8]~COUT\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[8]\ & !\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[7]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Inc~1\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[8]\,
	datac => VCC,
	cin => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[7]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[8]\,
	cout => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[8]~COUT\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[9]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[9]\ = DFFE((\Max_Clamp_Ld~4\ & VCC) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Val_Cntr|wysi_counter|q[9]\ $ (\Max_Clamp_Inc~1\ & \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[8]~COUT\)), GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )
-- \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[9]~COUT\ = CARRY(!\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[8]~COUT\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[9]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Inc~1\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[9]\,
	datac => VCC,
	cin => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[8]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[9]\,
	cout => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[9]~COUT\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[10]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[10]\ = DFFE((\Max_Clamp_Ld~4\ & VCC) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Val_Cntr|wysi_counter|q[10]\ $ (\Max_Clamp_Inc~1\ & !\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[9]~COUT\)), GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )
-- \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[10]~COUT\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[10]\ & !\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[9]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Inc~1\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[10]\,
	datac => VCC,
	cin => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[9]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[10]\,
	cout => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[10]~COUT\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[11]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[11]\ = DFFE((\Max_Clamp_Ld~4\ & VCC) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Val_Cntr|wysi_counter|q[11]\ $ (\Max_Clamp_Inc~1\ & \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[10]~COUT\)), GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )
-- \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[11]~COUT\ = CARRY(!\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[10]~COUT\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[11]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Inc~1\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[11]\,
	datac => VCC,
	cin => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[10]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[11]\,
	cout => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[11]~COUT\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[12]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[12]\ = DFFE((\Max_Clamp_Ld~4\ & \~GND\) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Val_Cntr|wysi_counter|q[12]\ $ (\Max_Clamp_Inc~1\ & !\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[11]~COUT\)), GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )
-- \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[12]~COUT\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[12]\ & !\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[11]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Inc~1\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[12]\,
	datac => \~GND\,
	cin => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[11]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[12]\,
	cout => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[12]~COUT\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[0]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[0]\ = DFFE((\Max_Clamp_Ld~4\ & VCC) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Inc~1\ $ (\Max_Clamp_Val_Cntr|wysi_counter|q[0]\)), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[0]~COUT\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5AF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Inc~1\,
	datac => VCC,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[0]\,
	cout => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[0]~COUT\);

\Clamp_Dout_Compare|comparator|cmp_end|lcarry[0]~1058_I\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[0]\ = CARRY(\Int_CPEak_FF|dffs[0]\ & !\Max_Clamp_Val_Cntr|wysi_counter|q[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0022",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[0]\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[0]~1058\,
	cout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[0]\);

\Clamp_Dout_Compare|comparator|cmp_end|lcarry[1]~1057_I\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[1]\ = CARRY(\Int_CPEak_FF|dffs[1]\ & \Max_Clamp_Val_Cntr|wysi_counter|q[1]\ & !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[0]\ # !\Int_CPEak_FF|dffs[1]\ & (\Max_Clamp_Val_Cntr|wysi_counter|q[1]\ # 
-- !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[0]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[1]\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[1]\,
	cin => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[1]~1057\,
	cout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[1]\);

\Clamp_Dout_Compare|comparator|cmp_end|lcarry[2]~1056_I\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[2]\ = CARRY(\Int_CPEak_FF|dffs[2]\ & (!\Clamp_Dout_Compare|comparator|cmp_end|lcarry[1]\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[2]\) # !\Int_CPEak_FF|dffs[2]\ & !\Max_Clamp_Val_Cntr|wysi_counter|q[2]\ & 
-- !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[2]\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[2]\,
	cin => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[2]~1056\,
	cout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[2]\);

\Clamp_Dout_Compare|comparator|cmp_end|lcarry[3]~1055_I\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[3]\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[3]\ & (!\Clamp_Dout_Compare|comparator|cmp_end|lcarry[2]\ # !\Int_CPEak_FF|dffs[3]\) # !\Max_Clamp_Val_Cntr|wysi_counter|q[3]\ & !\Int_CPEak_FF|dffs[3]\ & 
-- !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[2]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Val_Cntr|wysi_counter|q[3]\,
	datab => \Int_CPEak_FF|dffs[3]\,
	cin => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[3]~1055\,
	cout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[3]\);

\Clamp_Dout_Compare|comparator|cmp_end|lcarry[4]~1054_I\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[4]\ = CARRY(\Int_CPEak_FF|dffs[4]\ & (!\Clamp_Dout_Compare|comparator|cmp_end|lcarry[3]\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[4]\) # !\Int_CPEak_FF|dffs[4]\ & !\Max_Clamp_Val_Cntr|wysi_counter|q[4]\ & 
-- !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[3]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[4]\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[4]\,
	cin => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[3]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[4]~1054\,
	cout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[4]\);

\Clamp_Dout_Compare|comparator|cmp_end|lcarry[5]~1053_I\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[5]\ = CARRY(\Int_CPEak_FF|dffs[5]\ & \Max_Clamp_Compare|comparator|cmp_end|lcarry[5]~59\ & !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[4]\ # !\Int_CPEak_FF|dffs[5]\ & 
-- (\Max_Clamp_Compare|comparator|cmp_end|lcarry[5]~59\ # !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[4]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[5]\,
	datab => \Max_Clamp_Compare|comparator|cmp_end|lcarry[5]~59\,
	cin => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[4]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[5]~1053\,
	cout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[5]\);

\Clamp_Dout_Compare|comparator|cmp_end|lcarry[6]~1052_I\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[6]\ = CARRY(\Int_CPEak_FF|dffs[6]\ & (!\Clamp_Dout_Compare|comparator|cmp_end|lcarry[5]\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[6]\) # !\Int_CPEak_FF|dffs[6]\ & !\Max_Clamp_Val_Cntr|wysi_counter|q[6]\ & 
-- !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[5]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[6]\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[6]\,
	cin => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[6]~1052\,
	cout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[6]\);

\Clamp_Dout_Compare|comparator|cmp_end|lcarry[7]~1051_I\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[7]\ = CARRY(\Int_CPEak_FF|dffs[7]\ & \Max_Clamp_Val_Cntr|wysi_counter|q[7]\ & !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[6]\ # !\Int_CPEak_FF|dffs[7]\ & (\Max_Clamp_Val_Cntr|wysi_counter|q[7]\ # 
-- !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[6]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[7]\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[7]\,
	cin => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[7]~1051\,
	cout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[7]\);

\Clamp_Dout_Compare|comparator|cmp_end|lcarry[8]~1050_I\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[8]\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[8]\ & \Int_CPEak_FF|dffs[8]\ & !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[7]\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[8]\ & (\Int_CPEak_FF|dffs[8]\ # 
-- !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[7]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Val_Cntr|wysi_counter|q[8]\,
	datab => \Int_CPEak_FF|dffs[8]\,
	cin => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[8]~1050\,
	cout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[8]\);

\Clamp_Dout_Compare|comparator|cmp_end|lcarry[9]~1049_I\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[9]\ = CARRY(\Int_CPEak_FF|dffs[9]\ & \Max_Clamp_Val_Cntr|wysi_counter|q[9]\ & !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[8]\ # !\Int_CPEak_FF|dffs[9]\ & (\Max_Clamp_Val_Cntr|wysi_counter|q[9]\ # 
-- !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[8]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[9]\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[9]\,
	cin => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[9]~1049\,
	cout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[9]\);

\Clamp_Dout_Compare|comparator|cmp_end|lcarry[10]~1048_I\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[10]\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[10]\ & \Int_CPEak_FF|dffs[10]\ & !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[9]\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[10]\ & (\Int_CPEak_FF|dffs[10]\ # 
-- !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[9]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Val_Cntr|wysi_counter|q[10]\,
	datab => \Int_CPEak_FF|dffs[10]\,
	cin => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[9]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[10]~1048\,
	cout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[10]\);

\Clamp_Dout_Compare|comparator|cmp_end|lcarry[11]~1047_I\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[11]\ = CARRY(\Int_CPEak_FF|dffs[11]\ & \Max_Clamp_Val_Cntr|wysi_counter|q[11]\ & !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[10]\ # !\Int_CPEak_FF|dffs[11]\ & (\Max_Clamp_Val_Cntr|wysi_counter|q[11]\ # 
-- !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[10]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[11]\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[11]\,
	cin => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[10]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[11]~1047\,
	cout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[11]\);

\Clamp_Dout_Compare|comparator|cmp_end|lcarry[12]~1046_I\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[12]\ = CARRY(\Int_CPEak_FF|dffs[12]\ & (!\Clamp_Dout_Compare|comparator|cmp_end|lcarry[11]\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[12]\) # !\Int_CPEak_FF|dffs[12]\ & !\Max_Clamp_Val_Cntr|wysi_counter|q[12]\ & 
-- !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[11]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[12]\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[12]\,
	cin => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[11]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[12]~1046\,
	cout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[12]\);

\Clamp_Dout_Compare|comparator|cmp_end|lcarry[13]~1045_I\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[13]\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[13]\ & (!\Clamp_Dout_Compare|comparator|cmp_end|lcarry[12]\ # !\Int_CPEak_FF|dffs[13]\) # !\Max_Clamp_Val_Cntr|wysi_counter|q[13]\ & !\Int_CPEak_FF|dffs[13]\ & 
-- !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[12]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Val_Cntr|wysi_counter|q[13]\,
	datab => \Int_CPEak_FF|dffs[13]\,
	cin => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[12]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[13]~1045\,
	cout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[13]\);

\Clamp_Dout_Compare|comparator|cmp_end|lcarry[14]~1044_I\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[14]\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[14]\ & \Int_CPEak_FF|dffs[14]\ & !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[13]\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[14]\ & (\Int_CPEak_FF|dffs[14]\ # 
-- !\Clamp_Dout_Compare|comparator|cmp_end|lcarry[13]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Val_Cntr|wysi_counter|q[14]\,
	datab => \Int_CPEak_FF|dffs[14]\,
	cin => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[13]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[14]~1044\,
	cout => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[14]\);

\Clamp_Dout_Compare|comparator|cmp_end|agb_out_node\ : apex20ke_lcell
-- Equation(s):
-- \Clamp_Dout_Compare|comparator|cmp_end|agb_out\ = \Max_Clamp_Val_Cntr|wysi_counter|q[15]\ & (\Clamp_Dout_Compare|comparator|cmp_end|lcarry[14]\ # !\Int_CPEak_FF|dffs[15]\) # !\Max_Clamp_Val_Cntr|wysi_counter|q[15]\ & 
-- \Clamp_Dout_Compare|comparator|cmp_end|lcarry[14]\ & !\Int_CPEak_FF|dffs[15]\

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C0FC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[15]\,
	datad => \Int_CPEak_FF|dffs[15]\,
	cin => \Clamp_Dout_Compare|comparator|cmp_end|lcarry[14]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\);

\Max_Clamp_Inc~1_I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Inc~1\ = \Max_Clamp_Compare|comparator|cmp_end|agb_out\ & PK_DONE & \Clamp_Dout_Compare|comparator|cmp_end|agb_out\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Max_Clamp_Compare|comparator|cmp_end|agb_out\,
	datac => PK_DONE,
	datad => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Max_Clamp_Inc~1\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[13]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[13]\ = DFFE((\Max_Clamp_Ld~4\ & \~GND\) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Val_Cntr|wysi_counter|q[13]\ $ (\Max_Clamp_Inc~1\ & \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[12]~COUT\)), GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )
-- \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[13]~COUT\ = CARRY(!\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[12]~COUT\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[13]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Inc~1\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[13]\,
	datac => \~GND\,
	cin => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[12]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[13]\,
	cout => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[13]~COUT\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[14]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[14]\ = DFFE((\Max_Clamp_Ld~4\ & \~GND\) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Val_Cntr|wysi_counter|q[14]\ $ (\Max_Clamp_Inc~1\ & !\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[13]~COUT\)), GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )
-- \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[14]~COUT\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[14]\ & !\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[13]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Inc~1\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[14]\,
	datac => \~GND\,
	cin => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[13]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[14]\,
	cout => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[14]~COUT\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[15]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[15]\ = DFFE((\Max_Clamp_Ld~4\ & \~GND\) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Val_Cntr|wysi_counter|q[15]\ $ (\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[14]~COUT\ & \Max_Clamp_Inc~1\)), GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CCC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[15]\,
	datac => \~GND\,
	datad => \Max_Clamp_Inc~1\,
	cin => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[14]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[15]\);

\Max_Clamp_Compare|comparator|cmp_end|lcarry[6]~58_I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Compare|comparator|cmp_end|lcarry[6]\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[6]\ # !\Max_Clamp_Compare|comparator|cmp_end|lcarry[5]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Val_Cntr|wysi_counter|q[6]\,
	cin => \Max_Clamp_Compare|comparator|cmp_end|lcarry[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[6]~58\,
	cout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[6]\);

\Max_Clamp_Compare|comparator|cmp_end|lcarry[7]~57_I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Compare|comparator|cmp_end|lcarry[7]\ = CARRY(!\Max_Clamp_Compare|comparator|cmp_end|lcarry[6]\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[7]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "005F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Val_Cntr|wysi_counter|q[7]\,
	cin => \Max_Clamp_Compare|comparator|cmp_end|lcarry[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[7]~57\,
	cout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[7]\);

\Max_Clamp_Compare|comparator|cmp_end|lcarry[8]~56_I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Compare|comparator|cmp_end|lcarry[8]\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[8]\ & (!\Max_Clamp_Compare|comparator|cmp_end|lcarry[7]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000A",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Val_Cntr|wysi_counter|q[8]\,
	cin => \Max_Clamp_Compare|comparator|cmp_end|lcarry[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[8]~56\,
	cout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[8]\);

\Max_Clamp_Compare|comparator|cmp_end|lcarry[9]~55_I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Compare|comparator|cmp_end|lcarry[9]\ = CARRY(!\Max_Clamp_Compare|comparator|cmp_end|lcarry[8]\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[9]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "005F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Val_Cntr|wysi_counter|q[9]\,
	cin => \Max_Clamp_Compare|comparator|cmp_end|lcarry[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[9]~55\,
	cout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[9]\);

\Max_Clamp_Compare|comparator|cmp_end|lcarry[10]~54_I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Compare|comparator|cmp_end|lcarry[10]\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[10]\ & (!\Max_Clamp_Compare|comparator|cmp_end|lcarry[9]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000A",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Val_Cntr|wysi_counter|q[10]\,
	cin => \Max_Clamp_Compare|comparator|cmp_end|lcarry[9]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[10]~54\,
	cout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[10]\);

\Max_Clamp_Compare|comparator|cmp_end|lcarry[11]~53_I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Compare|comparator|cmp_end|lcarry[11]\ = CARRY(!\Max_Clamp_Compare|comparator|cmp_end|lcarry[10]\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[11]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "005F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Val_Cntr|wysi_counter|q[11]\,
	cin => \Max_Clamp_Compare|comparator|cmp_end|lcarry[10]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[11]~53\,
	cout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[11]\);

\Max_Clamp_Compare|comparator|cmp_end|lcarry[12]~52_I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Compare|comparator|cmp_end|lcarry[12]\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[12]\ # !\Max_Clamp_Compare|comparator|cmp_end|lcarry[11]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Val_Cntr|wysi_counter|q[12]\,
	cin => \Max_Clamp_Compare|comparator|cmp_end|lcarry[11]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[12]~52\,
	cout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[12]\);

\Max_Clamp_Compare|comparator|cmp_end|lcarry[13]~51_I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Compare|comparator|cmp_end|lcarry[13]\ = CARRY(!\Max_Clamp_Val_Cntr|wysi_counter|q[13]\ & (!\Max_Clamp_Compare|comparator|cmp_end|lcarry[12]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Val_Cntr|wysi_counter|q[13]\,
	cin => \Max_Clamp_Compare|comparator|cmp_end|lcarry[12]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[13]~51\,
	cout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[13]\);

\Max_Clamp_Compare|comparator|cmp_end|lcarry[14]~50_I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Compare|comparator|cmp_end|lcarry[14]\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[14]\ # !\Max_Clamp_Compare|comparator|cmp_end|lcarry[13]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Val_Cntr|wysi_counter|q[14]\,
	cin => \Max_Clamp_Compare|comparator|cmp_end|lcarry[13]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[14]~50\,
	cout => \Max_Clamp_Compare|comparator|cmp_end|lcarry[14]\);

\Max_Clamp_Compare|comparator|cmp_end|agb_out_node~1\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Compare|comparator|cmp_end|agb_out\ = !\Max_Clamp_Compare|comparator|cmp_end|lcarry[14]\ & !\Max_Clamp_Val_Cntr|wysi_counter|q[15]\

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Max_Clamp_Val_Cntr|wysi_counter|q[15]\,
	cin => \Max_Clamp_Compare|comparator|cmp_end|lcarry[14]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Max_Clamp_Compare|comparator|cmp_end|agb_out\);

\Time_Enable~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Time_Enable,
	combout => \Time_Enable~combout\);

\Max_Clamp_Ld~4_I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Ld~4\ = \Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & !\Max_Clamp_Compare|comparator|cmp_end|agb_out\ & PK_DONE # !\Time_Enable~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "20FF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	datab => \Max_Clamp_Compare|comparator|cmp_end|agb_out\,
	datac => PK_DONE,
	datad => \Time_Enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Max_Clamp_Ld~4\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[1]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[1]\ = DFFE((\Max_Clamp_Ld~4\ & VCC) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Val_Cntr|wysi_counter|q[1]\ $ (\Max_Clamp_Inc~1\ & \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[0]~COUT\)), GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )
-- \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[1]~COUT\ = CARRY(!\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[0]~COUT\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Inc~1\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[1]\,
	datac => VCC,
	cin => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[0]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[1]\,
	cout => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[1]~COUT\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[2]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[2]\ = DFFE((\Max_Clamp_Ld~4\ & \~GND\) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Val_Cntr|wysi_counter|q[2]\ $ (\Max_Clamp_Inc~1\ & !\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[1]~COUT\)), GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )
-- \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[2]~COUT\ = CARRY(\Max_Clamp_Val_Cntr|wysi_counter|q[2]\ & !\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[1]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Inc~1\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[2]\,
	datac => \~GND\,
	cin => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[1]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[2]\,
	cout => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[2]~COUT\);

\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[3]\ : apex20ke_lcell
-- Equation(s):
-- \Max_Clamp_Val_Cntr|wysi_counter|q[3]\ = DFFE((\Max_Clamp_Ld~4\ & VCC) # (!\Max_Clamp_Ld~4\ & \Max_Clamp_Val_Cntr|wysi_counter|q[3]\ $ (\Max_Clamp_Inc~1\ & \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[2]~COUT\)), GLOBAL(\clk~combout\), 
-- !GLOBAL(\imr~combout\), , )
-- \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[3]~COUT\ = CARRY(!\Max_Clamp_Val_Cntr|wysi_counter|counter_cell[2]~COUT\ # !\Max_Clamp_Val_Cntr|wysi_counter|q[3]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Clamp_Inc~1\,
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[3]\,
	datac => VCC,
	cin => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[2]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sload => \Max_Clamp_Ld~4\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Clamp_Val_Cntr|wysi_counter|q[3]\,
	cout => \Max_Clamp_Val_Cntr|wysi_counter|counter_cell[3]~COUT\);

\CPeak_FF|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \CPeak_FF|dffs[4]\ = DFFE(\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Max_Clamp_Val_Cntr|wysi_counter|q[4]\) # !\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & \Int_CPEak_FF|dffs[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0AA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[4]\,
	datac => \Max_Clamp_Val_Cntr|wysi_counter|q[4]\,
	datad => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CPeak_FF|dffs[4]\);

\Memory_Access~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Memory_Access,
	combout => \Memory_Access~combout\);

\ROI_ADDR[0]~56_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_ADDR[0]~56\ = \Memory_Access~combout\ & \DSP_RAMA[0]~combout\ # !\Memory_Access~combout\ & (\CPeak_FF|dffs[4]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \DSP_RAMA[0]~combout\,
	datac => \CPeak_FF|dffs[4]\,
	datad => \Memory_Access~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_ADDR[0]~56\);

\DSP_RAMA[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(1),
	combout => \DSP_RAMA[1]~combout\);

\CPeak_FF|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \CPeak_FF|dffs[5]\ = DFFE(\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Max_Clamp_Compare|comparator|cmp_end|lcarry[5]~59\) # !\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & \Int_CPEak_FF|dffs[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , 
-- )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0AA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[5]\,
	datac => \Max_Clamp_Compare|comparator|cmp_end|lcarry[5]~59\,
	datad => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CPeak_FF|dffs[5]\);

\ROI_ADDR[1]~57_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_ADDR[1]~57\ = \Memory_Access~combout\ & \DSP_RAMA[1]~combout\ # !\Memory_Access~combout\ & (\CPeak_FF|dffs[5]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \DSP_RAMA[1]~combout\,
	datac => \CPeak_FF|dffs[5]\,
	datad => \Memory_Access~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_ADDR[1]~57\);

\DSP_RAMA[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(2),
	combout => \DSP_RAMA[2]~combout\);

\CPeak_FF|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \CPeak_FF|dffs[6]\ = DFFE(\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Max_Clamp_Val_Cntr|wysi_counter|q[6]\) # !\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & \Int_CPEak_FF|dffs[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0AA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[6]\,
	datac => \Max_Clamp_Val_Cntr|wysi_counter|q[6]\,
	datad => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CPeak_FF|dffs[6]\);

\ROI_ADDR[2]~58_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_ADDR[2]~58\ = \Memory_Access~combout\ & \DSP_RAMA[2]~combout\ # !\Memory_Access~combout\ & (\CPeak_FF|dffs[6]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \DSP_RAMA[2]~combout\,
	datac => \CPeak_FF|dffs[6]\,
	datad => \Memory_Access~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_ADDR[2]~58\);

\DSP_RAMA[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(3),
	combout => \DSP_RAMA[3]~combout\);

\CPeak_FF|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \CPeak_FF|dffs[7]\ = DFFE(\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Max_Clamp_Val_Cntr|wysi_counter|q[7]\) # !\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Int_CPEak_FF|dffs[7]\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA50",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	datac => \Int_CPEak_FF|dffs[7]\,
	datad => \Max_Clamp_Val_Cntr|wysi_counter|q[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CPeak_FF|dffs[7]\);

\ROI_ADDR[3]~59_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_ADDR[3]~59\ = \Memory_Access~combout\ & \DSP_RAMA[3]~combout\ # !\Memory_Access~combout\ & (\CPeak_FF|dffs[7]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \DSP_RAMA[3]~combout\,
	datac => \CPeak_FF|dffs[7]\,
	datad => \Memory_Access~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_ADDR[3]~59\);

\CPeak_FF|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \CPeak_FF|dffs[8]\ = DFFE(\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Max_Clamp_Val_Cntr|wysi_counter|q[8]\) # !\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & \Int_CPEak_FF|dffs[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Int_CPEak_FF|dffs[8]\,
	datac => \Max_Clamp_Val_Cntr|wysi_counter|q[8]\,
	datad => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CPeak_FF|dffs[8]\);

\DSP_RAMA[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(4),
	combout => \DSP_RAMA[4]~combout\);

\ROI_ADDR[4]~60_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_ADDR[4]~60\ = \Memory_Access~combout\ & (\DSP_RAMA[4]~combout\) # !\Memory_Access~combout\ & \CPeak_FF|dffs[8]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0CC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \CPeak_FF|dffs[8]\,
	datac => \DSP_RAMA[4]~combout\,
	datad => \Memory_Access~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_ADDR[4]~60\);

\DSP_RAMA[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(5),
	combout => \DSP_RAMA[5]~combout\);

\CPeak_FF|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \CPeak_FF|dffs[9]\ = DFFE(\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & \Max_Clamp_Val_Cntr|wysi_counter|q[9]\ # !\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Int_CPEak_FF|dffs[9]\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[9]\,
	datac => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	datad => \Int_CPEak_FF|dffs[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CPeak_FF|dffs[9]\);

\ROI_ADDR[5]~61_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_ADDR[5]~61\ = \Memory_Access~combout\ & \DSP_RAMA[5]~combout\ # !\Memory_Access~combout\ & (\CPeak_FF|dffs[9]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \DSP_RAMA[5]~combout\,
	datac => \CPeak_FF|dffs[9]\,
	datad => \Memory_Access~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_ADDR[5]~61\);

\CPeak_FF|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \CPeak_FF|dffs[10]\ = DFFE(\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Max_Clamp_Val_Cntr|wysi_counter|q[10]\) # !\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Int_CPEak_FF|dffs[10]\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	datac => \Max_Clamp_Val_Cntr|wysi_counter|q[10]\,
	datad => \Int_CPEak_FF|dffs[10]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CPeak_FF|dffs[10]\);

\DSP_RAMA[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(6),
	combout => \DSP_RAMA[6]~combout\);

\ROI_ADDR[6]~62_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_ADDR[6]~62\ = \Memory_Access~combout\ & (\DSP_RAMA[6]~combout\) # !\Memory_Access~combout\ & \CPeak_FF|dffs[10]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0CC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \CPeak_FF|dffs[10]\,
	datac => \DSP_RAMA[6]~combout\,
	datad => \Memory_Access~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_ADDR[6]~62\);

\CPeak_FF|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \CPeak_FF|dffs[11]\ = DFFE(\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Max_Clamp_Val_Cntr|wysi_counter|q[11]\) # !\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & \Int_CPEak_FF|dffs[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0AA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[11]\,
	datac => \Max_Clamp_Val_Cntr|wysi_counter|q[11]\,
	datad => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CPeak_FF|dffs[11]\);

\DSP_RAMA[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(7),
	combout => \DSP_RAMA[7]~combout\);

\ROI_ADDR[7]~63_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_ADDR[7]~63\ = \Memory_Access~combout\ & (\DSP_RAMA[7]~combout\) # !\Memory_Access~combout\ & \CPeak_FF|dffs[11]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0CC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \CPeak_FF|dffs[11]\,
	datac => \DSP_RAMA[7]~combout\,
	datad => \Memory_Access~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_ADDR[7]~63\);

\ROI_RAM|sram|segment[0][15]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 15,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[15]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][15]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][15]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][15]_modesel\,
	dataout => \ROI_RAM|sram|q[15]\);

\CPeak_FF|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \CPeak_FF|dffs[0]\ = DFFE(\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & \Max_Clamp_Val_Cntr|wysi_counter|q[0]\ # !\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Int_CPEak_FF|dffs[0]\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[0]\,
	datac => \Int_CPEak_FF|dffs[0]\,
	datad => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CPeak_FF|dffs[0]\);

\ROI_RAM|sram|segment[0][12]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 12,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[12]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][12]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][12]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][12]_modesel\,
	dataout => \ROI_RAM|sram|q[12]\);

\ROI_RAM|sram|segment[0][14]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 14,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[14]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][14]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][14]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][14]_modesel\,
	dataout => \ROI_RAM|sram|q[14]\);

\ROI_DEF~62_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_DEF~62\ = \CPeak_FF|dffs[1]\ & (\CPeak_FF|dffs[0]\ # \ROI_RAM|sram|q[14]\) # !\CPeak_FF|dffs[1]\ & \ROI_RAM|sram|q[12]\ & !\CPeak_FF|dffs[0]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AEA4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \CPeak_FF|dffs[1]\,
	datab => \ROI_RAM|sram|q[12]\,
	datac => \CPeak_FF|dffs[0]\,
	datad => \ROI_RAM|sram|q[14]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_DEF~62\);

\ROI_DEF~63_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_DEF~63\ = \CPeak_FF|dffs[0]\ & (\ROI_DEF~62\ & (\ROI_RAM|sram|q[15]\) # !\ROI_DEF~62\ & \ROI_RAM|sram|q[13]\) # !\CPeak_FF|dffs[0]\ & (\ROI_DEF~62\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_RAM|sram|q[13]\,
	datab => \ROI_RAM|sram|q[15]\,
	datac => \CPeak_FF|dffs[0]\,
	datad => \ROI_DEF~62\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_DEF~63\);

\CPeak_FF|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \CPeak_FF|dffs[3]\ = DFFE(\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Max_Clamp_Val_Cntr|wysi_counter|q[3]\) # !\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & \Int_CPEak_FF|dffs[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Int_CPEak_FF|dffs[3]\,
	datac => \Max_Clamp_Val_Cntr|wysi_counter|q[3]\,
	datad => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CPeak_FF|dffs[3]\);

\ROI_RAM|sram|segment[0][5]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 5,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[5]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][5]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][5]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][5]_modesel\,
	dataout => \ROI_RAM|sram|q[5]\);

\ROI_RAM|sram|segment[0][6]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 6,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[6]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][6]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][6]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][6]_modesel\,
	dataout => \ROI_RAM|sram|q[6]\);

\ROI_RAM|sram|segment[0][4]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 4,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[4]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][4]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][4]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][4]_modesel\,
	dataout => \ROI_RAM|sram|q[4]\);

\ROI_DEF~57_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_DEF~57\ = \CPeak_FF|dffs[1]\ & (\ROI_RAM|sram|q[6]\ # \CPeak_FF|dffs[0]\) # !\CPeak_FF|dffs[1]\ & (!\CPeak_FF|dffs[0]\ & \ROI_RAM|sram|q[4]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ADA8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \CPeak_FF|dffs[1]\,
	datab => \ROI_RAM|sram|q[6]\,
	datac => \CPeak_FF|dffs[0]\,
	datad => \ROI_RAM|sram|q[4]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_DEF~57\);

\ROI_DEF~58_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_DEF~58\ = \CPeak_FF|dffs[0]\ & (\ROI_DEF~57\ & \ROI_RAM|sram|q[7]\ # !\ROI_DEF~57\ & (\ROI_RAM|sram|q[5]\)) # !\CPeak_FF|dffs[0]\ & (\ROI_DEF~57\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_RAM|sram|q[7]\,
	datab => \ROI_RAM|sram|q[5]\,
	datac => \CPeak_FF|dffs[0]\,
	datad => \ROI_DEF~57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_DEF~58\);

\ROI_RAM|sram|segment[0][2]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 2,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[2]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][2]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][2]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][2]_modesel\,
	dataout => \ROI_RAM|sram|q[2]\);

\CPeak_FF|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \CPeak_FF|dffs[1]\ = DFFE(\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & \Max_Clamp_Val_Cntr|wysi_counter|q[1]\ # !\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Int_CPEak_FF|dffs[1]\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Max_Clamp_Val_Cntr|wysi_counter|q[1]\,
	datac => \Int_CPEak_FF|dffs[1]\,
	datad => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CPeak_FF|dffs[1]\);

\ROI_RAM|sram|segment[0][1]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 1,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[1]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][1]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][1]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][1]_modesel\,
	dataout => \ROI_RAM|sram|q[1]\);

\ROI_RAM|sram|segment[0][0]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 0,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[0]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][0]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][0]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][0]_modesel\,
	dataout => \ROI_RAM|sram|q[0]\);

\ROI_DEF~59_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_DEF~59\ = \CPeak_FF|dffs[0]\ & (\ROI_RAM|sram|q[1]\ # \CPeak_FF|dffs[1]\) # !\CPeak_FF|dffs[0]\ & (!\CPeak_FF|dffs[1]\ & \ROI_RAM|sram|q[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ADA8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \CPeak_FF|dffs[0]\,
	datab => \ROI_RAM|sram|q[1]\,
	datac => \CPeak_FF|dffs[1]\,
	datad => \ROI_RAM|sram|q[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_DEF~59\);

\ROI_DEF~60_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_DEF~60\ = \CPeak_FF|dffs[1]\ & (\ROI_DEF~59\ & \ROI_RAM|sram|q[3]\ # !\ROI_DEF~59\ & (\ROI_RAM|sram|q[2]\)) # !\CPeak_FF|dffs[1]\ & (\ROI_DEF~59\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_RAM|sram|q[3]\,
	datab => \ROI_RAM|sram|q[2]\,
	datac => \CPeak_FF|dffs[1]\,
	datad => \ROI_DEF~59\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_DEF~60\);

\ROI_DEF~61_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_DEF~61\ = \CPeak_FF|dffs[2]\ & (\ROI_DEF~58\ # \CPeak_FF|dffs[3]\) # !\CPeak_FF|dffs[2]\ & (!\CPeak_FF|dffs[3]\ & \ROI_DEF~60\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ADA8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \CPeak_FF|dffs[2]\,
	datab => \ROI_DEF~58\,
	datac => \CPeak_FF|dffs[3]\,
	datad => \ROI_DEF~60\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_DEF~61\);

\ROI_DEF~64_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_DEF~64\ = \CPeak_FF|dffs[3]\ & (\ROI_DEF~61\ & (\ROI_DEF~63\) # !\ROI_DEF~61\ & \ROI_DEF~56\) # !\CPeak_FF|dffs[3]\ & (\ROI_DEF~61\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_DEF~56\,
	datab => \ROI_DEF~63\,
	datac => \CPeak_FF|dffs[3]\,
	datad => \ROI_DEF~61\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_DEF~64\);

\ROI_INC~22_I\ : apex20ke_lcell
-- Equation(s):
-- \ROI_INC~22\ = \ROI_DEF~64\ & PK_DONE & \Time_Enable~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \ROI_DEF~64\,
	datac => PK_DONE,
	datad => \Time_Enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ROI_INC~22\);

\Time_Clr~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Time_Clr,
	combout => \Time_Clr~combout\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[0]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[0]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_INC~22\ $ (\ROI_SUM_CNTR|wysi_counter|sload_path[0]\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[0]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5AF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[0]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[0]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[1]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[1]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[1]\ $ (\ROI_INC~22\ & \ROI_SUM_CNTR|wysi_counter|counter_cell[0]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[1]~COUT\ = CARRY(!\ROI_SUM_CNTR|wysi_counter|counter_cell[0]~COUT\ # !\ROI_SUM_CNTR|wysi_counter|sload_path[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[1]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[0]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[1]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[1]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[2]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[2]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[2]\ $ (\ROI_INC~22\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[1]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[2]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[2]\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[1]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[2]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[1]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[2]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[2]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[3]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[3]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[3]\ $ (\ROI_INC~22\ & \ROI_SUM_CNTR|wysi_counter|counter_cell[2]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[3]~COUT\ = CARRY(!\ROI_SUM_CNTR|wysi_counter|counter_cell[2]~COUT\ # !\ROI_SUM_CNTR|wysi_counter|sload_path[3]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[3]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[2]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[3]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[3]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[4]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[4]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[4]\ $ (\ROI_INC~22\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[3]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[4]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[4]\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[3]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[4]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[3]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[4]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[4]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[5]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[5]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[5]\ $ (\ROI_INC~22\ & \ROI_SUM_CNTR|wysi_counter|counter_cell[4]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[5]~COUT\ = CARRY(!\ROI_SUM_CNTR|wysi_counter|counter_cell[4]~COUT\ # !\ROI_SUM_CNTR|wysi_counter|sload_path[5]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[5]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[4]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[5]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[5]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[6]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[6]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[6]\ $ (\ROI_INC~22\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[5]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[6]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[6]\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[5]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[6]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[5]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[6]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[6]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[7]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[7]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[7]\ $ (\ROI_INC~22\ & \ROI_SUM_CNTR|wysi_counter|counter_cell[6]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[7]~COUT\ = CARRY(!\ROI_SUM_CNTR|wysi_counter|counter_cell[6]~COUT\ # !\ROI_SUM_CNTR|wysi_counter|sload_path[7]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[7]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[6]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[7]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[7]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[8]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[8]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[8]\ $ (\ROI_INC~22\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[7]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[8]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[8]\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[7]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[8]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[7]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[8]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[8]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[9]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[9]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[9]\ $ (\ROI_INC~22\ & \ROI_SUM_CNTR|wysi_counter|counter_cell[8]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[9]~COUT\ = CARRY(!\ROI_SUM_CNTR|wysi_counter|counter_cell[8]~COUT\ # !\ROI_SUM_CNTR|wysi_counter|sload_path[9]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[9]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[8]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[9]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[9]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[10]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[10]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[10]\ $ (\ROI_INC~22\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[9]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[10]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[10]\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[9]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[10]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[9]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[10]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[10]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[11]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[11]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[11]\ $ (\ROI_INC~22\ & \ROI_SUM_CNTR|wysi_counter|counter_cell[10]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[11]~COUT\ = CARRY(!\ROI_SUM_CNTR|wysi_counter|counter_cell[10]~COUT\ # !\ROI_SUM_CNTR|wysi_counter|sload_path[11]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[11]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[10]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[11]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[11]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[12]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[12]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[12]\ $ (\ROI_INC~22\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[11]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[12]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[12]\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[11]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[12]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[11]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[12]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[12]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[13]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[13]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[13]\ $ (\ROI_INC~22\ & \ROI_SUM_CNTR|wysi_counter|counter_cell[12]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[13]~COUT\ = CARRY(!\ROI_SUM_CNTR|wysi_counter|counter_cell[12]~COUT\ # !\ROI_SUM_CNTR|wysi_counter|sload_path[13]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_SUM_CNTR|wysi_counter|sload_path[13]\,
	datab => \ROI_INC~22\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[12]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[13]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[13]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[14]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[14]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[14]\ $ (\ROI_INC~22\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[13]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[14]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[14]\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[13]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[14]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[13]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[14]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[14]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[15]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[15]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[15]\ $ (\ROI_INC~22\ & \ROI_SUM_CNTR|wysi_counter|counter_cell[14]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[15]~COUT\ = CARRY(!\ROI_SUM_CNTR|wysi_counter|counter_cell[14]~COUT\ # !\ROI_SUM_CNTR|wysi_counter|sload_path[15]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[15]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[14]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[15]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[15]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[16]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[16]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[16]\ $ (\ROI_INC~22\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[15]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[16]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[16]\ & (!\ROI_SUM_CNTR|wysi_counter|counter_cell[15]~COUT\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_SUM_CNTR|wysi_counter|sload_path[16]\,
	datab => \ROI_INC~22\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[15]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[16]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[16]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[17]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[17]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[17]\ $ (\ROI_INC~22\ & \ROI_SUM_CNTR|wysi_counter|counter_cell[16]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[17]~COUT\ = CARRY(!\ROI_SUM_CNTR|wysi_counter|counter_cell[16]~COUT\ # !\ROI_SUM_CNTR|wysi_counter|sload_path[17]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[17]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[16]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[17]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[17]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[18]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[18]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[18]\ $ (\ROI_INC~22\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[17]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[18]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[18]\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[17]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[18]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[17]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[18]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[18]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[19]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[19]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[19]\ $ (\ROI_INC~22\ & \ROI_SUM_CNTR|wysi_counter|counter_cell[18]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[19]~COUT\ = CARRY(!\ROI_SUM_CNTR|wysi_counter|counter_cell[18]~COUT\ # !\ROI_SUM_CNTR|wysi_counter|sload_path[19]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[19]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[18]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[19]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[19]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[20]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[20]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[20]\ $ (\ROI_INC~22\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[19]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[20]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[20]\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[19]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[20]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[19]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[20]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[20]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[21]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[21]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[21]\ $ (\ROI_INC~22\ & \ROI_SUM_CNTR|wysi_counter|counter_cell[20]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[21]~COUT\ = CARRY(!\ROI_SUM_CNTR|wysi_counter|counter_cell[20]~COUT\ # !\ROI_SUM_CNTR|wysi_counter|sload_path[21]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[21]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[20]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[21]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[21]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[22]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[22]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[22]\ $ (\ROI_INC~22\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[21]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[22]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[22]\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[21]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[22]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[21]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[22]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[22]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[23]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[23]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[23]\ $ (\ROI_INC~22\ & \ROI_SUM_CNTR|wysi_counter|counter_cell[22]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[23]~COUT\ = CARRY(!\ROI_SUM_CNTR|wysi_counter|counter_cell[22]~COUT\ # !\ROI_SUM_CNTR|wysi_counter|sload_path[23]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[23]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[22]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[23]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[23]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[24]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[24]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[24]\ $ (\ROI_INC~22\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[23]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[24]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[24]\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[23]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[24]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[23]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[24]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[24]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[25]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[25]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[25]\ $ (\ROI_INC~22\ & \ROI_SUM_CNTR|wysi_counter|counter_cell[24]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[25]~COUT\ = CARRY(!\ROI_SUM_CNTR|wysi_counter|counter_cell[24]~COUT\ # !\ROI_SUM_CNTR|wysi_counter|sload_path[25]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[25]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[24]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[25]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[25]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[26]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[26]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[26]\ $ (\ROI_INC~22\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[25]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[26]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[26]\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[25]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[26]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[25]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[26]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[26]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[27]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[27]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[27]\ $ (\ROI_INC~22\ & \ROI_SUM_CNTR|wysi_counter|counter_cell[26]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[27]~COUT\ = CARRY(!\ROI_SUM_CNTR|wysi_counter|counter_cell[26]~COUT\ # !\ROI_SUM_CNTR|wysi_counter|sload_path[27]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[27]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[26]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[27]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[27]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[28]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[28]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[28]\ $ (\ROI_INC~22\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[27]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[28]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[28]\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[27]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[28]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[27]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[28]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[28]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[29]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[29]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[29]\ $ (\ROI_INC~22\ & \ROI_SUM_CNTR|wysi_counter|counter_cell[28]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[29]~COUT\ = CARRY(!\ROI_SUM_CNTR|wysi_counter|counter_cell[28]~COUT\ # !\ROI_SUM_CNTR|wysi_counter|sload_path[29]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_INC~22\,
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[29]\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[28]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[29]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[29]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[30]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[30]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[30]\ $ (\ROI_INC~22\ & !\ROI_SUM_CNTR|wysi_counter|counter_cell[29]~COUT\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \ROI_SUM_CNTR|wysi_counter|counter_cell[30]~COUT\ = CARRY(\ROI_SUM_CNTR|wysi_counter|sload_path[30]\ & (!\ROI_SUM_CNTR|wysi_counter|counter_cell[29]~COUT\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ROI_SUM_CNTR|wysi_counter|sload_path[30]\,
	datab => \ROI_INC~22\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[29]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[30]\,
	cout => \ROI_SUM_CNTR|wysi_counter|counter_cell[30]~COUT\);

\ROI_SUM_CNTR|wysi_counter|counter_cell[31]\ : apex20ke_lcell
-- Equation(s):
-- \ROI_SUM_CNTR|wysi_counter|sload_path[31]\ = DFFE(!GLOBAL(\Time_Clr~combout\) & \ROI_SUM_CNTR|wysi_counter|sload_path[31]\ $ (\ROI_SUM_CNTR|wysi_counter|counter_cell[30]~COUT\ & \ROI_INC~22\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CCC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \ROI_SUM_CNTR|wysi_counter|sload_path[31]\,
	datad => \ROI_INC~22\,
	cin => \ROI_SUM_CNTR|wysi_counter|counter_cell[30]~COUT\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Time_Clr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \ROI_SUM_CNTR|wysi_counter|sload_path[31]\);

\DSP_D[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(3),
	combout => \DSP_D[3]~combout\);

\ROI_RAM|sram|segment[0][3]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 3,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[3]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][3]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][3]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][3]_modesel\,
	dataout => \ROI_RAM|sram|q[3]\);

\DSP_D[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(7),
	combout => \DSP_D[7]~combout\);

\ROI_RAM|sram|segment[0][7]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 7,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[7]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][7]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][7]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][7]_modesel\,
	dataout => \ROI_RAM|sram|q[7]\);

\DSP_D[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(8),
	combout => \DSP_D[8]~combout\);

\ROI_RAM|sram|segment[0][8]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 8,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[8]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][8]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][8]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][8]_modesel\,
	dataout => \ROI_RAM|sram|q[8]\);

\DSP_D[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(9),
	combout => \DSP_D[9]~combout\);

\ROI_RAM|sram|segment[0][9]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 9,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[9]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][9]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][9]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][9]_modesel\,
	dataout => \ROI_RAM|sram|q[9]\);

\DSP_D[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(10),
	combout => \DSP_D[10]~combout\);

\ROI_RAM|sram|segment[0][10]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 10,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[10]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][10]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][10]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][10]_modesel\,
	dataout => \ROI_RAM|sram|q[10]\);

\DSP_D[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(11),
	combout => \DSP_D[11]~combout\);

\ROI_RAM|sram|segment[0][11]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 11,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[11]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][11]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][11]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][11]_modesel\,
	dataout => \ROI_RAM|sram|q[11]\);

\DSP_D[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(13),
	combout => \DSP_D[13]~combout\);

\ROI_RAM|sram|segment[0][13]\ : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 13,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => \DSP_D[13]~combout\,
	clk0 => \clk~combout\,
	clk1 => \clk~combout\,
	we => \ROI_WE~combout\,
	waddr => \ROI_RAM|sram|segment[0][13]_WADDR_bus\,
	raddr => \ROI_RAM|sram|segment[0][13]_RADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => \ROI_RAM|sram|segment[0][13]_modesel\,
	dataout => \ROI_RAM|sram|q[13]\);

\CPeak_FF|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \CPeak_FF|dffs[2]\ = DFFE(\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Max_Clamp_Val_Cntr|wysi_counter|q[2]\) # !\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & \Int_CPEak_FF|dffs[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Int_CPEak_FF|dffs[2]\,
	datac => \Max_Clamp_Val_Cntr|wysi_counter|q[2]\,
	datad => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CPeak_FF|dffs[2]\);

\CPeak_FF|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \CPeak_FF|dffs[12]\ = DFFE(\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Max_Clamp_Val_Cntr|wysi_counter|q[12]\) # !\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & \Int_CPEak_FF|dffs[12]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0AA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Int_CPEak_FF|dffs[12]\,
	datac => \Max_Clamp_Val_Cntr|wysi_counter|q[12]\,
	datad => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CPeak_FF|dffs[12]\);

\CPeak_FF|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \CPeak_FF|dffs[13]\ = DFFE(\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Max_Clamp_Val_Cntr|wysi_counter|q[13]\) # !\Clamp_Dout_Compare|comparator|cmp_end|agb_out\ & (\Int_CPEak_FF|dffs[13]\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Clamp_Dout_Compare|comparator|cmp_end|agb_out\,
	datac => \Max_Clamp_Val_Cntr|wysi_counter|q[13]\,
	datad => \Int_CPEak_FF|dffs[13]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CPeak_FF|dffs[13]\);

\BLEVEL_UPD~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \BLEVEL_UPD~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL_UPD);

\FDISC~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \FDISC~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FDISC);

\Meas_Done~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \Meas_Done~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Meas_Done);

\PBusy~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => FD_STATE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PBusy);

\BLEVEL[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \BLEVEL_FF|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(0));

\BLEVEL[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \BLEVEL_FF|dffs[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(1));

\BLEVEL[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \BLEVEL_FF|dffs[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(2));

\BLEVEL[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \BLEVEL_FF|dffs[3]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(3));

\BLEVEL[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \BLEVEL_FF|dffs[4]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(4));

\BLEVEL[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \BLEVEL_FF|dffs[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(5));

\BLEVEL[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \BLEVEL_FF|dffs[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(6));

\BLEVEL[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \BLEVEL_FF|dffs[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLEVEL(7));

\ROI_SUM[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(0));

\ROI_SUM[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(1));

\ROI_SUM[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(2));

\ROI_SUM[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[3]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(3));

\ROI_SUM[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[4]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(4));

\ROI_SUM[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(5));

\ROI_SUM[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(6));

\ROI_SUM[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(7));

\ROI_SUM[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(8));

\ROI_SUM[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[9]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(9));

\ROI_SUM[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[10]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(10));

\ROI_SUM[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[11]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(11));

\ROI_SUM[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[12]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(12));

\ROI_SUM[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[13]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(13));

\ROI_SUM[14]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[14]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(14));

\ROI_SUM[15]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[15]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(15));

\ROI_SUM[16]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[16]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(16));

\ROI_SUM[17]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[17]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(17));

\ROI_SUM[18]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[18]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(18));

\ROI_SUM[19]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[19]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(19));

\ROI_SUM[20]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[20]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(20));

\ROI_SUM[21]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[21]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(21));

\ROI_SUM[22]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[22]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(22));

\ROI_SUM[23]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[23]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(23));

\ROI_SUM[24]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[24]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(24));

\ROI_SUM[25]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[25]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(25));

\ROI_SUM[26]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[26]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(26));

\ROI_SUM[27]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[27]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(27));

\ROI_SUM[28]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[28]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(28));

\ROI_SUM[29]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[29]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(29));

\ROI_SUM[30]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[30]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(30));

\ROI_SUM[31]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_SUM_CNTR|wysi_counter|sload_path[31]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(31));

\ROI_LU[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(0));

\ROI_LU[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(1));

\ROI_LU[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(2));

\ROI_LU[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[3]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(3));

\ROI_LU[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[4]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(4));

\ROI_LU[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(5));

\ROI_LU[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(6));

\ROI_LU[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(7));

\ROI_LU[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(8));

\ROI_LU[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[9]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(9));

\ROI_LU[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[10]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(10));

\ROI_LU[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[11]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(11));

\ROI_LU[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[12]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(12));

\ROI_LU[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[13]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(13));

\ROI_LU[14]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[14]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(14));

\ROI_LU[15]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \ROI_RAM|sram|q[15]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(15));

\dout[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(0));

\dout[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(1));

\dout[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(2));

\dout[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[3]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(3));

\dout[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[4]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(4));

\dout[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(5));

\dout[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(6));

\dout[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(7));

\dout[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(8));

\dout[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[9]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(9));

\dout[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[10]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(10));

\dout[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[11]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(11));

\dout[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[12]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(12));

\dout[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[13]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(13));

\dout[14]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[14]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(14));

\dout[15]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \U_Filter|Dout_FF|dffs[15]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_dout(15));

\CPeak[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \CPeak_FF|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPeak(0));

\CPeak[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \CPeak_FF|dffs[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPeak(1));

\CPeak[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \CPeak_FF|dffs[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPeak(2));

\CPeak[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \CPeak_FF|dffs[3]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPeak(3));

\CPeak[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \CPeak_FF|dffs[4]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPeak(4));

\CPeak[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \CPeak_FF|dffs[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPeak(5));

\CPeak[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \CPeak_FF|dffs[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPeak(6));

\CPeak[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \CPeak_FF|dffs[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPeak(7));

\CPeak[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \CPeak_FF|dffs[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPeak(8));

\CPeak[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \CPeak_FF|dffs[9]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPeak(9));

\CPeak[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \CPeak_FF|dffs[10]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPeak(10));

\CPeak[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \CPeak_FF|dffs[11]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPeak(11));

\CPeak[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \CPeak_FF|dffs[12]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPeak(12));

\CPeak[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \CPeak_FF|dffs[13]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPeak(13));
END structure;


