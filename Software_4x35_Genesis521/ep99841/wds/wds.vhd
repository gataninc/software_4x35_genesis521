------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	wds
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module performs peak-height analysis for the Ketek AXAS SDD Detector
--		The digital gain has been boosted high enough for an ASAS output of 1V gaussian pulses
--		for Mn55 (6KeV)
--
--
--	History
--		01/16/06 - MCS : Ver 4005
--			Shorten to_reject to 750 ns
--			TODO:  Create "Pulse Busy Timeout for 25us to prevent baseline histogram update
--			FIX 0 Peak spike
--			FIX Wrap-around (1.5V input )
--		06/15/05 - MCS
--			Updated for QuartusII Version 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity wds is 
	port( 
		imr				: in		std_logic;					-- Master Reset
		clk				: in		std_logic;					-- MAster Clock
		Time_Clr			: in		std_logic;
		Time_Enable		: in		std_logic;		
		FIR_MR			: in		std_logic;
		Memory_Access		: in		std_logic;
		ROI_WE			: in		std_logic;
		DSP_D			: in		std_logic_vector( 15 downto 0 );	-- DSP Data Bus
		DSP_RAMA			: in		std_logic_Vector(  7 downto 0 );	-- DSP Address Bus
		Disc_en			: in		std_logic;
		Din				: in		std_logic_vector( 15 downto 0 );
		Tlevel			: in		std_logic_vector( 15 downto 0 );
		BLEVEL			: buffer	std_logic_vector(  7 downto 0 );
		BLEVEL_UPD		: buffer	std_logic;
		FDISC			: buffer	std_logic;
		Meas_Done			: buffer	std_logic;
		PBusy			: buffer	std_Logic;
		ROI_SUM			: buffer	std_logic_vector( 31 downto 0 );
		ROI_LU			: buffer	std_logic_Vector( 15 downto 0 );	-- ROI Lookup Data
		dout				: buffer  std_logic_Vector( 15 downto 0 );
		CPeak			: buffer	std_Logic_Vector( 13 downto 0 ) );
end wds;
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
architecture behavioral of wds is
	-- Constant Declarations ---------------------------------------------------------

	constant blevel_cnt_max		: integer := 199;		-- 10 us
	constant to_busy			: integer := 11;
	constant to_over			: integer := (  1600/50) - 1;	-- was 3000
	constant Din_Clamp_Val		: integer := 7400;
	constant Din_Diff_Abs_thr 	: integer := 35; -- was 70;

	-- Type Declarations ---------------------------------------------------------

	type FD_STATE_TYPE is (
		ST_IDLE,
		ST_BUSY );

	-- Signal Declarations ---------------------------------------------------------

	signal blevel_cnt 			: integer range 0 to blevel_cnt_max;
	signal blevel_cnt_Tc		: std_logic;
	
	signal CPeak_Clamp_Max		: std_logic;
	signal CPeak_Mux			: std_logic_Vector( 13 downto 0 );
	
	signal Dout_Del 			: std_logic_Vector( 15 downto 0 );
	signal dout_max_cmp			: std_logic;
	signal Dout_Max_Clr 		: std_logic;
	signal Dout_Max_En			: std_logic;

	signal FD_STATE 			: FD_STATE_TYPE;

	signal Int_Cpeak			: std_logic_vector( 15 downto 0 );

	signal Level_Cmp 			: std_Logic;

	signal Max_Clamp_Val		: std_logic_Vector( 15 downto 0 );
	signal Max_Clamp_Clamp 		: std_Logic;
	signal Max_Clamp_Inc 		: std_logic;
	signal Max_Clamp_Ld			: std_logic;
	
	signal PSlope_Gen			: std_logic;
	signal PSlope_Gen_Del		: std_logic;
	signal PSlope_Gen_Cmp 		: std_logic;
	signal PSlope_Gen_Cmp_Del	: std_logic;
	signal PK_DONE				: std_logic;
	

	signal ROI_INC				: std_logic;
	signal ROI_DEF				: std_logic;
	signal ROI_ADDR			: std_logic_Vector(  7 downto 0 );

	signal TLevel_Reg			: std_Logic_Vector( 15 downto 0 );
	signal TO_Cnt				: integer range 0 to to_over;
	signal To_Cnt_Tc 			: std_logic;


	------------------------------------------------------------------------------------
	component wds_filter is 
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			FIR_MR		: in		std_logic;
			din			: in		std_logic_Vector( 15 downto 0 );
			dout			: buffer	std_logic_Vector( 15 downto 0 ) );
	end component wds_filter;
	------------------------------------------------------------------------------------

	
begin
	PBUSY 		<= '0' when ( FD_State  = ST_IDLE ) else '1';
				
	To_Cnt_Tc 	<= '1' when ( To_Cnt = to_busy ) else '0';

	------------------------------------------------------------------------------------
	-- Pipeline Threshold Level
	tlevel_reg_ff : lpm_ff
		generic map(
			LPM_WIDTH				=> 16 )
		port map(
			aclr					=> imr,
			clock				=> clk,
			data					=> TLevel,
			q					=> TLevel_Reg );
	-- Pipeline Threshold Level
	------------------------------------------------------------------------------------
			
	------------------------------------------------------------------------------------
	-- Filter the Data
	--	Convert Tail Pulse to Triangular Filter
	U_Filter : wds_filter
		port map(
			imr					=> imr,
			clk					=> clk,
			fir_mr				=> fir_Mr,
			din					=> Din, 		-- Input Data
			dout					=> dout );		-- Filter Output			
	--	Convert Tail Pulse to Triangular Filter
     --------------------------------------------------------------------------

	------------------------------------------------------------------------------------
	-- Output Slope Determination
	Dout_Del_ff : lpm_ff
		generic map(
			LPM_WIDTH				=> 16 )
		port map(
			aclr					=> imr,
			clock				=> clk,
			data					=> Dout,			-- was Din
			q					=> Dout_Del );
	
	PSlope_Compare : lpm_compare
		generic map(
			LPM_WIDTH				=> 16,
			LPM_REPRESENTATION		=> "SIGNED",
			LPM_PIPELINE			=> 1 )		
		port map(
			aclr					=> imr,
			clock				=> clk,
			dataa				=> Dout,			-- was Din
			datab				=> Dout_Del,
			agb					=> PSlope_Gen_Cmp );
	-- Output Slope Determination
	------------------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- Dout Peak Determination	
	dout_max_Compare : lpm_compare		
		generic map(
			LPM_WIDTH				=> 16,
			LPM_REPRESENTATION		=> "SIGNED" )
		port map(	
			dataa				=> Dout,	
			datab				=> Int_CPEak,
			agb					=> dout_max_cmp );
			
	Dout_Max_Clr 	<= '1' when (( FD_State = ST_IDLE ) and ( PSlope_Gen = '1' ) and ( PSlope_Gen_Del = '0' )) else
			   '1' when ( Meas_Done = '1' ) else
	   			   '0';
	
	-- Prevent Peak from being altered during 
	Dout_Max_En	<= '1' when (   Dout_Max_Clr  	= '1' ) else
				   '1' when ( ( Level_Cmp 		= '1' ) 
						and ( Dout_max_Cmp 		= '1' )) else 
				   '0';

	Int_CPEak_FF : LPM_FF
		generic map(
			LPM_WIDTH				=> 16 )
		port map(
			aclr					=> imr,
			clock				=> clk,
			sclr					=> Dout_Max_Clr,
			enable				=> Dout_max_En,
			data					=> Dout,
			q					=> Int_CPEak );

			
	Clamp_Dout_Compare : lpm_compare
		generic map(
			LPM_WIDTH				=> 16,
			LPM_REPRESENTATION		=> "SIGNED" )
		port map(
			dataa				=> Int_CPEak,
			datab				=> Max_Clamp_Val,
			agb					=> CPeak_Clamp_Max );
	
	CPeak_Mux <= Max_Clamp_Val( 13 downto 0 ) when ( Cpeak_Clamp_max = '1' ) else Int_CPeak( 13 downto 0 );				

	CPeak_FF : lpm_ff
		generic map(
			LPM_WIDTH				=> 14 )
		port map(
			aclr					=> imr,
			clock				=> clk,
			data					=> CPeak_Mux,
			q					=> CPeak );
	-- Dout Peak Determination	
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- Max Clamp Determination

	Max_Clamp_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Max_Clamp_val,
			datab			=> conv_Std_logic_vector( 4000, 16 ),
			ageb				=> Max_Clamp_Clamp );

	Max_Clamp_Inc 	<= '1' when (( PK_Done = '1' ) and ( CPeak_Clamp_Max = '1' ) and ( Max_Clamp_Clamp = '0' )) else '0';
	
	Max_Clamp_Ld	<= '1' when ( Time_Enable = '0' ) else
				   '1' when (( PK_Done = '1' ) and ( CPeak_Clamp_Max = '1' ) and ( Max_Clamp_Clamp = '1' )) else 
				   '0';

	Max_Clamp_Val_Cntr : lpm_counter
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			sload			=> Max_Clamp_Ld,
			data				=> conv_Std_logic_Vector( 3995, 16 ),
			cnt_en			=> Max_Clamp_Inc,
			q				=> Max_Clamp_Val );
			
     -------------------------------------------------------------------------
	-- Thrshold with Accumulator Output 
	level_cmp_compare : lpm_compare
		generic map(
			LPM_WIDTH				=> 16,
			LPM_REPRESENTATION		=> "SIGNED")
		port map(
			dataa				=> Dout, 
			datab				=> Tlevel_Reg,
			agb					=> Level_Cmp );
     -------------------------------------------------------------------------

     --------------------------------------------------------------------------
    	-- Baseline Level
	blevel_cnt_tc	<= '1' when ( blevel_cnt = blevel_cnt_max ) else '0';

	
	BLEVEL_FF : LPM_FF
		generic map(
			LPM_WIDTH				=> 8 )
		port map(	
			aclr					=> imr,
			clock				=> clk,
			enable				=> not( PBusy ),
			data					=> Dout( 7 downto 0 ),
			q					=> Blevel );
    	-- Baseline Level
	-------------------------------------------------------------------------------
			
     -------------------------------------------------------------------------------
	-- ROI Counter - Counts all evenets in defined ROI

	ROI_ADDR	<= CPeak( 11 downto 4 ) when ( Memory_Access = '0' ) else
			   DSP_RAMA( 7 downto 0 );

	ROI_RAM : LPM_RAM_DQ
		generic map(
			LPM_WIDTH				=> 16,
			LPM_WIDTHAD			=> 8,
			LPM_INDATA			=> "REGISTERED",
			LPM_ADDRESS_CONTROL		=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_FILE				=> "ROI_RAM.MIF",
			LPM_TYPE				=> "LPM_RAM_DQ" )
		port map(
			inclock				=> clk,
			outclock				=> CLK,
			data					=> DSP_D,
			address				=> ROI_ADDR,
			we					=> ROI_WE,
			q					=> ROI_LU );

	ROI_DEF  	<= ROI_LU( Conv_Integer( CPeak( 3 downto 0 )));
	
	ROI_INC <= '1' when (   ( Time_Enable 	= '1' ) 
					and ( Pk_Done		= '1' ) 
					and ( ROI_DEF 		= '1' )) else '0';

	ROI_SUM_CNTR : LPM_COUNTER
		generic map(
			LPM_WIDTH				=> 32 )
		port map(		
			aclr					=> IMR,
			clock				=> CLK,
			sclr					=> Time_Clr,
			cnt_en				=> ROI_INC,
			q					=> ROI_SUM );			
	-- ROI Defined Look-up Memory
	
    --------------------------------------------------------------------------
	clock_proc : process( imr, clk )
	begin
		if( imr = '1' ) then
			Meas_Done				<= '0';
			FDisc				<= '0';
			blevel_upd			<= '0';
			blevel_Cnt			<= 0;
			PSlope_Gen_Cmp_Del		<= '0';
			PSlope_Gen			<= '0';
			PSlope_Gen_Del			<= '0';
			PK_DONE				<= '0';
			to_cnt 				<= 0;
			FD_State 				<= ST_Idle;				
		elsif(( clk'event ) and ( clk = '1' )) then
			Meas_Done			<= Pk_Done;
			
			if(( PSlope_Gen = '1' ) and ( PSlope_Gen_Del = '0' ))
				then FDisc <= '1';
				else FDisc <= '0';
			end if;			

			-- Baseline Level Timing Generation --------------------------------------------
			if(( FD_State = ST_IDLE ) and ( blevel_cnt_tc = '1' ))
				then blevel_upd <= '1';
				else blevel_upd <= '0';
			end if;
			
			if(( FD_State = ST_IDLE ) and ( blevel_cnt_tc = '1' ))
				then blevel_Cnt <= 0;
			elsif(( FD_State = ST_IDLE ) and ( Blevel_Cnt_Tc = '0' ))
				then blevel_cnt <= blevel_cnt + 1;
			end if;		
			-- Baseline Level Timing Generation --------------------------------------------

			-- Determine Slope of Input data for FDisc generation --------------------------
			PSlope_Gen_Cmp_Del <= PSlope_Gen_Cmp;
						
			if(( Disc_En = '1' ) and ( Level_Cmp = '1' ) and ( PSlope_Gen_Cmp = '1' ) and ( PSlope_Gen_Cmp_DEl = '1' ))
				then PSlope_Gen <= '1';
			elsif(( PSlope_Gen_Cmp = '0' ) and ( PSlope_Gen_Cmp_DEl = '0' ))
				then PSlope_Gen <= '0';
			end if;
			
			PSlope_Gen_Del <= PSlope_Gen;
			-- Determine Slope of Input data for FDisc generation --------------------------	

			---------------------------------------------------------------------------------------
			-- State Machine Control and State Machine
			case FD_STATE is
				when ST_IDLE =>
					Pk_Done 	<= '0';
					to_Cnt 	<= 0;
					if(( PSlope_Gen = '1' ) and ( PSlope_Gen_Del = '0' ))	
						then FD_State <= ST_BUSY;
					end if;
				
				when ST_BUSY =>
					if( To_Cnt_Tc = '1' ) then
						Pk_Done 	<= '1';
						FD_State 	<= St_Idle;
					else
						to_Cnt 	<= to_cnt + 1;
						Pk_Done 	<= '0';
					end if;					
			end case;
			-- State Machine Control and State Machine
			---------------------------------------------------------------------------------------
		end if;
	end process;
     -------------------------------------------------------------------------------
------------------------------------------------------------------------------------
end behavioral;               -- wds
------------------------------------------------------------------------------------
			
	
