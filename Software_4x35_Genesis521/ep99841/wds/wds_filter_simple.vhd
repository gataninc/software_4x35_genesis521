	------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	wds_filer
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module performs peak-height analysis for the Ketek AXAS SDD Detector
--		The digital gain has been boosted high enough for an ASAS output of 1V gaussian pulses
--		for Mn55 (6KeV)
--
--
--	History
--		06/15/05 - MCS
--			Updated for QuartusII Version 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity wds_filter is 
	port( 
		imr			: in		std_logic;					-- Master Reset
		clk			: in		std_logic;					-- MAster Clock
		FIR_MR		: in		std_logic;
		din			: in		std_logic_Vector( 15 downto 0 );
		ds1			: buffer	std_logic_Vector( 15 downto 0 );
		dout			: buffer	std_logic_Vector( 15 downto 0 ) );		
end wds_filter;
------------------------------------------------------------------------------------

architecture behavioral of wds_filter is
	constant ADR_WIDTH			: integer := 4;
	constant DELAY_LEN			: integer := 16 - 2;	--  was 16
	constant Dout_LSB 			: integer := 1;


	signal acc				: std_logic_Vector( 19 downto 0 );
	signal acc_sum				: std_logic_Vector( 19 downto 0 );

	signal din_dly				: std_logic_Vector( 15 downto 0 );

	signal Gnd				: std_logic;

	signal radr				: std_logic_Vector( ADR_WIDTH-1 downto 0 );

	signal vcc				: std_Logic;

	signal wadr				: std_logic_Vector( ADR_WIDTH-1 downto 0 );
	
begin
     -------------------------------------------------------------------------
	-- Delay Memory Write/Read Address Generator
	wadr_counter : lpm_Counter
		generic map(
			LPM_WIDTH				=> ADR_WIDTH,
			LPM_DIRECTION			=> "Up" )
		port map(	
			aclr					=> imr,
			clock				=> clk,
			q					=> Wadr );
			
	radr <= wadr - DELAY_LEN;	
	-- Delay Memory Write/Read Address Generator
     -------------------------------------------------------------------------
		
     -------------------------------------------------------------------------
	-- Delay Memory Element
	DLY1 : lpm_ram_dp
		generic map(
			LPM_WIDTH				=> 16,	-- Data Width
			LPM_WIDTHAD			=> ADR_WIDTH,
			LPM_INDATA			=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_RDADDRESS_CONTROL	=> "REGISTERED",
			LPM_WRADDRESS_CONTROL	=> "REGISTERED" )
		PORT map(
			Wren					=> Vcc,
			wrclock				=> clk,
			rdclock 				=> clk,
			rdaddress 			=> radr, 
			wraddress				=> wadr,
			data					=> din,
			q					=> din_dly );
	-- Delay Memory Element
     -------------------------------------------------------------------------
			
     -------------------------------------------------------------------------
	-- Delay Subtractor
	DS_Add_Sub : lpm_add_sub
		generic map(
			LPM_WIDTH				=> 16,
			LPM_REPRESENTATION		=> "SIGNED",
			LPM_PIPELINE			=> 1 )
		port map(
			aclr					=> imr,
			clock				=> clk,
			add_sub				=> Gnd,
			Cin					=> Vcc,
			dataa				=> Din,
			datab				=> Din_Dly,
			result				=> DS1 );
	-- Delay Subtractor
     -------------------------------------------------------------------------

     -------------------------------------------------------------------------
	-- Accumulator of Delay Subtractor ( Sign extend to 20 bits )
	Acc_Adder : lpm_add_Sub
		generic map(
			LPM_WIDTH				=> 20,
			LPM_REPRESENTATION		=> "SIGNED" )
		port map(
			add_Sub				=> Vcc,
			Cin					=> Gnd,
			dataa				=> DS1(15) & DS1(15) & DS1(15) & DS1(15) & DS1,
			datab				=> Acc,
			result				=> Acc_Sum );
	
	Acc_ff : lpm_ff
		generic map(
			LPM_WIDTH				=> 20 )
		port map(
			aclr					=> imr,
			clock				=> clk,
			sclr					=> fir_mr,
			data					=> Acc_Sum,
			q					=> Acc );	
	-- Accumulator of Delay Subtractor ( Sign extend to 20 bits )
     --------------------------------------------------------------------------
	Dout_FF : lpm_ff
		generic map(
			LPM_WIDTH				=> 16 )
		port map(
			aclr					=> imr,
			clock				=> clk,
			data					=> Acc_Sum( Dout_LSB+15 downto DouT_LSB ),
			q					=> Dout );
     --------------------------------------------------------------------------
-------------------------------------------------------------------------------
end behavioral;				-- WDS Filter
-------------------------------------------------------------------------------
