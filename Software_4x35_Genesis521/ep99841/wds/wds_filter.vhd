	------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	wds_filer
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This modules performs a Tail-Pulse to "Triangular" Filter conversion
--		for the WDS SPectrometer
--
--
--	History
--		12/21/05 - MCS
--			Created
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity wds_filter is 
	port( 
		imr			: in		std_logic;					-- Master Reset
		clk			: in		std_logic;					-- MAster Clock
		FIR_MR		: in		std_logic;
		din			: in		std_logic_Vector( 15 downto 0 );
		dout			: buffer	std_logic_Vector( 15 downto 0 ) );		
end wds_filter;
------------------------------------------------------------------------------------

architecture behavioral of wds_filter is

	constant Dout_LSB			: integer := 4;				-- was 5
	constant DELAY_LEN			: integer := 8 - 2;				-- was 16
--	constant K1				: integer := ( 16384 * 950 ) /1000;	-- was .97 better than .96 ( better than 95 )
--	constant K1				: integer := ( 16384 * 980 ) /1000;	-- was .97 better than .96 ( better than 95 )
	constant K1				: integer := ( 16384 * 1000 ) /1000;	-- was .97 better than .96 ( better than 95 )

	signal acc1				: std_logic_Vector( 39 downto 0 );
	signal acc1_del			: std_logic_Vector( 39 downto 0 );
	signal acc2				: std_logic_Vector( 39 downto 0 );

	signal din_ext 			: std_logic_vector( 39 downto 0 );

	signal KAcc1				: std_logic_Vector( 79 downto 0 );

	signal PZ1				: std_logic_Vector( 39 downto 0 );

	signal radr				: std_logic_Vector( 3 downto 0 );

	signal wadr				: std_logic_Vector( 3 downto 0 );

     -------------------------------------------------------------------------
	component wds_ds is 
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			FIR_MR		: in		std_logic;
			wadr			: in		std_logic_vector( 3 downto 0 );
			radr			: in		std_logic_vector( 3 downto 0 );
			din			: in		std_logic_Vector( 39 downto 0 );
			dout			: buffer	std_logic_Vector( 39 downto 0 ) );		
	end component wds_ds;
     -------------------------------------------------------------------------

begin
     -------------------------------------------------------------------------
	-- Delay Memory Write/Read Address Generator
	wadr_counter : lpm_Counter
		generic map(
			LPM_WIDTH			=> 4,
			LPM_DIRECTION		=> "Up" )
		port map(	
			aclr				=> imr,
			clock			=> clk,
			q				=> Wadr );
			
	radr <= wadr - DELAY_LEN;	
	-- Delay Memory Write/Read Address Generator
     -------------------------------------------------------------------------

     -------------------------------------------------------------------------
	-- Sign Extend Din to 40 bits -------------------------------------------
	din_ext_gena : for i in 0 to 15 generate
		din_ext(i) 	<= din(i);
	end generate;

	din_ext_genb : for i in 16 to 39 generate
		Din_Ext(i)	<= Din(15);
	end generate;
	-- Sign Extend Din to 32 bits -------------------------------------------
     -------------------------------------------------------------------------
	
     -------------------------------------------------------------------------
	-- First Stage Delay Subtractor
	DS1 : wds_ds 
		port map(	
			imr				=> imr,
			clk				=> clk,
			fir_mr			=> fir_mr,
			wadr				=> wadr,
			radr				=> radr,
			din				=> din_ext,
			dout				=> Acc1 );
	-- First Stage Delay Subtractor
     --------------------------------------------------------------------------
		
     --------------------------------------------------------------------------
	acc1_del_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 40 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> acc1,
			q				=> acc1_del );
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- Multiplier for PZ1
	PZ1_MULT : lpm_mult
		generic map(
			LPM_WIDTHA		=> 40,
			LPM_WIDTHB		=> 40,
			LPM_WIDTHS		=> 80,
			LPM_WIDTHP		=> 80,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_HINT			=> "MAXIMIZE_SPEED=1" )
		port map(
			dataa			=> Acc1_del,
			datab			=> Conv_Std_logic_Vector( K1, 40 ),
			sum				=> conv_std_Logic_Vector(  0, 80 ),
			result			=> KAcc1 );
	-- Multiplier for PZ1
     --------------------------------------------------------------------------
		
     --------------------------------------------------------------------------
	-- Difference for PZ1
	PZ1_Add_Sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 40,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			Cin				=> '1',
			dataa			=> Acc1,
			datab			=> KAcc1( 39+14 downto 14 ),
			result			=> PZ1 );
	-- Difference for PZ1
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- Delay Subtractor 2
	DS2 : wds_ds 
		port map(	
			imr				=> imr,
			clk				=> clk,
			fir_mr			=> fir_mr,
			wadr				=> wadr,
			radr				=> radr,
			din				=> PZ1, 
			dout				=> Acc2 );
	-- Delay Subtractor 2
     --------------------------------------------------------------------------
	
     --------------------------------------------------------------------------
	-- Final Output ( including clamp )
	Dout_FF : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> Acc2( 15+Dout_LSB downto Dout_LSB ),
			q				=> Dout );
	-- Final Output ( including clamp )
     --------------------------------------------------------------------------

-------------------------------------------------------------------------------
end behavioral;				-- WDS Filter
-------------------------------------------------------------------------------
