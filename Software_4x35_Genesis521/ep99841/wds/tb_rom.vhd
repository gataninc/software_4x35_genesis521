------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	tb_rom.vhd
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module performs peak-height analysis for the Ketek AXAS SDD Detector
--		The digital gain has been boosted high enough for an ASAS output of 1V gaussian pulses
--		for Mn55 (6KeV)
--
--
--	History
--		06/15/05 - MCS
--			Updated for QuartusII Version 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

entity tb_rom is
	port(
		clk	: in 	std_Logic;
		data	: out	std_logic_vector( 15 downto 0 );
end tb_rom;
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
architecture test_bench of tb_rom is
	signal addr : integer := 0;
begin

	clock_proc : process( clk )
	begin
		if(( clk'event ) and ( clk = '1' )) then
			addr <= addr + 1;
			case addr is
          When 1 => data <= conv_Std_logic_vector( -33,16 );
          When 2 => data <= conv_Std_logic_vector( -31,16 );
          When 3 => data <= conv_Std_logic_vector( -29,16 );
          When 4 => data <= conv_Std_logic_vector( -29,16 );
          When 5 => data <= conv_Std_logic_vector( -30,16 );
          When 6 => data <= conv_Std_logic_vector( -29,16 );
          When 7 => data <= conv_Std_logic_vector( -29,16 );
          When 8 => data <= conv_Std_logic_vector( -29,16 );
          When 9 => data <= conv_Std_logic_vector( -30,16 );
          When 10 => data <= conv_Std_logic_vector( -32,16 );
          When 11 => data <= conv_Std_logic_vector( -33,16 );
          When 12 => data <= conv_Std_logic_vector( -34,16 );
          When 13 => data <= conv_Std_logic_vector( -33,16 );
          When 14 => data <= conv_Std_logic_vector( -32,16 );
          When 15 => data <= conv_Std_logic_vector( -30,16 );
          When 16 => data <= conv_Std_logic_vector( -29,16 );
          When 17 => data <= conv_Std_logic_vector( -29,16 );
          When 18 => data <= conv_Std_logic_vector( -31,16 );
          When 19 => data <= conv_Std_logic_vector( -33,16 );
          When 20 => data <= conv_Std_logic_vector( -34,16 );
          When 21 => data <= conv_Std_logic_vector( -33,16 );
          When 22 => data <= conv_Std_logic_vector( -32,16 );
          When 23 => data <= conv_Std_logic_vector( -30,16 );
          When 24 => data <= conv_Std_logic_vector( -30,16 );
          When 25 => data <= conv_Std_logic_vector( -30,16 );
          When 26 => data <= conv_Std_logic_vector( -32,16 );
          When 27 => data <= conv_Std_logic_vector( -32,16 );
          When 28 => data <= conv_Std_logic_vector( -28,16 );
          When 29 => data <= conv_Std_logic_vector( -25,16 );
          When 30 => data <= conv_Std_logic_vector( -29,16 );
          When 31 => data <= conv_Std_logic_vector( -34,16 );
          When 32 => data <= conv_Std_logic_vector( -35,16 );
          When 33 => data <= conv_Std_logic_vector( -33,16 );
          When 34 => data <= conv_Std_logic_vector( -30,16 );
          When 35 => data <= conv_Std_logic_vector( -27,16 );
          When 36 => data <= conv_Std_logic_vector( -29,16 );
          When 37 => data <= conv_Std_logic_vector( -30,16 );
          When 38 => data <= conv_Std_logic_vector( -30,16 );
          When 39 => data <= conv_Std_logic_vector( -29,16 );
          When 40 => data <= conv_Std_logic_vector( -32,16 );
          When 41 => data <= conv_Std_logic_vector( -33,16 );
          When 42 => data <= conv_Std_logic_vector( -30,16 );
          When 43 => data <= conv_Std_logic_vector( -27,16 );
          When 44 => data <= conv_Std_logic_vector( -27,16 );
          When 45 => data <= conv_Std_logic_vector( -29,16 );
          When 46 => data <= conv_Std_logic_vector( -29,16 );
          When 47 => data <= conv_Std_logic_vector( -28,16 );
          When 48 => data <= conv_Std_logic_vector( -23,16 );
          When 49 => data <= conv_Std_logic_vector( -20,16 );
          When 50 => data <= conv_Std_logic_vector( -21,16 );
          When 51 => data <= conv_Std_logic_vector( -23,16 );
          When 52 => data <= conv_Std_logic_vector( -18,16 );
          When 53 => data <= conv_Std_logic_vector( -13,16 );
          When 54 => data <= conv_Std_logic_vector( -9,16 );
          When 55 => data <= conv_Std_logic_vector( -8,16 );
          When 56 => data <= conv_Std_logic_vector( -6,16 );
          When 57 => data <= conv_Std_logic_vector( -4,16 );
          When 58 => data <= conv_Std_logic_vector( 0,16 );
          When 59 => data <= conv_Std_logic_vector( 4,16 );
          When 60 => data <= conv_Std_logic_vector( 9,16 );
          When 61 => data <= conv_Std_logic_vector( 12,16 );
          When 62 => data <= conv_Std_logic_vector( 16,16 );
          When 63 => data <= conv_Std_logic_vector( 19,16 );
          When 64 => data <= conv_Std_logic_vector( 24,16 );
          When 65 => data <= conv_Std_logic_vector( 28,16 );
          When 66 => data <= conv_Std_logic_vector( 30,16 );
          When 67 => data <= conv_Std_logic_vector( 31,16 );
          When 68 => data <= conv_Std_logic_vector( 34,16 );
          When 69 => data <= conv_Std_logic_vector( 37,16 );
          When 70 => data <= conv_Std_logic_vector( 42,16 );
          When 71 => data <= conv_Std_logic_vector( 45,16 );
          When 72 => data <= conv_Std_logic_vector( 46,16 );
          When 73 => data <= conv_Std_logic_vector( 45,16 );
          When 74 => data <= conv_Std_logic_vector( 46,16 );
          When 75 => data <= conv_Std_logic_vector( 49,16 );
          When 76 => data <= conv_Std_logic_vector( 54,16 );
          When 77 => data <= conv_Std_logic_vector( 59,16 );
          When 78 => data <= conv_Std_logic_vector( 61,16 );
          When 79 => data <= conv_Std_logic_vector( 62,16 );
          When 80 => data <= conv_Std_logic_vector( 63,16 );
          When 81 => data <= conv_Std_logic_vector( 64,16 );
          When 82 => data <= conv_Std_logic_vector( 63,16 );
          When 83 => data <= conv_Std_logic_vector( 63,16 );
          When 84 => data <= conv_Std_logic_vector( 66,16 );
          When 85 => data <= conv_Std_logic_vector( 68,16 );
          When 86 => data <= conv_Std_logic_vector( 66,16 );
          When 87 => data <= conv_Std_logic_vector( 63,16 );
          When 88 => data <= conv_Std_logic_vector( 62,16 );
          When 89 => data <= conv_Std_logic_vector( 62,16 );
          When 90 => data <= conv_Std_logic_vector( 62,16 );
          When 91 => data <= conv_Std_logic_vector( 62,16 );
          When 92 => data <= conv_Std_logic_vector( 61,16 );
          When 93 => data <= conv_Std_logic_vector( 60,16 );
          When 94 => data <= conv_Std_logic_vector( 59,16 );
          When 95 => data <= conv_Std_logic_vector( 58,16 );
          When 96 => data <= conv_Std_logic_vector( 58,16 );
          When 97 => data <= conv_Std_logic_vector( 57,16 );
          When 98 => data <= conv_Std_logic_vector( 55,16 );
          When 99 => data <= conv_Std_logic_vector( 53,16 );
          When 100 => data <= conv_Std_logic_vector( 53,16 );
          When 101 => data <= conv_Std_logic_vector( 53,16 );
          When 102 => data <= conv_Std_logic_vector( 52,16 );
          When 103 => data <= conv_Std_logic_vector( 50,16 );
          When 104 => data <= conv_Std_logic_vector( 48,16 );
          When 105 => data <= conv_Std_logic_vector( 46,16 );
          When 106 => data <= conv_Std_logic_vector( 46,16 );
          When 107 => data <= conv_Std_logic_vector( 46,16 );
          When 108 => data <= conv_Std_logic_vector( 48,16 );
          When 109 => data <= conv_Std_logic_vector( 48,16 );
          When 110 => data <= conv_Std_logic_vector( 45,16 );
          When 111 => data <= conv_Std_logic_vector( 42,16 );
          When 112 => data <= conv_Std_logic_vector( 41,16 );
          When 113 => data <= conv_Std_logic_vector( 41,16 );
          When 114 => data <= conv_Std_logic_vector( 40,16 );
          When 115 => data <= conv_Std_logic_vector( 40,16 );
          When 116 => data <= conv_Std_logic_vector( 40,16 );
          When 117 => data <= conv_Std_logic_vector( 41,16 );
          When 118 => data <= conv_Std_logic_vector( 40,16 );
          When 119 => data <= conv_Std_logic_vector( 39,16 );
          When 120 => data <= conv_Std_logic_vector( 37,16 );
          When 121 => data <= conv_Std_logic_vector( 36,16 );
          When 122 => data <= conv_Std_logic_vector( 35,16 );
          When 123 => data <= conv_Std_logic_vector( 35,16 );
          When 124 => data <= conv_Std_logic_vector( 34,16 );
          When 125 => data <= conv_Std_logic_vector( 34,16 );
          When 126 => data <= conv_Std_logic_vector( 34,16 );
          When 127 => data <= conv_Std_logic_vector( 34,16 );
          When 128 => data <= conv_Std_logic_vector( 33,16 );
          When 129 => data <= conv_Std_logic_vector( 32,16 );
          When 130 => data <= conv_Std_logic_vector( 31,16 );
          When 131 => data <= conv_Std_logic_vector( 31,16 );
          When 132 => data <= conv_Std_logic_vector( 30,16 );
          When 133 => data <= conv_Std_logic_vector( 30,16 );
          When 134 => data <= conv_Std_logic_vector( 30,16 );
          When 135 => data <= conv_Std_logic_vector( 30,16 );
          When 136 => data <= conv_Std_logic_vector( 27,16 );
          When 137 => data <= conv_Std_logic_vector( 24,16 );
          When 138 => data <= conv_Std_logic_vector( 24,16 );
          When 139 => data <= conv_Std_logic_vector( 25,16 );
          When 140 => data <= conv_Std_logic_vector( 27,16 );
          When 141 => data <= conv_Std_logic_vector( 27,16 );
          When 142 => data <= conv_Std_logic_vector( 24,16 );
          When 143 => data <= conv_Std_logic_vector( 20,16 );
          When 144 => data <= conv_Std_logic_vector( 19,16 );
          When 145 => data <= conv_Std_logic_vector( 19,16 );
          When 146 => data <= conv_Std_logic_vector( 18,16 );
          When 147 => data <= conv_Std_logic_vector( 17,16 );
          When 148 => data <= conv_Std_logic_vector( 17,16 );
          When 149 => data <= conv_Std_logic_vector( 17,16 );
          When 150 => data <= conv_Std_logic_vector( 19,16 );
          When 151 => data <= conv_Std_logic_vector( 19,16 );
          When 152 => data <= conv_Std_logic_vector( 16,16 );
          When 153 => data <= conv_Std_logic_vector( 12,16 );
          When 154 => data <= conv_Std_logic_vector( 12,16 );
          When 155 => data <= conv_Std_logic_vector( 13,16 );
          When 156 => data <= conv_Std_logic_vector( 14,16 );
          When 157 => data <= conv_Std_logic_vector( 14,16 );
          When 158 => data <= conv_Std_logic_vector( 11,16 );
          When 159 => data <= conv_Std_logic_vector( 9,16 );
          When 160 => data <= conv_Std_logic_vector( 12,16 );
          When 161 => data <= conv_Std_logic_vector( 15,16 );
          When 162 => data <= conv_Std_logic_vector( 13,16 );
          When 163 => data <= conv_Std_logic_vector( 10,16 );
          When 164 => data <= conv_Std_logic_vector( 9,16 );
          When 165 => data <= conv_Std_logic_vector( 9,16 );
          When 166 => data <= conv_Std_logic_vector( 11,16 );
          When 167 => data <= conv_Std_logic_vector( 11,16 );
          When 168 => data <= conv_Std_logic_vector( 9,16 );
          When 169 => data <= conv_Std_logic_vector( 5,16 );
          When 170 => data <= conv_Std_logic_vector( 3,16 );
          When 171 => data <= conv_Std_logic_vector( 2,16 );
          When 172 => data <= conv_Std_logic_vector( 3,16 );
          When 173 => data <= conv_Std_logic_vector( 4,16 );
          When 174 => data <= conv_Std_logic_vector( 4,16 );
          When 175 => data <= conv_Std_logic_vector( 3,16 );
          When 176 => data <= conv_Std_logic_vector( 2,16 );
          When 177 => data <= conv_Std_logic_vector( 1,16 );
          When 178 => data <= conv_Std_logic_vector( 0,16 );
          When 179 => data <= conv_Std_logic_vector( 0,16 );
          When 180 => data <= conv_Std_logic_vector( 2,16 );
          When 181 => data <= conv_Std_logic_vector( 4,16 );
          When 182 => data <= conv_Std_logic_vector( 4,16 );
          When 183 => data <= conv_Std_logic_vector( 2,16 );
          When 184 => data <= conv_Std_logic_vector( 0,16 );
          When 185 => data <= conv_Std_logic_vector( -3,16 );
          When 186 => data <= conv_Std_logic_vector( -3,16 );
          When 187 => data <= conv_Std_logic_vector( -4,16 );
          When 188 => data <= conv_Std_logic_vector( -5,16 );
          When 189 => data <= conv_Std_logic_vector( -7,16 );
          When 190 => data <= conv_Std_logic_vector( -6,16 );
          When 191 => data <= conv_Std_logic_vector( -5,16 );
          When 192 => data <= conv_Std_logic_vector( -3,16 );
          When 193 => data <= conv_Std_logic_vector( -2,16 );
          When 194 => data <= conv_Std_logic_vector( -2,16 );
          When 195 => data <= conv_Std_logic_vector( -3,16 );
          When 196 => data <= conv_Std_logic_vector( -4,16 );
          When 197 => data <= conv_Std_logic_vector( -5,16 );
          When 198 => data <= conv_Std_logic_vector( -6,16 );
          When 199 => data <= conv_Std_logic_vector( -7,16 );
          When 200 => data <= conv_Std_logic_vector( -8,16 );
          When 201 => data <= conv_Std_logic_vector( -9,16 );
          When 202 => data <= conv_Std_logic_vector( -8,16 );
          When 203 => data <= conv_Std_logic_vector( -8,16 );
          When 204 => data <= conv_Std_logic_vector( -7,16 );
          When 205 => data <= conv_Std_logic_vector( -8,16 );
          When 206 => data <= conv_Std_logic_vector( -10,16 );
          When 207 => data <= conv_Std_logic_vector( -12,16 );
          When 208 => data <= conv_Std_logic_vector( -11,16 );
          When 209 => data <= conv_Std_logic_vector( -10,16 );
          When 210 => data <= conv_Std_logic_vector( -11,16 );
          When 211 => data <= conv_Std_logic_vector( -13,16 );
          When 212 => data <= conv_Std_logic_vector( -15,16 );
          When 213 => data <= conv_Std_logic_vector( -15,16 );
          When 214 => data <= conv_Std_logic_vector( -12,16 );
          When 215 => data <= conv_Std_logic_vector( -10,16 );
          When 216 => data <= conv_Std_logic_vector( -12,16 );
          When 217 => data <= conv_Std_logic_vector( -15,16 );
          When 218 => data <= conv_Std_logic_vector( -15,16 );
          When 219 => data <= conv_Std_logic_vector( -13,16 );
          When 220 => data <= conv_Std_logic_vector( -12,16 );
          When 221 => data <= conv_Std_logic_vector( -13,16 );
          When 222 => data <= conv_Std_logic_vector( -16,16 );
          When 223 => data <= conv_Std_logic_vector( -19,16 );
          When 224 => data <= conv_Std_logic_vector( -19,16 );
          When 225 => data <= conv_Std_logic_vector( -17,16 );
          When 226 => data <= conv_Std_logic_vector( -14,16 );
          When 227 => data <= conv_Std_logic_vector( -14,16 );
          When 228 => data <= conv_Std_logic_vector( -18,16 );
          When 229 => data <= conv_Std_logic_vector( -23,16 );
          When 230 => data <= conv_Std_logic_vector( -22,16 );
          When 231 => data <= conv_Std_logic_vector( -20,16 );
          When 232 => data <= conv_Std_logic_vector( -19,16 );
          When 233 => data <= conv_Std_logic_vector( -20,16 );
          When 234 => data <= conv_Std_logic_vector( -21,16 );
          When 235 => data <= conv_Std_logic_vector( -22,16 );
          When 236 => data <= conv_Std_logic_vector( -20,16 );
          When 237 => data <= conv_Std_logic_vector( -18,16 );
          When 238 => data <= conv_Std_logic_vector( -19,16 );
          When 239 => data <= conv_Std_logic_vector( -21,16 );
          When 240 => data <= conv_Std_logic_vector( -22,16 );
          When 241 => data <= conv_Std_logic_vector( -23,16 );
          When 242 => data <= conv_Std_logic_vector( -24,16 );
          When 243 => data <= conv_Std_logic_vector( -25,16 );
          When 244 => data <= conv_Std_logic_vector( -25,16 );
          When 245 => data <= conv_Std_logic_vector( -25,16 );
          When 246 => data <= conv_Std_logic_vector( -24,16 );
          When 247 => data <= conv_Std_logic_vector( -23,16 );
          When 248 => data <= conv_Std_logic_vector( -23,16 );
          When 249 => data <= conv_Std_logic_vector( -23,16 );
          When 250 => data <= conv_Std_logic_vector( -24,16 );
          When 251 => data <= conv_Std_logic_vector( -25,16 );
          When 252 => data <= conv_Std_logic_vector( -25,16 );
          When 253 => data <= conv_Std_logic_vector( -24,16 );
          When 254 => data <= conv_Std_logic_vector( -23,16 );
          When 255 => data <= conv_Std_logic_vector( -22,16 );
          When 256 => data <= conv_Std_logic_vector( -23,16 );
          When 257 => data <= conv_Std_logic_vector( -25,16 );
          When 258 => data <= conv_Std_logic_vector( -28,16 );
          When 259 => data <= conv_Std_logic_vector( -29,16 );
          When 260 => data <= conv_Std_logic_vector( -27,16 );
          When 261 => data <= conv_Std_logic_vector( -23,16 );
          When 262 => data <= conv_Std_logic_vector( -22,16 );
          When 263 => data <= conv_Std_logic_vector( -22,16 );
          When 264 => data <= conv_Std_logic_vector( -24,16 );
          When 265 => data <= conv_Std_logic_vector( -26,16 );
          When 266 => data <= conv_Std_logic_vector( -27,16 );
          When 267 => data <= conv_Std_logic_vector( -27,16 );
          When 268 => data <= conv_Std_logic_vector( -28,16 );
          When 269 => data <= conv_Std_logic_vector( -28,16 );
          When 270 => data <= conv_Std_logic_vector( -28,16 );
          When 271 => data <= conv_Std_logic_vector( -27,16 );
          When 272 => data <= conv_Std_logic_vector( -26,16 );
          When 273 => data <= conv_Std_logic_vector( -25,16 );
          When 274 => data <= conv_Std_logic_vector( -25,16 );
          When 275 => data <= conv_Std_logic_vector( -25,16 );
          When 276 => data <= conv_Std_logic_vector( -26,16 );
          When 277 => data <= conv_Std_logic_vector( -27,16 );
          When 278 => data <= conv_Std_logic_vector( -28,16 );
          When 279 => data <= conv_Std_logic_vector( -28,16 );
          When 280 => data <= conv_Std_logic_vector( -28,16 );
          When 281 => data <= conv_Std_logic_vector( -26,16 );
          When 282 => data <= conv_Std_logic_vector( -25,16 );
          When 283 => data <= conv_Std_logic_vector( -25,16 );
          When 284 => data <= conv_Std_logic_vector( -27,16 );
          When 285 => data <= conv_Std_logic_vector( -30,16 );
          When 286 => data <= conv_Std_logic_vector( -30,16 );
          When 287 => data <= conv_Std_logic_vector( -30,16 );
          When 288 => data <= conv_Std_logic_vector( -28,16 );
          When 289 => data <= conv_Std_logic_vector( -27,16 );
          When 290 => data <= conv_Std_logic_vector( -28,16 );
          When 291 => data <= conv_Std_logic_vector( -30,16 );
          When 292 => data <= conv_Std_logic_vector( -30,16 );
          When 293 => data <= conv_Std_logic_vector( -31,16 );
          When 294 => data <= conv_Std_logic_vector( -31,16 );
          When 295 => data <= conv_Std_logic_vector( -31,16 );
          When 296 => data <= conv_Std_logic_vector( -26,16 );
          When 297 => data <= conv_Std_logic_vector( -23,16 );
          When 298 => data <= conv_Std_logic_vector( -25,16 );
          When 299 => data <= conv_Std_logic_vector( -29,16 );
          When 300 => data <= conv_Std_logic_vector( -31,16 );
          When 301 => data <= conv_Std_logic_vector( -31,16 );
          When 302 => data <= conv_Std_logic_vector( -29,16 );
          When 303 => data <= conv_Std_logic_vector( -27,16 );
          When 304 => data <= conv_Std_logic_vector( -27,16 );
          When 305 => data <= conv_Std_logic_vector( -28,16 );
          When 306 => data <= conv_Std_logic_vector( -30,16 );
          When 307 => data <= conv_Std_logic_vector( -31,16 );
          When 308 => data <= conv_Std_logic_vector( -31,16 );
          When 309 => data <= conv_Std_logic_vector( -30,16 );
          When 310 => data <= conv_Std_logic_vector( -30,16 );
          When 311 => data <= conv_Std_logic_vector( -30,16 );
          When 312 => data <= conv_Std_logic_vector( -28,16 );
          When 313 => data <= conv_Std_logic_vector( -27,16 );
          When 314 => data <= conv_Std_logic_vector( -29,16 );
          When 315 => data <= conv_Std_logic_vector( -31,16 );
          When 316 => data <= conv_Std_logic_vector( -29,16 );
          When 317 => data <= conv_Std_logic_vector( -27,16 );
          When 318 => data <= conv_Std_logic_vector( -28,16 );
          When 319 => data <= conv_Std_logic_vector( -29,16 );
          When 320 => data <= conv_Std_logic_vector( -28,16 );
          When 321 => data <= conv_Std_logic_vector( -27,16 );
          When 322 => data <= conv_Std_logic_vector( -30,16 );
          When 323 => data <= conv_Std_logic_vector( -33,16 );
          When 324 => data <= conv_Std_logic_vector( -33,16 );
          When 325 => data <= conv_Std_logic_vector( -31,16 );
          When 326 => data <= conv_Std_logic_vector( -29,16 );
          When 327 => data <= conv_Std_logic_vector( -28,16 );
          When 328 => data <= conv_Std_logic_vector( -30,16 );
          When 329 => data <= conv_Std_logic_vector( -32,16 );
          When 330 => data <= conv_Std_logic_vector( -31,16 );
          When 331 => data <= conv_Std_logic_vector( -29,16 );
          When 332 => data <= conv_Std_logic_vector( -30,16 );
          When 333 => data <= conv_Std_logic_vector( -31,16 );
          When 334 => data <= conv_Std_logic_vector( -30,16 );
          When 335 => data <= conv_Std_logic_vector( -29,16 );
          When 336 => data <= conv_Std_logic_vector( -31,16 );
          When 337 => data <= conv_Std_logic_vector( -33,16 );
          When 338 => data <= conv_Std_logic_vector( -33,16 );
          When 339 => data <= conv_Std_logic_vector( -31,16 );
          When 340 => data <= conv_Std_logic_vector( -27,16 );
          When 341 => data <= conv_Std_logic_vector( -23,16 );
          When 342 => data <= conv_Std_logic_vector( -22,16 );
          When 343 => data <= conv_Std_logic_vector( -23,16 );
          When 344 => data <= conv_Std_logic_vector( -29,16 );
          When 345 => data <= conv_Std_logic_vector( -33,16 );
          When 346 => data <= conv_Std_logic_vector( -31,16 );
          When 347 => data <= conv_Std_logic_vector( -27,16 );
          When 348 => data <= conv_Std_logic_vector( -26,16 );
          When 349 => data <= conv_Std_logic_vector( -26,16 );
          When 350 => data <= conv_Std_logic_vector( -29,16 );
          When 351 => data <= conv_Std_logic_vector( -31,16 );
          When 352 => data <= conv_Std_logic_vector( -31,16 );
          When 353 => data <= conv_Std_logic_vector( -29,16 );
          When 354 => data <= conv_Std_logic_vector( -25,16 );
          When 355 => data <= conv_Std_logic_vector( -23,16 );
          When 356 => data <= conv_Std_logic_vector( -22,16 );
          When 357 => data <= conv_Std_logic_vector( -24,16 );
          When 358 => data <= conv_Std_logic_vector( -27,16 );
          When 359 => data <= conv_Std_logic_vector( -29,16 );
          When 360 => data <= conv_Std_logic_vector( -26,16 );
          When 361 => data <= conv_Std_logic_vector( -23,16 );
          When 362 => data <= conv_Std_logic_vector( -23,16 );
          When 363 => data <= conv_Std_logic_vector( -26,16 );
          When 364 => data <= conv_Std_logic_vector( -28,16 );
          When 365 => data <= conv_Std_logic_vector( -29,16 );
          When 366 => data <= conv_Std_logic_vector( -26,16 );
          When 367 => data <= conv_Std_logic_vector( -23,16 );
          When 368 => data <= conv_Std_logic_vector( -21,16 );
          When 369 => data <= conv_Std_logic_vector( -22,16 );
          When 370 => data <= conv_Std_logic_vector( -23,16 );
          When 371 => data <= conv_Std_logic_vector( -24,16 );
          When 372 => data <= conv_Std_logic_vector( -23,16 );
          When 373 => data <= conv_Std_logic_vector( -22,16 );
          When 374 => data <= conv_Std_logic_vector( -22,16 );
          When 375 => data <= conv_Std_logic_vector( -23,16 );
          When 376 => data <= conv_Std_logic_vector( -24,16 );
          When 377 => data <= conv_Std_logic_vector( -24,16 );
          When 378 => data <= conv_Std_logic_vector( -22,16 );
          When 379 => data <= conv_Std_logic_vector( -21,16 );
          When 380 => data <= conv_Std_logic_vector( -22,16 );
          When 381 => data <= conv_Std_logic_vector( -24,16 );
          When 382 => data <= conv_Std_logic_vector( -23,16 );
          When 383 => data <= conv_Std_logic_vector( -20,16 );
          When 384 => data <= conv_Std_logic_vector( -13,16 );
          When 385 => data <= conv_Std_logic_vector( -7,16 );
          When 386 => data <= conv_Std_logic_vector( -4,16 );
          When 387 => data <= conv_Std_logic_vector( -4,16 );
          When 388 => data <= conv_Std_logic_vector( -5,16 );
          When 389 => data <= conv_Std_logic_vector( -5,16 );
          When 390 => data <= conv_Std_logic_vector( 0,16 );
          When 391 => data <= conv_Std_logic_vector( 6,16 );
		when others => data <= conv_Std_logic_Vector( 0, 16 );
			end case;
		end if;
	end process;
------------------------------------------------------------------------------------
end test_bench;
------------------------------------------------------------------------------------