	------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	WDS_DS
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module performs the delay subtraction and accumulate function for the
--		Tail-Pulse conversion to a Triangular pulse
--
--	History
--		12/21/05 - MCS
--			Created
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity wds_ds is 
	port( 
		imr			: in		std_logic;					-- Master Reset
		clk			: in		std_logic;					-- MAster Clock
		FIR_MR		: in		std_logic;
		wadr			: in		std_logic_vector( 3 downto 0 );
		radr			: in		std_logic_vector( 3 downto 0 );
		din			: in		std_logic_Vector( 39 downto 0 );
		dout			: buffer	std_logic_Vector( 39 downto 0 ) );		
end wds_ds;
------------------------------------------------------------------------------------

architecture behavioral of wds_ds is
	signal acc				: std_logic_Vector( 39 downto 0 );
	signal din_dly				: std_logic_Vector( 39 downto 0 );
	signal ds					: std_logic_Vector( 39 downto 0 );
	signal sum				: std_logic_Vector( 39 downto 0 );
	
begin		
     -------------------------------------------------------------------------
	-- Delay Memory Elements
	DLY1 : lpm_ram_dp
		generic map(
			LPM_WIDTH				=> 40,	-- Data Width
			LPM_WIDTHAD			=> 4,
			LPM_INDATA			=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_RDADDRESS_CONTROL	=> "REGISTERED",
			LPM_WRADDRESS_CONTROL	=> "REGISTERED" )
		PORT map(
			Wren					=> '1',
			wrclock				=> clk,
			rdclock 				=> clk,
			rdaddress 			=> radr, 
			wraddress				=> wadr,
			data					=> din,
			q					=> din_dly );
	-- Delay Memory Element
     -------------------------------------------------------------------------
			
     -------------------------------------------------------------------------
	-- Delay Subtractor
	DS_Add_Sub : lpm_add_sub
		generic map(
			LPM_WIDTH				=> 40,
			LPM_DIRECTION			=> "SUB",
			LPM_REPRESENTATION		=> "SIGNED" )
		port map(
			Cin					=> '1',
			dataa				=> Din,
			datab				=> Din_Dly,
			result				=> DS );
	-- Delay Subtractor
     -------------------------------------------------------------------------

     -------------------------------------------------------------------------
	-- Accumulator of Delay Subtractor ( Sign extend to 20 bits )
	Acc_Adder : lpm_add_Sub
		generic map(
			LPM_WIDTH				=> 40,
			LPM_DIRECTION			=> "ADD",
			LPM_REPRESENTATION		=> "SIGNED" )
		port map(
			Cin					=> '0',
			dataa				=> DS,
			datab				=> Acc,
			result				=> Sum );
	
	Acc_ff : lpm_ff
		generic map(
			LPM_WIDTH				=> 40 )
		port map(
			aclr					=> imr,
			clock				=> clk,
			sclr					=> fir_mr,
			data					=> Sum,
			q					=> Acc );	

	Dout_ff : lpm_ff
		generic map(
			LPM_WIDTH				=> 40 )
		port map(
			aclr					=> imr,
			clock				=> clk,
			sclr					=> fir_mr,
			data					=> Sum,
			q					=> Dout );	
	-- Accumulator of Delay Subtractor ( Sign extend to 20 bits )
     --------------------------------------------------------------------------
	
-------------------------------------------------------------------------------
end behavioral;				-- wds_ds
-------------------------------------------------------------------------------
