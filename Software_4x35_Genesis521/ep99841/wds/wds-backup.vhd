------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	wds
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module performs peak-height analysis for the Ketek AXAS SDD Detector
--		The digital gain has been boosted high enough for an ASAS output of 1V gaussian pulses
--		for Mn55 (6KeV)
--
--
--	History
--		01/16/06 - MCS : Ver 4005
--			Shorten to_reject to 750 ns
--			TODO:  Create "Pulse Busy Timeout for 25us to prevent baseline histogram update
--			FIX 0 Peak spike
--			FIX Wrap-around (1.5V input )
--		06/15/05 - MCS
--			Updated for QuartusII Version 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity wds is 
	port( 
		imr				: in		std_logic;					-- Master Reset
		clk				: in		std_logic;					-- MAster Clock
		Time_Clr			: in		std_logic;
		Time_Enable		: in		std_logic;		
		FIR_MR			: in		std_logic;
		Memory_Access		: in		std_logic;
		ROI_WE			: in		std_logic;
		DSP_D			: in		std_logic_vector( 15 downto 0 );	-- DSP Data Bus
		DSP_RAMA			: in		std_logic_Vector(  7 downto 0 );	-- DSP Address Bus
		Disc_en			: in		std_logic;
		Din				: in		std_logic_vector( 15 downto 0 );
		Tlevel			: in		std_logic_vector( 15 downto 0 );
		BLEVEL			: buffer	std_logic_vector(  7 downto 0 );
		BLEVEL_UPD		: buffer	std_logic;
		FDISC			: buffer	std_logic;
		PK_DONE			: buffer	std_logic;
		PBusy			: buffer	std_Logic;
		ROI_SUM			: buffer	std_logic_vector( 31 downto 0 );
		ROI_LU			: buffer	std_logic_Vector( 15 downto 0 );	-- ROI Lookup Data
		dout				: buffer  std_logic_Vector( 15 downto 0 );
		CPeak			: buffer	std_Logic_Vector( 15 downto 0 ) );
end wds;
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
architecture behavioral of wds is
	-- Constant Declarations ---------------------------------------------------------

	constant blevel_cnt_max		: integer := 199;		-- 10 us
	constant to_reject 			: integer := (   800/50 ) - 1;				-- 500ns @ 20Mhz
	constant to_busy			: integer := 11;
	constant Din_Clamp_Val		: integer := 7400;
	constant Din_Diff_Abs_thr 	: integer := 70;

	-- Type Declarations ---------------------------------------------------------

	type FD_STATE_TYPE is (
		ST_IDLE,
		ST_BUSY,
		ST_Wait_Nslope_Active,
		St_Wait_Nslope_Inactive );

	-- Signal Declarations ---------------------------------------------------------

	signal blevel_cnt 			: integer range 0 to blevel_cnt_max;
	signal blevel_cnt_Tc		: std_logic;
	signal blevel_en			: std_logic;
	
	signal CPeak_Clamp_Max		: std_logic;
	
	signal din_del 			: std_logic_Vector( 15 downto 0 );
	signal Din_Diff 			: std_Logic_Vector( 15 downto 0 );
	signal din_diff_abs 		: std_logic_vector( 15 downto 0 );
	signal Din_NSlope_Cmp	 	: std_logic;
	signal Din_NSlope_Vec		: std_Logic_Vector( 3 downto 0 );

--	signal Din_PSlope			: std_logic;
--	signal din_PSlope_Cmp 		: std_logic;
	signal Din_PSlope_Gen		: std_logic;
	signal Din_PSlope_Gen_Cmp 	: std_logic;
	signal Din_PSlope_Gen_Vec 	: std_logic_Vector( 2 downto 0 );
--	signal Din_Pslope_Vec 		: std_logic_Vector( 2 downto 0 );
	signal Din_Slope_Det 		: std_logic;
	
	signal Dout_Del			: std_logic_vector( 15 downto 0 );
	signal dout_max_cmp			: std_logic;
	signal Dout_Max_Clr 		: std_logic;
	signal Dout_Max_En			: std_logic;
	signal Dout_PSlope_cmp		: std_logic;
	signal Dout_PSlope_vec		: std_logic_Vector( 2 downto 0 );
	signal Dout_PSlope			: std_logic;

	signal FD_STATE 			: FD_STATE_TYPE;
	signal FD_State_Del			: FD_State_type;

	signal Gnd				: std_logic;

	signal Level_Cmp 			: std_Logic;
	
	signal Pk_Done_Del			: std_logic;
	
	signal ROI_INC				: std_logic;
	signal ROI_DEF				: std_logic;
	signal ROI_ADDR			: std_logic_Vector(  7 downto 0 );

	signal TLevel_Reg			: std_Logic_Vector( 15 downto 0 );
	signal Tlevel_Vec			: std_logic_vector(  1 downto 0 );
	signal TO_Cnt				: integer range 0 to to_reject;
	signal TO_Cnt_Mux			: integer range 0 to to_reject;
	signal To_Cnt_Tc 			: std_logic;

	signal Vcc				: std_logic;

	signal Zero_Peak			: std_logic;

	------------------------------------------------------------------------------------
	component wds_filter is 
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			FIR_MR		: in		std_logic;
			din			: in		std_logic_Vector( 15 downto 0 );
			dout			: buffer	std_logic_Vector( 15 downto 0 ) );		
	end component wds_filter;
	------------------------------------------------------------------------------------

	
begin
	Gnd		<= '0';
	Vcc		<= '1';
	
	PBUSY 	<= '0' when ( FD_State = ST_IDLE ) else '1';
	
	FDisc 	<= '1' when (( PBusy = '0' ) and ( Tlevel_Vec = "01" ))  else '0';

	with FD_State select
		to_cnt_mux <= to_busy when ST_Busy,
				    to_reject when others;
				
	To_Cnt_Tc <= '1' when ( To_Cnt = to_cnt_mux ) else '0';

	------------------------------------------------------------------------------------
	-- Pipeline Threshold Level
	tlevel_reg_ff : lpm_ff
		generic map(
			LPM_WIDTH				=> 16 )
		port map(
			aclr					=> imr,
			clock				=> clk,
			data					=> TLevel,
			q					=> TLevel_Reg );
	-- Pipeline Threshold Level
	------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------
	-- Input Slope Determination
	din_del_ff : lpm_ff
		generic map(
			LPM_WIDTH				=> 16 )
		port map(
			aclr					=> imr,
			clock				=> clk,
			data					=> din,
			q					=> din_del );
			
	din_diff_add_sub : lpm_add_sub
		generic map(
			LPM_WIDTH				=> 16,
			LPM_REPRESENTATION		=> "SIGNED" )
		port map(
			add_sub				=> Gnd,	-- Subtract
			Cin					=> Vcc,
			dataa				=> Din,
			datab				=> Din_Del,
			result				=> Din_Diff );
			
	din_diff_absolute : lpm_abs
		generic map(
			LPM_WIDTH				=> 16 )
		port map(
			data					=> Din_Diff,
			result				=> Din_Diff_Abs );
			
	din_diff_abs_Cmp : lpm_compare
		generic map(
			LPM_WIDTH				=> 16,
			LPM_REPRESENTATIOn		=> "Signed" )
		port map(
			dataa				=> Din_Diff_abs,
			datab				=> Conv_Std_logic_Vector( Din_Diff_Abs_thr, 16 ),	
			agb					=> Din_Slope_Det );		
			
	din_diff_sign : lpm_compare
		generic map(
			LPM_WIDTH				=> 16,
			LPM_REPRESENTATION		=> "SIGNED" )
		port map(
			dataa				=> Din_Diff,
			datab				=> conv_std_logic_Vector( 0, 16 ),
--			agb					=> Din_PSlope_Cmp,
			alb					=> Din_NSlope_Cmp );
			
	Din_Pslope_Compare : lpm_compare
		generic map(
			LPM_WIDTH				=> 16,
			LPM_REPRESENTATION		=> "SIGNED" )
		port map(
			dataa				=> Din,
			datab				=> Din_Del,
			agb					=> Din_Pslope_Gen_Cmp );
	-- Input Slope Determination
	------------------------------------------------------------------------------------
			
	------------------------------------------------------------------------------------
	-- Filter the Data
	--	Convert Tail Pulse to Triangular Filter
	U_Filter : wds_filter
		port map(
			imr					=> imr,
			clk					=> clk,
			fir_mr				=> fir_Mr,
			din					=> Din, 		-- Input Data
			dout					=> dout );		-- Filter Output
	--	Convert Tail Pulse to Triangular Filter
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- Dout Peak Determination	
	dout_max_Compare : lpm_compare		
		generic map(
			LPM_WIDTH				=> 16,
			LPM_REPRESENTATION		=> "SIGNED" )
		port map(	
			dataa				=> Dout,	
			datab				=> CPEak,
			agb					=> dout_max_cmp );
			
	Dout_Max_Clr 	<= '1' when (( fdisc 		= '1' ) and ( FD_State = ST_IDLE )) else
				   '1' when  ( PK_Done_Del 	= '1' ) else 				
	   			   '0';
	
	CPeak_Clamp_Max <= '0' when ( Dout( 15 downto 12 ) = x"0" ) else '1';

	-- Prevent Peak from being altered during 
	Dout_Max_En	<= '1' when (   Dout_Max_Clr  = '1' 		) else
				   '1' when (   CPeak_Clamp_Max = '1' 		) else
				   '1' when ( ( Level_Cmp 	= '1' 		) 
						and ( Dout_PSlope 	= '1'		) 
						and ( Dout_max_Cmp 	= '1' 		)) else 
				   '0';

	CPEak_FF : LPM_FF
		generic map(
			LPM_WIDTH				=> 16 )
		port map(
			aclr					=> imr,
			clock				=> clk,
			sclr					=> Dout_Max_Clr,
			sset					=> CPeak_Clamp_Max,
			enable				=> Dout_max_En,
			data					=> Dout,
			q					=> CPEak );
			
	Zero_Peak <= '1' when ( conv_integer( CPeak ) = 0 ) else '0';
	-- Dout Peak Determination	
    --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- Positive Slope Determination
	Dout_Del_FF : lpm_ff
		generic map(
			LPM_WIDTH				=> 16  )
		port map(
			aclr					=> imr,
			clock				=> CLk,
			data					=> Dout,
			q					=> Dout_DEL );

	Dout_PSlope_Comparee : lpm_compare
		generic map(
			LPM_WIDTH				=> 16,
			LPM_REPRESENTATION		=> "SIGNED" )
		port map(
			dataa				=> Dout,
			datab				=> Dout_DEL,
			agb					=> Dout_PSlope_cmp );
     --------------------------------------------------------------------------

     -------------------------------------------------------------------------
	-- Thrshold with Accumulator Output ( in the same range an Dout or CPeak )
	level_cmp_compare : lpm_compare
		generic map(
			LPM_WIDTH				=> 16,
			LPM_REPRESENTATION		=> "SIGNED")
		port map(
			dataa				=> Dout, 
			datab				=> Tlevel_Reg,
			agb					=> Level_Cmp );
     -------------------------------------------------------------------------

     --------------------------------------------------------------------------
    	-- Baseline Level
	blevel_cnt_tc	<= '1' when ( blevel_cnt = blevel_cnt_max ) else '0';

	
	blevel_en		<= '1' when ( PBUsy = '0' ) else '0';
	
	BLEVEL_FF : LPM_FF
		generic map(
			LPM_WIDTH				=> 8 )
		port map(	
			aclr					=> imr,
			clock				=> clk,
			enable				=> Blevel_EN,
			data					=> Dout( 7 downto 0 ),
			q					=> Blevel );
    	-- Baseline Level
	-------------------------------------------------------------------------------
			
     -------------------------------------------------------------------------------
	-- ROI Counter - Counts all evenets in defined ROI

	ROI_ADDR	<= CPEak( 11 downto 4 ) when ( Memory_Access = '0' ) else
			   DSP_RAMA( 7 downto 0 );

	ROI_RAM : LPM_RAM_DQ
		generic map(
			LPM_WIDTH				=> 16,
			LPM_WIDTHAD			=> 8,
			LPM_INDATA			=> "REGISTERED",
			LPM_ADDRESS_CONTROL		=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_FILE				=> "ROI_RAM.MIF",
			LPM_TYPE				=> "LPM_RAM_DQ" )
		port map(
			inclock				=> clk,
			outclock				=> CLK,
			data					=> DSP_D,
			address				=> ROI_ADDR,
			we					=> ROI_WE,
			q					=> ROI_LU );

	ROI_DEF  	<= ROI_LU( Conv_Integer( CPEak( 3 downto 0 )));
	
	ROI_INC <= '1' when (   ( Time_Enable 	= '1' ) 
					and ( Pk_Done		= '1' ) 
					and ( ROI_DEF 		= '1' )) else '0';

	ROI_SUM_CNTR : LPM_COUNTER
		generic map(
			LPM_WIDTH				=> 32 )
		port map(		
			aclr					=> IMR,
			clock				=> CLK,
			sclr					=> Time_Clr,
			cnt_en				=> ROI_INC,
			q					=> ROI_SUM );			
	-- ROI Defined Look-up Memory
	
    --------------------------------------------------------------------------
	clock_proc : process( imr, clk )
	begin
		if( imr = '1' ) then
			blevel_upd		<= '0';
			blevel_Cnt		<= 0;

			Dout_PSlope_vec	<= "000";
			Dout_PSlope		<= '0';
--			Din_PSlope_Vec		<= "000";
			Din_NSlope_Vec		<= "0000";
	
			PK_DONE			<= '0';
			to_cnt 			<= 0;
			FD_State 			<= ST_Idle;				
			PK_Done_Del		<= '0';
--			Din_Pslope 		<= '0';
--			Din_NSlope		<= '0';
			Din_PSlope_Gen_Vec	<= "000";
			Din_PSlope_Gen		<= '0';
			FD_State_Del		<= ST_Idle;

		elsif(( clk'event ) and ( clk = '1' )) then
		
			-- Baseline Level Timing Generation --------------------------------------------
			if(( blevel_en = '1' ) and ( blevel_cnt_tc = '1' ) and ( PBUSY = '0' ))
				then blevel_upd <= '1';
				else blevel_upd <= '0';
			end if;
			
			if(( blevel_en = '1' ) and ( blevel_cnt_tc = '1' ))
				then blevel_Cnt <= 0;
			elsif(( Blevel_En = '1' ) and ( Blevel_Cnt_Tc = '0' ))
				then blevel_cnt <= blevel_cnt + 1;
			end if;		
			-- Baseline Level Timing Generation --------------------------------------------

			-- Determine the Slope of the Input data bus -----------------------------------		
--			if(( Disc_En = '1' ) and ( Level_Cmp = '1' ) and ( Din_Pslope_Cmp = '1' ) and ( Din_Slope_Det = '1' ))
--				then Din_PSlope_Vec <= Din_PSlope_Vec( 1 downto 0 ) & '1';
--				else Din_Pslope_Vec <= Din_PSlope_Vec( 1 downto 0 ) & '0';
--			end if;
			if(( Disc_En = '1' ) and ( Level_Cmp = '1' ) and ( Din_Nslope_Cmp = '1' ) and ( Din_Slope_Det = '1' ))
				then Din_NSlope_Vec <= Din_NSlope_Vec( 2 downto 0 ) & '1';
				else Din_NSlope_Vec <= Din_NSlope_Vec( 2 downto 0 ) & '0';
			end if;

--			Din_PSlope <= Din_Pslope_Vec(2);
			
--			Din_NSlope <= Din_NSlope_Vec(2);
			-- Determine the Slope of the Input data bus -----------------------------------

			-- Determine the Slope of the Output data bus ----------------------------------
			Dout_PSlope_vec 	<= Dout_PSlope_Vec( 1 downto 0 ) & Dout_PSlope_Cmp;
			
			if( Dout_PSlope_Vec = "111" )
				then Dout_PSlope <= '1';
			elsif( Dout_PSlope_Vec = "000" )
				then Dout_PSlope <= '0';
			end if;
			
			if(( Disc_En = '1' ) and ( Level_Cmp = '1' ) and ( Din_Pslope_Gen_Cmp = '1' ))
				then Din_Pslope_Gen_Vec <= Din_PSlope_Gen_Vec( 1 downto 0 ) & '1';
				else Din_Pslope_Gen_Vec <= Din_PSlope_Gen_Vec( 1 downto 0 ) & '0';
			end if;
			
			if( Din_PSlope_Gen_Vec = "111" )
				then Din_PSlope_Gen <= '1';
			elsif( Din_PSlope_Gen_Vec = "000" )
				then Din_Pslope_Gen <= '0';
			end if;
			-- Determine the Slope of the Output data bus ----------------------------------

			-- Threshold Determination ( Level or Positive Slope )
			if(( Disc_En = '1' ) and ( Level_Cmp = '1' ) and ( Din_PSlope_Gen = '1' )) 
				then TLevel_Vec	<= Tlevel_Vec(0) & '1';
				else Tlevel_Vec 	<= Tlevel_Vec(0) & '0';
			end if;


			if(( FD_State = ST_Busy ) and ( to_cnt_tc = '1' ) and ( Zero_Peak = '0' )) 
				then PK_DONE <= '1';
				else PK_DONE <= '0';
			end if;
			
			PK_Done_Del <= Pk_Done;
				
			---------------------------------------------------------------------------------------
			-- State Machine Control and State Machine
			if( FD_State /= FD_State_Del )	-- State changes - Reset Counter
				then To_Cnt <= 0;
			elsif( PBUSY = '1' )		
				then To_Cnt <= To_Cnt + 1;	
			end if;

			FD_State_Del <= FD_State;

			case FD_STATE is
				when ST_IDLE =>
					if( fdisc = '1' )	
						then FD_State <= ST_BUSY;
					end if;
				
				when ST_BUSY =>
					if(( To_Cnt_Tc = '1' ) and ( Din_Nslope_vec(3) = '0' ))
						then FD_State <= ST_Wait_NSlope_Active;
					elsif(( to_Cnt_Tc = '1' ) and ( Din_NSlope_vec(3) = '1' ))
						then FD_State <= St_Wait_Nslope_Inactive;
					end if;
				when ST_Wait_NSlope_Active =>
					if( Din_NSlope_vec(3) = '1' )
						then FD_State <= ST_Wait_Nslope_Inactive;
					elsif( to_Cnt_Tc = '1' )
						then FD_State <= ST_Wait_Nslope_Inactive;
					end if;
					
				when St_Wait_Nslope_Inactive =>
					if( Din_NSlope_vec(3) = '0' )
						then FD_State <= ST_Idle;
					elsif( to_cnt_Tc = '1' )
						then FD_State <= ST_Idle;
					end if;
			end case;
			-- State Machine Control and State Machine
			---------------------------------------------------------------------------------------

		end if;
			
	end process;
     -------------------------------------------------------------------------------
------------------------------------------------------------------------------------
end behavioral;               -- wds
------------------------------------------------------------------------------------
			
	
