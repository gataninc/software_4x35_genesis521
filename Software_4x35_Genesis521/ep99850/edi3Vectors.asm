	.title	"edi3vectors.asm"
	.ref _c_int01, _c_int02,_c_int03, _c_int04, _c_int09, _trap
	.sect  "vectors"; interrupt vectors located starting at 883000H
	.word		_trap    	;reserved ( Reset )
	.word       _c_int01   	;int0 service (isr0)	SEM
	.word       _c_int02   	;int1 service (isr1)	EDS
	.word		_c_int03   	;int2 service (isr2)	PCI
	.word       _c_int04   	;int3 service (isr3)	Serial Bus
	.word       _trap       ;xint0
	.word       _trap       ;rint0
	.word		_trap       ;reserved
	.word		_trap       ;reserved
	.word       _c_int09    ;tint0
	.word		_trap	    ;tint1 (tsr1)
	.word       _trap       ;dint0
	.word       _trap       ;dint1
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap  
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    
	.word		_trap    

	.end
