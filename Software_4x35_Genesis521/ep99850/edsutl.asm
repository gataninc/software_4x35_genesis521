;******************************************************************************
;* TMS320C3x/4x ANSI C Code Generator                            Version 5.11 *
;* Date/Time created: Thu May 24 17:11:47 2007                                *
;******************************************************************************
	.regalias	; enable floating point register aliases
fp	.set	ar3
FP	.set	ar3
;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Silicon Info       : C32 Revision PG1                                    *
;*   Optimization       : Conservatively Choose Speed over Size               *
;*   Memory             : Small Memory Model                                  *
;*   Float-to-Int       : Fast Conversions (round toward -inf)                *
;*   Multiply           : in Software (32 bits)                               *
;*   Memory Info        : Unmapped Memory Exists                              *
;*   Repeat Loops       : Use RPTS and/or RPTB                                *
;*   Calls              : Normal Library ASM calls                            *
;*   Debug Info         : Optimized TI Debug Information                      *
;******************************************************************************
;	c:\c3xtools\bin\ac30.exe -v32 edsutl.c C:\DOCUME~1\MSOLAZ~1.AME\LOCALS~1\Temp\edsutl.if 
	.file	"edsutl.c"
	.file	"bus32.h"
	.stag	.fake1,32
	.member	_holdst,0,14,18,1
	.member	_nohold,1,14,18,1
	.member	_hiz,2,14,18,1
	.member	_sww,3,14,18,2
	.member	_wtcnt,5,14,18,3
	.member	_bnkcmp,8,14,18,5
	.member	_r_131415,13,14,18,3
	.member	_datasize,16,14,18,2
	.member	_memwidth,18,14,18,2
	.member	_signext,20,14,18,1
	.member	_strbcnfg,21,14,18,1
	.member	_strbsw,22,14,18,1
	.member	_r_rest,23,14,18,9
	.eos
	.utag	.fake0,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake1
	.eos
	.sym	_STRB0_BUS_CONTROL,0,9,13,32,.fake0
	.stag	.fake3,32
	.member	_r_012,0,14,18,3
	.member	_sww,3,14,18,2
	.member	_wtcnt,5,14,18,3
	.member	_bnkcmp,8,14,18,5
	.member	_r_131415,13,14,18,3
	.member	_datasize,16,14,18,2
	.member	_memwidth,18,14,18,2
	.member	_signext,20,14,18,1
	.member	_r_rest,21,14,18,11
	.eos
	.utag	.fake2,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake3
	.eos
	.sym	_STRB1_BUS_CONTROL,0,9,13,32,.fake2
	.stag	.fake5,32
	.member	_r_012,0,14,18,3
	.member	_sww,3,14,18,2
	.member	_wtcnt,5,14,18,3
	.member	_r_rest,8,14,18,24
	.eos
	.utag	.fake4,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake5
	.eos
	.sym	_IOSTRB_BUS_CONTROL,0,9,13,32,.fake4
	.stag	.fake6,288
	.member	__ioctrl,0,9,8,32,.fake4
	.member	_reserved1,32,62,8,96,,3
	.member	__s0ctrl,128,9,8,32,.fake0
	.member	_reserved2,160,62,8,96,,3
	.member	__s1ctrl,256,9,8,32,.fake2
	.eos
	.sym	_BUS_REG,0,8,13,288,.fake6
	.file	"timer30.h"
	.stag	.fake8,32
	.member	_func,0,14,18,1
	.member	_i_o,1,14,18,1
	.member	_datout,2,14,18,1
	.member	_datin,3,14,18,1
	.member	_r_45,4,14,18,2
	.member	_go,6,14,18,1
	.member	_hld_,7,14,18,1
	.member	_cp_,8,14,18,1
	.member	_clksrc,9,14,18,1
	.member	_inv,10,14,18,1
	.member	_tstat,11,14,18,1
	.member	_r_rest,12,14,18,20
	.eos
	.utag	.fake7,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake8
	.eos
	.sym	_TIMER_CONTROL,0,9,13,32,.fake7
	.stag	.fake9,288
	.member	__gctrl,0,9,8,32,.fake7
	.member	_reserved1,32,62,8,96,,3
	.member	_counter,128,14,8,32
	.member	_reserved2,160,62,8,96,,3
	.member	_period,256,14,8,32
	.eos
	.sym	_TIMER_REG,0,8,13,288,.fake9
	.file	"serprt30.h"
	.stag	.fake11,32
	.member	_rrdy,0,14,18,1
	.member	_xrdy,1,14,18,1
	.member	_fsxout,2,14,18,1
	.member	_xsrempty,3,14,18,1
	.member	_rsrfull,4,14,18,1
	.member	_hs,5,14,18,1
	.member	_xclksrce,6,14,18,1
	.member	_rclksrce,7,14,18,1
	.member	_xvaren,8,14,18,1
	.member	_rvaren,9,14,18,1
	.member	_xfsm,10,14,18,1
	.member	_rfsm,11,14,18,1
	.member	_clkxp,12,14,18,1
	.member	_clkrp,13,14,18,1
	.member	_dxp,14,14,18,1
	.member	_drp,15,14,18,1
	.member	_fsxp,16,14,18,1
	.member	_fsrp,17,14,18,1
	.member	_xlen,18,14,18,2
	.member	_rlen,20,14,18,2
	.member	_xtint,22,14,18,1
	.member	_xint,23,14,18,1
	.member	_rtint,24,14,18,1
	.member	_rint,25,14,18,1
	.member	_xreset,26,14,18,1
	.member	_rreset,27,14,18,1
	.member	_r_rest,28,14,18,4
	.eos
	.utag	.fake10,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake11
	.eos
	.sym	_SERIAL_PORT_CONTROL,0,9,13,32,.fake10
	.stag	.fake13,32
	.member	_clkfunc,0,14,18,1
	.member	_clki_o,1,14,18,1
	.member	_clkdato,2,14,18,1
	.member	_clkdati,3,14,18,1
	.member	_dfunc,4,14,18,1
	.member	_di_o,5,14,18,1
	.member	_ddatout,6,14,18,1
	.member	_ddatin,7,14,18,1
	.member	_fsfunc,8,14,18,1
	.member	_fsi_o,9,14,18,1
	.member	_fsdatout,10,14,18,1
	.member	_fsdatin,11,14,18,1
	.member	_r_rest,12,14,18,20
	.eos
	.utag	.fake12,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake13
	.eos
	.sym	_RX_PORT_CONTROL,0,9,13,32,.fake12
	.stag	.fake15,32
	.member	_xgo,0,14,18,1
	.member	_xhld_,1,14,18,1
	.member	_xcp_,2,14,18,1
	.member	_xclksrc,3,14,18,1
	.member	_r_4,4,14,18,1
	.member	_xtstat,5,14,18,1
	.member	_rgo,6,14,18,1
	.member	_rhld_,7,14,18,1
	.member	_rcp_,8,14,18,1
	.member	_rclksrc,9,14,18,1
	.member	_r_10,10,14,18,1
	.member	_rtstat,11,14,18,1
	.member	_r_rest,12,14,18,20
	.eos
	.utag	.fake14,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake15
	.eos
	.sym	_RX_TIMER_CONTROL,0,9,13,32,.fake14
	.stag	.fake17,32
	.member	_x_counter,0,14,18,16
	.member	_r_counter,16,14,18,16
	.eos
	.utag	.fake16,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake17
	.eos
	.sym	_RX_TIMER_COUNTER,0,9,13,32,.fake16
	.stag	.fake19,32
	.member	_x_period,0,14,18,16
	.member	_r_period,16,14,18,16
	.eos
	.utag	.fake18,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake19
	.eos
	.sym	_RX_TIMER_PERIOD,0,9,13,32,.fake18
	.stag	.fake20,512
	.member	__gctrl,0,9,8,32,.fake10
	.member	_reserved1,32,14,8,32
	.member	__xctrl,64,9,8,32,.fake12
	.member	__rctrl,96,9,8,32,.fake12
	.member	__rxtctrl,128,9,8,32,.fake14
	.member	__rxtcounter,160,9,8,32,.fake16
	.member	__rxtperiod,192,9,8,32,.fake18
	.member	_reserved2,224,14,8,32
	.member	_x_data,256,14,8,32
	.member	_reserved3,288,62,8,96,,3
	.member	_r_data,384,14,8,32
	.member	_reserved4,416,62,8,96,,3
	.eos
	.sym	_SERIAL_PORT_REG,0,8,13,512,.fake20
	.file	"defines.h"
	.sym	_UCHAR,0,12,13,32
	.sym	_USHORT,0,13,13,32
	.sym	_ULONG,0,15,13,32
	.file	"edsutl.h"
	.stag	.fake21,1088
	.member	_FPGA_Preset_Done,0,13,8,32
	.member	_EDSXFer,32,13,8,32
	.member	_EDS_Stat,64,13,8,32
	.member	_DSO_PENDING,96,13,8,32
	.member	_TMR_ISR_CNT,128,15,8,32
	.member	_EDS_IRQ,160,12,8,32
	.member	_EDS_ISR_CNT,192,15,8,32
	.member	_CLIP_MIN,224,13,8,32
	.member	_CLIP_MAX,256,13,8,32
	.member	_Preset_Mode,288,13,8,32
	.member	_Preset_Done_Cnt,320,13,8,32
	.member	_Preset_Done_Points,352,13,8,32
	.member	_DSO_Index,384,13,8,32
	.member	_FFT_Mode,416,13,8,32
	.member	_DSO_Enable,448,13,8,32
	.member	_PixelCount,480,13,8,32
	.member	_EDS_Mode,512,13,8,32
	.member	_EDS_DEF_ROIS,544,13,8,32
	.member	_Max_Fir_Cnt,576,13,8,32
	.member	_Max_Fir,608,61,8,128,,4
	.member	_RTEM_Init,736,13,8,32
	.member	_RTEM_Target,768,13,8,32
	.member	_ADisc,800,13,8,32
	.member	_ASHBit,832,13,8,32
	.member	_ADiscVal,864,61,8,160,,5
	.member	_EDX_ANAL,1024,13,8,32
	.member	_Reset_FIR,1056,13,8,32
	.eos
	.sym	_EDS_TYPE,0,8,13,1088,.fake21
	.stag	.fake22,160
	.member	_Start,0,13,8,32
	.member	_End,32,13,8,32
	.member	_Sca,64,13,8,32
	.member	_Enable,96,12,8,32
	.member	_Defined,128,12,8,32
	.eos
	.sym	_ROI_TYPE,0,8,13,160,.fake22
	.file	"dpp2utl.h"
	.stag	.fake23,224
	.member	_PCI_IRQ,0,12,8,32
	.member	_IRQ_RESET,32,13,8,32
	.member	_MS100_TC,64,13,8,32
	.member	_OpCodeAck,96,13,8,32
	.member	_PCIIRQ_Cnt,128,15,8,32
	.member	_OpCode,160,13,8,32
	.member	_Set_PCI_IRQ,192,13,8,32
	.eos
	.sym	_DPP_TYPE,0,8,13,224,.fake23
	.file	"edsutl.c"
	.sect	 ".text"

	.global	_EDS_Init
	.sym	_EDS_Init,_EDS_Init,32,2,0
	.func	34
;******************************************************************************
;* FUNCTION NAME: _EDS_Init                                                   *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,r2,r3,ar0,fp,ir0,sp,st,rs,re                  *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 2 Auto + 0 SOE = 4 words          *
;******************************************************************************
_EDS_Init:
	.sym	_i,1,4,1,32
	.sym	_data,2,13,1,32
	.line	1
;----------------------------------------------------------------------
;  34 | void EDS_Init( void )                                                  
;  36 | int i;                                                                 
;  37 | USHORT data;                                                           
;  38 | //****** Initialize Parameters in the DPP Record **********************
;     | ****                                                                   
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      2,sp
	.line	7
;----------------------------------------------------------------------
;  40 | EDSRec.DSO_PENDING = 0;                                                
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_EDSRec+3
	.line	8
;----------------------------------------------------------------------
;  41 | EDSRec.Preset_Mode = 0;                                                
;----------------------------------------------------------------------
        sti       r0,@_EDSRec+9
	.line	9
;----------------------------------------------------------------------
;  42 | EDSRec.RTEM_Init   = 0;                                                
;----------------------------------------------------------------------
        sti       r0,@_EDSRec+23
	.line	10
;----------------------------------------------------------------------
;  43 | EDSRec.DSO_Enable       = 0;                                           
;----------------------------------------------------------------------
        sti       r0,@_EDSRec+14
	.line	12
;----------------------------------------------------------------------
;  45 | EDSRec.FPGA_Preset_Done = 0;                                           
;  47 | //****** Initialize Parameters in the DPP Record **********************
;     | ****                                                                   
;  50 | //---------------------------------------------------------------------
;     | -----------                                                            
;  51 | // Setup for Dither DAC Test - Sets a RAMP on the output               
;  52 | // Set bit 3 to a 1, disable dither                                    
;  55 | //***** EDS Control Word **********************************************
;     | **                                                                     
;  57 | //      WriteEDS( iADR_PA_RESET_THR, 0x80 );                           
;  58 | //      WriteEDS( iADR_PA_RESET_THR, 0x30 );                           
;  59 | //      WriteEDS( iADR_PA_RESET_NTHR, 0x0800 ); // Current             
;  60 | //      WriteEDS( iADR_PA_RESET_PTHR, 0xF000 );                        
;  61 | //      WriteEDS( iADR_PA_RESET_PTHR, 0x0100 ); // Current             
;  62 | //      WriteEDS( iADR_FIR_DIFF_THR, 0x010 );                          
;----------------------------------------------------------------------
        sti       r0,@_EDSRec+0
	.line	31
;----------------------------------------------------------------------
;  64 | WriteEDS( iADR_CW,              0x100 );                               
;----------------------------------------------------------------------
        ldiu      256,r1
        push      r1
        ldiu      258,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	32
;----------------------------------------------------------------------
;  65 | WriteEDS( iADR_SCA,             0x10 );                         // EDI-
;     | III                                                                    
;----------------------------------------------------------------------
        ldiu      16,r0
        ldiu      290,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	33
;----------------------------------------------------------------------
;  66 | WriteEDS( iADR_DSCA_PW, 0x9 );                  // Set to 1 us         
;----------------------------------------------------------------------
        ldiu      9,r0
        push      r0
        ldiu      292,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	35
;----------------------------------------------------------------------
;  68 | WriteEDS( iADR_FIFO_MR, 0x0 );                                         
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        ldiu      298,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	36
;----------------------------------------------------------------------
;  69 | WriteEDS( iADR_RAMA,    0x0 );                                         
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        ldiu      289,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	38
;----------------------------------------------------------------------
;  71 | WriteEDS( iADR_DSO_TRIG, 0x0 );                                        
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        ldiu      373,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	39
;----------------------------------------------------------------------
;  72 | WriteEDS( iADR_DSO_INT, 0x0 );                                         
;----------------------------------------------------------------------
        ldiu      374,r1
        ldiu      0,r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	40
;----------------------------------------------------------------------
;  73 | WriteEDS( iADR_TIME_CLR, 0x0 );                                 // Clea
;     | r Clock Time and Live Time                                             
;----------------------------------------------------------------------
        ldiu      277,r1
        ldiu      0,r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	42
;----------------------------------------------------------------------
;  75 | WriteEDS( iADR_TP_SEL,          0x91A );                               
;----------------------------------------------------------------------
        ldiu      2330,r0
        ldiu      264,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	43
;----------------------------------------------------------------------
;  76 | WriteEDS( iADR_INT_EN,          0x0 );                                 
;----------------------------------------------------------------------
        ldiu      0,r0
        ldiu      265,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	44
;----------------------------------------------------------------------
;  77 | WriteEDS( iADR_CGAIN,           13275 );                               
;----------------------------------------------------------------------
        ldiu      266,r1
        ldiu      13275,r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	45
;----------------------------------------------------------------------
;  78 | WriteEDS( iADR_PS,                      0x0 );                         
;----------------------------------------------------------------------
        ldiu      0,r0
        ldiu      308,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	48
;----------------------------------------------------------------------
;  81 | WriteDPRAM( dADR_AUTO_DISC, 0x0 );                                     
;----------------------------------------------------------------------
        ldiu      0,r0
        ldiu      29,r1
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	49
;----------------------------------------------------------------------
;  82 | EDSRec.ADisc = 0;                                                      
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_EDSRec+25
	.line	51
;----------------------------------------------------------------------
;  84 | EDSRec.Preset_Done_Points = 0x40;                                      
;----------------------------------------------------------------------
        ldiu      64,r0
        sti       r0,@_EDSRec+11
	.line	52
;----------------------------------------------------------------------
;  85 | WriteDPRAM( dADR_PRESET_POINTS, 0x40 );                                
;  88 | //---------------------------------------------------------------------
;     | ---------------                                                        
;  89 | // All FPGA Memory Accesses here become initialized                    
;----------------------------------------------------------------------
        ldiu      37,r1
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	57
;----------------------------------------------------------------------
;  90 | data = ReadEDS( iADR_CW );                                      // Read
;     |  the Control Word                                                      
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(2)
	.line	58
;----------------------------------------------------------------------
;  91 | data &= ~BIT_ENABLE;                                            // Clea
;     | r BIT_ENABLE Flag                                                      
;----------------------------------------------------------------------
        andn      1,r0
        sti       r0,*+fp(2)
	.line	59
;----------------------------------------------------------------------
;  92 | data |= RAM_ACCESS;
;     |  // Set the Memory Access Bit                                          
;----------------------------------------------------------------------
        ldiu      16,r0
        or        *+fp(2),r0
        sti       r0,*+fp(2)
	.line	60
;----------------------------------------------------------------------
;  93 | data |= NOISE_SPEC_EN;                                                 
;----------------------------------------------------------------------
        ldiu      4,r0
        or        *+fp(2),r0
        sti       r0,*+fp(2)
	.line	61
;----------------------------------------------------------------------
;  94 | WriteEDS( iADR_CW, data );                                      // Rewr
;     | ite the Control Word                                                   
;  97 | // SCA/Fast Map Look-Up                                                
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      258,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	65
;----------------------------------------------------------------------
;  98 | for( i= 0;i<= 0x7FF;i++){                                              
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
        cmpi      2047,r0
        bgtd      L8
	nop
	nop
        ldigt     258,r0
;*      Branch Occurs to L8 
	.line	66
;----------------------------------------------------------------------
;  99 | WriteEDS( iADR_RAMA, (USHORT) i);                                      
;----------------------------------------------------------------------
        ldiu      *+fp(1),r1
        ldiu      289,r0
L4:        
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	67
;----------------------------------------------------------------------
; 100 | if( i <= 0xFF ){                                                       
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
        cmpi      255,r0
        bgtd      L6
	nop
        ldigt     0,r1
        ldigt     291,r0
;*      Branch Occurs to L6 
	.line	68
;----------------------------------------------------------------------
; 101 | WriteEDS( iADR_ROI_LU, 0xFFFF );                // ROI Defined Look-Up 
;----------------------------------------------------------------------
        ldiu      @CL1,r0
        ldiu      293,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	70
;----------------------------------------------------------------------
; 103 | WriteEDS( iADR_SCA_LU,  0x0 );                  // Clear Digital SCA   
;----------------------------------------------------------------------
        ldiu      0,r1
        ldiu      291,r0
L6:        
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	65
        ldiu      1,r0
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      2047,r0
        bled      L4
	nop
        ldile     *+fp(1),r1
        ldile     289,r0
;*      Branch Occurs to L4 
	.line	73
;----------------------------------------------------------------------
; 106 | data = ReadEDS( iADR_CW );                                      // Read
;     |  the Control Word                                                      
;----------------------------------------------------------------------
        ldiu      258,r0
L8:        
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(2)
	.line	74
;----------------------------------------------------------------------
; 107 | data &= ~RAM_ACCESS;                                            // Set
;     | the Memory Access Bit                                                  
;----------------------------------------------------------------------
        andn      16,r0
        sti       r0,*+fp(2)
	.line	75
;----------------------------------------------------------------------
; 108 | WriteEDS( iADR_CW, data );                                      // Rewr
;     | ite the Control Word                                                   
; 109 | // All FPGA Memory Accesses here become initialized                    
; 110 | //---------------------------------------------------------------------
;     | ---------------                                                        
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      258,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	79
;----------------------------------------------------------------------
; 112 | WriteEDS( iADR_CLIP_MIN,                        0x0 );                 
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        ldiu      269,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	80
;----------------------------------------------------------------------
; 113 | WriteEDS( iADR_CLIP_MAX,                        0xFFF );               
;----------------------------------------------------------------------
        ldiu      4095,r0
        push      r0
        ldiu      270,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	82
;----------------------------------------------------------------------
; 115 | WriteEDS( iADR_TIME_STOP,                       0x1 );                 
;----------------------------------------------------------------------
        ldiu      276,r1
        ldiu      1,r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	83
;----------------------------------------------------------------------
; 116 | WriteEDS( iADR_TIME_CLR,                        0x1 );                 
;----------------------------------------------------------------------
        ldiu      1,r0
        push      r0
        ldiu      277,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	84
;----------------------------------------------------------------------
; 117 | WriteEDS( iADR_PRESET_MODE,                     0x0 );                 
;----------------------------------------------------------------------
        ldiu      0,r0
        ldiu      278,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	85
;----------------------------------------------------------------------
; 118 | data = 10000;                                           // 10000 X 1ms=
;     |  10 seconds                                                            
;----------------------------------------------------------------------
        ldiu      10000,r0
        sti       r0,*+fp(2)
	.line	86
;----------------------------------------------------------------------
; 119 | WriteDPRAM( iADR_PRESET_LO, (USHORT)data);                             
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      279,r0
        push      r1
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	87
;----------------------------------------------------------------------
; 120 | WriteDPRAM( iADR_PRESET_HI, (USHORT)(data>>16));                       
;----------------------------------------------------------------------
        ldiu      *+fp(2),r0
        ldiu      280,r1
        lsh       -16,r0
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	89
;----------------------------------------------------------------------
; 122 | WriteEDS( iADR_RTEM_INI,                                0x1312 );      
;----------------------------------------------------------------------
        ldiu      4882,r1
        push      r1
        ldiu      282,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	90
;----------------------------------------------------------------------
; 123 | WriteEDS( iADR_RTEM_CMD_OUT,                    0x0 );                 
;----------------------------------------------------------------------
        ldiu      0,r1
        push      r1
        ldiu      284,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	91
;----------------------------------------------------------------------
; 124 | WriteEDS( iADR_RTEM_HCMR,                       0x0 );                 
;----------------------------------------------------------------------
        ldiu      0,r1
        ldiu      285,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	92
;----------------------------------------------------------------------
; 125 | WriteEDS( iADR_RTEM_HC_RATE_LO,         9999 );                        
;----------------------------------------------------------------------
        ldiu      9999,r1
        ldiu      286,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	93
;----------------------------------------------------------------------
; 126 | WriteEDS( iADR_RTEM_HC_RATE_HI,         0x0 );          //             
;----------------------------------------------------------------------
        ldiu      0,r1
        push      r1
        ldiu      287,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	94
;----------------------------------------------------------------------
; 127 | WriteEDS( iADR_RTEM_HC_THRESHOLD,               0xA );                 
;----------------------------------------------------------------------
        ldiu      10,r1
        ldiu      288,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	95
;----------------------------------------------------------------------
; 128 | WriteEDS( iADR_SCA,                                     0x0 );         
;----------------------------------------------------------------------
        ldiu      0,r1
        push      r1
        ldiu      290,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	96
;----------------------------------------------------------------------
; 129 | WriteEDS( iADR_DSCA_PW,                         0x13 );
;     |  // 1 us                                                               
;----------------------------------------------------------------------
        ldiu      19,r0
        push      r0
        ldiu      292,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	97
;----------------------------------------------------------------------
; 130 | WriteEDS( iADR_FIFO_MR,                         0x0 );                 
;----------------------------------------------------------------------
        ldiu      298,r1
        ldiu      0,r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	98
;----------------------------------------------------------------------
; 131 | WriteEDS( iADR_DISC_EN,                         0x3FC );               
;----------------------------------------------------------------------
        ldiu      1020,r0
        ldiu      299,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	99
;----------------------------------------------------------------------
; 132 | WriteEDS( iADR_TC_SEL,                          0x6 );                 
;----------------------------------------------------------------------
        ldiu      6,r0
        push      r0
        ldiu      300,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	100
;----------------------------------------------------------------------
; 133 | WriteEDS( iADR_FIR_DLY_LEN,                     0x3 );
;     |  // Reduce for Throughput, Increase for Reso                           
;----------------------------------------------------------------------
        ldiu      377,r1
        ldiu      3,r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	101
;----------------------------------------------------------------------
; 134 | WriteEDS( iADR_FIR_DLY_INC,                     0x4 );                 
;----------------------------------------------------------------------
        ldiu      378,r1
        ldiu      4,r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	103
;----------------------------------------------------------------------
; 136 | WriteEDS( iADR_INT_EN, 0xFFFF );                                // Enab
;     | le the EDS Interrupt                                                   
;----------------------------------------------------------------------
        ldiu      @CL1,r0
        ldiu      265,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	105
;----------------------------------------------------------------------
; 138 | WriteEDS( iADR_DECAY, 0x0100 );                                        
;----------------------------------------------------------------------
        ldiu      256,r0
        ldiu      352,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	107
;----------------------------------------------------------------------
; 140 | WriteEDS( iADR_BLANK_MODE, 0x1 );                                      
;----------------------------------------------------------------------
        ldiu      1,r0
        push      r0
        ldiu      305,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	108
;----------------------------------------------------------------------
; 141 | WriteEDS( iADR_BLANK_DELAY, 0x0 );                                     
;----------------------------------------------------------------------
        ldiu      0,r0
        ldiu      306,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	109
;----------------------------------------------------------------------
; 142 | WriteEDS( iADR_BLANK_WIDTH, 0x10 );                                    
;----------------------------------------------------------------------
        ldiu      16,r0
        push      r0
        ldiu      307,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	112
;----------------------------------------------------------------------
; 145 | Write8420( iADR_EVCH0,  0xC00 );                                       
;----------------------------------------------------------------------
        ldiu      3072,r0
        ldiu      273,r1
        push      r0
        push      r1
        call      _Write8420
                                        ; Call Occurs
        subi      2,sp
	.line	113
;----------------------------------------------------------------------
; 146 | Write8420( iADR_BIT,   0x800 );                                        
;----------------------------------------------------------------------
        ldiu      2048,r0
        ldiu      274,r1
        push      r0
        push      r1
        call      _Write8420
                                        ; Call Occurs
        subi      2,sp
	.line	114
;----------------------------------------------------------------------
; 147 | Write8420( iADR_DISC,  1190 );                                         
;----------------------------------------------------------------------
        ldiu      1190,r0
        push      r0
        ldiu      271,r1
        push      r1
        call      _Write8420
                                        ; Call Occurs
        subi      2,sp
	.line	115
;----------------------------------------------------------------------
; 148 | Write8420( iADR_EVCH1,  0xFFF );                                       
;----------------------------------------------------------------------
        ldiu      4095,r0
        push      r0
        ldiu      272,r1
        push      r1
        call      _Write8420
                                        ; Call Occurs
        subi      2,sp
	.line	116
;----------------------------------------------------------------------
; 149 | WriteEDS( iADR_DLEVEL0,         52 );                                  
;----------------------------------------------------------------------
        ldiu      52,r0
        push      r0
        ldiu      301,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	117
;----------------------------------------------------------------------
; 150 | WriteEDS( iADR_DLEVEL1,         50 );                                  
;----------------------------------------------------------------------
        ldiu      50,r0
        ldiu      302,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	118
;----------------------------------------------------------------------
; 151 | WriteEDS( iADR_DLEVEL2,         48 );                                  
;----------------------------------------------------------------------
        ldiu      48,r0
        ldiu      303,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	119
;----------------------------------------------------------------------
; 152 | WriteEDS( iADR_DLEVEL3,         42 );                                  
; 154 | // IMS Motor Interface Parameters for the RTEM Slide                   
; 155 | //      MSEL = 25                                                      
; 156 | //      MRC  = 100%                                                    
; 157 | //      MRH  = 10%                                                     
;----------------------------------------------------------------------
        ldiu      42,r0
        ldiu      304,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	126
;----------------------------------------------------------------------
; 159 | WriteEDS( iADR_RTEM_LOW_SPEED,  UMS_Low_Speed );                       
;----------------------------------------------------------------------
        ldiu      127,r0
        push      r0
        ldiu      342,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	127
;----------------------------------------------------------------------
; 160 | WriteEDS( iADR_RTEM_MED_SPEED,  UMS_Med_Speed );                       
;----------------------------------------------------------------------
        ldiu      63,r0
        ldiu      343,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	128
;----------------------------------------------------------------------
; 161 | WriteEDS( iADR_RTEM_HIGH_SPEED,         UMS_High_Speed );              
;----------------------------------------------------------------------
        ldiu      23,r0
        push      r0
        ldiu      344,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	129
;----------------------------------------------------------------------
; 162 | Set_RTEM_Range( RTEM_MAX_RANGE );                                      
;----------------------------------------------------------------------
        ldiu      @CL2,r0
        push      r0
        call      _Set_RTEM_Range
                                        ; Call Occurs
        subi      1,sp
	.line	131
;----------------------------------------------------------------------
; 164 | WriteEDS( iADR_RMS_THR, 0x20 );                                        
;----------------------------------------------------------------------
        ldiu      32,r1
        ldiu      337,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	133
;----------------------------------------------------------------------
; 166 | WriteEDS( iADR_BIT_PEAK1,       0x80);                                 
;----------------------------------------------------------------------
        ldiu      128,r1
        push      r1
        ldiu      309,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	134
;----------------------------------------------------------------------
; 167 | WriteEDS( iADR_BIT_PEAK2,       0x0 );                                 
;----------------------------------------------------------------------
        ldiu      0,r1
        push      r1
        ldiu      310,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	135
;----------------------------------------------------------------------
; 168 | WriteEDS( iADR_BIT_CPS,         999 );                                 
;----------------------------------------------------------------------
        ldiu      999,r1
        ldiu      311,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	136
;----------------------------------------------------------------------
; 169 | WriteEDS( iADR_BIT_PK2D,                0x0 );                         
;----------------------------------------------------------------------
        ldiu      312,r0
        ldiu      0,r1
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	138
;----------------------------------------------------------------------
; 171 | for(i=0;i<=MAX_ROI;i++){                                               
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
        ldiu      r0,r1
        ldiu      0,rs
        cmpi      49,r1
        bgtd      L15
        ldiu      0,r3
        ldiu      0,r2
        ldiu      0,re
;*      Branch Occurs to L15 
	.line	139
;----------------------------------------------------------------------
; 172 | ROIRec[i].Start = 0;                                                   
;----------------------------------------------------------------------
        ldiu      r0,ir0
        ldiu      @CL3,ar0
L14:        
        mpyi      5,ir0
        sti       r0,*+ar0(ir0)
	.line	140
;----------------------------------------------------------------------
; 173 | ROIRec[i].End = 0;                                                     
;----------------------------------------------------------------------
        ldiu      *+fp(1),ir0
        ldiu      @CL4,ar0
        mpyi      5,ir0
        sti       rs,*+ar0(ir0)
	.line	141
;----------------------------------------------------------------------
; 174 | ROIRec[i].Enable = 0;                                                  
;----------------------------------------------------------------------
        ldiu      @CL5,ar0
        ldiu      *+fp(1),ir0
        mpyi      5,ir0
        sti       r3,*+ar0(ir0)
	.line	142
;----------------------------------------------------------------------
; 175 | ROIRec[i].Defined = 0;                                                 
;----------------------------------------------------------------------
        ldiu      @CL6,ar0
        ldiu      *+fp(1),ir0
        mpyi      5,ir0
        sti       r2,*+ar0(ir0)
	.line	143
;----------------------------------------------------------------------
; 176 | ROIRec[i].Sca = 0;                                                     
;----------------------------------------------------------------------
        ldiu      @CL7,ar0
        ldiu      *+fp(1),ir0
        mpyi      5,ir0
        sti       re,*+ar0(ir0)
	.line	138
        ldiu      1,r1
        addi      *+fp(1),r1
        sti       r1,*+fp(1)
        cmpi      49,r1
        bled      L14
        ldile     *+fp(1),ir0
        ldile     @CL3,ar0
        ldile     *+fp(1),ir0
;*      Branch Occurs to L14 
L15:        
	.line	145
;----------------------------------------------------------------------
; 178 | EDSRec.EDS_DEF_ROIS = 0;                                               
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_EDSRec+17
	.line	147
;----------------------------------------------------------------------
; 180 | WriteDPRAM( dADR_TIMECONST, 0x10 );                                    
;----------------------------------------------------------------------
        ldiu      16,r1
        push      r1
        ldiu      9,r0
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	148
;----------------------------------------------------------------------
; 181 | WriteDPRAM( dADR_BLANK_FACTOR, 10 );                                   
;----------------------------------------------------------------------
        ldiu      10,r1
        ldiu      32,r0
        push      r1
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	149
;----------------------------------------------------------------------
; 182 | Set_Time_Const();                                                      
;----------------------------------------------------------------------
        call      _Set_Time_Const
                                        ; Call Occurs
	.line	150
;----------------------------------------------------------------------
; 183 | Set_Blank_Factor();                                                    
;----------------------------------------------------------------------
        call      _Set_Blank_Factor
                                        ; Call Occurs
	.line	152
;----------------------------------------------------------------------
; 185 | WriteDPRAM( dADR_EVCH, 2 );                                     // Defa
;     | ult for 10eV/Ch                                                        
;----------------------------------------------------------------------
        ldiu      2,r1
        ldiu      31,r0
        push      r1
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	153
;----------------------------------------------------------------------
; 186 | Set_EvCh();                                                            
;----------------------------------------------------------------------
        call      _Set_EvCh
                                        ; Call Occurs
	.line	155
;----------------------------------------------------------------------
; 188 | SetROISCARM( 0 );
;     |                  // Rebuild ROI/SCA/Ratemeter Table                    
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        call      _SetROISCARM
                                        ; Call Occurs
        subi      1,sp
	.line	157
;----------------------------------------------------------------------
; 190 | EDSRec.Max_Fir_Cnt = 0;                                                
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_EDSRec+18
	.line	158
;----------------------------------------------------------------------
; 191 | for(i=0;i<=3;i++){                                                     
;----------------------------------------------------------------------
        sti       r0,*+fp(1)
        ldiu      0,r1
        cmpi      3,r0
        bgtd      L19
	nop
        ldigt     10,r0
        ldigt     30,r1
;*      Branch Occurs to L19 
	.line	159
;----------------------------------------------------------------------
; 192 | EDSRec.Max_Fir[i] = 0;                                                 
;----------------------------------------------------------------------
        ldiu      *+fp(1),ir0
        ldiu      @CL8,ar0
L17:        
        sti       r1,*+ar0(ir0)
	.line	158
        ldiu      1,r0
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      3,r0
        bled      L17
        ldile     *+fp(1),ir0
        ldile     @CL8,ar0
        ldile     *+fp(1),ir0
;*      Branch Occurs to L17 
	.line	161
;----------------------------------------------------------------------
; 194 | WriteDPRAM( dADR_MAX_CPS, 10 );                                        
;----------------------------------------------------------------------
        ldiu      10,r0
        ldiu      30,r1
L19:        
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	162
;----------------------------------------------------------------------
; 195 | data = 10 * 1000;                                                      
;----------------------------------------------------------------------
        ldiu      10000,r0
        sti       r0,*+fp(2)
	.line	163
;----------------------------------------------------------------------
; 196 | WriteDPRAM( iADR_PRESET_LO, data );                                    
;----------------------------------------------------------------------
        ldiu      279,r1
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	164
;----------------------------------------------------------------------
; 197 | WriteDPRAM( iADR_PRESET_HI, (USHORT)(data >> 16 ));                    
;----------------------------------------------------------------------
        ldiu      *+fp(2),r0
        lsh       -16,r0
        ldiu      280,r1
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	166
;----------------------------------------------------------------------
; 199 | WriteDPRAM( dADR_ANAL_MODE, EDS_ANAL_PRESET_NONE );                    
;----------------------------------------------------------------------
        ldiu      3,r1
        push      r1
        ldiu      17,r0
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	167
;----------------------------------------------------------------------
; 200 | WriteDPRAM( dADR_TRANSFER, 0x1 );                                      
;----------------------------------------------------------------------
        ldiu      1,r1
        ldiu      18,r0
        push      r1
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	168
;----------------------------------------------------------------------
; 201 | SetAnalMode();                                                         
;----------------------------------------------------------------------
        call      _SetAnalMode
                                        ; Call Occurs
	.line	170
;----------------------------------------------------------------------
; 203 | WriteEDS( iADR_INT_EN, 0xFFFF );                                // Enab
;     | le the EDS Interrupt                                                   
;----------------------------------------------------------------------
        ldiu      @CL1,r0
        push      r0
        ldiu      265,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	171
;----------------------------------------------------------------------
; 204 | WriteEDS( iADR_FIFO_MR, 0x0 );                                         
; 206 | //***** Initial Settings while waiting for "Shell Application"         
; 207 | //                                         DSO  | Analog Test Point | D
;     | igital Test point                                                      
;----------------------------------------------------------------------
        ldiu      0,r1
        ldiu      298,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	175
;----------------------------------------------------------------------
; 208 | WriteEDS( iADR_TP_SEL, ( 18 ) | ( ATP_AFILTER ) | ( 0xD << 12 ));      
;----------------------------------------------------------------------
        ldiu      @CL9,r1
        push      r1
        ldiu      264,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	177
;----------------------------------------------------------------------
; 210 | data = ReadEDS( iADR_CW );                                             
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(2)
	.line	178
;----------------------------------------------------------------------
; 211 | data &= ~( 3 << 11 );                                           // Clea
;     | r PreAmp Source Bits                                                   
;----------------------------------------------------------------------
        andn      6144,r0
        sti       r0,*+fp(2)
	.line	179
;----------------------------------------------------------------------
; 212 | data &= ~( 1 << 13 );                                           // Set
;     | Input Gain to 1X                                                       
; 214 | //      data |=  ( 3 << 11 );
;     |  // Set BIT_DAC - Digital Source                                       
; 215 | //      data |=  ( 2 << 11 );
;     |  // Set BIT_DAC - Analog Source                                        
; 216 | //      data |=  ( 1 << 14 );
;     |  // Set AD_DEBUG mode - Route AD_DATA_MUX to XPORT connector           
;----------------------------------------------------------------------
        andn      8192,r0
        sti       r0,*+fp(2)
	.line	184
;----------------------------------------------------------------------
; 217 | data |=  ( 1 << 13 );                                           // Set
;     | 2x Input Gain                                                          
;----------------------------------------------------------------------
        ldiu      8192,r0
        or        *+fp(2),r0
        sti       r0,*+fp(2)
	.line	185
;----------------------------------------------------------------------
; 218 | data |=  ( 1 << 15 );                                           // Set
;     | FDISC, triggered DSO Mode                                              
;----------------------------------------------------------------------
        or        32768,r0
        sti       r0,*+fp(2)
	.line	186
;----------------------------------------------------------------------
; 219 | WriteEDS( iADR_CW, data );                                             
;----------------------------------------------------------------------
        ldiu      r0,r1
        push      r1
        ldiu      258,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	188
;----------------------------------------------------------------------
; 221 | data = ReadEDS( iADR_CW );                                             
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(2)
	.line	189
;----------------------------------------------------------------------
; 222 | if(( data & 0x8000 ) == 0 ){                                           
; 223 | //              WriteEDS( iADR_SHDISC_DLY_SEL,  0x25 );         // SiLi
;     |  or Pulsed-Reset Detectors                                             
; 224 | //              WriteEDS( iADR_SHDISC_DLY_SEL,  0x24 );         // SiLi
;     |  or Pulsed-Reset Detectors                                             
; 225 | //              WriteEDS( iADR_SHDISC_DLY_SEL,  0x23 );         // SiLi
;     |  or Pulsed-Reset Detectors                                             
;----------------------------------------------------------------------
        tstb      32768,r0
        bned      L23
	nop
        ldine     73,r0
        ldine     375,r1
;*      Branch Occurs to L23 
	.line	193
;----------------------------------------------------------------------
; 226 | WriteEDS( iADR_SHDISC_DLY_SEL, 0x43 );                  // SiLi or Puls
;     | ed-Reset Detectors                                                     
; 227 | } else {                                                               
; 228 | //              WriteEDS( iADR_SHDISC_DLY_SEL,  0x31 );         // SDD
;     | or WDS or Tail-Pulse Detectors                                         
; 229 | //              if( TimeConst == 0 ){                                  
;----------------------------------------------------------------------
        ldiu      67,r1
        ldiu      375,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        bud       L25
        subi      2,sp
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L25 
	.line	197
;----------------------------------------------------------------------
; 230 | WriteEDS( iADR_SHDISC_DLY_SEL,  0x49 );         // SDD or WDS or Tail-P
;     | ulse Detectors                                                         
; 231 | //              } else {                                               
; 232 | //                      WriteEDS( iADR_SHDISC_DLY_SEL,  0x50 );
;     |  // SDD or WDS or Tail-Pulse Detectors                                 
; 233 | //              }                                                      
; 235 | //***** Initial Settings while waiting for "Shell Application"         
;----------------------------------------------------------------------
L23:        
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	203
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
L25:        
        subi      4,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	236,000000000h,2


	.sect	 ".text"

	.global	_WriteEDS
	.sym	_WriteEDS,_WriteEDS,32,2,0
	.func	242
;******************************************************************************
;* FUNCTION NAME: _WriteEDS                                                   *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,ar0,ir0                                          *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 2 Parm + 0 Auto + 0 SOE = 4 words          *
;******************************************************************************
_WriteEDS:
	.sym	_addr,-2,13,9,32
	.sym	_data,-3,13,9,32
	.line	1
;----------------------------------------------------------------------
; 242 | void WriteEDS( USHORT addr, USHORT data )                              
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
	.line	2
	.line	3
;----------------------------------------------------------------------
; 244 | *(ULONG *)( EDS_BASE + addr ) = data;                           // Writ
;     | e the data directly                                                    
;----------------------------------------------------------------------
        ldiu      @CL10,ar0
        ldiu      *-fp(2),ir0
        ldiu      *-fp(3),r0
        sti       r0,*+ar0(ir0)
	.line	4
        ldiu      *-fp(1),ir0
        ldiu      *fp,fp
        subi      2,sp
        bu        ir0
;*      Branch Occurs to ir0 
	.endfunc	245,000000000h,0


	.sect	 ".text"

	.global	_ReadEDS
	.sym	_ReadEDS,_ReadEDS,45,2,0
	.func	251
;******************************************************************************
;* FUNCTION NAME: _ReadEDS                                                    *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,ar0,ir0                                          *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 1 Auto + 0 SOE = 4 words          *
;******************************************************************************
_ReadEDS:
	.sym	_addr,-2,13,9,32
	.sym	_data,1,15,1,32
	.line	1
;----------------------------------------------------------------------
; 251 | USHORT ReadEDS( USHORT addr )                                          
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      1,sp
	.line	2
;----------------------------------------------------------------------
; 253 | ULONG data;                                                            
;----------------------------------------------------------------------
	.line	4
;----------------------------------------------------------------------
; 254 | data = *(ULONG *)( EDS_BASE + addr );                           // Writ
;     | e the data directly                                                    
;----------------------------------------------------------------------
        ldiu      @CL10,ar0
        ldiu      *-fp(2),ir0
        ldiu      *+ar0(ir0),r0
        sti       r0,*+fp(1)
	.line	5
;----------------------------------------------------------------------
; 255 | data &= 0xFFFF;                                                        
;----------------------------------------------------------------------
        and       65535,r0
        sti       r0,*+fp(1)
	.line	6
;----------------------------------------------------------------------
; 256 | return( (USHORT)data );                                                
;----------------------------------------------------------------------
	.line	7
        ldiu      *-fp(1),ir0
        ldiu      *fp,fp
        subi      3,sp
        bu        ir0
;*      Branch Occurs to ir0 
	.endfunc	257,000000000h,1


	.sect	 ".text"

	.global	_WriteDEDS
	.sym	_WriteDEDS,_WriteDEDS,32,2,0
	.func	263
;******************************************************************************
;* FUNCTION NAME: _WriteDEDS                                                  *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,ar0,ir0                                          *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 2 Parm + 0 Auto + 0 SOE = 4 words          *
;******************************************************************************
_WriteDEDS:
	.sym	_addr,-2,13,9,32
	.sym	_data,-3,15,9,32
	.line	1
;----------------------------------------------------------------------
; 263 | void WriteDEDS( USHORT addr, ULONG data )                              
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
	.line	2
	.line	3
;----------------------------------------------------------------------
; 265 | *(ULONG *)( EDS_BASE + addr ) = data;                           // Writ
;     | e the data directly                                                    
;----------------------------------------------------------------------
        ldiu      @CL10,ar0
        ldiu      *-fp(2),ir0
        ldiu      *-fp(3),r0
        sti       r0,*+ar0(ir0)
	.line	4
;----------------------------------------------------------------------
; 266 | *(ULONG *)( EDS_BASE + addr + 1 ) = ( data >> 16 );                    
;----------------------------------------------------------------------
        ldiu      @CL11,ar0
        ldiu      *-fp(3),r0
        ldiu      *-fp(2),ir0
        lsh       -16,r0
        sti       r0,*+ar0(ir0)
	.line	5
        ldiu      *-fp(1),ir0
        ldiu      *fp,fp
        subi      2,sp
        bu        ir0
;*      Branch Occurs to ir0 
	.endfunc	267,000000000h,0


	.sect	 ".text"

	.global	_ReadDEDS
	.sym	_ReadDEDS,_ReadDEDS,47,2,0
	.func	272
;******************************************************************************
;* FUNCTION NAME: _ReadDEDS                                                   *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,ar0,ir0                                          *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 1 Auto + 0 SOE = 4 words          *
;******************************************************************************
_ReadDEDS:
	.sym	_addr,-2,13,9,32
	.sym	_data,1,15,1,32
	.line	1
;----------------------------------------------------------------------
; 272 | ULONG ReadDEDS( USHORT addr )                                          
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      1,sp
	.line	2
;----------------------------------------------------------------------
; 274 | ULONG data;                                                            
;----------------------------------------------------------------------
	.line	4
;----------------------------------------------------------------------
; 275 | data = ( *(ULONG *)( EDS_BASE + addr ) ) & 0xFFFF;                     
;----------------------------------------------------------------------
        ldiu      @CL10,ar0
        ldiu      *-fp(2),ir0
        ldiu      *+ar0(ir0),r0
        and       65535,r0
        sti       r0,*+fp(1)
	.line	5
;----------------------------------------------------------------------
; 276 | data = data | ((( *(ULONG *)( EDS_BASE + addr ) ) & 0xFFFF ) << 16 );  
;----------------------------------------------------------------------
        ldiu      *+ar0(ir0),r0
        and       65535,r0
        ash       16,r0
        or        *+fp(1),r0
        sti       r0,*+fp(1)
	.line	6
;----------------------------------------------------------------------
; 277 | return( data );                                                        
;----------------------------------------------------------------------
	.line	7
        ldiu      *-fp(1),ir0
        ldiu      *fp,fp
        subi      3,sp
        bu        ir0
;*      Branch Occurs to ir0 
	.endfunc	278,000000000h,1


	.sect	 ".text"

	.global	_WriteEDSFIFO
	.sym	_WriteEDSFIFO,_WriteEDSFIFO,32,2,0
	.func	283
;******************************************************************************
;* FUNCTION NAME: _WriteEDSFIFO                                               *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,fp,sp                                         *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 1 Auto + 0 SOE = 4 words          *
;******************************************************************************
_WriteEDSFIFO:
	.sym	_data,-2,15,9,32
	.sym	_sdata,1,15,1,32
	.line	1
;----------------------------------------------------------------------
; 283 | void WriteEDSFIFO( ULONG data )                                        
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      1,sp
	.line	2
;----------------------------------------------------------------------
; 285 | ULONG sdata;                                                           
;----------------------------------------------------------------------
	.line	4
;----------------------------------------------------------------------
; 286 | sdata = data & 0xFFFF;                                                 
;----------------------------------------------------------------------
        ldiu      *-fp(2),r0
        and       65535,r0
        sti       r0,*+fp(1)
	.line	5
;----------------------------------------------------------------------
; 287 | WriteEDS( iADR_FIFO_LO, (USHORT)sdata );                // Load Low FIF
;     | O Address                                                              
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      296,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	6
;----------------------------------------------------------------------
; 288 | sdata = ( data >> 16 ) & 0xFFFF;                                       
;----------------------------------------------------------------------
        ldiu      *-fp(2),r0
        lsh       -16,r0
        and       65535,r0
        sti       r0,*+fp(1)
	.line	7
;----------------------------------------------------------------------
; 289 | WriteEDS( iADR_FIFO_HI, (USHORT)sdata );                // Load Hi FIFO
;     |  Address                                                               
;----------------------------------------------------------------------
        ldiu      r0,r1
        push      r1
        ldiu      297,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	8
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      3,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	290,000000000h,1


	.sect	 ".text"

	.global	_ReadEDSFIFO
	.sym	_ReadEDSFIFO,_ReadEDSFIFO,47,2,0
	.func	295
;******************************************************************************
;* FUNCTION NAME: _ReadEDSFIFO                                                *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,ar0,st                                           *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 1 Auto + 0 SOE = 3 words          *
;******************************************************************************
_ReadEDSFIFO:
	.sym	_rtmp,1,15,1,32
	.line	1
;----------------------------------------------------------------------
; 295 | ULONG ReadEDSFIFO( void )                                              
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      1,sp
	.line	3
;----------------------------------------------------------------------
; 297 | ULONG rtmp = 0;                                                        
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
	.line	5
;----------------------------------------------------------------------
; 299 | rtmp = *(ULONG *) EDS_FIFO_STAT;                // Read the FIFO Status
;----------------------------------------------------------------------
        ldiu      @CL12,ar0
        ldiu      *ar0,r0
        sti       r0,*+fp(1)
	.line	6
;----------------------------------------------------------------------
; 300 | rtmp &= 0x1;                                                           
;----------------------------------------------------------------------
        ldiu      1,r0
        and       *+fp(1),r0
        sti       r0,*+fp(1)
	.line	7
;----------------------------------------------------------------------
; 301 | if( rtmp == 0 ){                                                // FIFO
;     |  Not Empty                                                             
;----------------------------------------------------------------------
        bned      L45
	nop
	nop
        ldine     *+fp(1),r0
;*      Branch Occurs to L45 
	.line	8
;----------------------------------------------------------------------
; 302 | rtmp = *(ULONG *) EDS_FIFO_BASE;        // Read the EDS FIF0           
;----------------------------------------------------------------------
        ldiu      @CL13,ar0
        ldiu      *ar0,r0
        sti       r0,*+fp(1)
	.line	9
;----------------------------------------------------------------------
; 303 | rtmp &= 0x3FFFF;                                                       
;----------------------------------------------------------------------
        ldiu      @CL14,r0
        and       *+fp(1),r0
        sti       r0,*+fp(1)
	.line	11
;----------------------------------------------------------------------
; 305 | return( rtmp );                                                        
;----------------------------------------------------------------------
	.line	12
L45:        
        ldiu      *-fp(1),ar0
        ldiu      *fp,fp
        subi      3,sp
        bu        ar0
;*      Branch Occurs to ar0 
	.endfunc	306,000000000h,1


	.sect	 ".text"

	.global	_Write8420
	.sym	_Write8420,_Write8420,32,2,0
	.func	313
;******************************************************************************
;* FUNCTION NAME: _Write8420                                                  *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,ar0,fp,ir0,sp,st                              *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 2 Parm + 3 Auto + 0 SOE = 7 words          *
;******************************************************************************
_Write8420:
	.sym	_addr,-2,13,9,32
	.sym	_data,-3,13,9,32
	.sym	_rtmp,1,15,1,32
	.sym	_CWord,2,13,1,32
	.sym	_i,3,4,1,32
	.line	1
;----------------------------------------------------------------------
; 313 | void Write8420( USHORT addr, USHORT data )                             
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      3,sp
	.line	2
;----------------------------------------------------------------------
; 315 | ULONG rtmp;                                                            
; 316 | USHORT CWord;                                                          
; 317 | int i;                                                                 
; 319 | // Disable Blevel Update and BIT_DAC                                   
;----------------------------------------------------------------------
	.line	8
;----------------------------------------------------------------------
; 320 | CWord = ReadEDS( iADR_CW );                                            
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(2)
	.line	9
;----------------------------------------------------------------------
; 321 | WriteEDS( iADR_CW, (USHORT)( CWord & 0xA7FF) );                        
;----------------------------------------------------------------------
        ldiu      258,r1
        ldiu      @CL15,r0
        and       *+fp(2),r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	10
;----------------------------------------------------------------------
; 322 | i = 1000;                                                              
; 323 | do{                                                                    
;----------------------------------------------------------------------
        ldiu      1000,r0
        sti       r0,*+fp(3)
	.line	12
;----------------------------------------------------------------------
; 324 | rtmp = *(ULONG *) EDS_BASE + iADR_STAT;                                
;----------------------------------------------------------------------
        ldiu      @CL10,ar0
        ldiu      257,r0
L48:        
        addi3     r0,*ar0,r0            ; Unsigned
        sti       r0,*+fp(1)
	.line	13
;----------------------------------------------------------------------
; 325 | } while((( rtmp & 0x40 ) == 0x40 ) && ( --i > 0 ));                    
;----------------------------------------------------------------------
        ldiu      64,r0
        and       *+fp(1),r0
        cmpi      64,r0
        bned      L50
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L50 
        subri     *+fp(3),r0
        bgtd      L48
        sti       r0,*+fp(3)
        ldigt     @CL10,ar0
        ldigt     257,r0
;*      Branch Occurs to L48 
L50:        
	.line	14
;----------------------------------------------------------------------
; 326 | *(ULONG *)( EDS_BASE + addr ) = data;                           // Writ
;     | e the data directly                                                    
;----------------------------------------------------------------------
        ldiu      *-fp(2),ir0
        ldiu      @CL10,ar0
        ldiu      *-fp(3),r0
        sti       r0,*+ar0(ir0)
	.line	15
;----------------------------------------------------------------------
; 327 | WriteEDS( iADR_CW, CWord );                                            
;----------------------------------------------------------------------
        ldiu      258,r1
        ldiu      *+fp(2),r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	16
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      5,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	328,000000000h,3


	.sect	 ".text"

	.global	_ReadBITADC
	.sym	_ReadBITADC,_ReadBITADC,45,2,0
	.func	334
;******************************************************************************
;* FUNCTION NAME: _ReadBITADC                                                 *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,fp,sp,st                                      *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 2 Auto + 0 SOE = 4 words          *
;******************************************************************************
_ReadBITADC:
	.sym	_rtmp,1,15,1,32
	.sym	_i,2,4,1,32
	.line	1
;----------------------------------------------------------------------
; 334 | USHORT ReadBITADC( void )                                              
; 336 | ULONG rtmp;                                                            
; 337 | int i;                                                                 
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      2,sp
	.line	6
;----------------------------------------------------------------------
; 339 | WriteEDS( iADR_BIT_ADC, 0xFFF );                // Write to the BIT DAC
;     |  to start the conversion                                               
;----------------------------------------------------------------------
        ldiu      4095,r1
        ldiu      353,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	7
;----------------------------------------------------------------------
; 340 | i = 1000;                                                              
; 341 | do{                                                                    
; 342 |         // rtmp = *(ULONG *) EDS_BASE + ( iADR_STAT & 0xFF );          
;----------------------------------------------------------------------
        ldiu      1000,r0
        sti       r0,*+fp(2)
	.line	10
;----------------------------------------------------------------------
; 343 | rtmp = ReadEDS( iADR_STAT );                                           
;----------------------------------------------------------------------
        ldiu      257,r0
L54:        
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	11
;----------------------------------------------------------------------
; 344 | } while((( rtmp & 0x4 ) == 0x4 ) && ( --i > 0 ));
;     |  // ADC still busy                                                     
;----------------------------------------------------------------------
        ldiu      4,r0
        and       *+fp(1),r0
        cmpi      4,r0
        bned      L56
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L56 
        subri     *+fp(2),r0
        bgtd      L54
        sti       r0,*+fp(2)
	nop
        ldigt     257,r0
;*      Branch Occurs to L54 
L56:        
	.line	13
;----------------------------------------------------------------------
; 346 | if( i == 0 ){                                                          
;----------------------------------------------------------------------
        ldi       *+fp(2),r0
        bned      L59
	nop
	nop
        ldine     353,r0
;*      Branch Occurs to L59 
	.line	14
;----------------------------------------------------------------------
; 347 | rtmp = 0xFFFF;                                                         
; 348 | } else {                                                               
;----------------------------------------------------------------------
        bud       L60
        ldiu      @CL16,r0
        sti       r0,*+fp(1)
        nop
;*      Branch Occurs to L60 
	.line	16
;----------------------------------------------------------------------
; 349 | rtmp = ReadEDS( iADR_BIT_ADC );                         // No-Longer Bu
;     | sy so Read the ADC                                                     
;----------------------------------------------------------------------
L59:        
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	18
;----------------------------------------------------------------------
; 351 | rtmp &= 0xFFFF;                                                        
;----------------------------------------------------------------------
L60:        
        and       65535,r0
        sti       r0,*+fp(1)
	.line	19
;----------------------------------------------------------------------
; 352 | return( (USHORT)rtmp );                                                
;----------------------------------------------------------------------
	.line	20
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      4,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	353,000000000h,2


	.sect	 ".text"

	.global	_Set_Time_Const
	.sym	_Set_Time_Const,_Set_Time_Const,32,2,0
	.func	362
;******************************************************************************
;* FUNCTION NAME: _Set_Time_Const                                             *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,ar0,fp,ir0,sp,st                              *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 4 Auto + 0 SOE = 6 words          *
;******************************************************************************
_Set_Time_Const:
	.sym	_DiscEnable,1,13,1,32
	.sym	_data,2,13,1,32
	.sym	_TimeConst,3,13,1,32
	.sym	_Reset_Fir,4,13,1,32
	.line	1
;----------------------------------------------------------------------
; 362 | void Set_Time_Const( void )                                            
; 364 | USHORT DiscEnable;                                                     
; 365 | USHORT data;                                                           
; 366 | USHORT TimeConst;                                                      
; 367 | USHORT Reset_Fir;                                                      
; 370 | // This code is used to set the DPP to the ME FET Mode until the SW han
;     | dles this properly                                                     
; 371 | //      data = ReadEDS( iADR_CW );
;     |                          // Read the Control Word                      
; 372 | //      data |= 0x100;                                                 
; 373 | //      WriteEDS( iADR_CW, data );                                     
; 375 | // These are here to enable proper operation when using the Signal-Tap 
; 376 | //      WriteEDS( iADR_PA_RESET_NTHR,   0x0800 );                      
; 377 | //      WriteEDS( iADR_PA_RESET_PTHR,   0x0100 );                      
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      4,sp
	.line	17
;----------------------------------------------------------------------
; 378 | WriteEDS( iADR_FIR_DLY_LEN,             0x3 );                  // Redu
;     | ce for Throughput, Increase for Reso                                   
;----------------------------------------------------------------------
        ldiu      3,r1
        ldiu      377,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	18
;----------------------------------------------------------------------
; 379 | WriteEDS( iADR_FIR_DLY_INC,             0x4 );                  // Redu
;     | ce for Throughput, Increase for Reso                                   
;----------------------------------------------------------------------
        ldiu      4,r0
        ldiu      378,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	19
;----------------------------------------------------------------------
; 380 | WriteEDS( iADR_INT_EN, 0xFFFF );                                // Enab
;     | le the EDS Interrupt                                                   
;----------------------------------------------------------------------
        ldiu      @CL1,r0
        push      r0
        ldiu      265,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	20
;----------------------------------------------------------------------
; 381 | WriteEDS( iADR_DECAY, 0x0100 );                                        
; 382 | // These are here to enable proper operation when using the Signal-Tap 
; 384 | // Read the TimeConst from the FPGA                                    
;----------------------------------------------------------------------
        ldiu      256,r0
        push      r0
        ldiu      352,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	24
;----------------------------------------------------------------------
; 385 | TimeConst = ReadEDS( iADR_TC_SEL );                                    
;----------------------------------------------------------------------
        ldiu      300,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	25
;----------------------------------------------------------------------
; 386 | TimeConst &= 0xF;                                                      
; 388 | // Read the TimeConst from the DPRAM                                   
;----------------------------------------------------------------------
        ldiu      15,r0
        and       *+fp(3),r0
        sti       r0,*+fp(3)
	.line	28
;----------------------------------------------------------------------
; 389 | data = ReadDPRAM( dADR_TIMECONST );                                    
;----------------------------------------------------------------------
        ldiu      9,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(2)
	.line	29
;----------------------------------------------------------------------
; 390 | data &= 0xF;                                                           
; 392 | // If the Two TimeConsts are different, Reset the FIR                  
;----------------------------------------------------------------------
        ldiu      15,r0
        and       *+fp(2),r0
        sti       r0,*+fp(2)
	.line	32
;----------------------------------------------------------------------
; 393 | if( data == TimeConst ){                                               
;----------------------------------------------------------------------
        cmpi      *+fp(3),r0
        bned      L66
	nop
	nop
        ldine     1,r0
;*      Branch Occurs to L66 
	.line	33
;----------------------------------------------------------------------
; 394 | EDSRec.Reset_FIR = 0;                                                  
; 395 | } else {                                                               
;----------------------------------------------------------------------
        bud       L67
        ldiu      0,r0
        sti       r0,@_EDSRec+33
        ldiu      *+fp(2),r1
;*      Branch Occurs to L67 
	.line	35
;----------------------------------------------------------------------
; 396 | EDSRec.Reset_FIR = 1;                                                  
;----------------------------------------------------------------------
L66:        
        sti       r0,@_EDSRec+33
	.line	37
;----------------------------------------------------------------------
; 398 | WriteEDS( iADR_TC_SEL, data );                                         
;----------------------------------------------------------------------
        ldiu      *+fp(2),r1
L67:        
        ldiu      300,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	38
;----------------------------------------------------------------------
; 399 | TimeConst       = data;                                                
;----------------------------------------------------------------------
        ldiu      *+fp(2),r0
        sti       r0,*+fp(3)
	.line	39
;----------------------------------------------------------------------
; 400 | WriteEDS( iADR_EVCH1, 0x0 );                                    // eV/C
;     | h1 Must be set to 0V for SiLi Detectors                                
;----------------------------------------------------------------------
        ldiu      0,r1
        push      r1
        ldiu      272,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	42
;----------------------------------------------------------------------
; 403 | data = ReadEDS( iADR_CW );                                             
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(2)
	.line	43
;----------------------------------------------------------------------
; 404 | if(( data & 0x8000 ) == 0 ){                                           
;----------------------------------------------------------------------
        tstb      32768,r0
        bned      L70
	nop
	nop
        ldine     16,r0
;*      Branch Occurs to L70 
	.line	44
;----------------------------------------------------------------------
; 405 | DiscEnable = DISC_SH | DISC_E;                                         
; 406 | } else {                                                               
;----------------------------------------------------------------------
        bud       L77
        ldiu      17,r0
        sti       r0,*+fp(1)
        ldiu      *+fp(3),ir0
;*      Branch Occurs to L77 
	.line	46
;----------------------------------------------------------------------
; 407 | DiscEnable = DISC_E;
;     |  // SDD (Ketek - Disable Super High Speed Dicriminator                 
; 410 | switch( TimeConst ){                                                   
; 411 | case 0 :
;     |                  //  PT0:  0.4us                                       
; 412 | case 1 :
;     |                  //  PT1:  0.8us                                       
;----------------------------------------------------------------------
L70:        
        bud       L78
        sti       r0,*+fp(1)
        ldiu      *+fp(3),ir0
        cmpi      7,ir0
;*      Branch Occurs to L78 
	.line	52
;----------------------------------------------------------------------
; 413 | break;                                                                 
; 415 | case 2 :
;     |                  //  PT2:  1.6us                                       
;----------------------------------------------------------------------
L72:        
	.line	55
;----------------------------------------------------------------------
; 416 | DiscEnable |= DISC_H;                                           // Enab
;     | le the High 0.4us                                                      
;----------------------------------------------------------------------
	.line	56
;----------------------------------------------------------------------
; 417 | break;                                                                 
; 418 | case 3 :
;     |                  //  PT3:  3.2us                                       
; 419 | case 4 :
;     |                  //  PT4:  6.4us                                       
; 420 | case 5 :
;     |                  //  PT5: 12.8us                                       
;----------------------------------------------------------------------
        bud       L80
        ldiu      2,r0
        or        *+fp(1),r0
        sti       r0,*+fp(1)
;*      Branch Occurs to L80 
L73:        
	.line	60
;----------------------------------------------------------------------
; 421 | DiscEnable |= DISC_H;                                           // Enab
;     | le the High 0.4us                                                      
;----------------------------------------------------------------------
        ldiu      2,r0
        or        *+fp(1),r0
        sti       r0,*+fp(1)
	.line	61
;----------------------------------------------------------------------
; 422 | DiscEnable |= DISC_M;                                           // Enab
;     | le the Med  0.8us                                                      
;----------------------------------------------------------------------
	.line	62
;----------------------------------------------------------------------
; 423 | break;                                                                 
; 424 | case 6 :
;     |                  //  PT6: 25.6us                                       
; 425 | case 7 :
;     |                  //  PT7: 51.2us                                       
; 426 | default :
;     |                  // PT8: 102.4us                                       
;----------------------------------------------------------------------
        bud       L80
        ldiu      4,r0
        or        *+fp(1),r0
        sti       r0,*+fp(1)
;*      Branch Occurs to L80 
L74:        
	.line	66
;----------------------------------------------------------------------
; 427 | DiscEnable |= DISC_H;                                           // Enab
;     | le the High 0.4us                                                      
;----------------------------------------------------------------------
        ldiu      2,r0
L75:        
        or        *+fp(1),r0
        sti       r0,*+fp(1)
	.line	67
;----------------------------------------------------------------------
; 428 | DiscEnable |= DISC_M;                                           // Enab
;     | le the Med  0.8us                                                      
;----------------------------------------------------------------------
        ldiu      4,r0
        or        *+fp(1),r0
        sti       r0,*+fp(1)
	.line	68
;----------------------------------------------------------------------
; 429 | DiscEnable |= DISC_L;                                           // Enab
;     | le the Low  6.4us                                                      
;----------------------------------------------------------------------
	.line	69
;----------------------------------------------------------------------
; 430 | break;                                                                 
;----------------------------------------------------------------------
        bud       L80
        ldiu      8,r0
        or        *+fp(1),r0
        sti       r0,*+fp(1)
;*      Branch Occurs to L80 
	.line	49
L77:        
        cmpi      7,ir0
L78:        
        bhid      L75
	nop
	nop
        ldihi     2,r0
;*      Branch Occurs to L75 
        ldiu      @CL17,ar0
        ldiu      *+ar0(ir0),r0
        bu        r0

	.sect	".text"
SW0:	.word	L80	; 0
	.word	L80	; 1
	.word	L72	; 2
	.word	L73	; 3
	.word	L73	; 4
	.word	L73	; 5
	.word	L74	; 6
	.word	L74	; 7
	.sect	".text"
;*      Branch Occurs to r0 
L80:        
	.line	71
;----------------------------------------------------------------------
; 432 | WriteEDS( iADR_DISC_EN, DiscEnable );                                  
;----------------------------------------------------------------------
        ldiu      *+fp(1),r1
        ldiu      299,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	72
;----------------------------------------------------------------------
; 433 | Set_EvCh();                                                            
;----------------------------------------------------------------------
        call      _Set_EvCh
                                        ; Call Occurs
	.line	73
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	434,000000000h,4


	.sect	 ".text"

	.global	_Set_EvCh
	.sym	_Set_EvCh,_Set_EvCh,32,2,0
	.func	438
;******************************************************************************
;* FUNCTION NAME: _Set_EvCh                                                   *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,fp,sp                                         *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 1 Auto + 0 SOE = 3 words          *
;******************************************************************************
_Set_EvCh:
	.sym	_data,1,13,1,32
	.line	1
;----------------------------------------------------------------------
; 438 | void Set_EvCh( void )                                                  
; 440 | USHORT data;                                                           
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      1,sp
	.line	5
;----------------------------------------------------------------------
; 442 | data = ReadDPRAM( dADR_EVCH );
;     |  // Read from DP Memory                                                
;----------------------------------------------------------------------
        ldiu      31,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	6
;----------------------------------------------------------------------
; 443 | WriteEDS( iADR_DISC_EVCH, data );                                      
;----------------------------------------------------------------------
        ldiu      379,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	7
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      3,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	444,000000000h,1


	.sect	 ".text"

	.global	_Set_Blank_Factor
	.sym	_Set_Blank_Factor,_Set_Blank_Factor,32,2,0
	.func	510
;******************************************************************************
;* FUNCTION NAME: _Set_Blank_Factor                                           *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : f0,r0,f1,r1,ar0,fp,ir0,sp,st                        *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 3 Auto + 0 SOE = 5 words          *
;******************************************************************************
_Set_Blank_Factor:
	.sym	_data,1,13,1,32
	.sym	_factor,2,6,1,32
	.sym	_PTime,3,6,1,32
	.line	1
;----------------------------------------------------------------------
; 510 | void Set_Blank_Factor( void )                                          
; 512 | USHORT data;                                                           
; 513 | float factor;                                                          
; 514 | float PTime;                                                           
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      3,sp
	.line	7
;----------------------------------------------------------------------
; 516 | data = ReadDPRAM( dADR_TIMECONST );                                    
;----------------------------------------------------------------------
        ldiu      9,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	8
;----------------------------------------------------------------------
; 517 | data &= 0xF;                                                           
; 519 | switch( data ){                                                        
;----------------------------------------------------------------------
        bud       L97
        ldiu      15,r0
        and       *+fp(1),r0
        sti       r0,*+fp(1)
;*      Branch Occurs to L97 
L87:        
	.line	11
;----------------------------------------------------------------------
; 520 | case 0  : PTime =   0.4f; break;                                       
;----------------------------------------------------------------------
        bud       L100
        ldfu      @CL18,f0
        stf       f0,*+fp(3)
        ldiu      32,r0
;*      Branch Occurs to L100 
L88:        
	.line	12
;----------------------------------------------------------------------
; 521 | case 1  : PTime =   0.8f; break;                                       
;----------------------------------------------------------------------
        bud       L100
        ldfu      @CL19,f0
        stf       f0,*+fp(3)
        ldiu      32,r0
;*      Branch Occurs to L100 
L89:        
	.line	13
;----------------------------------------------------------------------
; 522 | case 2  : PTime =   1.6f; break;                                       
;----------------------------------------------------------------------
        bud       L100
        ldfu      @CL20,f0
        stf       f0,*+fp(3)
        ldiu      32,r0
;*      Branch Occurs to L100 
L90:        
	.line	14
;----------------------------------------------------------------------
; 523 | case 3  : PTime =   3.2f; break;                                       
;----------------------------------------------------------------------
        bud       L100
        ldfu      @CL21,f0
        stf       f0,*+fp(3)
        ldiu      32,r0
;*      Branch Occurs to L100 
L91:        
	.line	15
;----------------------------------------------------------------------
; 524 | case 4  : PTime =   6.4f; break;                                       
;----------------------------------------------------------------------
        bud       L100
        ldfu      @CL22,f0
        stf       f0,*+fp(3)
        ldiu      32,r0
;*      Branch Occurs to L100 
L92:        
	.line	16
;----------------------------------------------------------------------
; 525 | case 5  : PTime =  12.8f; break;                                       
;----------------------------------------------------------------------
        bud       L100
        ldfu      @CL23,f0
        stf       f0,*+fp(3)
        ldiu      32,r0
;*      Branch Occurs to L100 
L93:        
	.line	17
;----------------------------------------------------------------------
; 526 | case 6  : PTime =  25.6f; break;                                       
;----------------------------------------------------------------------
        bud       L100
        ldfu      @CL24,f0
        stf       f0,*+fp(3)
        ldiu      32,r0
;*      Branch Occurs to L100 
L94:        
	.line	18
;----------------------------------------------------------------------
; 527 | case 7  : PTime =  51.2f; break;                                       
;----------------------------------------------------------------------
        bud       L100
        ldfu      @CL25,f0
        stf       f0,*+fp(3)
        ldiu      32,r0
;*      Branch Occurs to L100 
L95:        
	.line	19
;----------------------------------------------------------------------
; 528 | default : PTime = 102.4f; break;                                       
;----------------------------------------------------------------------
        bud       L100
        ldfu      @CL26,f0
        stf       f0,*+fp(3)
        ldiu      32,r0
;*      Branch Occurs to L100 
L97:        
	.line	10
        ldiu      *+fp(1),ir0
        cmpi      7,ir0
        bhid      L95
	nop
	nop
        ldils     @CL27,ar0
;*      Branch Occurs to L95 
        ldiu      *+ar0(ir0),r0
        bu        r0

	.sect	".text"
SW1:	.word	L87	; 0
	.word	L88	; 1
	.word	L89	; 2
	.word	L90	; 3
	.word	L91	; 4
	.word	L92	; 5
	.word	L93	; 6
	.word	L94	; 7
	.sect	".text"
;*      Branch Occurs to r0 
	.line	21
;----------------------------------------------------------------------
; 530 | factor = (float)ReadDPRAM( dADR_BLANK_FACTOR ) / (float)10.0f;         
;----------------------------------------------------------------------
L100:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        float     r0,f0
        ldflt     @CL28,f1
        subi      1,sp
        ldfge     0.0000000000e+00,f1
        addf3     f0,f1,f0
        mpyf      @CL29,f0
        stf       f0,*+fp(2)
	.line	22
;----------------------------------------------------------------------
; 531 | data = (USHORT) (( PTime*factor ) / 0.05f );                           
;----------------------------------------------------------------------
        mpyf      *+fp(3),f0
        mpyf      2.0000000000e+01,f0
        cmpf      @CL30,f0
        ldflt     0.0000000000e+00,f1
        ldfge     @CL28,f1
        subrf     f0,f1
        fix       f1,r0
        sti       r0,*+fp(1)
	.line	23
;----------------------------------------------------------------------
; 532 | WriteEDS( iADR_BLANK_WIDTH, data );                                    
;----------------------------------------------------------------------
        ldiu      r0,r1
        push      r1
        ldiu      307,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	25
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      5,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	534,000000000h,3


	.sect	 ".text"

	.global	_SetAnalMode
	.sym	_SetAnalMode,_SetAnalMode,32,2,0
	.func	538
;******************************************************************************
;* FUNCTION NAME: _SetAnalMode                                                *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,ar0,fp,ir0,sp,st                              *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 3 Auto + 0 SOE = 5 words          *
;******************************************************************************
_SetAnalMode:
	.sym	_data,1,13,1,32
	.sym	_Target_Status,2,13,1,32
	.sym	_done,3,4,1,32
	.line	1
;----------------------------------------------------------------------
; 538 | void SetAnalMode( void )                                               
; 540 | USHORT data;                                                           
; 541 | USHORT Target_Status;                                                  
; 542 | int done;                                                              
; 544 | // Set Status Bit 2 to indicate in SetAnalMode                         
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      3,sp
	.line	8
;----------------------------------------------------------------------
; 545 | data = ReadDPRAM( iADR_STAT );                                         
;----------------------------------------------------------------------
        ldiu      257,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	9
;----------------------------------------------------------------------
; 546 | data |= 0x4;                                                           
;----------------------------------------------------------------------
        ldiu      4,r0
        or        *+fp(1),r0
        sti       r0,*+fp(1)
	.line	10
;----------------------------------------------------------------------
; 547 | WriteDPRAM( dADR_EDS_STAT, data );                      // Clear the St
;     | atus Register but Set bit 2                                            
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      4,r0
        push      r1
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	12
;----------------------------------------------------------------------
; 549 | EDSRec.EDS_Mode = (UCHAR)( ReadDPRAM( dADR_ANAL_MODE ) & 0xF );        
; 551 | switch( EDSRec.EDS_Mode) {                                             
;----------------------------------------------------------------------
        ldiu      17,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        bud       L114
        and       15,r0
        subi      1,sp
        sti       r0,@_EDSRec+16
;*      Branch Occurs to L114 
L104:        
	.line	15
;----------------------------------------------------------------------
; 552 | case EDS_ANAL_PRESET_REAL               : EDSRec.Preset_Mode = FPGA_PRE
;     | SET_CLOCK;               break;                                        
;----------------------------------------------------------------------
        bud       L117
        ldiu      1,r0
        sti       r0,@_EDSRec+9
        ldiu      @_EDSRec+9,r1
;*      Branch Occurs to L117 
L105:        
	.line	16
;----------------------------------------------------------------------
; 553 | case EDS_ANAL_PRESET_LIVE               : EDSRec.Preset_Mode = FPGA_PRE
;     | SET_LIVE;                break;                                        
;----------------------------------------------------------------------
        bud       L117
        ldiu      2,r0
        sti       r0,@_EDSRec+9
        ldiu      @_EDSRec+9,r1
;*      Branch Occurs to L117 
L106:        
	.line	17
;----------------------------------------------------------------------
; 554 | case EDS_ANAL_PRESET_COUNT              : EDSRec.Preset_Mode = FPGA_PRE
;     | SET_COUNT;               break;                                        
;----------------------------------------------------------------------
        bud       L117
        ldiu      5,r0
        sti       r0,@_EDSRec+9
        ldiu      @_EDSRec+9,r1
;*      Branch Occurs to L117 
L107:        
	.line	18
;----------------------------------------------------------------------
; 555 | case EDS_ANAL_PRESET_SMAP_REAL  : EDSRec.Preset_Mode = FPGA_PRESET_LSMA
;     | P_REAL;  break;                                                        
;----------------------------------------------------------------------
        bud       L117
        ldiu      7,r0
        sti       r0,@_EDSRec+9
        ldiu      @_EDSRec+9,r1
;*      Branch Occurs to L117 
L108:        
	.line	19
;----------------------------------------------------------------------
; 556 | case EDS_ANAL_PRESET_SMAP_LIVE  : EDSRec.Preset_Mode = FPGA_PRESET_LSMA
;     | P_LIVE;  break;                                                        
;----------------------------------------------------------------------
        bud       L117
        ldiu      8,r0
        sti       r0,@_EDSRec+9
        ldiu      @_EDSRec+9,r1
;*      Branch Occurs to L117 
L109:        
	.line	20
;----------------------------------------------------------------------
; 557 | case EDS_ANAL_PRESET_NONE               : EDSRec.Preset_Mode = FPGA_PRE
;     | SET_NONE;                break;                                        
;----------------------------------------------------------------------
        bud       L117
        ldiu      0,r0
        sti       r0,@_EDSRec+9
        ldiu      @_EDSRec+9,r1
;*      Branch Occurs to L117 
L110:        
	.line	21
;----------------------------------------------------------------------
; 558 | case EDS_ANAL_PRESET_REAL_PAMR  : EDSRec.Preset_Mode = FPGA_PRESET_CLOC
;     | K_PAMR;  break;                                                        
;----------------------------------------------------------------------
        bud       L117
        ldiu      6,r0
        sti       r0,@_EDSRec+9
        ldiu      @_EDSRec+9,r1
;*      Branch Occurs to L117 
L111:        
	.line	22
;----------------------------------------------------------------------
; 559 | case EDS_ANAL_PRESET_WDS_REAL   : EDSRec.Preset_Mode = FPGA_PRESET_WDS_
;     | REAL;    break;                                                        
;----------------------------------------------------------------------
        bud       L117
        ldiu      9,r0
        sti       r0,@_EDSRec+9
        ldiu      @_EDSRec+9,r1
;*      Branch Occurs to L117 
L112:        
	.line	23
;----------------------------------------------------------------------
; 560 | case EDS_ANAL_PRESET_WDS_LIVE   : EDSRec.Preset_Mode = FPGA_PRESET_WDS_
;     | LIVE;    break;                                                        
;----------------------------------------------------------------------
        bud       L117
        ldiu      10,r0
        sti       r0,@_EDSRec+9
        ldiu      @_EDSRec+9,r1
;*      Branch Occurs to L117 
	.line	24
;----------------------------------------------------------------------
; 561 | case EDS_ANAL_STOP                      :
;     |                                          break;                        
;----------------------------------------------------------------------
	.line	25
;----------------------------------------------------------------------
; 562 | case EDS_ANAL_RESUME            :
;     |                                  break;                                
;----------------------------------------------------------------------
	.line	26
;----------------------------------------------------------------------
; 563 | default                                         :
;     |                                                  break;                
;----------------------------------------------------------------------
L114:        
	.line	14
        ldiu      @_EDSRec+16,ir0
        cmpi      14,ir0
        bhid      L118
	nop
        ldihi     @_EDSRec+9,r1
        ldihi     278,r0
;*      Branch Occurs to L118 
        ldiu      @CL31,ar0
        ldiu      *+ar0(ir0),r0
        bu        r0

	.sect	".text"
SW2:	.word	L116	; 0
	.word	L104	; 1
	.word	L106	; 2
	.word	L109	; 3
	.word	L105	; 4
	.word	L116	; 5
	.word	L116	; 0
	.word	L116	; 0
	.word	L116	; 0
	.word	L116	; 0
	.word	L107	; 10
	.word	L108	; 11
	.word	L110	; 12
	.word	L111	; 13
	.word	L112	; 14
	.sect	".text"
;*      Branch Occurs to r0 
L116:        
	.line	28
;----------------------------------------------------------------------
; 565 | WriteEDS( iADR_PRESET_MODE, EDSRec.Preset_Mode );       // Write th FPG
;     | A Preset Mode                                                          
; 567 | // Determine if xfer data to PCI while analyzer is running             
;----------------------------------------------------------------------
        ldiu      @_EDSRec+9,r1
L117:        
        ldiu      278,r0
L118:        
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	31
;----------------------------------------------------------------------
; 568 | data  = ReadEDS( iADR_CW );                                            
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	32
;----------------------------------------------------------------------
; 569 | if(( ReadDPRAM( dADR_TRANSFER ) & 0x1 ) == 0x1 ){                      
;----------------------------------------------------------------------
        ldiu      18,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        and       1,r0
        cmpi      1,r0
        bned      L121
        subi      1,sp
	nop
        ldine     0,r0
;*      Branch Occurs to L121 
	.line	33
;----------------------------------------------------------------------
; 570 | EDSRec.EDSXFer = 1;                                                    
;----------------------------------------------------------------------
        ldiu      1,r0
        sti       r0,@_EDSRec+1
	.line	34
;----------------------------------------------------------------------
; 571 | data |= EDS_XFER;                                                      
; 572 | } else {                                                               
;----------------------------------------------------------------------
        bud       L122
        ldiu      8,r0
        or        *+fp(1),r0
        sti       r0,*+fp(1)
;*      Branch Occurs to L122 
	.line	36
;----------------------------------------------------------------------
; 573 | EDSRec.EDSXFer = 0;                                                    
;----------------------------------------------------------------------
L121:        
        sti       r0,@_EDSRec+1
	.line	37
;----------------------------------------------------------------------
; 574 | data &= ~EDS_XFER;                                                     
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
        andn      8,r0
        sti       r0,*+fp(1)
L122:        
	.line	39
;----------------------------------------------------------------------
; 576 | WriteEDS( iADR_CW, data );                                             
; 578 | switch( EDSRec.EDS_Mode ){                                             
; 579 |         case EDS_ANAL_PRESET_REAL :                                    
; 580 |         case EDS_ANAL_PRESET_LIVE :                                    
; 581 |         case EDS_ANAL_PRESET_COUNT:                     // Preset ROI  
; 582 |         case EDS_ANAL_PRESET_NONE :                                    
; 583 |         case EDS_ANAL_PRESET_REAL_PAMR :                               
;----------------------------------------------------------------------
        ldiu      *+fp(1),r1
        ldiu      258,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        bud       L129
        subi      2,sp
        ldiu      @_EDSRec+16,ir0
        cmpi      14,ir0
;*      Branch Occurs to L129 
L123:        
	.line	47
;----------------------------------------------------------------------
; 584 | Target_Status = 1;                                                     
;----------------------------------------------------------------------
        ldiu      1,r0
        sti       r0,*+fp(2)
	.line	48
;----------------------------------------------------------------------
; 585 | EDSRec.FPGA_Preset_Done = 0;                                           
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_EDSRec+0
	.line	50
;----------------------------------------------------------------------
; 587 | WriteEDS( iADR_TIME_STOP, 0xFFFF );             // Stop the Acquisition
;----------------------------------------------------------------------
        ldiu      276,r1
        ldiu      @CL1,r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	51
;----------------------------------------------------------------------
; 588 | WriteEDS( iADR_TIME_CLR, 0xFFFF);                       // Clear Clock
;     | Time and Live Time                                                     
;----------------------------------------------------------------------
        ldiu      @CL1,r0
        ldiu      277,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	53
;----------------------------------------------------------------------
; 590 | data = ReadDPRAM( iADR_PRESET_LO );             // Read the Preset Low
;     | Bits                                                                   
;----------------------------------------------------------------------
        ldiu      279,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	54
;----------------------------------------------------------------------
; 591 | WriteEDS( iADR_PRESET_LO, data );                       // Write them t
;     | o the FPGA                                                             
;----------------------------------------------------------------------
        ldiu      r0,r1
        push      r1
        ldiu      279,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	55
;----------------------------------------------------------------------
; 592 | data = ReadDPRAM( iADR_PRESET_HI );             // Read the Preset High
;     |  Bits                                                                  
;----------------------------------------------------------------------
        ldiu      280,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	56
;----------------------------------------------------------------------
; 593 | WriteEDS( iADR_PRESET_HI, data );                       // Write them t
;     | o the FPGA                                                             
;----------------------------------------------------------------------
        ldiu      280,r0
        ldiu      *+fp(1),r1
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	58
;----------------------------------------------------------------------
; 595 | WriteEDS( iADR_TIME_START, 0x0 );                       // Start the Ac
;     | quisition                                                              
;----------------------------------------------------------------------
        ldiu      275,r1
        ldiu      0,r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
	.line	59
;----------------------------------------------------------------------
; 596 | break;                                                                 
; 599 | case EDS_ANAL_PRESET_SMAP_REAL :                                       
; 600 | case EDS_ANAL_PRESET_SMAP_LIVE :                                       
; 601 | case EDS_ANAL_PRESET_WDS_REAL :                                        
; 602 | case EDS_ANAL_PRESET_WDS_LIVE :                                        
;----------------------------------------------------------------------
        bud       L132
        subi      2,sp
        ldiu      @_EDSRec+16,r0
        cmpi      12,r0
;*      Branch Occurs to L132 
L124:        
	.line	66
;----------------------------------------------------------------------
; 603 | EDSRec.FPGA_Preset_Done = 0;                                           
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_EDSRec+0
	.line	67
;----------------------------------------------------------------------
; 604 | EDSRec.Preset_Done_Cnt = 0;                                            
;----------------------------------------------------------------------
        sti       r0,@_EDSRec+10
	.line	68
;----------------------------------------------------------------------
; 605 | WriteDPRAM( dADR_PRESET_DONE_CNT, 0x0 );                               
;----------------------------------------------------------------------
        ldiu      0,r1
        push      r1
        ldiu      36,r0
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	69
;----------------------------------------------------------------------
; 606 | WriteEDS( iADR_TIME_STOP, 0xFFFF );             // Stop the Acquisition
;----------------------------------------------------------------------
        ldiu      @CL1,r0
        ldiu      276,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	70
;----------------------------------------------------------------------
; 607 | WriteEDS( iADR_TIME_CLR, 0xFFFF);                       // Clear Clock
;     | Time and Live Time                                                     
;----------------------------------------------------------------------
        ldiu      @CL1,r0
        push      r0
        ldiu      277,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	72
;----------------------------------------------------------------------
; 609 | data = ReadDPRAM( iADR_PRESET_LO );             // Read the Preset Low
;     | Bits                                                                   
;----------------------------------------------------------------------
        ldiu      279,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	73
;----------------------------------------------------------------------
; 610 | WriteEDS( iADR_PRESET_LO, data );                       // Write them t
;     | o the FPGA                                                             
;----------------------------------------------------------------------
        ldiu      r0,r1
        push      r1
        ldiu      279,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	74
;----------------------------------------------------------------------
; 611 | data = ReadDPRAM( iADR_PRESET_HI );             // Read the Preset High
;     |  Bits                                                                  
;----------------------------------------------------------------------
        ldiu      280,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	75
;----------------------------------------------------------------------
; 612 | WriteEDS( iADR_PRESET_HI, data );                       // Write them t
;     | o the FPGA                                                             
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      280,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
	.line	76
;----------------------------------------------------------------------
; 613 | break;                                                                 
; 615 | case EDS_ANAL_RESUME    :                                              
;----------------------------------------------------------------------
        bud       L132
        subi      2,sp
        ldiu      @_EDSRec+16,r0
        cmpi      12,r0
;*      Branch Occurs to L132 
L125:        
	.line	79
;----------------------------------------------------------------------
; 616 | Target_Status = 1;                                                     
;----------------------------------------------------------------------
        ldiu      1,r0
        sti       r0,*+fp(2)
	.line	80
;----------------------------------------------------------------------
; 617 | EDSRec.FPGA_Preset_Done = 0;                                           
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_EDSRec+0
	.line	81
;----------------------------------------------------------------------
; 618 | WriteEDS( iADR_TIME_START, 0x0 );                       // Start the Ac
;     | quisition                                                              
;----------------------------------------------------------------------
        ldiu      0,r1
        push      r1
        ldiu      275,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
	.line	82
;----------------------------------------------------------------------
; 619 | break;                                                                 
; 621 | case EDS_ANAL_STOP      :                                              
; 622 | default :                                                              
;----------------------------------------------------------------------
        bud       L132
        subi      2,sp
        ldiu      @_EDSRec+16,r0
        cmpi      12,r0
;*      Branch Occurs to L132 
L126:        
	.line	86
;----------------------------------------------------------------------
; 623 | Target_Status = 0;                                                     
;----------------------------------------------------------------------
        ldiu      0,r0
L127:        
        sti       r0,*+fp(2)
	.line	87
;----------------------------------------------------------------------
; 624 | EDSRec.FPGA_Preset_Done = 0x4000;                       // Set the FPGA
;     |  Preset Done, will force Preset Done,                                  
; 625 | 
;     |  // Preset Done will disable the Timer Interrupt to the PCI            
;----------------------------------------------------------------------
        ldiu      16384,r0
        sti       r0,@_EDSRec+0
	.line	89
;----------------------------------------------------------------------
; 626 | WriteEDS( iADR_TIME_STOP, 0xFFFF );             // Stop the Acquisition
;----------------------------------------------------------------------
        ldiu      @CL1,r1
        push      r1
        ldiu      276,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
	.line	90
;----------------------------------------------------------------------
; 627 | break;                                                                 
; 628 | }
;     |                  // switch...                                          
;----------------------------------------------------------------------
        bud       L132
        subi      2,sp
        ldiu      @_EDSRec+16,r0
        cmpi      12,r0
;*      Branch Occurs to L132 
	.line	41
L129:        
        bhid      L127
	nop
	nop
        ldihi     0,r0
;*      Branch Occurs to L127 
        ldiu      @CL32,ar0
        ldiu      *+ar0(ir0),r0
        bu        r0

	.sect	".text"
SW3:	.word	L126	; 0
	.word	L123	; 1
	.word	L123	; 2
	.word	L123	; 3
	.word	L123	; 4
	.word	L125	; 5
	.word	L126	; 0
	.word	L126	; 0
	.word	L126	; 0
	.word	L126	; 0
	.word	L124	; 10
	.word	L124	; 11
	.word	L123	; 12
	.word	L124	; 13
	.word	L124	; 14
	.sect	".text"
;*      Branch Occurs to r0 
	.line	93
;----------------------------------------------------------------------
; 630 | if( ( EDSRec.EDS_Mode  == EDS_ANAL_PRESET_REAL_PAMR )                  
; 631 |  || ( EDSRec.EDS_Mode  == EDS_ANAL_PRESET_SMAP_REAL )                  
; 632 |  || ( EDSRec.EDS_Mode  == EDS_ANAL_PRESET_SMAP_LIVE )                  
; 633 |  || ( EDSRec.EDS_Mode  == EDS_ANAL_PRESET_WDS_REAL  )                  
; 634 |  || ( EDSRec.EDS_Mode  == EDS_ANAL_PRESET_WDS_LIVE  ) ){               
;----------------------------------------------------------------------
L132:        
        beqd      L138
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L138 
        ldiu      @_EDSRec+16,r0
        cmpi      10,r0
        beqd      L138
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L138 
        ldiu      @_EDSRec+16,r0
        cmpi      11,r0
        beqd      L138
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L138 
        ldiu      @_EDSRec+16,r0
        cmpi      13,r0
        beqd      L138
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L138 
        ldiu      @_EDSRec+16,r0
        cmpi      14,r0
        bned      L140
	nop
	nop
        ldine     0,r0
;*      Branch Occurs to L140 
	.line	98
;----------------------------------------------------------------------
; 635 | done = 1;                                                              
; 636 | } else {                                                               
;----------------------------------------------------------------------
        ldiu      1,r0
L138:        
        bud       L155
        sti       r0,*+fp(3)
        nop
        cmpi      1,r0
;*      Branch Occurs to L155 
	.line	100
;----------------------------------------------------------------------
; 637 | data = 0;                                                              
;----------------------------------------------------------------------
L140:        
        sti       r0,*+fp(1)
	.line	101
;----------------------------------------------------------------------
; 638 | done = 0;                                                              
; 639 | do{                                                                    
;----------------------------------------------------------------------
        sti       r0,*+fp(3)
	.line	103
;----------------------------------------------------------------------
; 640 | EDSRec.EDS_Stat = (( ReadEDS( iADR_STAT ) >> 3 ) & 0x1 );       // Get
;     | EDS Status from FPGA                                                   
;----------------------------------------------------------------------
        ldiu      257,r0
L141:        
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        lsh       -3,r0
        subi      1,sp
        and       1,r0
        sti       r0,@_EDSRec+2
	.line	104
;----------------------------------------------------------------------
; 641 | if( Target_Status == 1 ){                                              
;----------------------------------------------------------------------
        ldiu      *+fp(2),r0
        cmpi      1,r0
        bned      L146
	nop
	nop
        ldieq     @_EDSRec+2,r0
;*      Branch Occurs to L146 
	.line	105
;----------------------------------------------------------------------
; 642 | if( EDSRec.EDS_Stat == 1 ){                             // Attempting t
;     | o STart                                                                
;----------------------------------------------------------------------
        cmpi      1,r0
        bned      L145
	nop
        ldine     0,r1
        ldine     275,r0
;*      Branch Occurs to L145 
	.line	106
;----------------------------------------------------------------------
; 643 | done = 1;                                                              
; 644 | } else {                                                               
;----------------------------------------------------------------------
        bud       L151
        ldiu      1,r0
        sti       r0,*+fp(3)
        nop
;*      Branch Occurs to L151 
	.line	108
;----------------------------------------------------------------------
; 645 | WriteEDS( iADR_TIME_START, 0x0 );                                      
;----------------------------------------------------------------------
L145:        
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
	.line	109
;----------------------------------------------------------------------
; 646 | done = 0;                                                              
; 648 | } else {                                                               
;----------------------------------------------------------------------
        bud       L150
        subi      2,sp
        ldiu      0,r0
        sti       r0,*+fp(3)
;*      Branch Occurs to L150 
L146:        
	.line	112
;----------------------------------------------------------------------
; 649 | if( EDSRec.EDS_Stat == 0 ){                                            
;----------------------------------------------------------------------
        ldi       @_EDSRec+2,r0
        bned      L149
	nop
        ldine     0,r0
        ldine     276,r1
;*      Branch Occurs to L149 
	.line	113
;----------------------------------------------------------------------
; 650 | done = 1;                                                              
; 651 | } else {                                                               
;----------------------------------------------------------------------
        bud       L151
        ldiu      1,r0
        sti       r0,*+fp(3)
        nop
;*      Branch Occurs to L151 
	.line	115
;----------------------------------------------------------------------
; 652 | WriteEDS( iADR_TIME_STOP, 0x0 );                                       
;----------------------------------------------------------------------
L149:        
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	116
;----------------------------------------------------------------------
; 653 | done = 0;                                                              
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(3)
L150:        
	.line	119
;----------------------------------------------------------------------
; 656 | data++;                                                                
;----------------------------------------------------------------------
        ldiu      1,r0
L151:        
        addi      *+fp(1),r0            ; Unsigned
        sti       r0,*+fp(1)
	.line	120
;----------------------------------------------------------------------
; 657 | } while(( done == 0 ) && ( data < 0xFE ));                             
;----------------------------------------------------------------------
        ldi       *+fp(3),r0
        bned      L154
	nop
	nop
        ldine     *+fp(3),r0
;*      Branch Occurs to L154 
        ldiu      *+fp(1),r0
        cmpi      254,r0
        blod      L141
	nop
	nop
        ldilo     257,r0
;*      Branch Occurs to L141 
	.line	123
;----------------------------------------------------------------------
; 660 | if( done == 1 ){                                                       
; 661 |         // 05/16/00 - MCS - Ver 1.07                                   
;----------------------------------------------------------------------
        ldiu      *+fp(3),r0
L154:        
        cmpi      1,r0
L155:        
        bned      L158
	nop
        ldine     @CL1,r0
        ldine     4,r1
;*      Branch Occurs to L158 
	.line	125
;----------------------------------------------------------------------
; 662 | data = ReadDPRAM( dADR_EDS_STAT );                                     
;----------------------------------------------------------------------
        ldiu      4,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	126
;----------------------------------------------------------------------
; 663 | data &= 0xFFFC;                                         // Clear Preset
;     |  Done and EDS Status                                                   
;----------------------------------------------------------------------
        and       65532,r0
        sti       r0,*+fp(1)
	.line	127
;----------------------------------------------------------------------
; 664 | data |= EDSRec.EDS_Stat;                                        // Set
;     | EDS Status in the LSB                                                  
;----------------------------------------------------------------------
        ldiu      @_EDSRec+2,r0
        or        *+fp(1),r0
        sti       r0,*+fp(1)
	.line	128
;----------------------------------------------------------------------
; 665 | WriteDPRAM( dADR_EDS_STAT, data );                                     
; 666 | } else {                                                               
;----------------------------------------------------------------------
        ldiu      r0,r1
        push      r1
        ldiu      4,r0
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        bud       L159
        subi      2,sp
        ldiu      4,r0
        push      r0
;*      Branch Occurs to L159 
	.line	130
;----------------------------------------------------------------------
; 667 | WriteDPRAM( dADR_EDS_STAT, 0xFFFF );            // Error Code          
;----------------------------------------------------------------------
L158:        
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	132
;----------------------------------------------------------------------
; 669 | data = ReadDPRAM( dADR_EDS_STAT );                                     
;----------------------------------------------------------------------
        ldiu      4,r0
        push      r0
L159:        
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	133
;----------------------------------------------------------------------
; 670 | data &= ~0x4;
;     |  // Clear in SET_ANAL bit                                              
;----------------------------------------------------------------------
        andn      4,r0
        sti       r0,*+fp(1)
	.line	134
;----------------------------------------------------------------------
; 671 | WriteDPRAM( dADR_EDS_STAT, data );                                     
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      4,r0
        push      r1
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	135
;----------------------------------------------------------------------
; 672 | }
;     |                  // SetAnalMode                                        
;----------------------------------------------------------------------
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      5,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	672,000000000h,3


	.sect	 ".text"

	.global	_DefineROI
	.sym	_DefineROI,_DefineROI,32,2,0
	.func	676
;******************************************************************************
;* FUNCTION NAME: _DefineROI                                                  *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,ar0,fp,ir0,sp,st                                 *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 1 Auto + 0 SOE = 3 words          *
;******************************************************************************
_DefineROI:
	.sym	_roi,1,13,1,32
	.line	1
;----------------------------------------------------------------------
; 676 | void DefineROI( void )                                                 
; 678 | USHORT roi;                                                            
; 680 | // Read ROI Number                                                     
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      1,sp
	.line	6
;----------------------------------------------------------------------
; 681 | roi = ReadDPRAM( dADR_ROI_NUM );                                       
;----------------------------------------------------------------------
        ldiu      25,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	7
;----------------------------------------------------------------------
; 682 | if(( roi >= 1 ) && ( roi <= MAX_ROI )){         // Valid ROI Number    
;----------------------------------------------------------------------
        cmpi      0,r0
        beq       L165
;*      Branch Occurs to L165 
        cmpi      49,r0
        bhid      L165
	nop
	nop
        ldils     22,r0
;*      Branch Occurs to L165 
	.line	8
;----------------------------------------------------------------------
; 683 | ROIRec[roi].Start = ReadDPRAM( dADR_ROI_START );        // Read Startin
;     | g Channel                                                              
;----------------------------------------------------------------------
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        ldiu      @CL3,ar0
        ldiu      *+fp(1),ir0
        mpyi      5,ir0
        sti       r0,*+ar0(ir0)
	.line	9
;----------------------------------------------------------------------
; 684 | ROIRec[roi].End   = ReadDPRAM( dADR_ROI_END   );        // Read Startin
;     | g Channel                                                              
;----------------------------------------------------------------------
        ldiu      23,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        ldiu      @CL4,ar0
        ldiu      *+fp(1),ir0
        mpyi      5,ir0
        sti       r0,*+ar0(ir0)
	.line	10
;----------------------------------------------------------------------
; 685 | ROIRec[roi].Enable = 0;
;     |          // Disable the ROI                                            
;----------------------------------------------------------------------
        ldiu      *+fp(1),ir0
        ldiu      @CL5,ar0
        mpyi      5,ir0
        ldiu      0,r0
        sti       r0,*+ar0(ir0)
	.line	11
;----------------------------------------------------------------------
; 686 | ROIRec[roi].Defined = 1;
;     |          // Has just been defined                                      
;----------------------------------------------------------------------
        ldiu      @CL6,ar0
        ldiu      *+fp(1),ir0
        mpyi      5,ir0
        ldiu      1,r0
        sti       r0,*+ar0(ir0)
	.line	12
;----------------------------------------------------------------------
; 687 | ROIRec[roi].Sca = 0;                                                   
;----------------------------------------------------------------------
        ldiu      *+fp(1),ir0
        mpyi      5,ir0
        ldiu      @CL7,ar0
        ldiu      0,r0
        sti       r0,*+ar0(ir0)
	.line	13
;----------------------------------------------------------------------
; 688 | EDSRec.EDS_DEF_ROIS ++;
;     |  // Increment the Number of ROIs                                       
;----------------------------------------------------------------------
        ldiu      1,r0
        addi      @_EDSRec+17,r0        ; Unsigned
        sti       r0,@_EDSRec+17
L165:        
	.line	15
;----------------------------------------------------------------------
; 690 | }                                                               // Defi
;     | ne ROI                                                                 
;----------------------------------------------------------------------
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      3,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	690,000000000h,1


	.sect	 ".text"

	.global	_PSetROI
	.sym	_PSetROI,_PSetROI,32,2,0
	.func	694
;******************************************************************************
;* FUNCTION NAME: _PSetROI                                                    *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,ar0,fp,ir0,sp,st                                 *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 3 Auto + 0 SOE = 5 words          *
;******************************************************************************
_PSetROI:
	.sym	_i,1,4,1,32
	.sym	_roi,2,13,1,32
	.sym	_roi_enable,3,12,1,32
	.line	1
;----------------------------------------------------------------------
; 694 | void PSetROI( void )                                                   
; 696 | int i;                                                                 
; 697 | USHORT roi;                                                            
; 698 | UCHAR roi_enable;                                                      
; 700 | // Read ROI Number                                                     
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      3,sp
	.line	8
;----------------------------------------------------------------------
; 701 | roi = ReadDPRAM( dADR_ROI_NUM );                                       
;----------------------------------------------------------------------
        ldiu      25,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(2)
	.line	9
;----------------------------------------------------------------------
; 702 | roi_enable = (UCHAR)( ReadDPRAM( dADR_ROI_ENABLE ) & 0x1 );            
;----------------------------------------------------------------------
        ldiu      24,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        and       1,r0
        sti       r0,*+fp(3)
	.line	10
;----------------------------------------------------------------------
; 703 | if( roi == 0 ){                                                        
;----------------------------------------------------------------------
        ldi       *+fp(2),r0
        bned      L174
	nop
	nop
        ldieq     0,r0
;*      Branch Occurs to L174 
	.line	11
;----------------------------------------------------------------------
; 704 | for(i=0;i<=MAX_ROI;i++){                                               
;----------------------------------------------------------------------
        sti       r0,*+fp(1)
        cmpi      49,r0
        bgtd      L177
	nop
	nop
        ldigt     *+fp(2),r0
;*      Branch Occurs to L177 
	.line	12
;----------------------------------------------------------------------
; 705 | if( ROIRec[i].Defined == 1 ){
;     |          // If Defined Set Enable Flag                                 
;----------------------------------------------------------------------
        ldiu      *+fp(1),ir0
        ldiu      @CL6,ar0
L170:        
        mpyi      5,ir0
        ldiu      *+ar0(ir0),r0
        cmpi      1,r0
        bned      L172
	nop
	nop
        ldine     1,r0
;*      Branch Occurs to L172 
	.line	13
;----------------------------------------------------------------------
; 706 | ROIRec[i].Enable = roi_enable;
;     |  // Enable/Disable the ROI                                             
;----------------------------------------------------------------------
        ldiu      *+fp(1),ir0
        ldiu      @CL5,ar0
        mpyi      5,ir0
        ldiu      *+fp(3),r0
        sti       r0,*+ar0(ir0)
	.line	11
        ldiu      1,r0
L172:        
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      49,r0
        bled      L170
	nop
        ldile     *+fp(1),ir0
        ldile     @CL6,ar0
;*      Branch Occurs to L170 
        bud       L178
	nop
        ldiu      *+fp(2),r0
        push      r0
;*      Branch Occurs to L178 
L174:        
	.line	17
;----------------------------------------------------------------------
; 710 | else if(( roi >= 1 ) && ( roi <= MAX_ROI )){
;     |          // Valid ROI Number                                           
;----------------------------------------------------------------------
        ldi       *+fp(2),r0
        beqd      L177
	nop
	nop
        ldieq     *+fp(2),r0
;*      Branch Occurs to L177 
        cmpi      49,r0
        bhid      L177
	nop
	nop
        ldihi     *+fp(2),r0
;*      Branch Occurs to L177 
	.line	18
;----------------------------------------------------------------------
; 711 | ROIRec[roi].Enable = roi_enable;
;     |                  // Enable/Disable the ROI                             
;----------------------------------------------------------------------
        ldiu      *+fp(2),ir0
        ldiu      @CL5,ar0
        mpyi      5,ir0
        ldiu      *+fp(3),r0
        sti       r0,*+ar0(ir0)
	.line	20
;----------------------------------------------------------------------
; 713 | SetROISCARM( roi );                                                    
;----------------------------------------------------------------------
        ldiu      *+fp(2),r0
L177:        
        push      r0
L178:        
        call      _SetROISCARM
                                        ; Call Occurs
        subi      1,sp
	.line	21
;----------------------------------------------------------------------
; 714 | }                                                       // PSetROI     
;----------------------------------------------------------------------
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      5,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	714,000000000h,3


	.sect	 ".text"

	.global	_SetROISCARM
	.sym	_SetROISCARM,_SetROISCARM,32,2,0
	.func	718
;******************************************************************************
;* FUNCTION NAME: _SetROISCARM                                                *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,ar0,fp,ir0,sp,st                              *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 10 Auto + 0 SOE = 13 words        *
;******************************************************************************
_SetROISCARM:
	.sym	_ROI_NUM,-2,4,9,32
	.sym	_i,1,4,1,32
	.sym	_j,2,4,1,32
	.sym	_data,3,13,1,32
	.sym	_sca,4,13,1,32
	.sym	_rm_lu_mask,5,15,1,32
	.sym	_RMLU,6,13,1,32
	.sym	_Sav_Status,7,13,1,32
	.sym	_addr,8,13,1,32
	.sym	_sca_mask,9,13,1,32
	.sym	_sca_mode,10,13,1,32
	.line	1
;----------------------------------------------------------------------
; 718 | void SetROISCARM( int ROI_NUM )                                        
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      10,sp
	.line	2
;----------------------------------------------------------------------
; 720 | int i;                                                                 
; 721 | int j;                                                                 
; 722 | USHORT data;                                                           
; 723 | USHORT sca;                                                            
; 724 | ULONG rm_lu_mask;                                                      
; 725 | USHORT RMLU;                                                           
; 726 | USHORT Sav_Status;                                                     
; 727 | USHORT addr;                                                           
; 728 | USHORT sca_mask;                                                       
; 729 | USHORT sca_mode;                                                       
; 731 | // Acquistion Must be stopped                                          
;----------------------------------------------------------------------
	.line	15
;----------------------------------------------------------------------
; 732 | Sav_Status = ( ReadEDS( iADR_STAT ) >> 3 ) & 0x1;               // Read
;     |  the Time_Enable                                                       
;----------------------------------------------------------------------
        ldiu      257,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        lsh       -3,r0
        subi      1,sp
        and       1,r0
        sti       r0,*+fp(7)
	.line	16
;----------------------------------------------------------------------
; 733 | WriteEDS( iADR_TIME_STOP, 0x0 );
;     |          // Stop the Acquistion                                        
;----------------------------------------------------------------------
        ldiu      0,r1
        push      r1
        ldiu      276,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	18
;----------------------------------------------------------------------
; 735 | data = ReadEDS( iADR_CW );
;     |                  // Read the Control Word                              
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	20
;----------------------------------------------------------------------
; 737 | sca_mode = 0;                                                          
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(10)
	.line	21
;----------------------------------------------------------------------
; 738 | if(( data & SCA_EN ) == SCA_EN ){
;     |          // Is SCA Mode                                                
;----------------------------------------------------------------------
        ldiu      32,r0
        and       *+fp(3),r0
        cmpi      32,r0
        bned      L183
	nop
	nop
        ldine     16,r0
;*      Branch Occurs to L183 
	.line	22
;----------------------------------------------------------------------
; 739 | sca_mode = 1;                                                          
;----------------------------------------------------------------------
        ldiu      1,r0
        sti       r0,*+fp(10)
	.line	24
;----------------------------------------------------------------------
; 741 | data |= RAM_ACCESS;
;     |                          // Set the Memory Access Bit                  
;----------------------------------------------------------------------
        ldiu      16,r0
L183:        
        or        *+fp(3),r0
        sti       r0,*+fp(3)
	.line	25
;----------------------------------------------------------------------
; 742 | WriteEDS( iADR_CW, (USHORT)data );
;     |          // Rewrite the data                                           
; 744 | // If ROI_NUM is Zero, Calcualte based on All ROIs                     
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      258,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	28
;----------------------------------------------------------------------
; 745 | if( ROI_NUM == 0 ){                                                    
;----------------------------------------------------------------------
        ldi       *-fp(2),r0
        bned      L207
	nop
	nop
        ldieq     0,r0
;*      Branch Occurs to L207 
	.line	29
;----------------------------------------------------------------------
; 746 | for(i=0;i<=0xFFF;i++){                                                 
;----------------------------------------------------------------------
        sti       r0,*+fp(1)
        cmpi      4095,r0
        bgtd      L220
	nop
	nop
        ldigt     258,r0
;*      Branch Occurs to L220 
	.line	30
;----------------------------------------------------------------------
; 747 | rm_lu_mask = ( 1 << ( i & 0xF ) );                              // Form
;     |  the Ratemeter look-Up Mask                                            
;----------------------------------------------------------------------
        ldiu      15,r0
        ldiu      1,r1
L186:        
        and       *+fp(1),r0
        ash3      r0,r1,r0
        sti       r0,*+fp(5)
	.line	31
;----------------------------------------------------------------------
; 748 | if(( i & 0xF ) == 0 ){                                                 
;----------------------------------------------------------------------
        ldiu      15,r0
        tstb      *+fp(1),r0
        bned      L188
	nop
	nop
        ldine     0,r0
;*      Branch Occurs to L188 
	.line	32
;----------------------------------------------------------------------
; 749 | RMLU = 0;                                                              
; 751 | // Determine active ROI for only 1st 8 for Fast Mapping                
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(6)
	.line	36
;----------------------------------------------------------------------
; 753 | sca_mask= 0;                                                           
;----------------------------------------------------------------------
L188:        
        sti       r0,*+fp(9)
	.line	37
;----------------------------------------------------------------------
; 754 | for(j=1;j<=MAX_ROI;j++){                                               
;----------------------------------------------------------------------
        ldiu      1,r0
        sti       r0,*+fp(2)
        cmpi      49,r0
        bgtd      L197
	nop
	nop
        ldigt     *+fp(10),r0
;*      Branch Occurs to L197 
	.line	38
;----------------------------------------------------------------------
; 755 | if(    ( ROIRec[j].Defined == 1 )                               // Must
;     |  be defined                                                            
; 756 |         && ( ROIRec[j].Enable == 1 )                            // Must
;     |  be enabled                                                            
; 757 |         && ( i >= (int)ROIRec[j].Start )                        // Must
;     |  be >= Start                                                           
; 758 |         && ( i <= (int)ROIRec[j].End )){                        // Must
;     |  be <= End                                                             
;----------------------------------------------------------------------
        ldiu      *+fp(2),ir0
        ldiu      @CL6,ar0
L190:        
        mpyi      5,ir0
        ldiu      *+ar0(ir0),r0
        cmpi      1,r0
        bned      L195
	nop
	nop
        ldine     1,r0
;*      Branch Occurs to L195 
        ldiu      *+fp(2),ir0
        ldiu      @CL5,ar0
        mpyi      5,ir0
        ldiu      *+ar0(ir0),r0
        cmpi      1,r0
        bned      L195
	nop
	nop
        ldine     1,r0
;*      Branch Occurs to L195 
        ldiu      *+fp(2),ir0
        ldiu      @CL3,ar0
        mpyi      5,ir0
        ldiu      *+fp(1),r0
        cmpi3     *+ar0(ir0),r0
        bltd      L195
	nop
	nop
        ldilt     1,r0
;*      Branch Occurs to L195 
        ldiu      *+fp(2),ir0
        ldiu      @CL4,ar0
        mpyi      5,ir0
        cmpi3     *+ar0(ir0),r0
        bgtd      L195
	nop
	nop
        ldigt     1,r0
;*      Branch Occurs to L195 
	.line	42
;----------------------------------------------------------------------
; 759 | sca_mask |= ROIRec[j].Sca;                      // Or in th SCA Value  
;----------------------------------------------------------------------
        ldiu      *+fp(2),ir0
        ldiu      @CL7,ar0
        mpyi      5,ir0
        ldiu      *+ar0(ir0),r0
        or        *+fp(9),r0
        sti       r0,*+fp(9)
	.line	43
;----------------------------------------------------------------------
; 760 | RMLU |= rm_lu_mask;                                     // Set Appropri
;     | ate Bit                                                                
; 761 | }
;     |          // if ROI defined                                             
; 762 | }
;     |                  // for j                                              
; 764 | //----- SCA -----------------------------------------------------------
;     | --                                                                     
;----------------------------------------------------------------------
        ldiu      *+fp(5),r0
        or        *+fp(6),r0
        sti       r0,*+fp(6)
	.line	37
        ldiu      1,r0
L195:        
        addi      *+fp(2),r0
        sti       r0,*+fp(2)
        cmpi      49,r0
        bled      L190
	nop
        ldile     *+fp(2),ir0
        ldile     @CL6,ar0
;*      Branch Occurs to L190 
	.line	48
;----------------------------------------------------------------------
; 765 | if( sca_mode == 1 ){                                                   
;----------------------------------------------------------------------
        ldiu      *+fp(10),r0
L197:        
        cmpi      1,r0
        bned      L203
	nop
	nop
        ldine     15,r0
;*      Branch Occurs to L203 
	.line	49
;----------------------------------------------------------------------
; 766 | if(( i & 0x1 ) == 0 ){                                                 
;----------------------------------------------------------------------
        ldiu      1,r0
        tstb      *+fp(1),r0
        bned      L201
	nop
	nop
        ldine     255,r0
;*      Branch Occurs to L201 
	.line	50
;----------------------------------------------------------------------
; 767 | sca &= 0xFF00;
;     |  // Clear the Lower 8 bits                                             
; 768 | } else {                                                               
;----------------------------------------------------------------------
        bud       L202
        ldiu      *+fp(4),r0
        and       65280,r0
        sti       r0,*+fp(4)
;*      Branch Occurs to L202 
	.line	52
;----------------------------------------------------------------------
; 769 | sca &= 0x00FF;
;     |  // clear the upper 8 bits                                             
;----------------------------------------------------------------------
L201:        
        and       *+fp(4),r0
        sti       r0,*+fp(4)
	.line	53
;----------------------------------------------------------------------
; 770 | sca_mask = sca_mask << 8 ;                                      // Shif
;     | t SCA Mask up by 8                                                     
;----------------------------------------------------------------------
        ldiu      *+fp(9),r0
        ash       8,r0
        sti       r0,*+fp(9)
L202:        
	.line	55
;----------------------------------------------------------------------
; 772 | sca |= sca_mask;
;     |          // OR in SCA Mask                                             
; 773 | //                              addr = i >> 1;
;     |                                          // Form the SCA Address       
;----------------------------------------------------------------------
        ldiu      *+fp(9),r0
        or        *+fp(4),r0
        sti       r0,*+fp(4)
	.line	57
;----------------------------------------------------------------------
; 774 | addr = i;
;     |                  // Form the SCA Address - Ver 4x23                    
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
        sti       r0,*+fp(8)
	.line	58
;----------------------------------------------------------------------
; 775 | WriteEDS( iADR_RAMA, addr );
;     |  // Write the SCA Address                                              
;----------------------------------------------------------------------
        ldiu      289,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	59
;----------------------------------------------------------------------
; 776 | WriteEDS( iADR_SCA_LU, sca );
;     |  // Write the SCA Data                                                 
; 778 | //----- SCA --- -------------------------------------------------------
;     | --                                                                     
; 780 | //----- ROI DEF -------------------------------------------------------
;     | --                                                                     
;----------------------------------------------------------------------
        ldiu      *+fp(4),r0
        ldiu      291,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	64
;----------------------------------------------------------------------
; 781 | if(( i & 0xF ) == 0xF ){                                               
;----------------------------------------------------------------------
        ldiu      15,r0
L203:        
        and       *+fp(1),r0
        cmpi      15,r0
        bned      L205
	nop
	nop
        ldine     1,r0
;*      Branch Occurs to L205 
	.line	65
;----------------------------------------------------------------------
; 782 | addr = i >> 4;
;     |  // Form the ROI Look-UP Address                                       
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
        ash       -4,r0
        sti       r0,*+fp(8)
	.line	66
;----------------------------------------------------------------------
; 783 | WriteEDS( iADR_RAMA, addr );                                    // Writ
;     | e the address                                                          
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      289,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	67
;----------------------------------------------------------------------
; 784 | WriteEDS( iADR_ROI_LU, RMLU );                          // Write the da
;     | ta                                                                     
; 786 | //----- ROI DEF -------------------------------------------------------
;     | --                                                                     
; 787 | }
;     |                          // for i...                                   
;----------------------------------------------------------------------
        ldiu      293,r1
        ldiu      *+fp(6),r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	29
        ldiu      1,r0
L205:        
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      4095,r0
        bled      L186
	nop
        ldile     15,r0
        ldile     1,r1
;*      Branch Occurs to L186 
        bud       L221
	nop
        ldiu      258,r0
        push      r0
;*      Branch Occurs to L221 
L207:        
	.line	71
;----------------------------------------------------------------------
; 788 | } else if(( ROI_NUM >= 1 ) && ( ROI_NUM <= MAX_ROI )){                 
; 789 |         // Only Calculate based on ROI[ROI_NUM]                        
;----------------------------------------------------------------------
        ldi       *-fp(2),r0
        bled      L220
	nop
	nop
        ldile     258,r0
;*      Branch Occurs to L220 
        cmpi      49,r0
        bgtd      L220
	nop
	nop
        ldigt     258,r0
;*      Branch Occurs to L220 
	.line	73
;----------------------------------------------------------------------
; 790 | if(( ROIRec[ROI_NUM].Defined == 1 ) && ( ROIRec[ROI_NUM].Enable == 1 ))
;     | {                                                                      
;----------------------------------------------------------------------
        ldiu      r0,ir0
        ldiu      @CL6,ar0
        mpyi      5,ir0
        ldiu      *+ar0(ir0),r0
        cmpi      1,r0
        bned      L220
	nop
	nop
        ldine     258,r0
;*      Branch Occurs to L220 
        ldiu      *-fp(2),ir0
        ldiu      @CL5,ar0
        mpyi      5,ir0
        ldiu      *+ar0(ir0),r0
        cmpi      1,r0
        bned      L220
	nop
	nop
        ldine     258,r0
;*      Branch Occurs to L220 
	.line	74
;----------------------------------------------------------------------
; 791 | for(i=ROIRec[ROI_NUM].Start;i<=ROIRec[ROI_NUM].End;i++){               
; 793 |         //----- Update the ROI Look-Up Table --------------------------
;     | -----------------                                                      
;----------------------------------------------------------------------
        ldiu      *-fp(2),ir0
        ldiu      @CL3,ar0
        bud       L218
        mpyi      5,ir0
        ldiu      *+ar0(ir0),r0
        sti       r0,*+fp(1)
;*      Branch Occurs to L218 
	.line	77
;----------------------------------------------------------------------
; 794 | addr = ((USHORT) i ) >> 4;                                      // form
;     |  the address                                                           
;----------------------------------------------------------------------
L213:        
        lsh       -4,r0
        sti       r0,*+fp(8)
	.line	78
;----------------------------------------------------------------------
; 795 | WriteEDS( iADR_RAMA, addr );                                    // writ
;     | e the address                                                          
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      289,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	79
;----------------------------------------------------------------------
; 796 | rm_lu_mask = 1 << ( i & 0xF );                          // generate the
;     |  mask bit                                                              
;----------------------------------------------------------------------
        ldiu      15,r0
        ldiu      1,r1
        and       *+fp(1),r0
        ash3      r0,r1,r0
        sti       r0,*+fp(5)
	.line	80
;----------------------------------------------------------------------
; 797 | data = ReadEDS( iADR_ROI_LU );                          // read the dat
;     | a                                                                      
;----------------------------------------------------------------------
        ldiu      293,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	81
;----------------------------------------------------------------------
; 798 | data |= rm_lu_mask;
;     |  // Set the Appropriate Bit                                            
;----------------------------------------------------------------------
        ldiu      *+fp(5),r0
        or        *+fp(3),r0
        sti       r0,*+fp(3)
	.line	82
;----------------------------------------------------------------------
; 799 | WriteEDS( iADR_ROI_LU, (USHORT)data );                  // rewrite the
;     | data                                                                   
; 800 | //----- EDI-III -------------------------------------------------------
;     | -----                                                                  
; 801 | //----- Update the ROI Look-Up Table ----------------------------------
;     | ---------                                                              
; 803 | //----- Update the SCA Look-Up Table ----------------------------------
;     | ---------                                                              
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      293,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	87
;----------------------------------------------------------------------
; 804 | if( sca_mode == 1 ){                                                   
; 805 | //                                      addr = i >> 1;
;     |                                                  // Form the SCA Addres
;     | s                                                                      
;----------------------------------------------------------------------
        ldiu      *+fp(10),r0
        cmpi      1,r0
        bned      L217
	nop
	nop
        ldine     1,r0
;*      Branch Occurs to L217 
	.line	89
;----------------------------------------------------------------------
; 806 | addr = i;
;     |                  // Form the SCA Address - Ver 4x23                    
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
        sti       r0,*+fp(8)
	.line	90
;----------------------------------------------------------------------
; 807 | WriteEDS( iADR_RAMA, addr );                            // write the ad
;     | dress                                                                  
;----------------------------------------------------------------------
        ldiu      289,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	91
;----------------------------------------------------------------------
; 808 | sca_mask = ROIRec[ROI_NUM].Sca;                         // Get New SCA
;     | Data                                                                   
;----------------------------------------------------------------------
        ldiu      @CL7,ar0
        ldiu      *-fp(2),ir0
        mpyi      5,ir0
        ldiu      *+ar0(ir0),r0
        sti       r0,*+fp(9)
	.line	93
;----------------------------------------------------------------------
; 810 | sca = ReadEDS( iADR_SCA_LU );                           // Read the Dat
;     | a                                                                      
; 811 |         // Don't Clear the bits of the Read SCA                        
;----------------------------------------------------------------------
        ldiu      291,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(4)
	.line	95
;----------------------------------------------------------------------
; 812 | if(( i & 0x1 ) == 1 ){                                                 
;----------------------------------------------------------------------
        ldiu      1,r0
        and       *+fp(1),r0
        cmpi      1,r0
        bned      L216
	nop
	nop
        ldine     *+fp(9),r0
;*      Branch Occurs to L216 
	.line	96
;----------------------------------------------------------------------
; 813 | sca_mask = sca_mask << 8;                               // Shift SCA Ma
;     | sk up by 8                                                             
;----------------------------------------------------------------------
        ldiu      *+fp(9),r0
        ash       8,r0
        sti       r0,*+fp(9)
	.line	98
;----------------------------------------------------------------------
; 815 | sca |= sca_mask;                                                       
;----------------------------------------------------------------------
L216:        
        or        *+fp(4),r0
        sti       r0,*+fp(4)
	.line	99
;----------------------------------------------------------------------
; 816 | WriteEDS( iADR_SCA_LU, sca );                                          
; 818 | //----- Update the SCA Look-Up Table ----------------------------------
;     | ---------                                                              
; 819 | }
;     |                                  // for i...                           
; 820 | }
;     |                                          // if defined...              
; 821 | }
;     |                                                  // ROI NUM > 0        
; 822 | // Turn-Off Auto-Address Increment                                     
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      291,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	74
        ldiu      1,r0
L217:        
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
L218:        
        ldiu      *-fp(2),ir0
        ldiu      @CL4,ar0
        mpyi      5,ir0
        ldiu      *+fp(1),r0
        cmpi3     *+ar0(ir0),r0
        blsd      L213
	nop
	nop
        ldils     *+fp(1),r0
;*      Branch Occurs to L213 
	.line	106
;----------------------------------------------------------------------
; 823 | data = ReadEDS( iADR_CW );                                             
;----------------------------------------------------------------------
        ldiu      258,r0
L220:        
        push      r0
L221:        
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	107
;----------------------------------------------------------------------
; 824 | data &= ~RAM_ACCESS;
;     |                          // Disable the Memory Access Bit              
;----------------------------------------------------------------------
        andn      16,r0
        sti       r0,*+fp(3)
	.line	108
;----------------------------------------------------------------------
; 825 | WriteEDS( iADR_CW, (USHORT)data );                                     
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      258,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	109
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      12,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	826,000000000h,10


	.sect	 ".text"

	.global	_ClrROI
	.sym	_ClrROI,_ClrROI,32,2,0
	.func	830
;******************************************************************************
;* FUNCTION NAME: _ClrROI                                                     *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,r2,r3,r4,r5,ar0,fp,ar4,ir0,sp,st,rs,re,rc     *
;*   Regs Saved         : r4,r5,ar4                                           *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 5 Auto + 3 SOE = 10 words         *
;******************************************************************************
_ClrROI:
	.sym	_i,1,4,1,32
	.sym	_roi,2,13,1,32
	.sym	_Sav_Status,3,13,1,32
	.sym	_data,4,13,1,32
	.sym	_sca_mode,5,13,1,32
	.line	1
;----------------------------------------------------------------------
; 830 | void ClrROI( void )                                                    
; 832 | int i;                                                                 
; 833 | USHORT roi;                                                            
; 834 | USHORT Sav_Status;                                                     
; 835 | USHORT data;                                                           
; 836 | USHORT sca_mode;                                                       
; 839 | // Acquistion Must be stopped                                          
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      5,sp
        push      r4
        push      r5
        push      ar4
	.line	11
;----------------------------------------------------------------------
; 840 | Sav_Status = ( ReadEDS( iADR_STAT ) >> 3 ) & 0x1;               // Read
;     |  the Time_Enable                                                       
;----------------------------------------------------------------------
        ldiu      257,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        lsh       -3,r0
        subi      1,sp
        and       1,r0
        sti       r0,*+fp(3)
	.line	12
;----------------------------------------------------------------------
; 841 | WriteEDS( iADR_TIME_STOP, 0x0 );
;     |                          // Stop the Acquistion                        
; 843 | // Read ROI Number                                                     
;----------------------------------------------------------------------
        ldiu      0,r1
        ldiu      276,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	15
;----------------------------------------------------------------------
; 844 | roi = ReadDPRAM( dADR_ROI_NUM );                                       
;----------------------------------------------------------------------
        ldiu      25,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(2)
	.line	16
;----------------------------------------------------------------------
; 845 | if( roi == 0 ){                                                        
;----------------------------------------------------------------------
        cmpi      0,r0
        bned      L243
	nop
	nop
        ldieq     0,r0
;*      Branch Occurs to L243 
	.line	17
;----------------------------------------------------------------------
; 846 | for(i=0;i<=MAX_ROI;i++){                                               
;----------------------------------------------------------------------
        sti       r0,*+fp(1)
        ldiu      0,r1
        ldiu      0,re
        ldiu      0,rs
        ldiu      0,r3
        ldiu      0,r2
        cmpi      49,r0
        bgtd      L229
        ldiu      0,rc
	nop
        ldigt     0,r0
;*      Branch Occurs to L229 
	.line	18
;----------------------------------------------------------------------
; 847 | ROIRec[i].Enable = 0;                           // Disable ROI         
;----------------------------------------------------------------------
        ldiu      *+fp(1),ir0
        ldiu      @CL5,ar0
L227:        
        mpyi      5,ir0
        sti       r2,*+ar0(ir0)
	.line	19
;----------------------------------------------------------------------
; 848 | ROIRec[i].Start = 0;                            // Set Start Channel Nu
;     | mber to 0                                                              
;----------------------------------------------------------------------
        ldiu      *+fp(1),ir0
        ldiu      @CL3,ar0
        mpyi      5,ir0
        sti       r3,*+ar0(ir0)
	.line	20
;----------------------------------------------------------------------
; 849 | ROIRec[i].End   = 0;                            // Set End Channel Numb
;     | er to 0                                                                
;----------------------------------------------------------------------
        ldiu      @CL4,ar0
        ldiu      *+fp(1),ir0
        mpyi      5,ir0
        sti       rs,*+ar0(ir0)
	.line	21
;----------------------------------------------------------------------
; 850 | ROIRec[i].Defined = 0;                          // Clear Defined Flag  
;----------------------------------------------------------------------
        ldiu      @CL6,ar0
        ldiu      *+fp(1),ir0
        mpyi      5,ir0
        sti       re,*+ar0(ir0)
	.line	22
;----------------------------------------------------------------------
; 851 | ROIRec[i].Sca = 0;                                      // Clear SCA Ma
;     | sk                                                                     
;----------------------------------------------------------------------
        ldiu      @CL7,ar0
        ldiu      *+fp(1),ir0
        mpyi      5,ir0
        sti       r1,*+ar0(ir0)
	.line	23
;----------------------------------------------------------------------
; 852 | EDSRec.EDS_DEF_ROIS = 0;                                // Clear NUmber
;     |  of Defined ROIS                                                       
;----------------------------------------------------------------------
        sti       rc,@_EDSRec+17
	.line	17
        ldiu      1,r0
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      49,r0
        bled      L227
        ldile     *+fp(1),ir0
        ldile     @CL5,ar0
        ldile     *+fp(1),ir0
;*      Branch Occurs to L227 
	.line	26
;----------------------------------------------------------------------
; 855 | sca_mode = 0;                                                          
;----------------------------------------------------------------------
        ldiu      0,r0
L229:        
        sti       r0,*+fp(5)
	.line	27
;----------------------------------------------------------------------
; 856 | data = ReadEDS( iADR_CW );                                             
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(4)
	.line	28
;----------------------------------------------------------------------
; 857 | data |= RAM_ACCESS;                                             // Enab
;     | le the Memory Access Bit                                               
;----------------------------------------------------------------------
        ldiu      16,r0
        or        *+fp(4),r0
        sti       r0,*+fp(4)
	.line	29
;----------------------------------------------------------------------
; 858 | WriteEDS( iADR_CW, data );                                             
;----------------------------------------------------------------------
        ldiu      r0,r1
        push      r1
        ldiu      258,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	30
;----------------------------------------------------------------------
; 859 | if(( data & SCA_EN ) == SCA_EN ){
;     |          // Is SCA Mode                                                
;----------------------------------------------------------------------
        ldiu      32,r0
        and       *+fp(4),r0
        cmpi      32,r0
        bned      L231
	nop
	nop
        ldine     0,r0
;*      Branch Occurs to L231 
	.line	31
;----------------------------------------------------------------------
; 860 | sca_mode = 1;                                                          
;----------------------------------------------------------------------
        ldiu      1,r0
        sti       r0,*+fp(5)
	.line	34
;----------------------------------------------------------------------
; 863 | for(i=0;i<=0xFF;i++){                                                  
;----------------------------------------------------------------------
        ldiu      0,r0
L231:        
        sti       r0,*+fp(1)
        cmpi      255,r0
        bgtd      L237
	nop
	nop
        ldigt     *+fp(5),r0
;*      Branch Occurs to L237 
	.line	35
;----------------------------------------------------------------------
; 864 | WriteEDS( iADR_RAMA, (USHORT)i );                                      
;----------------------------------------------------------------------
        ldiu      *+fp(1),r1
        ldiu      289,r0
L233:        
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	36
;----------------------------------------------------------------------
; 865 | WriteEDS( iADR_ROI_LU, 0x0 );                                          
;----------------------------------------------------------------------
        ldiu      0,r0
        ldiu      293,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	37
;----------------------------------------------------------------------
; 866 | if( sca_mode == 1 ){                                                   
;----------------------------------------------------------------------
        ldiu      *+fp(5),r0
        cmpi      1,r0
        bned      L235
	nop
	nop
        ldine     1,r0
;*      Branch Occurs to L235 
	.line	38
;----------------------------------------------------------------------
; 867 | WriteEDS( iADR_SCA_LU, 0x0 );                                          
;----------------------------------------------------------------------
        ldiu      0,r1
        ldiu      291,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	34
        ldiu      1,r0
L235:        
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      255,r0
        bled      L233
	nop
        ldile     *+fp(1),r1
        ldile     289,r0
;*      Branch Occurs to L233 
	.line	42
;----------------------------------------------------------------------
; 871 | if( sca_mode == 1 ){                                                   
;----------------------------------------------------------------------
        ldiu      *+fp(5),r0
L237:        
        cmpi      1,r0
        bned      L242
	nop
	nop
        ldine     258,r0
;*      Branch Occurs to L242 
	.line	43
;----------------------------------------------------------------------
; 872 | for(i=0x100;i<=0x7FF;i++){                                             
;----------------------------------------------------------------------
        ldiu      256,r0
        sti       r0,*+fp(1)
        ldiu      0,r5
        ldiu      289,r4
        cmpi      2047,r0
        bgtd      L242
        ldiu      291,ar4
	nop
        ldigt     258,r0
;*      Branch Occurs to L242 
	.line	44
;----------------------------------------------------------------------
; 873 | WriteEDS( iADR_RAMA, (USHORT)i );                                      
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
L240:        
        push      r0
        push      r4
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	45
;----------------------------------------------------------------------
; 874 | WriteEDS( iADR_SCA_LU, 0x0 );                                          
; 878 | // Turn-Off Auto-Address Increment                                     
;----------------------------------------------------------------------
        push      r5
        push      ar4
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	43
        ldiu      1,r0
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      2047,r0
        bled      L240
        ldile     *+fp(1),r0
	nop
        ldile     *+fp(1),r0
;*      Branch Occurs to L240 
	.line	50
;----------------------------------------------------------------------
; 879 | data = ReadEDS( iADR_CW );                                             
;----------------------------------------------------------------------
        ldiu      258,r0
L242:        
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(4)
	.line	51
;----------------------------------------------------------------------
; 880 | data &= ~RAM_ACCESS;
;     |                          // Enable the Memory Access Bit               
;----------------------------------------------------------------------
        andn      16,r0
        sti       r0,*+fp(4)
	.line	52
;----------------------------------------------------------------------
; 881 | WriteEDS( iADR_CW, data );                                             
;----------------------------------------------------------------------
        ldiu      258,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        bud       L247
        subi      2,sp
        pop       ar4
        ldiu      *-fp(1),bk
;*      Branch Occurs to L247 
L243:        
	.line	54
;----------------------------------------------------------------------
; 883 | else if(( roi >= 1 ) && ( roi <= MAX_ROI )){    // Valid ROI Number    
;----------------------------------------------------------------------
        ldi       *+fp(2),r0
        beq       L246
;*      Branch Occurs to L246 
        cmpi      49,r0
        bhid      L246
	nop
        ldils     *+fp(2),ir0
        ldils     @CL5,ar0
;*      Branch Occurs to L246 
	.line	55
;----------------------------------------------------------------------
; 884 | ROIRec[roi].Enable = 0;                                         // Disa
;     | ble ROI                                                                
;----------------------------------------------------------------------
        mpyi      5,ir0
        ldiu      0,r0
        sti       r0,*+ar0(ir0)
	.line	56
;----------------------------------------------------------------------
; 885 | ROIRec[roi].Start = 0;                                          // Set
;     | Start Channel Number to 0                                              
;----------------------------------------------------------------------
        ldiu      *+fp(2),ir0
        ldiu      @CL3,ar0
        mpyi      5,ir0
        sti       r0,*+ar0(ir0)
	.line	57
;----------------------------------------------------------------------
; 886 | ROIRec[roi].End   = 0;                                          // Set
;     | End Channel Number to 0                                                
;----------------------------------------------------------------------
        ldiu      @CL4,ar0
        ldiu      *+fp(2),ir0
        mpyi      5,ir0
        sti       r0,*+ar0(ir0)
	.line	58
;----------------------------------------------------------------------
; 887 | ROIRec[roi].Defined = 0;                                        // Clea
;     | r Defined Flag                                                         
;----------------------------------------------------------------------
        ldiu      @CL6,ar0
        ldiu      *+fp(2),ir0
        mpyi      5,ir0
        sti       r0,*+ar0(ir0)
	.line	59
;----------------------------------------------------------------------
; 888 | ROIRec[roi].Sca = 0;                                            // Clea
;     | r SCA Mask                                                             
;----------------------------------------------------------------------
        ldiu      @CL7,ar0
        ldiu      *+fp(2),ir0
        mpyi      5,ir0
        sti       r0,*+ar0(ir0)
	.line	60
;----------------------------------------------------------------------
; 889 | EDSRec.EDS_DEF_ROIS --;                                         // Decr
;     | ement the Defined Number of ROIS                                       
;----------------------------------------------------------------------
        ldiu      1,r0
        subri     @_EDSRec+17,r0        ; Unsigned
        sti       r0,@_EDSRec+17
	.line	61
;----------------------------------------------------------------------
; 890 | SetROISCARM( 0 );
;     |  // Rebuild ROI/SCA/Ratemeter Table                                    
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        call      _SetROISCARM
                                        ; Call Occurs
        subi      1,sp
L246:        
	.line	63
        pop       ar4
        ldiu      *-fp(1),bk
L247:        
        pop       r5
        ldiu      *fp,fp
        pop       r4
        subi      7,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	892,000001030h,5


	.sect	 ".text"

	.global	_DefineSCA
	.sym	_DefineSCA,_DefineSCA,32,2,0
	.func	896
;******************************************************************************
;* FUNCTION NAME: _DefineSCA                                                  *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,ar0,fp,ir0,sp,st                                 *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 2 Auto + 0 SOE = 4 words          *
;******************************************************************************
_DefineSCA:
	.sym	_roi,1,13,1,32
	.sym	_sca,2,13,1,32
	.line	1
;----------------------------------------------------------------------
; 896 | void DefineSCA( void )                                                 
; 898 | USHORT roi;                                                            
; 899 | USHORT sca;                                                            
; 901 | // Read ROI Number                                                     
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      2,sp
	.line	7
;----------------------------------------------------------------------
; 902 | roi = ReadDPRAM( dADR_ROI_NUM );                                       
;----------------------------------------------------------------------
        ldiu      25,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	8
;----------------------------------------------------------------------
; 903 | sca     = ReadDPRAM( dADR_SCA_MASK ) & 0xFF;                           
;----------------------------------------------------------------------
        ldiu      27,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        and       255,r0
        sti       r0,*+fp(2)
	.line	9
;----------------------------------------------------------------------
; 904 | if(( roi >= 1 ) && ( roi <= MAX_ROI )){         // Valid ROI Number    
;----------------------------------------------------------------------
        ldi       *+fp(1),r0
        beq       L252
;*      Branch Occurs to L252 
        cmpi      49,r0
        bhid      L252
	nop
        ldils     *+fp(1),ir0
        ldils     @CL7,ar0
;*      Branch Occurs to L252 
	.line	10
;----------------------------------------------------------------------
; 905 | ROIRec[roi].Sca = sca;                                                 
;----------------------------------------------------------------------
        mpyi      5,ir0
        ldiu      *+fp(2),r0
        sti       r0,*+ar0(ir0)
	.line	11
;----------------------------------------------------------------------
; 906 | SetROISCARM( roi );                                                    
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
        push      r0
        call      _SetROISCARM
                                        ; Call Occurs
        subi      1,sp
L252:        
	.line	13
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      4,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	908,000000000h,2


	.sect	 ".text"

	.global	_Set_RTEM_Range
	.sym	_Set_RTEM_Range,_Set_RTEM_Range,32,2,0
	.func	912
;******************************************************************************
;* FUNCTION NAME: _Set_RTEM_Range                                             *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : f0,r0,f1,fp,sp,st                                   *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 4 Auto + 0 SOE = 7 words          *
;******************************************************************************
_Set_RTEM_Range:
	.sym	_cnts,-2,15,9,32
	.sym	_factor,1,6,1,32
	.sym	_addr,2,13,1,32
	.sym	_tmp,3,15,1,32
	.sym	_i,4,4,1,32
	.line	1
;----------------------------------------------------------------------
; 912 | void Set_RTEM_Range( ULONG cnts )                                      
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      4,sp
	.line	2
;----------------------------------------------------------------------
; 914 | float factor;                                                          
; 915 | USHORT addr;                                                           
; 916 | ULONG tmp;                                                             
; 917 | int i;                                                                 
;----------------------------------------------------------------------
	.line	8
;----------------------------------------------------------------------
; 919 | for(i=0;i<=5;i++){                                                     
; 920 |         switch(i){                                                     
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(4)
        cmpi      5,r0
        bgt       L276
;*      Branch Occurs to L276 
        bu        L269
;*      Branch Occurs to L269 
	.line	10
;----------------------------------------------------------------------
; 921 | case 0 : factor = 0.025f; addr = iADR_RTEM_CNT_10_L; break;            
;----------------------------------------------------------------------
L257:        
        bud       L275
        stf       f0,*+fp(1)
        ldiu      355,r0
        sti       r0,*+fp(2)
;*      Branch Occurs to L275 
	.line	11
;----------------------------------------------------------------------
; 922 | case 1 : factor = 0.050f; addr = iADR_RTEM_CNT_25_L; break;            
;----------------------------------------------------------------------
L259:        
        bud       L275
        stf       f0,*+fp(1)
        ldiu      357,r0
        sti       r0,*+fp(2)
;*      Branch Occurs to L275 
	.line	12
;----------------------------------------------------------------------
; 923 | case 2 : factor = 0.90f;  addr = iADR_RTEM_CNT_75_L; break;            
;----------------------------------------------------------------------
L261:        
        bud       L275
        stf       f0,*+fp(1)
        ldiu      359,r0
        sti       r0,*+fp(2)
;*      Branch Occurs to L275 
	.line	13
;----------------------------------------------------------------------
; 924 | case 3 : factor = 0.95f;  addr = iADR_RTEM_CNT_90_L; break;            
;----------------------------------------------------------------------
L263:        
        bud       L275
        stf       f0,*+fp(1)
        ldiu      361,r0
        sti       r0,*+fp(2)
;*      Branch Occurs to L275 
	.line	14
;----------------------------------------------------------------------
; 925 | case 4 : factor = 1.00f;  addr = iADR_RTEM_CNT_100_L; break;           
;----------------------------------------------------------------------
L265:        
        bud       L275
        stf       f0,*+fp(1)
        ldiu      363,r0
        sti       r0,*+fp(2)
;*      Branch Occurs to L275 
	.line	15
;----------------------------------------------------------------------
; 926 | case 5 : factor = 1.10f;  addr = iADR_RTEM_CNT_110_L; ; break;         
;----------------------------------------------------------------------
L267:        
        bud       L275
        stf       f0,*+fp(1)
        ldiu      365,r0
        sti       r0,*+fp(2)
;*      Branch Occurs to L275 
	.line	16
;----------------------------------------------------------------------
; 927 | default : break;                                                       
;----------------------------------------------------------------------
L269:        
	.line	9
        ldi       *+fp(4),r0
        beqd      L257
	nop
	nop
        ldfeq     @CL33,f0
;*      Branch Occurs to L257 
        cmpi      1,r0
        beqd      L259
	nop
	nop
        ldfeq     @CL34,f0
;*      Branch Occurs to L259 
        cmpi      2,r0
        beqd      L261
	nop
	nop
        ldfeq     @CL35,f0
;*      Branch Occurs to L261 
        cmpi      3,r0
        beqd      L263
	nop
	nop
        ldfeq     @CL36,f0
;*      Branch Occurs to L263 
        cmpi      4,r0
        beqd      L265
	nop
	nop
        ldfeq     1.0000000000e+00,f0
;*      Branch Occurs to L265 
        cmpi      5,r0
        beqd      L267
	nop
	nop
        ldfeq     @CL37,f0
;*      Branch Occurs to L267 
L275:        
	.line	18
;----------------------------------------------------------------------
; 929 | tmp = (ULONG)((float)factor * (float)cnts );                           
;----------------------------------------------------------------------
        float     *-fp(2),f0
        ldflt     @CL28,f1
        ldfge     0.0000000000e+00,f1
        addf3     f0,f1,f0
        mpyf      *+fp(1),f0
        cmpf      @CL30,f0
        ldflt     0.0000000000e+00,f1
        ldfge     @CL28,f1
        subrf     f0,f1
        fix       f1,r0
        sti       r0,*+fp(3)
	.line	19
;----------------------------------------------------------------------
; 930 | WriteDEDS( addr, tmp );                                                
;----------------------------------------------------------------------
        push      r0
        ldiu      *+fp(2),r0
        push      r0
        call      _WriteDEDS
                                        ; Call Occurs
        subi      2,sp
	.line	8
        ldiu      1,r0
        addi      *+fp(4),r0
        sti       r0,*+fp(4)
        cmpi      5,r0
        ble       L269
;*      Branch Occurs to L269 
L276:        
	.line	21
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	932,000000000h,4


	.sect	 ".text"

	.global	_Get_Rtem_State
	.sym	_Get_Rtem_State,_Get_Rtem_State,45,2,0
	.func	936
;******************************************************************************
;* FUNCTION NAME: _Get_Rtem_State                                             *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,fp,sp                                            *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 1 Auto + 0 SOE = 3 words          *
;******************************************************************************
_Get_Rtem_State:
	.sym	_tmp,1,13,1,32
	.line	1
;----------------------------------------------------------------------
; 936 | USHORT Get_Rtem_State()                                                
; 938 | USHORT tmp;                                                            
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      1,sp
	.line	4
;----------------------------------------------------------------------
; 939 | tmp = ( ReadEDS( iADR_STAT ) >> 8 ) & 0xF;                             
;----------------------------------------------------------------------
        ldiu      257,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        lsh       -8,r0
        subi      1,sp
        and       15,r0
        sti       r0,*+fp(1)
	.line	5
;----------------------------------------------------------------------
; 940 | return( tmp );                                                         
;----------------------------------------------------------------------
	.line	6
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      3,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	941,000000000h,1


	.sect	 ".text"

	.global	_Auto_Disc
	.sym	_Auto_Disc,_Auto_Disc,32,2,0
	.func	1044
;******************************************************************************
;* FUNCTION NAME: _Auto_Disc                                                  *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : f0,r0,f1,r1,ar0,fp,ir0,sp,st                        *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 7 Auto + 0 SOE = 9 words          *
;******************************************************************************
_Auto_Disc:
	.bss	_wait_cnt$1,1
	.sym	_wait_cnt,_wait_cnt$1,4,3,32
	.bss	_rps_iter$2,1
	.sym	_rps_iter,_rps_iter$2,4,3,32
	.sym	_data,1,13,1,32
	.sym	_cps,2,13,1,32
	.sym	_time,3,6,1,32
	.sym	_maxcps,4,13,1,32
	.sym	_mask,5,13,1,32
	.sym	_addr,6,13,1,32
	.sym	_i,7,4,1,32
	.line	1
;----------------------------------------------------------------------
; 1044 | void Auto_Disc( void )                                                 
; 1046 | USHORT data;                                                           
; 1047 | USHORT cps;                                                            
; 1048 | float time;                                                            
; 1049 | USHORT maxcps;                                                         
; 1050 | USHORT mask;                                                           
; 1051 | USHORT addr;                                                           
; 1052 | // USHORT rps;                                                         
; 1053 | static wait_cnt;                                                       
; 1054 | static rps_iter;                                                       
; 1055 | int i;                                                                 
; 1057 | switch( EDSRec.ADisc ) {                                               
; 1058 |         case 0 :        // Do nothing                                  
;----------------------------------------------------------------------
        bud       L327
        push      fp
        ldiu      sp,fp
        addi      7,sp
;*      Branch Occurs to L327 
	.line	16
;----------------------------------------------------------------------
; 1059 | break;                                                                 
; 1060 | case 1 :                                                               
;----------------------------------------------------------------------
L283:        
	.line	18
;----------------------------------------------------------------------
; 1061 | EDSRec.ASHBit = 11;                                                    
;----------------------------------------------------------------------
        ldiu      11,r0
        sti       r0,@_EDSRec+26
	.line	19
;----------------------------------------------------------------------
; 1062 | WriteEDS( iADR_DISC_EN, 0x1F );
;     |  // Enable all Discriminators                                          
; 1064 | // Set All Discriminator to a particular value (1/2 Scale )            
;----------------------------------------------------------------------
        ldiu      31,r1
        push      r1
        ldiu      299,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	22
;----------------------------------------------------------------------
; 1065 | data = 1 << EDSRec.ASHBit;                                             
;----------------------------------------------------------------------
        ldiu      1,r0
        ash       @_EDSRec+26,r0
        sti       r0,*+fp(1)
	.line	23
;----------------------------------------------------------------------
; 1066 | for(i=0;i<=4;i++){                                                     
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(7)
        cmpi      4,r0
        bgtd      L287
	nop
        ldigt     @_EDSRec+27,r1
        ldigt     271,r0
;*      Branch Occurs to L287 
	.line	24
;----------------------------------------------------------------------
; 1067 | EDSRec.ADiscVal[i] = data;                                             
;----------------------------------------------------------------------
        ldiu      *+fp(7),ir0
        ldiu      @CL38,ar0
        ldiu      *+fp(1),r0
L285:        
        sti       r0,*+ar0(ir0)
	.line	23
        ldiu      1,r0
        addi      *+fp(7),r0
        sti       r0,*+fp(7)
        cmpi      4,r0
        bled      L285
        ldile     *+fp(7),ir0
        ldile     @CL38,ar0
        ldile     *+fp(1),r0
;*      Branch Occurs to L285 
	.line	27
;----------------------------------------------------------------------
; 1070 | WriteEDS( iADR_DISC,            EDSRec.ADiscVal[0] );           // Supe
;     | r High                                                                 
;----------------------------------------------------------------------
        ldiu      @_EDSRec+27,r1
        ldiu      271,r0
L287:        
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	28
;----------------------------------------------------------------------
; 1071 | WriteEDS( iADR_DLEVEL0,         EDSRec.ADiscVal[1] );           // High
;----------------------------------------------------------------------
        ldiu      @_EDSRec+28,r0
        ldiu      301,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	29
;----------------------------------------------------------------------
; 1072 | WriteEDS( iADR_DLEVEL1,         EDSRec.ADiscVal[2] );           // Medi
;     | um                                                                     
;----------------------------------------------------------------------
        ldiu      @_EDSRec+29,r0
        push      r0
        ldiu      302,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	30
;----------------------------------------------------------------------
; 1073 | WriteEDS( iADR_DLEVEL2,         EDSRec.ADiscVal[3] );           // Low 
;----------------------------------------------------------------------
        ldiu      @_EDSRec+30,r0
        push      r0
        ldiu      303,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	31
;----------------------------------------------------------------------
; 1074 | WriteEDS( iADR_DLEVEL3,         EDSRec.ADiscVal[4] );           // Ener
;     | gy                                                                     
;----------------------------------------------------------------------
        ldiu      @_EDSRec+31,r0
        push      r0
        ldiu      304,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	32
;----------------------------------------------------------------------
; 1075 | EDSRec.ADisc ++;                                                       
;----------------------------------------------------------------------
        ldiu      1,r0
        addi      @_EDSRec+25,r0        ; Unsigned
	.line	33
;----------------------------------------------------------------------
; 1076 | rps_iter = 0;                                                          
;----------------------------------------------------------------------
	.line	34
;----------------------------------------------------------------------
; 1077 | break;                                                                 
; 1078 | case 2 :                                                               
; 1079 | case 3 :                                                               
; 1080 | case 4 :                                                               
; 1081 | case 5 :                                                               
;----------------------------------------------------------------------
        bud       L329
        sti       r0,@_EDSRec+25
        ldiu      0,r0
        sti       r0,@_rps_iter$2+0
;*      Branch Occurs to L329 
L288:        
	.line	39
;----------------------------------------------------------------------
; 1082 | EDSRec.ADisc ++;
;     |                  // just wait 100ms for each count allowing disc's to s
;     | ettle                                                                  
;----------------------------------------------------------------------
	.line	40
;----------------------------------------------------------------------
; 1083 | break;                                                                 
; 1085 | case 6 :                                                               
; 1086 | // Start Acquisition                                                   
;----------------------------------------------------------------------
        bud       L329
        ldiu      1,r0
        addi      @_EDSRec+25,r0        ; Unsigned
        sti       r0,@_EDSRec+25
;*      Branch Occurs to L329 
L289:        
	.line	44
;----------------------------------------------------------------------
; 1087 | WriteDPRAM( dADR_ANAL_MODE, EDS_ANAL_PRESET_REAL_PAMR );        // Set
;     | Preset Clock, but wait for PA_MR Trailing Edge Detect                  
; 1088 | //                      WriteDPRAM( dADR_ANAL_MODE, EDS_ANAL_PRESET_REA
;     | L );             // Set Preset Clock Mode                              
;----------------------------------------------------------------------
        ldiu      12,r1
        ldiu      17,r0
        push      r1
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	46
;----------------------------------------------------------------------
; 1089 | WriteDPRAM( dADR_TRANSFER, 0x1 );                                      
;----------------------------------------------------------------------
        ldiu      1,r0
        ldiu      18,r1
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	47
;----------------------------------------------------------------------
; 1090 | SetAnalMode();                                                         
;----------------------------------------------------------------------
        call      _SetAnalMode
                                        ; Call Occurs
	.line	48
;----------------------------------------------------------------------
; 1091 | EDSRec.ADisc ++;                                                       
;----------------------------------------------------------------------
        ldiu      1,r0
        addi      @_EDSRec+25,r0        ; Unsigned
	.line	49
;----------------------------------------------------------------------
; 1092 | wait_cnt = 50;                                  // 5 seconds           
;----------------------------------------------------------------------
	.line	50
;----------------------------------------------------------------------
; 1093 | break;                                                                 
; 1094 | case 7 :                                                               
; 1095 | // Wait for Analysis to be Started                                     
;----------------------------------------------------------------------
        bud       L329
        sti       r0,@_EDSRec+25
        ldiu      50,r0
        sti       r0,@_wait_cnt$1+0
;*      Branch Occurs to L329 
L290:        
	.line	53
;----------------------------------------------------------------------
; 1096 | data = ReadEDS( iADR_STAT );
;     |          // Wait for EDX ANal started                                  
;----------------------------------------------------------------------
        ldiu      257,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	54
;----------------------------------------------------------------------
; 1097 | if(( data & 0x8 ) == 0x8 ){                                            
;----------------------------------------------------------------------
        ldiu      8,r0
        and       *+fp(1),r0
        cmpi      8,r0
        bned      L293
	nop
	nop
        ldine     1,r0
;*      Branch Occurs to L293 
	.line	55
;----------------------------------------------------------------------
; 1098 | EDSRec.ADisc ++;                                                       
; 1099 | } else {                                                               
;----------------------------------------------------------------------
        bud       L329
        ldiu      1,r0
        addi      @_EDSRec+25,r0        ; Unsigned
        sti       r0,@_EDSRec+25
;*      Branch Occurs to L329 
	.line	57
;----------------------------------------------------------------------
; 1100 | wait_cnt --;                                                           
;----------------------------------------------------------------------
L293:        
        subri     @_wait_cnt$1+0,r0
        sti       r0,@_wait_cnt$1+0
	.line	58
;----------------------------------------------------------------------
; 1101 | if( wait_cnt == 0 ){                                                   
;----------------------------------------------------------------------
        bned      L329
	nop
        ldieq     0,r1
        ldieq     17,r0
;*      Branch Occurs to L329 
	.line	59
;----------------------------------------------------------------------
; 1102 | WriteDPRAM( dADR_ANAL_MODE, EDS_ANAL_STOP );                           
;----------------------------------------------------------------------
        push      r1
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	60
;----------------------------------------------------------------------
; 1103 | SetAnalMode();
;     |                          // Stop the Analysis                          
;----------------------------------------------------------------------
        call      _SetAnalMode
                                        ; Call Occurs
	.line	61
;----------------------------------------------------------------------
; 1104 | WriteDPRAM( dADR_ANAL_MODE, EDS_ANAL_PRESET_REAL );             // Set
;     | Preset Clock Mode                                                      
;----------------------------------------------------------------------
        ldiu      1,r0
        ldiu      17,r1
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	62
;----------------------------------------------------------------------
; 1105 | WriteDPRAM( dADR_TRANSFER, 0x1 );
;     |          // Reset the Analysis                                         
;----------------------------------------------------------------------
        ldiu      1,r0
        push      r0
        ldiu      18,r1
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	63
;----------------------------------------------------------------------
; 1106 | SetAnalMode();                                                         
;----------------------------------------------------------------------
        call      _SetAnalMode
                                        ; Call Occurs
	.line	66
;----------------------------------------------------------------------
; 1109 | break;                                                                 
; 1111 | case 8 :                                                               
; 1112 | // do nothing, wait until preset done                                  
; 1113 | // Analysis will be off                                                
;----------------------------------------------------------------------
        bud       L332
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      9,sp
;*      Branch Occurs to L332 
L295:        
	.line	71
;----------------------------------------------------------------------
; 1114 | data = ReadEDS( iADR_STAT );
;     |          // EDX Anal is Done, so Preset is also done                   
;----------------------------------------------------------------------
        ldiu      257,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	72
;----------------------------------------------------------------------
; 1115 | if(( data & 0x8 ) == 0 ){                                              
;----------------------------------------------------------------------
        ldiu      8,r0
        tstb      *+fp(1),r0
        bned      L329
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L329 
	.line	73
;----------------------------------------------------------------------
; 1116 | EDSRec.ADisc ++;                                                       
;----------------------------------------------------------------------
	.line	75
;----------------------------------------------------------------------
; 1118 | break;                                                                 
; 1120 | case 9 :                                                               
; 1121 | // Read the "Clock Time" from the DPRAM                                
;----------------------------------------------------------------------
        bud       L330
        addi      @_EDSRec+25,r0        ; Unsigned
        sti       r0,@_EDSRec+25
        ldiu      *-fp(1),bk
;*      Branch Occurs to L330 
L297:        
	.line	79
;----------------------------------------------------------------------
; 1122 | data = ReadDPRAM( iADR_PRESET_LO );                                    
;----------------------------------------------------------------------
        ldiu      279,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	80
;----------------------------------------------------------------------
; 1123 | data |= ( ReadDPRAM( iADR_PRESET_HI ) << 16 );                         
;----------------------------------------------------------------------
        ldiu      280,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        ash       16,r0
        or        *+fp(1),r0
        sti       r0,*+fp(1)
	.line	81
;----------------------------------------------------------------------
; 1124 | time = ((float)data / (float)1000.0f );                                
; 1126 | // Read the Net number of Resets for the time periord                  
; 1127 | //                      data = ReadEDS( iADR_NET_RPSL );               
; 1128 | //                      data |= ( ReadEDS( iADR_NET_RPSH ) << 16 );    
; 1130 | // normalize it per unit time                                          
; 1131 | //                      rps = (USHORT)((float)2.0f * (float) time );   
; 1133 | //                      if(( data < rps ) || ( rps_iter > 5 )){        
;----------------------------------------------------------------------
        float     *+fp(1),f0
        ldflt     @CL28,f1
        ldfge     0.0000000000e+00,f1
        addf3     f0,f1,f0
        mpyf      @CL39,f0
        stf       f0,*+fp(3)
	.line	91
;----------------------------------------------------------------------
; 1134 | maxcps = (USHORT)( (float)ReadDPRAM( dADR_MAX_CPS ) * time );          
;----------------------------------------------------------------------
        ldiu      30,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        float     r0,f0
        ldflt     @CL28,f1
        ldfge     0.0000000000e+00,f1
        subi      1,sp
        addf3     f0,f1,f0
        mpyf      *+fp(3),f0
        cmpf      @CL30,f0
        ldflt     0.0000000000e+00,f1
        ldfge     @CL28,f1
        subrf     f0,f1
        fix       f1,r0
        sti       r0,*+fp(4)
	.line	93
;----------------------------------------------------------------------
; 1136 | mask = 1 << EDSRec.ASHBit;
;     |                  // Create Mask Bit                                    
;----------------------------------------------------------------------
        ldiu      1,r0
        ash       @_EDSRec+26,r0
        sti       r0,*+fp(5)
	.line	94
;----------------------------------------------------------------------
; 1137 | for(i=0;i<=4;i++){                                                     
; 1138 | //                                      data= ReadEDS( (USHORT)(iADR_SH
;     | _Disc_Cnts+i) );                 // Read the Counts                    
; 1139 |         // Calculate the Average Input Count Rate                      
; 1140 | //                                      cps = (USHORT)((float)data / (f
;     | loat)time );                             // Calculate the CPS          
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(7)
        cmpi      4,r0
        bgt       L321
;*      Branch Occurs to L321 
	.line	99
;----------------------------------------------------------------------
; 1142 | cps = ReadEDS( (USHORT)(iADR_SH_Disc_Cnts+i) );                 // Read
;     |  the Counts                                                            
;----------------------------------------------------------------------
        ldiu      347,r0
L299:        
        addi      *+fp(7),r0            ; Unsigned
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(2)
	.line	100
;----------------------------------------------------------------------
; 1143 | if( cps >= maxcps ){
;     |                  // Value is too high                                  
;----------------------------------------------------------------------
        cmpi      *+fp(4),r0
        blod      L302
        ldilo     @CL38,ir0
        ldilo     *+fp(7),ar0
        ldilo     *+fp(5),r0
;*      Branch Occurs to L302 
	.line	101
;----------------------------------------------------------------------
; 1144 | EDSRec.ADiscVal[i] |= mask;
;     |  // So Increase DAC value by Current Bit                               
; 1145 | } else {                                                               
;----------------------------------------------------------------------
        ldiu      @CL38,ir0
        ldiu      *+fp(7),ar0
        bud       L303
        ldiu      *+fp(5),r0
        or3       r0,*+ar0(ir0),r0
        sti       r0,*+ar0(ir0)
;*      Branch Occurs to L303 
	.line	103
;----------------------------------------------------------------------
; 1146 | EDSRec.ADiscVal[i] &= ~mask;
;     |  // Clear the "Current" bit                                            
;----------------------------------------------------------------------
L302:        
        andn3     r0,*+ar0(ir0),r0
        sti       r0,*+ar0(ir0)
L303:        
	.line	106
;----------------------------------------------------------------------
; 1149 | if( EDSRec.ASHBit != 0 ){
;     |          // if the last bit                                            
;----------------------------------------------------------------------
        ldi       @_EDSRec+26,r0
        beqd      L306
	nop
	nop
        ldieq     100,r0
;*      Branch Occurs to L306 
	.line	107
;----------------------------------------------------------------------
; 1150 | data = 1 << ( EDSRec.ASHBit-1 );
;     |  // Set the Next Bit                                                   
;----------------------------------------------------------------------
        ldiu      1,r0
        ldiu      1,r1
        subri     @_EDSRec+26,r0        ; Unsigned
        ash3      r0,r1,r0
        sti       r0,*+fp(1)
	.line	108
;----------------------------------------------------------------------
; 1151 | EDSRec.ADiscVal[i] |= data;
;     |  // Update the Value for the Next Bit                                  
; 1152 | } else {                                                               
;----------------------------------------------------------------------
        ldiu      *+fp(7),ar0
        ldiu      @CL38,ir0
        bud       L314
        or3       r0,*+ar0(ir0),r0
        sti       r0,*+ar0(ir0)
	nop
;*      Branch Occurs to L314 
	.line	110
;----------------------------------------------------------------------
; 1153 | EDSRec.ADiscVal[0] += 100;
;     |  // Super High                                                         
;----------------------------------------------------------------------
L306:        
        addi      @_EDSRec+27,r0        ; Unsigned
        sti       r0,@_EDSRec+27
	.line	111
;----------------------------------------------------------------------
; 1154 | EDSRec.ADiscVal[1] += 5;
;     |          // High                                                       
;----------------------------------------------------------------------
        ldiu      5,r0
        addi      @_EDSRec+28,r0        ; Unsigned
        sti       r0,@_EDSRec+28
	.line	112
;----------------------------------------------------------------------
; 1155 | EDSRec.ADiscVal[2] += 5;
;     |          // Medium                                                     
;----------------------------------------------------------------------
        ldiu      5,r0
        addi      @_EDSRec+29,r0        ; Unsigned
        sti       r0,@_EDSRec+29
	.line	113
;----------------------------------------------------------------------
; 1156 | EDSRec.ADiscVal[3] += 5;
;     |          // Low                                                        
;----------------------------------------------------------------------
        ldiu      5,r0
        addi      @_EDSRec+30,r0        ; Unsigned
        sti       r0,@_EDSRec+30
	.line	114
;----------------------------------------------------------------------
; 1157 | EDSRec.ADiscVal[4] += 5;
;     |          // Energy                                                     
; 1159 | switch( i ){                                                           
;----------------------------------------------------------------------
        bud       L313
        ldiu      5,r0
        addi      @_EDSRec+31,r0        ; Unsigned
        sti       r0,@_EDSRec+31
;*      Branch Occurs to L313 
L307:        
	.line	117
;----------------------------------------------------------------------
; 1160 | case 0 : addr = iADR_DISC;                      break;                 
;----------------------------------------------------------------------
        bud       L320
        ldiu      271,r0
        sti       r0,*+fp(6)
        ldiu      *+fp(7),ir0
;*      Branch Occurs to L320 
L308:        
	.line	118
;----------------------------------------------------------------------
; 1161 | case 1 : addr = iADR_DLEVEL0;           break;                         
;----------------------------------------------------------------------
        bud       L320
        ldiu      301,r0
        sti       r0,*+fp(6)
        ldiu      *+fp(7),ir0
;*      Branch Occurs to L320 
L309:        
	.line	119
;----------------------------------------------------------------------
; 1162 | case 2 : addr = iADR_DLEVEL1;           break;                         
;----------------------------------------------------------------------
        bud       L320
        ldiu      302,r0
        sti       r0,*+fp(6)
        ldiu      *+fp(7),ir0
;*      Branch Occurs to L320 
L310:        
	.line	120
;----------------------------------------------------------------------
; 1163 | case 3 : addr = iADR_DLEVEL2;           break;                         
;----------------------------------------------------------------------
        bud       L320
        ldiu      303,r0
        sti       r0,*+fp(6)
        ldiu      *+fp(7),ir0
;*      Branch Occurs to L320 
L311:        
	.line	121
;----------------------------------------------------------------------
; 1164 | case 4 : addr = iADR_DLEVEL3;           break;                         
;----------------------------------------------------------------------
        bud       L320
        ldiu      304,r0
        sti       r0,*+fp(6)
        ldiu      *+fp(7),ir0
;*      Branch Occurs to L320 
L313:        
	.line	116
L314:        
        ldi       *+fp(7),r0
        beq       L307
;*      Branch Occurs to L307 
        cmpi      1,r0
        beq       L308
;*      Branch Occurs to L308 
        cmpi      2,r0
        beq       L309
;*      Branch Occurs to L309 
        cmpi      3,r0
        beq       L310
;*      Branch Occurs to L310 
        cmpi      4,r0
        beqd      L311
	nop
	nop
        ldine     *+fp(7),ir0
;*      Branch Occurs to L311 
	.line	123
;----------------------------------------------------------------------
; 1166 | WriteEDS( addr,   EDSRec.ADiscVal[i] );                                
; 1167 | }
;     |                                          // for i...                   
;----------------------------------------------------------------------
L320:        
        ldiu      @CL38,ar0
        ldiu      *+ar0(ir0),r0
        push      r0
        ldiu      *+fp(6),r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	94
        ldiu      1,r0
        addi      *+fp(7),r0
        sti       r0,*+fp(7)
        cmpi      4,r0
        bled      L299
	nop
	nop
        ldile     347,r0
;*      Branch Occurs to L299 
L321:        
	.line	126
;----------------------------------------------------------------------
; 1169 | if( EDSRec.ASHBit == 0 ){
;     |                  // if the last bit                                    
;----------------------------------------------------------------------
        ldi       @_EDSRec+26,r0
        bned      L324
	nop
	nop
        ldine     2,r0
;*      Branch Occurs to L324 
	.line	127
;----------------------------------------------------------------------
; 1170 | EDSRec.ASHBit = 0;
;     |                  // clear both records                                 
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_EDSRec+26
	.line	128
;----------------------------------------------------------------------
; 1171 | EDSRec.ADisc = 0;                                                      
;----------------------------------------------------------------------
        sti       r0,@_EDSRec+25
	.line	129
;----------------------------------------------------------------------
; 1172 | WriteEDS( iADR_TIME_CLR, 0xFFFF);
;     |          // Clear Clock Time and Live Time                             
; 1174 | } else {                                                               
;----------------------------------------------------------------------
        ldiu      @CL1,r1
        push      r1
        ldiu      277,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        bud       L331
        subi      2,sp
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L331 
	.line	132
;----------------------------------------------------------------------
; 1175 | EDSRec.ADisc = 2;
;     |                  // Setup to Restart the process                       
;----------------------------------------------------------------------
L324:        
        sti       r0,@_EDSRec+25
	.line	133
;----------------------------------------------------------------------
; 1176 | EDSRec.ASHBit--;
;     |                  // Decrement the Bit Index                            
; 1177 | }
;     |                                          // if ASHBit...               
; 1179 | //                      } else {
;     |                                                                  // rps
;     |  > 2                                                                   
; 1180 | //                              EDSRec.ADisc = 2;
;     |                                                          // Restart the
;     |  process w/o changing the bit                                          
; 1181 | //                              rps_iter ++;                           
; 1182 | //                      }                                              
;----------------------------------------------------------------------
	.line	140
;----------------------------------------------------------------------
; 1183 | break;                                                                 
; 1184 | default :                                                              
;----------------------------------------------------------------------
        bud       L329
        ldiu      1,r0
        subri     @_EDSRec+26,r0        ; Unsigned
        sti       r0,@_EDSRec+26
;*      Branch Occurs to L329 
L325:        
	.line	142
;----------------------------------------------------------------------
; 1185 | EDSRec.ADisc =0;                                                       
;----------------------------------------------------------------------
	.line	143
;----------------------------------------------------------------------
; 1186 | break;                                                                 
; 1187 | }
;     |                                                          // Case       
;----------------------------------------------------------------------
        bud       L330
        ldiu      0,r0
        sti       r0,@_EDSRec+25
        ldiu      *-fp(1),bk
;*      Branch Occurs to L330 
L327:        
	.line	14
        ldiu      @_EDSRec+25,ir0
        cmpi      9,ir0
        bhid      L325
	nop
	nop
        ldils     @CL40,ar0
;*      Branch Occurs to L325 
        ldiu      *+ar0(ir0),r0
        bu        r0

	.sect	".text"
SW4:	.word	L329	; 0
	.word	L283	; 1
	.word	L288	; 2
	.word	L288	; 3
	.word	L288	; 4
	.word	L288	; 5
	.word	L289	; 6
	.word	L290	; 7
	.word	L295	; 8
	.word	L297	; 9
	.sect	".text"
;*      Branch Occurs to r0 
L329:        
	.line	145
        ldiu      *-fp(1),bk
L330:        
        ldiu      *fp,fp
L331:        
        subi      9,sp
L332:        
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	1188,000000000h,7


	.sect	 ".text"

	.global	_SetNonLinearRam
	.sym	_SetNonLinearRam,_SetNonLinearRam,32,2,0
	.func	1192
;******************************************************************************
;* FUNCTION NAME: _SetNonLinearRam                                            *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,fp,sp,st                                      *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 3 Auto + 0 SOE = 6 words          *
;******************************************************************************
_SetNonLinearRam:
	.sym	_data,-2,13,9,32
	.sym	_i,1,4,1,32
	.sym	_tmp,2,13,1,32
	.sym	_tmp2,3,13,1,32
	.line	1
;----------------------------------------------------------------------
; 1192 | void SetNonLinearRam( USHORT data )                                    
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      3,sp
	.line	2
;----------------------------------------------------------------------
; 1194 | int i;                                                                 
; 1195 | USHORT tmp;                                                            
; 1196 | USHORT tmp2;                                                           
; 1198 | // Set the RAM to DSP Memory Access                                    
;----------------------------------------------------------------------
	.line	8
;----------------------------------------------------------------------
; 1199 | tmp = ReadEDS( iADR_CW );                                              
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(2)
	.line	9
;----------------------------------------------------------------------
; 1200 | tmp |= RAM_ACCESS;                                                     
;----------------------------------------------------------------------
        ldiu      16,r0
        or        *+fp(2),r0
        sti       r0,*+fp(2)
	.line	10
;----------------------------------------------------------------------
; 1201 | tmp &= ~SCA_EN;                                                        
;----------------------------------------------------------------------
        andn      32,r0
        sti       r0,*+fp(2)
	.line	11
;----------------------------------------------------------------------
; 1202 | WriteEDS( iADR_CW, tmp );                                              
;----------------------------------------------------------------------
        ldiu      258,r0
        ldiu      *+fp(2),r1
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	13
;----------------------------------------------------------------------
; 1204 | for(i=0;i<=0xFFF;i++){                                                 
; 1205 |         switch(data){                                                  
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
        cmpi      4095,r0
        bgtd      L368
	nop
	nop
        ldigt     258,r0
;*      Branch Occurs to L368 
        bu        L358
;*      Branch Occurs to L358 
L336:        
	.line	15
;----------------------------------------------------------------------
; 1206 | case 0 : tmp2 = 0; break;                                              
; 1207 | case 1 :                                                               
;----------------------------------------------------------------------
        bud       L362
        ldiu      0,r0
        sti       r0,*+fp(3)
        ldiu      1,r0
;*      Branch Occurs to L362 
	.line	17
;----------------------------------------------------------------------
; 1208 | if( i <= 589 ){                                                        
;----------------------------------------------------------------------
L338:        
        cmpi      589,r0
        bgtd      L341
	nop
	nop
        ldigt     *+fp(1),r0
;*      Branch Occurs to L341 
	.line	18
;----------------------------------------------------------------------
; 1209 | tmp2 = 589 - i;                                                        
; 1210 | } else {                                                               
;----------------------------------------------------------------------
        bud       L342
        subri     589,r0                ; Unsigned
        sti       r0,*+fp(3)
        ldiu      *+fp(1),r0
;*      Branch Occurs to L342 
	.line	20
;----------------------------------------------------------------------
; 1211 | tmp2 = 0x100 - ( i - 589 );                                            
;----------------------------------------------------------------------
L341:        
        subri     256,r0                ; Unsigned
        addi      589,r0                ; Unsigned
        sti       r0,*+fp(3)
	.line	22
;----------------------------------------------------------------------
; 1213 | if( i <= ( 589-10 )) tmp2 = 10;                                        
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
L342:        
        cmpi      579,r0
        bgtd      L344
	nop
	nop
        ldigt     *+fp(1),r0
;*      Branch Occurs to L344 
        ldiu      10,r0
        sti       r0,*+fp(3)
	.line	23
;----------------------------------------------------------------------
; 1214 | if( i >= ( 589+10 )) tmp2 = 0x100 - 10;
;     |                                                                        
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
L344:        
        cmpi      599,r0
        bltd      L362
	nop
	nop
        ldilt     1,r0
;*      Branch Occurs to L362 
	.line	24
;----------------------------------------------------------------------
; 1215 | break;                                                                 
; 1217 | case 2 :                                                               
;----------------------------------------------------------------------
        bud       L362
        ldiu      246,r0
        sti       r0,*+fp(3)
        ldiu      1,r0
;*      Branch Occurs to L362 
	.line	27
;----------------------------------------------------------------------
; 1218 | if( i < 589 ) tmp2 = (( 589 - i ) * 2 ) - 1;                           
;----------------------------------------------------------------------
L347:        
        cmpi      589,r0
        bged      L349
	nop
	nop
        ldige     *+fp(1),r0
;*      Branch Occurs to L349 
        subri     589,r0
        ash       1,r0
        subi      1,r0                  ; Unsigned
        sti       r0,*+fp(3)
	.line	28
;----------------------------------------------------------------------
; 1219 | if( i == 589 ) tmp2 = 0;                                               
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
L349:        
        cmpi      589,r0
        bned      L351
	nop
	nop
        ldine     *+fp(1),r0
;*      Branch Occurs to L351 
        ldiu      0,r0
        sti       r0,*+fp(3)
	.line	29
;----------------------------------------------------------------------
; 1220 | if( i > 589 ) tmp2 = 0x100 - (( 590 - i ) + 1 );                       
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
L351:        
        cmpi      589,r0
        bled      L353
	nop
	nop
        ldile     *+fp(1),r0
;*      Branch Occurs to L353 
        ldiu      335,r0
        subri     *+fp(1),r0            ; Unsigned
        sti       r0,*+fp(3)
	.line	30
;----------------------------------------------------------------------
; 1221 | if( i <= ( 589-10 )) tmp2 = 17;                                        
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
L353:        
        cmpi      579,r0
        bgtd      L355
	nop
	nop
        ldigt     *+fp(1),r0
;*      Branch Occurs to L355 
        ldiu      17,r0
        sti       r0,*+fp(3)
	.line	31
;----------------------------------------------------------------------
; 1222 | if( i >= ( 589+10 )) tmp2 = 0x100 - 17;                                
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
L355:        
        cmpi      599,r0
        bltd      L362
	nop
	nop
        ldilt     1,r0
;*      Branch Occurs to L362 
	.line	33
;----------------------------------------------------------------------
; 1224 | break;                                                                 
;----------------------------------------------------------------------
        bud       L362
        ldiu      239,r0
        sti       r0,*+fp(3)
        ldiu      1,r0
;*      Branch Occurs to L362 
	.line	34
;----------------------------------------------------------------------
; 1225 | default : break;                                                       
;----------------------------------------------------------------------
L358:        
	.line	14
        ldi       *-fp(2),r0
        beq       L336
;*      Branch Occurs to L336 
        cmpi      1,r0
        beqd      L338
	nop
	nop
        ldieq     *+fp(1),r0
;*      Branch Occurs to L338 
        cmpi      2,r0
        beqd      L347
	nop
	nop
        ldieq     *+fp(1),r0
;*      Branch Occurs to L347 
	.line	37
;----------------------------------------------------------------------
; 1228 | if(( i & 1 ) == 0 ){                                                   
;----------------------------------------------------------------------
        ldiu      1,r0
L362:        
        tstb      *+fp(1),r0
        bned      L365
	nop
	nop
        ldine     *+fp(3),r0
;*      Branch Occurs to L365 
	.line	38
;----------------------------------------------------------------------
; 1229 | tmp = tmp2;                                                            
; 1230 | } else {                                                               
;----------------------------------------------------------------------
        bud       L366
        ldiu      *+fp(3),r0
        sti       r0,*+fp(2)
        ldiu      1,r0
;*      Branch Occurs to L366 
	.line	40
;----------------------------------------------------------------------
; 1231 | tmp |= ( tmp2 << 8 );                                                  
;----------------------------------------------------------------------
L365:        
        ash       8,r0
        or        *+fp(2),r0
        sti       r0,*+fp(2)
	.line	41
;----------------------------------------------------------------------
; 1232 | WriteEDS( iADR_RAMA, (USHORT)  (i >> 1 ) );                            
;----------------------------------------------------------------------
        ldiu      289,r1
        ldiu      *+fp(1),r0
        ash       -1,r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	42
;----------------------------------------------------------------------
; 1233 | WriteEDS( iADR_SCA_LU, tmp );                                          
; 1238 | // Set the RAM to non-DSP Memory Access                                
;----------------------------------------------------------------------
        ldiu      *+fp(2),r0
        ldiu      291,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	13
        ldiu      1,r0
L366:        
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      4095,r0
        bled      L358
	nop
	nop
        ldigt     258,r0
;*      Branch Occurs to L358 
	.line	48
;----------------------------------------------------------------------
; 1239 | tmp = ReadEDS( iADR_CW );                                              
;----------------------------------------------------------------------
L368:        
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(2)
	.line	49
;----------------------------------------------------------------------
; 1240 | tmp &= ~RAM_ACCESS;                                                    
;----------------------------------------------------------------------
        andn      16,r0
        sti       r0,*+fp(2)
	.line	50
;----------------------------------------------------------------------
; 1241 | WriteEDS( iADR_CW, tmp );                                              
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      258,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	52
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      5,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	1243,000000000h,3



	.global	_EDSRec
	.bss	_EDSRec,34
	.sym	_EDSRec,_EDSRec,8,2,1088,.fake21

	.global	_ROIRec
	.bss	_ROIRec,250
	.sym	_ROIRec,_ROIRec,56,2,8000,.fake22,50
;******************************************************************************
;* CONSTANT TABLE                                                             *
;******************************************************************************
	.sect	".const"
	.bss	CL1,1
	.bss	CL2,1
	.bss	CL3,1
	.bss	CL4,1
	.bss	CL5,1
	.bss	CL6,1
	.bss	CL7,1
	.bss	CL8,1
	.bss	CL9,1
	.bss	CL10,1
	.bss	CL11,1
	.bss	CL12,1
	.bss	CL13,1
	.bss	CL14,1
	.bss	CL15,1
	.bss	CL16,1
	.bss	CL17,1
	.bss	CL18,1
	.bss	CL19,1
	.bss	CL20,1
	.bss	CL21,1
	.bss	CL22,1
	.bss	CL23,1
	.bss	CL24,1
	.bss	CL25,1
	.bss	CL26,1
	.bss	CL27,1
	.bss	CL28,1
	.bss	CL29,1
	.bss	CL30,1
	.bss	CL31,1
	.bss	CL32,1
	.bss	CL33,1
	.bss	CL34,1
	.bss	CL35,1
	.bss	CL36,1
	.bss	CL37,1
	.bss	CL38,1
	.bss	CL39,1
	.bss	CL40,1

	.sect	".cinit"
	.field  	40,32
	.field  	CL1+0,32
	.field  	65535,32
	.field  	170000,32
	.field  	_ROIRec,32
	.field  	_ROIRec+1,32
	.field  	_ROIRec+3,32
	.field  	_ROIRec+4,32
	.field  	_ROIRec+2,32
	.field  	_EDSRec+19,32
	.field  	55314,32
	.field  	9461760,32
	.field  	9461761,32
	.field  	9469952,32
	.field  	9469953,32
	.field  	262143,32
	.field  	43007,32
	.field  	65535,32
	.field  	SW0,32
	.word   	0FE4CCCCDH ; float   4.000000000000000e-01
	.word   	0FF4CCCCDH ; float   8.000000000000000e-01
	.word   	0004CCCCDH ; float   1.600000000000000e+00
	.word   	0014CCCCDH ; float   3.200000000000000e+00
	.word   	0024CCCCDH ; float   6.400000000000000e+00
	.word   	0034CCCCDH ; float   1.280000000000000e+01
	.word   	0044CCCCDH ; float   2.560000000000000e+01
	.word   	0054CCCCDH ; float   5.120000000000000e+01
	.word   	0064CCCCDH ; float   1.024000000000000e+02
	.field  	SW1,32
	.word   	020000000H ; float   4.294967296000000e+09
	.word   	0FC4CCCCDH ; float   1.000000000000000e-01
	.word   	01F000000H ; float   2.147483648000000e+09
	.field  	SW2,32
	.field  	SW3,32
	.word   	0FA4CCCCDH ; float   2.500000000000000e-02
	.word   	0FB4CCCCDH ; float   5.000000000000000e-02
	.word   	0FF666666H ; float   9.000000000000000e-01
	.word   	0FF733333H ; float   9.500000000000000e-01
	.word   	0000CCCCDH ; float   1.100000000000000e+00
	.field  	_EDSRec+27,32
	.word   	0F603126FH ; float   1.000000000000000e-03
	.field  	SW4,32

	.sect	".text"
;******************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                              *
;******************************************************************************

	.global	_ReadDPRAM

	.global	_WriteDPRAM
