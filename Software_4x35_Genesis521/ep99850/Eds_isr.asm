;********************************************************************************/
;*																*/
;*	Module:	EDS_ISR.ASM  (DPP-II)									*/
;*																*/
;*	EDAX Inc														*/
;*																*/
;*  Author:		Michael Solazzi									*/
;*  Created:    3/25/99 												*/
;*  Board:      EDI-III;												*/
;*	Description:													*/
;*		This Code file contains all of the Interrupt Service Routines for the	*/
;*		EDI-II Board.												*/
;*																*/
;*  History:		
;*		05/11/16 - MCS 	Ver 357C
;*				SPEAK_RESTART function for test with PRESET_DONE
;*				BNZ EXIT_ISR instead of PRESET_DONE ( Same as Genesis 3.60 Release )												*/
;*		00/05/03 - MCS												*/
;*				Added test to check for SEMRec.IRQ_PEND, if it is set		*/
;*				Exit the ISR ( Give SEM higher priority )				*/
;*		00/01/07 - MCS												*/
;*				Deleted Altering the EDSRec.EDSXFer flag				*/
;*				EDSRec.EDS_Stat is updated when Reading the FIFO Status	*/
;*		99/11/29 - MCS - Updated										*/
;*		99/08/31 - MCS - PROC_SEM_ISR									*/
;*			Key Decodes updated for SEMFPGA requirements					*/
;*		99/08/16 - MCS - Updated for EDI-II Rev B						*/
;*																*/
;*	ISR Interrupts are automatically disabled							*/
;*	asm("	and     0DFFFH,st       ;disable interrupts	");				*/
;*	Re-Enable Interrupts, automatically done							*/
;*	asm("	or     2000H,st       ;enable interrupts	");				*/
;********************************************************************************/


;******************************************************************************
;* FUNCTION NAME: _Proc_Eds_Isr                                               *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : f0,r0,f1,ar0,st                                     *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 4 Auto + 0 SOE = 6 words          *
;******************************************************************************
;
; Uses AR0, AR0, 
;	R1
;	R2	= Key
	.global _EDSRec, _Proc_Eds_Isr
_Proc_Eds_Isr:

;----- DSP Address Bus Decodes --------------------------------------
JUNK					.LONG 0904000H
PCI_FIFO_BASE			.LONG 0904000H
EDS_FPGA_BASE			.LONG 0906000H
EDS_FIFO_STAT			.LONG 0908000H
EDS_FIFO_BASE			.LONG 0908001H
MCARec				.LONG 0884000H
BaselineRec			.LONG 0885000H

;----- PCI FIFO Keys ------------------------------------------------
kFF_CHAN				.set 001000H
kFF_RADC				.set 000000H
kFF_BASELINE			.set 002000H

;----- EDS FPGA Offsets
kSTATUS				.set 001H
kPRESET_ACK			.set 019H
kFIFO_LO				.set 028H

;----- EDS Record Offsets -------------------------------------------
; Offsets in the EDS_TYPE record residing in file EDSUTL.H
rFPGA_Preset_Done		.set 000H		; FPGA Preset Done 
rEDS_STAT				.set 002H		; EDS Status
rDSO_PENDING			.set 003H		; DSO Pending
rEDS_IRQ				.set 005H
rPreset_Mode			.set 009H
rPreset_Done_Cnt		.set 00AH
rDSO_ENABLE			.set 00EH		; DSO Mode Enabled
rPixelCount			.set 00FH		; Live Spectrum Mapping Preset DOne Count
;----- Constant 
kFPGA_PRESET_LSMAP_REAL	.set 007H		; Local Definition for Live Spectrum Mapping (set in SetAnalMode in EDSUTIL.C)
kFPGA_PRESET_LSMAP_LIVE	.set 008H
kFPGA_PRESET_WDS_REAL	.set 009H
kFPGA_PRESET_WDS_LIVE	.set 00AH

kSPECTRUM				.set 01000H
kPRESET_DONE			.set 04000H
;----- Offsets in the EDS FPGA --------------------------------------

;----- Misc Constants -----------------------------------------------

;----- Assign Names to Registers ------------------------------------
PCI_FIFO_REG			.set AR0;
EDS_FIFO_DATA			.set AR1;
MCA_BASE_REG			.set AR2;
EDS_FIFO_STAT_REG		.set AR3;
BASELINE_REG			.set AR4;
EDS_FPGA_REG			.set AR5;

FF_DATA				.set R0;
FF_KEY				.set R1;
LOOP_CNTR				.set R2;
TMP_REG				.set R3;
FF_PRESET_DONE			.set R4;
KSPECTRUM_KEY			.set R5;

;----- Assign Names to Registers ------------------------------------

; Push R0-R7 onto Stack							;		;
	PUSH IR0;									;	1	;	
	PUSH FF_DATA;								;	1	;
	PUSH FF_KEY;								;	1	;
	PUSH LOOP_CNTR;							;	1	;
	PUSH TMP_REG;								;	1	;
	PUSH FF_PRESET_DONE;						;	1	;
	PUSH KSPECTRUM_KEY;							;	1	;
		
; Push AR0-AR7 onto Stack						;		;
	PUSH PCI_FIFO_REG;							;	1	;
	PUSH EDS_FIFO_DATA;							;	1	;
	PUSH MCA_BASE_REG;							;	1	;
	PUSH EDS_FIFO_STAT_REG;						;	1	;
	PUSH BASELINE_REG;							;	1	;
	PUSH EDS_FPGA_REG;							;	1	;
	PUSH DP;									;	1	;
	
	; Setup Pointer to _EDSRec Record
	LDP _EDSRec,			DP;					;	1	; Load the Data Pointer for the EDSRecord

	; Setup Pointers for all other addresses
	LDP PCI_FIFO_BASE,		DP;					;	1	; Set the Data Pagew
	LDI @PCI_FIFO_BASE,		PCI_FIFO_REG;			;	1	; Setup the PCI FIFO Base Address
	LDI @EDS_FIFO_BASE,		EDS_FIFO_DATA;			;	1	; Setup EDS FIFO Base Address
	LDI @MCARec,			MCA_BASE_REG;			;	1	; Setup the MCA Base Register
	LDI @EDS_FIFO_STAT,		EDS_FIFO_STAT_REG;		;	1	;
	LDI @BaselineRec,		BASELINE_REG;			;	1	;
	LDI @EDS_FPGA_BASE,		EDS_FPGA_REG;			;	1	;

	;----- Setup Loop-Counter ( even it exit, won't destroy any data )	
	LDI 04000H, LOOP_CNTR;						;	0	; Mask off Higher Bits

	LDI kPRESET_DONE, FF_PRESET_DONE;				;	1	; Load Preset Done FIFO Key into Register
	LSH 16, FF_PRESET_DONE;						;	1	; Shift Register up by 16-bits (DO NOT ALTER)

	LDI kSPECTRUM, KSPECTRUM_KEY;					;	1	; Load Spectrum Key into register
	LSH 16, KSPECTRUM_KEY;						;	1	; Shift Register Left by 16 bits

	LDI @_EDSRec+rDSO_ENABLE, TMP_REG;				;	1	; Load DSO Enable flag into IR0
	CMPI 01H, TMP_REG;							;	1	; compare against Flag
	BZ DSO_RESTART;							;	4	; 

	LDI @_EDSRec+rPreset_Mode, IR0;				;	1	; Get the Preset Mode
	LDI kFPGA_PRESET_LSMAP_REAL, TMP_REG;			;	1	; Load Preset Live Spectrum Map Real into TMP_REG
	SUBI 1, TMP_REG;							;	1	; Sub 1 from TMP_REG
	SUBI IR0, TMP_REG;							;	1	; TMP_REG = TMP_REG - IR0 (6-Preset Mode)
	BN  SPEAK_RESTART;							;	4	; Branch on Negative result
	;----- First load kEDS_TMR_CNT into Register ( incase Exit, number can be updated properly )
	
	;----- Now Restart the Peak Detect


;**********************************************************************************************
;----- Non-Spectral Mapping Mode ------------------------------------------------------------------

PEAK_RESTART	
	SUBI 1, LOOP_CNTR;							;	1	; Subtract 1 from Loop_Counter
	BZ QUIT_ISR;								;	4	; If Zero Exit

	;----- Code to check the FIFO Empty Flag --------------------
	LDI *EDS_FIFO_STAT_REG, FF_DATA;			;	1	; Read the Status - Forces a FIFO Read Enable
	AND 01H, FF_DATA;							;	1	; Keep only the LSB ( FIFO Empty Flag Active High )
	BNZ QUIT_ISR;								;	4	; If not Zero, FIFO is empty so Exit
	;----- Code to check the FIFO Empty Flag --------------------

	;----- Read the EDS FIFO, 20 bits worth, the Empty flag is bit 18
	LDI *EDS_FIFO_DATA, FF_DATA					
 ||	LDI *EDS_FIFO_DATA, FF_KEY;					;	1	; Read The EDS FIFO and put into two registers

	;----- Form the FIFO Key used for testing data --------------
	LSH -15, FF_KEY;							;	1	; Shift the FIFO Key right by 15
	AND 07H, FF_KEY;							;	1	; Keep 3 bits
	;----- Form the FIFO Key used for testing data --------------

	;----- PEAK DATA COMPARISON -------------------------------------------------------------------
	; FIFO Key is in FF_KEY
	; Peak Data found in FF_DATA
	CMPI 00H, FF_KEY;			 				;	1	; Compare Against Code for Peak Data
	BZD PEAKDATA;								;	4	; Goto Peak Data
	LDI FF_DATA, IR0;							;	0	; Load FIFO Data into IR0
	AND 0FFFH, IR0;							;	0	; Clear Unwanted Bits
	NOP;										;	0	; 

	;----- PRESET DONE COMPARISON ------------------------------------------------------------------
	; CHeck for Equality
	CMPI 01H, FF_KEY;							;	1	; Compare Against Code for Preset Done
	BZ PRESETDONE;								;	4	; Goto Preset Done

	;----- Auto Zero COMPARISON --------------------------------------------------------------------
	; CHeck for Equality
	; R1 contains the Data ( all 12 bits )
	CMPI 02H, FF_KEY;							;	1	; Compare Against Code for Auto-Zero
	BZ AZERO;									;	4	; Goto AutoZero

	BR PEAK_RESTART;							;	4	; Otherwise restart process
	;----------------------------------------------------------------------------------------------
	;----- End of Comparisons ---------------------------------------------------------------------
	;----------------------------------------------------------------------------------------------
	
	;----------------------------------------------------------------------------------------------
	;----- Peak Data Processing ----------------; 5 max ;------------------------------------------
	; Peak Data is in IR0 ( ie Channel Number )
PEAKDATA
	BRD PEAK_RESTART							;	4	; Restart the Process
	LDI *+MCA_BASE_REG(IR0),FF_DATA;			;	0	; Get the MCA Counts from Memory
	ADDI 1, FF_DATA;							;	0	; Increment Register by 1
	STI FF_DATA, *+MCA_BASE_REG(IR0)			;	0	; Store Register back to MCA( FF_DATA )

	;----- Peak Data Processing -------------------------------------------------------------------
	;----------------------------------------------------------------------------------------------

	;----------------------------------------------------------------------------------------------

	;-----------------------------------------------------------------------------------------------
	;----- Auto Zero PreProcessing -------------;  10  ;--------------------------------------------
	; R1 contains the Channel Data normalized to ..BLEVEL_MAX channels
	; Baseline Value + 0x200 - Dither Correction
	; The Driver Must add: 1000dec - 0x200 = to place the "Zero" at channel 1000

AZERO
	AND 01FFH, IR0;							;	1	; Mask off the Unwanted Bits
	BRD PEAK_RESTART;							;	4	; Restart the Process
	LDI *+BASELINE_REG(IR0), FF_DATA;				;	0	; Read Value from Memory
	ADDI 1, FF_DATA;							;	0	; Increment Register by 1
	STI FF_DATA, *+BASELINE_REG(IR0);				;	0	; Store value back to memory
 	;----- Auto Zero PreProcessing ----------------------------------------------------------------

PRESETDONE
	STI FF_PRESET_DONE, @_EDSRec+rFPGA_Preset_Done;	;	1	; Set FPGA_Preset_Done_Flag in the DSP
	BR QUIT_ISR;								;	4	; 
;----- Non-Spectral Mapping Mode --------------------------------------------------------------
;**********************************************************************************************

;**********************************************************************************************
;----- Spectral Mapping Mode ------------------------------------------------------------------
SPEAK_RESTART	
	SUBI 1, LOOP_CNTR;							;	1	; Subtract 1 from Loop_Counter
	BZ QUIT_ISR;								;	4	; If Zero Exit

	;----- Code to check the FIFO Empty Flag --------------------
	LDI *EDS_FIFO_STAT_REG, FF_DATA;				;	1	; Read the FIFO Status Register - Forces a Read Enable
	AND 01H, FF_DATA;							;	1	; Keep only the LSB ( FIFO Empty Flag Active High )
	BNZ QUIT_ISR;								;	4	; If not Zero, FIFO is empty so Exit
	;----- Code to check the FIFO Empty Flag --------------------

	;----- Read the EDS FIFO, 20 bits worth, the Empty flag is bit 18
	LDI *EDS_FIFO_DATA, FF_DATA					
 ||	LDI *EDS_FIFO_DATA, FF_KEY;					;	1	; Read The EDS FIFO and put into two registers

	;----- Form the FIFO Key used for testing data --------------
	LSH -17, FF_KEY;							;	1	; Shift the FIFO Key right by 17
	AND 01H, FF_KEY;							;	1	; Keep the LSB Only
	;----- Form the FIFO Key used for testing data --------------

	;----- Preset Done Comparison -------------------------------
	CMPI 01H, FF_KEY;							;	1	; Compare Against Code for Preset Done
	BZ SPRESETDONE;								;	4	; if Zero, Preset Done so process PReset DOne
	
	;----- Spectral Mapping Peak Processing --------------------------------------------------------
	; Otherwise Spectral Data and process accordingly ;   10	;
	;	FF_DATA contains the Channel Number
	;	FIFO should contain:
	;	3 3 2 2 | 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 | 1 1 
	;	1 0 9 8 | 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 | 1 0 9 8 7 6 5 4 3 2 1 0
	;	--------+---------------------------------+------------------------
	;	0 0 0 1 | Pixel ( aka Preset Done Count ) | Channel Number          

SPEAKDATA
	LDI *PCI_FIFO_REG, FF_KEY;					;	1	; Get the PCI FIFO Status
	AND 00CH, FF_KEY;							;	1	; Keep only the PCI FIFO Full Flag Bits
	BNZD SPEAKDATA;							;	4	; If not Zero FIFO is Full, Recheck

	AND 0FFFH, FF_DATA;							;	0 	; Clear unwanted Channel number bits
	OR KSPECTRUM_KEY, FF_DATA;					; 	0	; Combine Spectrum Key to FF_DATA
	LDI @_EDSRec+rPixelCount, TMP_REG;				;	0	; Load Preset DOne Count to TMP_REG

	BRD SPEAK_RESTART
	LSH 12, TMP_REG;							;	4	; Shift Preset DOne Count left by 12
	OR TMP_REG, FF_DATA							;	0 	; COmbine it wiht FF_DATA
	STI FF_DATA, *PCI_FIFO_REG;					;	0	; Write it to the FIFO
	;----- Spectral Mapping Peak Processing --------------------------------------------------------

	;----- Preset Done Clean-Up Code --------------------------------------------------------------
	;----- FF_DATA contains the lower 15 bits of the Preset DOne Count
	;	3 3 2 2 | 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 | 1 1 
	;	1 0 9 8 | 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 | 1 0 9 8 7 6 5 4 3 2 1 0
	;	--------+---------------------------------+------------------------
	;	0 0 0 1 | Pixel ( aka Preset Done Count ) | Channel Number          
SPRESETDONE
	LDI *PCI_FIFO_REG, FF_KEY;					;	1	; Get the PCI FIFO Status
	AND 00CH, FF_KEY;							;	1	; Keep only the PCI FIFO Full Flag Bits
	BNZD SPRESETDONE;							;	4	; If not Zero FIFO is Full, Recheck


	STI FF_PRESET_DONE, @_EDSRec+rFPGA_Preset_Done;	;	0	; Set FPGA_Preset_Done_Flag in the DSP
	AND 0FFFFH, FF_DATA;						;	0	; Clear Unwanted Bits
	LDI FF_DATA, TMP_REG;						;	0	; Copy Current Pixel Count into TMP_REG

	ADDI 01H, TMP_REG;							;	1	; Increment TMP_REG (Pixel Count) by 1
	STI TMP_REG, @_EDSRec+rPixelCount;				;	1	; Store Pixel Count in EDSRec.Preset_Done_Cnt
	
	BRD SPEAK_RESTART							;	4	; Restart the Process
	LSH 12, FF_DATA;							;	0	; Shift Preset Done Count up by 12-bits
	OR FF_PRESET_DONE, FF_DATA;					;	0	; FF_DATA <= FF_DATA or FF_PRESET_DONE
	STI FF_DATA, *PCI_FIFO_REG;					;	0	; PCI_FIFO <= FF_DATA;	
	
	;----- Preset Done Clean-Up Code --------------------------------------------------------------

;----- Spectral Mapping Mode ------------------------------------------------------------------
;**********************************************************************************************

;**********************************************************************************************
;----- DSO Processing -------------------------------------------------------------------------
DSO_RESTART
	SUBI 1, LOOP_CNTR;							;	1	; Subtract 1 from Loop_Counter
	BZ QUIT_ISR;								;	4	; If Zero Exit

	;----- Code to check the FIFO Empty Flag --------------------
	LDI *EDS_FIFO_STAT_REG, FF_DATA;				;	1	; Read the Status - Forces a FIFO Read Enable
	AND 01H, FF_DATA;							;	1	; Keep only the LSB ( FIFO Empty Flag Active High )
	BNZ QUIT_ISR;								;	4	; If not Zero, FIFO is empty so Exit
	;----- Code to check the FIFO Empty Flag --------------------

	;----- Read the EDS FIFO, 20 bits worth, the Empty flag is bit 18
	LDI *EDS_FIFO_DATA, FF_DATA					

DSO_PCI_FF
	;----- Check the PCI FIFO Status to see for FIFO Full------------------------------------------
	LDI *PCI_FIFO_REG, TMP_REG;					;	1	; Get the PCI FIFO Status
	AND 00CH, TMP_REG;							;	1	; Keep only the PCI FIFO Full Flag Bits
	BNZD DSO_PCI_FF;							;	4	; If not FIFO is Full, Recheck
	;----- Check the PCI FIFO Status to see for FIFO Full------------------------------------------


	BRD DSO_RESTART;							;	4	; Otherise Go back to Beginning	
	STI FF_DATA, *PCI_FIFO_REG;					;	0	; Store Data (R0) into PCI FIFO
	LDI 01H, TMP_REG;							;	0	; Set 1 into R0
	STI TMP_REG, @_EDSRec+rDSO_PENDING;			;	0	; Set DSO Enabled Flag to 1
	;----- DSO Mode Processing -----------------------;   10    ;----------------------------------
	;----------------------------------------------------------------------------------------------
;----- DSO Processing -------------------------------------------------------------------------
;**********************************************************************************************



QUIT_ISR
	; Clear the EDSRec.EDS_IRQ flag
	LDI 0, TMP_REG;							;	1	; Set TMP_REG to 0
	STI TMP_REG, @_EDSRec+rEDS_IRQ;				;	1	; Set EDS_IRQ to Zero

	POP DP;									;	1	;
; POP AR7-AR0 from the Stack
	POP EDS_FPGA_REG;							;	1	;
	POP BASELINE_REG;							;	1	;
	POP EDS_FIFO_STAT_REG;						;	1	;
	POP MCA_BASE_REG;							;	1	;
	POP EDS_FIFO_DATA;							;	1	;
	POP PCI_FIFO_REG;							;	1	;

; Pop R7-R0 from the Stack
	POP KSPECTRUM_KEY;
	POP FF_PRESET_DONE;							;	1	;
	POP TMP_REG;								;	1	;
	POP LOOP_CNTR;								;	1	;
	POP FF_KEY;									;	1	;
	POP FF_DATA;								;	1	;
	POP IR0;									;	1	;

	RETS;									; Return from Subroutine
.endfunc


;**************************************************************************************************
; End of EDS_ISR.ASM
;**************************************************************************************************

