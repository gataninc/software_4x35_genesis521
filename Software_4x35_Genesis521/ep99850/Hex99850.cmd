/************************************************************/
/* File: hex99950.cmd										*/
/************************************************************/
ep99850.out
-i
-datawidth 32
-memwidth 32            
-romwidth 32
-o ep99850.hex
-iostrb 0
-strb0 001f1008h
-strb1 001f1028h
-e 880000h			/* Specify Scratch Memory Starting Address */
-map ep99850.hmp
-image  

/* -boot 				Specify to Insert Boot Sequece- Don't use for Serial	*/
-bootorg SERIAL		/* Specify Serial Boot Loader 	*/

/* Specify a ROMS directive to fill all non-consecutive blocks with zeroes  */
/* used in conjunction with the -image option above */
ROMS { 	
	MAIN : org = 880000h, fill = 0, romwidth = 32, len = 8000h 
} 
/************************************************************/
/* File: hex99930.cmd										*/
/************************************************************/
