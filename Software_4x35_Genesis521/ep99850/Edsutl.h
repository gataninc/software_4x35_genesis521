/********************************************************************************/
/*																*/
/*	Module:	EDSUTL.H												*/
/*																*/
/*	EDAX Inc														*/
/*																*/
/*  Author:		Michael Solazzi									*/
/*  Created:    3/25/99 												*/
/*  Board:      EDI-II 												*/
/*																*/
/*	Description:													*/
/*		This Header file contains all of the definitions and general purpose	*/
/*		routines to be used in the EDI-II Board.						*/
/*																*/
/*  History:														*/
/*																*/
/*		99/08/16 - MCS - Updated for EDI-II Rev B						*/
/********************************************************************************/
//#ifndef _EDSUTL_H 
//#define _EDSUTL_H

#include "defines.h"

// FPGA_GAIN_CONST is the value to scale the output to a UNITY gain.
#define FPGA_GAIN_CONST (float)0x4000   				// Floating point to USHORT conversion to the EDS FPGA

// Different Preset Definitions as seen by the Host
	#define EDS_ANAL_STOP				0x0
	#define EDS_ANAL_PRESET_REAL		0x01
	#define EDS_ANAL_PRESET_COUNT		0x02
	#define EDS_ANAL_PRESET_NONE		0x03
	#define EDS_ANAL_PRESET_LIVE		0x04
	#define EDS_ANAL_RESUME				0x05
	#define EDS_ANAL_RATEMETER			0x06
//	#define EDS_ANAL_PRESET_MAP			0x07		// 99/08/17 - MCS
//	#define EDS_ANAL_PRESET_FMAP		0x08		// 99/08/17 - MCS
	#define EDS_ANAL_PRESET_SMAP_REAL	0x0A		// 02/16/07 - MCS (Changed from 9 to A )
	#define EDS_ANAL_PRESET_SMAP_LIVE	0x0B		// 02/16/07 - MCS (Changed from 9 to A )
	#define EDS_ANAL_PRESET_REAL_PAMR	0x0C
	#define EDS_ANAL_PRESET_WDS_REAL	0x0D
	#define EDS_ANAL_PRESET_WDS_LIVE	0x0E


// Different Preset Definitions as seen by the FPGA
	#define FPGA_PRESET_NONE		0
	#define FPGA_PRESET_CLOCK		1
	#define FPGA_PRESET_LIVE		2
//	#define FPGA_PRESET_RATEMETER	4		// Not Used
	#define FPGA_PRESET_COUNT		5
	#define FPGA_PRESET_CLOCK_PAMR	6
	#define FPGA_PRESET_LSMAP_REAL	7
	#define FPGA_PRESET_LSMAP_LIVE	8
	#define FPGA_PRESET_WDS_REAL	9
	#define FPGA_PRESET_WDS_LIVE	10

// Define Discriminator Enable Bits
	#define DISC_SH			0x001
	#define DISC_H				0x002
	#define DISC_M				0x004
	#define DISC_L				0x008
	#define DISC_E				0x010
	#define DISC_SLOPE			0x020


// ROI Opcode Offsets
	#define MAX_ROI					49
	#define SCA_BITS 		8

//----- Control Word Constants ------------------------
	#define BIT_ENABLE				0x0001
	#define DSCA_POL				0x0002
	#define NOISE_SPEC_EN			0x0004
	#define EDS_XFER				0x0008
	#define RAM_ACCESS				0x0010
	#define SCA_EN					0x0020
	#define DSO_ENABLE				0x0040
	#define ADisc_En				0x0080
	#define ME_FET					0x0100
	#define SDD_PUR_EN				0x0200
	#define LS_VTHR_MODE			0x0400


	#define Proc_SCA			0
	#define Proc_FMAP			1
	#define Proc_Map			2
	#define Proc_TTL			3

//----- Analog Test Point Mux Select Constants ----------

	#define ATP_EVCH				0x000		// Channel 0
	#define ATP_BIT_DAC				0x100			
	#define ATP_DISC2				0x200		// EVCH 1
	#define ATP_DISC1				0x300		// SHDIsc
	#define ATP_PAMP				0x400
	#define ATP_CGAIN				0x500
	#define ATP_SHDISC				0x600

	#define ATP_AFILTER				0x800
	#define ATP_ADCP				0x900
	#define ATP_ADCN				0xA00
	#define ATP_BFILTER				0xB00
	#define ATP_AUX				0xC00
	#define ATP_DISC_INV			0xD00
	#define ATP_DISC_EVCH			0xE00

//----- EDS FPGA Parameter Offsets ----------------------------------------
	#define iADR_ID    				0x0100          //	ID Word
	#define iADR_STAT   			0x0101          //	Status Word
	#define iADR_CW   				0x0102          //	Control Word
	#define iADR_SN0				0x0103          //	Serial Number Bits 15:0
	#define iADR_SN1				0x0104          //	Serial Number Bits 31:16
	#define iADR_SN2				0x0105          //	Serial Number Bits 47:32
	#define iADR_SN3				0x0106          //	Serial Number Bits 63:48
	#define iADR_DSO_START			0x0107          //	Start DSO
	#define iADR_TP_SEL				0x0108          //	
	#define iADR_INT_EN				0x0109          //	Interrupt Enable/Disable
	#define iADR_CGAIN				0x010A          //	Coarse Gain (Analog)
	#define iADR_FGAIN				0x010B          //	Fine Gain (Digital)
	#define iADR_OFFSET   			0x010C          //	Offset ( Digital )
	#define iADR_CLIP_MIN			0x010D          //	
	#define iADR_CLIP_MAX			0x010E          //	
	#define iADR_DISC				0x010F          //  Disc1 DAC
	#define iADR_EVCH1				0x0110          //	Disc2 DAC
	#define iADR_EVCH0				0x0111          //	BIT DAC Directly
	#define iADR_BIT				0x0112          //	eV/Ch DAC
	#define iADR_TIME_START			0x0113          //	
	#define iADR_TIME_STOP			0x0114          //	
	#define iADR_TIME_CLR			0x0115          //	
	#define iADR_PRESET_MODE 		0x0116          //	
	#define iADR_PRESET_LO			0x0117          //	
	#define iADR_PRESET_HI  			0x0118          //	
	#define iADR_PRESET_ACK			0x0119          //
	#define iADR_RTEM_INI			0x011A          //
	#define iADR_RTEM_CMD_IN			0x011B          //
	#define iADR_RTEM_CMD_OUT		0x011C          //
	#define iADR_RTEM_HCMR			0x011D          //
	#define iADR_RTEM_HC_RATE_LO		0x011E          //
	#define iADR_RTEM_HC_RATE_HI		0x011F          //
	#define iADR_RTEM_HC_THRESHOLD	0x0120          //
	#define iADR_RAMA				0x0121          //	Look-Up Ram Address
	#define iADR_SCA				0x0122          //	Analog SCA/Digital ( Direct )
	#define iADR_SCA_LU				0x0123          //	Digital SCA Look-Up Data
	#define iADR_DSCA_PW			0x0124          //	Digital SCA Pulse Width
	#define iADR_ROI_LU				0x0125          //	ROI Lookup Data 
//	#define iADR_PA_RESET_NTHR		0x0126		 // AD Multiplier
//	#define iADR_PA_RESET_PTHR		0x0127	
	#define iADR_FIFO_LO			0x0128          //	FIFO Low Word
	#define iADR_FIFO_HI			0x0129          //	FIFO High Word
	#define iADR_FIFO_MR			0x012A          //	FIFO Master Reset
	#define iADR_DISC_EN			0x012B          //	Discriminator Enables
	#define iADR_TC_SEL				0x012C          //	Adaptive Mode/Time #define Select
	#define iADR_DLEVEL0			0x012D          //	High
	#define iADR_DLEVEL1			0x012E          //	Medium
	#define iADR_DLEVEL2			0x012F          //	Low
	#define iADR_DLEVEL3			0x0130          //	Energy (BLM)
	#define iADR_BLANK_MODE			0x0131
	#define iADR_BLANK_DELAY		0x0132
	#define iADR_BLANK_WIDTH		0x0133
	#define iADR_PS					0x0134

	#define iADR_BIT_PEAK1			0x0135          //	BIT Peak 1 Level
	#define iADR_BIT_PEAK2			0x0136          //	BIT Peak 2 Level
	#define iADR_BIT_CPS			0x0137          //	BIT CPS
	#define iADR_BIT_PK2D			0x0138          //	BIT Peak 2 Delay from Peak 1
	#define iADR_TLO  				0x0139          //	DPP-2 Temperature Low Pulse Width
	#define iADR_THI				0x013A          //	DPP-2 Temperature Hi Pulse Width
	#define iADR_DU0_TLO			0x013B          //	
	#define iADR_DU0_THI			0x013C          //	
	#define iADR_DU1_TLO			0x013D          //	
	#define iADR_DU1_THI			0x013E          //	
	#define iADR_IN_CPSL			0x013F          //	
	#define iADR_IN_CPSH			0x0140          //	
	#define iADR_OUT_CPSL			0x0141          //	
	#define iADR_OUT_CPSH			0x0142          //	
	//#define iADR_PAMRL				0x0143          //
	//#define iADR_PAMRH				0x0144          //
	#define iADR_LTIME_LO			0x0145          //	
	#define iADR_LTIME_HI			0x0146          //	
	#define iADR_CTIME_LO			0x0147          //	
	#define iADR_CTIME_HI			0x0148          //	
	#define iADR_RAMP_LO			0x0149
	#define iADR_RAMP_HI			0x014A
	#define iADR_RESET_WIDTH			0x014B
	#define iADR_NET_CPSL			0x014C          //	
	#define iADR_NET_CPSH			0x014D          //	
	#define iADR_NET_NPSL			0x014E          //	
	#define iADR_NET_NPSH			0x014F          //	
	#define iADR_RMS_OUT			0x0150
	#define iADR_RMS_THR			0x0151

	//#define iADR_NET_RPSL			0x0150
	//#define iADR_NET_RPSH			0x0151
	#define iADR_PA_TIME_LO			0x0152
	#define iADR_PA_TIME_HI			0x0153
	#define iADR_AUTO_SHDELAY		0x0154

	#define iADR_RTEM_SEL			0x0155
	#define iADR_RTEM_LOW_SPEED		0x0156
	#define iADR_RTEM_MED_SPEED		0x0157
	#define iADR_RTEM_HIGH_SPEED		0x0158
	#define iADR_RTEM_CNT_L			0x0159
	#define iADR_RTEM_CNT_H			0x015A

	#define iADR_SH_Disc_Cnts		0x015B
	#define iADR_H_DISC_CNTS			0x015C
	#define iADR_M_DISC_CNTS			0x015D
	#define iADR_L_DISC_CNTS			0x015E
	#define iADR_E_DISC_CNTS			0x015F
	#define iADR_DECAY				0x0160

	
	#define iADR_BIT_ADC			0x0161	//	0x0160
	#define iADR_FIR_MR				0x0162
	#define iADR_RTEM_CNT_10_L		0x0163
	#define iADR_RTEM_CNT_10_H		0x0164
	#define iADR_RTEM_CNT_25_L		0x0165
	#define iADR_RTEM_CNT_25_H		0x0166
	#define iADR_RTEM_CNT_75_L		0x0167
	#define iADR_RTEM_CNT_75_H		0x0168
	#define iADR_RTEM_CNT_90_L		0x0169
	#define iADR_RTEM_CNT_90_H		0x016A
	#define iADR_RTEM_CNT_100_L		0x016B
	#define iADR_RTEM_CNT_100_H		0x016C
	#define iADR_RTEM_CNT_110_L		0x016D
	#define iADR_RTEM_CNT_110_H		0x016E
	#define iADR_RTEM_CNT_MAX_L		0x016F
	#define iADR_RTEM_CNT_MAX_H		0x0170
	#define iADR_RTEM_CMD_MID_IN		0x0171
	#define iADR_RTEM_CMD_MID_OUT		0x0172
	#define iADR_RTEM_CMD_WD_MR		0x0173
	#define iADR_BIT_FADC			0x0174	//	0x0161

	#define iADR_DSO_TRIG			0x0175
	#define iADR_DSO_INT			0x0176
	#define iADR_SHDISC_DLY_SEL		0x0177
	#define iADR_FIR_DLY_LEN			0x0179
	#define iADR_FIR_DLY_INC			0x017A
	#define iADR_DISC_EVCH			0x017B
	#define iADR_PRESET_DONE_CNT		0x017C
	#define iADR_PRESET_DONE_CNT2		0x017D
	
	#define iADR_RESET_MR    		0x017E
	#define iADR_AVG_CODE			0x017F
	
	#define ST_RTEM_INIT 			0x0
	#define ST_RTEM_ERROR			0x1
	#define ST_RTEM_STOPPED			0x2
	#define ST_RTEM_IN				0x3
	#define ST_RTEM_OUT				0x4
	#define ST_RTEM_MID				0x5
	#define ST_RTEM_WD				0x6
	#define ST_RTEM_MOVE_IN   		0x7
	#define ST_RTEM_MOVE_OUT			0x8
	#define ST_RTEM_MOVE_MID_IN		0x9
	#define ST_RTEM_MOVE_MID_OUT		0xA
	#define ST_RTEM_MOVE_WD			0xB
	#define ST_RTEM_MOVE_ERROR		0xC
	#define ST_RTEM_NO_POWER			0xD
	#define ST_RTEM_HI_COUNT			0xE

	#define RTEM_Low_Speed			0xE800
	#define RTEM_High_Speed			0xF800			

	// #define UMS_Init_Speed		0xF000
	//#define UMS_Init_Speed			0xF000
	//#define UMS_Low_Speed			0xF200
	//#define UMS_High1_Speed		0xFB00
	//#define UMS_High2_Speed		0xFE00
	#define RTEM_MAX_RANGE			170000

	// MSEL = 25
	#define UMS_Init_Speed 	149
	#define UMS_Low_Speed 	127
	#define UMS_Med_Speed 	63 
	#define UMS_High_Speed 	23

//	#define UMS_Low_Speed 	0xFC80  
//	#define UMS_Med_Speed 	0xFEF8
//	#define UMS_High_Speed 	0xFF80

//----- EDS FPGA Parameter Offsets ----------------------------------------

// Define a structure
typedef struct {				// Offset 
	// EDS & TMR ISR Records
	USHORT	FPGA_Preset_Done;	//  0		Digital Zero
	USHORT	EDSXFer;			//  1		EDS Transfer Mode Flag
	USHORT	EDS_Stat;			//  2		Analyzer Status
	USHORT	DSO_PENDING;		//  3 		DSO Occured
	ULONG	TMR_ISR_CNT;		//  4
	UCHAR	EDS_IRQ;			//  5		EDS Interrupt is Enabled
	ULONG	EDS_ISR_CNT;		//  6
	USHORT	CLIP_MIN;			//  7
	USHORT	CLIP_MAX;			//  8
	USHORT	Preset_Mode;		//  9 		FPGA Preset Mode
	USHORT  	Preset_Done_Cnt;	//  A
	USHORT	Preset_Done_Points;	//  B
	USHORT	DSO_Index;		//  C
	USHORT 	FFT_Mode;			//  D
	USHORT	DSO_Enable;		//  E
	USHORT	PixelCount;		//  F

	// EDS & TMR ISR Records
	USHORT	EDS_Mode;			// 			Analyzer Mode
	USHORT	EDS_DEF_ROIS;		// 			Number of Valid Defined ROIs
	USHORT    Max_Fir_Cnt;
	USHORT	Max_Fir[4];
	USHORT	RTEM_Init;
	USHORT	RTEM_Target;
	USHORT	ADisc;			// AutoDisc in process
	USHORT	ASHBit;			// Which AutoDisc bit
	USHORT    ADiscVal[5];		// AutoDisc Value
	USHORT	EDX_ANAL;
	USHORT Reset_FIR;
} EDS_TYPE;

// Structure to Define a ROI
typedef struct {
	USHORT Start;		// Starting Channel
	USHORT End;		// Ending Channel
	USHORT Sca;		// SCA Mask
	UCHAR  Enable;		// ROI Enabled/Disabled = 1 == Enabled
	UCHAR  Defined;	// ROI is Defined
} ROI_TYPE;

//----- Macro Declarations 
// #define WriteEDS( addr, data )	( *(ULONG *)( EDS_BASE + (addr) ) = data )		// Write the data directly
// #define ReadEDS( addr )	(USHORT)(( *(ULONG*)( EDS_BASE + (addr) )) & 0xFFFF )

//------ EDS Initialization Routine --------------------------------------------------------
void		EDS_Init( void );
void		WriteEDS( USHORT addr, USHORT data );
USHORT	ReadEDS( USHORT addr );
void 	WriteDEDS( USHORT addr, ULONG data );
ULONG 	ReadDEDS( USHORT addr );
void		Write8420( USHORT addr, USHORT data );
USHORT	ReadBITADC( void );
void 	WriteEDSFIFO( ULONG data );
ULONG	ReadEDSFIFO( void );
void		Set_Time_Const( void );
void		SetROISCARM( int ROI_NUM );
void		SetAnalMode( void );
void		DefineROI( void );
void		PSetROI( void );
void		ClrROI( void );
void		DefineSCA( void );
void		Set_RTEM_Range( ULONG cnts );
// void		rtem_init( void );
void		Auto_SHDisc( void );
void		Auto_Disc( void );
void		Set_EvCh( void );
void		Set_Blank_Factor( void );
void 		SetNonLinearRam( USHORT data );

//#endif

/********************************************************************************/
/* END OF EDSUTL.H	    													    */
/********************************************************************************/
