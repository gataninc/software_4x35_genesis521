/************************************************************/
/* File: lnk99930.cmd										*/
/************************************************************/
/* Command file to run linker defining used object files, 	*/
/* Memory usage for Data and Program storage				*/
-c
-o ep99850.out
-m ep99850.map
-l rts30.lib

div_f30.obj
edi3vectors.obj
eds_isr.obj
tmr_isr.obj
edsutl.obj
dpp2utl.obj
dpp2bit.obj
ep99850.obj


/*Define Memory Usage for Program and Data Storage	*/
MEMORY
{
	INIT_RAM0 : origin = 87FE00H, length = 100H
	INIT_RAM1 : origin = 87FF00H, length = 100H
	EXT_RAM0  : origin = 880000H, length = 3000H	/* Code				*/
	EXT_RAM1  : origin = 883000H, length = 1000H	/* Vector Table		*/
	EXT_RAM2  : origin = 884000H, length = 2000H	/* Histogram		*/
	EXT_RAM3  : origin = 886000H, length = 2000H	/* More Data        */
	DP_RAM    : origin = 900000H, length = 2000H	/* Dual Port RAM	*/
}

SECTIONS
{
	.text: > EXT_RAM0
	.cinit: > EXT_RAM0
	.data: > EXT_RAM0
	.bss: > EXT_RAM0
	.stack: > EXT_RAM0
	.const: > EXT_RAM0
	vectors: > EXT_RAM1
}
/************************************************************/
/* File: lnk99930.cmd										*/
/************************************************************/

