/********************************************************************************/
/*																*/
/*	Module:	DPP2BIT.H												*/
/*																*/
/*	EDAX Inc														*/
/*																*/
/*  Author:		Michael Solazzi									*/
/*  Created:    3/25/99 												*/
/*  Board:      EDI-II 												*/
/*																*/
/*	Description:													*/
/*		This Code file contains all of the Built-In Test Functions for the	*/
/*		EDI-II Board.												*/
/*																*/
/*  History:														*/
/*		99/08/16 - MCS - Updated for EDI-II Rev B						*/
/*																*/
/********************************************************************************/

void 	DPP_BIT_Test( void );
ULONG	DPRAM_DATA_TEST( void );
ULONG	DPRAM_ADDRESS_TEST( void );
ULONG	FPGA_TEST( int target );
ULONG	FIFO_DATA_TEST( void );
ULONG	FIFO_FLAG_TEST( void );
ULONG	Ram_Data_Test( int target );
ULONG	Ram_Address_Test( int target );
ULONG	Ram_Memory_Test( int target );
ULONG	Dac_Test( int target );
void	BIT_SCA_GEN( void );
void	BIT_DAC_GEN( void );
/********************************************************************************/
/* END OF DPP2BIT.H													*/
/********************************************************************************/
