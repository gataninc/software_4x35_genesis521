TMS320C3x/4x C Compiler Shell           Version 5.10
Copyright (c) 1987-1997 Texas Instruments Incorporated

Usage:  cl30 [-options] filenames... [-z link_options...]

Options:
  -@<fn>  read options from file fn     -q      quiet
  -c      no linking (negates -z)       -qq     super quiet
  -dNAME  pre-define NAME               -s      source interlist
  -g      symbolic debugging            -ss     C source-code interlist
  -ga     write out unref'd externs     -uNAME  undefined NAME
  -i<dir> #include search path          -v#     target processor version
  -k      keep .asm file                -z...   link, options follow
  -n      compile only (no asm)       
  -abs    absolute listing file (only after -z)                                

Parser options:  -p<options...>
  -pe     'E' errors are warnings       -pn     no #line directives
  -pf     generate prototype file       -po     preprocess only
  -pg     enable trigraph expansion     -pr     generate error listing
  -pk     K&R compatability             -pwN    warning level N
  -pl     generate .pp file             -px<fn> for -pm, name output file
  -pm     program mode compilation    

Type checking options:  -t<options...>
  -tf     relax prototype checking      -tp     relax pointer combinations

Runtime model options:  -m<options...>
  -ma     assume aliased variables exist
  -mb     big memory model
  -mc     perform fast float-to-int conversions
  -mf     use far pointers
  -mi     interrupts, avoid RPTS loops
  -ml     runtime support assembly calls use far calls
  -mm     use hardware MPYI for integer multiply
  -mn     normal optimization in addition to debug info
  -mp     perform speed optimizations at cost of increased code size
  -mr     register parameter passing convention
  -ms     assume all memory accessable when optimizing
  -mt     generate Ada compatible frame structure
  -mtc    generate Tartan LAJ compatible function prolog

Optimizations:  -o<options...>
  -o0     level 0: register optim       -o      optimize (level 2)
  -o1     level 1: +local optim         -oi<xx> auto inlining threshold
  -o2     level 2: +global optim                 
  -o3     level 3: +interproc optim   
  -os     optimizer source comment interlisting                                
  -ou     allow repeat block operations with unsigned counts                   

Library function assumptions:  -ol<x>
  -ol0    file alters RTS lib func      -ol(2)  file defines no RTS lib func
  -ol1    file contains RTS lib func  

Call graph assumptions:  -op<x>
  -op0    other files may call funcs and change vars defined here
  -op1    other files do not call functions in this file
  -op2    other files do not call funcs or change vars defined here
  -op3    other files do not change variables defined here

Optimizer info levels (requires -o3):  -on<x>
  -on0    no info file                  -on2    create verbose info file
  -on1    create info file            

Inline function expansion:  -x<options...>
  -x0     disable all                   -x(2)   enable all
  -x1     built-in functions only     

Assembler options:  -a<options...>
  -aa     use absolute listing option   -al     assembly listing file
  -adNAME pre-define NAME               -as     keep local symbols
  -ahc<f> .copy file f                  -auNAME undefine NAME
  -ahi<f> .include file f               -ax     cross reference file

File and Directory specifiers:  -f...
  -fa<file> this file is an asm file       (default for .asm/.s*)
  -fc<file> this file is a C file          (default for .c/no ext)
  -fo<file> this file is an obj file       (default for .o*)
  -fr<dir>  object file directory          (default is .)
  -fs<dir>  asm file directory             (default is .)
  -ft<dir>  temp file directory            (default is .)

Default file extensions:  -e...
  -ea<.ext> extension for assembly files    (default is .asm)
  -eo<.ext> extension for object files      (default is .obj)

Linker options:    (all options following -z go to the linker)
  -a        absolute output             -m<file>  map file name
  -ar       relocatable output          -o<file>  output file name
  -c        ROM initialization          -q        quiet
  -cr       RAM initialization          -r        relinkable output
  -e<sym>   entry point                 -s        strip symbol table
  -f<val>   fill value                  -u<sym>   undefine sym
  -g<sym>   sym is global even if -h    -v#       generate COFF format 0,1,2
  -h        global symbols static       -w        warn about output sections
  -i<dir>   library search path         -x        exhaustively search libs
  -l<lib>   library name              

  -heap 0x<size>   set heap size
  -stack 0x<size>  set stack size
  -heap8 0x<size>  set 8-bit heap size (C32 only)
  -heap16 0x<size> set 16-bit heap size (C32 only)

(Linking is enabled only if -z is used and -c is not used)

Environment Variables
  set C_OPTION=<options> to set default options
  set C_DIR=<dirs>       to set cpp and linker search paths
  set A_DIR=<dirs>       to set asm and linker search paths
  set TMP=<dir>          to set directory for temp files

>> no source files
