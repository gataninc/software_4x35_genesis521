;************************************************************/
; File: Emuinit.cmd											*/
;************************************************************/
;
; TMS320C30 MEMORY MAP, NEEDS TO BE CUSTOMIZED FOR USER SYSTEM
; Note: If the debugger comes up with invalid address messages then you
;	probably need to add to the maps below to match your system.
;
; The following line should be modified to set external busses for system
alias setbus, "e *0x808060 = 0x38; e *0x808064 = 0x1000; e iof = 0xa"
alias myreset, "reset; setbus"
;
MR  				; reset memory map
MA 0x809800,0x000800,RAM 	; RAM0 and RAM1
MA 0x808000,0x000010,IOPORT ; DMA memory mapped on-chip peripheral
MA 0x808020,0x000010,IOPORT ; TIMER0 memory mapped on-chip peripheral
MA 0x808030,0x000010,IOPORT ; TIMER1 memory mapped on-chip peripheral
MA 0x808040,0x000010,IOPORT ; SPORT0 memory mapped on-chip peripheral
MA 0x808050,0x000010,IOPORT	; SPORT1 memory mapped on-chip peripheral (not available on C31)
MA 0x808060,0x000001,IOPORT	; XBUSCTL (not available on C31)
MA 0x808064,0x000001,IOPORT ; STRB0
MA 0x808068,0x000001,IOPORT	; STRB1
MA 0x880000,0x008000,RAM	; DSP Scratchpad Memory
MA 0x900000,0x000100,RAM	; Dual Port RAM1 ( At Boot 3 )
MA 0x902000,0x000100,RAM	; Dual Port RAM2 ( At Boot 3 )
MA 0x904000,0x000100,RAM	; Glue Memory/PCI FIFO Write...
MA 0x906000,0x000100,RAM	; EDS FPGA (Read/Write )
MA 0x908000,0x000002,RAM	; EDS FIFO and Status
fill 0x808064, 0x1, 0x001f1068	; Set DSP Wait States for Strobe 0
fill 0x808068, 0x1, 0x001f1028	; Set DSP Wait States for Strobe 1 ( 1-wait states )

MAP ON

; ****************************************************************************
; Point the PC to an internal memory location to minimize "invalid memory msg".
; This line should be removed after you add memory maps to match your system.
; e pc = 0x809800
; ****************************************************************************
;reset
load ep99850.out

;Setup Some Window Sizes and Positions
win CPU
size 30, 18
move 95,1
win DISASSEMBLY
size 95,50
win FILE
size 125,47
win COMMAND
move 0,48
echo TMS320C3x EMUINIT.CMD HAS BEEN LOADED FOR DPP-II
go main
;************************************************************/
; File: Emuinit.cmd											*/
;************************************************************/




