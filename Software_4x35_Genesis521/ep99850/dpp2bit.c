/********************************************************************************/
/*																				*/
/*	Module:	DPPBIT.C															*/
/*																				*/
/*	EDAX Inc																	*/
/*																				*/
/*  Author:		Michael Solazzi													*/
/*  Created:    3/25/99 														*/
/*  Board:      EDI-II 															*/
/*																				*/
/*	Description:																*/
/*		This Code file contains all of the Built-In Test Functions for the		*/
/*		EDI-II Board.															*/
/*																				*/
/*  History:																	*/
/*																				*/
/*		99/08/16 - MCS - Updated for EDI-II Rev B								*/
/********************************************************************************/

#include "bus32.h"
#include "timer30.h"
#include "serprt30.h"
#include "defines.h"
#include "dpp2utl.h"
#include "edsutl.h"
#include "dpp2bit.h"

#define INITIALIZE_TIME_OUT 0	// Run-Time
// #define INITIALIZE_TIME_OUT 1	// Emulator
#define MAX_LOOP		1000000L
#define FF_FULL_DEPTH	16384

//---------------------------------------------------------------------------------------
// General Purpose Routines for hte EDS and SEM Sub-Sections
//---------------------------------------------------------------------------------------

void DPP_BIT_Test()
{
	int i;
	int done;
	USHORT data;
	ULONG data1;
	ULONG data2;
	USHORT addr;

	//**** BIT: Start Sequence *********************************************************
	//----- Start of Initialization Tests ----------------------------------------------
	// Now Wait for Driver to Write all a PCI's Device ID to DPPRAM before continuing
	// Driver must determine that the DSP has booted before continuing with the tests
	i = MAX_LOOP;
	do{
		data = ReadDPRAM( dADR_DSP_VER );
		data1 = data & 0xFFF0;
		#if INITIALIZE_TIME_OUT == 0
			i = 2;
		#endif 
	}
	while(( data1 != 0x1970 ) && ( --i > 0 ));

		
	// First test the DPRAM and Store the results, after the test
	// update the DPRAM with the Results
	data1 = DPRAM_DATA_TEST();
	data2 = DPRAM_ADDRESS_TEST();

	// Now Clear the DPRAM
	for( i=0; i<=0x1FF;i++){
		WriteDPRAM( (USHORT)i, 0x0 );
	}

	WriteDPRAM( 0x003E, (USHORT)DSP_VER );			// Copy the DSP Version to the DPRAM
	WriteDPRAM( 0x003F, (USHORT)ReadEDS(iADR_ID));	// Copy the EDS FPGA Version to the DPRAM
	WriteDPRAM( dADR_SCA_GEN, 0x0 );
	WriteDPRAM( dADR_DAC_GEN, 0x0 );

	done=0;
	i=0;
	do{
		switch(i) {
			case 0x00: data1 = data1;			break;	// DPRAM Data Test
			case 0x01: data1 = data2;			break;	// DPRAM Address Test
			case 0x02: data1 = FPGA_TEST(0);		break;	// GLUE Test Register
			case 0x03: data1 = FPGA_TEST(1);		break;	// EDS FPGA TEST
			case 0x04: data1 = FIFO_DATA_TEST();	
					#if NO_FIFO_TEST == TRUE
						data1 = 0;
					#endif
					break;	// EDS FIFO DATA
			case 0x05: data1 = FIFO_FLAG_TEST(); 	
					#if NO_FIFO_TEST == TRUE
						data1 = 0;
					#endif
					break;	// E-F	EDS FIFO FLAG
			case 0x06: data1 = Ram_Data_Test(0);	break;	// ROI/SCA RAm Data
			case 0x07: data1 = Ram_Address_Test(0); break; 	// ROI/SCA Ram Address
			case 0x08: data1 = Ram_Memory_Test(0);	break;  	// ROI/SCA Ram Memory
			case 0x09: data1 = Ram_Data_Test(1);	break;	// ROI DEF RAm Data
			case 0x0A: data1 = Ram_Address_Test(1);	break;	// ROI DEF Ram Address
			case 0x0B: data1 = Ram_Memory_Test(1); 	break;	// ROI DEF Ram Memory

			case 0x0C:								// Fine Gain RAM Data		- No longer used
			case 0x0D:								// Fine Gain RAM Address	- No Longer Used
			case 0x0E:								// Fine Gain RAM Memory		- No Longer U
			case 0x0F:								// Zero RAM Data			- No longer used
			case 0x10:								// Zero RAM Address			- No Longer Used
			case 0x11:								// Zero RAM Memory			- No Longer U
				data1 = 0; 
				break;

			case 0x12: data1 = Dac_Test(0x0);		break;				// Disc 1
			case 0x13: data1 = Dac_Test(0x1);		break;				// Disc 2
			case 0x14: data1 = Dac_Test(0x2);		break;				// eV/ch
			case 0x15: data1 = Dac_Test(0x3);		break;				// BIT
			case 0x16: data1 = Dac_Test(0x4);		break;				// Post Amp 1X Gain
			case 0x17: data1 = Dac_Test(0x5);		break;				// Post Amp 2X Gain
			case 0x18: data1 = Dac_Test(0x6);		break;				// Coarse Gain
			case 0x19: data1 = Dac_Test(0x7);		break;				// Bessel Filter
			case 0x1A: data1 = Dac_Test(0x8);		break;				// Anti-Alias Filter
			case 0x1B: data1 = Dac_Test(0x9);		break;				// ADC Positive Input
			case 0x1C: data1 = Dac_Test(0xA);		break;				// ADC Negative Input
			case 0x1D: data1 = Dac_Test(0xB);		break;				// Disc Ev/Ch
			case 0x1E: data1 = Dac_Test(0xC);		break;				// Disc Inv
			// Could keep adding BIT Tests here.
			default : done = 1; break;
		}
		addr = ( i << 1 ) + 0x40;
		WriteDPRAM( addr, (USHORT)data1);
		addr ++;
		WriteDPRAM( addr, (USHORT)(data1 >> 16 ));
		i++;
		data1 = 0;
	} while( done == 0 );
	//-----------------------------------------------------------------------------------------------
	// End of Initialization Test
	// ADR_DSP_VER i s used instead of ADR_OPCODE to prevent any interrupts
	WriteDPRAM( dADR_DSP_VER, 0x1234 );						// Write all 1234 to DPRAM at Loc 0 

	// Now Wait for Driver to Write all 1's to Loc 0 of DPPRAM before continuing
	i = MAX_LOOP;
	do{
		data = ReadDPRAM( dADR_DSP_VER );
		#if INITIALIZE_TIME_OUT == 0
			i = 2;
		#endif 
	}
	while(( data == 0x1234 ) && ( --i > 0 ));

	//-----------------------------------------------------------------------------------------------
	// Now allow for the driver to perform the PCI FIFO Tests
	// Reset the PCI FIFO
	data = ReadGlue( iADR_GLUE_ID );
	i = 100;
	do{
		WriteGlue( iADR_GLUE_FF_MR, 0x0 );						// Reset the PCI FIFO
		data = ReadGlue( iADR_GLUE_FF_CNT );
		data = ReadGlue( iADR_GLUE_STATUS ) & 0xF;
	} while(( data != PFF_EF ) && ( --i > 0 ));

	data = ReadGlue( iADR_GLUE_FF_CNT );

		// Write data to the FIFO until it is full
	i=0;
	do{
		WritePCIFifo( i );
		data = ReadGlue( iADR_GLUE_FF_CNT );
		data = ReadGlue( iADR_GLUE_STATUS ) & 0xF;
	} while(( data != PFF_FF ) && ( ++i < 0xFFFF ));

	data = ReadGlue( iADR_GLUE_FF_CNT );
	if( data < 16384 ){
		data = 1;
	}

	// Now Wait for the Driver to Acknowledge and check the data
	WriteDPRAM( dADR_DSP_VER, 0x1234 );						// Write all 1234 to DPRAM at Loc 0 
	i = MAX_LOOP;
	do{
		data = ReadDPRAM( dADR_DSP_VER );
		#if INITIALIZE_TIME_OUT == 0
			i = 2;
		#endif 
	}
	while(( data == 0x1234 ) && ( --i > 0 ));

	// Now start the BIT TEST PATTERN
	// Reset the PCI FIFO
	WriteGlue( iADR_GLUE_FF_MR, 0x0 );						// Reset the PCI FIFO

	// Write the Test Pattern
	for(i=0;i<=31;i++){
		data = ( 1 << i );
		WritePCIFifo( data );
		data = ReadGlue( iADR_GLUE_FF_CNT );
		data = ReadGlue( iADR_GLUE_STATUS ) & 0xF;
	}

	// Wait for the Driver to Acknowledge
	WriteDPRAM( dADR_DSP_VER, 0x1234 );						// Write all 1234 to DPRAM at Loc 0 
	i = MAX_LOOP;
	do{
		data = ReadDPRAM( dADR_DSP_VER );
		#if INITIALIZE_TIME_OUT == 0
			i = 2;
		#endif 
	}
	while(( data == 0x1234 ) && ( --i > 0 ));
	
	for(i=0;i<0x1FF;i++) { 
		WriteDPRAM((USHORT)i,0x0); 
	}
	// Final hand-shaking for the Driver before final boot
	WriteDPRAM( dADR_DSP_VER, 0x1234 );						// Write all 1234 to DPRAM at Loc 0 
	i = MAX_LOOP;
	do{
		data = ReadDPRAM( dADR_DSP_VER );
		#if INITIALIZE_TIME_OUT == 0
			i = 2;
		#endif 
	}
	while(( data == 0x1234 ) && ( --i > 0 ));
}
//-----------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//------- Routine to Test the Dual Port RAM ---------------------------------------------
ULONG DPRAM_DATA_TEST( void )
{
	
	int i;
	USHORT wtmp;
	USHORT rtmp;
	ULONG error = 0;
	
	//----- Check Data bits to DSP Scratch RAM -------------------------------
	for( i=0;i<=15;i++){ 
		wtmp = 1 << i;
		WriteDPRAM( 0, wtmp );
		rtmp = ReadDPRAM( 0 );
		if( wtmp != rtmp ){
			error |= wtmp;
		}
	}				// for...
	
	return( error );
}
//------- Routine to Test the Dual Port RAM ---------------------------------------------
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
ULONG DPRAM_ADDRESS_TEST( void )
{
	int i, j;
	USHORT wtmp;
	USHORT rtmp;
	ULONG error = 0;

	for(j=0;j<=1;j++){
		for(i=0;i<=7;i++){
			wtmp = ( j << 8 ) | ( 1 << i );
			WriteDPRAM(wtmp, wtmp );
		}
	}

	for(j=0;j<=1;j++){
		for(i=0;i<=7;i++){
			wtmp = ( j << 8 ) | ( 1 << i );
			rtmp = ReadDPRAM( wtmp );
			if( wtmp != rtmp ){
				error |= wtmp;
			}
		}
	}
	//----- Check Data bits to DSP Scratch RAM -------------------------------

	return( error );
}
//------- Routine to Test the Dual Port RAM ---------------------------------------------
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
ULONG FPGA_TEST( int target )
{
	USHORT wdata;
	USHORT rdata;
	ULONG error = 0;
	int i;

	for(i=0;i<=15;i++){
		wdata = 1 << i;
		if( target == 0 ){
			WriteGlue( iADR_GLUE_TREG, wdata );
			rdata = ReadGlue( iADR_GLUE_TREG );
		} else {
			WriteEDS( iADR_PRESET_LO, wdata );
			rdata = ReadEDS( iADR_PRESET_LO );
		}
		if( wdata != rdata ){
			error |= wdata;
		}
	}
	return( error );
}
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//------- Routine to Test the EDS FIFO --------------------------------------------------
ULONG FIFO_DATA_TEST( void )
{
	int i;
	ULONG error = 0;
	ULONG wtmp;
	ULONG rtmp;

	wtmp = ReadEDS( iADR_CW );
	wtmp |= BIT_ENABLE;							// force Bit Enable
	WriteEDS( iADR_CW, (USHORT) wtmp );

	WriteEDS( iADR_TP_SEL, 0x03 << 12 );
	WriteEDS( iADR_FIFO_MR, 0x0 );								// Reset EDS FIFO

	for( i=0;i<=17;i++) {
		wtmp = ( 1 << i );
		WriteEDSFIFO( wtmp );
	}

	for( i=0;i<=17;i++) {
		wtmp = ( 1 << i );
		// Now Read the FIFO
		rtmp = ReadEDSFIFO();
		
		if( wtmp != rtmp ){
			error |= wtmp;
		}
	}

	wtmp = ReadEDS( iADR_CW );
	wtmp &= ~BIT_ENABLE;							// force Bit Enable
	WriteEDS( iADR_CW, (USHORT) wtmp );
	return( error );
}
//------- Routine to Test the EDS FIFO --------------------------------------------------
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//------- Routine to Test the EDS FIFO --------------------------------------------------
ULONG FIFO_FLAG_TEST( void )
{
	int i, j;
	ULONG error = 0;
	USHORT tmp;
	ULONG wtmp;

	wtmp = ReadEDS( iADR_CW );
	wtmp |= BIT_ENABLE;							// force Bit Enable
	WriteEDS( iADR_CW, (USHORT) wtmp );

	WriteEDS( iADR_TP_SEL, 0x03  << 12 );
	WriteEDS( iADR_FIFO_MR, 0x0 );								// Reset EDS FIFO

	// Now Write until the fifo is half full
	for(j=0;j<=1;j++){
		for(i=0;i<FF_FULL_DEPTH;i++){
			switch( j ){
				case 0 :
					tmp = ReadEDS( iADR_STAT ) & 0x3;			// Read the Status Flags
					WriteEDSFIFO( i );
					break;
				case 1:
					tmp = ReadEDS( iADR_STAT ) & 0x3;			// Read the Status Flags
					wtmp = ReadEDSFIFO();
					break;
			}

			switch( tmp ) {
				case 0 :								// FIFO CONTAINS DATA
						if(( i == FF_FULL_DEPTH ) || ( i == 0 )){
							if( j == 0 ){
								error |= 1;
							} else {
								error |= 0x100;
							}
						}	
						break;

				case 1:									// FIFO EMPTY INDICATION
						if((( j == 0 ) && ( i > 0 ))
							|| (( j == 1 ) && ( i < FF_FULL_DEPTH ))){
								if( j == 0 ){
									error |= 2;
								} else {
									error |= 0x200;
									error |= ( i & 0xFF );
								}
						}
						break;

				case 2:									// FIFO FULL INDICATION
						if((( j == 0 ) && ( i < FF_FULL_DEPTH )) 
						|| (( j == 1 ) && ( i > 0 ))) 
						{	
							if( j == 0 ){
								error |= 4;
							} else {
								error |= 0x400;
							}

						}	
						break;

				case 3:									// Both Flags active - Invalid
							if( j == 0 ){
								error |= 8;
							} else {
								error |= 0x800;
							}
						break;
			}						// switch
		}							// for i...
	}								// for j...

	wtmp = ReadEDS( iADR_CW );
	wtmp &= ~BIT_ENABLE;							// force Bit Enable
	WriteEDS( iADR_CW, (USHORT) wtmp );
	return( error );
}
//------- Routine to Test the EDS FIFO --------------------------------------------------
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
ULONG Ram_Data_Test( int target )
{
	ULONG error = 0;
	int i, bits;
	USHORT wtmp;
	USHORT rtmp;
	USHORT addr;
	USHORT status;
	USHORT data;

	WriteEDS( iADR_TP_SEL, 0x02  << 12 );

	switch ( target )
	{
		case 0 : addr = iADR_SCA_LU; bits = SCA_BITS; break;
		case 1 : addr = iADR_ROI_LU; bits = 16; break;
	}

	status = ( ReadEDS( iADR_STAT ) >> 3 );
	if(( status & 0x1 ) == 1 ){
		error = 0x10000000;
	}
	WriteEDS( iADR_TIME_STOP, 0x0 );			// Stop EDS Analysis
	WriteEDS( iADR_RAMA, 0 );					// Reset the Ram Address

	data = ReadEDS( iADR_CW );					// Read the Control Word
	data |= RAM_ACCESS;							// Enable RAM Access
	WriteEDS( iADR_CW, data );					// ReWrite the Control Word

	//----- Ram Data Test ----------------------------------------------
	for(i=0;i<bits;i++){
		wtmp = 1 << i;
		WriteEDS( iADR_RAMA, 0 );				// Write the RAM Address
		WriteEDS( addr, wtmp );					// Write the RAM Data
		rtmp = ReadEDS( addr );					// Read the RAM Data

		if( wtmp != rtmp ){						// Compare
			error |= wtmp;
		}
	}
	//----- Ram Data Test ----------------------------------------------

	data = ReadEDS( iADR_CW );					// Read the Control Word
	data &= ~RAM_ACCESS;						// Disable Ram Access
	WriteEDS( iADR_CW, data );					// ReWrite the Control Word
	#if NO_SCA == TRUE
		error = 0;
	#endif
	
	return( error );
}
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
ULONG Ram_Address_Test( int target )
{
	ULONG error = 0;
	int i;
	USHORT wtmp;
	USHORT rtmp;
	USHORT addr;
	int max_loop;
	USHORT status;
	USHORT data;

	WriteEDS( iADR_TP_SEL, 0x02  << 12 );	

	status = ( ReadEDS( iADR_STAT ) >> 3 );
	if(( status & 0x1 ) == 1 ){
		error = 0x10000000;
	}
	WriteEDS( iADR_TIME_STOP, 0x0 );			// Stop EDS Analysis
	WriteEDS( iADR_RAMA, 0 );					// Reset the Ram Address

	data = ReadEDS( iADR_CW );					// Read the Control Word
	data |= RAM_ACCESS;							// Enable RAM Access
	WriteEDS( iADR_CW, data );					// ReWrite the Control Word


	switch ( target )
	{
		case 0 : max_loop = 12; addr = iADR_SCA_LU; break;
		case 1 : max_loop = 8;  addr = iADR_ROI_LU; break;
	}

	for(i=0;i<max_loop;i++){
		wtmp = ( 1 << i );
		WriteEDS( iADR_RAMA, wtmp );				// Write Dither RAM Address
		WriteEDS( addr, (USHORT) i );				// Write RAM Data
	}

	for(i=0;i<max_loop;i++){
		wtmp = ( 1 << i );
		WriteEDS( iADR_RAMA, wtmp );		// Write Ram Address
		rtmp = ReadEDS( addr );				// Read RAM Data

		if( i != rtmp ){
			error |= wtmp;
		}
	}

	data = ReadEDS( iADR_CW );					// Read the Control Word
	data &= ~RAM_ACCESS;						// Disable Ram Access
	WriteEDS( iADR_CW, data );					// ReWrite the Control Word
	#if NO_SCA == TRUE
		error = 0;
	#endif

	return( error );
}
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
ULONG Ram_Memory_Test( int target )
{
	ULONG error = 0;
	int i;
	int j;
	USHORT wtmp;
	USHORT rtmp;
	USHORT addr;
	int ram_max;
	int data_max;
	USHORT status;
	USHORT data;

	WriteEDS( iADR_TP_SEL, 0x02  << 12 );

	status = ( ReadEDS( iADR_STAT ) >> 3 );
	if(( status & 0x1 ) == 1 ){
		error = 0x10000000;
	}
	WriteEDS( iADR_TIME_STOP, 0x0 );			// Stop EDS Analysis
	WriteEDS( iADR_RAMA, 0 );					// Reset the Ram Address

	data = ReadEDS( iADR_CW );					// Read the Control Word
	data |= RAM_ACCESS;							// Enable RAM Access
	WriteEDS( iADR_CW, data );					// ReWrite the Control Word

	switch ( target )
	{
		case 0 : ram_max = 4096; data_max = SCA_BITS;  addr = iADR_SCA_LU; break;		// 4K
		case 1 : ram_max = 256;  data_max = 16; 	  addr = iADR_ROI_LU; break;		// 256
	}

	//----- Dither RAM Memory Test
	for(i=0;i<ram_max;i++){
		for(j=0;j<data_max;j++){
			wtmp = 1 << j;
			WriteEDS( iADR_RAMA, (USHORT) i );		// Load Ram Address
			WriteEDS( addr, wtmp );						// Write DitherRAM DAta
			rtmp = ReadEDS( addr );
			if( wtmp != rtmp ){
				error ++;
			}
		}				// for j...
	}					// for i...

	data = ReadEDS( iADR_CW );					// Read the Control Word
	data &= ~RAM_ACCESS;						// Disable Ram Access
	WriteEDS( iADR_CW, data );					// ReWrite the Control Word
	#if NO_SCA == TRUE
		error = 0;
	#endif
	return( error );
}
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
ULONG	Dac_Test( int target )
{
	ULONG error = 0;
	ULONG done = 0;
	USHORT addr;
	USHORT tpsel;
	USHORT data = 0;
	USHORT rdata;
	USHORT comp;
	USHORT cmp_data;
	int max_loop;
	int i;

	WriteEDS( iADR_BIT_PEAK1, 0x0 );
	WriteEDS( iADR_BIT_PEAK2, 0x0 );

	data = ReadEDS( iADR_CW );
	data &= ~( 3 << 11 );						// Clear PreAmp Source Bits
	data &= ~( 1 << 13 );						// Set Input Gain to 1X
	data |= ( 2 << 11 );						// Set the PreAmp Source to BIT DAC


	tpsel = 0x2  << 12 ;						// DAC8420 Debug
	tpsel = 0x4 << 12 ;							// ADS8321 Debug
	tpsel = 0x7 << 12 ;							// LTC1595 Debug
	tpsel = 0x0 << 12 ;							// AD9260  Debug
	max_loop = 5;
	switch( target ){
		case 0x0 : addr = iADR_DISC;	tpsel |= ATP_DISC1;		break;	// Disc 1 Test Point
		case 0x1 : addr = iADR_EVCH1;	tpsel |= ATP_DISC2;		break;	// Disc 2 Test Point 
		case 0x2 : addr = iADR_EVCH0;	tpsel |= ATP_EVCH;		break;	// EV/CH Test Point
		case 0x3 : addr = iADR_BIT;		tpsel |= ATP_BIT_DAC;	break;	// BIT DAC Test Point
		case 0x4 : addr = iADR_BIT;		tpsel |= ATP_PAMP;  	break;	// Post AMP Test Point X1 Gain
		case 0x5 : 
				max_loop = 3;										// Restrict due to Over-Drive
				data |= ( 1 << 13 );									// Set Input Gain to 2X
				addr = iADR_BIT;		
				tpsel |= ATP_PAMP;		
				break;	// Post AMP Test Point X2 Gain
		case 0x6 : 
				WriteEDS( iADR_BIT, 0xFFF ); 
				addr = iADR_CGAIN;	
				tpsel |= ATP_CGAIN; 	
				break;	// Coarse Gain Out
		case 0x7 : 
				addr = iADR_BIT;		
				tpsel |= ATP_BFILTER;	
				
				break;	// Bessell Filter
		case 0x8 : 
				addr = iADR_BIT;		
				tpsel |= ATP_AFILTER;	
				break;	// Anti-Alias
		case 0x9 : 
				addr = iADR_BIT;		
				tpsel |= ATP_ADCP;		
				break;	// ADCIn Positive
		case 0xA : 
				addr = iADR_BIT;		
				tpsel |= ATP_ADCN;		
				break;	// ADCIn Negative
		case 0xB :																		// Disc Ev/Ch
				data |= ( 1 << 13 );						// Set Input Gain to 2X
				data |= 1;									// Set BIT Mode Enable
				WriteEDS( iADR_BIT, 0xFFF );
				addr = iADR_DISC_EVCH; 
				tpsel |= ATP_DISC_EVCH;
				max_loop = 4;
				break;
		case 0xC :																		// Disc Inv
				addr = iADR_BIT;		
				tpsel	|= ATP_DISC_INV;
				break;
		default :	return( 0xFFFF );							break;	// Invalid Case so Exit
	}
	WriteEDS( iADR_CW, data );											// ReWrite the Control Word
	WriteEDS( iADR_TP_SEL, tpsel );										// Write the Test Point Select

	error = 0;
	for(i=0;i<max_loop;i++){
		switch( i ){
			case 0 : data = 0x00; break;
			case 1 : data = 0x3FF; break;
			case 2 : data = 0x7FF; break;
			case 3 : data = 0xBFF; break;
			case 4 : data = 0xFFF; break;
		}

		switch( target ){
			case 0x0 : 
			case 0x1 :
			case 0x2 : 
			case 0x3 :
			default  : 
					switch(i){
						case 0 : comp = 0x0E; break;
						case 1 : comp = 0x2B; break;
						case 2 : comp = 0x47; break;
						case 3 : comp = 0x63; break; 
						case 4 : comp = 0x80; break; 
					}
					break;

			case 0x4 :
					switch(i){
						case 0 : comp = 0x35; break;
						case 1 : comp = 0x3C; break;
						case 2 : comp = 0x43; break;
						case 3 : comp = 0x4a; break; 
						case 4 : comp = 0x51; break; 
					}
					break;
			case 0x5 :
					switch(i){
						case 0 : comp = 0x26; break;
						case 1 : comp = 0x35; break;
						case 2 : comp = 0x43; break;
					}
					break;

			case 0x6 :
					switch(i){
						case 0 : data = 0x0000; comp = 0x45; break;
						case 1 : data = 0x3FFF; comp = 0x49; break;
						case 2 : data = 0x7FFF; comp = 0x4c; break;
						case 3 : data = 0xBFFF; comp = 0x50; break; 
						case 4 : data = 0xFFFF; comp = 0x53; break; 
					}
					break;

			case 0x7 :
			case 0x8 :
					switch(i){
						case 0 : comp = 0x37; break;
						case 1 : comp = 0x3e; break;
						case 2 : comp = 0x45; break;
						case 3 : comp = 0x4c; break; 
						case 4 : comp = 0x53; break; 
					}
					break;

			case 0x9 : 
					switch(i){
						case 0 : comp = 0x8A; break;
						case 1 : comp = 0x83; break;
						case 2 : comp = 0x7D; break;
						case 3 : comp = 0x75; break; 
						case 4 : comp = 0x6E; break; 
					}
					break;

//					switch(i){
//						case 0 : comp = 0x81; break;
//						case 1 : comp = 0x7A; break;
//						case 2 : comp = 0x72; break;
//						case 3 : comp = 0x6B; break; 
//						case 4 : comp = 0x64; break; 
//					}
//					break;

			case 0xA : 
					switch(i){
						case 0 : comp = 0x71; break;
						case 1 : comp = 0x78; break;
						case 2 : comp = 0x7F; break;
						case 3 : comp = 0x87; break; 
						case 4 : comp = 0x8E; break; 
					}
					break;
//			switch(i){
//				case 0 : comp = 0x68; break;
//				case 1 : comp = 0x6f; break;
//				case 2 : comp = 0x76; break;
//				case 3 : comp = 0x7d; break; 
//				case 4 : comp = 0x85; break; 
//			}
//			break;


			case 0xb : 
				switch(i) {
					case 0 : data = 3;	comp = 0x49; break;		// 20eV/CH X1
					case 1 : data = 2;	comp = 0x4b; break;		// 10eV/CH /2
					case 2 : data = 1;	comp = 0x51; break;		//  5eV/CH /4
					case 3 : data = 0;	comp = 0x5f; break;		// 2.5V/CH /8
					default : break;
				}		
				break;
			case 0xC : 
				switch(i) {
					case 0 : comp = 0x52; break;		// 20eV/CH X1
					case 1 : comp = 0x4b; break;		// 10eV/CH /2
					case 2 : comp = 0x44; break;		//  5eV/CH /4
					case 3 : comp = 0x3d; break;		// 2.5V/CH /8
					case 4 : comp = 0x36; break;
					default : break;
				}		
				break;
		}

		WriteEDS( addr, data );				// Load The appropriate DAC with the Approprite Data
		rdata = ReadBITADC();				// Read the ADC
		rdata = ReadBITADC();				// Read the ADC
		rdata = ReadBITADC();				// Read the ADC
		cmp_data = rdata >> 8;				// Keep only the 4 upper bits ( Just Range checking since Serial DAC )
//		if( cmp_data != comp ){
		if( cmp_data > comp ){
			rdata = cmp_data - comp;
		} else { 
			rdata = comp - cmp_data;
		}

		if( rdata > 1 ){
			error |= ( 1 << i );
		}
	}										// for i...

	data = ReadEDS( iADR_CW );
	data &= ~( 3 << 11 );					// Clear PreAmp Source Bits
	data &= ~( 1 << 13 );					// Set Input Gain to 1X
	WriteEDS( iADR_CW, data );

	return( error );
}
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
void BIT_SCA_GEN( void )
{
	USHORT data;	
	data = ReadDPRAM( dADR_SCA_GEN );
	WriteEDS( iADR_SCA, data );
}
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
void BIT_DAC_GEN( void )
{
	USHORT target;
	static USHORT count = 0;
	target = ReadDPRAM( dADR_DAC_GEN );

	switch( target ){
		case 1 : WriteEDS( iADR_DISC,  count ); break;
		case 2 : WriteEDS( iADR_EVCH1, count ); break;
		case 3 : WriteEDS( iADR_BIT,   count ); break;
		case 4 : WriteEDS( iADR_EVCH0, count ); break;
		default : break;
	}
	count ++;
	count &= 0xFFF;
}
//---------------------------------------------------------------------------------------

/********************************************************************************/
/* END OF DPP2BIT.C															    */
/********************************************************************************/
