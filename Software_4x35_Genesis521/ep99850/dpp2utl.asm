;******************************************************************************
;* TMS320C3x/4x ANSI C Code Generator                            Version 5.11 *
;* Date/Time created: Thu May 24 17:11:48 2007                                *
;******************************************************************************
	.regalias	; enable floating point register aliases
fp	.set	ar3
FP	.set	ar3
;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Silicon Info       : C32 Revision PG1                                    *
;*   Optimization       : Conservatively Choose Speed over Size               *
;*   Memory             : Small Memory Model                                  *
;*   Float-to-Int       : Fast Conversions (round toward -inf)                *
;*   Multiply           : in Software (32 bits)                               *
;*   Memory Info        : Unmapped Memory Exists                              *
;*   Repeat Loops       : Use RPTS and/or RPTB                                *
;*   Calls              : Normal Library ASM calls                            *
;*   Debug Info         : Optimized TI Debug Information                      *
;******************************************************************************
;	c:\c3xtools\bin\ac30.exe -v32 dpp2utl.c C:\DOCUME~1\MSOLAZ~1.AME\LOCALS~1\Temp\dpp2utl.if 
	.file	"dpp2utl.c"
	.file	"bus32.h"
	.stag	.fake1,32
	.member	_holdst,0,14,18,1
	.member	_nohold,1,14,18,1
	.member	_hiz,2,14,18,1
	.member	_sww,3,14,18,2
	.member	_wtcnt,5,14,18,3
	.member	_bnkcmp,8,14,18,5
	.member	_r_131415,13,14,18,3
	.member	_datasize,16,14,18,2
	.member	_memwidth,18,14,18,2
	.member	_signext,20,14,18,1
	.member	_strbcnfg,21,14,18,1
	.member	_strbsw,22,14,18,1
	.member	_r_rest,23,14,18,9
	.eos
	.utag	.fake0,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake1
	.eos
	.sym	_STRB0_BUS_CONTROL,0,9,13,32,.fake0
	.stag	.fake3,32
	.member	_r_012,0,14,18,3
	.member	_sww,3,14,18,2
	.member	_wtcnt,5,14,18,3
	.member	_bnkcmp,8,14,18,5
	.member	_r_131415,13,14,18,3
	.member	_datasize,16,14,18,2
	.member	_memwidth,18,14,18,2
	.member	_signext,20,14,18,1
	.member	_r_rest,21,14,18,11
	.eos
	.utag	.fake2,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake3
	.eos
	.sym	_STRB1_BUS_CONTROL,0,9,13,32,.fake2
	.stag	.fake5,32
	.member	_r_012,0,14,18,3
	.member	_sww,3,14,18,2
	.member	_wtcnt,5,14,18,3
	.member	_r_rest,8,14,18,24
	.eos
	.utag	.fake4,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake5
	.eos
	.sym	_IOSTRB_BUS_CONTROL,0,9,13,32,.fake4
	.stag	.fake6,288
	.member	__ioctrl,0,9,8,32,.fake4
	.member	_reserved1,32,62,8,96,,3
	.member	__s0ctrl,128,9,8,32,.fake0
	.member	_reserved2,160,62,8,96,,3
	.member	__s1ctrl,256,9,8,32,.fake2
	.eos
	.sym	_BUS_REG,0,8,13,288,.fake6
	.file	"timer30.h"
	.stag	.fake8,32
	.member	_func,0,14,18,1
	.member	_i_o,1,14,18,1
	.member	_datout,2,14,18,1
	.member	_datin,3,14,18,1
	.member	_r_45,4,14,18,2
	.member	_go,6,14,18,1
	.member	_hld_,7,14,18,1
	.member	_cp_,8,14,18,1
	.member	_clksrc,9,14,18,1
	.member	_inv,10,14,18,1
	.member	_tstat,11,14,18,1
	.member	_r_rest,12,14,18,20
	.eos
	.utag	.fake7,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake8
	.eos
	.sym	_TIMER_CONTROL,0,9,13,32,.fake7
	.stag	.fake9,288
	.member	__gctrl,0,9,8,32,.fake7
	.member	_reserved1,32,62,8,96,,3
	.member	_counter,128,14,8,32
	.member	_reserved2,160,62,8,96,,3
	.member	_period,256,14,8,32
	.eos
	.sym	_TIMER_REG,0,8,13,288,.fake9
	.file	"serprt30.h"
	.stag	.fake11,32
	.member	_rrdy,0,14,18,1
	.member	_xrdy,1,14,18,1
	.member	_fsxout,2,14,18,1
	.member	_xsrempty,3,14,18,1
	.member	_rsrfull,4,14,18,1
	.member	_hs,5,14,18,1
	.member	_xclksrce,6,14,18,1
	.member	_rclksrce,7,14,18,1
	.member	_xvaren,8,14,18,1
	.member	_rvaren,9,14,18,1
	.member	_xfsm,10,14,18,1
	.member	_rfsm,11,14,18,1
	.member	_clkxp,12,14,18,1
	.member	_clkrp,13,14,18,1
	.member	_dxp,14,14,18,1
	.member	_drp,15,14,18,1
	.member	_fsxp,16,14,18,1
	.member	_fsrp,17,14,18,1
	.member	_xlen,18,14,18,2
	.member	_rlen,20,14,18,2
	.member	_xtint,22,14,18,1
	.member	_xint,23,14,18,1
	.member	_rtint,24,14,18,1
	.member	_rint,25,14,18,1
	.member	_xreset,26,14,18,1
	.member	_rreset,27,14,18,1
	.member	_r_rest,28,14,18,4
	.eos
	.utag	.fake10,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake11
	.eos
	.sym	_SERIAL_PORT_CONTROL,0,9,13,32,.fake10
	.stag	.fake13,32
	.member	_clkfunc,0,14,18,1
	.member	_clki_o,1,14,18,1
	.member	_clkdato,2,14,18,1
	.member	_clkdati,3,14,18,1
	.member	_dfunc,4,14,18,1
	.member	_di_o,5,14,18,1
	.member	_ddatout,6,14,18,1
	.member	_ddatin,7,14,18,1
	.member	_fsfunc,8,14,18,1
	.member	_fsi_o,9,14,18,1
	.member	_fsdatout,10,14,18,1
	.member	_fsdatin,11,14,18,1
	.member	_r_rest,12,14,18,20
	.eos
	.utag	.fake12,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake13
	.eos
	.sym	_RX_PORT_CONTROL,0,9,13,32,.fake12
	.stag	.fake15,32
	.member	_xgo,0,14,18,1
	.member	_xhld_,1,14,18,1
	.member	_xcp_,2,14,18,1
	.member	_xclksrc,3,14,18,1
	.member	_r_4,4,14,18,1
	.member	_xtstat,5,14,18,1
	.member	_rgo,6,14,18,1
	.member	_rhld_,7,14,18,1
	.member	_rcp_,8,14,18,1
	.member	_rclksrc,9,14,18,1
	.member	_r_10,10,14,18,1
	.member	_rtstat,11,14,18,1
	.member	_r_rest,12,14,18,20
	.eos
	.utag	.fake14,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake15
	.eos
	.sym	_RX_TIMER_CONTROL,0,9,13,32,.fake14
	.stag	.fake17,32
	.member	_x_counter,0,14,18,16
	.member	_r_counter,16,14,18,16
	.eos
	.utag	.fake16,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake17
	.eos
	.sym	_RX_TIMER_COUNTER,0,9,13,32,.fake16
	.stag	.fake19,32
	.member	_x_period,0,14,18,16
	.member	_r_period,16,14,18,16
	.eos
	.utag	.fake18,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake19
	.eos
	.sym	_RX_TIMER_PERIOD,0,9,13,32,.fake18
	.stag	.fake20,512
	.member	__gctrl,0,9,8,32,.fake10
	.member	_reserved1,32,14,8,32
	.member	__xctrl,64,9,8,32,.fake12
	.member	__rctrl,96,9,8,32,.fake12
	.member	__rxtctrl,128,9,8,32,.fake14
	.member	__rxtcounter,160,9,8,32,.fake16
	.member	__rxtperiod,192,9,8,32,.fake18
	.member	_reserved2,224,14,8,32
	.member	_x_data,256,14,8,32
	.member	_reserved3,288,62,8,96,,3
	.member	_r_data,384,14,8,32
	.member	_reserved4,416,62,8,96,,3
	.eos
	.sym	_SERIAL_PORT_REG,0,8,13,512,.fake20
	.file	"defines.h"
	.sym	_UCHAR,0,12,13,32
	.sym	_USHORT,0,13,13,32
	.sym	_ULONG,0,15,13,32
	.file	"dpp2utl.h"
	.stag	.fake21,224
	.member	_PCI_IRQ,0,12,8,32
	.member	_IRQ_RESET,32,13,8,32
	.member	_MS100_TC,64,13,8,32
	.member	_OpCodeAck,96,13,8,32
	.member	_PCIIRQ_Cnt,128,15,8,32
	.member	_OpCode,160,13,8,32
	.member	_Set_PCI_IRQ,192,13,8,32
	.eos
	.sym	_DPP_TYPE,0,8,13,224,.fake21
	.file	"dpp2bit.h"
	.file	"edsutl.h"
	.stag	.fake22,1088
	.member	_FPGA_Preset_Done,0,13,8,32
	.member	_EDSXFer,32,13,8,32
	.member	_EDS_Stat,64,13,8,32
	.member	_DSO_PENDING,96,13,8,32
	.member	_TMR_ISR_CNT,128,15,8,32
	.member	_EDS_IRQ,160,12,8,32
	.member	_EDS_ISR_CNT,192,15,8,32
	.member	_CLIP_MIN,224,13,8,32
	.member	_CLIP_MAX,256,13,8,32
	.member	_Preset_Mode,288,13,8,32
	.member	_Preset_Done_Cnt,320,13,8,32
	.member	_Preset_Done_Points,352,13,8,32
	.member	_DSO_Index,384,13,8,32
	.member	_FFT_Mode,416,13,8,32
	.member	_DSO_Enable,448,13,8,32
	.member	_PixelCount,480,13,8,32
	.member	_EDS_Mode,512,13,8,32
	.member	_EDS_DEF_ROIS,544,13,8,32
	.member	_Max_Fir_Cnt,576,13,8,32
	.member	_Max_Fir,608,61,8,128,,4
	.member	_RTEM_Init,736,13,8,32
	.member	_RTEM_Target,768,13,8,32
	.member	_ADisc,800,13,8,32
	.member	_ASHBit,832,13,8,32
	.member	_ADiscVal,864,61,8,160,,5
	.member	_EDX_ANAL,1024,13,8,32
	.member	_Reset_FIR,1056,13,8,32
	.eos
	.sym	_EDS_TYPE,0,8,13,1088,.fake22
	.stag	.fake23,160
	.member	_Start,0,13,8,32
	.member	_End,32,13,8,32
	.member	_Sca,64,13,8,32
	.member	_Enable,96,12,8,32
	.member	_Defined,128,12,8,32
	.eos
	.sym	_ROI_TYPE,0,8,13,160,.fake23
	.file	"dpp2utl.c"
	.sect	 ".text"

	.global	_DSP_Init
	.sym	_DSP_Init,_DSP_Init,32,2,0
	.func	78
;******************************************************************************
;* FUNCTION NAME: _DSP_Init                                                   *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,ar0,fp,sp                                     *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 4 Auto + 0 SOE = 6 words          *
;******************************************************************************
_DSP_Init:
	.sym	_bus_ptr,1,24,1,32,.fake6
	.sym	_timer0_ptr,2,24,1,32,.fake9
	.sym	_timer1_ptr,3,24,1,32,.fake9
	.sym	_sp,4,24,1,32,.fake20
	.line	1
;----------------------------------------------------------------------
;  78 | void DSP_Init( void )                                                  
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      4,sp
	.line	3
;----------------------------------------------------------------------
;  80 | volatile BUS_REG *bus_ptr=BUS_ADDR;                     // Define Point
;     | er to Bus Control Registers                                            
;----------------------------------------------------------------------
        ldiu      @CL1,r0
        sti       r0,*+fp(1)
	.line	4
;----------------------------------------------------------------------
;  81 | volatile TIMER_REG *timer0_ptr = TIMER_ADDR(0); // Define Pointer to Ti
;     | mer0 Register                                                          
;----------------------------------------------------------------------
        ldiu      @CL2,r0
        sti       r0,*+fp(2)
	.line	5
;----------------------------------------------------------------------
;  82 | volatile TIMER_REG *timer1_ptr = TIMER_ADDR(1); // Define Pointer to Ti
;     | mer1 Register                                                          
;----------------------------------------------------------------------
        ldiu      @CL3,r0
        sti       r0,*+fp(3)
	.line	6
;----------------------------------------------------------------------
;  83 | volatile SERIAL_PORT_REG *sp = SERIAL_PORT_ADDR(0);     // Define Point
;     | er to Serial Port Register                                             
;  85 | // ****** Setup the processor access parameters ********               
;  88 | // ****** IOSTRB Control Setup                                         
;----------------------------------------------------------------------
        ldiu      @CL4,r0
        sti       r0,*+fp(4)
	.line	12
;----------------------------------------------------------------------
;  89 | bus_ptr->iostrb_gcontrol = 0x0;         // not used                    
;  91 | // ****** STRB0 Control Setup                                          
;  92 | //      Set the EXT_RAM                                                
;  93 | //      STRB switch                                     = 0, no nop bet
;     | ween back-to-back access                                               
;  94 | //      STRB config                                     = 0            
;  95 | //      zero fill                                       = 1            
;  96 | //      physical memory width                   = 11, 32 bit           
;  97 | //      data type size                          = 11, 32 bit           
;  98 | //      BNKCMP                                          = 10000        
;  99 | //      WTCNT                                           = 000, zero wai
;     | t state                                                                
; 100 | //      SWW                                                     = 01, i
;     | nternal wait state control                                             
; 101 | //      HIZ                                                     = 0, no
;     |  hold                                                                  
; 102 | //      NOHOLD                                          = 0, respond to
;     |  external hold                                                         
; 103 | //      software controlled wait state  = zero                         
; 104 | //**********************************************                       
;----------------------------------------------------------------------
        ldiu      *+fp(1),ar0
        ldiu      0,r0
        sti       r0,*ar0
	.line	28
;----------------------------------------------------------------------
; 105 | bus_ptr->strb0_gcontrol         = STRB_SW_NO                           
; 106 |                                 | STRB0_CNFG                           
; 107 |                                 | NO_SIGN_EXT                          
; 108 |                                 | MEMW_32                              
; 109 |                                 | DATA_32                              
; 110 |                                 | BANK_256                             
; 111 |                                 | WS_0                                 
; 112 |                                 | INTERNAL_RDY;                        
; 113 |                                 // 0x001f1008                          
; 115 | //***** STRB1 Control Setup                                            
; 116 | //      zero fill                                       = 1            
; 117 | //      physical memory width                   = 11, 32 bit           
; 118 | //      data type size                          = 11, 32 bit           
; 119 | //      BNKCMP                                          = 10000        
; 120 | //      WTCNT                                           = 001, one wait
;     |  state                                                                 
; 121 | //      SWW                                                     = 00, e
;     | xternal wait state control                                             
; 122 | //**********************************************                       
;----------------------------------------------------------------------
        ldiu      *+fp(1),ar0
        ldiu      @CL5,r0
        sti       r0,*+ar0(4)
	.line	46
;----------------------------------------------------------------------
; 123 | bus_ptr->strb1_gcontrol         = NO_SIGN_EXT                          
; 124 |                                 | MEMW_32                              
; 125 |                                 | DATA_32                              
; 126 |                                 | BANK_256                             
; 127 | //                                      | WS_3                         
; 128 |                                 | WS_2                                 
; 129 | //                                      | WS_1                         
; 130 |                                 | INTERNAL_RDY;                        
; 131 |                                 // 0x001f1028                          
; 133 | //****** TIMER0 Global Control Register Setup                          
; 134 | //      FUNC                                    = 1; timer pin - genera
;     | te 4 Hz clock                                                          
; 135 | //      I/O                                             = 1; Output pin
; 136 | //      DATOUT                                  = 0                    
; 137 | //      DATIN                                   = 0                    
; 138 | //      GO                                              = 1; reset and
;     | start                                                                  
; 139 | //      HLD/                                    = 1; no hold           
; 140 | //      C/P                                             = 1; clock mode
; 141 | //      CLKSRC                                  = 1; Internal clock sou
;     | rce                                                                    
; 142 | //      INV                                             = 0; no inverti
;     | ng                                                                     
; 143 | //      TSTAT                                   = 0; timer status
;     |                                                                        
; 144 | //***********************************************                      
;----------------------------------------------------------------------
        ldiu      *+fp(1),ar0
        ldiu      @CL6,r0
        sti       r0,*+ar0(8)
	.line	68
;----------------------------------------------------------------------
; 145 | timer0_ptr->gcontrol    = FUNC                                         
; 146 |                                 | I_O                                  
; 147 |                                 | GO                                   
; 148 |                                 | HLD_                                 
; 149 |                                 | CP_                                  
; 150 |                                 | CLKSRC;                              
; 151 |                                 // 0x1C1                               
; 153 | //****** TIMER0 Period Control Register Setup                          
;----------------------------------------------------------------------
        ldiu      *+fp(2),ar0
        ldiu      963,r0
        sti       r0,*ar0
	.line	77
;----------------------------------------------------------------------
; 154 | timer0_ptr->period      = (ULONG) (DSP_CLK / (2*TIMER0_FREQ));  // 10 H
;     | z;                                                                     
; 157 | //****** TIMER1 Global Control Register Setup Used for LED             
; 158 | //      FUNC            = 1     timer pin                              
; 159 | //      I/O                     = 1     Output Pin                     
; 160 | //      DATOUT          = 0             Don't Care                     
; 161 | //      DATIN           = 0             Don't Care                     
; 162 | //      GO                      = 1;    reset and start                
; 163 | //      HLD/            = 1; no hold                                   
; 164 | //      C/P                     = 1; clock mode - 50% DC               
; 165 | //      CLKSRC          = 1; internal clock source                     
; 166 | //      INV                     = 0; no inverting                      
; 167 | //      TSTAT           = 0; timer status                              
; 168 | //**********************************************                       
;----------------------------------------------------------------------
        ldiu      *+fp(2),ar0
        ldiu      @CL7,r0
        sti       r0,*+ar0(8)
	.line	92
;----------------------------------------------------------------------
; 169 | timer1_ptr->gcontrol    = FUNC                                         
; 170 |                                 | I_O                                  
; 171 |                                 | GO                                   
; 172 |                                 | HLD_                                 
; 173 |                                 | CP_                                  
; 174 |                                 | CLKSRC;                              
; 175 |                                 // 0x3C3                               
; 177 | //****** TIMER1 Period Control Register Setup                          
;----------------------------------------------------------------------
        ldiu      *+fp(3),ar0
        ldiu      963,r0
        sti       r0,*ar0
	.line	101
;----------------------------------------------------------------------
; 178 | timer1_ptr->period      = (ULONG)( DSP_CLK / ( 2 * TIMER1_FREQ ));
;     |  // 0x7270e0;            // 2Hz                                        
; 180 | //----- Register Initialization for the TMS320C32                      
; 181 | // Clear and Enable Cache, disable OVM, disable interrupts, edge trigge
;     | red interrupts                                                         
; 183 | // Initialize the Status Regiseter of the TMS320C32                    
; 184 | // Bit 14 = Interrupt Configuration, 1 = Edge, 0 = Level               
;----------------------------------------------------------------------
        ldiu      *+fp(3),ar0
        ldiu      @CL8,r0
        sti       r0,*+ar0(8)
	.line	108
;----------------------------------------------------------------------
; 185 | asm("   ldi     05800H,st       ");                                    
;----------------------------------------------------------------------
	ldi     05800H,st       
	.line	109
;----------------------------------------------------------------------
; 186 | asm("   push    r0");                                                  
;----------------------------------------------------------------------
	push	r0
	.line	110
;----------------------------------------------------------------------
; 187 | asm("   ldi     08830H,r0");                                           
;----------------------------------------------------------------------
	ldi 	08830H,r0
	.line	111
;----------------------------------------------------------------------
; 188 | asm("   lsh     16,r0");                                               
;----------------------------------------------------------------------
	lsh 	16,r0
	.line	112
;----------------------------------------------------------------------
; 189 | asm("   or      r0,if           ;locate the vector table starting at 0x
;     | 883000");                                                              
;----------------------------------------------------------------------
	or 	r0,if   	;locate the vector table starting at 0x883000
	.line	113
;----------------------------------------------------------------------
; 190 | asm("   ldi     0105H,ie        ;enable CPU timer0 interrupt and extern
;     | al interrupt 2");                                                      
;----------------------------------------------------------------------
	ldi 	0105H,ie   	;enable CPU timer0 interrupt and external interrupt 2
	.line	114
;----------------------------------------------------------------------
; 191 | asm("   pop     r0");                                                  
; 192 | //----- Register Initialization for the TMS320C32                      
;----------------------------------------------------------------------
	pop	r0
	.line	117
;----------------------------------------------------------------------
; 194 | asm("   or     02000H,st       ");              // Enables Global Inter
;     | rupts                                                                  
; 195 | //      asm("   and    ~02000H,st       ");             // Disables Glo
;     | bal Interrupts                                                         
; 198 | //***** Initialize the Seral Port *************************************
;     | ***************                                                        
; 199 | //      Reset Serial port Global Register                              
;----------------------------------------------------------------------
	or     02000H,st       
	.line	123
;----------------------------------------------------------------------
; 200 | sp->gcontrol = SP_GCONTROL;                     // 0x0                 
; 203 | //                                                                     
; 204 | //      write the serial port FSX/DX/CLKX control register             
; 205 | //      refer to TMS320C3x User's Guide Pages 8-17,18                  
; 206 | //      set FSX = output;                               D[9] = 1       
; 207 | //      set FSX = serial port pin;              D[8] = 1               
; 208 | //
;     |  D[7] = 0                                                              
; 209 | //
;     |  D[6] = 0                                                              
; 210 | //      set DX = output;                                D[5] = 1       
; 211 | //      set DXfunc = serial port pin;   D[4] = 1                       
; 212 | //
;     |  D[3] = 0                                                              
; 213 | //                                                      D[2] = 0       
; 214 | //      set CLKX = output;                              D[1] = 1       
; 215 | //      set CLKX = serial port pin;     D[0] = 1                       
; 216 | //                                                                     
;----------------------------------------------------------------------
        ldiu      *+fp(4),ar0
        ldiu      @CL9,r0
        sti       r0,*ar0
	.line	140
;----------------------------------------------------------------------
; 217 | sp->s_x_control = 0x0333;                                              
; 219 | //                                                                     
; 220 | //      write the serial port FSR/DR/CLKR control register             
; 221 | //      refer to TMS320C3x User's Guide Pages 8-18,19                  
; 222 | //      set FSR = Input                                 D[9] = 0       
; 223 | //      set FSR = User Pin                              D[8] = 0       
; 224 | //      set DR = Input;                                 D[5] = 0       
; 225 | //      set DR = serial port pin;               D[4] = 1               
; 226 | //      set CLKR = Input;                               D[1] = 0       
; 227 | //      set CLKR = serial port pin;     D[0] = 1                       
; 228 | //                                                                     
;----------------------------------------------------------------------
        ldiu      *+fp(4),ar0
        ldiu      819,r0
        sti       r0,*+ar0(2)
	.line	152
;----------------------------------------------------------------------
; 229 | sp->s_r_control = 0x0111;       // forced high                         
; 231 | // set the serial port timer to 5 MHz                                  
;----------------------------------------------------------------------
        ldiu      *+fp(4),ar0
        ldiu      273,r0
        sti       r0,*+ar0(3)
	.line	155
;----------------------------------------------------------------------
; 232 | sp->s_rxt_period = 0x00030003;                                         
; 234 | //      write the serial port timer control register                   
; 235 | //      refer to TMS320C3x User's Guide Pages 8-19,20                  
; 236 | //      set source of receive clock = internal; D[9] = 1               
; 237 | //      select receive clock mode;
;     |  D[8] = 1                                                              
; 238 |  //     don't hold the set receive counter;                     D[7] =
;     | 1                                                                      
; 239 | //      start the receive counter;                      D[6] = 1       
; 240 | //      set source of transmit clock = internal;        D[3] = 1       
; 241 | //      select transmit clock mode;                     D[2] = 1       
; 242 | //      don't hold the set transmit counter;    D[1] = 1               
; 243 | //      start the transmit counter;             D[0] = 1               
;----------------------------------------------------------------------
        ldiu      *+fp(4),ar0
        ldiu      @CL10,r0
        sti       r0,*+ar0(6)
	.line	167
;----------------------------------------------------------------------
; 244 | sp->s_rxt_control = 0x03CF;                                            
;----------------------------------------------------------------------
        ldiu      *+fp(4),ar0
        ldiu      975,r0
        sti       r0,*+ar0(4)
	.line	170
;----------------------------------------------------------------------
; 247 | sp->gcontrol = SP_GCONTROL;                                            
; 248 | //sp->gcontrol = 0x02970344;    // 16 bit transmit                     
; 249 | //***** Initialize the Seral Port *************************************
;     | ***************                                                        
; 251 | //***** Initialize IO Flag Register ******************************     
; 253 | // Flag1 is used for FSIN                                              
; 254 | // INFX1 Output Value   D[7] = 0                                       
; 255 | // OUTFX1 Output Value  D[6] = 1                                       
; 256 | // IOFX1 as Output              D[5] = 1                               
; 258 | // Flag0 is used for OAE                                               
; 259 | // INFX0 Output Value   D[3] = 0                                       
; 260 | // OUTFX0 Output Value  D[2] = 1                                       
; 261 | // IOFX0 as Output              D[1] = 1                               
;----------------------------------------------------------------------
        ldiu      *+fp(4),ar0
        ldiu      @CL9,r0
        sti       r0,*ar0
	.line	185
;----------------------------------------------------------------------
; 262 | asm("   and             ~0eeh, iof");                   // Clear All of
;     |  the programmable bits                                                 
;----------------------------------------------------------------------
	and		~0eeh, iof
	.line	186
;----------------------------------------------------------------------
; 263 | asm("   or               66h, iof");                    // Set the OSB_
;     | AE low and OFSIN High                                                  
;----------------------------------------------------------------------
	or		 66h, iof
	.line	188
;----------------------------------------------------------------------
; 265 | WriteGlue( iADR_GLUE_IRQ_DISABLE, 0x0 );        // De-Assert the PCI In
;     | terrupt                                                                
;----------------------------------------------------------------------
        ldiu      2,r1
        ldiu      0,r0
        push      r0
        push      r1
        call      _WriteGlue
                                        ; Call Occurs
        subi      2,sp
	.line	189
;----------------------------------------------------------------------
; 266 | DPPRec.PCI_IRQ                  = 0;                                   
; 267 | //      DPPRec.TMR_IRQ                  = 0;                           
; 268 | //      DPPRec.TMR_IRQ_ENABLE   = 1;                                   
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_DPPRec+0
	.line	192
;----------------------------------------------------------------------
; 269 | DPPRec.IRQ_RESET                = 0;                                   
;----------------------------------------------------------------------
        sti       r0,@_DPPRec+1
	.line	193
;----------------------------------------------------------------------
; 270 | DPPRec.MS100_TC         = 0;                                           
;----------------------------------------------------------------------
        sti       r0,@_DPPRec+2
	.line	194
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	271,000000000h,4


	.sect	 ".text"

	.global	_WriteDPRAM
	.sym	_WriteDPRAM,_WriteDPRAM,32,2,0
	.func	275
;******************************************************************************
;* FUNCTION NAME: _WriteDPRAM                                                 *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,ar0,ir0,st                                       *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 2 Parm + 0 Auto + 0 SOE = 4 words          *
;******************************************************************************
_WriteDPRAM:
	.sym	_addr,-2,13,9,32
	.sym	_data,-3,13,9,32
	.line	1
;----------------------------------------------------------------------
; 275 | void WriteDPRAM( USHORT addr, USHORT data )                            
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
	.line	2
	.line	3
;----------------------------------------------------------------------
; 277 | if(( addr & 0x100 ) == 0 ){                                            
;----------------------------------------------------------------------
        ldiu      256,r0
        tstb      *-fp(2),r0
        bne       L6
;*      Branch Occurs to L6 
	.line	4
;----------------------------------------------------------------------
; 278 | *(ULONG *) ( DPRAM0_BASE + addr ) = data;                              
; 279 | } else {                                                               
;----------------------------------------------------------------------
        ldiu      *-fp(2),ir0
        bud       L7
        ldiu      @CL11,ar0
        ldiu      *-fp(3),r0
        sti       r0,*+ar0(ir0)
;*      Branch Occurs to L7 
L6:        
	.line	6
;----------------------------------------------------------------------
; 280 | *(ULONG *) ( DPRAM1_BASE + addr ) = data;                              
; 282 | // Set the EDS Address                                                 
;----------------------------------------------------------------------
        ldiu      *-fp(2),ir0
        ldiu      @CL12,ar0
        ldiu      *-fp(3),r0
        sti       r0,*+ar0(ir0)
L7:        
	.line	9
        ldiu      *-fp(1),ir0
        ldiu      *fp,fp
        subi      2,sp
        bu        ir0
;*      Branch Occurs to ir0 
	.endfunc	283,000000000h,0


	.sect	 ".text"

	.global	_ReadDPRAM
	.sym	_ReadDPRAM,_ReadDPRAM,45,2,0
	.func	287
;******************************************************************************
;* FUNCTION NAME: _ReadDPRAM                                                  *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,ar0,ir0,st                                       *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 1 Auto + 0 SOE = 4 words          *
;******************************************************************************
_ReadDPRAM:
	.sym	_addr,-2,13,9,32
	.sym	_data,1,15,1,32
	.line	1
;----------------------------------------------------------------------
; 287 | USHORT ReadDPRAM( USHORT addr )                                        
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      1,sp
	.line	2
;----------------------------------------------------------------------
; 289 | ULONG data;                                                            
;----------------------------------------------------------------------
	.line	5
;----------------------------------------------------------------------
; 291 | if(( addr & 0x100 ) == 0 ){                                            
;----------------------------------------------------------------------
        ldiu      256,r0
        tstb      *-fp(2),r0
        bne       L11
;*      Branch Occurs to L11 
	.line	6
;----------------------------------------------------------------------
; 292 | data = *(ULONG *) ( DPRAM0_BASE + addr );       // Return the Data     
; 293 | } else {                                                               
;----------------------------------------------------------------------
        ldiu      *-fp(2),ir0
        bud       L12
        ldiu      @CL11,ar0
        ldiu      *+ar0(ir0),r0
        sti       r0,*+fp(1)
;*      Branch Occurs to L12 
L11:        
	.line	8
;----------------------------------------------------------------------
; 294 | data = *(ULONG *) ( DPRAM1_BASE + addr );       // Return the Data     
; 297 | // CHeck for bit 8 of address                                          
;----------------------------------------------------------------------
        ldiu      *-fp(2),ir0
        ldiu      @CL12,ar0
        ldiu      *+ar0(ir0),r0
        sti       r0,*+fp(1)
L12:        
	.line	13
;----------------------------------------------------------------------
; 299 | data &= 0xFFFF;                                                        
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
        and       65535,r0
        sti       r0,*+fp(1)
	.line	14
;----------------------------------------------------------------------
; 300 | return( (USHORT)data );                                                
;----------------------------------------------------------------------
	.line	15
        ldiu      *-fp(1),ir0
        ldiu      *fp,fp
        subi      3,sp
        bu        ir0
;*      Branch Occurs to ir0 
	.endfunc	301,000000000h,1


	.sect	 ".text"

	.global	_WriteGlue
	.sym	_WriteGlue,_WriteGlue,32,2,0
	.func	305
;******************************************************************************
;* FUNCTION NAME: _WriteGlue                                                  *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,ar0,ir0                                          *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 2 Parm + 0 Auto + 0 SOE = 4 words          *
;******************************************************************************
_WriteGlue:
	.sym	_addr,-2,13,9,32
	.sym	_data,-3,13,9,32
	.line	1
;----------------------------------------------------------------------
; 305 | void WriteGlue( USHORT addr, USHORT data )                             
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
	.line	2
	.line	3
;----------------------------------------------------------------------
; 307 | *(ULONG *)( GLUE_BASE + addr ) = data;                                 
;----------------------------------------------------------------------
        ldiu      @CL13,ar0
        ldiu      *-fp(2),ir0
        ldiu      *-fp(3),r0
        sti       r0,*+ar0(ir0)
	.line	4
        ldiu      *-fp(1),ir0
        ldiu      *fp,fp
        subi      2,sp
        bu        ir0
;*      Branch Occurs to ir0 
	.endfunc	308,000000000h,0


	.sect	 ".text"

	.global	_ReadGlue
	.sym	_ReadGlue,_ReadGlue,45,2,0
	.func	312
;******************************************************************************
;* FUNCTION NAME: _ReadGlue                                                   *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,ar0,ir0                                          *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 1 Auto + 0 SOE = 4 words          *
;******************************************************************************
_ReadGlue:
	.sym	_addr,-2,13,9,32
	.sym	_data,1,15,1,32
	.line	1
;----------------------------------------------------------------------
; 312 | USHORT ReadGlue( USHORT addr )                                         
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      1,sp
	.line	2
;----------------------------------------------------------------------
; 314 | ULONG data;                                                            
;----------------------------------------------------------------------
	.line	4
;----------------------------------------------------------------------
; 315 | data = *(ULONG *)(GLUE_BASE + addr );                                  
;----------------------------------------------------------------------
        ldiu      @CL13,ar0
        ldiu      *-fp(2),ir0
        ldiu      *+ar0(ir0),r0
        sti       r0,*+fp(1)
	.line	5
;----------------------------------------------------------------------
; 316 | data &= 0xFFFF;                                                        
;----------------------------------------------------------------------
        and       65535,r0
        sti       r0,*+fp(1)
	.line	6
;----------------------------------------------------------------------
; 317 | return( (USHORT) data );                                               
;----------------------------------------------------------------------
	.line	7
        ldiu      *-fp(1),ir0
        ldiu      *fp,fp
        subi      3,sp
        bu        ir0
;*      Branch Occurs to ir0 
	.endfunc	318,000000000h,1


	.sect	 ".text"

	.global	_WritePCIFifo
	.sym	_WritePCIFifo,_WritePCIFifo,32,2,0
	.func	322
;******************************************************************************
;* FUNCTION NAME: _WritePCIFifo                                               *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,ar0                                              *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 0 Auto + 0 SOE = 3 words          *
;******************************************************************************
_WritePCIFifo:
	.sym	_data,-2,15,9,32
	.line	1
;----------------------------------------------------------------------
; 322 | void WritePCIFifo( ULONG data )                                        
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
	.line	2
	.line	3
;----------------------------------------------------------------------
; 324 | *(ULONG *)GLUE_BASE = data;                                            
;----------------------------------------------------------------------
        ldiu      @CL13,ar0
        ldiu      *-fp(2),r0
        sti       r0,*ar0
	.line	4
        ldiu      *-fp(1),ar0
        ldiu      *fp,fp
        subi      2,sp
        bu        ar0
;*      Branch Occurs to ar0 
	.endfunc	325,000000000h,0


	.sect	 ".text"

	.global	_Set_DPRAM_Bit
	.sym	_Set_DPRAM_Bit,_Set_DPRAM_Bit,32,2,0
	.func	330
;******************************************************************************
;* FUNCTION NAME: _Set_DPRAM_Bit                                              *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,fp,sp                                            *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 2 Parm + 1 Auto + 0 SOE = 5 words          *
;******************************************************************************
_Set_DPRAM_Bit:
	.sym	_addr,-2,13,9,32
	.sym	_val,-3,13,9,32
	.sym	_data,1,13,1,32
	.line	1
;----------------------------------------------------------------------
; 330 | void Set_DPRAM_Bit( USHORT addr, USHORT val )                          
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      1,sp
	.line	2
;----------------------------------------------------------------------
; 332 | USHORT data;                                                           
;----------------------------------------------------------------------
	.line	5
;----------------------------------------------------------------------
; 334 | data = ReadDPRAM( addr );                                              
;----------------------------------------------------------------------
        ldiu      *-fp(2),r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	6
;----------------------------------------------------------------------
; 335 | data |= val;                                                           
;----------------------------------------------------------------------
        ldiu      *-fp(3),r0
        or        *+fp(1),r0
        sti       r0,*+fp(1)
	.line	7
;----------------------------------------------------------------------
; 336 | WriteDPRAM( addr, data );                                              
;----------------------------------------------------------------------
        push      r0
        ldiu      *-fp(2),r0
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	8
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      3,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	337,000000000h,1


	.sect	 ".text"

	.global	_Proc_Write_OpCode
	.sym	_Proc_Write_OpCode,_Proc_Write_OpCode,32,2,0
	.func	342
;******************************************************************************
;* FUNCTION NAME: _Proc_Write_OpCode                                          *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,ar0,fp,ir0,sp,st                              *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 4 Auto + 0 SOE = 7 words          *
;******************************************************************************
_Proc_Write_OpCode:
	.sym	_opcode,-2,13,9,32
	.sym	_data,1,13,1,32
	.sym	_offset,2,13,1,32
	.sym	_datal,3,15,1,32
	.sym	_i,4,4,1,32
	.line	1
;----------------------------------------------------------------------
; 342 | void Proc_Write_OpCode( USHORT opcode )                                
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      4,sp
	.line	2
;----------------------------------------------------------------------
; 344 | USHORT data;                                                           
; 346 | USHORT offset;                                                         
; 347 | ULONG  datal;                                                          
; 348 | int i;                                                                 
;----------------------------------------------------------------------
	.line	10
;----------------------------------------------------------------------
; 351 | offset = opcode & 0x03FF;                                              
; 352 | switch( offset ) {                                                     
; 353 |         case dADR_OPCODE  :
;     |          // OpCode                                                     
; 354 |         case dADR_DSP_VER :
;     |          // DSP Version                                                
;----------------------------------------------------------------------
        bud       L89
        ldiu      1023,r0
        and       *-fp(2),r0
        sti       r0,*+fp(2)
;*      Branch Occurs to L89 
	.line	14
;----------------------------------------------------------------------
; 355 | break;                                                                 
; 357 | //----- PCI FIFO Write ------------------------------------------------
;     | ---------                                                              
; 358 | case dADR_PCI_FIFO_LO :
;     |                                                          // Load PCI FI
;     | FO Low                                                                 
;----------------------------------------------------------------------
	.line	18
;----------------------------------------------------------------------
; 359 | break;                                                                 
; 361 | case dADR_PCI_FIFO_HI :
;     |                                                          // Load PCI FI
;     | FO High                                                                
;----------------------------------------------------------------------
	.line	21
;----------------------------------------------------------------------
; 362 | datal = ReadDPRAM( dADR_PCI_FIFO_LO );
;     |  // Read Low Bits                                                      
;----------------------------------------------------------------------
L29:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	22
;----------------------------------------------------------------------
; 363 | datal |= ( ReadDPRAM( dADR_PCI_FIFO_HI ) << 16 );
;     |          // Read High Bits                                             
;----------------------------------------------------------------------
        ldiu      8,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        ash       16,r0
        or        *+fp(3),r0
        sti       r0,*+fp(3)
	.line	23
;----------------------------------------------------------------------
; 364 | WritePCIFifo( datal );                                                 
;----------------------------------------------------------------------
        push      r0
        call      _WritePCIFifo
                                        ; Call Occurs
	.line	24
;----------------------------------------------------------------------
; 365 | break;                                                                 
; 366 | //----- PCI FIFO Write ------------------------------------------------
;     | ---------                                                              
; 368 | //----- EDS Related Write ---------------------------------------------
;     | ---------                                                              
; 369 | case dADR_NON_LINEAR_CODE :                                            
;----------------------------------------------------------------------
        bud       L109
        subi      1,sp
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L109 
	.line	29
;----------------------------------------------------------------------
; 370 | data = ReadDPRAM( dADR_NON_LINEAR_CODE );                              
;----------------------------------------------------------------------
L31:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	30
;----------------------------------------------------------------------
; 371 | SetNonLinearRam( data );                                               
;----------------------------------------------------------------------
        push      r0
        call      _SetNonLinearRam
                                        ; Call Occurs
	.line	31
;----------------------------------------------------------------------
; 372 | break;                                                                 
; 374 | case dADR_ANAL_MODE :                                                  
;----------------------------------------------------------------------
        bud       L109
        subi      1,sp
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L109 
L32:        
	.line	34
;----------------------------------------------------------------------
; 375 | SetAnalMode();                                                         
;----------------------------------------------------------------------
        call      _SetAnalMode
                                        ; Call Occurs
	.line	35
;----------------------------------------------------------------------
; 376 | break;                                                                 
; 378 | case dADR_TIMECONST :                                                  
;----------------------------------------------------------------------
        bud       L110
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
;*      Branch Occurs to L110 
L33:        
	.line	38
;----------------------------------------------------------------------
; 379 | Set_Time_Const();                                                      
;----------------------------------------------------------------------
        call      _Set_Time_Const
                                        ; Call Occurs
	.line	39
;----------------------------------------------------------------------
; 380 | Set_Blank_Factor();                                                    
;----------------------------------------------------------------------
        call      _Set_Blank_Factor
                                        ; Call Occurs
	.line	40
;----------------------------------------------------------------------
; 381 | break;                                                                 
; 383 | case dADR_BLANK_FACTOR :                                               
;----------------------------------------------------------------------
        bud       L110
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
;*      Branch Occurs to L110 
L34:        
	.line	43
;----------------------------------------------------------------------
; 384 | Set_Blank_Factor();                                                    
;----------------------------------------------------------------------
        call      _Set_Blank_Factor
                                        ; Call Occurs
	.line	44
;----------------------------------------------------------------------
; 385 | break;                                                                 
; 387 | case dADR_ROI_DEF :                                                    
;----------------------------------------------------------------------
        bud       L110
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
;*      Branch Occurs to L110 
L35:        
	.line	47
;----------------------------------------------------------------------
; 388 | DefineROI();                                                           
;----------------------------------------------------------------------
        call      _DefineROI
                                        ; Call Occurs
	.line	48
;----------------------------------------------------------------------
; 389 | break;                                                                 
; 391 | case dADR_ROI_PSET :                                                   
;----------------------------------------------------------------------
        bud       L110
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
;*      Branch Occurs to L110 
L36:        
	.line	51
;----------------------------------------------------------------------
; 392 | PSetROI();                                              // Roi's Change
;     | d So Updatae Look-UIP Tables ( calls to SetROISCARM )                  
;----------------------------------------------------------------------
        call      _PSetROI
                                        ; Call Occurs
	.line	52
;----------------------------------------------------------------------
; 393 | break;                                                                 
; 395 | case dADR_ROI_CLR :                                                    
;----------------------------------------------------------------------
        bud       L110
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
;*      Branch Occurs to L110 
L37:        
	.line	55
;----------------------------------------------------------------------
; 396 | ClrROI();                                               // ROI's Change
;     | d so Update Look-UP Tables ( Calls SetROISCARM )                       
;----------------------------------------------------------------------
        call      _ClrROI
                                        ; Call Occurs
	.line	56
;----------------------------------------------------------------------
; 397 | break;                                                                 
; 399 | case dADR_SCA_DEF :                                                    
;----------------------------------------------------------------------
        bud       L110
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
;*      Branch Occurs to L110 
L38:        
	.line	59
;----------------------------------------------------------------------
; 400 | DefineSCA();                                    // SCA Changed so Updat
;     | e Look-Up Tables ( Calls SETROISCARM )                                 
;----------------------------------------------------------------------
        call      _DefineSCA
                                        ; Call Occurs
	.line	60
;----------------------------------------------------------------------
; 401 | break;                                                                 
; 403 | case dADR_AUTO_DISC :                                                  
;----------------------------------------------------------------------
        bud       L110
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
;*      Branch Occurs to L110 
	.line	63
;----------------------------------------------------------------------
; 404 | data = ReadDPRAM( offset );                                            
;----------------------------------------------------------------------
L40:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	64
;----------------------------------------------------------------------
; 405 | if( data > 0 ){                                                        
;----------------------------------------------------------------------
        cmpi      0,r0
        beqd      L43
	nop
	nop
        ldieq     0,r0
;*      Branch Occurs to L43 
	.line	65
;----------------------------------------------------------------------
; 406 | EDSRec.ADisc = 1;                                                      
;----------------------------------------------------------------------
        ldiu      1,r0
        sti       r0,@_EDSRec+25
	.line	66
;----------------------------------------------------------------------
; 407 | Auto_Disc();                                                           
; 408 | } else {                                                               
;----------------------------------------------------------------------
        call      _Auto_Disc
                                        ; Call Occurs
        bud       L110
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
;*      Branch Occurs to L110 
	.line	68
;----------------------------------------------------------------------
; 409 | EDSRec.ADisc = 0;                                                      
;----------------------------------------------------------------------
L43:        
	.line	70
;----------------------------------------------------------------------
; 411 | break;                                                                 
; 413 | case dADR_EVCH :                                                       
;----------------------------------------------------------------------
        bud       L109
        sti       r0,@_EDSRec+25
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L109 
L44:        
	.line	73
;----------------------------------------------------------------------
; 414 | Set_EvCh();                                                            
;----------------------------------------------------------------------
        call      _Set_EvCh
                                        ; Call Occurs
	.line	74
;----------------------------------------------------------------------
; 415 | break;                                                                 
; 418 | //              case dADR_RTEM_INIT :                                  
; 419 | //                      data= ReadDPRAM( offset );                     
; 420 | //                      if( data != 0 ){                               
; 421 | //                              EDSRec.RTEM_Init = 1;                  
; 422 | //                      } else {                                       
; 423 | //                              EDSRec.RTEM_Init = 0;                  
; 424 | //                      }                                              
; 425 | //                      break;                                         
; 427 | case dADR_PRESET_DONE_CNT :                                            
;----------------------------------------------------------------------
        bud       L110
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
;*      Branch Occurs to L110 
	.line	87
;----------------------------------------------------------------------
; 428 | EDSRec.Preset_Done_Cnt = 0;                                            
;----------------------------------------------------------------------
L46:        
        sti       r0,@_EDSRec+10
	.line	88
;----------------------------------------------------------------------
; 429 | EDSRec.PixelCount = 0;                                                 
;----------------------------------------------------------------------
        sti       r0,@_EDSRec+15
	.line	89
;----------------------------------------------------------------------
; 430 | WriteEDS( iADR_PRESET_DONE_CNT, 0xFFFF );               // Reset FPGA a
;     | s well                                                                 
;----------------------------------------------------------------------
        ldiu      @CL14,r1
        push      r1
        ldiu      380,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	90
;----------------------------------------------------------------------
; 431 | WriteEDS( iADR_PRESET_DONE_CNT2, 0xFFFF );              // reset FPGA a
;     | s well                                                                 
;----------------------------------------------------------------------
        ldiu      @CL14,r0
        ldiu      381,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
	.line	91
;----------------------------------------------------------------------
; 432 | break;                                                                 
; 434 | case dADR_PRESET_POINTS :                                              
;----------------------------------------------------------------------
        bud       L109
        subi      2,sp
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L109 
	.line	94
;----------------------------------------------------------------------
; 435 | data = ReadDPRAM( offset );                                            
;----------------------------------------------------------------------
L48:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	95
;----------------------------------------------------------------------
; 436 | EDSRec.Preset_Done_Points = data;                                      
;----------------------------------------------------------------------
	.line	96
;----------------------------------------------------------------------
; 437 | break;                                                                 
; 451 | default :                                                              
;----------------------------------------------------------------------
        bud       L109
        sti       r0,@_EDSRec+11
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L109 
	.line	111
;----------------------------------------------------------------------
; 452 | if(( offset >= 0x100 ) && ( offset <= 0x1FF )){
;     |                  // Write EDS FPGA                                     
; 453 |         switch( offset ){                                              
; 454 |                 case iADR_CLIP_MIN :                                   
;----------------------------------------------------------------------
L50:        
        blo       L108
;*      Branch Occurs to L108 
        cmpi      511,r0
        bhi       L108
;*      Branch Occurs to L108 
        bud       L76
	nop
        ldiu      *+fp(2),r0
        cmpi      271,r0
;*      Branch Occurs to L76 
	.line	114
;----------------------------------------------------------------------
; 455 | data = ReadDPRAM( offset );                                            
;----------------------------------------------------------------------
L54:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	115
;----------------------------------------------------------------------
; 456 | EDSRec.CLIP_MIN = data;                                                
;----------------------------------------------------------------------
        sti       r0,@_EDSRec+7
	.line	116
;----------------------------------------------------------------------
; 457 | WriteEDS( offset, data );                                              
;----------------------------------------------------------------------
        push      r0
        ldiu      *+fp(2),r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
	.line	117
;----------------------------------------------------------------------
; 458 | break;                                                                 
; 459 | case iADR_CLIP_MAX :                                                   
;----------------------------------------------------------------------
        bud       L109
        subi      2,sp
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L109 
	.line	119
;----------------------------------------------------------------------
; 460 | data = ReadDPRAM( offset );                                            
;----------------------------------------------------------------------
L56:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	120
;----------------------------------------------------------------------
; 461 | EDSRec.CLIP_MAX = data;                                                
;----------------------------------------------------------------------
        sti       r0,@_EDSRec+8
	.line	121
;----------------------------------------------------------------------
; 462 | WriteEDS( offset, data );                                              
;----------------------------------------------------------------------
        push      r0
        ldiu      *+fp(2),r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
	.line	122
;----------------------------------------------------------------------
; 463 | break;                                                                 
; 465 | case iADR_DISC  :                                                      
; 466 | case iADR_EVCH0 :                                                      
; 467 | case iADR_EVCH1 :                                                      
; 468 | case iADR_BIT :                                                        
;----------------------------------------------------------------------
        bud       L109
        subi      2,sp
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L109 
	.line	128
;----------------------------------------------------------------------
; 469 | data = ReadDPRAM( offset );                                            
;----------------------------------------------------------------------
L58:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	129
;----------------------------------------------------------------------
; 470 | Write8420( offset, data );                                             
;----------------------------------------------------------------------
        push      r0
        ldiu      *+fp(2),r0
        push      r0
        call      _Write8420
                                        ; Call Occurs
	.line	130
;----------------------------------------------------------------------
; 471 | break;                                                                 
; 473 | case iADR_TIME_CLR :                                                   
;----------------------------------------------------------------------
        bud       L109
        subi      2,sp
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L109 
	.line	133
;----------------------------------------------------------------------
; 474 | EDSRec.EDS_ISR_CNT = 0;         // Clear the Net Processed Stored Count
;     | s                                                                      
;----------------------------------------------------------------------
L60:        
        sti       r0,@_EDSRec+6
	.line	134
;----------------------------------------------------------------------
; 475 | EDSRec.TMR_ISR_CNT = 0;                                                
;----------------------------------------------------------------------
        sti       r0,@_EDSRec+4
	.line	135
;----------------------------------------------------------------------
; 476 | WriteEDS( offset, 0xFFFF );                                            
;----------------------------------------------------------------------
        ldiu      @CL14,r0
        push      r0
        ldiu      *+fp(2),r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	136
;----------------------------------------------------------------------
; 477 | for(i=0;i<=0xFFF;i++){                                                 
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(4)
        ldiu      0,r1
        cmpi      4095,r0
        bgtd      L64
	nop
	nop
        ldigt     0,r0
;*      Branch Occurs to L64 
	.line	137
;----------------------------------------------------------------------
; 478 | *(ULONG *)( MCA_BASE + i ) = 0;                                        
;----------------------------------------------------------------------
        ldiu      *+fp(4),ir0
        ldiu      @CL15,ar0
L62:        
        sti       r1,*+ar0(ir0)
	.line	136
        ldiu      1,r0
        addi      *+fp(4),r0
        sti       r0,*+fp(4)
        cmpi      4095,r0
        bled      L62
        ldile     *+fp(4),ir0
        ldile     @CL15,ar0
        ldile     *+fp(4),ir0
;*      Branch Occurs to L62 
	.line	140
;----------------------------------------------------------------------
; 481 | for(i=0;i<=0x1FF;i++){                                                 
;----------------------------------------------------------------------
        ldiu      0,r0
L64:        
        sti       r0,*+fp(4)
        cmpi      511,r0
        bgtd      L108
	nop
	nop
        ldiu      0,r1
;*      Branch Occurs to L108 
	.line	141
;----------------------------------------------------------------------
; 482 | *(ULONG *)( BASELINE_BASE + i ) = 0;                                   
;----------------------------------------------------------------------
        ldiu      r0,ir0
        ldiu      @CL16,ar0
L66:        
        sti       r1,*+ar0(ir0)
	.line	140
        ldiu      1,r0
        addi      *+fp(4),r0
        sti       r0,*+fp(4)
        cmpi      511,r0
        bled      L66
        ldile     *+fp(4),ir0
        ldile     @CL16,ar0
        ldile     *+fp(4),ir0
;*      Branch Occurs to L66 
	.line	143
;----------------------------------------------------------------------
; 484 | break;                                                                 
; 486 | case iADR_CW :                                                         
;----------------------------------------------------------------------
        bud       L110
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
;*      Branch Occurs to L110 
	.line	146
;----------------------------------------------------------------------
; 487 | data = ReadDPRAM( offset );
;     |                                                                        
;----------------------------------------------------------------------
L69:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	147
;----------------------------------------------------------------------
; 488 | WriteEDS( offset, data );                                              
;----------------------------------------------------------------------
        push      r0
        ldiu      *+fp(2),r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	148
;----------------------------------------------------------------------
; 489 | EDSRec.DSO_Enable = 0;                                                 
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_EDSRec+14
	.line	149
;----------------------------------------------------------------------
; 490 | if(( data & 0x40 ) == 0x40 )                                           
;----------------------------------------------------------------------
        ldiu      64,r0
        and       *+fp(1),r0
        cmpi      64,r0
        bned      L108
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L108 
	.line	150
;----------------------------------------------------------------------
; 491 | EDSRec.DSO_Enable = 1;                                                 
;----------------------------------------------------------------------
	.line	151
;----------------------------------------------------------------------
; 492 | break;                                                                 
; 493 | //----- DSO Debug Code ------------------------------------------------
;     | -----------------------                                                
; 494 | case iADR_DSO_START :                                                  
;----------------------------------------------------------------------
        bud       L109
        sti       r0,@_EDSRec+14
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L109 
	.line	154
;----------------------------------------------------------------------
; 495 | WriteEDS( iADR_FIFO_MR, 0x0 );
;     |  // Reset EDS FIFO                                                     
;----------------------------------------------------------------------
L72:        
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	155
;----------------------------------------------------------------------
; 496 | WriteEDS( iADR_DSO_START, 0xFFFF );                                    
;----------------------------------------------------------------------
        ldiu      @CL14,r0
        ldiu      263,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
	.line	156
;----------------------------------------------------------------------
; 497 | break;                                                                 
; 498 | //----- DSO Debug Code ------------------------------------------------
;     | -----------------------                                                
; 500 | default :                                                              
;----------------------------------------------------------------------
        bud       L109
        subi      2,sp
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L109 
	.line	160
;----------------------------------------------------------------------
; 501 | data = ReadDPRAM( offset );
;     |                                                                        
;----------------------------------------------------------------------
L74:        
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	161
;----------------------------------------------------------------------
; 502 | WriteEDS( offset, data );                                              
;----------------------------------------------------------------------
        push      r0
        ldiu      *+fp(2),r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
	.line	162
;----------------------------------------------------------------------
; 503 | break;                                                                 
; 506 | }                                                               // swit
;     | ch ...                                                                 
;----------------------------------------------------------------------
        bud       L109
        subi      2,sp
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L109 
	.line	112
L76:        
        bgt       L83
;*      Branch Occurs to L83 
        beqd      L58
	nop
	nop
        ldieq     *+fp(2),r0
;*      Branch Occurs to L58 
        cmpi      258,r0
        beqd      L69
	nop
	nop
        ldieq     *+fp(2),r0
;*      Branch Occurs to L69 
        cmpi      263,r0
        beqd      L72
	nop
        ldieq     0,r0
        ldieq     298,r1
;*      Branch Occurs to L72 
        cmpi      269,r0
        beqd      L54
	nop
	nop
        ldieq     *+fp(2),r0
;*      Branch Occurs to L54 
        cmpi      270,r0
        beqd      L56
	nop
	nop
        ldieq     *+fp(2),r0
;*      Branch Occurs to L56 
        bud       L74
	nop
        ldiu      *+fp(2),r0
        push      r0
;*      Branch Occurs to L74 
L83:        
        cmpi      272,r0
        beqd      L58
	nop
	nop
        ldieq     *+fp(2),r0
;*      Branch Occurs to L58 
        cmpi      273,r0
        beqd      L58
	nop
	nop
        ldieq     *+fp(2),r0
;*      Branch Occurs to L58 
        cmpi      274,r0
        beqd      L58
	nop
	nop
        ldieq     *+fp(2),r0
;*      Branch Occurs to L58 
        cmpi      277,r0
        beqd      L60
	nop
	nop
        ldieq     0,r0
;*      Branch Occurs to L60 
        bud       L74
	nop
        ldiu      *+fp(2),r0
        push      r0
;*      Branch Occurs to L74 
L89:        
	.line	11
        ldiu      *+fp(2),r0
        cmpi      21,r0
        bgt       L100
;*      Branch Occurs to L100 
        beq       L37
;*      Branch Occurs to L37 
        cmpi      0,r0
        beq       L108
;*      Branch Occurs to L108 
        cmpi      2,r0
        beq       L108
;*      Branch Occurs to L108 
        cmpi      7,r0
        beq       L108
;*      Branch Occurs to L108 
        cmpi      8,r0
        beqd      L29
	nop
	nop
        ldieq     7,r0
;*      Branch Occurs to L29 
        cmpi      9,r0
        beq       L33
;*      Branch Occurs to L33 
        cmpi      17,r0
        beq       L32
;*      Branch Occurs to L32 
        cmpi      19,r0
        beq       L35
;*      Branch Occurs to L35 
        cmpi      20,r0
        beq       L36
;*      Branch Occurs to L36 
        bud       L50
	nop
        ldiu      *+fp(2),r0
        cmpi      256,r0
;*      Branch Occurs to L50 
L100:        
        cmpi      26,r0
        beq       L38
;*      Branch Occurs to L38 
        cmpi      29,r0
        beqd      L40
	nop
	nop
        ldieq     *+fp(2),r0
;*      Branch Occurs to L40 
        cmpi      31,r0
        beq       L44
;*      Branch Occurs to L44 
        cmpi      32,r0
        beq       L34
;*      Branch Occurs to L34 
        cmpi      36,r0
        beqd      L46
	nop
	nop
        ldieq     0,r0
;*      Branch Occurs to L46 
        cmpi      37,r0
        beqd      L48
	nop
	nop
        ldieq     *+fp(2),r0
;*      Branch Occurs to L48 
        cmpi      39,r0
        beqd      L31
	nop
	nop
        ldieq     39,r0
;*      Branch Occurs to L31 
        bud       L50
	nop
        ldiu      *+fp(2),r0
        cmpi      256,r0
;*      Branch Occurs to L50 
L108:        
	.line	166
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
L109:        
        subi      6,sp
L110:        
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	507,000000000h,4


	.sect	 ".text"

	.global	_Proc_Read_OpCode
	.sym	_Proc_Read_OpCode,_Proc_Read_OpCode,32,2,0
	.func	512
;******************************************************************************
;* FUNCTION NAME: _Proc_Read_OpCode                                           *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : f0,r0,f1,r1,r2,fp,sp,st                             *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 2 Auto + 0 SOE = 5 words          *
;******************************************************************************
_Proc_Read_OpCode:
	.sym	_opcode,-2,13,9,32
	.sym	_data,1,13,1,32
	.sym	_offset,2,13,1,32
	.line	1
;----------------------------------------------------------------------
; 512 | void Proc_Read_OpCode( USHORT opcode )                                 
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      2,sp
	.line	2
;----------------------------------------------------------------------
; 515 | USHORT data;                                                           
; 517 | USHORT offset;                                                         
;----------------------------------------------------------------------
	.line	8
;----------------------------------------------------------------------
; 519 | offset = opcode & 0x03FF;                                              
; 521 | switch ( offset ) {                                                    
; 522 |         //----- Miscellaneouse Read Parameters ------------------------
;     | ------------------                                                     
; 523 |         case dADR_OPCODE :                              // OpCode      
; 524 |         case dADR_PCI_FIFO_LO :                                        
; 525 |         case dADR_PCI_FIFO_HI :                                        
;----------------------------------------------------------------------
        bud       L128
        ldiu      1023,r0
        and       *-fp(2),r0
        sti       r0,*+fp(2)
;*      Branch Occurs to L128 
	.line	15
;----------------------------------------------------------------------
; 526 | break;                                  // Do nothing                  
; 528 | case dADR_DSP_VER :                                                    
;----------------------------------------------------------------------
	.line	18
;----------------------------------------------------------------------
; 529 | WriteDPRAM( offset, (ULONG)DSP_VER );                                  
;----------------------------------------------------------------------
L114:        
        push      r0
        ldiu      *+fp(2),r0
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
	.line	19
;----------------------------------------------------------------------
; 530 | break;                                                                 
; 532 | //              case dADR_RTEM_INIT :                                  
; 533 | //                      WriteDPRAM( offset, EDSRec.RTEM_Init );        
; 534 | //                      break;                                         
; 536 | case dADR_AUTO_DISC :                                                  
;----------------------------------------------------------------------
        bud       L137
        subi      2,sp
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L137 
	.line	26
;----------------------------------------------------------------------
; 537 | data = ( 12 - EDSRec.ASHBit ) * 100;                                   
;----------------------------------------------------------------------
L116:        
        subri     12,r0                 ; Unsigned
        lsh3      r2,r0,r2
        and       65535,r1
        and       65535,r0
        mpyi3     r2,r1,r2
        mpyi3     r1,r0,r0
        lsh       16,r2
        addi3     r2,r0,r0
        sti       r0,*+fp(1)
	.line	27
;----------------------------------------------------------------------
; 538 | data = (USHORT)((float)data / (float)12.0f );                          
;----------------------------------------------------------------------
        float     *+fp(1),f1
        ldflt     @CL17,f0
        ldfge     0.0000000000e+00,f0
        addf3     f1,f0,f1
        mpyf      @CL18,f1
        cmpf      @CL19,f1
        ldflt     0.0000000000e+00,f0
        ldfge     @CL17,f0
        subrf     f1,f0
        fix       f0,r0
        sti       r0,*+fp(1)
	.line	28
;----------------------------------------------------------------------
; 539 | WriteDPRAM( offset, data );                                            
;----------------------------------------------------------------------
        push      r0
        ldiu      *+fp(2),r0
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
	.line	29
;----------------------------------------------------------------------
; 540 | break;                                                                 
; 542 | case dADR_PRESET_DONE_CNT :                                            
;----------------------------------------------------------------------
        bud       L137
        subi      2,sp
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L137 
	.line	32
;----------------------------------------------------------------------
; 543 | WriteDPRAM( offset, EDSRec.PixelCount  );                              
;----------------------------------------------------------------------
L118:        
        push      r0
        ldiu      *+fp(2),r0
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
	.line	33
;----------------------------------------------------------------------
; 544 | break;                                                                 
; 546 | case dADR_PRESET_POINTS :                                              
;----------------------------------------------------------------------
        bud       L137
        subi      2,sp
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L137 
	.line	36
;----------------------------------------------------------------------
; 547 | WriteDPRAM( offset, EDSRec.Preset_Done_Points );                       
;----------------------------------------------------------------------
L120:        
        push      r0
        ldiu      *+fp(2),r0
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
	.line	37
;----------------------------------------------------------------------
; 548 | break;                                                                 
; 550 | //----- Miscellaneouse Read Parameters --------------------------------
;     | ----------                                                             
; 551 | default :                                                              
;----------------------------------------------------------------------
        bud       L137
        subi      2,sp
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L137 
	.line	41
;----------------------------------------------------------------------
; 552 | if(( offset >= 0x100 ) && ( offset <= 0x1FF )){
;     |  // Read EDS FPGA                                                      
; 553 |         switch( offset ){                                              
; 554 |                 default :                                              
;----------------------------------------------------------------------
L122:        
        blo       L136
;*      Branch Occurs to L136 
        cmpi      511,r0
        bhi       L136
;*      Branch Occurs to L136 
	.line	44
;----------------------------------------------------------------------
; 555 | data = ReadEDS( offset );                                              
;----------------------------------------------------------------------
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
	.line	45
;----------------------------------------------------------------------
; 556 | break;                                                                 
;----------------------------------------------------------------------
        bud       L126
        sti       r0,*+fp(1)
        nop
        push      r0
;*      Branch Occurs to L126 
	.line	42
	.line	47
;----------------------------------------------------------------------
; 558 | WriteDPRAM( offset, data );                                            
;----------------------------------------------------------------------
L126:        
        ldiu      *+fp(2),r0
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
	.line	49
;----------------------------------------------------------------------
; 560 | break;                                                                 
;----------------------------------------------------------------------
        bud       L137
        subi      2,sp
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
;*      Branch Occurs to L137 
L128:        
	.line	10
        ldi       *+fp(2),r0
        beq       L136
;*      Branch Occurs to L136 
        cmpi      2,r0
        beqd      L114
	nop
	nop
        ldieq     16432,r0
;*      Branch Occurs to L114 
        cmpi      7,r0
        beq       L136
;*      Branch Occurs to L136 
        cmpi      8,r0
        beq       L136
;*      Branch Occurs to L136 
        cmpi      29,r0
        beqd      L116
        ldieq     @_EDSRec+26,r0
        ldieq     -16,r2
        ldieq     100,r1
;*      Branch Occurs to L116 
        cmpi      36,r0
        beqd      L118
	nop
	nop
        ldieq     @_EDSRec+15,r0
;*      Branch Occurs to L118 
        cmpi      37,r0
        beqd      L120
	nop
	nop
        ldieq     @_EDSRec+11,r0
;*      Branch Occurs to L120 
        bud       L122
	nop
        ldiu      *+fp(2),r0
        cmpi      256,r0
;*      Branch Occurs to L122 
L136:        
	.line	51
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
L137:        
        subi      4,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	562,000000000h,2



	.global	_DPPRec
	.bss	_DPPRec,7
	.sym	_DPPRec,_DPPRec,8,2,224,.fake21
;******************************************************************************
;* CONSTANT TABLE                                                             *
;******************************************************************************
	.sect	".const"
	.bss	CL1,1
	.bss	CL2,1
	.bss	CL3,1
	.bss	CL4,1
	.bss	CL5,1
	.bss	CL6,1
	.bss	CL7,1
	.bss	CL8,1
	.bss	CL9,1
	.bss	CL10,1
	.bss	CL11,1
	.bss	CL12,1
	.bss	CL13,1
	.bss	CL14,1
	.bss	CL15,1
	.bss	CL16,1
	.bss	CL17,1
	.bss	CL18,1
	.bss	CL19,1

	.sect	".cinit"
	.field  	19,32
	.field  	CL1+0,32
	.field  	8421472,32
	.field  	8421408,32
	.field  	8421424,32
	.field  	8421440,32
	.field  	2035720,32
	.field  	2035784,32
	.field  	750000,32
	.field  	7500000,32
	.field  	202842948,32
	.field  	196611,32
	.field  	9437184,32
	.field  	9445376,32
	.field  	9453568,32
	.field  	65535,32
	.field  	8929280,32
	.field  	8933376,32
	.word   	020000000H ; float   4.294967296000000e+09
	.word   	0FC2AAAABH ; float   8.333333333333333e-02
	.word   	01F000000H ; float   2.147483648000000e+09

	.sect	".text"
;******************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                              *
;******************************************************************************

	.global	_ClrROI

	.global	_EDSRec

	.global	_SetNonLinearRam

	.global	_Set_Blank_Factor

	.global	_Set_EvCh

	.global	_Auto_Disc

	.global	_DefineSCA

	.global	_WriteEDS

	.global	_ReadEDS

	.global	_Write8420

	.global	_Set_Time_Const

	.global	_SetAnalMode

	.global	_DefineROI

	.global	_PSetROI
