;********************************************************************************/
;*																*/
;*	Module:	TMR_data.ASM  											*/
;*																*/
;*	EDAX Inc														*/
;*																*/
;*  Author:		Michael Solazzi									*/
;*  Created:    3/25/99 												*/
;*  Board:      EDI-I;												*/
;*	Description:													*/
;*		This Code copy all of the data at the 100ms bountry to the DPRAM		*/
;*																*/
;*  History:														*/
;*		99/12/20 - MCS - Code Split from original routine					*/
;*		99/11/29 - MCS - Updated										*/
;*		99/08/16 - MCS - Updated for EDI-II Rev B						*/
;*																*/
;*	ISR Interrupts are automatically disabled							*/
;*	asm("	and     0DFFFH,st       ;disable interrupts	");				*/
;*	Re-Enable Interrupts, automatically done							*/
;*	asm("	or     2000H,st       ;enable interrupts	");				*/
;********************************************************************************/


;******************************************************************************
;* FUNCTION NAME: _Proc_Tmr_Isr                                               *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : f0,r0,f1,ar0,st                                     *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 4 Auto + 0 SOE = 6 words          *
;******************************************************************************
;
; Uses AR0, AR1 
;	R1
;	R2	= Key
	.global _EDSRec, _Proc_Tmr_Isr
_Proc_Tmr_Isr:

;----- DSP Address Bus Decodes -------------------------------------
JUNK					.LONG 0900000H
DPRAM_BASE				.LONG 0900000H
DPRAM_EBASE				.LONG 0902000H	; was 90239h
EDS_BASE				.LONG 0906000H
PCI_FIFO_BASE			.LONG 0904000H
MCARec					.LONG 0884000H
BaselineRec				.LONG 0885000H

;----- EDS FPGA Offsets --------------------------------------------
iADR_STAT				.set 001H
iADR_CW					.set 002H
iADR_START				.set 039H	; Start at iADR_TMP_LO 
iADR_LAST				.set 055H	; End at iADR_RTEM_SEL

;----- DPRAM Offsets ------------------------------------------------
dADR_EDS_STAT			.set  004H	; Status in DPRAM

;----- EDS Record Offsets ---------------------------------------------
rEDS_Stat				.set 002H	; EDS Record
rCLIP_MIN				.set 007H
rCLIP_MAX				.set 008H
rPreset_Mode			.set 009H

;----- Constant 
kFPGA_PRESET_PAMR				.set 006H
kFPGA_PRESET_LSMAP_REAL			.set 007H		; Preset_SMap - 1, testing for negative
kFPGA_PRESET_LSMAP_LIVE			.set 008H
kFPGA_PRESET_WDS_REAL			.set 009H
kFPGA_PRESET_WDS_LIVE			.set 00AH


DPRAM_BASE_REG			.set AR0;
DPRAM_EBASE_REG			.set AR1;
EDS_BASE_REG			.set AR2;
MCA_BASE_REG			.set AR3;
PCI_FIFO_REG			.set AR4;

TMP_REG				.set R0;
FF_DATA				.set R1;
ZERO_REG				.set R2;
FF_KEY				.set R3;
FF_MAX				.set R4

; Push R0-R7 onto Stack						;		;
	PUSH IR0;								;	1	;	
	PUSH IR1;								;	1	;
	PUSH TMP_REG;							;	1	;
	PUSH FF_DATA;							;	1	;
	PUSH ZERO_REG;							;	1	;
	PUSH FF_KEY;							;	1	;
	PUSH FF_MAX;							;	1	;
			
; Push AR0-AR7 onto	Stack		
	PUSH DPRAM_BASE_REG						;	1	;
	PUSH DPRAM_EBASE_REG					;	1	;
	PUSH EDS_BASE_REG						;	1	;
	PUSH MCA_BASE_REG						;	1	;
	PUSH PCI_FIFO_REG						;	1	;
	PUSH DP;								;	1	;

	LDP _EDSRec;							;	1	;
	
	LDP _Proc_Tmr_Isr;						;	1	;	Load the Data Page to Timer ISR
	; All other stuff at 100ms
	LDI @DPRAM_BASE,		DPRAM_BASE_REG;		;	1	; Load DPRAM Base into AR0
	LDI @DPRAM_EBASE,		DPRAM_EBASE_REG 	;	1	; Load EDS Base in DPRAM
	LDI @EDS_BASE,			EDS_BASE_REG;		;	1	; Load EDS Address Base into AR1
	LDI @PCI_FIFO_BASE,		PCI_FIFO_REG;		;	1	; Setup the PCI FIFO Base Address
	LDI @MCARec,			MCA_BASE_REG;		;	1	; Setup the MCA Base Register

	;----- Get EDS Status -----------------------------------------------------
	LDI iADR_STAT, IR0;							;	1	; Load FPGA Offset into R0
	LDI *+EDS_BASE_REG(IR0), TMP_REG;			;	1	; Read the EDS STatus from FPGA into R0
	STI TMP_REG, *+DPRAM_EBASE_REG(IR0);		;	1	; Copy TMP_REG into DPRAM Memory
	LSH -3, TMP_REG;							;	1	; Shift R0 Right by 3
	AND 1, TMP_REG;								;	1	; Logical AND ( Keep only 1 bit )
	STI TMP_REG, @_EDSRec+rEDS_Stat;			;	1	; Store into EDSRec.EDS_Stat


	LDI dADR_EDS_STAT, IR0;						;	1	; Set the DPRAM Displacement into IR0
	LDI *+DPRAM_BASE_REG(IR0), FF_DATA;			;	1	; Read The DPRAM Status Register into R1
	AND 0FFFEH, FF_DATA;						;	1	; Clear the LSB ONLY
	OR	TMP_REG, FF_DATA;						;	1	; Or R0 into R1
	STI FF_DATA, *+DPRAM_BASE_REG(IR0)			;	1	; Store R1 into the DPRAM Status
	;----- Get EDS Status -----------------------------------------------------
		
	;****** Parameter Coping Loop from the EDS FPGA to the DPRAM *************************
	;----- Copy the following parameters from the EDS FPGA to the DPRAM
	LDI iADR_START, IR0;					;	1	; Load First Address into IR0

	;****** Parameter Coping Loop from the EDS FPGA to the DPRAM *************************
	;----- Clock Count for the Main Loop --------;    5	;------------------------------
STAT_LOOP
	CMPI iADR_LAST, IR0;						;	1	; Compare against the last address
	BNZD STAT_LOOP								;	4	; If Not Zero Loop to the Next 
	LDI *+EDS_BASE_REG(IR0), TMP_REG;			;	0	; Get Data and copy into TMP_REG
	STI TMP_REG, *+DPRAM_EBASE_REG(IR0);		;	0	; Copy TMP_REG into DPRAM Memory
	ADDI 1, IR0;								;	0	; Increment IR0

	;****** Parameter Coping Loop from the EDS FPGA to the DPRAM *************************

	;****** Test to see if Spectral Mapping Mode, if so exit
	LDI @_EDSRec+rPreset_Mode, IR0;				;	1	; Get the Preset Mode
	LDI kFPGA_PRESET_LSMAP_REAL, TMP_REG;		;	1	; Load Preset Live Spectrum Map Real into TMP_REG
	SUBI 1, TMP_REG;							;	1	; Sub 1 from TMP_REG
	SUBI IR0, TMP_REG;							;	1	; TMP_REG = TMP_REG - IR0 (6-Preset Mode)
	BN  EXIT_ISR;								;	4	; Branch on Negative result

;	CMPI kFPGA_PRESET_LSMAP_REAL, TMP_REG;			;	1	; TMP_REG(PreseT_Mode)-kPreset_SMap(5)
;	BZ EXIT_ISR;
;	CMPI kFPGA_PRESET_LSMAP_LIVE, TMP_REG;			;	1	; TMP_REG(PreseT_Mode)-kPreset_SMap(5)
;	BZ EXIT_ISR;
;	CMPI kFPGA_PRESET_WDS_REAL, TMP_REG;
;	BZ EXIT_ISR;
;	CMPI kFPGA_PRESET_WDS_LIVE, TMP_REG;
;	BZ EXIT_ISR;

	;****** Spectrum Processing Loop *****************************************************
	;----- Now Process the Spectrum --------------------------------------------
	;-- Setup to Process the Spectrum
	;-- Get Clip Min, ( the starting point )
	;-- Crete the FIFO Key
	;----- Get the Detect Max from the FPGA
	LDI @_EDSRec+rCLIP_MIN, IR0;			;	1	; Get Starting Point
	LDI @_EDSRec+rCLIP_MAX, FF_MAX;			;	1	; Get Ending Point

	LDI 0H, ZERO_REG;						;	1	; Initialize Zero Register
	LDI 01000H, FF_KEY;						;	1	; Load 1000h to the FF Key
	LSH 16, FF_KEY;						;	1	; Shift up by 16

	; First Point ( since it is incorporated at the end of the loop )
	LDI *+MCA_BASE_REG(IR0), TMP_REG
||	STI ZERO_REG, *+MCA_BASE_REG(IR0);			;	1	; TMP_REG contains the number of Counts

	;****** Spectrum Processing Loop *****************************************************
	;----- Clock Count for the Main Loop --------;  16	;------------------------------
SPRS
	AND 0FFFFH, TMP_REG;					;	1	; Clip the Number of Bits
	BZD SPSK;								;	4	; If Zero - Skip and Restart to Inc IR0

	OR3 FF_KEY, IR0, FF_DATA;				;	0	; Form the Data
	LSH 12, TMP_REG;						;	0	; Shift TMP_REG ( the Counts ) up by 12 Bits
	OR TMP_REG, FF_DATA;					;	0	; Combine Counts with the FIFO Key and Channel Number

	;----- Check the PCI FIFO Status to see for FIFO Full-------------------------------------------
SPCI
	LDI *PCI_FIFO_REG, TMP_REG;				;	1	; Get the PCI FIFO Status
	AND 00CH, TMP_REG;						;	1	; Keep only the PCI FIFO Full Flag Bits
	BNZ SPCI;								;	4	; If Zero FIFO is Full, Recheck
	;----- Check the PCI FIFO Status to see for FIFO Full-------------------------------------------
	STI FF_DATA, *PCI_FIFO_REG;				;	0	; Store Data into PCI FIFO Register

SPSK
	CMPI FF_MAX, IR0;						;	1	; Compare Against Clip_Max
	BNZD SPRS;							;	4	; If Negative, Restart ( otherwise Exit )
	ADDI 1, IR0;							;	0	; Increment Index
	NOP									;	0	;
	LDI *+MCA_BASE_REG(IR0), TMP_REG
||	STI ZERO_REG, *+MCA_BASE_REG(IR0);			;	0	; TMP_REG contains the number of Counts
	;****** Spectrum Processing Loop *****************************************************

	;****** Baseline Histogram Processing Loop *******************************************
	; First see of Baseline is enabled
	LDI iADR_CW, IR0;						;	1	; load Control Word offset to IR0
	LDI *+EDS_BASE_REG(IR0), TMP_REG;			;	1	; Read Control Word from FPGA
	AND 004H, TMP_REG;						;	1	; Check Bit 2
	BZ EXIT_ISR;							;	4	; If Zero ( don't Process so Exit

BASE_LOOP
	LDI 0, IR0							;	1	;
	LDI 01FFH, FF_MAX;						;	1	;

	LDI 02000H, FF_KEY;						;	1	; Load 2000h to the FF Key
	LSH 16, FF_KEY;						;	1	; Shift up by 16
	LDI @BaselineRec,		MCA_BASE_REG;		;	1	; Redefine MCA_BASE_REG with Baseline Offset

	; First Point ( since it is incorporated at the end of the loop )
	LDI *+MCA_BASE_REG(IR0), TMP_REG  			
||	STI ZERO_REG, *+MCA_BASE_REG(IR0);			;	1	; TMP_REG contains the number of Counts
	AND 0FFFFH, TMP_REG;					;	1	; Clip the Number of Bits
;	BZD BPSK;								;	4	; If Zero - Skip and Restart to Inc IR0

	;****** Baseline Histogram Processing Loop *******************************************
	;----- Clock Count for the Main Loop --------;   14	;------------------------------
BPRS
	OR3 FF_KEY, IR0, FF_DATA;				;	1	; Form the Data
	LSH 12, TMP_REG;						;	1	; Shift TMP_REG ( the Counts ) up by 12 Bits
	OR TMP_REG, FF_DATA;					;	1	; Combine Counts with the FIFO Key and Channel Number

	;----- Check the PCI FIFO Status to see for FIFO Full-------------------------------------------
BPCI
	LDI *PCI_FIFO_REG, TMP_REG;				;	1	; Get the PCI FIFO Status
	AND 00CH, TMP_REG;						;	1	; Keep only the PCI FIFO Full Flag Bits
	BNZ BPCI;								;	4	; If not Zero FIFO is Full, Recheck
	;----- Check the PCI FIFO Status to see for FIFO Full-------------------------------------------
	STI FF_DATA, *PCI_FIFO_REG;				;	1	; Store Data into PCI FIFO Register

BPSK
	CMPI FF_MAX, IR0;						;	1	; Compare Against Clip_Max
	BNZD BPRS;							;	4	; If Negative, Restart ( otherwise Exit )
	ADDI 1, IR0;							;	0	; Increment Index
	LDI *+MCA_BASE_REG(IR0), TMP_REG  			
||	STI ZERO_REG, *+MCA_BASE_REG(IR0);			;	0	; TMP_REG contains the number of Counts
	AND 0FFFFH, TMP_REG;					;	0	; Clip the Number of Bits
	;****** Baseline Processing Loop ****************************************************
EXIT_ISR

	;***** EDS ISR Count *****************************************************************
	POP DP;								;	1	;
; POP AR7-AR0 from the Stack
	POP PCI_FIFO_REG						;	1	;
	POP MCA_BASE_REG						;	1	;
	POP EDS_BASE_REG						;	1	;
	POP DPRAM_EBASE_REG;					;	1	;
	POP DPRAM_BASE_REG						;	1	;


; Pop R7-R0 from the Stack
	POP FF_MAX;								;	1	;
	POP FF_KEY;								;	1	;
	POP ZERO_REG;							;	1	;
	POP FF_DATA;							;	1	;
	POP TMP_REG;							;	1	;
	POP IR1;								;	1	;
	POP IR0;								;	1	;	

	RETS;							; Return from Subroutine
.endfunc

;******************************************************************************
; End of TMR_ISR.ASM
;******************************************************************************

