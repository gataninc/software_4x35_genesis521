;******************************************************************************
;* TMS320C3x/4x ANSI C Code Generator                            Version 5.11 *
;* Date/Time created: Thu May 24 17:11:50 2007                                *
;******************************************************************************
	.regalias	; enable floating point register aliases
fp	.set	ar3
FP	.set	ar3
;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Silicon Info       : C32 Revision PG1                                    *
;*   Optimization       : Conservatively Choose Speed over Size               *
;*   Memory             : Small Memory Model                                  *
;*   Float-to-Int       : Fast Conversions (round toward -inf)                *
;*   Multiply           : in Software (32 bits)                               *
;*   Memory Info        : Unmapped Memory Exists                              *
;*   Repeat Loops       : Use RPTS and/or RPTB                                *
;*   Calls              : Normal Library ASM calls                            *
;*   Debug Info         : Optimized TI Debug Information                      *
;******************************************************************************
;	c:\c3xtools\bin\ac30.exe -v32 ep99850.c C:\DOCUME~1\MSOLAZ~1.AME\LOCALS~1\Temp\ep99850.if 
	.file	"ep99850.c"
	.file	"bus32.h"
	.stag	.fake1,32
	.member	_holdst,0,14,18,1
	.member	_nohold,1,14,18,1
	.member	_hiz,2,14,18,1
	.member	_sww,3,14,18,2
	.member	_wtcnt,5,14,18,3
	.member	_bnkcmp,8,14,18,5
	.member	_r_131415,13,14,18,3
	.member	_datasize,16,14,18,2
	.member	_memwidth,18,14,18,2
	.member	_signext,20,14,18,1
	.member	_strbcnfg,21,14,18,1
	.member	_strbsw,22,14,18,1
	.member	_r_rest,23,14,18,9
	.eos
	.utag	.fake0,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake1
	.eos
	.sym	_STRB0_BUS_CONTROL,0,9,13,32,.fake0
	.stag	.fake3,32
	.member	_r_012,0,14,18,3
	.member	_sww,3,14,18,2
	.member	_wtcnt,5,14,18,3
	.member	_bnkcmp,8,14,18,5
	.member	_r_131415,13,14,18,3
	.member	_datasize,16,14,18,2
	.member	_memwidth,18,14,18,2
	.member	_signext,20,14,18,1
	.member	_r_rest,21,14,18,11
	.eos
	.utag	.fake2,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake3
	.eos
	.sym	_STRB1_BUS_CONTROL,0,9,13,32,.fake2
	.stag	.fake5,32
	.member	_r_012,0,14,18,3
	.member	_sww,3,14,18,2
	.member	_wtcnt,5,14,18,3
	.member	_r_rest,8,14,18,24
	.eos
	.utag	.fake4,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake5
	.eos
	.sym	_IOSTRB_BUS_CONTROL,0,9,13,32,.fake4
	.stag	.fake6,288
	.member	__ioctrl,0,9,8,32,.fake4
	.member	_reserved1,32,62,8,96,,3
	.member	__s0ctrl,128,9,8,32,.fake0
	.member	_reserved2,160,62,8,96,,3
	.member	__s1ctrl,256,9,8,32,.fake2
	.eos
	.sym	_BUS_REG,0,8,13,288,.fake6
	.file	"timer30.h"
	.stag	.fake8,32
	.member	_func,0,14,18,1
	.member	_i_o,1,14,18,1
	.member	_datout,2,14,18,1
	.member	_datin,3,14,18,1
	.member	_r_45,4,14,18,2
	.member	_go,6,14,18,1
	.member	_hld_,7,14,18,1
	.member	_cp_,8,14,18,1
	.member	_clksrc,9,14,18,1
	.member	_inv,10,14,18,1
	.member	_tstat,11,14,18,1
	.member	_r_rest,12,14,18,20
	.eos
	.utag	.fake7,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake8
	.eos
	.sym	_TIMER_CONTROL,0,9,13,32,.fake7
	.stag	.fake9,288
	.member	__gctrl,0,9,8,32,.fake7
	.member	_reserved1,32,62,8,96,,3
	.member	_counter,128,14,8,32
	.member	_reserved2,160,62,8,96,,3
	.member	_period,256,14,8,32
	.eos
	.sym	_TIMER_REG,0,8,13,288,.fake9
	.file	"serprt30.h"
	.stag	.fake11,32
	.member	_rrdy,0,14,18,1
	.member	_xrdy,1,14,18,1
	.member	_fsxout,2,14,18,1
	.member	_xsrempty,3,14,18,1
	.member	_rsrfull,4,14,18,1
	.member	_hs,5,14,18,1
	.member	_xclksrce,6,14,18,1
	.member	_rclksrce,7,14,18,1
	.member	_xvaren,8,14,18,1
	.member	_rvaren,9,14,18,1
	.member	_xfsm,10,14,18,1
	.member	_rfsm,11,14,18,1
	.member	_clkxp,12,14,18,1
	.member	_clkrp,13,14,18,1
	.member	_dxp,14,14,18,1
	.member	_drp,15,14,18,1
	.member	_fsxp,16,14,18,1
	.member	_fsrp,17,14,18,1
	.member	_xlen,18,14,18,2
	.member	_rlen,20,14,18,2
	.member	_xtint,22,14,18,1
	.member	_xint,23,14,18,1
	.member	_rtint,24,14,18,1
	.member	_rint,25,14,18,1
	.member	_xreset,26,14,18,1
	.member	_rreset,27,14,18,1
	.member	_r_rest,28,14,18,4
	.eos
	.utag	.fake10,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake11
	.eos
	.sym	_SERIAL_PORT_CONTROL,0,9,13,32,.fake10
	.stag	.fake13,32
	.member	_clkfunc,0,14,18,1
	.member	_clki_o,1,14,18,1
	.member	_clkdato,2,14,18,1
	.member	_clkdati,3,14,18,1
	.member	_dfunc,4,14,18,1
	.member	_di_o,5,14,18,1
	.member	_ddatout,6,14,18,1
	.member	_ddatin,7,14,18,1
	.member	_fsfunc,8,14,18,1
	.member	_fsi_o,9,14,18,1
	.member	_fsdatout,10,14,18,1
	.member	_fsdatin,11,14,18,1
	.member	_r_rest,12,14,18,20
	.eos
	.utag	.fake12,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake13
	.eos
	.sym	_RX_PORT_CONTROL,0,9,13,32,.fake12
	.stag	.fake15,32
	.member	_xgo,0,14,18,1
	.member	_xhld_,1,14,18,1
	.member	_xcp_,2,14,18,1
	.member	_xclksrc,3,14,18,1
	.member	_r_4,4,14,18,1
	.member	_xtstat,5,14,18,1
	.member	_rgo,6,14,18,1
	.member	_rhld_,7,14,18,1
	.member	_rcp_,8,14,18,1
	.member	_rclksrc,9,14,18,1
	.member	_r_10,10,14,18,1
	.member	_rtstat,11,14,18,1
	.member	_r_rest,12,14,18,20
	.eos
	.utag	.fake14,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake15
	.eos
	.sym	_RX_TIMER_CONTROL,0,9,13,32,.fake14
	.stag	.fake17,32
	.member	_x_counter,0,14,18,16
	.member	_r_counter,16,14,18,16
	.eos
	.utag	.fake16,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake17
	.eos
	.sym	_RX_TIMER_COUNTER,0,9,13,32,.fake16
	.stag	.fake19,32
	.member	_x_period,0,14,18,16
	.member	_r_period,16,14,18,16
	.eos
	.utag	.fake18,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake19
	.eos
	.sym	_RX_TIMER_PERIOD,0,9,13,32,.fake18
	.stag	.fake20,512
	.member	__gctrl,0,9,8,32,.fake10
	.member	_reserved1,32,14,8,32
	.member	__xctrl,64,9,8,32,.fake12
	.member	__rctrl,96,9,8,32,.fake12
	.member	__rxtctrl,128,9,8,32,.fake14
	.member	__rxtcounter,160,9,8,32,.fake16
	.member	__rxtperiod,192,9,8,32,.fake18
	.member	_reserved2,224,14,8,32
	.member	_x_data,256,14,8,32
	.member	_reserved3,288,62,8,96,,3
	.member	_r_data,384,14,8,32
	.member	_reserved4,416,62,8,96,,3
	.eos
	.sym	_SERIAL_PORT_REG,0,8,13,512,.fake20
	.file	"defines.h"
	.sym	_UCHAR,0,12,13,32
	.sym	_USHORT,0,13,13,32
	.sym	_ULONG,0,15,13,32
	.file	"edsutl.h"
	.stag	.fake21,1088
	.member	_FPGA_Preset_Done,0,13,8,32
	.member	_EDSXFer,32,13,8,32
	.member	_EDS_Stat,64,13,8,32
	.member	_DSO_PENDING,96,13,8,32
	.member	_TMR_ISR_CNT,128,15,8,32
	.member	_EDS_IRQ,160,12,8,32
	.member	_EDS_ISR_CNT,192,15,8,32
	.member	_CLIP_MIN,224,13,8,32
	.member	_CLIP_MAX,256,13,8,32
	.member	_Preset_Mode,288,13,8,32
	.member	_Preset_Done_Cnt,320,13,8,32
	.member	_Preset_Done_Points,352,13,8,32
	.member	_DSO_Index,384,13,8,32
	.member	_FFT_Mode,416,13,8,32
	.member	_DSO_Enable,448,13,8,32
	.member	_PixelCount,480,13,8,32
	.member	_EDS_Mode,512,13,8,32
	.member	_EDS_DEF_ROIS,544,13,8,32
	.member	_Max_Fir_Cnt,576,13,8,32
	.member	_Max_Fir,608,61,8,128,,4
	.member	_RTEM_Init,736,13,8,32
	.member	_RTEM_Target,768,13,8,32
	.member	_ADisc,800,13,8,32
	.member	_ASHBit,832,13,8,32
	.member	_ADiscVal,864,61,8,160,,5
	.member	_EDX_ANAL,1024,13,8,32
	.member	_Reset_FIR,1056,13,8,32
	.eos
	.sym	_EDS_TYPE,0,8,13,1088,.fake21
	.stag	.fake22,160
	.member	_Start,0,13,8,32
	.member	_End,32,13,8,32
	.member	_Sca,64,13,8,32
	.member	_Enable,96,12,8,32
	.member	_Defined,128,12,8,32
	.eos
	.sym	_ROI_TYPE,0,8,13,160,.fake22
	.file	"dpp2utl.h"
	.stag	.fake23,224
	.member	_PCI_IRQ,0,12,8,32
	.member	_IRQ_RESET,32,13,8,32
	.member	_MS100_TC,64,13,8,32
	.member	_OpCodeAck,96,13,8,32
	.member	_PCIIRQ_Cnt,128,15,8,32
	.member	_OpCode,160,13,8,32
	.member	_Set_PCI_IRQ,192,13,8,32
	.eos
	.sym	_DPP_TYPE,0,8,13,224,.fake23
	.file	"dpp2bit.h"
	.file	"ep99850.c"
	.sect	 ".text"

	.global	_c_int01
	.sym	_c_int01,_c_int01,32,2,0
	.func	67
;******************************************************************************
;* FUNCTION NAME: _c_int01                                                    *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0                                                  *
;*   Regs Saved         : f0,r0                                               *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 0 Auto + 2 SOE = 4 words          *
;******************************************************************************
_c_int01:
	.line	1
;----------------------------------------------------------------------
;  67 | void IntFromEDS(void)   // no arguments no return value allowed        
;----------------------------------------------------------------------
        push      st
        push      fp
        ldiu      sp,fp
        push      r0
        pushf     f0
	.line	3
;----------------------------------------------------------------------
;  69 | EDSRec.EDS_IRQ = 1;                                                    
;----------------------------------------------------------------------
        ldiu      1,r0
        sti       r0,@_EDSRec+5
	.line	4
        popf      f0
        pop       r0
        pop       fp
        pop       st
        reti
                                        	; Return From Interrupt
	.endfunc	70,000000001h,0


	.sect	 ".text"

	.global	_c_int09
	.sym	_c_int09,_c_int09,32,2,0
	.func	77
;******************************************************************************
;* FUNCTION NAME: _c_int09                                                    *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0                                                  *
;*   Regs Saved         : f0,r0                                               *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 0 Auto + 2 SOE = 4 words          *
;******************************************************************************
_c_int09:
	.line	1
;----------------------------------------------------------------------
;  77 | void IntFromT0(void)    // no arguments no return value allowed        
;----------------------------------------------------------------------
        push      st
        push      fp
        ldiu      sp,fp
        push      r0
        pushf     f0
	.line	3
;----------------------------------------------------------------------
;  79 | DPPRec.MS100_TC = 1;                                                   
;----------------------------------------------------------------------
        ldiu      1,r0
        sti       r0,@_DPPRec+2
	.line	4
;----------------------------------------------------------------------
;  80 | DPPRec.PCIIRQ_Cnt ++;                                                  
;----------------------------------------------------------------------
        addi      @_DPPRec+4,r0         ; Unsigned
        sti       r0,@_DPPRec+4
	.line	5
        popf      f0
        pop       r0
        pop       fp
        pop       st
        reti
                                        	; Return From Interrupt
	.endfunc	81,000000001h,0


	.sect	 ".text"

	.global	_c_int03
	.sym	_c_int03,_c_int03,32,2,0
	.func	85
;******************************************************************************
;* FUNCTION NAME: _c_int03                                                    *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,ar0,st                                           *
;*   Regs Saved         : f0,r0,ar0,pc                                        *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 1 Auto + 3 SOE = 6 words          *
;******************************************************************************
_c_int03:
	.sym	_status,1,15,1,32
	.line	1
;----------------------------------------------------------------------
;  85 | void IntFromPCI(void)   // no arguments no return value allowed        
;  87 | ULONG status;                                                          
;  89 | //status = ReadDPRAM( dADR_IRQ_STAT );
;     |          // Read the Status Register                                   
;----------------------------------------------------------------------
        push      st
        push      fp
        ldiu      sp,fp
        addi      1,sp
        push      r0
        pushf     f0
        push      ar0
	.line	6
;----------------------------------------------------------------------
;  90 | status = *(ULONG *)( DPRAM0_BASE + dADR_IRQ_STAT );                    
;----------------------------------------------------------------------
        ldiu      @CL1,ar0
        ldiu      *ar0,r0
        sti       r0,*+fp(1)
	.line	7
;----------------------------------------------------------------------
;  91 | status &= 0xFFF0;
;     |                          // Clear the unwanted bits                    
;----------------------------------------------------------------------
        and       65520,r0
        sti       r0,*+fp(1)
	.line	8
;----------------------------------------------------------------------
;  92 | if( status == 0x3210 ){
;     |                  // Interrupt Acknowledge                              
;----------------------------------------------------------------------
        cmpi      12816,r0
        bned      L10
	nop
	nop
        ldine     @CL5,ar0
;*      Branch Occurs to L10 
	.line	9
;----------------------------------------------------------------------
;  93 | DPPRec.IRQ_RESET = 0;
;     |          // Reset the Internal Flag                                    
;  94 | // WriteGlue( iADR_GLUE_IRQ_DISABLE, 0x0 );
;     |  // Disable the PCI Interrupt                                          
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_DPPRec+1
	.line	11
;----------------------------------------------------------------------
;  95 | *(ULONG *)( GLUE_BASE + iADR_GLUE_IRQ_DISABLE ) = 0xFFFF;       // Disa
;     | ble the PCI INterrupt                                                  
;  97 | // WriteDPRAM( dADR_IRQ_STAT, 0 );
;     |          // Clear the Interrupt Status Register                        
;----------------------------------------------------------------------
        ldiu      @CL2,ar0
        ldiu      @CL3,r0
        sti       r0,*ar0
	.line	14
;----------------------------------------------------------------------
;  98 | *(ULONG *)( DPRAM0_BASE + dADR_IRQ_STAT ) = 0;                  // Clea
;     | r the Interrupt Status Register                                        
;----------------------------------------------------------------------
        ldiu      @CL1,ar0
        ldiu      0,r0
        sti       r0,*ar0
	.line	15
;----------------------------------------------------------------------
;  99 | *(ULONG *)( DPRAM0_BASE + dADR_OPCODE_LOOP_BACK ) = 0x5555;            
; 100 | } else {                                                               
; 101 | // DPPRec.OpCode = ReadDPRAM( dADR_OPCODE );                           
;----------------------------------------------------------------------
        bud       L11
        ldiu      @CL4,ar0
        ldiu      21845,r0
        sti       r0,*ar0
;*      Branch Occurs to L11 
	.line	18
;----------------------------------------------------------------------
; 102 | DPPRec.OpCode = (USHORT)(*(ULONG *)( DPRAM0_BASE + dADR_OPCODE ));
;     |          // Read the OpCode Register                                   
;----------------------------------------------------------------------
L10:        
        ldiu      *ar0,r0
        sti       r0,@_DPPRec+5
	.line	19
;----------------------------------------------------------------------
; 103 | DPPRec.OpCode &= 0xFFFF;
;     |                          // Mask off Upper Bits                        
;----------------------------------------------------------------------
        and       65535,r0
        sti       r0,@_DPPRec+5
	.line	20
;----------------------------------------------------------------------
; 104 | *(ULONG *)( DPRAM0_BASE + dADR_OPCODE_LOOP_BACK ) = 0xAAAA;            
;----------------------------------------------------------------------
        ldiu      @CL4,ar0
        ldiu      @CL6,r0
        sti       r0,*ar0
	.line	21
;----------------------------------------------------------------------
; 105 | *(ULONG *)( DPRAM0_BASE + dADR_OPCODE_LOOP_BACK2) = DPPRec.OpCode;
;     |                                                                        
;----------------------------------------------------------------------
        ldiu      @CL7,ar0
        ldiu      @_DPPRec+5,r0
        sti       r0,*ar0
L11:        
	.line	23
;----------------------------------------------------------------------
; 107 | }                                                       // Read the Int
;     | errupt Status}                                                         
;----------------------------------------------------------------------
        pop       ar0
        popf      f0
        pop       r0
        subi      1,sp
        pop       fp
        pop       st
        reti
                                        	; Return From Interrupt
	.endfunc	107,000000101h,1


	.sect	 ".text"

	.global	_trap
	.sym	_trap,_trap,32,2,0
	.func	111
;******************************************************************************
;* FUNCTION NAME: _trap                                                       *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs :                                                     *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 0 Auto + 0 SOE = 2 words          *
;******************************************************************************
_trap:
	.line	1
;----------------------------------------------------------------------
; 111 | void IntFromMr(void)    // no arguments no return value allowed        
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
	.line	3
        ldiu      *-fp(1),r1
        ldiu      *fp,fp
        subi      2,sp
        bu        r1
;*      Branch Occurs to r1 
	.endfunc	113,000000000h,0


	.sect	 ".text"

	.global	_c_int04
	.sym	_c_int04,_c_int04,32,2,0
	.func	117
;******************************************************************************
;* FUNCTION NAME: _c_int04                                                    *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs :                                                     *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 0 Auto + 0 SOE = 2 words          *
;******************************************************************************
_c_int04:
	.line	1
;----------------------------------------------------------------------
; 117 | void IntFromSB(void)    // no arguments no return value allowed        
;----------------------------------------------------------------------
        push      st
        push      fp
        ldiu      sp,fp
	.line	3
        pop       fp
        pop       st
        reti
                                        	; Return From Interrupt
	.endfunc	119,000000000h,0


	.sect	 ".text"

	.global	_c_int02
	.sym	_c_int02,_c_int02,32,2,0
	.func	122
;******************************************************************************
;* FUNCTION NAME: _c_int02                                                    *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs :                                                     *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 0 Auto + 0 SOE = 2 words          *
;******************************************************************************
_c_int02:
	.line	1
;----------------------------------------------------------------------
; 122 | void IntFromSEM(void)   // no arguments no return value allowed        
;----------------------------------------------------------------------
        push      st
        push      fp
        ldiu      sp,fp
	.line	3
        pop       fp
        pop       st
        reti
                                        	; Return From Interrupt
	.endfunc	124,000000000h,0


	.sect	 ".text"

	.global	_main
	.sym	_main,_main,36,2,0
	.func	129
;******************************************************************************
;* FUNCTION NAME: _main                                                       *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,fp,sp,st                                      *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 1 Auto + 0 SOE = 3 words          *
;******************************************************************************
_main:
	.sym	_data,1,15,1,32
	.line	1
;----------------------------------------------------------------------
; 129 | main( void )                                                           
; 131 | ULONG data;                                                            
; 133 | //----- Initialization Procedure   ------------------------------------
;     | -----------                                                            
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      1,sp
	.line	6
;----------------------------------------------------------------------
; 134 | DSP_Init();                                                     // Init
;     | ialize DSP Hardware                                                    
;----------------------------------------------------------------------
        call      _DSP_Init
                                        ; Call Occurs
	.line	7
;----------------------------------------------------------------------
; 135 | WriteEDS( iADR_INT_EN, 0x0 );                           // Disable the
;     | EDS Interrupt                                                          
;----------------------------------------------------------------------
        ldiu      0,r1
        ldiu      265,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	8
;----------------------------------------------------------------------
; 136 | WriteEDS( iADR_FIFO_MR, 0x0 );                  // Reset the EDS FIFO
;     |                                                                        
;----------------------------------------------------------------------
        ldiu      0,r0
        ldiu      298,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	9
;----------------------------------------------------------------------
; 137 | WriteEDS( iADR_CW, BIT_ENABLE );                        // Enable BIT i
;     | n the EDS FPGA ( Prevents data collection )                            
;----------------------------------------------------------------------
        ldiu      258,r1
        ldiu      1,r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	10
;----------------------------------------------------------------------
; 138 | WriteEDS( iADR_TIME_STOP, 0x0 );                        // Stop EDS Ana
;     | lysis                                                                  
;----------------------------------------------------------------------
        ldiu      0,r0
        ldiu      276,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	11
;----------------------------------------------------------------------
; 139 | WriteEDS( iADR_TIME_CLR, 0x0 );                 // Clear the Timers    
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        ldiu      277,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	12
;----------------------------------------------------------------------
; 140 | WriteDPRAM( dADR_DSP_VER, DSP_VER );            // This DSP Version is
;     | used to verify that the DSP has booted by the driver                   
; 141 | //----- Initialization Procedure   ------------------------------------
;     | -----------                                                            
;----------------------------------------------------------------------
        ldiu      16432,r0
        push      r0
        ldiu      2,r1
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	16
;----------------------------------------------------------------------
; 144 | DPP_BIT_Test();                                         // Perform BIT
;     | Tests                                                                  
; 145 | //---------------------------------------------------------------------
;     | --------------------------                                             
; 146 | //**** END OF BIT *****************************************************
;     | ***************************                                            
; 147 | //----- Initialization Procedure   ------------------------------------
;     | -----------                                                            
; 148 | // Clear the Contents of the DPRAM                                     
;----------------------------------------------------------------------
        call      _DPP_BIT_Test
                                        ; Call Occurs
	.line	22
;----------------------------------------------------------------------
; 150 | DPPRec.PCIIRQ_Cnt = 0;                                                 
; 152 | //----- Register Initialization for the TMS320C32                      
; 153 | // Clear and Enable Cache, disable OVM, disable interrupts, edge trigge
;     | red interrupts                                                         
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_DPPRec+4
	.line	26
;----------------------------------------------------------------------
; 154 | WriteEDS( iADR_FIFO_MR, 0x0 );                                         
;----------------------------------------------------------------------
        ldiu      0,r1
        push      r1
        ldiu      298,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	27
;----------------------------------------------------------------------
; 155 | WriteGlue( iADR_GLUE_FF_MR, 0x0 );
;     |                  // Reset the PCI FIFO                                 
; 157 | //----- Start of Initialization ---------------------------------------
;     | -------------                                                          
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        ldiu      5,r1
        push      r1
        call      _WriteGlue
                                        ; Call Occurs
        subi      2,sp
	.line	31
;----------------------------------------------------------------------
; 159 | EDS_Init();
;     |                          // Initialize EDS Hardware                    
;----------------------------------------------------------------------
        call      _EDS_Init
                                        ; Call Occurs
	.line	33
;----------------------------------------------------------------------
; 161 | data = ReadEDS( iADR_CW );
;     |                  // Read the Control Word                              
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	34
;----------------------------------------------------------------------
; 162 | data |= ( 1 << 9 );                                                    
;----------------------------------------------------------------------
        ldiu      512,r0
        or        *+fp(1),r0
        sti       r0,*+fp(1)
	.line	35
;----------------------------------------------------------------------
; 163 | WriteEDS( iADR_CW, (USHORT)data );                                     
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      258,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	36
;----------------------------------------------------------------------
; 164 | WriteEDS( iADR_FIR_MR, 0xFFFF );                                       
;----------------------------------------------------------------------
        ldiu      @CL8,r0
        ldiu      354,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	38
;----------------------------------------------------------------------
; 166 | data = ReadEDS( iADR_TP_SEL );
;     |          // Set DIgital TP Select to debug Live Spectrum MApping       
;----------------------------------------------------------------------
        ldiu      264,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	39
;----------------------------------------------------------------------
; 167 | data &= 0x0FFF;                                                        
;----------------------------------------------------------------------
        ldiu      4095,r0
        and       *+fp(1),r0
        sti       r0,*+fp(1)
	.line	40
;----------------------------------------------------------------------
; 168 | data |= 0xD000;                                                        
;----------------------------------------------------------------------
        or        53248,r0
        sti       r0,*+fp(1)
	.line	41
;----------------------------------------------------------------------
; 169 | WriteEDS( iADR_TP_SEL, (USHORT)data );                                 
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      264,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	43
;----------------------------------------------------------------------
; 171 | WriteDPRAM( dADR_SCA_GEN, 0x0 );                                       
;----------------------------------------------------------------------
        ldiu      0,r1
        ldiu      33,r0
        push      r1
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	44
;----------------------------------------------------------------------
; 172 | WriteDPRAM( dADR_DAC_GEN, 0x0 );                                       
;----------------------------------------------------------------------
        ldiu      0,r1
        push      r1
        ldiu      34,r0
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	45
;----------------------------------------------------------------------
; 173 | WriteDPRAM( dADR_DSP_VER, (USHORT)DSP_VER );                           
;----------------------------------------------------------------------
        ldiu      16432,r0
        ldiu      2,r1
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	46
;----------------------------------------------------------------------
; 174 | WriteEDS( iADR_RESET_MR, 0xFFFF );                                     
; 175 | // Final hand-shaking for the Driver before final boot                 
;----------------------------------------------------------------------
        ldiu      @CL8,r0
        push      r0
        ldiu      382,r1
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	48
;----------------------------------------------------------------------
; 176 | WriteEDS( iADR_DSO_TRIG, 0x6 );                                        
;----------------------------------------------------------------------
        ldiu      373,r1
        ldiu      6,r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	49
;----------------------------------------------------------------------
; 177 | WriteEDS( iADR_DSO_INT, 0x320 );                                       
;----------------------------------------------------------------------
        ldiu      800,r0
        ldiu      374,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	50
;----------------------------------------------------------------------
; 178 | EDSRec.FFT_Mode = 0;                                                   
; 181 | //----- Main Loop for the DSP Code ------------------------------------
;     | -----------                                                            
; 182 | while( 1 )                                                             
; 184 |         //----- Process a PCI Interrupt -------------------------------
;     | ----------------------------                                           
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_EDSRec+13
L25:        
	.line	57
;----------------------------------------------------------------------
; 185 | if( DPPRec.OpCode != 0 ){                                              
;----------------------------------------------------------------------
L26:        
        ldi       @_DPPRec+5,r0
        beq       L32
;*      Branch Occurs to L32 
	.line	58
;----------------------------------------------------------------------
; 186 | data = DPPRec.OpCode;
;     |                  // Save OpCode                                        
;----------------------------------------------------------------------
        sti       r0,*+fp(1)
	.line	59
;----------------------------------------------------------------------
; 187 | WriteEDS( iADR_RTEM_CMD_WD_MR, 0xFFFF );
;     |          // Reset Watch-Dog with every op-code                         
;----------------------------------------------------------------------
        ldiu      371,r1
        ldiu      @CL8,r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	61
;----------------------------------------------------------------------
; 189 | if(( data & 0x8000 ) == 0 ){
;     |  // Write OpCode                                                       
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
        tstb      32768,r0
        bned      L30
	nop
	nop
        ldine     *+fp(1),r0
;*      Branch Occurs to L30 
	.line	62
;----------------------------------------------------------------------
; 190 | Proc_Write_OpCode( (USHORT)data );                                     
; 191 | } else {
;     |                                  // Read OpCode                        
;----------------------------------------------------------------------
        push      r0
        call      _Proc_Write_OpCode
                                        ; Call Occurs
        bud       L31
        subi      1,sp
        ldiu      *+fp(1),r1
        ldiu      1,r0
;*      Branch Occurs to L31 
	.line	64
;----------------------------------------------------------------------
; 192 | Proc_Read_OpCode( (USHORT)data );                                      
;----------------------------------------------------------------------
L30:        
        push      r0
        call      _Proc_Read_OpCode
                                        ; Call Occurs
        subi      1,sp
	.line	66
;----------------------------------------------------------------------
; 194 | WriteDPRAM( dADR_OPCODE_LOOP_BACK, (USHORT)data );
;     |                  // Write Processed OpCode to Loop-Back Register       
;     |                                                                        
;----------------------------------------------------------------------
        ldiu      *+fp(1),r1
        ldiu      1,r0
L31:        
        push      r1
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	67
;----------------------------------------------------------------------
; 195 | DPPRec.OpCode = 0;
;     |                          // Clear the OpCode                           
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_DPPRec+5
	.line	68
;----------------------------------------------------------------------
; 196 | WriteDPRAM( dADR_OPCODE, DPPRec.OpCode );
;     |          // Write it back to DPRAM                                     
; 198 | //----- Process a PCI Interrupt ---------------------------------------
;     | --------------------                                                   
; 200 | //----- Process an EDX Interrupt --------------------------------------
;     | ----------------                                                       
;----------------------------------------------------------------------
        ldiu      0,r1
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
L32:        
	.line	73
;----------------------------------------------------------------------
; 201 | if( EDSRec.EDS_IRQ != 0 ){                                             
;----------------------------------------------------------------------
        ldi       @_EDSRec+5,r0
        beq       L35
;*      Branch Occurs to L35 
	.line	74
;----------------------------------------------------------------------
; 202 | Proc_Eds_Isr();                                                        
;----------------------------------------------------------------------
        call      _Proc_Eds_Isr
                                        ; Call Occurs
	.line	75
;----------------------------------------------------------------------
; 203 | if(( ReadEDS( iADR_STAT ) & 0x1 ) == 0 ){                       // Read
;     |  the Status of the EDS FPGA and if FIFO is not empty                   
;----------------------------------------------------------------------
        ldiu      257,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        tstb      1,r0
        bned      L35
        subi      1,sp
	nop
        ldieq     1,r0
;*      Branch Occurs to L35 
	.line	76
;----------------------------------------------------------------------
; 204 | EDSRec.EDS_IRQ = 1;                                                    
; 207 | //----- Process an EDX Interrupt --------------------------------------
;     | ----------------                                                       
;----------------------------------------------------------------------
        sti       r0,@_EDSRec+5
L35:        
	.line	81
;----------------------------------------------------------------------
; 209 | if( DPPRec.MS100_TC != 0 ){
;     |          // 100ms Timer                                                
;----------------------------------------------------------------------
        ldi       @_DPPRec+2,r0
        beqd      L38
	nop
	nop
        ldine     1,r0
;*      Branch Occurs to L38 
	.line	82
;----------------------------------------------------------------------
; 210 | if( ++EDSRec.Reset_FIR > 1 ){                                          
; 211 | //      WriteEDS( iADR_FIR_MR, 0xFFFF );                               
;----------------------------------------------------------------------
        addi      @_EDSRec+33,r0        ; Unsigned
        cmpi      1,r0
        blsd      L38
        sti       r0,@_EDSRec+33
	nop
        ldihi     0,r0
;*      Branch Occurs to L38 
	.line	84
;----------------------------------------------------------------------
; 212 | EDSRec.Reset_FIR = 0;                                                  
;----------------------------------------------------------------------
        sti       r0,@_EDSRec+33
L38:        
	.line	88
;----------------------------------------------------------------------
; 216 | if( EDSRec.DSO_Enable != 0 ){                                          
;----------------------------------------------------------------------
        ldi       @_EDSRec+14,r0
        beq       L42
;*      Branch Occurs to L42 
	.line	89
;----------------------------------------------------------------------
; 217 | if(( EDSRec.EDS_IRQ == 0 ) && (( ReadEDS( iADR_STAT ) & 0x1 ) == 0 )){
;     |                  // Read the Status of the EDS FPGA and if FIFO is not 
;     | empty                                                                  
;----------------------------------------------------------------------
        ldi       @_EDSRec+5,r0
        bned      L42
	nop
	nop
        ldieq     257,r0
;*      Branch Occurs to L42 
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        tstb      1,r0
        bned      L42
        subi      1,sp
	nop
        ldieq     1,r0
;*      Branch Occurs to L42 
	.line	90
;----------------------------------------------------------------------
; 218 | EDSRec.EDS_IRQ = 1;                                                    
;----------------------------------------------------------------------
        sti       r0,@_EDSRec+5
L42:        
	.line	95
;----------------------------------------------------------------------
; 223 | if( DPPRec.IRQ_RESET == 0 ){                                           
; 224 |         //----- Process DSO Done Interrupt ----------------------------
;     | ------------------------                                               
;----------------------------------------------------------------------
        ldi       @_DPPRec+1,r0
        bne       L65
;*      Branch Occurs to L65 
	.line	97
;----------------------------------------------------------------------
; 225 | if( EDSRec.DSO_PENDING != 0 ){                                  // DSO
;     | Mode Pending Data                                                      
;----------------------------------------------------------------------
        ldi       @_EDSRec+3,r0
        beqd      L45
	nop
	nop
        ldieq     @_EDSRec+9,r0
;*      Branch Occurs to L45 
	.line	98
;----------------------------------------------------------------------
; 226 | DPPRec.Set_PCI_IRQ = 1;
;     |          // Set_PCI_IRQ();                                             
;----------------------------------------------------------------------
        ldiu      1,r0
        sti       r0,@_DPPRec+6
	.line	99
;----------------------------------------------------------------------
; 227 | EDSRec.DSO_PENDING = 0;
;     |          // Clear Pending flag                                         
; 229 | //----- Process DSO Done Interrupt ------------------------------------
;     | ----------------                                                       
; 232 | //----- LIVE SPECTRUM MAPPING MODE ( REDUCED FUNCTION ) ---------------
;     | ----------------                                                       
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_EDSRec+3
	.line	105
;----------------------------------------------------------------------
; 233 | if( EDSRec.Preset_Mode >= FPGA_PRESET_LSMAP_REAL ){                    
; 235 |         // if WDS Mapping, force PRESET_DONE_CNT to be PRESET_DONE_POIN
;     | TS to trigger TMR_ISR interrupt                                        
;----------------------------------------------------------------------
        ldiu      @_EDSRec+9,r0
L45:        
        cmpi      7,r0
        blo       L57
;*      Branch Occurs to L57 
	.line	108
;----------------------------------------------------------------------
; 236 | if( EDSRec.Preset_Mode >= FPGA_PRESET_WDS_REAL ){                      
;----------------------------------------------------------------------
        cmpi      9,r0
        blod      L48
	nop
	nop
        ldihs     @_EDSRec+11,r0
;*      Branch Occurs to L48 
	.line	109
;----------------------------------------------------------------------
; 237 | EDSRec.Preset_Done_Cnt = EDSRec.Preset_Done_Points;                    
; 240 | //                              EDSRec.Preset_Done_Cnt = ReadEDS( iADR_
;     | PRESET_DONE_CNT );                       // Read Preset Done Counts fro
;     | m FPGA                                                                 
; 241 | //----- Process 100ms Timer Interrupt ---------------------------------
;     | ----------------                                                       
;----------------------------------------------------------------------
        sti       r0,@_EDSRec+10
L48:        
	.line	114
;----------------------------------------------------------------------
; 242 | if(    ( DPPRec.MS100_TC != 0 )                                        
; 243 |         || ( EDSRec.FPGA_Preset_Done != 0 )) {                         
; 244 | //                                      || (( EDSRec.FPGA_Preset_Done !
;     | = 0 ) && ( EDSRec.Preset_Done_Points == EDSRec.Preset_Done_Cnt )) ){   
;----------------------------------------------------------------------
        ldi       @_DPPRec+2,r0
        bned      L51
	nop
	nop
        ldine     257,r0
;*      Branch Occurs to L51 
        ldi       @_EDSRec+0,r0
        beqd      L53
	nop
	nop
        ldine     257,r0
;*      Branch Occurs to L53 
	.line	117
;----------------------------------------------------------------------
; 245 | if(( ReadEDS( iADR_STAT ) & 0x8 ) == 0 ){                              
;----------------------------------------------------------------------
L51:        
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        tstb      8,r0
        bned      L53
	nop
	nop
        subi      1,sp
;*      Branch Occurs to L53 
	.line	118
;----------------------------------------------------------------------
; 246 | Proc_Tmr_Isr();                                                        
;----------------------------------------------------------------------
        call      _Proc_Tmr_Isr
                                        ; Call Occurs
	.line	119
;----------------------------------------------------------------------
; 247 | DPPRec.Set_PCI_IRQ = 1;                                                
;----------------------------------------------------------------------
        ldiu      1,r0
        sti       r0,@_DPPRec+6
L53:        
	.line	123
;----------------------------------------------------------------------
; 251 | if( DPPRec.MS100_TC != 0 )                                             
;----------------------------------------------------------------------
        ldi       @_DPPRec+2,r0
        beqd      L55
	nop
	nop
        ldine     0,r0
;*      Branch Occurs to L55 
	.line	124
;----------------------------------------------------------------------
; 252 | DPPRec.MS100_TC = 0;                                                   
;----------------------------------------------------------------------
        sti       r0,@_DPPRec+2
L55:        
	.line	126
;----------------------------------------------------------------------
; 254 | if( EDSRec.FPGA_Preset_Done != 0 )                                     
;----------------------------------------------------------------------
        ldi       @_EDSRec+0,r0
        beqd      L65
	nop
	nop
        ldine     0,r0
;*      Branch Occurs to L65 
	.line	127
;----------------------------------------------------------------------
; 255 | EDSRec.FPGA_Preset_Done = 0;                                           
; 256 | //----- Process 100ms Timer Interrupt ---------------------------------
;     | ----------------                                                       
; 259 | //----- LIVE SPECTRUM MAPPING MODE ( REDUCED FUNCTION ) ---------------
;     | ----------------                                                       
; 261 | //----- Normal Mode ---------------------------------------------------
;     | ----------------                                                       
; 262 | else {
;     |                          // All Other Modes                            
; 263 | //----- Process 100ms Timer Interrupt ---------------------------------
;     | ----------------                                                       
;----------------------------------------------------------------------
        bud       L66
        sti       r0,@_EDSRec+0
	nop
	nop
;*      Branch Occurs to L66 
L57:        
	.line	136
;----------------------------------------------------------------------
; 264 | if( DPPRec.MS100_TC != 0 ){
;     |          // 100ms Timer                                                
; 265 | //                                      if( EDSRec.RTEM_Init != 0 ) rte
;     | m_init();                                                              
;----------------------------------------------------------------------
        ldi       @_DPPRec+2,r0
        beq       L59
;*      Branch Occurs to L59 
	.line	138
;----------------------------------------------------------------------
; 266 | if( EDSRec.ADisc != 0 ) Auto_Disc();                                   
; 268 | //----- Process 100ms Timer Interrupt ---------------------------------
;     | ----------------                                                       
; 270 | //----- Process EDX Anal Done Interrupt -------------------------------
;     | ----------------                                                       
;----------------------------------------------------------------------
        ldi       @_EDSRec+25,r0
        callne    _Auto_Disc
                                        ; Call Occurs
L59:        
	.line	143
;----------------------------------------------------------------------
; 271 | if(( EDSRec.FPGA_Preset_Done != 0 ) || ( DPPRec.MS100_TC != 0 )){
;     |                                  // EDS Preset Done                    
;     |                                                                        
;----------------------------------------------------------------------
        ldi       @_EDSRec+0,r0
        bne       L61
;*      Branch Occurs to L61 
        ldi       @_DPPRec+2,r0
        beqd      L62
	nop
	nop
        ldieq     33,r0
;*      Branch Occurs to L62 
L61:        
	.line	144
;----------------------------------------------------------------------
; 272 | Proc_Tmr_Isr();                                                        
;----------------------------------------------------------------------
        call      _Proc_Tmr_Isr
                                        ; Call Occurs
	.line	145
;----------------------------------------------------------------------
; 273 | DPPRec.Set_PCI_IRQ = 1;                                         // Set_
;     | PCI_IRQ();                                                             
;----------------------------------------------------------------------
        ldiu      1,r0
        sti       r0,@_DPPRec+6
	.line	146
;----------------------------------------------------------------------
; 274 | EDSRec.FPGA_Preset_Done = 0;                                    // Clea
;     | r PReset Done Flag                                                     
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_EDSRec+0
	.line	147
;----------------------------------------------------------------------
; 275 | DPPRec.MS100_TC = 0;                                                   
; 277 | //----- Process EDX Anal Done Interrupt -------------------------------
;     | ----------------                                                       
;----------------------------------------------------------------------
        sti       r0,@_DPPRec+2
	.line	152
;----------------------------------------------------------------------
; 280 | if( ReadDPRAM( dADR_SCA_GEN ) != 0 ){                                  
;----------------------------------------------------------------------
        ldiu      33,r0
L62:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        cmpi      0,r0
        beqd      L64
        subi      1,sp
	nop
        ldieq     34,r0
;*      Branch Occurs to L64 
	.line	153
;----------------------------------------------------------------------
; 281 | WriteEDS( iADR_SCA, ReadDPRAM( dADR_SCA_GEN ) );                       
;----------------------------------------------------------------------
        ldiu      33,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        ldiu      290,r1
        subi      1,sp
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	156
;----------------------------------------------------------------------
; 284 | if( ReadDPRAM( dADR_DAC_GEN ) != 0 ){                                  
;----------------------------------------------------------------------
        ldiu      34,r0
L64:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        cmpi      0,r0
        subi      1,sp
	.line	157
;----------------------------------------------------------------------
; 285 | BIT_DAC_GEN();                                                         
; 287 | }
;     |                          // Live Spectrum Mapping Mode                 
; 288 | //----- Normal Mode ---------------------------------------------------
;     | ----------------                                                       
; 289 | }
;     |                                  // if IRQ_RESET == 0                  
; 290 | //---------------------------------------------------------------------
;     | ---------------------                                                  
; 293 | //---------------------------------------------------------------------
;     | ---------------------                                                  
;----------------------------------------------------------------------
        callne    _BIT_DAC_GEN
                                        ; Call Occurs
L65:        
	.line	166
;----------------------------------------------------------------------
; 294 | if(( DPPRec.IRQ_RESET == 0 ) && ( DPPRec.Set_PCI_IRQ == 1 )){          
; 295 |         //      Set_PCI_IRQ();                                         
;----------------------------------------------------------------------
L66:        
        ldi       @_DPPRec+1,r0
        bned      L25
	nop
	nop
        ldieq     @_DPPRec+6,r0
;*      Branch Occurs to L25 
        cmpi      1,r0
        bned      L25
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L25 
	.line	168
;----------------------------------------------------------------------
; 296 | DPPRec.IRQ_RESET = 1;                                                  
;----------------------------------------------------------------------
        sti       r0,@_DPPRec+1
	.line	169
;----------------------------------------------------------------------
; 297 | WriteGlue( iADR_GLUE_IRQ_ENABLE, 0xF );
;     |          // Acknowledge PCI and Interrupt                              
;----------------------------------------------------------------------
        ldiu      15,r1
        push      r1
        push      r0
        call      _WriteGlue
                                        ; Call Occurs
        subi      2,sp
	.line	170
;----------------------------------------------------------------------
; 298 | WriteDPRAM( dADR_IRQ_STAT, 0x8000 );
;     |          // Read the Status Register                                   
;----------------------------------------------------------------------
        ldiu      @CL9,r0
        ldiu      3,r1
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	171
;----------------------------------------------------------------------
; 299 | DPPRec.PCIIRQ_Cnt = 0;                                                 
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,@_DPPRec+4
	.line	172
;----------------------------------------------------------------------
; 300 | DPPRec.Set_PCI_IRQ = 0;                                                
;----------------------------------------------------------------------
	.line	175
        bud       L26
        sti       r0,@_DPPRec+6
	nop
	nop
;*      Branch Occurs to L26 
	.line	176
;----------------------------------------------------------------------
; 304 | return(0);                                                             
;----------------------------------------------------------------------
	.line	177
;----------------------------------------------------------------------
; 305 | }                                       // Main                        
;----------------------------------------------------------------------
	.endfunc	305,000000000h,1


;******************************************************************************
;* CONSTANT TABLE                                                             *
;******************************************************************************
	.sect	".const"
	.bss	CL1,1
	.bss	CL2,1
	.bss	CL3,1
	.bss	CL4,1
	.bss	CL5,1
	.bss	CL6,1
	.bss	CL7,1
	.bss	CL8,1
	.bss	CL9,1

	.sect	".cinit"
	.field  	9,32
	.field  	CL1+0,32
	.field  	9437187,32
	.field  	9453570,32
	.field  	65535,32
	.field  	9437185,32
	.field  	9437184,32
	.field  	43690,32
	.field  	9437222,32
	.field  	65535,32
	.field  	32768,32

	.sect	".text"
;******************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                              *
;******************************************************************************

	.global	_EDS_Init

	.global	_WriteEDS

	.global	_ReadEDS

	.global	_Auto_Disc

	.global	_DSP_Init

	.global	_WriteDPRAM

	.global	_ReadDPRAM

	.global	_WriteGlue

	.global	_Proc_Write_OpCode

	.global	_Proc_Read_OpCode

	.global	_DPP_BIT_Test

	.global	_BIT_DAC_GEN

	.global	_EDSRec

	.global	_DPPRec
	.global	_Proc_Eds_Isr
	.global	_Proc_Tmr_Isr
