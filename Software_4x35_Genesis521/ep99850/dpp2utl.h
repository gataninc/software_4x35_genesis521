/*********************************************************************************************************
--																					
--	Module:	EDI2UTL.H																	
--																					
--	EDAX Inc																			
--																					
--  Author:		Michael Solazzi														
--  Created:    3/25/99 																	
--  Board:      DPP-II																
--																					
--	Description:																		
--		This Header file contains all of the definitions and general purpose						
--		routines to be used in the EDI-II Board.											
--																					
--  History:				
--		04/24/07 - MCS - Ver 4x30
--			Apollo 10 Disable 2X Gain Switch
--		04/23/07 - MCS - Ver 4x2F
--			Apollo 10 Enable 2X Gain Switch ( again )
--		04/03/07 - MCS - Ver 4x2C
--			Apollo 10 Enable 2x gain switch
--		03/21/07 - MCS - Ver 4x2B
--			Disc is inverted ( confirmend with scope on U10-1 on DPP2 FR board ).
--
--		03/14/07 - MCS - Ver 4x2A
--			New Detector type DET_204NFR with a code of 5. This type will not throw the x2 gain switch
--			type DET_204 was renamed to DET_204FR
--			
--		03/02/07 - MCS - Ver 4x29
--			PA_MODE(3) had to be inverted for Det type DET_204
--			Disc_Inv(0) is to not be inverted from AD_Invert
--		02/22/07 - MCS - Ver 4x28
--			New Register DET_TYPE at address 0x63 in the FPGA used to contain more detector types	
-- 				DET_204			= 0 ( Existing )
-- 				DET_UNICORN		= 1 ( Existing )
-- 				DET_AP40 			= 2 ( Existing )
-- 				DET_WDS( LEX or TEX ) 	= 3 ( Existing )
-- 				DET_AP10			= 4 � New Decode
--			Added "AD_INVERT" to be set when the A/D input is to be inverted, 
--			which will invert to module AD9240 as well as set the appropraite bits
--			for the SHDisc Inversion
-- 		08/23/06 - MCS - Ver 4x27
--			Added a RESET_GEN_CNT_MAX to limit the pulse width for the reset set to 200us
--
--		08/22/06 - MCS - Ver 4x26: Module AD9240.VHD											
--			Max/Min Adjust threshold changed to original method due to reset being stuck active with 	
--			a ramp that is too small														
--			seen by increased gain from VItus SDD											
--																					
--		08/11/06 - MCS - Ver 4x25: Module AD9240.VHD											
--			Added the circuit to force a reset generate if the ramp is saturated,					
--			ie no reset for some time..													
--																					
--		***** 08/10/06 - Ver 4x24 ***** GENESIS 5.1 Release Candidate *********************************
--		08/10/06 - MCS Ver 4x24 - Genesis 5.1 Release Candidate								
--			ADDED OTR_EN which is disabled (0) when Preset Mode is Live Spectrum Mapping or WDS Modes	
--			otherwise it is enabled ( Per Laszlo's request )									
--																					
--		08/09/06 - MCS Ver 4x23															
--			Edsutil.C : SetROISCARM( int ROI_NUM ) changed the address to the SCA Look-up memory 	
--			to be just the index not 1/2 the index											
--	     07/05/06 - MCS Ver 4x22															
--			EDS_ISR : modified for:														
--			LSM: Preset_DOne count is read by DSP, from FPGA and the count is incremented			
--			     by 1 before writting it to the record, to "Pixel Tag" the LSM Data				
--				Channel data is tagged with the Pixel number for LSM operation					
--			DSO mode seperated to its own loop for more "Streamlined" operation					
--				Each time trough the loop, the DSO_PENDING flag is set so it interrupts			
--				the driver to offload the data (8x during a DSO operation						
--		08/03/05 - MCS  Ver 356F																
--				No changes to the DSP COde, only to the FPGA regarding Detector Active Reset		
--		00/02/03 - MCS - Ver 0xB32														
--					Added the Read of the EDS Status and update the EDS Record					
--					EDSRec.EDS_Stat with every 100ms Timer Interrupt		    					
--		99/10/05 - MCS - Cleanup															
--		99/08/16 - MCS - Updated for EDI-II Rev B											
*********************************************************************************************************/

#ifndef _DPP2UTL_H 
#define _DPP2UTL_H									

	#define DSP_VER				0x4030
	#define NO_SCA					FALSE		// if Defined Forces EDS FPGA Memory Tests to No Erros 
	#define NO_FIFO_TEST			FALSE
														
	#define MCA_BASE			(volatile ULONG *) 0x884000
	#define BASELINE_BASE		(volatile ULONG *) 0x885000
//	#define EDS_RM_LU_BASE		(volatile ULONG *) 0x886000
//	#define OTHER_BASE			(volatile ULONG *) 0x886040
	#define DSO_BASE			(volatile ULONG *) 0x884000	//
	#define SRAM_BASE			(volatile ULONG *) 0x880000	//
	#define DPRAM0_BASE  		(volatile ULONG *) 0x900000	// Dual Port Memory
	#define DPRAM1_BASE  		(volatile ULONG *) 0x902000	// Dual Port Memory - FPGA Related
	#define GLUE_BASE			(volatile ULONG *) 0x904000
	#define PCI_FIFO_BASE		(volatile ULONG *) 0x904000 	// 00 0010
	#define EDS_BASE			(volatile ULONG *) 0x906000	// 00 0100	New for EDI-III
	#define EDS_FIFO_STAT  		(volatile ULONG *) 0x908000 	// 00 0110	New for EDI-III
	#define EDS_FIFO_BASE  		(volatile ULONG *) 0x908001 	// 00 0110	New for EDI-III

	#define iADR_GLUE_STATUS			0x0
	#define iADR_GLUE_IRQ_ENABLE		0x01
	#define iADR_GLUE_IRQ_DISABLE		0x02
	#define iADR_GLUE_ID			0x03
	#define iADR_GLUE_TREG			0x04
	#define iADR_GLUE_FF_MR			0x05
	#define iADR_GLUE_FF_CNT			0x06

	#define PCI_DEBUG				1
	#define SB0    	(ULONG)		0x8001

	#define PCIIRQ_CNT_MAX			50		// 50 * 100ms = 5 Seconds 

	#define PFF_FF 				0xC		// Active Low  0xC for Active High		
	#define PFF_EF 				0x3		// Active Low  0x3 for Active High


	#define DSP_ACK_IRQ				0x01
	#define DSP_EDS_IRQ				0x02		// Not Used
	#define DSP_TMR_IRQ				0x08
	#define DSP_FF_IRQ				0x10		// Not Used
	#define DSP_EDS_DSO				0x20		// Not Used

	#define TIMER0_FREQ		(float)20.0	// Was 200	// Timer 0 Frequency - 5 Hz EDI2 to PCI Interrupt
	#define TIMER1_FREQ		(float) 2.0				// Timer 1 Frequency - 2 Hz
	#define DSP_CLK			(float) 30e6			// DSP Clock Frequency

	// OpCode DPRAM Offsets ( All of Them!)
	#define dADR_OPCODE				0x00			
	#define dADR_OPCODE_LOOP_BACK		0x01
	#define dADR_DSP_VER			0x02			// DSP Version Number EDXGETBUILD
	#define dADR_IRQ_STAT			0x03
	#define dADR_EDS_STAT			0x04			// 1 = Running, 0 = Idle
	#define dADR_SBUS_ADDR			0x05			// Serial Bus Address
	#define dADR_SBUS_DATA			0x06			// Serial Bus Data
	#define dADR_PCI_FIFO_LO			0x07			// Write to PCI FIFO Low Bits
	#define dADR_PCI_FIFO_HI			0x08			// Write to PCI FIFO High Bits
	#define dADR_TIMECONST			0x09			// Time Constant

//	#define dADR_ADAPT_MODE			0x0A
//	#define dADR_SET_CAL			0x0B			// Sets EDS Paraameters
//	#define dADR_GAIN				0x0C			// Digital Fine Gain ( Floating Point )
//	#define dADR_ZERO				0x0D			// Digital Fine Zero ( Floating Point )
//	#define dADR_GAIN_CONSTANT		0x0E			// EDS Gain  Constant
//	#define dADR_ZERO_CONSTANT		0x0F			// EDS Zero Constant
//	#define dADR_MCA_CLR			0x10			// Clear Spectrum, Max Peak, ROIs etc

	#define dADR_ANAL_MODE			0x11			// Analysis Mode Reads the following
	#define dADR_TRANSFER			0x12			// 0 = Disabled, 1 = Enabled
	#define dADR_ROI_DEF			0x13			// Define a ROI
	#define dADR_ROI_PSET			0x14			// PSet a ROI
	#define dADR_ROI_CLR			0x15			// Clear a ROI
	#define dADR_ROI_START			0x16			// ROI Starting Channel
	#define dADR_ROI_END			0x17			// ROI Ending Channel
	#define dADR_ROI_ENABLE			0x18			// ROI Enable/Disable
	#define dADR_ROI_NUM			0x19			// ROI Nuimber
	#define dADR_SCA_DEF			0x1A			// SCA Def
	#define dADR_SCA_MASK			0x1B			// SCA Mask
	#define dADR_RTEM_INIT			0x1C			// RTEM Initalize Routine
	#define dADR_AUTO_DISC			0x1D
	#define dADR_MAX_CPS			0x1E
	#define dADR_EVCH				0x1F
	#define dADR_BLANK_FACTOR		0x20
	#define dADR_SCA_GEN			0x21
	#define dADR_DAC_GEN			0x22
//	#define dADR_SPC_CLR			0x23
	#define dADR_PRESET_DONE_CNT	0x24
	#define dADR_PRESET_POINTS		0x25
	#define dADR_OPCODE_LOOP_BACK2	0x26
	#define dADR_NON_LINEAR_CODE	0x27
	#define dADR_EDS_FIFO_DATA_TEST	0x30
	#define dADR_EDS_FIFO_FLAG_TEST	0x32

	// Define Constants for PCI FIFO
	#define FF_KEY_RADC		(ULONG) 	0x00 << 28
	#define FF_KEY_SPECTRUM (ULONG) 	0x01 << 28
	#define FF_KEY_NOISE	(ULONG) 	0x02 << 28
	#define FF_KEY_XPORT	(ULONG) 	0x03 << 28
	#define FF_KEY_BPOS		(ULONG) 	0x04 << 28

	// Define a structure
	typedef struct {				// Offeset for ISRs
		UCHAR	PCI_IRQ;			//	0			// Interrupt from PCI
		USHORT	IRQ_RESET;			//	2
		USHORT	MS100_TC;			//	3
		USHORT	OpCodeAck;			//  4
		ULONG   PCIIRQ_Cnt;			//  5
		USHORT	OpCode;				// 	6
		USHORT	Set_PCI_IRQ;		//  7
	} DPP_TYPE;


	//------ DSP Initialization Routine --------------------------------------------------------
	void		DSP_Init( void );
	void		WriteDPRAM( USHORT addr, USHORT data );
	USHORT		ReadDPRAM( USHORT addr );
	void		WriteGlue( USHORT addr, USHORT data );
	USHORT		ReadGlue( USHORT addr );
	void		WritePCIFifo( ULONG data );
	void		Set_DPRAM_Bit( USHORT addr, USHORT val );
	void		wait( int val );
	USHORT		GetPCIFIFOStatus( void );
	void		Proc_Write_OpCode( USHORT opcode );
	void		Proc_Read_OpCode( USHORT opcode );
#endif
/********************************************************************************/
/* END OF EDI2UTIL.H														    */
/********************************************************************************/
