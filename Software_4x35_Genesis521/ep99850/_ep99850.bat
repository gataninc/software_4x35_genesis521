@echo off
del *.obj
del *.out
del *.hex
rem -------------------------------------------------------
rem File: _ep99850.bat
rem -------------------------------------------------------
rem call a_clup.bat
cls
rem echo %COptions%
rem pause
rem cl30 %COptions% edsutl.c

rem Compiler Directives
rem -g Enable Symbolic Debugging
rem -s interlist optimizer comments and assembly source statments
rem -ss interlist C and assembly source statements
rem -mc	uses faster INT to FLOAT conversions
rem -mn reenable optimizer options disabled by -g
rem -mp	cause speed improvememt at the expense of increasing code size

set logfile=ep99850.log
set AOptions=-v32 -s
set COptions=-v32 -g -s -ss -mc -mn -mp 

echo ----- Assemble DSP Code -----------------------------------
REM
REM the -S opetion for the ASM30 command puts all symbols
REM in the symbol table for debugging purposes
REM
echo       ----- DIF_F30 --------------------------------------- 
echo "ASM div_f30" > %logfile%
asm30 %AOptions% div_f30.asm >> %logfile%

echo       ----- COS ------------------------------------------- 
echo "ASM COS" > %logfile%
asm30 %AOptions% COS.asm >> %logfile%

echo       ----- SIN ------------------------------------------- 
echo "ASM SIN" > %logfile%
asm30 %AOptions% SIN.ASM >> %logfile%

echo       ----- EDI2VECTORS ----------------------------------- 
echo "edi2vectors" >>  %logfile%
asm30 %AOptions% edi3vectors.asm >> %logfile%

echo       ----- EDS_ISR --------------------------------------- 
echo "eds_isr" >>  %logfile%
asm30 %AOptions% eds_isr.asm >> %logfile%

echo       ----- TMR_ISR --------------------------------------- 
echo "tmr_isr" >>  %logfile%
asm30 %AOptions% tmr_isr.asm  >> %logfile%

echo ----- Compile DSP Code ------------------------------------
echo      ----- EDSUTL.C
cl30 %COptions% edsutl.c >> %logfile%
echo      ----- DPP2UTL.C
cl30 %COptions% dpp2utl.c >> %logfile%
echo      ----- DPP2BIT.C
cl30 %COptions% dpp2bit.c >> %logfile%
echo      ----- EP99850.C
cl30 %COptions% ep99850.c >> %logfile%

echo ----- link dsp code ---------------------------------------
lnk30 lnk99850.cmd

echo ----- Build Intel Hex File --------------------------------
hex30 hex99850.cmd
copy ep99850.hex ..\*.hex
echo ----- See EP99850.LOG for Details -------------------------

rem ------------------------------------------------------------
rem File: _ep99850.bat
rem ------------------------------------------------------------

echo ----- Updating the DSP File to the network ----------------
cd ..
call _update.bat Q

echo ----- Coping Files from the Network to the Working Directory -
y:
cd \Dpp2\Setup
call _setup.bat Q
f:
:Done
echo ----- Completed ----------------------------------------------
