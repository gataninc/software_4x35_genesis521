/********************************************************************************/
/*																*/
/*	Module:	EP99850.C												*/
/*																*/
/*	EDAX Inc														*/
/*																*/
/*  Author:		Michael Solazzi									*/
/*  Created:    3/25/99 												*/
/*  Board:      DPP-II 												*/
/*																*/
/*	Description:													*/
/*		This Code file contains the main process of the DSP for the			*/
/*		DPP-II Board.												*/
/*																*/
/*  History:														*/
/*		05/16/06: MCS												*/
/*			EDS_ISR.ASM - Live Spectrum Mapping changed to add the 		*/
/*			pixel number to the spectral data ( in the report from bits		*/
/*			27 to 12, while leaving bits 11 to 0 as the channel number		*/
/*			The driver must be checked to make sure it only adds on		*/
/*			for a given channel number instead of the pixel number			*/	
/* 																*/
/*		99/08/16 - MCS - Updated for EDI-II Rev B						*/
/********************************************************************************/

#include "bus32.h"
#include "timer30.h"
#include "serprt30.h"
#include "edsutl.h"
#include "defines.h"
#include "dpp2utl.h"
#include "dpp2bit.h"

#define	IntFromEDS 	c_int01
#define	IntFromSEM 	c_int02
#define	IntFromPCI 	c_int03
#define	IntFromSB 	c_int04
#define	IntFromT0 	c_int09
#define IntFromMr 	trap

extern 	EDS_TYPE EDSRec;
extern 	DPP_TYPE DPPRec;

#define Irq_OpCodeAck == 0

/*
//----------------------------------------------------------------------
void Set_PCI_IRQ( void )
{
	DPPRec.IRQ_RESET = 1;
	WriteGlue( iADR_GLUE_IRQ_ENABLE, 0xF );				// Acknowledge PCI and Interrupt
	DPPRec.PCIIRQ_Cnt = 0;
}
//----------------------------------------------------------------------

//----------------------------------------------------------------------
void Reset_PCI_IRQ( void )
{
	DPPRec.IRQ_RESET = 0;								// Reset the Internal Flag
	WriteGlue( iADR_GLUE_IRQ_DISABLE, 0xF );			// Disable the PCI Interrupt
	WriteDPRAM( dADR_IRQ_STAT, 0 );						// Clear the Interrupt Status Register
}
//----------------------------------------------------------------------
*/

//----------------------------------------------------------------------
void IntFromEDS(void)	// no arguments no return value allowed
{
	EDSRec.EDS_IRQ = 1;
}
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Timer0 is setup for 10ms Intervals to support Ratemeter
// However, the Data Transfer is to occur at 100ms Boundries, every 10 10ms Intervals

void IntFromT0(void)	// no arguments no return value allowed
{
	DPPRec.MS100_TC = 1;
	DPPRec.PCIIRQ_Cnt ++;
}
//---------------------------------------------------------------------

//----------------------------------------------------------------------
void IntFromPCI(void)	// no arguments no return value allowed
{
	ULONG status;

	//status = ReadDPRAM( dADR_IRQ_STAT );						// Read the Status Register
	status = *(ULONG *)( DPRAM0_BASE + dADR_IRQ_STAT );
	status &= 0xFFF0;										// Clear the unwanted bits
	if( status == 0x3210 ){									// Interrupt Acknowledge
		DPPRec.IRQ_RESET = 0;								// Reset the Internal Flag
		// WriteGlue( iADR_GLUE_IRQ_DISABLE, 0x0 );				// Disable the PCI Interrupt
		*(ULONG *)( GLUE_BASE + iADR_GLUE_IRQ_DISABLE ) = 0xFFFF;	// Disable the PCI INterrupt
		
		// WriteDPRAM( dADR_IRQ_STAT, 0 );						// Clear the Interrupt Status Register
		*(ULONG *)( DPRAM0_BASE + dADR_IRQ_STAT ) = 0;			// Clear the Interrupt Status Register
		*(ULONG *)( DPRAM0_BASE + dADR_OPCODE_LOOP_BACK ) = 0x5555;
	} else {
		// DPPRec.OpCode = ReadDPRAM( dADR_OPCODE );
		DPPRec.OpCode = (USHORT)(*(ULONG *)( DPRAM0_BASE + dADR_OPCODE ));		// Read the OpCode Register
		DPPRec.OpCode &= 0xFFFF;									// Mask off Upper Bits
		*(ULONG *)( DPRAM0_BASE + dADR_OPCODE_LOOP_BACK ) = 0xAAAA;
		*(ULONG *)( DPRAM0_BASE + dADR_OPCODE_LOOP_BACK2) = DPPRec.OpCode;	
	}
}							// Read the Interrupt Status}
//----------------------------------------------------------------------

//----------------------------------------------------------------------
void IntFromMr(void)	// no arguments no return value allowed
{
}
//----------------------------------------------------------------------

//----------------------------------------------------------------------
void IntFromSB(void)	// no arguments no return value allowed
{
}
//----------------------------------------------------------------------
//----------------------------------------------------------------------
void IntFromSEM(void)	// no arguments no return value allowed
{
}
//----------------------------------------------------------------------


//-----------------------------------------------------------------------------
main( void )
{
	ULONG data;

	//----- Initialization Procedure   -----------------------------------------------
	DSP_Init();							// Initialize DSP Hardware
	WriteEDS( iADR_INT_EN, 0x0 );				// Disable the EDS Interrupt
	WriteEDS( iADR_FIFO_MR, 0x0 );			// Reset the EDS FIFO	
	WriteEDS( iADR_CW, BIT_ENABLE );			// Enable BIT in the EDS FPGA ( Prevents data collection )
	WriteEDS( iADR_TIME_STOP, 0x0 );			// Stop EDS Analysis
	WriteEDS( iADR_TIME_CLR, 0x0 );			// Clear the Timers
	WriteDPRAM( dADR_DSP_VER, DSP_VER );		// This DSP Version is used to verify that the DSP has booted by the driver
	//----- Initialization Procedure   -----------------------------------------------


	DPP_BIT_Test();						// Perform BIT Tests
	//-----------------------------------------------------------------------------------------------
	//**** END OF BIT ********************************************************************************
	//----- Initialization Procedure   -----------------------------------------------
	// Clear the Contents of the DPRAM

	DPPRec.PCIIRQ_Cnt = 0;

	//----- Register Initialization for the TMS320C32
	// Clear and Enable Cache, disable OVM, disable interrupts, edge triggered interrupts
	WriteEDS( iADR_FIFO_MR, 0x0 );
	WriteGlue( iADR_GLUE_FF_MR, 0x0 );							// Reset the PCI FIFO

	//----- Start of Initialization ----------------------------------------------------

	EDS_Init();	 										// Initialize EDS Hardware

	data = ReadEDS( iADR_CW );								// Read the Control Word
	data |= ( 1 << 9 );
	WriteEDS( iADR_CW, (USHORT)data );
	WriteEDS( iADR_FIR_MR, 0xFFFF );

	data = ReadEDS( iADR_TP_SEL );							// Set DIgital TP Select to debug Live Spectrum MApping
	data &= 0x0FFF;
	data |= 0xD000;
	WriteEDS( iADR_TP_SEL, (USHORT)data );
	
	WriteDPRAM( dADR_SCA_GEN, 0x0 );
	WriteDPRAM( dADR_DAC_GEN, 0x0 );
	WriteDPRAM( dADR_DSP_VER, (USHORT)DSP_VER );
	WriteEDS( iADR_RESET_MR, 0xFFFF );
	// Final hand-shaking for the Driver before final boot
	WriteEDS( iADR_DSO_TRIG, 0x6 );
	WriteEDS( iADR_DSO_INT, 0x320 );
	EDSRec.FFT_Mode = 0;


	//----- Main Loop for the DSP Code -----------------------------------------------
	while( 1 )
	{		
		//----- Process a PCI Interrupt -----------------------------------------------------------
		if( DPPRec.OpCode != 0 ){
			data = DPPRec.OpCode;									// Save OpCode
			WriteEDS( iADR_RTEM_CMD_WD_MR, 0xFFFF );					// Reset Watch-Dog with every op-code

			if(( data & 0x8000 ) == 0 ){						// Write OpCode
				Proc_Write_OpCode( (USHORT)data );
			} else {												// Read OpCode
				Proc_Read_OpCode( (USHORT)data );
			}
			WriteDPRAM( dADR_OPCODE_LOOP_BACK, (USHORT)data );					// Write Processed OpCode to Loop-Back Register		
			DPPRec.OpCode = 0;										// Clear the OpCode
			WriteDPRAM( dADR_OPCODE, DPPRec.OpCode );					// Write it back to DPRAM
		}
		//----- Process a PCI Interrupt -----------------------------------------------------------

		//----- Process an EDX Interrupt ------------------------------------------------------
		if( EDSRec.EDS_IRQ != 0 ){
			Proc_Eds_Isr();
			if(( ReadEDS( iADR_STAT ) & 0x1 ) == 0 ){			// Read the Status of the EDS FPGA and if FIFO is not empty
				EDSRec.EDS_IRQ = 1;
			}
		}
		//----- Process an EDX Interrupt ------------------------------------------------------

		if( DPPRec.MS100_TC != 0 ){							// 100ms Timer
			if( ++EDSRec.Reset_FIR > 1 ){
			//	WriteEDS( iADR_FIR_MR, 0xFFFF );
				EDSRec.Reset_FIR = 0;
			}
		}

		if( EDSRec.DSO_Enable != 0 ){
			if(( EDSRec.EDS_IRQ == 0 ) && (( ReadEDS( iADR_STAT ) & 0x1 ) == 0 )){			// Read the Status of the EDS FPGA and if FIFO is not empty
				EDSRec.EDS_IRQ = 1;
				
			}
		}

		if( DPPRec.IRQ_RESET == 0 ){
			//----- Process DSO Done Interrupt ----------------------------------------------------
			if( EDSRec.DSO_PENDING != 0 ){					// DSO Mode Pending Data	
				DPPRec.Set_PCI_IRQ = 1;								// Set_PCI_IRQ();
				EDSRec.DSO_PENDING = 0;								// Clear Pending flag
			}
 			//----- Process DSO Done Interrupt ----------------------------------------------------


			//----- LIVE SPECTRUM MAPPING MODE ( REDUCED FUNCTION ) -------------------------------
			if( EDSRec.Preset_Mode >= FPGA_PRESET_LSMAP_REAL ){

				// if WDS Mapping, force PRESET_DONE_CNT to be PRESET_DONE_POINTS to trigger TMR_ISR interrupt
				if( EDSRec.Preset_Mode >= FPGA_PRESET_WDS_REAL ){
					EDSRec.Preset_Done_Cnt = EDSRec.Preset_Done_Points;
				}

//				EDSRec.Preset_Done_Cnt = ReadEDS( iADR_PRESET_DONE_CNT );			// Read Preset Done Counts from FPGA
				//----- Process 100ms Timer Interrupt -------------------------------------------------
				if(    ( DPPRec.MS100_TC != 0 ) 
					|| ( EDSRec.FPGA_Preset_Done != 0 )) {
//					|| (( EDSRec.FPGA_Preset_Done != 0 ) && ( EDSRec.Preset_Done_Points == EDSRec.Preset_Done_Cnt )) ){
						if(( ReadEDS( iADR_STAT ) & 0x8 ) == 0 ){
							Proc_Tmr_Isr();
							DPPRec.Set_PCI_IRQ = 1;
						}
				}

				if( DPPRec.MS100_TC != 0 ) 
					DPPRec.MS100_TC = 0;
					
				if( EDSRec.FPGA_Preset_Done != 0 ) 
					EDSRec.FPGA_Preset_Done = 0;
				//----- Process 100ms Timer Interrupt -------------------------------------------------
				
			}
			//----- LIVE SPECTRUM MAPPING MODE ( REDUCED FUNCTION ) -------------------------------
			
			//----- Normal Mode -------------------------------------------------------------------
			else {												// All Other Modes
				//----- Process 100ms Timer Interrupt -------------------------------------------------
				if( DPPRec.MS100_TC != 0 ){							// 100ms Timer
//					if( EDSRec.RTEM_Init != 0 ) rtem_init();
					if( EDSRec.ADisc != 0 ) Auto_Disc();
				}
				//----- Process 100ms Timer Interrupt -------------------------------------------------

				//----- Process EDX Anal Done Interrupt -----------------------------------------------
				if(( EDSRec.FPGA_Preset_Done != 0 ) || ( DPPRec.MS100_TC != 0 )){					// EDS Preset Done				
					Proc_Tmr_Isr();					
					DPPRec.Set_PCI_IRQ = 1;						// Set_PCI_IRQ();
					EDSRec.FPGA_Preset_Done = 0;					// Clear PReset Done Flag
					DPPRec.MS100_TC = 0;
				}	
				//----- Process EDX Anal Done Interrupt -----------------------------------------------

				
				if( ReadDPRAM( dADR_SCA_GEN ) != 0 ){
					WriteEDS( iADR_SCA, ReadDPRAM( dADR_SCA_GEN ) );
				}

				if( ReadDPRAM( dADR_DAC_GEN ) != 0 ){
					BIT_DAC_GEN();
				}
			}												// Live Spectrum Mapping Mode
			//----- Normal Mode -------------------------------------------------------------------
		}													// if IRQ_RESET == 0
		//------------------------------------------------------------------------------------------


		//------------------------------------------------------------------------------------------
		if(( DPPRec.IRQ_RESET == 0 ) && ( DPPRec.Set_PCI_IRQ == 1 )){
			//	Set_PCI_IRQ();
			DPPRec.IRQ_RESET = 1;
			WriteGlue( iADR_GLUE_IRQ_ENABLE, 0xF );						// Acknowledge PCI and Interrupt
			WriteDPRAM( dADR_IRQ_STAT, 0x8000 );						// Read the Status Register
			DPPRec.PCIIRQ_Cnt = 0;
			DPPRec.Set_PCI_IRQ = 0;
		}

	}
	return(0);
}					// Main
//---------------------------------------------------------------------------------------------------

/********************************************************************************/
/* END OF EP99850.C													*/
/********************************************************************************/
