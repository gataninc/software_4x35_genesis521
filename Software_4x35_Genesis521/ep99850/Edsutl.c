/********************************************************************************/
/*																*/
/*	Module:	EDSUTL.C												*/
/*																*/
/*	EDAX Inc														*/
/*																*/
/*  Author:		Michael Solazzi									*/
/*  Created:    3/25/99 												*/
/*  Board:      EDI-II 												*/
/*																*/
/*	Description:													*/
/*		This Code file contains all of the definitions and general purpose	*/
/*		routines to be used in the EDI-II Board.						*/
/*																*/
/*  History:														*/
/*																*/
/*		99/08/16 - MCS - Updated for EDI-II Rev B						*/
/********************************************************************************/

#include "bus32.h"
#include "timer30.h"
#include "serprt30.h"
#include "defines.h"
#include "edsutl.h"
#include "dpp2utl.h"

volatile EDS_TYPE EDSRec;						// EDS Record
volatile ROI_TYPE ROIRec[MAX_ROI+1];			// ROI Record

extern DPP_TYPE DPPRec;

//*****************************************************************************
//------ EDS Initialization Routine --------------------------------------------------------
void EDS_Init( void )
{
	int i;
	USHORT data;
	//****** Initialize Parameters in the DPP Record **************************

	EDSRec.DSO_PENDING = 0;
	EDSRec.Preset_Mode = 0;
	EDSRec.RTEM_Init   = 0;
	EDSRec.DSO_Enable	= 0;

	EDSRec.FPGA_Preset_Done = 0;

	//****** Initialize Parameters in the DPP Record **************************

				
	//--------------------------------------------------------------------------------
	// Setup for Dither DAC Test - Sets a RAMP on the output 
	// Set bit 3 to a 1, disable dither


	//***** EDS Control Word ************************************************

//	WriteEDS( iADR_PA_RESET_THR, 0x80 );
//	WriteEDS( iADR_PA_RESET_THR, 0x30 );
//	WriteEDS( iADR_PA_RESET_NTHR, 0x0800 ); // Current
//	WriteEDS( iADR_PA_RESET_PTHR, 0xF000 );
//	WriteEDS( iADR_PA_RESET_PTHR, 0x0100 );	// Current
//	WriteEDS( iADR_FIR_DIFF_THR, 0x010 );

	WriteEDS( iADR_CW,		0x100 );	
	WriteEDS( iADR_SCA,		0x10 );				// EDI-III
	WriteEDS( iADR_DSCA_PW, 0x9 );			// Set to 1 us
	
	WriteEDS( iADR_FIFO_MR, 0x0 );
	WriteEDS( iADR_RAMA,	0x0 );

	WriteEDS( iADR_DSO_TRIG, 0x0 );
	WriteEDS( iADR_DSO_INT, 0x0 );
	WriteEDS( iADR_TIME_CLR, 0x0 );					// Clear Clock Time and Live Time

	WriteEDS( iADR_TP_SEL,		0x91A );
	WriteEDS( iADR_INT_EN,		0x0 );
	WriteEDS( iADR_CGAIN,		13275 );
	WriteEDS( iADR_PS,			0x0 );


	WriteDPRAM( dADR_AUTO_DISC, 0x0 );
	EDSRec.ADisc = 0;

	EDSRec.Preset_Done_Points = 0x40;
	WriteDPRAM( dADR_PRESET_POINTS, 0x40 );


	//------------------------------------------------------------------------------------
	// All FPGA Memory Accesses here become initialized
	data = ReadEDS( iADR_CW );					// Read the Control Word
	data &= ~BIT_ENABLE;						// Clear BIT_ENABLE Flag
	data |= RAM_ACCESS;							// Set the Memory Access Bit
	data |= NOISE_SPEC_EN;
	WriteEDS( iADR_CW, data );					// Rewrite the Control Word


	// SCA/Fast Map Look-Up
	for( i= 0;i<= 0x7FF;i++){
		WriteEDS( iADR_RAMA, (USHORT) i);
		if( i <= 0xFF ){
			WriteEDS( iADR_ROI_LU, 0xFFFF );		// ROI Defined Look-Up
		}
		WriteEDS( iADR_SCA_LU, 	0x0 );			// Clear Digital SCA
	}
	
	data = ReadEDS( iADR_CW );					// Read the Control Word
	data &= ~RAM_ACCESS;						// Set the Memory Access Bit
	WriteEDS( iADR_CW, data );					// Rewrite the Control Word
	// All FPGA Memory Accesses here become initialized
	//------------------------------------------------------------------------------------

	WriteEDS( iADR_CLIP_MIN,			0x0 );
	WriteEDS( iADR_CLIP_MAX,			0xFFF );
		
	WriteEDS( iADR_TIME_STOP,			0x1 );
	WriteEDS( iADR_TIME_CLR,			0x1 );
	WriteEDS( iADR_PRESET_MODE,			0x0 );
	data = 10000;						// 10000 X 1ms= 10 seconds
	WriteDPRAM( iADR_PRESET_LO, (USHORT)data);
	WriteDPRAM( iADR_PRESET_HI, (USHORT)(data>>16));

	WriteEDS( iADR_RTEM_INI,				0x1312 );
	WriteEDS( iADR_RTEM_CMD_OUT,			0x0 );
	WriteEDS( iADR_RTEM_HCMR,			0x0 );
	WriteEDS( iADR_RTEM_HC_RATE_LO,		9999 );
	WriteEDS( iADR_RTEM_HC_RATE_HI,		0x0 );          //
	WriteEDS( iADR_RTEM_HC_THRESHOLD,		0xA );
	WriteEDS( iADR_SCA,					0x0 );
	WriteEDS( iADR_DSCA_PW,				0x13 );			// 1 us
	WriteEDS( iADR_FIFO_MR,				0x0 );
	WriteEDS( iADR_DISC_EN,				0x3FC );
	WriteEDS( iADR_TC_SEL,				0x6 );
	WriteEDS( iADR_FIR_DLY_LEN,			0x3 );			// Reduce for Throughput, Increase for Reso
	WriteEDS( iADR_FIR_DLY_INC,			0x4 );

	WriteEDS( iADR_INT_EN, 0xFFFF );				// Enable the EDS Interrupt

	WriteEDS( iADR_DECAY, 0x0100 );
	
	WriteEDS( iADR_BLANK_MODE, 0x1 );
	WriteEDS( iADR_BLANK_DELAY, 0x0 );
	WriteEDS( iADR_BLANK_WIDTH, 0x10 );


	Write8420( iADR_EVCH0,  0xC00 );
	Write8420( iADR_BIT,   0x800 );
	Write8420( iADR_DISC,  1190 );
	Write8420( iADR_EVCH1,  0xFFF );
	WriteEDS( iADR_DLEVEL0,		52 );
	WriteEDS( iADR_DLEVEL1,		50 );
	WriteEDS( iADR_DLEVEL2,		48 );
	WriteEDS( iADR_DLEVEL3,		42 );

	// IMS Motor Interface Parameters for the RTEM Slide
	//	MSEL = 25
	//	MRC  = 100%
	//	MRH  = 10%

	WriteEDS( iADR_RTEM_LOW_SPEED,	UMS_Low_Speed );
	WriteEDS( iADR_RTEM_MED_SPEED,	UMS_Med_Speed );
	WriteEDS( iADR_RTEM_HIGH_SPEED, 	UMS_High_Speed );
	Set_RTEM_Range( RTEM_MAX_RANGE );

	WriteEDS( iADR_RMS_THR, 0x20 );

	WriteEDS( iADR_BIT_PEAK1,	0x80);
	WriteEDS( iADR_BIT_PEAK2,	0x0 );
	WriteEDS( iADR_BIT_CPS,		999 );
	WriteEDS( iADR_BIT_PK2D,		0x0 );
	
	for(i=0;i<=MAX_ROI;i++){
		ROIRec[i].Start = 0;
		ROIRec[i].End = 0;
		ROIRec[i].Enable = 0;
		ROIRec[i].Defined = 0;
		ROIRec[i].Sca = 0;
	}
	EDSRec.EDS_DEF_ROIS = 0;

	WriteDPRAM( dADR_TIMECONST, 0x10 );
	WriteDPRAM( dADR_BLANK_FACTOR, 10 );
	Set_Time_Const();
	Set_Blank_Factor();

	WriteDPRAM( dADR_EVCH, 2 );					// Default for 10eV/Ch
	Set_EvCh();

	SetROISCARM( 0 );									// Rebuild ROI/SCA/Ratemeter Table

	EDSRec.Max_Fir_Cnt = 0;
	for(i=0;i<=3;i++){
		EDSRec.Max_Fir[i] = 0;
	}
	WriteDPRAM( dADR_MAX_CPS, 10 );
	data = 10 * 1000;
	WriteDPRAM( iADR_PRESET_LO, data );
	WriteDPRAM( iADR_PRESET_HI, (USHORT)(data >> 16 ));

	WriteDPRAM( dADR_ANAL_MODE, EDS_ANAL_PRESET_NONE );
	WriteDPRAM( dADR_TRANSFER, 0x1 );
	SetAnalMode();

	WriteEDS( iADR_INT_EN, 0xFFFF );				// Enable the EDS Interrupt
	WriteEDS( iADR_FIFO_MR, 0x0 );

	//***** Initial Settings while waiting for "Shell Application"
	//					   DSO  | Analog Test Point | Digital Test point
	WriteEDS( iADR_TP_SEL, ( 18 ) | ( ATP_AFILTER ) | ( 0xD << 12 ));

	data = ReadEDS( iADR_CW );
	data &= ~( 3 << 11 );						// Clear PreAmp Source Bits
	data &= ~( 1 << 13 );						// Set Input Gain to 1X

//	data |=  ( 3 << 11 );						// Set BIT_DAC - Digital Source
//	data |=  ( 2 << 11 );						// Set BIT_DAC - Analog Source
//	data |=  ( 1 << 14 );						// Set AD_DEBUG mode - Route AD_DATA_MUX to XPORT connector
	data |=  ( 1 << 13 );						// Set 2x Input Gain
	data |=  ( 1 << 15 );						// Set FDISC, triggered DSO Mode
	WriteEDS( iADR_CW, data );

	data = ReadEDS( iADR_CW );
	if(( data & 0x8000 ) == 0 ){
//		WriteEDS( iADR_SHDISC_DLY_SEL,	0x25 );		// SiLi or Pulsed-Reset Detectors
//		WriteEDS( iADR_SHDISC_DLY_SEL,	0x24 );		// SiLi or Pulsed-Reset Detectors
//		WriteEDS( iADR_SHDISC_DLY_SEL,	0x23 );		// SiLi or Pulsed-Reset Detectors
		WriteEDS( iADR_SHDISC_DLY_SEL, 0x43 );			// SiLi or Pulsed-Reset Detectors
	} else {
//		WriteEDS( iADR_SHDISC_DLY_SEL,	0x31 );		// SDD or WDS or Tail-Pulse Detectors
//		if( TimeConst == 0 ){
			WriteEDS( iADR_SHDISC_DLY_SEL,	0x49 );		// SDD or WDS or Tail-Pulse Detectors
//		} else {
//			WriteEDS( iADR_SHDISC_DLY_SEL,	0x50 );		// SDD or WDS or Tail-Pulse Detectors
//		}
	}
	//***** Initial Settings while waiting for "Shell Application"
}
//------ EDS Initialization Routine --------------------------------------------------------
//*****************************************************************************

//*****************************************************************************
//------- Routine to Write to EDS FPGA --------------------------------------------------
void WriteEDS( USHORT addr, USHORT data )
{
	*(ULONG *)( EDS_BASE + addr ) = data;				// Write the data directly
}
//------- Routine to Write to EDS FPGA --------------------------------------------------
//*****************************************************************************

//*****************************************************************************
//------- Routine to Write to EDS FPGA --------------------------------------------------
USHORT ReadEDS( USHORT addr )
{
	ULONG data;
	data = *(ULONG *)( EDS_BASE + addr );				// Write the data directly
	data &= 0xFFFF;
	return( (USHORT)data );
}
//------- Routine to Write to EDS FPGA --------------------------------------------------
//*****************************************************************************

//*****************************************************************************
//------- Routine to Write to EDS FPGA --------------------------------------------------
void WriteDEDS( USHORT addr, ULONG data )
{
	*(ULONG *)( EDS_BASE + addr ) = data;				// Write the data directly
	*(ULONG *)( EDS_BASE + addr + 1 ) = ( data >> 16 );
}
//------- Routine to Write to EDS FPGA --------------------------------------------------
//*****************************************************************************

//*****************************************************************************
ULONG ReadDEDS( USHORT addr )
{
	ULONG data;
	data = ( *(ULONG *)( EDS_BASE + addr ) ) & 0xFFFF;
	data = data | ((( *(ULONG *)( EDS_BASE + addr ) ) & 0xFFFF ) << 16 );
	return( data );
}
//*****************************************************************************
	
//*****************************************************************************
//------- Routine to Write to the EDS FIFO  -----------------------------------
void WriteEDSFIFO( ULONG data )
{
	ULONG sdata;
	sdata = data & 0xFFFF;
	WriteEDS( iADR_FIFO_LO, (USHORT)sdata );		// Load Low FIFO Address
	sdata = ( data >> 16 ) & 0xFFFF;
	WriteEDS( iADR_FIFO_HI, (USHORT)sdata );		// Load Hi FIFO Address
}
//*****************************************************************************

//*****************************************************************************
//------- Routine to Read from the EDS FIFO  ----------------------------------
ULONG ReadEDSFIFO( void )
{
	ULONG rtmp = 0;

	rtmp = *(ULONG *) EDS_FIFO_STAT;		// Read the FIFO Status
	rtmp &= 0x1;
	if( rtmp == 0 ){						// FIFO Not Empty
		rtmp = *(ULONG *) EDS_FIFO_BASE;	// Read the EDS FIF0
		rtmp &= 0x3FFFF;
	} 
	return( rtmp );
}
//------- Routine to Read from SEM FIFO  -----------------------------------------------------
//*****************************************************************************

//*****************************************************************************
//------- Routine to Write to the DAC8420 ------------------------------------
// it waits for it not to be busy before writting
void Write8420( USHORT addr, USHORT data )
{
	ULONG rtmp;
	USHORT CWord;
	int i;

	// Disable Blevel Update and BIT_DAC
	CWord = ReadEDS( iADR_CW );
	WriteEDS( iADR_CW, (USHORT)( CWord & 0xA7FF) );
	i = 1000;
	do{
		rtmp = *(ULONG *) EDS_BASE + iADR_STAT;
	} while((( rtmp & 0x40 ) == 0x40 ) && ( --i > 0 ));
	*(ULONG *)( EDS_BASE + addr ) = data;				// Write the data directly
	WriteEDS( iADR_CW, CWord );
}
//------- Routine to Write to the DAC8420 ------------------------------------
//*****************************************************************************

//*****************************************************************************
//------- Routine to Read from BIT ADC   -----------------------------------------------------
USHORT ReadBITADC( void )
{
	ULONG rtmp;
	int i;

	WriteEDS( iADR_BIT_ADC, 0xFFF );		// Write to the BIT DAC to start the conversion
	i = 1000;
	do{
		// rtmp = *(ULONG *) EDS_BASE + ( iADR_STAT & 0xFF );
		rtmp = ReadEDS( iADR_STAT );
	} while((( rtmp & 0x4 ) == 0x4 ) && ( --i > 0 ));			// ADC still busy

	if( i == 0 ){
		rtmp = 0xFFFF;
	} else {
		rtmp = ReadEDS( iADR_BIT_ADC );				// No-Longer Busy so Read the ADC
	}
	rtmp &= 0xFFFF;
	return( (USHORT)rtmp );
}
//------- Routine to Read from BIT ADC   -----------------------------------------------------
//*****************************************************************************


//*****************************************************************************
// Functions to Set and Get global parameters for various operations
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void Set_Time_Const( void )
{
	USHORT DiscEnable;
	USHORT data;
	USHORT TimeConst;
	USHORT Reset_Fir;


	// This code is used to set the DPP to the ME FET Mode until the SW handles this properly
//	data = ReadEDS( iADR_CW );								// Read the Control Word
//	data |= 0x100;
//	WriteEDS( iADR_CW, data );

	// These are here to enable proper operation when using the Signal-Tap
//	WriteEDS( iADR_PA_RESET_NTHR,	0x0800 );
//	WriteEDS( iADR_PA_RESET_PTHR,	0x0100 );
	WriteEDS( iADR_FIR_DLY_LEN,		0x3 );			// Reduce for Throughput, Increase for Reso
	WriteEDS( iADR_FIR_DLY_INC,		0x4 );			// Reduce for Throughput, Increase for Reso
	WriteEDS( iADR_INT_EN, 0xFFFF );				// Enable the EDS Interrupt
	WriteEDS( iADR_DECAY, 0x0100 );
	// These are here to enable proper operation when using the Signal-Tap

	// Read the TimeConst from the FPGA
	TimeConst = ReadEDS( iADR_TC_SEL );
	TimeConst &= 0xF;

	// Read the TimeConst from the DPRAM
	data = ReadDPRAM( dADR_TIMECONST );
	data &= 0xF;

	// If the Two TimeConsts are different, Reset the FIR
	if( data == TimeConst ){
		EDSRec.Reset_FIR = 0;
	} else {
		EDSRec.Reset_FIR = 1;
	}
	WriteEDS( iADR_TC_SEL, data );	
	TimeConst	= data;
	WriteEDS( iADR_EVCH1, 0x0 );					// eV/Ch1 Must be set to 0V for SiLi Detectors
	

	data = ReadEDS( iADR_CW );
	if(( data & 0x8000 ) == 0 ){
		DiscEnable = DISC_SH | DISC_E;			
	} else {
		DiscEnable = DISC_E;							// SDD (Ketek - Disable Super High Speed Dicriminator
	}

	switch( TimeConst ){
		case 0 : 										//  PT0:  0.4us
		case 1 :										//  PT1:  0.8us
			break;	

		case 2 : 										//  PT2:  1.6us
			DiscEnable |= DISC_H;						// Enable the High 0.4us
			break;	
		case 3 : 										//  PT3:  3.2us
		case 4 : 										//  PT4:  6.4us
		case 5 :										//  PT5: 12.8us 
			DiscEnable |= DISC_H;						// Enable the High 0.4us
			DiscEnable |= DISC_M;						// Enable the Med  0.8us
			break;	
		case 6 :										//  PT6: 25.6us
		case 7 :										//  PT7: 51.2us
		default :										// PT8: 102.4us
			DiscEnable |= DISC_H;						// Enable the High 0.4us
			DiscEnable |= DISC_M;						// Enable the Med  0.8us
			DiscEnable |= DISC_L;						// Enable the Low  6.4us
			break;	
	}
	WriteEDS( iADR_DISC_EN, DiscEnable );
	Set_EvCh();
}
//-----------------------------------------------------------------------------

//----------------------------------------------------------------------
void Set_EvCh( void )
{
	USHORT data;

	data = ReadDPRAM( dADR_EVCH );						// Read from DP Memory
	WriteEDS( iADR_DISC_EVCH, data );
}
/***********************************************************************************************************
	#define EVCH_FSC 4095
	cw = ReadEDS( iADR_CW );
	if(( cw & 0x8000 ) == 0 ){						// Sili Detectors
		WriteEDS( iADR_DISC_EVCH, (USHORT)(3-data));			// Set Code to Disc Ev/Ch
		
		// evch = ( 4 - data ) * FSC - FSC
		//        ------------         ---
		//            4                 8
		// which reduces to:
		// evch = ( 7 - 2data ) * FSC
		//        -------------
		//              8

		data = ( 7 - ( 2 * data )) * EVCH_FSC;
		data = (USHORT)((float)data / (float) 8 );
		WriteEDS( iADR_EVCH0, data );
		WriteEDS( iADR_EVCH1, 0x0 );					// eV/Ch1 Must be set to 0V for SiLi Detectors
	} else {										// Gaussian Input
		WriteEDS( iADR_DISC_EVCH, 0x3 );
	}
**************************************************************************************************************/

//----------------------------------------------------------------------
// was in ev/ch	
//		switch( data ){
//			case 0 : 									// 2.5eV/ch	
//				WriteEDS( iADR_DISC_EVCH, 3 );	
//				WriteEDS( iADR_EVCH0, 3584 ); 
//				break;	
//			case 1 : 									// 5.0ev/ch
//				WriteEDS( iADR_DISC_EVCH, 2 ); 
//				WriteEDS( iADR_EVCH0, 2560 );				
//				break;	
//			case 2 : 									// 10ev/ch
//				WriteEDS( iADR_DISC_EVCH, 1 );	
//				WriteEDS( iADR_EVCH0, 1536 );			
//				break;	
//			case 3 : 									// 20evch
//				WriteEDS( iADR_DISC_EVCH, 0 );	
//				WriteEDS( iADR_EVCH0, 512  );
//				break;	
//			default : ;
//		}

//	if((( cw >> 11 ) & 0x3 ) == 0 ){							// ( Vpgm / 2.5 ) * 4096
//		switch( data ){
//			case 0 : WriteEDS( iADR_EVCH0, 3584 );   break;	// 2.5eV/Ch
//			case 1 : WriteEDS( iADR_EVCH0, 2560 );   break;	// 5.0eV/Ch
//			case 2 : WriteEDS( iADR_EVCH0, 1536 );	 break;	// 10.0eV/Ch
//			case 3 : WriteEDS( iADR_EVCH0, 512  );	 break;	// 20.0eV/Ch
//			default : ;
//		}
//	} else {
//		switch( data ){
//			case 0 : WriteEDS( iADR_EVCH1, 3584 );   break;	// 2.5eV/Ch
//			case 1 : WriteEDS( iADR_EVCH1, 2560 );   break;	// 5.0eV/Ch
//			case 2 : WriteEDS( iADR_EVCH1, 1536 );	 break;	// 10.0eV/Ch
//			case 3 : WriteEDS( iADR_EVCH1, 512  );	 break;	// 20.0eV/Ch
//			default : ;
//		}
//	}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void Set_Blank_Factor( void )
{
	USHORT data;
	float factor;
	float PTime;

	data = ReadDPRAM( dADR_TIMECONST );
	data &= 0xF;

	switch( data ){
		case 0  : PTime =   0.4f; break;
		case 1  : PTime =   0.8f; break;
		case 2  : PTime =   1.6f; break;
		case 3  : PTime =   3.2f; break;
		case 4  : PTime =   6.4f; break;
		case 5  : PTime =  12.8f; break;
		case 6  : PTime =  25.6f; break;
		case 7  : PTime =  51.2f; break;
		default : PTime = 102.4f; break;
	}
	factor = (float)ReadDPRAM( dADR_BLANK_FACTOR ) / (float)10.0f;
	data = (USHORT) (( PTime*factor ) / 0.05f );
	WriteEDS( iADR_BLANK_WIDTH, data );

}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void SetAnalMode( void )
{
	USHORT data;
	USHORT Target_Status;
	int done;

	// Set Status Bit 2 to indicate in SetAnalMode
	data = ReadDPRAM( iADR_STAT );
	data |= 0x4;
	WriteDPRAM( dADR_EDS_STAT, data );			// Clear the Status Register but Set bit 2

	EDSRec.EDS_Mode = (UCHAR)( ReadDPRAM( dADR_ANAL_MODE ) & 0xF );

	switch( EDSRec.EDS_Mode) {
		case EDS_ANAL_PRESET_REAL 		: EDSRec.Preset_Mode = FPGA_PRESET_CLOCK;		break;
		case EDS_ANAL_PRESET_LIVE 		: EDSRec.Preset_Mode = FPGA_PRESET_LIVE;		break;
		case EDS_ANAL_PRESET_COUNT 		: EDSRec.Preset_Mode = FPGA_PRESET_COUNT;		break;
		case EDS_ANAL_PRESET_SMAP_REAL	: EDSRec.Preset_Mode = FPGA_PRESET_LSMAP_REAL;	break;
		case EDS_ANAL_PRESET_SMAP_LIVE	: EDSRec.Preset_Mode = FPGA_PRESET_LSMAP_LIVE;	break;
		case EDS_ANAL_PRESET_NONE 		: EDSRec.Preset_Mode = FPGA_PRESET_NONE;		break;
		case EDS_ANAL_PRESET_REAL_PAMR	: EDSRec.Preset_Mode = FPGA_PRESET_CLOCK_PAMR;	break;
		case EDS_ANAL_PRESET_WDS_REAL	: EDSRec.Preset_Mode = FPGA_PRESET_WDS_REAL;	break;
		case EDS_ANAL_PRESET_WDS_LIVE	: EDSRec.Preset_Mode = FPGA_PRESET_WDS_LIVE;	break;
		case EDS_ANAL_STOP 			:									break;		
		case EDS_ANAL_RESUME 		:									break;
		default 					:									break;
	}
	WriteEDS( iADR_PRESET_MODE, EDSRec.Preset_Mode );	// Write th FPGA Preset Mode

	// Determine if xfer data to PCI while analyzer is running
	data  = ReadEDS( iADR_CW );
	if(( ReadDPRAM( dADR_TRANSFER ) & 0x1 ) == 0x1 ){
		EDSRec.EDSXFer = 1;
		data |= EDS_XFER;
	} else {
		EDSRec.EDSXFer = 0;
		data &= ~EDS_XFER;
	}
	WriteEDS( iADR_CW, data );
	
	switch( EDSRec.EDS_Mode ){
		case EDS_ANAL_PRESET_REAL :
		case EDS_ANAL_PRESET_LIVE :	
		case EDS_ANAL_PRESET_COUNT:			// Preset ROI
		case EDS_ANAL_PRESET_NONE :
		case EDS_ANAL_PRESET_REAL_PAMR :
			Target_Status = 1;
			EDSRec.FPGA_Preset_Done = 0;

			WriteEDS( iADR_TIME_STOP, 0xFFFF );		// Stop the Acquisition
			WriteEDS( iADR_TIME_CLR, 0xFFFF);			// Clear Clock Time and Live Time

			data = ReadDPRAM( iADR_PRESET_LO );		// Read the Preset Low Bits
			WriteEDS( iADR_PRESET_LO, data );			// Write them to the FPGA
			data = ReadDPRAM( iADR_PRESET_HI );		// Read the Preset High Bits
			WriteEDS( iADR_PRESET_HI, data );			// Write them to the FPGA

			WriteEDS( iADR_TIME_START, 0x0 );			// Start the Acquisition
			break;


		case EDS_ANAL_PRESET_SMAP_REAL :
		case EDS_ANAL_PRESET_SMAP_LIVE :
		case EDS_ANAL_PRESET_WDS_REAL :
		case EDS_ANAL_PRESET_WDS_LIVE :
			EDSRec.FPGA_Preset_Done = 0;
			EDSRec.Preset_Done_Cnt = 0;
			WriteDPRAM( dADR_PRESET_DONE_CNT, 0x0 );
			WriteEDS( iADR_TIME_STOP, 0xFFFF );		// Stop the Acquisition
			WriteEDS( iADR_TIME_CLR, 0xFFFF);			// Clear Clock Time and Live Time

			data = ReadDPRAM( iADR_PRESET_LO );		// Read the Preset Low Bits
			WriteEDS( iADR_PRESET_LO, data );			// Write them to the FPGA
			data = ReadDPRAM( iADR_PRESET_HI );		// Read the Preset High Bits
			WriteEDS( iADR_PRESET_HI, data );			// Write them to the FPGA
			break;

		case EDS_ANAL_RESUME	:
			Target_Status = 1;
			EDSRec.FPGA_Preset_Done = 0;
			WriteEDS( iADR_TIME_START, 0x0 );			// Start the Acquisition
			break;

		case EDS_ANAL_STOP	:
		default :
			Target_Status = 0;
			EDSRec.FPGA_Preset_Done = 0x4000;			// Set the FPGA Preset Done, will force Preset Done, 
												// Preset Done will disable the Timer Interrupt to the PCI
			WriteEDS( iADR_TIME_STOP, 0xFFFF );		// Stop the Acquisition
			break;
	}											// switch...

	if( ( EDSRec.EDS_Mode  == EDS_ANAL_PRESET_REAL_PAMR ) 
	 || ( EDSRec.EDS_Mode  == EDS_ANAL_PRESET_SMAP_REAL )
	 || ( EDSRec.EDS_Mode  == EDS_ANAL_PRESET_SMAP_LIVE )
	 || ( EDSRec.EDS_Mode  == EDS_ANAL_PRESET_WDS_REAL  )
	 || ( EDSRec.EDS_Mode  == EDS_ANAL_PRESET_WDS_LIVE  ) ){
		done = 1;
	} else {
		data = 0;
		done = 0;
		do{
			EDSRec.EDS_Stat = (( ReadEDS( iADR_STAT ) >> 3 ) & 0x1 );	// Get EDS Status from FPGA
			if( Target_Status == 1 ){
				if( EDSRec.EDS_Stat == 1 ){				// Attempting to STart
					done = 1;
				} else {
					WriteEDS( iADR_TIME_START, 0x0 );
					done = 0;
				}
			} else {
				if( EDSRec.EDS_Stat == 0 ){
					done = 1;
				} else {
					WriteEDS( iADR_TIME_STOP, 0x0 );
					done = 0;
				}
			}
			data++;
		} while(( done == 0 ) && ( data < 0xFE ));
	}

	if( done == 1 ){
		// 05/16/00 - MCS - Ver 1.07
		data = ReadDPRAM( dADR_EDS_STAT );
		data &= 0xFFFC;						// Clear Preset Done and EDS Status
		data |= EDSRec.EDS_Stat;					// Set EDS Status in the LSB
		WriteDPRAM( dADR_EDS_STAT, data );
	} else {
		WriteDPRAM( dADR_EDS_STAT, 0xFFFF );		// Error Code
	}
	data = ReadDPRAM( dADR_EDS_STAT );
	data &= ~0x4;								// Clear in SET_ANAL bit
	WriteDPRAM( dADR_EDS_STAT, data );		
}											// SetAnalMode
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void DefineROI( void )
{
	USHORT roi;
	
	// Read ROI Number
	roi = ReadDPRAM( dADR_ROI_NUM );
	if(( roi >= 1 ) && ( roi <= MAX_ROI )){		// Valid ROI Number
		ROIRec[roi].Start = ReadDPRAM( dADR_ROI_START );	// Read Starting Channel
		ROIRec[roi].End   = ReadDPRAM( dADR_ROI_END   );	// Read Starting Channel
		ROIRec[roi].Enable = 0;								// Disable the ROI
		ROIRec[roi].Defined = 1;							// Has just been defined
		ROIRec[roi].Sca = 0;
		EDSRec.EDS_DEF_ROIS ++;							// Increment the Number of ROIs
	}
}								// Define ROI
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void PSetROI( void )
{
	int i;
	USHORT roi;
	UCHAR roi_enable;
	
	// Read ROI Number
	roi = ReadDPRAM( dADR_ROI_NUM );
	roi_enable = (UCHAR)( ReadDPRAM( dADR_ROI_ENABLE ) & 0x1 );
	if( roi == 0 ){
		for(i=0;i<=MAX_ROI;i++){
			if( ROIRec[i].Defined == 1 ){							// If Defined Set Enable Flag
				ROIRec[i].Enable = roi_enable;						// Enable/Disable the ROI
			}
		}
	}
	else if(( roi >= 1 ) && ( roi <= MAX_ROI )){					// Valid ROI Number
		ROIRec[roi].Enable = roi_enable;							// Enable/Disable the ROI
	}
	SetROISCARM( roi );
}							// PSetROI
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void SetROISCARM( int ROI_NUM )
{
	int i;
	int j;
	USHORT data;
	USHORT sca;
	ULONG rm_lu_mask;
	USHORT RMLU;
	USHORT Sav_Status;
	USHORT addr;
	USHORT sca_mask;
	USHORT sca_mode;

	// Acquistion Must be stopped 
	Sav_Status = ( ReadEDS( iADR_STAT ) >> 3 ) & 0x1;		// Read the Time_Enable
	WriteEDS( iADR_TIME_STOP, 0x0 );						// Stop the Acquistion

	data = ReadEDS( iADR_CW );								// Read the Control Word
	
	sca_mode = 0;
	if(( data & SCA_EN ) == SCA_EN ){						// Is SCA Mode
		sca_mode = 1;
	}
	data |= RAM_ACCESS;										// Set the Memory Access Bit
	WriteEDS( iADR_CW, (USHORT)data );						// Rewrite the data
	
	// If ROI_NUM is Zero, Calcualte based on All ROIs
	if( ROI_NUM == 0 ){
		for(i=0;i<=0xFFF;i++){
			rm_lu_mask = ( 1 << ( i & 0xF ) );				// Form the Ratemeter look-Up Mask
			if(( i & 0xF ) == 0 ){
				RMLU = 0;
			}
				// Determine active ROI for only 1st 8 for Fast Mapping
				
			sca_mask= 0;
			for(j=1;j<=MAX_ROI;j++){
				if(    ( ROIRec[j].Defined == 1 )				// Must be defined
					&& ( ROIRec[j].Enable == 1 )				// Must be enabled
					&& ( i >= (int)ROIRec[j].Start )			// Must be >= Start
					&& ( i <= (int)ROIRec[j].End )){			// Must be <= End
						sca_mask |= ROIRec[j].Sca;			// Or in th SCA Value
						RMLU |= rm_lu_mask;					// Set Appropriate Bit
				}										// if ROI defined
			}											// for j
			
			//----- SCA -------------------------------------------------------------
			if( sca_mode == 1 ){
				if(( i & 0x1 ) == 0 ){
					sca &= 0xFF00;								// Clear the Lower 8 bits
				} else {
					sca &= 0x00FF;								// clear the upper 8 bits
					sca_mask = sca_mask << 8 ;					// Shift SCA Mask up by 8 
				}
				sca |= sca_mask;								// OR in SCA Mask
//				addr = i >> 1;									// Form the SCA Address
				addr = i;										// Form the SCA Address - Ver 4x23
				WriteEDS( iADR_RAMA, addr );						// Write the SCA Address
				WriteEDS( iADR_SCA_LU, sca );						// Write the SCA Data
			}
			//----- SCA --- ---------------------------------------------------------

			//----- ROI DEF ---------------------------------------------------------
			if(( i & 0xF ) == 0xF ){
				addr = i >> 4;								// Form the ROI Look-UP Address
				WriteEDS( iADR_RAMA, addr );					// Write the address
				WriteEDS( iADR_ROI_LU, RMLU );				// Write the data
			}
			//----- ROI DEF ---------------------------------------------------------
		}												// for i...
	} else if(( ROI_NUM >= 1 ) && ( ROI_NUM <= MAX_ROI )){
		// Only Calculate based on ROI[ROI_NUM]
		if(( ROIRec[ROI_NUM].Defined == 1 ) && ( ROIRec[ROI_NUM].Enable == 1 )){
			for(i=ROIRec[ROI_NUM].Start;i<=ROIRec[ROI_NUM].End;i++){

				//----- Update the ROI Look-Up Table -------------------------------------------
				addr = ((USHORT) i ) >> 4;					// form the address
				WriteEDS( iADR_RAMA, addr );					// write the address
				rm_lu_mask = 1 << ( i & 0xF );				// generate the mask bit
				data = ReadEDS( iADR_ROI_LU );				// read the data
				data |= rm_lu_mask;							// Set the Appropriate Bit
				WriteEDS( iADR_ROI_LU, (USHORT)data );			// rewrite the data
				//----- EDI-III ------------------------------------------------------------
				//----- Update the ROI Look-Up Table -------------------------------------------

				//----- Update the SCA Look-Up Table -------------------------------------------
				if( sca_mode == 1 ){
//					addr = i >> 1;									// Form the SCA Address
					addr = i;										// Form the SCA Address - Ver 4x23
					WriteEDS( iADR_RAMA, addr );				// write the address
					sca_mask = ROIRec[ROI_NUM].Sca;				// Get New SCA Data

					sca = ReadEDS( iADR_SCA_LU );				// Read the Data
						// Don't Clear the bits of the Read SCA
					if(( i & 0x1 ) == 1 ){
						sca_mask = sca_mask << 8;				// Shift SCA Mask up by 8 
					}
					sca |= sca_mask;
					WriteEDS( iADR_SCA_LU, sca );
				}
				//----- Update the SCA Look-Up Table -------------------------------------------
			}													// for i...
		}														// if defined...
	}															// ROI NUM > 0 
	// Turn-Off Auto-Address Increment
	data = ReadEDS( iADR_CW );
	data &= ~RAM_ACCESS;										// Disable the Memory Access Bit
	WriteEDS( iADR_CW, (USHORT)data );
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void ClrROI( void )
{
	int i;
	USHORT roi;
	USHORT Sav_Status;
	USHORT data;
	USHORT sca_mode;


	// Acquistion Must be stopped 
	Sav_Status = ( ReadEDS( iADR_STAT ) >> 3 ) & 0x1;		// Read the Time_Enable
	WriteEDS( iADR_TIME_STOP, 0x0 );								// Stop the Acquistion

	// Read ROI Number
	roi = ReadDPRAM( dADR_ROI_NUM );
	if( roi == 0 ){
		for(i=0;i<=MAX_ROI;i++){
			ROIRec[i].Enable = 0;				// Disable ROI
			ROIRec[i].Start = 0;				// Set Start Channel Number to 0 
			ROIRec[i].End   = 0;				// Set End Channel Number to 0 
			ROIRec[i].Defined = 0;				// Clear Defined Flag
			ROIRec[i].Sca = 0;					// Clear SCA Mask
			EDSRec.EDS_DEF_ROIS = 0;				// Clear NUmber of Defined ROIS
		}

		sca_mode = 0;
		data = ReadEDS( iADR_CW );
		data |= RAM_ACCESS;						// Enable the Memory Access Bit
		WriteEDS( iADR_CW, data );
		if(( data & SCA_EN ) == SCA_EN ){						// Is SCA Mode
			sca_mode = 1;
		}

		for(i=0;i<=0xFF;i++){
			WriteEDS( iADR_RAMA, (USHORT)i );
			WriteEDS( iADR_ROI_LU, 0x0 );
			if( sca_mode == 1 ){
				WriteEDS( iADR_SCA_LU, 0x0 );
			}
		}
		
		if( sca_mode == 1 ){
			for(i=0x100;i<=0x7FF;i++){
				WriteEDS( iADR_RAMA, (USHORT)i );
				WriteEDS( iADR_SCA_LU, 0x0 );
			}
		}

		// Turn-Off Auto-Address Increment
		data = ReadEDS( iADR_CW );
		data &= ~RAM_ACCESS;										// Enable the Memory Access Bit
		WriteEDS( iADR_CW, data );
	}
	else if(( roi >= 1 ) && ( roi <= MAX_ROI )){	// Valid ROI Number
		ROIRec[roi].Enable = 0;						// Disable ROI
		ROIRec[roi].Start = 0;						// Set Start Channel Number to 0
		ROIRec[roi].End   = 0;						// Set End Channel Number to 0
		ROIRec[roi].Defined = 0;					// Clear Defined Flag
		ROIRec[roi].Sca = 0;						// Clear SCA Mask
		EDSRec.EDS_DEF_ROIS --;						// Decrement the Defined Number of ROIS
		SetROISCARM( 0 );							// Rebuild ROI/SCA/Ratemeter Table
	}
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void DefineSCA( void )
{
	USHORT roi;
	USHORT sca;
	
	// Read ROI Number
	roi = ReadDPRAM( dADR_ROI_NUM );
	sca	= ReadDPRAM( dADR_SCA_MASK ) & 0xFF;
	if(( roi >= 1 ) && ( roi <= MAX_ROI )){		// Valid ROI Number
		ROIRec[roi].Sca = sca;
		SetROISCARM( roi );
	}
}
//-----------------------------------------------------------------------------

//----------------------------------------------------------------------
void Set_RTEM_Range( ULONG cnts )
{
	float factor;
	USHORT addr;
	ULONG tmp;
	int i;

	for(i=0;i<=5;i++){
		switch(i){
			case 0 : factor = 0.025f; addr = iADR_RTEM_CNT_10_L; break;
			case 1 : factor = 0.050f; addr = iADR_RTEM_CNT_25_L; break;
			case 2 : factor = 0.90f;  addr = iADR_RTEM_CNT_75_L; break;
			case 3 : factor = 0.95f;  addr = iADR_RTEM_CNT_90_L; break;
			case 4 : factor = 1.00f;  addr = iADR_RTEM_CNT_100_L; break;
			case 5 : factor = 1.10f;  addr = iADR_RTEM_CNT_110_L; ; break;
			default : break;
		}
		tmp = (ULONG)((float)factor * (float)cnts );
		WriteDEDS( addr, tmp );
	}
}
//----------------------------------------------------------------------

//----------------------------------------------------------------------
USHORT Get_Rtem_State()
{
	USHORT tmp;
	tmp = ( ReadEDS( iADR_STAT ) >> 8 ) & 0xF;
	return( tmp );
}
//----------------------------------------------------------------------
/*
//----------------------------------------------------------------------
void rtem_init( void )
{
	unsigned long cnts;
	
	USHORT rtem_state;

	switch( EDSRec.RTEM_Init ){
		case 0x00 : 
				break;
		case 0x01 :
				WriteEDS( iADR_RTEM_SEL, 0x1 );
				WriteEDS( iADR_RTEM_INI, 0x1234 );
				WriteEDS( iADR_RTEM_LOW_SPEED, UMS_Init_Speed );
				WriteEDS( iADR_RTEM_MED_SPEED, UMS_Init_Speed );
				WriteEDS( iADR_RTEM_HIGH_SPEED, UMS_Init_Speed );
				WriteDEDS( iADR_RTEM_CNT_10_L, 0x7FFFFFFF );
				WriteDEDS( iADR_RTEM_CNT_25_L, 0x7FFFFFFF );
				WriteDEDS( iADR_RTEM_CNT_75_L, 0x7FFFFFFF );
				WriteDEDS( iADR_RTEM_CNT_90_L, 0x7FFFFFFF );
				WriteDEDS( iADR_RTEM_CNT_100_L, 0x7FFFFFFF );
				WriteDEDS( iADR_RTEM_CNT_110_L, 0x7FFFFFFF );
				WriteDEDS( iADR_RTEM_CNT_MAX_L, 0x7FFFFFFF );
				WriteEDS(  iADR_RTEM_HCMR, 0xFFFF );				// Issue Move Out to Stop Moving
				EDSRec.RTEM_Init ++;
				break;
				
		case 0x02 :
				rtem_state = Get_Rtem_State();
				if( rtem_state == ST_RTEM_OUT ){				// If RTEM is in the "Out" position, must move in to back out
					WriteEDS( iADR_RTEM_CMD_IN, 0xFFFF );		// Move In
					EDSRec.RTEM_Init ++;
				} else {									// Not out, so start from moving out
					EDSRec.RTEM_Init = 0x30;
				}
				break;

		case 0x20 :
				WriteEDS( iADR_RTEM_CMD_OUT, 0xFFFF );			// Issue Move Out to Stop Moving
				EDSRec.RTEM_Init ++;
				break;
						
		case 0x30 :
				WriteEDS( iADR_RTEM_CMD_OUT, 0xFFFF );			// Issue Move Out Command
				EDSRec.RTEM_Target = ST_RTEM_OUT;
				EDSRec.RTEM_Init = 0x40;
				break;
					
		case 0x50 :
				cnts = 450000;								// Reset Parameters here based on 450k (9" slide )
				Set_RTEM_Range( cnts );

				// Reset the Following Parameters
				WriteDEDS( iADR_RTEM_CNT_100_L, 	0x7FFFFFFF );
				WriteDEDS( iADR_RTEM_CNT_110_L, 	0x7FFFFFFF );
				WriteDEDS( iADR_RTEM_CNT_MAX_L, 	0x7FFFFFFF );
				WriteEDS( iADR_RTEM_LOW_SPEED,	UMS_Low_Speed );
				WriteEDS( iADR_RTEM_MED_SPEED,	UMS_Med_Speed );
				WriteEDS( iADR_RTEM_HIGH_SPEED, 	UMS_High_Speed );
				WriteEDS( iADR_RTEM_CMD_IN, 0xFFFF );
				EDSRec.RTEM_Target = ST_RTEM_IN;
				EDSRec.RTEM_Init = 0x60;
				break;

		case 0x70 :
				cnts = ReadDEDS( iADR_RTEM_CNT_L );
				WriteDEDS( iADR_RTEM_CNT_MAX_L, cnts );
			
				Set_RTEM_Range( cnts );
				WriteEDS( iADR_RTEM_LOW_SPEED, UMS_Low_Speed );
				WriteEDS( iADR_RTEM_MED_SPEED, UMS_Low_Speed / 2 );
				WriteEDS( iADR_RTEM_HIGH_SPEED, UMS_Low_Speed / 4 );

				WriteEDS( iADR_RTEM_CMD_OUT, 0xFFFF );
				EDSRec.RTEM_Target = ST_RTEM_OUT;
				EDSRec.RTEM_Init = 0x80;
				break;
			
		case 0x90 :
				// Routine is Done - so Reset RTEM_INIT
				EDSRec.RTEM_Init = 0x00;
				break;

		case 0x40 :
		case 0x60 :
		case 0x80 :
				rtem_state = Get_Rtem_State();
				if( rtem_state == EDSRec.RTEM_Target ){				
					EDSRec.RTEM_Init ++;
				}
				break;

		default :
				EDSRec.RTEM_Init ++;
				break;
	}							// switch...
}
//----------------------------------------------------------------------
*/
//----------------------------------------------------------------------
void Auto_Disc( void )
{
	USHORT data;
	USHORT cps;
	float time;
	USHORT maxcps;
	USHORT mask;
	USHORT addr;
	// USHORT rps;
	static wait_cnt;
	static rps_iter;
	int i;

	switch( EDSRec.ADisc ) {
		case 0 :	// Do nothing
			break;
		case 1 :
			EDSRec.ASHBit = 11;
			WriteEDS( iADR_DISC_EN, 0x1F );						// Enable all Discriminators

			// Set All Discriminator to a particular value (1/2 Scale )
			data = 1 << EDSRec.ASHBit;
			for(i=0;i<=4;i++){
				EDSRec.ADiscVal[i] = data;
			}

			WriteEDS( iADR_DISC,   		EDSRec.ADiscVal[0] );		// Super High
			WriteEDS( iADR_DLEVEL0, 	EDSRec.ADiscVal[1] );		// High
			WriteEDS( iADR_DLEVEL1, 	EDSRec.ADiscVal[2] );		// Medium
			WriteEDS( iADR_DLEVEL2, 	EDSRec.ADiscVal[3] );		// Low
			WriteEDS( iADR_DLEVEL3, 	EDSRec.ADiscVal[4] );		// Energy
			EDSRec.ADisc ++;
			rps_iter = 0;
			break;
		case 2 :
		case 3 :
		case 4 :
		case 5 :
			EDSRec.ADisc ++;									// just wait 100ms for each count allowing disc's to settle
			break;

		case 6 :			
			// Start Acquisition
			WriteDPRAM( dADR_ANAL_MODE, EDS_ANAL_PRESET_REAL_PAMR );	// Set Preset Clock, but wait for PA_MR Trailing Edge Detect
//			WriteDPRAM( dADR_ANAL_MODE, EDS_ANAL_PRESET_REAL );		// Set Preset Clock Mode
			WriteDPRAM( dADR_TRANSFER, 0x1 );
			SetAnalMode();
			EDSRec.ADisc ++;
			wait_cnt = 50;					// 5 seconds
			break;
		case 7 :
			// Wait for Analysis to be Started
			data = ReadEDS( iADR_STAT );							// Wait for EDX ANal started
			if(( data & 0x8 ) == 0x8 ){
				EDSRec.ADisc ++;
			} else {
				wait_cnt --;
				if( wait_cnt == 0 ){
					WriteDPRAM( dADR_ANAL_MODE, EDS_ANAL_STOP );
					SetAnalMode();											// Stop the Analysis
					WriteDPRAM( dADR_ANAL_MODE, EDS_ANAL_PRESET_REAL );		// Set Preset Clock Mode
					WriteDPRAM( dADR_TRANSFER, 0x1 );						// Reset the Analysis
					SetAnalMode();
				}
			}
			break;

		case 8 :
			// do nothing, wait until preset done
			// Analysis will be off
			data = ReadEDS( iADR_STAT );							// EDX Anal is Done, so Preset is also done
			if(( data & 0x8 ) == 0 ){					
				EDSRec.ADisc ++;
			}
			break;

		case 9 :
			// Read the "Clock Time" from the DPRAM
			data = ReadDPRAM( iADR_PRESET_LO );
			data |= ( ReadDPRAM( iADR_PRESET_HI ) << 16 );
			time = ((float)data / (float)1000.0f );

			// Read the Net number of Resets for the time periord
//			data = ReadEDS( iADR_NET_RPSL );
//			data |= ( ReadEDS( iADR_NET_RPSH ) << 16 );
			
			// normalize it per unit time
//			rps = (USHORT)((float)2.0f * (float) time );
			
//			if(( data < rps ) || ( rps_iter > 5 )){
				maxcps = (USHORT)( (float)ReadDPRAM( dADR_MAX_CPS ) * time );

				mask = 1 << EDSRec.ASHBit;								// Create Mask Bit
				for(i=0;i<=4;i++){
//					data= ReadEDS( (USHORT)(iADR_SH_Disc_Cnts+i) );			// Read the Counts
					// Calculate the Average Input Count Rate 
//					cps = (USHORT)((float)data / (float)time );				// Calculate the CPS

					cps = ReadEDS( (USHORT)(iADR_SH_Disc_Cnts+i) );			// Read the Counts
					if( cps >= maxcps ){									// Value is too high
						EDSRec.ADiscVal[i] |= mask;						// So Increase DAC value by Current Bit
					} else {
						EDSRec.ADiscVal[i] &= ~mask;						// Clear the "Current" bit
					}

					if( EDSRec.ASHBit != 0 ){							// if the last bit
						data = 1 << ( EDSRec.ASHBit-1 );					// Set the Next Bit
						EDSRec.ADiscVal[i] |= data;						// Update the Value for the Next Bit
					} else {
						EDSRec.ADiscVal[0] += 100;						// Super High
						EDSRec.ADiscVal[1] += 5;							// High
						EDSRec.ADiscVal[2] += 5;							// Medium
						EDSRec.ADiscVal[3] += 5;							// Low
						EDSRec.ADiscVal[4] += 5;							// Energy
					}
					switch( i ){
						case 0 : addr = iADR_DISC;			break;
						case 1 : addr = iADR_DLEVEL0;		break;
						case 2 : addr = iADR_DLEVEL1;		break;
						case 3 : addr = iADR_DLEVEL2;		break;
						case 4 : addr = iADR_DLEVEL3;		break;
					}
					WriteEDS( addr,   EDSRec.ADiscVal[i] );	
				}														// for i...

				if( EDSRec.ASHBit == 0 ){								// if the last bit
					EDSRec.ASHBit = 0;									// clear both records
					EDSRec.ADisc = 0;	
					WriteEDS( iADR_TIME_CLR, 0xFFFF);						// Clear Clock Time and Live Time

				} else {
					EDSRec.ADisc = 2;									// Setup to Restart the process
					EDSRec.ASHBit--;									// Decrement the Bit Index
				}														// if ASHBit...

//			} else {													// rps > 2
//				EDSRec.ADisc = 2;										// Restart the process w/o changing the bit
//				rps_iter ++;
//			}		
			break;
		default :
			EDSRec.ADisc =0;
			break;
	}																// Case
}
//----------------------------------------------------------------------

//----------------------------------------------------------------------
void SetNonLinearRam( USHORT data )
{
	int i;
	USHORT tmp;
	USHORT tmp2;

	// Set the RAM to DSP Memory Access	
	tmp = ReadEDS( iADR_CW );
	tmp |= RAM_ACCESS;
	tmp &= ~SCA_EN;
	WriteEDS( iADR_CW, tmp );
	
	for(i=0;i<=0xFFF;i++){
		switch(data){
			case 0 : tmp2 = 0; break;
			case 1 :	
					if( i <= 589 ){
						tmp2 = 589 - i;
					} else {
						tmp2 = 0x100 - ( i - 589 );
					}
					if( i <= ( 589-10 )) tmp2 = 10;
					if( i >= ( 589+10 )) tmp2 = 0x100 - 10;					
					break; 
					
			case 2 :  
					if( i < 589 ) tmp2 = (( 589 - i ) * 2 ) - 1;
					if( i == 589 ) tmp2 = 0;				
					if( i > 589 ) tmp2 = 0x100 - (( 590 - i ) + 1 );
					if( i <= ( 589-10 )) tmp2 = 17;
					if( i >= ( 589+10 )) tmp2 = 0x100 - 17;
			
					break;
			default : break;
		}
	
		if(( i & 1 ) == 0 ){
			tmp = tmp2;
		} else {
			tmp |= ( tmp2 << 8 );
			WriteEDS( iADR_RAMA, (USHORT)  (i >> 1 ) );
			WriteEDS( iADR_SCA_LU, tmp );
		}
	}
		
	
	// Set the RAM to non-DSP Memory Access
	tmp = ReadEDS( iADR_CW );
	tmp &= ~RAM_ACCESS;
	WriteEDS( iADR_CW, tmp );
	
}
//----------------------------------------------------------------------

/***********************************************************************
FFT Routines, but not enough Memory on the DPP to implement
//----------------------------------------------------------------------
// Bit Reversal as required by a butterfly FFT algorithm
// Bits in a work are normally numbered:
//
//	8 7 6 5 4 3 2 1 0 (LSB)
//
// This routine will revers the order of the bits.

bit_rev(long data)
{
	int i;
	long tmp = 0;


	for(i=0;i<m_Bits;i++){
		if(( data & ( 1 << i )) != 0 ){
			tmp |= ( 1 << ((m_Bits-1)-i) );
		}
	}
	return( tmp );
}
//----------------------------------------------------------------------

//----------------------------------------------------------------------
double m_W_r( long k )
{
	float pn;
	pn = ((float)k*2.0f*Pi)/(float)m_Size;
	return( cos(pn) );
}
//----------------------------------------------------------------------

//----------------------------------------------------------------------
double m_W_i( long k )
{
	float pn;
	pn = ((float)k*2.0f*Pi)/(float)m_Size;
	return( sin(pn) );
}
//----------------------------------------------------------------------

//----------------------------------------------------------------------
void Calc_FFT( void )
{
	float tmp;
	float kpin;

	int k;
	int loop;
	int incnt;
	int p;
	int exponent;
	int shift;
	int N2;

	long brk, brnk;

	double ti, tr, xrt, xit;
	double tmpr, tmpi, tmpr1, tmpi1;

	double m_SrcBuff_r[m_Size];
	double m_SrcBuff_i[m_Size];

	p			= 0;
	N2			= m_Size >> 1;			// Size / 2
	exponent	= m_Bits;
	shift		= exponent-1;

	//----- Calculate the Constants -------------------------------
	for (k=0;k<m_Size;k++){
		brk = *(ULONG *)( DSO_BASE + k );
		brk &= 0xFFFF;
		if(( brk & 0x8000 ) != 0 ){			// negative number
			brk = -1 - ( ~brk & 0xFFFF );	// 0 - ( not(v)+1 )
		}
		m_SrcBuff_r[k] = (double) brk;
	}
	//----- Calculate the Constants -------------------------------

	//----- Main Calculation ---------------------------------------
	loop = 1;
	do{
		k=0;
		do{													// while k...
			incnt = 0;
			do{												// while incnt...
				incnt ++;
				tmp = ((float)k / (float)( 1 << shift ));
				p = bit_rev( (long)tmp );					// P=BR(int(K/2^SHIFT)
				tmpr = ((m_W_r(p) * m_SrcBuff_r[k+N2]) - ( m_W_i(p) * m_SrcBuff_i[k+N2] )) / 2.0;
				tmpi = ((m_W_r(p) * m_SrcBuff_i[k+N2]) + ( m_W_i(p) * m_SrcBuff_r[k+N2] )) / 2.0;
				tmpr1 = m_SrcBuff_r[k] / 2.0;
				tmpi1 = m_SrcBuff_i[k] / 2.0;
				m_SrcBuff_r[k+N2] = tmpr1 - tmpr;
				m_SrcBuff_i[k+N2] = tmpi1 - tmpi;
				m_SrcBuff_r[k] = tmpr1 + tmpr;
				m_SrcBuff_i[k] = tmpi1 + tmpi;
				k++;
			} while( incnt < N2 );	
			k+= N2;						// Since array is processed 2 points at a time
		} while( k < (m_Size-1) );				// while k...
		loop ++;						// Increment Loop
		N2 = N2 >> 1;					// N = N /2
		shift--;						// Decrement Shift
	} while( loop <= exponent );
	//----- Main Calculation ---------------------------------------

	//----- Post Processing ----------------------------------------
	for(k=0;k<m_Size;k++){
		brk = bit_rev(k);
		brnk = bit_rev(m_Size-k);
		tr =  (m_SrcBuff_r[brk] - m_SrcBuff_r[brnk])/2.0f;
		ti =  (m_SrcBuff_i[brk] + m_SrcBuff_i[brnk])/2.0f;
		xrt = (m_SrcBuff_r[brk] + m_SrcBuff_r[brnk])/4.0f;
		xit = (m_SrcBuff_i[brk] - m_SrcBuff_i[brnk])/4.0f;

		kpin = ( (float)k * Pi) / (float)m_Size;
		ti = xrt + (( ti * cos(kpin)) / 2.0f ) - (( tr*sin(kpin)) /2.0f );			// Result is a Double
		ti = abs(ti);
		ti *= FFT_Gain;
		brk = (long) ti;															// convert to  long
		brk &= 0xFFFF;																// mask off unwanted buts
		brk |= 0x10000;																// set Sign bit
		*(ULONG *)( DSO_BASE + k ) = brk;
	}							// for 
//	for(k=m_Size;k<0x3FFF;k++){
//		*(ULONG *)( DSO_BASE + k ) = (ULONG) 0;
//	}
	//----- Post Processing ----------------------------------------
}
//----------------------------------------------------------------------
***********************************************************************/

/********************************************************************************/
/* END OF EDSUTL.C   														    */
/********************************************************************************/
