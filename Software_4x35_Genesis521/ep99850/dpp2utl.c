/********************************************************************************/
/*																*/
/*	Module:	DPP2UTL												*/
/*																*/
/*	EDAX Inc														*/
/*																*/
/*  Author:		Michael Solazzi									*/
/*  Created:    3/25/99 												*/
/*  Board:      EDI-II 												*/
/*																*/
/*	Description:													*/
/*		This Code file contains all of the definitions and general purpose	*/
/*		routines to be used in the EDI-II Board.						*/
/*																*/
/*  History:														*/
/*		99/10/05 - MCS - Cleanup										*/
/*		99/08/16 - MCS - Updated for EDI-II Rev B						*/
/********************************************************************************/

#include "bus32.h"
#include "timer30.h"
#include "serprt30.h"
#include "defines.h"
#include "dpp2utl.h"
#include "dpp2bit.h"
#include "edsutl.h"


// Define the Serial Port Global Control Register Constants
//	write the serial port global control register 
// 		don't command receive reset;            	D[27] = 1 
//		don't command transmit reset;           	D[26] = 1 
//		disable receive interrupt;					D[25] = 0 
//  	disable receive timer interrupt;			D[24] = 0 
//													===============> C

//  	disable transmit interrupt; 				D[23] = 0 
//  	disable transmit timer interrupt;			D[22] = 0 
//  	set receive word length = 16;				D[21..20] = 01
//													===============> 1

// 		set trasmit word length = 16;				D[19..18] = 01
// 		set FSR polarity = active low;				D[17] = 1 
//  	set FSX polarity = active low;				D[16] = 1	
//													===============> 7
//  	set DR polarity = active high;				D[15] = 0 
//  	set DX polarity = active high;				D[14] = 0 
//  	set CLKR polarity = active low  (R)			D[13] = 1 
//  	set CLKX polarity = active high (R)			D[12] = 0	
//													===============> 2
//  	use standard mode receive frame sync;		D[11] = 0 
//  	use standard mode transmit frame sync;		D[10] = 0 
//  	use variable receive data rate;				D[ 9] = 1 
//  	use variable transmit data rate;			D[ 8] = 1	
//													===============> 3
//  	use External receive clock; 				D[ 7] = 0
//  	use Internal transmit clock;				D[ 6] = 1
//  	disable handshake mode;						D[ 5] = 0 
//		RSR Empty ( Read Only )						D[ 4] = 0	
//													===============> 4
//		XSR Empty ( Read Only )						D[ 3] = 0
//  	set FSXOUT = output;						D[ 2] = 1 
//		XRDY ( Read Only )							D[ 1] = 0
//		RRDY ( Read Only )							D[ 0] = 0	
//													===============> 4

#define SP_GCONTROL  0x0C172344
#define SP_RGCONTROL 0x0C173344
#define DELAY_1US					 25

extern EDS_TYPE EDSRec;
volatile DPP_TYPE DPPRec;

//------ DSP Initialization Routine --------------------------------------------------------
//------ DSP Initialization Routine --------------------------------------------------------

//------ DSP Initialization Routine --------------------------------------------------------
void DSP_Init( void )
{
	volatile BUS_REG *bus_ptr=BUS_ADDR;			// Define Pointer to Bus Control Registers
	volatile TIMER_REG *timer0_ptr = TIMER_ADDR(0);	// Define Pointer to Timer0 Register
	volatile TIMER_REG *timer1_ptr = TIMER_ADDR(1);	// Define Pointer to Timer1 Register
	volatile SERIAL_PORT_REG *sp = SERIAL_PORT_ADDR(0);	// Define Pointer to Serial Port Register

	// ****** Setup the processor access parameters ********
 
 
	// ****** IOSTRB Control Setup 
	bus_ptr->iostrb_gcontrol = 0x0; 	// not used
	
	// ****** STRB0 Control Setup
	//	Set the EXT_RAM
	//	STRB switch 					= 0, no nop between back-to-back access 
	//	STRB config 					= 0
	//	zero fill 					= 1
	//	physical memory width 			= 11, 32 bit
	//	data type size 				= 11, 32 bit
	//	BNKCMP 						= 10000
	//	WTCNT 						= 000, zero wait state
	//	SWW 							= 01, internal wait state control
	//	HIZ 							= 0, no hold
	//	NOHOLD 						= 0, respond to external hold
	//	software controlled wait state	= zero
	//**********************************************
	bus_ptr->strb0_gcontrol 	= STRB_SW_NO 
					| STRB0_CNFG 
					| NO_SIGN_EXT 
					| MEMW_32 
					| DATA_32 
					| BANK_256
					| WS_0
					| INTERNAL_RDY;
					// 0x001f1008
				
	//***** STRB1 Control Setup 
	//	zero fill 					= 1
 	//	physical memory width 			= 11, 32 bit
	//	data type size 				= 11, 32 bit
	//	BNKCMP 						= 10000
	//	WTCNT 						= 001, one wait state
	//	SWW 							= 00, external wait state control 
	//**********************************************
	bus_ptr->strb1_gcontrol 	= NO_SIGN_EXT 
					| MEMW_32 
					| DATA_32 
					| BANK_256
//					| WS_3
					| WS_2
//					| WS_1
					| INTERNAL_RDY;
					// 0x001f1028
	
	//****** TIMER0 Global Control Register Setup  	
	//	FUNC 					= 1; timer pin - generate 4 Hz clock
	//	I/O 						= 1; Output pin
	//	DATOUT 					= 0
	//	DATIN 					= 0
	//	GO 						= 1; reset and start
	//	HLD/ 					= 1; no hold
	//	C/P 						= 1; clock mode
	//	CLKSRC 					= 1; Internal clock source
	//	INV 						= 0; no inverting
	//	TSTAT 					= 0; timer status		
	//***********************************************
	timer0_ptr->gcontrol 	= FUNC 
					| I_O
					| GO 
					| HLD_ 
					| CP_
					| CLKSRC;
					// 0x1C1
		
	//****** TIMER0 Period Control Register Setup 
	timer0_ptr->period	= (ULONG) (DSP_CLK / (2*TIMER0_FREQ));	// 10 Hz;
	
	
	//****** TIMER1 Global Control Register Setup Used for LED
	// 	FUNC 		= 1 	timer pin 
	//	I/O 			= 1 	Output Pin
	//	DATOUT 		= 0		Don't Care
	//	DATIN 		= 0		Don't Care
	//	GO 			= 1;	reset and start
	//	HLD/ 		= 1; no hold
	//	C/P 			= 1; clock mode - 50% DC
	//	CLKSRC 		= 1; internal clock source 
	//	INV 			= 0; no inverting
	//	TSTAT 		= 0; timer status		
	//**********************************************
	timer1_ptr->gcontrol 	= FUNC 
					| I_O
					| GO 
					| HLD_ 
					| CP_
					| CLKSRC;	
					// 0x3C3
	
	//****** TIMER1 Period Control Register Setup
	timer1_ptr->period 	= (ULONG)( DSP_CLK / ( 2 * TIMER1_FREQ ));	// 0x7270e0;		// 2Hz		

	//----- Register Initialization for the TMS320C32
	// Clear and Enable Cache, disable OVM, disable interrupts, edge triggered interrupts
	
	// Initialize the Status Regiseter of the TMS320C32
	// Bit 14 = Interrupt Configuration, 1 = Edge, 0 = Level
	asm("	ldi     05800H,st       ");
	asm("	push	r0");
	asm("	ldi 	08830H,r0");
	asm("	lsh 	16,r0");
	asm("	or 	r0,if   	;locate the vector table starting at 0x883000");
	asm("	ldi 	0105H,ie   	;enable CPU timer0 interrupt and external interrupt 2");
	asm("	pop	r0");
	//----- Register Initialization for the TMS320C32

	asm("	or     02000H,st       ");		// Enables Global Interrupts
//	asm("	and    ~02000H,st       ");		// Disables Global Interrupts


	//***** Initialize the Seral Port ****************************************************
	//	Reset Serial port Global Register	 
	sp->gcontrol = SP_GCONTROL;			// 0x0
	
	
	//
	//	write the serial port FSX/DX/CLKX control register
	//  	refer to TMS320C3x User's Guide Pages 8-17,18 
	//  	set FSX = output;				D[9] = 1 
	//  	set FSX = serial port pin;		D[8] = 1 
	//  									D[7] = 0 
	//  									D[6] = 0 
	//  	set DX = output;				D[5] = 1 
	//  	set DXfunc = serial port pin;	D[4] = 1 
	//  									D[3] = 0 
	//  	                   				D[2] = 0 
	//  	set CLKX = output;				D[1] = 1 
	//  	set CLKX = serial port pin; 	D[0] = 1 
	//
	sp->s_x_control = 0x0333; 
	
	//  
	//	write the serial port FSR/DR/CLKR control register
	//	refer to TMS320C3x User's Guide Pages 8-18,19 
	//  	set FSR = Input 				D[9] = 0
	//  	set FSR = User Pin				D[8] = 0 
	//  	set DR = Input;					D[5] = 0 
	//  	set DR = serial port pin;		D[4] = 1 
	//  	set CLKR = Input;				D[1] = 0
	//  	set CLKR = serial port pin; 	D[0] = 1 
	//
	sp->s_r_control = 0x0111; 	// forced high
	
	// set the serial port timer to 5 MHz
	sp->s_rxt_period = 0x00030003;
  
	//	write the serial port timer control register 
	// 	refer to TMS320C3x User's Guide Pages 8-19,20 
	//  	set source of receive clock = internal;	D[9] = 1 
	//	select receive clock mode;					D[8] = 1 
	 //	don't hold the set receive counter;			D[7] = 1 
	//	start the receive counter;            		D[6] = 1 
	// 	set source of transmit clock = internal;	D[3] = 1 
	//  	select transmit clock mode; 			D[2] = 1 
	//  	don't hold the set transmit counter;	D[1] = 1 
	//  	start the transmit counter;           	D[0] = 1 
	sp->s_rxt_control = 0x03CF;
	

	sp->gcontrol = SP_GCONTROL;
	//sp->gcontrol = 0x02970344; 	// 16 bit transmit 
//***** Initialize the Seral Port ****************************************************
	
	//***** Initialize IO Flag Register ******************************
	
	// Flag1 is used for FSIN
	// INFX1 Output Value   D[7] = 0
	// OUTFX1 Output Value  D[6] = 1
	// IOFX1 as Output		D[5] = 1

	// Flag0 is used for OAE
	// INFX0 Output Value   D[3] = 0
	// OUTFX0 Output Value  D[2] = 1
	// IOFX0 as Output		D[1] = 1
	asm("	and		~0eeh, iof"); 			// Clear All of the programmable bits
	asm("	or		 66h, iof"); 			// Set the OSB_AE low and OFSIN High

	WriteGlue( iADR_GLUE_IRQ_DISABLE, 0x0 );	// De-Assert the PCI Interrupt
	DPPRec.PCI_IRQ			= 0;
//	DPPRec.TMR_IRQ			= 0;
//	DPPRec.TMR_IRQ_ENABLE	= 1;
	DPPRec.IRQ_RESET		= 0;
	DPPRec.MS100_TC		= 0;
}
//------ DSP Initialization Routine --------------------------------------------------------
 
//------- Routine to Write to DPRAM ---------------------------------------------------
void WriteDPRAM( USHORT addr, USHORT data )
{
	if(( addr & 0x100 ) == 0 ){
		*(ULONG *) ( DPRAM0_BASE + addr ) = data;
	} else {
		*(ULONG *) ( DPRAM1_BASE + addr ) = data;
	}
		// Set the EDS Address
}
//------- Routine to Write to DPRAM ---------------------------------------------------

//------- Routine to Read from the DPRAM ---------------------------------------------------
USHORT ReadDPRAM( USHORT addr )
{
	ULONG data;

	if(( addr & 0x100 ) == 0 ){
		data = *(ULONG *) ( DPRAM0_BASE + addr );	// Return the Data
	} else {
		data = *(ULONG *) ( DPRAM1_BASE + addr );	// Return the Data
	}

	// CHeck for bit 8 of address

	data &= 0xFFFF;
	return( (USHORT)data );
}
//------- Routine to Read from the DPRAM ---------------------------------------------------

//------- Routine to Write to the GLUE FPGA ------------------------------------------------
void WriteGlue( USHORT addr, USHORT data )
{
	*(ULONG *)( GLUE_BASE + addr ) = data;
}
//------- Routine to Write to the GLUE FPGA ------------------------------------------------

//------- Routine to Read from the GLUE FPGA ------------------------------------------------
USHORT ReadGlue( USHORT addr )
{
	ULONG data;
	data = *(ULONG *)(GLUE_BASE + addr );
	data &= 0xFFFF;
	return( (USHORT) data );
}
//------- Routine to Read from the GLUE FPGA ------------------------------------------------

//------- Routine to Read from the GLUE FPGA ------------------------------------------------
void WritePCIFifo( ULONG data )
{
	*(ULONG *)GLUE_BASE = data;
}
//------- Routine to Read from the GLUE FPGA ------------------------------------------------


//------- Routine to Read from the DPRAM ---------------------------------------------------
void Set_DPRAM_Bit( USHORT addr, USHORT val )
{
	USHORT data;

	data = ReadDPRAM( addr );
	data |= val;
	WriteDPRAM( addr, data );
}
//------- Routine to Read from the DPRAM ---------------------------------------------------

//*****************************************************************************

void Proc_Write_OpCode( USHORT opcode )
{
	USHORT data;

	USHORT offset;
	ULONG  datal;
	int i;


	offset = opcode & 0x03FF;
	switch( offset ) {
		case dADR_OPCODE  :							// OpCode
		case dADR_DSP_VER :							// DSP Version
			break;

		//----- PCI FIFO Write ---------------------------------------------------------
		case dADR_PCI_FIFO_LO :														// Load PCI FIFO Low
			break;

		case dADR_PCI_FIFO_HI :														// Load PCI FIFO High
			datal = ReadDPRAM( dADR_PCI_FIFO_LO );					// Read Low Bits
			datal |= ( ReadDPRAM( dADR_PCI_FIFO_HI ) << 16 );				// Read High Bits
			WritePCIFifo( datal );
			break;
		//----- PCI FIFO Write ---------------------------------------------------------

		//----- EDS Related Write ------------------------------------------------------
		case dADR_NON_LINEAR_CODE :
			data = ReadDPRAM( dADR_NON_LINEAR_CODE );
			SetNonLinearRam( data );
			break;
					
		case dADR_ANAL_MODE :
			SetAnalMode();
			break;

		case dADR_TIMECONST :
			Set_Time_Const();
			Set_Blank_Factor();
			break;

		case dADR_BLANK_FACTOR :
			Set_Blank_Factor();
			break;

		case dADR_ROI_DEF :
			DefineROI();
			break;

		case dADR_ROI_PSET :
			PSetROI();						// Roi's Changed So Updatae Look-UIP Tables ( calls to SetROISCARM )
			break;

		case dADR_ROI_CLR :
			ClrROI();						// ROI's Changed so Update Look-UP Tables ( Calls SetROISCARM )
			break;

		case dADR_SCA_DEF :
			DefineSCA();					// SCA Changed so Update Look-Up Tables ( Calls SETROISCARM )
			break;

		case dADR_AUTO_DISC :
			data = ReadDPRAM( offset );
			if( data > 0 ){
				EDSRec.ADisc = 1;
				Auto_Disc();
			} else {
				EDSRec.ADisc = 0;
			}
			break;

		case dADR_EVCH :
			Set_EvCh();
			break;


//		case dADR_RTEM_INIT :
//			data= ReadDPRAM( offset );
//			if( data != 0 ){
//				EDSRec.RTEM_Init = 1;
//			} else {
//				EDSRec.RTEM_Init = 0;
//			}
//			break;

		case dADR_PRESET_DONE_CNT :
			EDSRec.Preset_Done_Cnt = 0;
			EDSRec.PixelCount = 0;
			WriteEDS( iADR_PRESET_DONE_CNT, 0xFFFF );		// Reset FPGA as well
			WriteEDS( iADR_PRESET_DONE_CNT2, 0xFFFF );		// reset FPGA as well
			break;

		case dADR_PRESET_POINTS :
			data = ReadDPRAM( offset );
			EDSRec.Preset_Done_Points = data;
			break;
/*
		case dADR_EDS_FIFO_DATA_TEST:
			datal = FIFO_DATA_TEST();
			WriteDPRAM( offset,   (USHORT)datal );			
			WriteDPRAM( ++offset, (USHORT)(datal >> 16 ) );
			break;

		case dADR_EDS_FIFO_FLAG_TEST:
			datal = FIFO_FLAG_TEST();
			WriteDPRAM( offset,   (USHORT)datal );			
			WriteDPRAM( ++offset, (USHORT)(datal >> 16 ) );
			break;
*/
		default :
			if(( offset >= 0x100 ) && ( offset <= 0x1FF )){						// Write EDS FPGA
				switch( offset ){
					case iADR_CLIP_MIN :
						data = ReadDPRAM( offset );
						EDSRec.CLIP_MIN = data;
						WriteEDS( offset, data );
						break;
					case iADR_CLIP_MAX :
						data = ReadDPRAM( offset );
						EDSRec.CLIP_MAX = data;
						WriteEDS( offset, data );
						break;

					case iADR_DISC	:
					case iADR_EVCH0 :
					case iADR_EVCH1 :
					case iADR_BIT :
						data = ReadDPRAM( offset );
						Write8420( offset, data );
						break;

					case iADR_TIME_CLR :
						EDSRec.EDS_ISR_CNT = 0;		// Clear the Net Processed Stored Counts
						EDSRec.TMR_ISR_CNT = 0;
						WriteEDS( offset, 0xFFFF );
						for(i=0;i<=0xFFF;i++){
							*(ULONG *)( MCA_BASE + i ) = 0;
						}

						for(i=0;i<=0x1FF;i++){
							*(ULONG *)( BASELINE_BASE + i ) = 0;
						}
						break;

					case iADR_CW :
						data = ReadDPRAM( offset );										
						WriteEDS( offset, data );
						EDSRec.DSO_Enable = 0;
						if(( data & 0x40 ) == 0x40 )
							EDSRec.DSO_Enable = 1;
						break;
					//----- DSO Debug Code -----------------------------------------------------------------------
					case iADR_DSO_START :
						WriteEDS( iADR_FIFO_MR, 0x0 );						// Reset EDS FIFO
						WriteEDS( iADR_DSO_START, 0xFFFF );
						break;
					//----- DSO Debug Code -----------------------------------------------------------------------

					default :
						data = ReadDPRAM( offset );										
						WriteEDS( offset, data );
						break;
				}
			}
	}								// switch ...
}
//*********************************************************************************

//*********************************************************************************

void Proc_Read_OpCode( USHORT opcode )
{

	USHORT data;

	USHORT offset;

	offset = opcode & 0x03FF;

	switch ( offset ) {
		//----- Miscellaneouse Read Parameters ------------------------------------------
		case dADR_OPCODE : 				// OpCode
		case dADR_PCI_FIFO_LO :
		case dADR_PCI_FIFO_HI : 
			break;					// Do nothing

		case dADR_DSP_VER :  
			WriteDPRAM( offset, (ULONG)DSP_VER );
			break;

//		case dADR_RTEM_INIT :
//			WriteDPRAM( offset, EDSRec.RTEM_Init );
//			break;

		case dADR_AUTO_DISC :
			data = ( 12 - EDSRec.ASHBit ) * 100;
			data = (USHORT)((float)data / (float)12.0f );
			WriteDPRAM( offset, data );
			break;

		case dADR_PRESET_DONE_CNT :
			WriteDPRAM( offset, EDSRec.PixelCount  );
			break;

		case dADR_PRESET_POINTS :
			WriteDPRAM( offset, EDSRec.Preset_Done_Points );
			break;

		//----- Miscellaneouse Read Parameters ------------------------------------------
		default :
			if(( offset >= 0x100 ) && ( offset <= 0x1FF )){				// Read EDS FPGA
				switch( offset ){
					default :
						data = ReadEDS( offset );
						break;
				}
				WriteDPRAM( offset, data );
			}
			break;
		}
}
//********* Read *****************************************************************
//*********************************************************************************


/********************************************************************************/
/* END OF EDI3UTIL.C												*/
/********************************************************************************/


