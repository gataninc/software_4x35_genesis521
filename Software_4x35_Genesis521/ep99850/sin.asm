*************************************************************************
* SIN V5.11 - Sine function
* Copyright (c) 1992-1999 Texas Instruments Incorporated
*************************************************************************
*    Syntax:         double sin(double x);
*
*    Outputs:        Returns trignometric sine of x
*    Status:         Set from result in R0.
*    Registers used: AR0,AR1,AR2,R0,R1,R2,R3,DP,SP                      *
*    Other calls:    None                                               *
*									*
*  N = int(x / PI)							*
*									*
*  f = x - N * PI							*
*									*
*  g = f * f								*
*									*
*  R = ((((r4 * g + r3) * g + r2) * g + r1) * g)			*
*									*
*  result = f + f * R							*
*									*
*  if x < 0, result = - result						*
*									*
*  if N is even, result = - result					*
*									*
*  For x < -2 ^ 31 or x > 2 ^ 31, sin will return an incorrect answer	*
*									*
*************************************************************************

	.def _sin

	.text
;
; Initialization: get arguement, setup data page, and save registers
;
_sin:	POP	AR2		      ;return address -> AR2
        .if .REGPARM == 0
	LDI     SP,AR0
        LDF     *-AR0(0),R0           ;x -> R0
        .else
        LDF     R2,R0                 ;x -> R0
        .endif
        .if .BIGMODEL
	LDP	SIN_ADR 	      ;save data page
        .endif
	CMPF	0,R0		      ;compare x to 0
	LDIN	-1,AR1		      ;if x < 0, sign = - 1
	LDINN	1,AR1		      ;if x >= 0, sign = 1
	LDI	@SIN_ADR,AR0	      ;load data address in AR0
	ABSF	R0		      ;Y = absolute value of x
	MPYF	*AR0++,R0,R1	      ;Y / PI -> R1
	FIX	R1,R2		      ;N = integer R1
	FLOAT	R2		      ;XN = float N
	SUBF	R2,R1		      ;R1 - XN -> R1
	CMPF	0.5,R1		      ;compare R1 to 0.5
	LDFNN	1.0,R1		      ;if R1 >= 0.5, 1 -> R1
	LDFN	0.0,R1		      ;if R1 < 0.5, 0 -> R1
	ADDF	R2,R1		      ;R2 + R1 to round R2
	FIX	R1,R2		      ;XN = integer R1
	TSTB	1,R2		      ;logical AND XN and 1
	LDINZ	-1,R3		      ;if XN is odd, - 1 -> R3
	LDIZ	1,R3		      ;if XN is even, 1 -> R3
	MPYI	R3,AR1		      ;sign *= R3
	MPYF	*AR0++,R1,R3	      ;3.140625 * XN -> R3
	SUBF	R3,R0		      ;Y - R3 -> R3
	MPYF	*AR0++,R1,R2	      ;9.67653589796e-4 * XN -> R2
	SUBF	R2,R0		      ;f = Y - XN * PI
	MPYF	R0,R0,R3	      ;g = f * f
	MPYF	*AR0++,R3,R2	      ;0.2601903036e-5 * g -> R2
	ADDF	*AR0++,R2	      ;-0.1980741872e-3 + R2 -> R2
	MPYF	R3,R2		      ;R2 * g -> R2
	ADDF	*AR0++,R2	      ;0.8333025739e-2 + R2 -> R2
	MPYF	R3,R2		      ;R2 * g -> R2
	ADDF	*AR0++,R2	      ;-0.1666665668 + R2 -> R2
	MPYF	R3,R2		      ;g * R2 -> R2
	MPYF	R0,R2		      ;f * R2 -> R2
	BD	AR2		      ;return from routine
	ADDF	R0,R2		      ;result = f + R2
	FLOAT	AR1,R1		      ;sign -> R1
	MPYF	R2,R1,R0	      ;result *= sign

***********************************************************************
*  DEFINE CONSTANTS
***********************************************************************
 .if .BIGMODEL
SIN_ADR:        .word   SIN
 .else
		.bss SIN_ADR,1
		.sect ".cinit"
		.word 1, SIN_ADR, SIN
		.text
 .endif

SIN             .float  0.318309886183790
		.float	3.140625
		.float	9.67653589793e-4
		.float	0.2601903036e-5
		.float -0.1980741872e-3
		.float	0.8333025139e-2
		.float -0.1666665668

	.end
