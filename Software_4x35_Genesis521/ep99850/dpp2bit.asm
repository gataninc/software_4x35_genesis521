;******************************************************************************
;* TMS320C3x/4x ANSI C Code Generator                            Version 5.11 *
;* Date/Time created: Thu May 24 17:11:49 2007                                *
;******************************************************************************
	.regalias	; enable floating point register aliases
fp	.set	ar3
FP	.set	ar3
;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Silicon Info       : C32 Revision PG1                                    *
;*   Optimization       : Conservatively Choose Speed over Size               *
;*   Memory             : Small Memory Model                                  *
;*   Float-to-Int       : Fast Conversions (round toward -inf)                *
;*   Multiply           : in Software (32 bits)                               *
;*   Memory Info        : Unmapped Memory Exists                              *
;*   Repeat Loops       : Use RPTS and/or RPTB                                *
;*   Calls              : Normal Library ASM calls                            *
;*   Debug Info         : Optimized TI Debug Information                      *
;******************************************************************************
;	c:\c3xtools\bin\ac30.exe -v32 dpp2bit.c C:\DOCUME~1\MSOLAZ~1.AME\LOCALS~1\Temp\dpp2bit.if 
	.file	"dpp2bit.c"
	.file	"bus32.h"
	.stag	.fake1,32
	.member	_holdst,0,14,18,1
	.member	_nohold,1,14,18,1
	.member	_hiz,2,14,18,1
	.member	_sww,3,14,18,2
	.member	_wtcnt,5,14,18,3
	.member	_bnkcmp,8,14,18,5
	.member	_r_131415,13,14,18,3
	.member	_datasize,16,14,18,2
	.member	_memwidth,18,14,18,2
	.member	_signext,20,14,18,1
	.member	_strbcnfg,21,14,18,1
	.member	_strbsw,22,14,18,1
	.member	_r_rest,23,14,18,9
	.eos
	.utag	.fake0,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake1
	.eos
	.sym	_STRB0_BUS_CONTROL,0,9,13,32,.fake0
	.stag	.fake3,32
	.member	_r_012,0,14,18,3
	.member	_sww,3,14,18,2
	.member	_wtcnt,5,14,18,3
	.member	_bnkcmp,8,14,18,5
	.member	_r_131415,13,14,18,3
	.member	_datasize,16,14,18,2
	.member	_memwidth,18,14,18,2
	.member	_signext,20,14,18,1
	.member	_r_rest,21,14,18,11
	.eos
	.utag	.fake2,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake3
	.eos
	.sym	_STRB1_BUS_CONTROL,0,9,13,32,.fake2
	.stag	.fake5,32
	.member	_r_012,0,14,18,3
	.member	_sww,3,14,18,2
	.member	_wtcnt,5,14,18,3
	.member	_r_rest,8,14,18,24
	.eos
	.utag	.fake4,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake5
	.eos
	.sym	_IOSTRB_BUS_CONTROL,0,9,13,32,.fake4
	.stag	.fake6,288
	.member	__ioctrl,0,9,8,32,.fake4
	.member	_reserved1,32,62,8,96,,3
	.member	__s0ctrl,128,9,8,32,.fake0
	.member	_reserved2,160,62,8,96,,3
	.member	__s1ctrl,256,9,8,32,.fake2
	.eos
	.sym	_BUS_REG,0,8,13,288,.fake6
	.file	"timer30.h"
	.stag	.fake8,32
	.member	_func,0,14,18,1
	.member	_i_o,1,14,18,1
	.member	_datout,2,14,18,1
	.member	_datin,3,14,18,1
	.member	_r_45,4,14,18,2
	.member	_go,6,14,18,1
	.member	_hld_,7,14,18,1
	.member	_cp_,8,14,18,1
	.member	_clksrc,9,14,18,1
	.member	_inv,10,14,18,1
	.member	_tstat,11,14,18,1
	.member	_r_rest,12,14,18,20
	.eos
	.utag	.fake7,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake8
	.eos
	.sym	_TIMER_CONTROL,0,9,13,32,.fake7
	.stag	.fake9,288
	.member	__gctrl,0,9,8,32,.fake7
	.member	_reserved1,32,62,8,96,,3
	.member	_counter,128,14,8,32
	.member	_reserved2,160,62,8,96,,3
	.member	_period,256,14,8,32
	.eos
	.sym	_TIMER_REG,0,8,13,288,.fake9
	.file	"serprt30.h"
	.stag	.fake11,32
	.member	_rrdy,0,14,18,1
	.member	_xrdy,1,14,18,1
	.member	_fsxout,2,14,18,1
	.member	_xsrempty,3,14,18,1
	.member	_rsrfull,4,14,18,1
	.member	_hs,5,14,18,1
	.member	_xclksrce,6,14,18,1
	.member	_rclksrce,7,14,18,1
	.member	_xvaren,8,14,18,1
	.member	_rvaren,9,14,18,1
	.member	_xfsm,10,14,18,1
	.member	_rfsm,11,14,18,1
	.member	_clkxp,12,14,18,1
	.member	_clkrp,13,14,18,1
	.member	_dxp,14,14,18,1
	.member	_drp,15,14,18,1
	.member	_fsxp,16,14,18,1
	.member	_fsrp,17,14,18,1
	.member	_xlen,18,14,18,2
	.member	_rlen,20,14,18,2
	.member	_xtint,22,14,18,1
	.member	_xint,23,14,18,1
	.member	_rtint,24,14,18,1
	.member	_rint,25,14,18,1
	.member	_xreset,26,14,18,1
	.member	_rreset,27,14,18,1
	.member	_r_rest,28,14,18,4
	.eos
	.utag	.fake10,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake11
	.eos
	.sym	_SERIAL_PORT_CONTROL,0,9,13,32,.fake10
	.stag	.fake13,32
	.member	_clkfunc,0,14,18,1
	.member	_clki_o,1,14,18,1
	.member	_clkdato,2,14,18,1
	.member	_clkdati,3,14,18,1
	.member	_dfunc,4,14,18,1
	.member	_di_o,5,14,18,1
	.member	_ddatout,6,14,18,1
	.member	_ddatin,7,14,18,1
	.member	_fsfunc,8,14,18,1
	.member	_fsi_o,9,14,18,1
	.member	_fsdatout,10,14,18,1
	.member	_fsdatin,11,14,18,1
	.member	_r_rest,12,14,18,20
	.eos
	.utag	.fake12,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake13
	.eos
	.sym	_RX_PORT_CONTROL,0,9,13,32,.fake12
	.stag	.fake15,32
	.member	_xgo,0,14,18,1
	.member	_xhld_,1,14,18,1
	.member	_xcp_,2,14,18,1
	.member	_xclksrc,3,14,18,1
	.member	_r_4,4,14,18,1
	.member	_xtstat,5,14,18,1
	.member	_rgo,6,14,18,1
	.member	_rhld_,7,14,18,1
	.member	_rcp_,8,14,18,1
	.member	_rclksrc,9,14,18,1
	.member	_r_10,10,14,18,1
	.member	_rtstat,11,14,18,1
	.member	_r_rest,12,14,18,20
	.eos
	.utag	.fake14,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake15
	.eos
	.sym	_RX_TIMER_CONTROL,0,9,13,32,.fake14
	.stag	.fake17,32
	.member	_x_counter,0,14,18,16
	.member	_r_counter,16,14,18,16
	.eos
	.utag	.fake16,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake17
	.eos
	.sym	_RX_TIMER_COUNTER,0,9,13,32,.fake16
	.stag	.fake19,32
	.member	_x_period,0,14,18,16
	.member	_r_period,16,14,18,16
	.eos
	.utag	.fake18,32
	.member	__intval,0,14,11,32
	.member	__bitval,0,8,11,32,.fake19
	.eos
	.sym	_RX_TIMER_PERIOD,0,9,13,32,.fake18
	.stag	.fake20,512
	.member	__gctrl,0,9,8,32,.fake10
	.member	_reserved1,32,14,8,32
	.member	__xctrl,64,9,8,32,.fake12
	.member	__rctrl,96,9,8,32,.fake12
	.member	__rxtctrl,128,9,8,32,.fake14
	.member	__rxtcounter,160,9,8,32,.fake16
	.member	__rxtperiod,192,9,8,32,.fake18
	.member	_reserved2,224,14,8,32
	.member	_x_data,256,14,8,32
	.member	_reserved3,288,62,8,96,,3
	.member	_r_data,384,14,8,32
	.member	_reserved4,416,62,8,96,,3
	.eos
	.sym	_SERIAL_PORT_REG,0,8,13,512,.fake20
	.file	"defines.h"
	.sym	_UCHAR,0,12,13,32
	.sym	_USHORT,0,13,13,32
	.sym	_ULONG,0,15,13,32
	.file	"dpp2utl.h"
	.stag	.fake21,224
	.member	_PCI_IRQ,0,12,8,32
	.member	_IRQ_RESET,32,13,8,32
	.member	_MS100_TC,64,13,8,32
	.member	_OpCodeAck,96,13,8,32
	.member	_PCIIRQ_Cnt,128,15,8,32
	.member	_OpCode,160,13,8,32
	.member	_Set_PCI_IRQ,192,13,8,32
	.eos
	.sym	_DPP_TYPE,0,8,13,224,.fake21
	.file	"edsutl.h"
	.stag	.fake22,1088
	.member	_FPGA_Preset_Done,0,13,8,32
	.member	_EDSXFer,32,13,8,32
	.member	_EDS_Stat,64,13,8,32
	.member	_DSO_PENDING,96,13,8,32
	.member	_TMR_ISR_CNT,128,15,8,32
	.member	_EDS_IRQ,160,12,8,32
	.member	_EDS_ISR_CNT,192,15,8,32
	.member	_CLIP_MIN,224,13,8,32
	.member	_CLIP_MAX,256,13,8,32
	.member	_Preset_Mode,288,13,8,32
	.member	_Preset_Done_Cnt,320,13,8,32
	.member	_Preset_Done_Points,352,13,8,32
	.member	_DSO_Index,384,13,8,32
	.member	_FFT_Mode,416,13,8,32
	.member	_DSO_Enable,448,13,8,32
	.member	_PixelCount,480,13,8,32
	.member	_EDS_Mode,512,13,8,32
	.member	_EDS_DEF_ROIS,544,13,8,32
	.member	_Max_Fir_Cnt,576,13,8,32
	.member	_Max_Fir,608,61,8,128,,4
	.member	_RTEM_Init,736,13,8,32
	.member	_RTEM_Target,768,13,8,32
	.member	_ADisc,800,13,8,32
	.member	_ASHBit,832,13,8,32
	.member	_ADiscVal,864,61,8,160,,5
	.member	_EDX_ANAL,1024,13,8,32
	.member	_Reset_FIR,1056,13,8,32
	.eos
	.sym	_EDS_TYPE,0,8,13,1088,.fake22
	.stag	.fake23,160
	.member	_Start,0,13,8,32
	.member	_End,32,13,8,32
	.member	_Sca,64,13,8,32
	.member	_Enable,96,12,8,32
	.member	_Defined,128,12,8,32
	.eos
	.sym	_ROI_TYPE,0,8,13,160,.fake23
	.file	"dpp2bit.h"
	.file	"dpp2bit.c"
	.sect	 ".text"

	.global	_DPP_BIT_Test
	.sym	_DPP_BIT_Test,_DPP_BIT_Test,32,2,0
	.func	37
;******************************************************************************
;* FUNCTION NAME: _DPP_BIT_Test                                               *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,r4,r5,ar0,fp,ir0,sp,st                        *
;*   Regs Saved         : r4,r5                                               *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 6 Auto + 2 SOE = 10 words         *
;******************************************************************************
_DPP_BIT_Test:
	.sym	_i,1,4,1,32
	.sym	_done,2,4,1,32
	.sym	_data,3,13,1,32
	.sym	_data1,4,15,1,32
	.sym	_data2,5,15,1,32
	.sym	_addr,6,13,1,32
	.line	1
;----------------------------------------------------------------------
;  37 | void DPP_BIT_Test()                                                    
;  39 | int i;                                                                 
;  40 | int done;                                                              
;  41 | USHORT data;                                                           
;  42 | ULONG data1;                                                           
;  43 | ULONG data2;                                                           
;  44 | USHORT addr;                                                           
;  46 | //**** BIT: Start Sequence ********************************************
;     | *************                                                          
;  47 | //----- Start of Initialization Tests ---------------------------------
;     | -------------                                                          
;  48 | // Now Wait for Driver to Write all a PCI's Device ID to DPPRAM before
;     | continuing                                                             
;  49 | // Driver must determine that the DSP has booted before continuing with
;     |  the tests                                                             
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      6,sp
        push      r4
        push      r5
	.line	14
;----------------------------------------------------------------------
;  50 | i = MAX_LOOP;                                                          
;  51 | do{                                                                    
;----------------------------------------------------------------------
        ldiu      @CL1,r0
        sti       r0,*+fp(1)
	.line	16
;----------------------------------------------------------------------
;  52 | data = ReadDPRAM( dADR_DSP_VER );                                      
;----------------------------------------------------------------------
        ldiu      2,r0
L2:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	17
;----------------------------------------------------------------------
;  53 | data1 = data & 0xFFF0;                                                 
;  54 | #if INITIALIZE_TIME_OUT == 0                                           
;----------------------------------------------------------------------
        and       65520,r0
        sti       r0,*+fp(4)
	.line	19
;----------------------------------------------------------------------
;  55 | i = 2;                                                                 
;  56 | #endif                                                                 
;----------------------------------------------------------------------
        ldiu      2,r0
        sti       r0,*+fp(1)
	.line	22
;----------------------------------------------------------------------
;  58 | while(( data1 != 0x1970 ) && ( --i > 0 ));                             
;  61 | // First test the DPRAM and Store the results, after the test          
;  62 | // update the DPRAM with the Results                                   
;----------------------------------------------------------------------
        ldiu      *+fp(4),r0
        cmpi      6512,r0
        beqd      L4
	nop
	nop
        ldine     1,r0
;*      Branch Occurs to L4 
        subri     *+fp(1),r0
        bgtd      L2
        sti       r0,*+fp(1)
	nop
        ldigt     2,r0
;*      Branch Occurs to L2 
L4:        
	.line	27
;----------------------------------------------------------------------
;  63 | data1 = DPRAM_DATA_TEST();                                             
;----------------------------------------------------------------------
        call      _DPRAM_DATA_TEST
                                        ; Call Occurs
        sti       r0,*+fp(4)
	.line	28
;----------------------------------------------------------------------
;  64 | data2 = DPRAM_ADDRESS_TEST();                                          
;  66 | // Now Clear the DPRAM                                                 
;----------------------------------------------------------------------
        call      _DPRAM_ADDRESS_TEST
                                        ; Call Occurs
        sti       r0,*+fp(5)
	.line	31
;----------------------------------------------------------------------
;  67 | for( i=0; i<=0x1FF;i++){                                               
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
        ldiu      0,r4
        cmpi      511,r0
        bgtd      L8
	nop
        ldigt     16432,r0
        ldigt     62,r1
;*      Branch Occurs to L8 
	.line	32
;----------------------------------------------------------------------
;  68 | WriteDPRAM( (USHORT)i, 0x0 );                                          
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
L6:        
        push      r4
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	31
        ldiu      1,r0
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      511,r0
        bled      L6
        ldile     *+fp(1),r0
	nop
        ldile     *+fp(1),r0
;*      Branch Occurs to L6 
	.line	35
;----------------------------------------------------------------------
;  71 | WriteDPRAM( 0x003E, (USHORT)DSP_VER );                  // Copy the DSP
;     |  Version to the DPRAM                                                  
;----------------------------------------------------------------------
        ldiu      16432,r0
        ldiu      62,r1
L8:        
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	36
;----------------------------------------------------------------------
;  72 | WriteDPRAM( 0x003F, (USHORT)ReadEDS(iADR_ID));  // Copy the EDS FPGA Ve
;     | rsion to the DPRAM                                                     
;----------------------------------------------------------------------
        ldiu      256,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        ldiu      63,r1
        subi      1,sp
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	37
;----------------------------------------------------------------------
;  73 | WriteDPRAM( dADR_SCA_GEN, 0x0 );                                       
;----------------------------------------------------------------------
        ldiu      33,r0
        ldiu      0,r1
        push      r1
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	38
;----------------------------------------------------------------------
;  74 | WriteDPRAM( dADR_DAC_GEN, 0x0 );                                       
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        ldiu      34,r1
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	40
;----------------------------------------------------------------------
;  76 | done=0;                                                                
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(2)
	.line	41
;----------------------------------------------------------------------
;  77 | i=0;                                                                   
;  78 | do{                                                                    
;  79 |         switch(i) {                                                    
;----------------------------------------------------------------------
        bud       L38
        sti       r0,*+fp(1)
        ldiu      r0,ir0
        cmpi      30,ir0
;*      Branch Occurs to L38 
L9:        
	.line	44
;----------------------------------------------------------------------
;  80 | case 0x00: data1 = data1;                       break;  // DPRAM Data T
;     | est                                                                    
;----------------------------------------------------------------------
        bud       L43
        ldiu      *+fp(1),r0
        ash       1,r0
        addi      64,r0                 ; Unsigned
;*      Branch Occurs to L43 
L10:        
	.line	45
;----------------------------------------------------------------------
;  81 | case 0x01: data1 = data2;                       break;  // DPRAM Addres
;     | s Test                                                                 
;----------------------------------------------------------------------
        bud       L41
        ldiu      *+fp(5),r0
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
;*      Branch Occurs to L41 
L11:        
	.line	46
;----------------------------------------------------------------------
;  82 | case 0x02: data1 = FPGA_TEST(0);                break;  // GLUE Test Re
;     | gister                                                                 
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        call      _FPGA_TEST
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L12:        
	.line	47
;----------------------------------------------------------------------
;  83 | case 0x03: data1 = FPGA_TEST(1);                break;  // EDS FPGA TES
;     | T                                                                      
;----------------------------------------------------------------------
        ldiu      1,r0
        push      r0
        call      _FPGA_TEST
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L13:        
	.line	48
;----------------------------------------------------------------------
;  84 | case 0x04: data1 = FIFO_DATA_TEST();                                   
;  85 |                 #if NO_FIFO_TEST == TRUE                               
;----------------------------------------------------------------------
        call      _FIFO_DATA_TEST
                                        ; Call Occurs
	.line	50
;----------------------------------------------------------------------
;  86 | data1 = 0;                                                             
;  87 | #endif                                                                 
;----------------------------------------------------------------------
	.line	52
;----------------------------------------------------------------------
;  88 | break;  // EDS FIFO DATA                                               
;----------------------------------------------------------------------
        bud       L40
        sti       r0,*+fp(4)
        ldiu      0,r0
        sti       r0,*+fp(4)
;*      Branch Occurs to L40 
L14:        
	.line	53
;----------------------------------------------------------------------
;  89 | case 0x05: data1 = FIFO_FLAG_TEST();                                   
;  90 |                 #if NO_FIFO_TEST == TRUE                               
;----------------------------------------------------------------------
        call      _FIFO_FLAG_TEST
                                        ; Call Occurs
	.line	55
;----------------------------------------------------------------------
;  91 | data1 = 0;                                                             
;  92 | #endif                                                                 
;----------------------------------------------------------------------
	.line	57
;----------------------------------------------------------------------
;  93 | break;  // E-F  EDS FIFO FLAG                                          
;----------------------------------------------------------------------
        bud       L40
        sti       r0,*+fp(4)
        ldiu      0,r0
        sti       r0,*+fp(4)
;*      Branch Occurs to L40 
L15:        
	.line	58
;----------------------------------------------------------------------
;  94 | case 0x06: data1 = Ram_Data_Test(0);    break;  // ROI/SCA RAm Data    
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        call      _Ram_Data_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L16:        
	.line	59
;----------------------------------------------------------------------
;  95 | case 0x07: data1 = Ram_Address_Test(0); break;  // ROI/SCA Ram Address 
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        call      _Ram_Address_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L17:        
	.line	60
;----------------------------------------------------------------------
;  96 | case 0x08: data1 = Ram_Memory_Test(0);  break;          // ROI/SCA Ram
;     | Memory                                                                 
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        call      _Ram_Memory_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L18:        
	.line	61
;----------------------------------------------------------------------
;  97 | case 0x09: data1 = Ram_Data_Test(1);    break;  // ROI DEF RAm Data    
;----------------------------------------------------------------------
        ldiu      1,r0
        push      r0
        call      _Ram_Data_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L19:        
	.line	62
;----------------------------------------------------------------------
;  98 | case 0x0A: data1 = Ram_Address_Test(1); break;  // ROI DEF Ram Address 
;----------------------------------------------------------------------
        ldiu      1,r0
        push      r0
        call      _Ram_Address_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L20:        
	.line	63
;----------------------------------------------------------------------
;  99 | case 0x0B: data1 = Ram_Memory_Test(1);  break;  // ROI DEF Ram Memory  
; 101 | case 0x0C:
;     |  // Fine Gain RAM Data           - No longer used                      
; 102 | case 0x0D:
;     |  // Fine Gain RAM Address        - No Longer Used                      
; 103 | case 0x0E:
;     |  // Fine Gain RAM Memory         - No Longer U                         
; 104 | case 0x0F:
;     |  // Zero RAM Data                        - No longer used              
; 105 | case 0x10:
;     |  // Zero RAM Address                     - No Longer Used              
; 106 | case 0x11:
;     |  // Zero RAM Memory                      - No Longer U                 
;----------------------------------------------------------------------
        ldiu      1,r0
        push      r0
        call      _Ram_Memory_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L21:        
	.line	71
;----------------------------------------------------------------------
; 107 | data1 = 0;                                                             
;----------------------------------------------------------------------
	.line	72
;----------------------------------------------------------------------
; 108 | break;                                                                 
;----------------------------------------------------------------------
        bud       L41
        ldiu      0,r0
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
;*      Branch Occurs to L41 
L22:        
	.line	74
;----------------------------------------------------------------------
; 110 | case 0x12: data1 = Dac_Test(0x0);               break;
;     |          // Disc 1                                                     
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        call      _Dac_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L23:        
	.line	75
;----------------------------------------------------------------------
; 111 | case 0x13: data1 = Dac_Test(0x1);               break;
;     |          // Disc 2                                                     
;----------------------------------------------------------------------
        ldiu      1,r0
        push      r0
        call      _Dac_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L24:        
	.line	76
;----------------------------------------------------------------------
; 112 | case 0x14: data1 = Dac_Test(0x2);               break;
;     |          // eV/ch                                                      
;----------------------------------------------------------------------
        ldiu      2,r0
        push      r0
        call      _Dac_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L25:        
	.line	77
;----------------------------------------------------------------------
; 113 | case 0x15: data1 = Dac_Test(0x3);               break;
;     |          // BIT                                                        
;----------------------------------------------------------------------
        ldiu      3,r0
        push      r0
        call      _Dac_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L26:        
	.line	78
;----------------------------------------------------------------------
; 114 | case 0x16: data1 = Dac_Test(0x4);               break;
;     |          // Post Amp 1X Gain                                           
;----------------------------------------------------------------------
        ldiu      4,r0
        push      r0
        call      _Dac_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L27:        
	.line	79
;----------------------------------------------------------------------
; 115 | case 0x17: data1 = Dac_Test(0x5);               break;
;     |          // Post Amp 2X Gain                                           
;----------------------------------------------------------------------
        ldiu      5,r0
        push      r0
        call      _Dac_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L28:        
	.line	80
;----------------------------------------------------------------------
; 116 | case 0x18: data1 = Dac_Test(0x6);               break;
;     |          // Coarse Gain                                                
;----------------------------------------------------------------------
        ldiu      6,r0
        push      r0
        call      _Dac_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L29:        
	.line	81
;----------------------------------------------------------------------
; 117 | case 0x19: data1 = Dac_Test(0x7);               break;
;     |          // Bessel Filter                                              
;----------------------------------------------------------------------
        ldiu      7,r0
        push      r0
        call      _Dac_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L30:        
	.line	82
;----------------------------------------------------------------------
; 118 | case 0x1A: data1 = Dac_Test(0x8);               break;
;     |          // Anti-Alias Filter                                          
;----------------------------------------------------------------------
        ldiu      8,r0
        push      r0
        call      _Dac_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L31:        
	.line	83
;----------------------------------------------------------------------
; 119 | case 0x1B: data1 = Dac_Test(0x9);               break;
;     |          // ADC Positive Input                                         
;----------------------------------------------------------------------
        ldiu      9,r0
        push      r0
        call      _Dac_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L32:        
	.line	84
;----------------------------------------------------------------------
; 120 | case 0x1C: data1 = Dac_Test(0xA);               break;
;     |          // ADC Negative Input                                         
;----------------------------------------------------------------------
        ldiu      10,r0
        push      r0
        call      _Dac_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L33:        
	.line	85
;----------------------------------------------------------------------
; 121 | case 0x1D: data1 = Dac_Test(0xB);               break;
;     |          // Disc Ev/Ch                                                 
;----------------------------------------------------------------------
        ldiu      11,r0
        push      r0
        call      _Dac_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L34:        
	.line	86
;----------------------------------------------------------------------
; 122 | case 0x1E: data1 = Dac_Test(0xC);               break;
;     |          // Disc Inv                                                   
; 123 | // Could keep adding BIT Tests here.                                   
;----------------------------------------------------------------------
        ldiu      12,r0
        push      r0
        call      _Dac_Test
                                        ; Call Occurs
        subi      1,sp
        bud       L42
        sti       r0,*+fp(4)
        ldiu      *+fp(1),r0
        ash       1,r0
;*      Branch Occurs to L42 
L35:        
	.line	88
;----------------------------------------------------------------------
; 124 | default : done = 1; break;                                             
;----------------------------------------------------------------------
        bud       L41
        ldiu      1,r0
        sti       r0,*+fp(2)
        ldiu      *+fp(1),r0
;*      Branch Occurs to L41 
	.line	43
L37:        
        cmpi      30,ir0
L38:        
        bhid      L35
	nop
	nop
        ldils     @CL2,ar0
;*      Branch Occurs to L35 
        ldiu      *+ar0(ir0),r0
        bu        r0

	.sect	".text"
SW0:	.word	L9	; 0
	.word	L10	; 1
	.word	L11	; 2
	.word	L12	; 3
	.word	L13	; 4
	.word	L14	; 5
	.word	L15	; 6
	.word	L16	; 7
	.word	L17	; 8
	.word	L18	; 9
	.word	L19	; 10
	.word	L20	; 11
	.word	L21	; 12
	.word	L21	; 13
	.word	L21	; 14
	.word	L21	; 15
	.word	L21	; 16
	.word	L21	; 17
	.word	L22	; 18
	.word	L23	; 19
	.word	L24	; 20
	.word	L25	; 21
	.word	L26	; 22
	.word	L27	; 23
	.word	L28	; 24
	.word	L29	; 25
	.word	L30	; 26
	.word	L31	; 27
	.word	L32	; 28
	.word	L33	; 29
	.word	L34	; 30
	.sect	".text"
;*      Branch Occurs to r0 
L40:        
	.line	90
;----------------------------------------------------------------------
; 126 | addr = ( i << 1 ) + 0x40;                                              
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
L41:        
        ash       1,r0
L42:        
        addi      64,r0                 ; Unsigned
L43:        
        sti       r0,*+fp(6)
	.line	91
;----------------------------------------------------------------------
; 127 | WriteDPRAM( addr, (USHORT)data1);                                      
;----------------------------------------------------------------------
        ldiu      *+fp(4),r0
        push      r0
        ldiu      *+fp(6),r0
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	92
;----------------------------------------------------------------------
; 128 | addr ++;                                                               
;----------------------------------------------------------------------
        ldiu      1,r0
        addi      *+fp(6),r0            ; Unsigned
        sti       r0,*+fp(6)
	.line	93
;----------------------------------------------------------------------
; 129 | WriteDPRAM( addr, (USHORT)(data1 >> 16 ));                             
;----------------------------------------------------------------------
        ldiu      *+fp(4),r0
        lsh       -16,r0
        push      r0
        ldiu      *+fp(6),r0
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	94
;----------------------------------------------------------------------
; 130 | i++;                                                                   
;----------------------------------------------------------------------
        ldiu      1,r0
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
	.line	95
;----------------------------------------------------------------------
; 131 | data1 = 0;                                                             
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(4)
	.line	96
;----------------------------------------------------------------------
; 132 | } while( done == 0 );                                                  
; 133 | //---------------------------------------------------------------------
;     | --------------------------                                             
; 134 | // End of Initialization Test                                          
; 135 | // ADR_DSP_VER i s used instead of ADR_OPCODE to prevent any interrupts
;----------------------------------------------------------------------
        ldi       *+fp(2),r0
        beqd      L37
	nop
	nop
        ldieq     *+fp(1),ir0
;*      Branch Occurs to L37 
	.line	100
;----------------------------------------------------------------------
; 136 | WriteDPRAM( dADR_DSP_VER, 0x1234 );
;     |          // Write all 1234 to DPRAM at Loc 0                           
; 138 | // Now Wait for Driver to Write all 1's to Loc 0 of DPPRAM before conti
;     | nuing                                                                  
;----------------------------------------------------------------------
        ldiu      4660,r1
        ldiu      2,r0
        push      r1
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	103
;----------------------------------------------------------------------
; 139 | i = MAX_LOOP;                                                          
; 140 | do{                                                                    
;----------------------------------------------------------------------
        ldiu      @CL1,r0
        sti       r0,*+fp(1)
	.line	105
;----------------------------------------------------------------------
; 141 | data = ReadDPRAM( dADR_DSP_VER );                                      
; 142 | #if INITIALIZE_TIME_OUT == 0                                           
;----------------------------------------------------------------------
        ldiu      2,r0
L45:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	107
;----------------------------------------------------------------------
; 143 | i = 2;                                                                 
; 144 | #endif                                                                 
;----------------------------------------------------------------------
        ldiu      2,r0
        sti       r0,*+fp(1)
	.line	110
;----------------------------------------------------------------------
; 146 | while(( data == 0x1234 ) && ( --i > 0 ));                              
; 148 | //---------------------------------------------------------------------
;     | --------------------------                                             
; 149 | // Now allow for the driver to perform the PCI FIFO Tests              
; 150 | // Reset the PCI FIFO                                                  
;----------------------------------------------------------------------
        ldiu      *+fp(3),r0
        cmpi      4660,r0
        bned      L48
	nop
	nop
        ldine     3,r0
;*      Branch Occurs to L48 
        ldiu      1,r0
        subri     *+fp(1),r0
        bgtd      L45
        sti       r0,*+fp(1)
	nop
        ldigt     2,r0
;*      Branch Occurs to L45 
	.line	115
;----------------------------------------------------------------------
; 151 | data = ReadGlue( iADR_GLUE_ID );                                       
;----------------------------------------------------------------------
        ldiu      3,r0
L48:        
        push      r0
        call      _ReadGlue
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	116
;----------------------------------------------------------------------
; 152 | i = 100;                                                               
; 153 | do{                                                                    
;----------------------------------------------------------------------
        ldiu      100,r0
        sti       r0,*+fp(1)
	.line	118
;----------------------------------------------------------------------
; 154 | WriteGlue( iADR_GLUE_FF_MR, 0x0 );
;     |          // Reset the PCI FIFO                                         
;----------------------------------------------------------------------
        ldiu      0,r0
        ldiu      5,r1
L49:        
        push      r0
        push      r1
        call      _WriteGlue
                                        ; Call Occurs
        subi      2,sp
	.line	119
;----------------------------------------------------------------------
; 155 | data = ReadGlue( iADR_GLUE_FF_CNT );                                   
;----------------------------------------------------------------------
        ldiu      6,r0
        push      r0
        call      _ReadGlue
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	120
;----------------------------------------------------------------------
; 156 | data = ReadGlue( iADR_GLUE_STATUS ) & 0xF;                             
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        call      _ReadGlue
                                        ; Call Occurs
        subi      1,sp
        and       15,r0
        sti       r0,*+fp(3)
	.line	121
;----------------------------------------------------------------------
; 157 | } while(( data != PFF_EF ) && ( --i > 0 ));                            
;----------------------------------------------------------------------
        cmpi      3,r0
        beqd      L52
	nop
	nop
        ldieq     6,r0
;*      Branch Occurs to L52 
        ldiu      1,r0
        subri     *+fp(1),r0
        bgtd      L49
        sti       r0,*+fp(1)
        ldigt     0,r0
        ldigt     5,r1
;*      Branch Occurs to L49 
	.line	123
;----------------------------------------------------------------------
; 159 | data = ReadGlue( iADR_GLUE_FF_CNT );                                   
; 161 |         // Write data to the FIFO until it is full                     
;----------------------------------------------------------------------
        ldiu      6,r0
L52:        
        push      r0
        call      _ReadGlue
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	126
;----------------------------------------------------------------------
; 162 | i=0;                                                                   
; 163 | do{                                                                    
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
	.line	128
;----------------------------------------------------------------------
; 164 | WritePCIFifo( i );                                                     
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
L53:        
        push      r0
        call      _WritePCIFifo
                                        ; Call Occurs
        subi      1,sp
	.line	129
;----------------------------------------------------------------------
; 165 | data = ReadGlue( iADR_GLUE_FF_CNT );                                   
;----------------------------------------------------------------------
        ldiu      6,r0
        push      r0
        call      _ReadGlue
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	130
;----------------------------------------------------------------------
; 166 | data = ReadGlue( iADR_GLUE_STATUS ) & 0xF;                             
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        call      _ReadGlue
                                        ; Call Occurs
        subi      1,sp
        and       15,r0
        sti       r0,*+fp(3)
	.line	131
;----------------------------------------------------------------------
; 167 | } while(( data != PFF_FF ) && ( ++i < 0xFFFF ));                       
;----------------------------------------------------------------------
        cmpi      12,r0
        beqd      L56
	nop
	nop
        ldieq     6,r0
;*      Branch Occurs to L56 
        ldiu      1,r0
        addi      *+fp(1),r0
        cmpi      @CL3,r0
        bltd      L53
        sti       r0,*+fp(1)
	nop
        ldilt     *+fp(1),r0
;*      Branch Occurs to L53 
	.line	133
;----------------------------------------------------------------------
; 169 | data = ReadGlue( iADR_GLUE_FF_CNT );                                   
;----------------------------------------------------------------------
        ldiu      6,r0
L56:        
        push      r0
        call      _ReadGlue
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	134
;----------------------------------------------------------------------
; 170 | if( data < 16384 ){                                                    
;----------------------------------------------------------------------
        cmpi      16384,r0
        bhsd      L58
	nop
        ldihs     4660,r0
        ldihs     2,r1
;*      Branch Occurs to L58 
	.line	135
;----------------------------------------------------------------------
; 171 | data = 1;                                                              
; 174 | // Now Wait for the Driver to Acknowledge and check the data           
;----------------------------------------------------------------------
        ldiu      1,r0
        sti       r0,*+fp(3)
	.line	139
;----------------------------------------------------------------------
; 175 | WriteDPRAM( dADR_DSP_VER, 0x1234 );
;     |          // Write all 1234 to DPRAM at Loc 0                           
;----------------------------------------------------------------------
        ldiu      4660,r0
        ldiu      2,r1
L58:        
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	140
;----------------------------------------------------------------------
; 176 | i = MAX_LOOP;                                                          
; 177 | do{                                                                    
;----------------------------------------------------------------------
        ldiu      @CL1,r0
        sti       r0,*+fp(1)
	.line	142
;----------------------------------------------------------------------
; 178 | data = ReadDPRAM( dADR_DSP_VER );                                      
; 179 | #if INITIALIZE_TIME_OUT == 0                                           
;----------------------------------------------------------------------
        ldiu      2,r0
L59:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	144
;----------------------------------------------------------------------
; 180 | i = 2;                                                                 
; 181 | #endif                                                                 
;----------------------------------------------------------------------
        ldiu      2,r0
        sti       r0,*+fp(1)
	.line	147
;----------------------------------------------------------------------
; 183 | while(( data == 0x1234 ) && ( --i > 0 ));                              
; 185 | // Now start the BIT TEST PATTERN                                      
; 186 | // Reset the PCI FIFO                                                  
;----------------------------------------------------------------------
        ldiu      *+fp(3),r0
        cmpi      4660,r0
        bned      L62
	nop
        ldine     0,r1
        ldine     5,r0
;*      Branch Occurs to L62 
        ldiu      1,r0
        subri     *+fp(1),r0
        bgtd      L59
        sti       r0,*+fp(1)
	nop
        ldigt     2,r0
;*      Branch Occurs to L59 
	.line	151
;----------------------------------------------------------------------
; 187 | WriteGlue( iADR_GLUE_FF_MR, 0x0 );
;     |          // Reset the PCI FIFO                                         
; 189 | // Write the Test Pattern                                              
;----------------------------------------------------------------------
        ldiu      0,r1
        ldiu      5,r0
L62:        
        push      r1
        push      r0
        call      _WriteGlue
                                        ; Call Occurs
        subi      2,sp
	.line	154
;----------------------------------------------------------------------
; 190 | for(i=0;i<=31;i++){                                                    
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
        ldiu      0,r4
        ldiu      6,r5
        cmpi      31,r0
        bgtd      L66
	nop
        ldigt     4660,r1
        ldigt     2,r0
;*      Branch Occurs to L66 
	.line	155
;----------------------------------------------------------------------
; 191 | data = ( 1 << i );                                                     
;----------------------------------------------------------------------
        ldiu      1,r0
L64:        
        ash       *+fp(1),r0
        sti       r0,*+fp(3)
	.line	156
;----------------------------------------------------------------------
; 192 | WritePCIFifo( data );                                                  
;----------------------------------------------------------------------
        push      r0
        call      _WritePCIFifo
                                        ; Call Occurs
        subi      1,sp
	.line	157
;----------------------------------------------------------------------
; 193 | data = ReadGlue( iADR_GLUE_FF_CNT );                                   
;----------------------------------------------------------------------
        push      r5
        call      _ReadGlue
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	158
;----------------------------------------------------------------------
; 194 | data = ReadGlue( iADR_GLUE_STATUS ) & 0xF;                             
; 197 | // Wait for the Driver to Acknowledge                                  
;----------------------------------------------------------------------
        push      r4
        call      _ReadGlue
                                        ; Call Occurs
        subi      1,sp
        and       15,r0
        sti       r0,*+fp(3)
	.line	154
        ldiu      1,r0
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      31,r0
        bled      L64
        ldile     1,r0
	nop
        ldile     1,r0
;*      Branch Occurs to L64 
	.line	162
;----------------------------------------------------------------------
; 198 | WriteDPRAM( dADR_DSP_VER, 0x1234 );
;     |          // Write all 1234 to DPRAM at Loc 0                           
;----------------------------------------------------------------------
        ldiu      4660,r1
        ldiu      2,r0
L66:        
        push      r1
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	163
;----------------------------------------------------------------------
; 199 | i = MAX_LOOP;                                                          
; 200 | do{                                                                    
;----------------------------------------------------------------------
        ldiu      @CL1,r0
        sti       r0,*+fp(1)
	.line	165
;----------------------------------------------------------------------
; 201 | data = ReadDPRAM( dADR_DSP_VER );                                      
; 202 | #if INITIALIZE_TIME_OUT == 0                                           
;----------------------------------------------------------------------
        ldiu      2,r0
L67:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	167
;----------------------------------------------------------------------
; 203 | i = 2;                                                                 
; 204 | #endif                                                                 
;----------------------------------------------------------------------
        ldiu      2,r0
        sti       r0,*+fp(1)
	.line	170
;----------------------------------------------------------------------
; 206 | while(( data == 0x1234 ) && ( --i > 0 ));                              
;----------------------------------------------------------------------
        ldiu      *+fp(3),r0
        cmpi      4660,r0
        bned      L70
	nop
	nop
        ldine     0,r0
;*      Branch Occurs to L70 
        ldiu      1,r0
        subri     *+fp(1),r0
        bgtd      L67
        sti       r0,*+fp(1)
	nop
        ldigt     2,r0
;*      Branch Occurs to L67 
	.line	172
;----------------------------------------------------------------------
; 208 | for(i=0;i<0x1FF;i++) {                                                 
;----------------------------------------------------------------------
        ldiu      0,r0
L70:        
        sti       r0,*+fp(1)
        cmpi      511,r0
        bged      L74
        ldiu      0,r4
        ldige     4660,r0
        ldige     2,r1
;*      Branch Occurs to L74 
	.line	173
;----------------------------------------------------------------------
; 209 | WriteDPRAM((USHORT)i,0x0);                                             
; 211 | // Final hand-shaking for the Driver before final boot                 
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
L72:        
        push      r4
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	172
        ldiu      1,r0
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      511,r0
        bltd      L72
        ldilt     *+fp(1),r0
	nop
        ldilt     *+fp(1),r0
;*      Branch Occurs to L72 
	.line	176
;----------------------------------------------------------------------
; 212 | WriteDPRAM( dADR_DSP_VER, 0x1234 );
;     |          // Write all 1234 to DPRAM at Loc 0                           
;----------------------------------------------------------------------
        ldiu      4660,r0
        ldiu      2,r1
L74:        
        push      r0
        push      r1
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	177
;----------------------------------------------------------------------
; 213 | i = MAX_LOOP;                                                          
; 214 | do{                                                                    
;----------------------------------------------------------------------
        ldiu      @CL1,r0
        sti       r0,*+fp(1)
	.line	179
;----------------------------------------------------------------------
; 215 | data = ReadDPRAM( dADR_DSP_VER );                                      
; 216 | #if INITIALIZE_TIME_OUT == 0                                           
;----------------------------------------------------------------------
        ldiu      2,r0
L75:        
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	181
;----------------------------------------------------------------------
; 217 | i = 2;                                                                 
; 218 | #endif                                                                 
;----------------------------------------------------------------------
        ldiu      2,r0
        sti       r0,*+fp(1)
	.line	184
;----------------------------------------------------------------------
; 220 | while(( data == 0x1234 ) && ( --i > 0 ));                              
;----------------------------------------------------------------------
        ldiu      *+fp(3),r0
        cmpi      4660,r0
        bned      L77
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L77 
        subri     *+fp(1),r0
        bgtd      L75
        sti       r0,*+fp(1)
	nop
        ldigt     2,r0
;*      Branch Occurs to L75 
L77:        
	.line	185
        pop       r5
        ldiu      *-fp(1),bk
        pop       r4
        ldiu      *fp,fp
        subi      8,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	221,000000030h,6


	.sect	 ".text"

	.global	_DPRAM_DATA_TEST
	.sym	_DPRAM_DATA_TEST,_DPRAM_DATA_TEST,47,2,0
	.func	226
;******************************************************************************
;* FUNCTION NAME: _DPRAM_DATA_TEST                                            *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,fp,sp,st                                      *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 4 Auto + 0 SOE = 6 words          *
;******************************************************************************
_DPRAM_DATA_TEST:
	.sym	_i,1,4,1,32
	.sym	_wtmp,2,13,1,32
	.sym	_rtmp,3,13,1,32
	.sym	_error,4,15,1,32
	.line	1
;----------------------------------------------------------------------
; 226 | ULONG DPRAM_DATA_TEST( void )                                          
; 229 | int i;                                                                 
; 230 | USHORT wtmp;                                                           
; 231 | USHORT rtmp;                                                           
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      4,sp
	.line	7
;----------------------------------------------------------------------
; 232 | ULONG error = 0;                                                       
; 234 | //----- Check Data bits to DSP Scratch RAM ----------------------------
;     | ---                                                                    
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(4)
	.line	10
;----------------------------------------------------------------------
; 235 | for( i=0;i<=15;i++){                                                   
;----------------------------------------------------------------------
        sti       r0,*+fp(1)
        cmpi      15,r0
        bgtd      L86
	nop
	nop
        ldigt     *+fp(4),r0
;*      Branch Occurs to L86 
	.line	11
;----------------------------------------------------------------------
; 236 | wtmp = 1 << i;                                                         
;----------------------------------------------------------------------
        ldiu      1,r0
L81:        
        ash       *+fp(1),r0
        sti       r0,*+fp(2)
	.line	12
;----------------------------------------------------------------------
; 237 | WriteDPRAM( 0, wtmp );                                                 
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      0,r0
        push      r1
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	13
;----------------------------------------------------------------------
; 238 | rtmp = ReadDPRAM( 0 );                                                 
;----------------------------------------------------------------------
        ldiu      0,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	14
;----------------------------------------------------------------------
; 239 | if( wtmp != rtmp ){                                                    
;----------------------------------------------------------------------
        ldiu      *+fp(2),r0
        cmpi      *+fp(3),r0
        beqd      L83
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L83 
	.line	15
;----------------------------------------------------------------------
; 240 | error |= wtmp;                                                         
; 242 | }                               // for...                              
;----------------------------------------------------------------------
        or        *+fp(4),r0
        sti       r0,*+fp(4)
	.line	10
        ldiu      1,r0
L83:        
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      15,r0
        bled      L81
	nop
	nop
        ldile     1,r0
;*      Branch Occurs to L81 
	.line	19
;----------------------------------------------------------------------
; 244 | return( error );                                                       
;----------------------------------------------------------------------
        ldiu      *+fp(4),r0
	.line	20
L86:        
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	245,000000000h,4


	.sect	 ".text"

	.global	_DPRAM_ADDRESS_TEST
	.sym	_DPRAM_ADDRESS_TEST,_DPRAM_ADDRESS_TEST,47,2,0
	.func	250
;******************************************************************************
;* FUNCTION NAME: _DPRAM_ADDRESS_TEST                                         *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,fp,sp,st                                      *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 5 Auto + 0 SOE = 7 words          *
;******************************************************************************
_DPRAM_ADDRESS_TEST:
	.sym	_i,1,4,1,32
	.sym	_j,2,4,1,32
	.sym	_wtmp,3,13,1,32
	.sym	_rtmp,4,13,1,32
	.sym	_error,5,15,1,32
	.line	1
;----------------------------------------------------------------------
; 250 | ULONG DPRAM_ADDRESS_TEST( void )                                       
; 252 | int i, j;                                                              
; 253 | USHORT wtmp;                                                           
; 254 | USHORT rtmp;                                                           
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      5,sp
	.line	6
;----------------------------------------------------------------------
; 255 | ULONG error = 0;                                                       
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(5)
	.line	8
;----------------------------------------------------------------------
; 257 | for(j=0;j<=1;j++){                                                     
;----------------------------------------------------------------------
        sti       r0,*+fp(2)
        cmpi      1,r0
        bgtd      L96
	nop
	nop
        ldigt     0,r0
;*      Branch Occurs to L96 
	.line	9
;----------------------------------------------------------------------
; 258 | for(i=0;i<=7;i++){                                                     
;----------------------------------------------------------------------
        ldiu      0,r0
L90:        
        sti       r0,*+fp(1)
        cmpi      7,r0
        bgtd      L94
	nop
	nop
        ldigt     1,r0
;*      Branch Occurs to L94 
	.line	10
;----------------------------------------------------------------------
; 259 | wtmp = ( j << 8 ) | ( 1 << i );                                        
;----------------------------------------------------------------------
        ldiu      *+fp(2),r0
        ldiu      1,r1
L92:        
        ash       8,r0
        ash       *+fp(1),r1
        or3       r0,r1,r0
        sti       r0,*+fp(3)
	.line	11
;----------------------------------------------------------------------
; 260 | WriteDPRAM(wtmp, wtmp );                                               
;----------------------------------------------------------------------
        push      r0
        ldiu      *+fp(3),r0
        push      r0
        call      _WriteDPRAM
                                        ; Call Occurs
        subi      2,sp
	.line	9
        ldiu      1,r0
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      7,r0
        bled      L92
        ldile     *+fp(2),r0
        ldile     1,r1
        ldile     *+fp(2),r0
;*      Branch Occurs to L92 
	.line	8
        ldiu      1,r0
L94:        
        addi      *+fp(2),r0
        sti       r0,*+fp(2)
        cmpi      1,r0
        bled      L90
	nop
	nop
        ldile     0,r0
;*      Branch Occurs to L90 
	.line	15
;----------------------------------------------------------------------
; 264 | for(j=0;j<=1;j++){                                                     
;----------------------------------------------------------------------
        ldiu      0,r0
L96:        
        sti       r0,*+fp(2)
        cmpi      1,r0
        bgtd      L107
	nop
	nop
        ldigt     *+fp(5),r0
;*      Branch Occurs to L107 
	.line	16
;----------------------------------------------------------------------
; 265 | for(i=0;i<=7;i++){                                                     
;----------------------------------------------------------------------
        ldiu      0,r0
L98:        
        sti       r0,*+fp(1)
        cmpi      7,r0
        bgtd      L104
	nop
	nop
        ldigt     1,r0
;*      Branch Occurs to L104 
	.line	17
;----------------------------------------------------------------------
; 266 | wtmp = ( j << 8 ) | ( 1 << i );                                        
;----------------------------------------------------------------------
        ldiu      *+fp(2),r1
        ldiu      1,r0
L100:        
        ash       8,r1
        ash       *+fp(1),r0
        or3       r1,r0,r0
        sti       r0,*+fp(3)
	.line	18
;----------------------------------------------------------------------
; 267 | rtmp = ReadDPRAM( wtmp );                                              
;----------------------------------------------------------------------
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(4)
	.line	19
;----------------------------------------------------------------------
; 268 | if( wtmp != rtmp ){                                                    
;----------------------------------------------------------------------
        ldiu      *+fp(3),r0
        cmpi      *+fp(4),r0
        beqd      L102
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L102 
	.line	20
;----------------------------------------------------------------------
; 269 | error |= wtmp;                                                         
; 273 | //----- Check Data bits to DSP Scratch RAM ----------------------------
;     | ---                                                                    
;----------------------------------------------------------------------
        or        *+fp(5),r0
        sti       r0,*+fp(5)
	.line	16
        ldiu      1,r0
L102:        
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      7,r0
        bled      L100
	nop
        ldile     *+fp(2),r1
        ldile     1,r0
;*      Branch Occurs to L100 
	.line	15
        ldiu      1,r0
L104:        
        addi      *+fp(2),r0
        sti       r0,*+fp(2)
        cmpi      1,r0
        bled      L98
	nop
	nop
        ldile     0,r0
;*      Branch Occurs to L98 
	.line	26
;----------------------------------------------------------------------
; 275 | return( error );                                                       
;----------------------------------------------------------------------
        ldiu      *+fp(5),r0
	.line	27
L107:        
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      7,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	276,000000000h,5


	.sect	 ".text"

	.global	_FPGA_TEST
	.sym	_FPGA_TEST,_FPGA_TEST,47,2,0
	.func	281
;******************************************************************************
;* FUNCTION NAME: _FPGA_TEST                                                  *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,fp,sp,st                                      *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 4 Auto + 0 SOE = 7 words          *
;******************************************************************************
_FPGA_TEST:
	.sym	_target,-2,4,9,32
	.sym	_wdata,1,13,1,32
	.sym	_rdata,2,13,1,32
	.sym	_error,3,15,1,32
	.sym	_i,4,4,1,32
	.line	1
;----------------------------------------------------------------------
; 281 | ULONG FPGA_TEST( int target )                                          
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      4,sp
	.line	2
;----------------------------------------------------------------------
; 283 | USHORT wdata;                                                          
; 284 | USHORT rdata;                                                          
;----------------------------------------------------------------------
	.line	5
;----------------------------------------------------------------------
; 285 | ULONG error = 0;                                                       
; 286 | int i;                                                                 
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(3)
	.line	8
;----------------------------------------------------------------------
; 288 | for(i=0;i<=15;i++){                                                    
;----------------------------------------------------------------------
        sti       r0,*+fp(4)
        cmpi      15,r0
        bgtd      L120
	nop
	nop
        ldigt     *+fp(3),r0
;*      Branch Occurs to L120 
	.line	9
;----------------------------------------------------------------------
; 289 | wdata = 1 << i;                                                        
;----------------------------------------------------------------------
        ldiu      1,r0
L111:        
        ash       *+fp(4),r0
        sti       r0,*+fp(1)
	.line	10
;----------------------------------------------------------------------
; 290 | if( target == 0 ){                                                     
;----------------------------------------------------------------------
        ldi       *-fp(2),r0
        bned      L114
	nop
        ldine     *+fp(1),r0
        ldine     279,r1
;*      Branch Occurs to L114 
	.line	11
;----------------------------------------------------------------------
; 291 | WriteGlue( iADR_GLUE_TREG, wdata );                                    
;----------------------------------------------------------------------
        ldiu      *+fp(1),r1
        ldiu      4,r0
        push      r1
        push      r0
        call      _WriteGlue
                                        ; Call Occurs
        subi      2,sp
	.line	12
;----------------------------------------------------------------------
; 292 | rdata = ReadGlue( iADR_GLUE_TREG );                                    
; 293 | } else {                                                               
;----------------------------------------------------------------------
        ldiu      4,r0
        push      r0
        call      _ReadGlue
                                        ; Call Occurs
        subi      1,sp
        bud       L115
        sti       r0,*+fp(2)
        ldiu      *+fp(1),r0
        cmpi      *+fp(2),r0
;*      Branch Occurs to L115 
	.line	14
;----------------------------------------------------------------------
; 294 | WriteEDS( iADR_PRESET_LO, wdata );                                     
;----------------------------------------------------------------------
L114:        
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	15
;----------------------------------------------------------------------
; 295 | rdata = ReadEDS( iADR_PRESET_LO );                                     
;----------------------------------------------------------------------
        ldiu      279,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(2)
	.line	17
;----------------------------------------------------------------------
; 297 | if( wdata != rdata ){                                                  
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
        cmpi      *+fp(2),r0
L115:        
        beqd      L117
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L117 
	.line	18
;----------------------------------------------------------------------
; 298 | error |= wdata;                                                        
;----------------------------------------------------------------------
        or        *+fp(3),r0
        sti       r0,*+fp(3)
	.line	8
        ldiu      1,r0
L117:        
        addi      *+fp(4),r0
        sti       r0,*+fp(4)
        cmpi      15,r0
        bled      L111
	nop
	nop
        ldile     1,r0
;*      Branch Occurs to L111 
	.line	21
;----------------------------------------------------------------------
; 301 | return( error );                                                       
;----------------------------------------------------------------------
        ldiu      *+fp(3),r0
	.line	22
L120:        
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	302,000000000h,4


	.sect	 ".text"

	.global	_FIFO_DATA_TEST
	.sym	_FIFO_DATA_TEST,_FIFO_DATA_TEST,47,2,0
	.func	307
;******************************************************************************
;* FUNCTION NAME: _FIFO_DATA_TEST                                             *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,fp,sp,st                                      *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 4 Auto + 0 SOE = 6 words          *
;******************************************************************************
_FIFO_DATA_TEST:
	.sym	_i,1,4,1,32
	.sym	_error,2,15,1,32
	.sym	_wtmp,3,15,1,32
	.sym	_rtmp,4,15,1,32
	.line	1
;----------------------------------------------------------------------
; 307 | ULONG FIFO_DATA_TEST( void )                                           
; 309 | int i;                                                                 
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      4,sp
	.line	4
;----------------------------------------------------------------------
; 310 | ULONG error = 0;                                                       
; 311 | ULONG wtmp;                                                            
; 312 | ULONG rtmp;                                                            
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(2)
	.line	8
;----------------------------------------------------------------------
; 314 | wtmp = ReadEDS( iADR_CW );                                             
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	9
;----------------------------------------------------------------------
; 315 | wtmp |= BIT_ENABLE;
;     |  // force Bit Enable                                                   
;----------------------------------------------------------------------
        ldiu      1,r0
        or        *+fp(3),r0
        sti       r0,*+fp(3)
	.line	10
;----------------------------------------------------------------------
; 316 | WriteEDS( iADR_CW, (USHORT) wtmp );                                    
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      258,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	12
;----------------------------------------------------------------------
; 318 | WriteEDS( iADR_TP_SEL, 0x03 << 12 );                                   
;----------------------------------------------------------------------
        ldiu      12288,r1
        push      r1
        ldiu      264,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	13
;----------------------------------------------------------------------
; 319 | WriteEDS( iADR_FIFO_MR, 0x0 );
;     |                  // Reset EDS FIFO                                     
;----------------------------------------------------------------------
        ldiu      0,r1
        ldiu      298,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	15
;----------------------------------------------------------------------
; 321 | for( i=0;i<=17;i++) {                                                  
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
        cmpi      17,r0
        bgtd      L126
	nop
	nop
        ldigt     0,r0
;*      Branch Occurs to L126 
	.line	16
;----------------------------------------------------------------------
; 322 | wtmp = ( 1 << i );                                                     
;----------------------------------------------------------------------
        ldiu      1,r0
L124:        
        ash       *+fp(1),r0
        sti       r0,*+fp(3)
	.line	17
;----------------------------------------------------------------------
; 323 | WriteEDSFIFO( wtmp );                                                  
;----------------------------------------------------------------------
        push      r0
        call      _WriteEDSFIFO
                                        ; Call Occurs
        subi      1,sp
	.line	15
        ldiu      1,r0
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      17,r0
        bled      L124
        ldile     1,r0
	nop
        ldile     1,r0
;*      Branch Occurs to L124 
	.line	20
;----------------------------------------------------------------------
; 326 | for( i=0;i<=17;i++) {                                                  
;----------------------------------------------------------------------
        ldiu      0,r0
L126:        
        sti       r0,*+fp(1)
        cmpi      17,r0
        bgtd      L132
	nop
	nop
        ldigt     258,r0
;*      Branch Occurs to L132 
	.line	21
;----------------------------------------------------------------------
; 327 | wtmp = ( 1 << i );                                                     
; 328 | // Now Read the FIFO                                                   
;----------------------------------------------------------------------
        ldiu      1,r0
L128:        
        ash       *+fp(1),r0
        sti       r0,*+fp(3)
	.line	23
;----------------------------------------------------------------------
; 329 | rtmp = ReadEDSFIFO();                                                  
;----------------------------------------------------------------------
        call      _ReadEDSFIFO
                                        ; Call Occurs
        sti       r0,*+fp(4)
	.line	25
;----------------------------------------------------------------------
; 331 | if( wtmp != rtmp ){                                                    
;----------------------------------------------------------------------
        ldiu      *+fp(3),r0
        cmpi      *+fp(4),r0
        beqd      L130
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L130 
	.line	26
;----------------------------------------------------------------------
; 332 | error |= wtmp;                                                         
;----------------------------------------------------------------------
        or        *+fp(2),r0
        sti       r0,*+fp(2)
	.line	20
        ldiu      1,r0
L130:        
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      17,r0
        bled      L128
	nop
	nop
        ldile     1,r0
;*      Branch Occurs to L128 
	.line	30
;----------------------------------------------------------------------
; 336 | wtmp = ReadEDS( iADR_CW );                                             
;----------------------------------------------------------------------
        ldiu      258,r0
L132:        
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(3)
	.line	31
;----------------------------------------------------------------------
; 337 | wtmp &= ~BIT_ENABLE;
;     |  // force Bit Enable                                                   
;----------------------------------------------------------------------
        andn      1,r0
        sti       r0,*+fp(3)
	.line	32
;----------------------------------------------------------------------
; 338 | WriteEDS( iADR_CW, (USHORT) wtmp );                                    
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      258,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	33
;----------------------------------------------------------------------
; 339 | return( error );                                                       
;----------------------------------------------------------------------
        ldiu      *+fp(2),r0
	.line	34
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      6,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	340,000000000h,4


	.sect	 ".text"

	.global	_FIFO_FLAG_TEST
	.sym	_FIFO_FLAG_TEST,_FIFO_FLAG_TEST,47,2,0
	.func	346
;******************************************************************************
;* FUNCTION NAME: _FIFO_FLAG_TEST                                             *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,fp,sp,st                                      *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 5 Auto + 0 SOE = 7 words          *
;******************************************************************************
_FIFO_FLAG_TEST:
	.sym	_i,1,4,1,32
	.sym	_j,2,4,1,32
	.sym	_error,3,15,1,32
	.sym	_tmp,4,13,1,32
	.sym	_wtmp,5,15,1,32
	.line	1
;----------------------------------------------------------------------
; 346 | ULONG FIFO_FLAG_TEST( void )                                           
; 348 | int i, j;                                                              
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      5,sp
	.line	4
;----------------------------------------------------------------------
; 349 | ULONG error = 0;                                                       
; 350 | USHORT tmp;                                                            
; 351 | ULONG wtmp;                                                            
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(3)
	.line	8
;----------------------------------------------------------------------
; 353 | wtmp = ReadEDS( iADR_CW );                                             
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(5)
	.line	9
;----------------------------------------------------------------------
; 354 | wtmp |= BIT_ENABLE;
;     |  // force Bit Enable                                                   
;----------------------------------------------------------------------
        ldiu      1,r0
        or        *+fp(5),r0
        sti       r0,*+fp(5)
	.line	10
;----------------------------------------------------------------------
; 355 | WriteEDS( iADR_CW, (USHORT) wtmp );                                    
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      258,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	12
;----------------------------------------------------------------------
; 357 | WriteEDS( iADR_TP_SEL, 0x03  << 12 );                                  
;----------------------------------------------------------------------
        ldiu      12288,r1
        push      r1
        ldiu      264,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	13
;----------------------------------------------------------------------
; 358 | WriteEDS( iADR_FIFO_MR, 0x0 );
;     |                  // Reset EDS FIFO                                     
; 360 | // Now Write until the fifo is half full                               
;----------------------------------------------------------------------
        ldiu      0,r1
        ldiu      298,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	16
;----------------------------------------------------------------------
; 361 | for(j=0;j<=1;j++){                                                     
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(2)
        cmpi      1,r0
        bgtd      L185
	nop
	nop
        ldigt     258,r0
;*      Branch Occurs to L185 
	.line	17
;----------------------------------------------------------------------
; 362 | for(i=0;i<FF_FULL_DEPTH;i++){                                          
; 363 |         switch( j ){                                                   
; 364 |                 case 0 :                                               
;----------------------------------------------------------------------
        ldiu      0,r0
L137:        
        sti       r0,*+fp(1)
        cmpi      16384,r0
        bged      L183
	nop
	nop
        ldige     1,r0
;*      Branch Occurs to L183 
        bu        L144
;*      Branch Occurs to L144 
	.line	20
;----------------------------------------------------------------------
; 365 | tmp = ReadEDS( iADR_STAT ) & 0x3;                       // Read the Sta
;     | tus Flags                                                              
;----------------------------------------------------------------------
L140:        
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        and       3,r0
        sti       r0,*+fp(4)
	.line	21
;----------------------------------------------------------------------
; 366 | WriteEDSFIFO( i );                                                     
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
        push      r0
        call      _WriteEDSFIFO
                                        ; Call Occurs
	.line	22
;----------------------------------------------------------------------
; 367 | break;                                                                 
; 368 | case 1:                                                                
;----------------------------------------------------------------------
        bud       L176
        subi      1,sp
	nop
	nop
;*      Branch Occurs to L176 
	.line	24
;----------------------------------------------------------------------
; 369 | tmp = ReadEDS( iADR_STAT ) & 0x3;                       // Read the Sta
;     | tus Flags                                                              
;----------------------------------------------------------------------
L142:        
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        and       3,r0
        sti       r0,*+fp(4)
	.line	25
;----------------------------------------------------------------------
; 370 | wtmp = ReadEDSFIFO();                                                  
;----------------------------------------------------------------------
        call      _ReadEDSFIFO
                                        ; Call Occurs
	.line	26
;----------------------------------------------------------------------
; 371 | break;                                                                 
; 374 | switch( tmp ) {                                                        
; 375 | case 0 :
;     |  // FIFO CONTAINS DATA                                                 
;----------------------------------------------------------------------
        bud       L176
        sti       r0,*+fp(5)
	nop
	nop
;*      Branch Occurs to L176 
L144:        
	.line	18
        ldi       *+fp(2),r0
        beqd      L140
	nop
	nop
        ldieq     257,r0
;*      Branch Occurs to L140 
        cmpi      1,r0
        beqd      L142
	nop
	nop
        ldieq     257,r0
;*      Branch Occurs to L142 
        bu        L175
;*      Branch Occurs to L175 
	.line	31
;----------------------------------------------------------------------
; 376 | if(( i == FF_FULL_DEPTH ) || ( i == 0 )){                              
;----------------------------------------------------------------------
L148:        
        cmpi      16384,r0
        beq       L150
;*      Branch Occurs to L150 
        cmpi      0,r0
        bned      L181
	nop
	nop
        ldine     1,r0
;*      Branch Occurs to L181 
L150:        
	.line	32
;----------------------------------------------------------------------
; 377 | if( j == 0 ){                                                          
;----------------------------------------------------------------------
        ldi       *+fp(2),r0
        bned      L153
	nop
	nop
        ldine     256,r0
;*      Branch Occurs to L153 
	.line	33
;----------------------------------------------------------------------
; 378 | error |= 1;                                                            
; 379 | } else {                                                               
;----------------------------------------------------------------------
        bud       L180
        ldiu      1,r0
        or        *+fp(3),r0
        sti       r0,*+fp(3)
;*      Branch Occurs to L180 
	.line	35
;----------------------------------------------------------------------
; 380 | error |= 0x100;                                                        
;----------------------------------------------------------------------
L153:        
	.line	38
;----------------------------------------------------------------------
; 383 | break;                                                                 
; 385 | case 1:
;     |  // FIFO EMPTY INDICATION                                              
;----------------------------------------------------------------------
        bud       L181
        or        *+fp(3),r0
        sti       r0,*+fp(3)
        ldiu      1,r0
;*      Branch Occurs to L181 
L154:        
	.line	41
;----------------------------------------------------------------------
; 386 | if((( j == 0 ) && ( i > 0 ))                                           
; 387 |         || (( j == 1 ) && ( i < FF_FULL_DEPTH ))){                     
;----------------------------------------------------------------------
        ldi       *+fp(2),r0
        bned      L156
	nop
	nop
        ldine     *+fp(2),r0
;*      Branch Occurs to L156 
        ldi       *+fp(1),r0
        bgtd      L158
	nop
	nop
        ldile     *+fp(2),r0
;*      Branch Occurs to L158 
L156:        
        cmpi      1,r0
        bned      L181
	nop
	nop
        ldine     1,r0
;*      Branch Occurs to L181 
        ldiu      *+fp(1),r0
        cmpi      16384,r0
        bged      L181
	nop
	nop
        ldige     1,r0
;*      Branch Occurs to L181 
L158:        
	.line	43
;----------------------------------------------------------------------
; 388 | if( j == 0 ){                                                          
;----------------------------------------------------------------------
        ldi       *+fp(2),r0
        bned      L161
	nop
	nop
        ldine     512,r0
;*      Branch Occurs to L161 
	.line	44
;----------------------------------------------------------------------
; 389 | error |= 2;                                                            
; 390 | } else {                                                               
;----------------------------------------------------------------------
        bud       L180
        ldiu      2,r0
        or        *+fp(3),r0
        sti       r0,*+fp(3)
;*      Branch Occurs to L180 
	.line	46
;----------------------------------------------------------------------
; 391 | error |= 0x200;                                                        
;----------------------------------------------------------------------
L161:        
        or        *+fp(3),r0
        sti       r0,*+fp(3)
	.line	47
;----------------------------------------------------------------------
; 392 | error |= ( i & 0xFF );                                                 
;----------------------------------------------------------------------
        ldiu      255,r0
	.line	50
;----------------------------------------------------------------------
; 395 | break;                                                                 
; 397 | case 2:
;     |  // FIFO FULL INDICATION                                               
;----------------------------------------------------------------------
        bud       L180
        and       *+fp(1),r0
        or        *+fp(3),r0
        sti       r0,*+fp(3)
;*      Branch Occurs to L180 
L162:        
	.line	53
;----------------------------------------------------------------------
; 398 | if((( j == 0 ) && ( i < FF_FULL_DEPTH ))                               
; 399 | || (( j == 1 ) && ( i > 0 )))                                          
;----------------------------------------------------------------------
        ldi       *+fp(2),r0
        bned      L164
	nop
	nop
        ldine     *+fp(2),r0
;*      Branch Occurs to L164 
        ldiu      *+fp(1),r0
        cmpi      16384,r0
        bltd      L166
	nop
	nop
        ldige     *+fp(2),r0
;*      Branch Occurs to L166 
L164:        
        cmpi      1,r0
        bned      L181
	nop
	nop
        ldine     1,r0
;*      Branch Occurs to L181 
        ldi       *+fp(1),r0
        bled      L181
	nop
	nop
        ldile     1,r0
;*      Branch Occurs to L181 
L166:        
	.line	56
;----------------------------------------------------------------------
; 401 | if( j == 0 ){                                                          
;----------------------------------------------------------------------
        ldi       *+fp(2),r0
        bned      L169
	nop
	nop
        ldine     1024,r0
;*      Branch Occurs to L169 
	.line	57
;----------------------------------------------------------------------
; 402 | error |= 4;                                                            
; 403 | } else {                                                               
;----------------------------------------------------------------------
        bud       L180
        ldiu      4,r0
        or        *+fp(3),r0
        sti       r0,*+fp(3)
;*      Branch Occurs to L180 
	.line	59
;----------------------------------------------------------------------
; 404 | error |= 0x400;                                                        
;----------------------------------------------------------------------
L169:        
	.line	63
;----------------------------------------------------------------------
; 408 | break;                                                                 
; 410 | case 3:
;     |  // Both Flags active - Invalid                                        
;----------------------------------------------------------------------
        bud       L181
        or        *+fp(3),r0
        sti       r0,*+fp(3)
        ldiu      1,r0
;*      Branch Occurs to L181 
L170:        
	.line	66
;----------------------------------------------------------------------
; 411 | if( j == 0 ){                                                          
;----------------------------------------------------------------------
        ldi       *+fp(2),r0
        bned      L173
	nop
	nop
        ldine     2048,r0
;*      Branch Occurs to L173 
	.line	67
;----------------------------------------------------------------------
; 412 | error |= 8;                                                            
; 413 | } else {                                                               
;----------------------------------------------------------------------
        bud       L180
        ldiu      8,r0
        or        *+fp(3),r0
        sti       r0,*+fp(3)
;*      Branch Occurs to L180 
	.line	69
;----------------------------------------------------------------------
; 414 | error |= 0x800;                                                        
;----------------------------------------------------------------------
L173:        
	.line	71
;----------------------------------------------------------------------
; 416 | break;                                                                 
; 417 | }                                               // switch              
; 418 | }                                                       // for i...    
; 419 | }                                                               // for
;     | j...                                                                   
;----------------------------------------------------------------------
        bud       L181
        or        *+fp(3),r0
        sti       r0,*+fp(3)
        ldiu      1,r0
;*      Branch Occurs to L181 
L175:        
	.line	29
L176:        
        ldi       *+fp(4),r0
        beqd      L148
	nop
	nop
        ldieq     *+fp(1),r0
;*      Branch Occurs to L148 
        cmpi      1,r0
        beq       L154
;*      Branch Occurs to L154 
        cmpi      2,r0
        beq       L162
;*      Branch Occurs to L162 
        cmpi      3,r0
        beq       L170
;*      Branch Occurs to L170 
L180:        
	.line	17
        ldiu      1,r0
L181:        
        addi      *+fp(1),r0
        sti       r0,*+fp(1)
        cmpi      16384,r0
        bltd      L144
	nop
	nop
        ldige     1,r0
;*      Branch Occurs to L144 
	.line	16
L183:        
        addi      *+fp(2),r0
        sti       r0,*+fp(2)
        cmpi      1,r0
        bled      L137
	nop
	nop
        ldile     0,r0
;*      Branch Occurs to L137 
	.line	76
;----------------------------------------------------------------------
; 421 | wtmp = ReadEDS( iADR_CW );                                             
;----------------------------------------------------------------------
        ldiu      258,r0
L185:        
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(5)
	.line	77
;----------------------------------------------------------------------
; 422 | wtmp &= ~BIT_ENABLE;
;     |  // force Bit Enable                                                   
;----------------------------------------------------------------------
        andn      1,r0
        sti       r0,*+fp(5)
	.line	78
;----------------------------------------------------------------------
; 423 | WriteEDS( iADR_CW, (USHORT) wtmp );                                    
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      258,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	79
;----------------------------------------------------------------------
; 424 | return( error );                                                       
;----------------------------------------------------------------------
        ldiu      *+fp(3),r0
	.line	80
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      7,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	425,000000000h,5


	.sect	 ".text"

	.global	_Ram_Data_Test
	.sym	_Ram_Data_Test,_Ram_Data_Test,47,2,0
	.func	430
;******************************************************************************
;* FUNCTION NAME: _Ram_Data_Test                                              *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,fp,sp,st                                      *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 8 Auto + 0 SOE = 11 words         *
;******************************************************************************
_Ram_Data_Test:
	.sym	_target,-2,4,9,32
	.sym	_error,1,15,1,32
	.sym	_i,2,4,1,32
	.sym	_bits,3,4,1,32
	.sym	_wtmp,4,13,1,32
	.sym	_rtmp,5,13,1,32
	.sym	_addr,6,13,1,32
	.sym	_status,7,13,1,32
	.sym	_data,8,13,1,32
	.line	1
;----------------------------------------------------------------------
; 430 | ULONG Ram_Data_Test( int target )                                      
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      8,sp
	.line	2
	.line	3
;----------------------------------------------------------------------
; 432 | ULONG error = 0;                                                       
; 433 | int i, bits;                                                           
; 434 | USHORT wtmp;                                                           
; 435 | USHORT rtmp;                                                           
; 436 | USHORT addr;                                                           
; 437 | USHORT status;                                                         
; 438 | USHORT data;                                                           
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
	.line	11
;----------------------------------------------------------------------
; 440 | WriteEDS( iADR_TP_SEL, 0x02  << 12 );                                  
; 442 | switch ( target )                                                      
;----------------------------------------------------------------------
        ldiu      8192,r1
        push      r1
        ldiu      264,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        bud       L194
        subi      2,sp
	nop
	nop
;*      Branch Occurs to L194 
	.line	15
;----------------------------------------------------------------------
; 444 | case 0 : addr = iADR_SCA_LU; bits = SCA_BITS; break;                   
;----------------------------------------------------------------------
L190:        
        bud       L196
        sti       r0,*+fp(6)
        ldiu      8,r0
        sti       r0,*+fp(3)
;*      Branch Occurs to L196 
	.line	16
;----------------------------------------------------------------------
; 445 | case 1 : addr = iADR_ROI_LU; bits = 16; break;                         
;----------------------------------------------------------------------
L192:        
        bud       L196
        sti       r0,*+fp(6)
        ldiu      16,r0
        sti       r0,*+fp(3)
;*      Branch Occurs to L196 
	.line	13
L194:        
        ldi       *-fp(2),r0
        beqd      L190
	nop
	nop
        ldieq     291,r0
;*      Branch Occurs to L190 
        cmpi      1,r0
        beqd      L192
	nop
	nop
        ldieq     293,r0
;*      Branch Occurs to L192 
L196:        
	.line	19
;----------------------------------------------------------------------
; 448 | status = ( ReadEDS( iADR_STAT ) >> 3 );                                
;----------------------------------------------------------------------
        ldiu      257,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        lsh       -3,r0
        sti       r0,*+fp(7)
	.line	20
;----------------------------------------------------------------------
; 449 | if(( status & 0x1 ) == 1 ){                                            
;----------------------------------------------------------------------
        ldiu      1,r0
        and       *+fp(7),r0
        cmpi      1,r0
        bned      L198
	nop
        ldine     0,r1
        ldine     276,r0
;*      Branch Occurs to L198 
	.line	21
;----------------------------------------------------------------------
; 450 | error = 0x10000000;                                                    
;----------------------------------------------------------------------
        ldiu      @CL4,r0
        sti       r0,*+fp(1)
	.line	23
;----------------------------------------------------------------------
; 452 | WriteEDS( iADR_TIME_STOP, 0x0 );                        // Stop EDS Ana
;     | lysis                                                                  
;----------------------------------------------------------------------
        ldiu      0,r1
        ldiu      276,r0
L198:        
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	24
;----------------------------------------------------------------------
; 453 | WriteEDS( iADR_RAMA, 0 );                                       // Rese
;     | t the Ram Address                                                      
;----------------------------------------------------------------------
        ldiu      0,r0
        ldiu      289,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	26
;----------------------------------------------------------------------
; 455 | data = ReadEDS( iADR_CW );                                      // Read
;     |  the Control Word                                                      
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(8)
	.line	27
;----------------------------------------------------------------------
; 456 | data |= RAM_ACCESS;
;     |  // Enable RAM Access                                                  
;----------------------------------------------------------------------
        ldiu      16,r0
        or        *+fp(8),r0
        sti       r0,*+fp(8)
	.line	28
;----------------------------------------------------------------------
; 457 | WriteEDS( iADR_CW, data );                                      // ReWr
;     | ite the Control Word                                                   
; 459 | //----- Ram Data Test ----------------------------------------------   
;----------------------------------------------------------------------
        ldiu      r0,r1
        push      r1
        ldiu      258,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	31
;----------------------------------------------------------------------
; 460 | for(i=0;i<bits;i++){                                                   
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(2)
        cmpi      *+fp(3),r0
        bged      L204
	nop
	nop
        ldige     258,r0
;*      Branch Occurs to L204 
	.line	32
;----------------------------------------------------------------------
; 461 | wtmp = 1 << i;                                                         
;----------------------------------------------------------------------
        ldiu      1,r0
L200:        
        ash       *+fp(2),r0
        sti       r0,*+fp(4)
	.line	33
;----------------------------------------------------------------------
; 462 | WriteEDS( iADR_RAMA, 0 );                               // Write the RA
;     | M Address                                                              
;----------------------------------------------------------------------
        ldiu      0,r1
        push      r1
        ldiu      289,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	34
;----------------------------------------------------------------------
; 463 | WriteEDS( addr, wtmp );                                 // Write the RA
;     | M Data                                                                 
;----------------------------------------------------------------------
        ldiu      *+fp(4),r0
        push      r0
        ldiu      *+fp(6),r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	35
;----------------------------------------------------------------------
; 464 | rtmp = ReadEDS( addr );                                 // Read the RAM
;     |  Data                                                                  
;----------------------------------------------------------------------
        ldiu      *+fp(6),r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(5)
	.line	37
;----------------------------------------------------------------------
; 466 | if( wtmp != rtmp ){                                             // Comp
;     | are                                                                    
;----------------------------------------------------------------------
        ldiu      *+fp(4),r0
        cmpi      *+fp(5),r0
        beqd      L202
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L202 
	.line	38
;----------------------------------------------------------------------
; 467 | error |= wtmp;                                                         
; 470 | //----- Ram Data Test ----------------------------------------------   
;----------------------------------------------------------------------
        or        *+fp(1),r0
        sti       r0,*+fp(1)
	.line	31
        ldiu      1,r0
L202:        
        addi      *+fp(2),r0
        sti       r0,*+fp(2)
        cmpi      *+fp(3),r0
        bltd      L200
	nop
	nop
        ldilt     1,r0
;*      Branch Occurs to L200 
	.line	43
;----------------------------------------------------------------------
; 472 | data = ReadEDS( iADR_CW );                                      // Read
;     |  the Control Word                                                      
;----------------------------------------------------------------------
        ldiu      258,r0
L204:        
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(8)
	.line	44
;----------------------------------------------------------------------
; 473 | data &= ~RAM_ACCESS;                                            // Disa
;     | ble Ram Access                                                         
;----------------------------------------------------------------------
        andn      16,r0
        sti       r0,*+fp(8)
	.line	45
;----------------------------------------------------------------------
; 474 | WriteEDS( iADR_CW, data );                                      // ReWr
;     | ite the Control Word                                                   
; 475 | #if NO_SCA == TRUE                                                     
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      258,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	47
;----------------------------------------------------------------------
; 476 | error = 0;                                                             
; 477 | #endif                                                                 
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
	.line	50
;----------------------------------------------------------------------
; 479 | return( error );                                                       
;----------------------------------------------------------------------
	.line	51
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      10,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	480,000000000h,8


	.sect	 ".text"

	.global	_Ram_Address_Test
	.sym	_Ram_Address_Test,_Ram_Address_Test,47,2,0
	.func	484
;******************************************************************************
;* FUNCTION NAME: _Ram_Address_Test                                           *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,r4,fp,sp,st                                   *
;*   Regs Saved         : r4                                                  *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 8 Auto + 1 SOE = 12 words         *
;******************************************************************************
_Ram_Address_Test:
	.sym	_target,-2,4,9,32
	.sym	_error,1,15,1,32
	.sym	_i,2,4,1,32
	.sym	_wtmp,3,13,1,32
	.sym	_rtmp,4,13,1,32
	.sym	_addr,5,13,1,32
	.sym	_max_loop,6,4,1,32
	.sym	_status,7,13,1,32
	.sym	_data,8,13,1,32
	.line	1
;----------------------------------------------------------------------
; 484 | ULONG Ram_Address_Test( int target )                                   
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      8,sp
        push      r4
	.line	2
	.line	3
;----------------------------------------------------------------------
; 486 | ULONG error = 0;                                                       
; 487 | int i;                                                                 
; 488 | USHORT wtmp;                                                           
; 489 | USHORT rtmp;                                                           
; 490 | USHORT addr;                                                           
; 491 | int max_loop;                                                          
; 492 | USHORT status;                                                         
; 493 | USHORT data;                                                           
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
	.line	12
;----------------------------------------------------------------------
; 495 | WriteEDS( iADR_TP_SEL, 0x02  << 12 );                                  
;----------------------------------------------------------------------
        ldiu      8192,r1
        ldiu      264,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	14
;----------------------------------------------------------------------
; 497 | status = ( ReadEDS( iADR_STAT ) >> 3 );                                
;----------------------------------------------------------------------
        ldiu      257,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        lsh       -3,r0
        sti       r0,*+fp(7)
	.line	15
;----------------------------------------------------------------------
; 498 | if(( status & 0x1 ) == 1 ){                                            
;----------------------------------------------------------------------
        ldiu      1,r0
        and       *+fp(7),r0
        cmpi      1,r0
        bned      L209
	nop
        ldine     0,r0
        ldine     276,r1
;*      Branch Occurs to L209 
	.line	16
;----------------------------------------------------------------------
; 499 | error = 0x10000000;                                                    
;----------------------------------------------------------------------
        ldiu      @CL4,r0
        sti       r0,*+fp(1)
	.line	18
;----------------------------------------------------------------------
; 501 | WriteEDS( iADR_TIME_STOP, 0x0 );                        // Stop EDS Ana
;     | lysis                                                                  
;----------------------------------------------------------------------
        ldiu      0,r0
        ldiu      276,r1
L209:        
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	19
;----------------------------------------------------------------------
; 502 | WriteEDS( iADR_RAMA, 0 );                                       // Rese
;     | t the Ram Address                                                      
;----------------------------------------------------------------------
        ldiu      0,r0
        ldiu      289,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	21
;----------------------------------------------------------------------
; 504 | data = ReadEDS( iADR_CW );                                      // Read
;     |  the Control Word                                                      
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(8)
	.line	22
;----------------------------------------------------------------------
; 505 | data |= RAM_ACCESS;
;     |  // Enable RAM Access                                                  
;----------------------------------------------------------------------
        ldiu      16,r0
        or        *+fp(8),r0
        sti       r0,*+fp(8)
	.line	23
;----------------------------------------------------------------------
; 506 | WriteEDS( iADR_CW, data );                                      // ReWr
;     | ite the Control Word                                                   
; 509 | switch ( target )                                                      
;----------------------------------------------------------------------
        ldiu      258,r0
        ldiu      *+fp(8),r1
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        bud       L215
        subi      2,sp
	nop
	nop
;*      Branch Occurs to L215 
	.line	28
;----------------------------------------------------------------------
; 511 | case 0 : max_loop = 12; addr = iADR_SCA_LU; break;                     
;----------------------------------------------------------------------
L211:        
        bud       L217
        sti       r0,*+fp(6)
        ldiu      291,r0
        sti       r0,*+fp(5)
;*      Branch Occurs to L217 
	.line	29
;----------------------------------------------------------------------
; 512 | case 1 : max_loop = 8;  addr = iADR_ROI_LU; break;                     
;----------------------------------------------------------------------
L213:        
        bud       L217
        sti       r0,*+fp(6)
        ldiu      293,r0
        sti       r0,*+fp(5)
;*      Branch Occurs to L217 
	.line	26
L215:        
        ldi       *-fp(2),r0
        beqd      L211
	nop
	nop
        ldieq     12,r0
;*      Branch Occurs to L211 
        cmpi      1,r0
        beqd      L213
	nop
	nop
        ldieq     8,r0
;*      Branch Occurs to L213 
L217:        
	.line	32
;----------------------------------------------------------------------
; 515 | for(i=0;i<max_loop;i++){                                               
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(2)
        cmpi      *+fp(6),r0
        bged      L221
        ldiu      289,r4
	nop
        ldige     0,r0
;*      Branch Occurs to L221 
	.line	33
;----------------------------------------------------------------------
; 516 | wtmp = ( 1 << i );                                                     
;----------------------------------------------------------------------
        ldiu      1,r0
L219:        
        ash       *+fp(2),r0
        sti       r0,*+fp(3)
	.line	34
;----------------------------------------------------------------------
; 517 | WriteEDS( iADR_RAMA, wtmp );                            // Write Dither
;     |  RAM Address                                                           
;----------------------------------------------------------------------
        push      r0
        push      r4
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	35
;----------------------------------------------------------------------
; 518 | WriteEDS( addr, (USHORT) i );                           // Write RAM Da
;     | ta                                                                     
;----------------------------------------------------------------------
        ldiu      *+fp(2),r0
        push      r0
        ldiu      *+fp(5),r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	32
        ldiu      1,r0
        addi      *+fp(2),r0
        sti       r0,*+fp(2)
        cmpi      *+fp(6),r0
        bltd      L219
        ldilt     1,r0
	nop
        ldilt     1,r0
;*      Branch Occurs to L219 
	.line	38
;----------------------------------------------------------------------
; 521 | for(i=0;i<max_loop;i++){                                               
;----------------------------------------------------------------------
        ldiu      0,r0
L221:        
        sti       r0,*+fp(2)
        cmpi      *+fp(6),r0
        bged      L227
	nop
	nop
        ldige     258,r0
;*      Branch Occurs to L227 
	.line	39
;----------------------------------------------------------------------
; 522 | wtmp = ( 1 << i );                                                     
;----------------------------------------------------------------------
        ldiu      1,r0
L223:        
        ash       *+fp(2),r0
        sti       r0,*+fp(3)
	.line	40
;----------------------------------------------------------------------
; 523 | WriteEDS( iADR_RAMA, wtmp );            // Write Ram Address           
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      289,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	41
;----------------------------------------------------------------------
; 524 | rtmp = ReadEDS( addr );                         // Read RAM Data       
;----------------------------------------------------------------------
        ldiu      *+fp(5),r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(4)
	.line	43
;----------------------------------------------------------------------
; 526 | if( i != rtmp ){                                                       
;----------------------------------------------------------------------
        ldiu      *+fp(2),r0
        cmpi      *+fp(4),r0
        beqd      L225
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L225 
	.line	44
;----------------------------------------------------------------------
; 527 | error |= wtmp;                                                         
;----------------------------------------------------------------------
        ldiu      *+fp(3),r0
        or        *+fp(1),r0
        sti       r0,*+fp(1)
	.line	38
        ldiu      1,r0
L225:        
        addi      *+fp(2),r0
        sti       r0,*+fp(2)
        cmpi      *+fp(6),r0
        bltd      L223
	nop
	nop
        ldilt     1,r0
;*      Branch Occurs to L223 
	.line	48
;----------------------------------------------------------------------
; 531 | data = ReadEDS( iADR_CW );                                      // Read
;     |  the Control Word                                                      
;----------------------------------------------------------------------
        ldiu      258,r0
L227:        
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(8)
	.line	49
;----------------------------------------------------------------------
; 532 | data &= ~RAM_ACCESS;                                            // Disa
;     | ble Ram Access                                                         
;----------------------------------------------------------------------
        andn      16,r0
        sti       r0,*+fp(8)
	.line	50
;----------------------------------------------------------------------
; 533 | WriteEDS( iADR_CW, data );                                      // ReWr
;     | ite the Control Word                                                   
; 534 | #if NO_SCA == TRUE                                                     
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      258,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	52
;----------------------------------------------------------------------
; 535 | error = 0;                                                             
; 536 | #endif                                                                 
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
	.line	55
;----------------------------------------------------------------------
; 538 | return( error );                                                       
;----------------------------------------------------------------------
	.line	56
        ldiu      *-fp(1),bk
        pop       r4
        ldiu      *fp,fp
        subi      10,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	539,000000010h,8


	.sect	 ".text"

	.global	_Ram_Memory_Test
	.sym	_Ram_Memory_Test,_Ram_Memory_Test,47,2,0
	.func	543
;******************************************************************************
;* FUNCTION NAME: _Ram_Memory_Test                                            *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,fp,sp,st                                      *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 10 Auto + 0 SOE = 13 words        *
;******************************************************************************
_Ram_Memory_Test:
	.sym	_target,-2,4,9,32
	.sym	_error,1,15,1,32
	.sym	_i,2,4,1,32
	.sym	_j,3,4,1,32
	.sym	_wtmp,4,13,1,32
	.sym	_rtmp,5,13,1,32
	.sym	_addr,6,13,1,32
	.sym	_ram_max,7,4,1,32
	.sym	_data_max,8,4,1,32
	.sym	_status,9,13,1,32
	.sym	_data,10,13,1,32
	.line	1
;----------------------------------------------------------------------
; 543 | ULONG Ram_Memory_Test( int target )                                    
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      10,sp
	.line	2
	.line	3
;----------------------------------------------------------------------
; 545 | ULONG error = 0;                                                       
; 546 | int i;                                                                 
; 547 | int j;                                                                 
; 548 | USHORT wtmp;                                                           
; 549 | USHORT rtmp;                                                           
; 550 | USHORT addr;                                                           
; 551 | int ram_max;                                                           
; 552 | int data_max;                                                          
; 553 | USHORT status;                                                         
; 554 | USHORT data;                                                           
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
	.line	14
;----------------------------------------------------------------------
; 556 | WriteEDS( iADR_TP_SEL, 0x02  << 12 );                                  
;----------------------------------------------------------------------
        ldiu      8192,r1
        push      r1
        ldiu      264,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	16
;----------------------------------------------------------------------
; 558 | status = ( ReadEDS( iADR_STAT ) >> 3 );                                
;----------------------------------------------------------------------
        ldiu      257,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        lsh       -3,r0
        sti       r0,*+fp(9)
	.line	17
;----------------------------------------------------------------------
; 559 | if(( status & 0x1 ) == 1 ){                                            
;----------------------------------------------------------------------
        ldiu      1,r0
        and       *+fp(9),r0
        cmpi      1,r0
        bned      L232
	nop
        ldine     0,r0
        ldine     276,r1
;*      Branch Occurs to L232 
	.line	18
;----------------------------------------------------------------------
; 560 | error = 0x10000000;                                                    
;----------------------------------------------------------------------
        ldiu      @CL4,r0
        sti       r0,*+fp(1)
	.line	20
;----------------------------------------------------------------------
; 562 | WriteEDS( iADR_TIME_STOP, 0x0 );                        // Stop EDS Ana
;     | lysis                                                                  
;----------------------------------------------------------------------
        ldiu      0,r0
        ldiu      276,r1
L232:        
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	21
;----------------------------------------------------------------------
; 563 | WriteEDS( iADR_RAMA, 0 );                                       // Rese
;     | t the Ram Address                                                      
;----------------------------------------------------------------------
        ldiu      0,r0
        ldiu      289,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	23
;----------------------------------------------------------------------
; 565 | data = ReadEDS( iADR_CW );                                      // Read
;     |  the Control Word                                                      
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(10)
	.line	24
;----------------------------------------------------------------------
; 566 | data |= RAM_ACCESS;
;     |  // Enable RAM Access                                                  
;----------------------------------------------------------------------
        ldiu      16,r0
        or        *+fp(10),r0
        sti       r0,*+fp(10)
	.line	25
;----------------------------------------------------------------------
; 567 | WriteEDS( iADR_CW, data );                                      // ReWr
;     | ite the Control Word                                                   
; 569 | switch ( target )                                                      
;----------------------------------------------------------------------
        ldiu      258,r0
        ldiu      *+fp(10),r1
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        bud       L238
        subi      2,sp
	nop
	nop
;*      Branch Occurs to L238 
	.line	29
;----------------------------------------------------------------------
; 571 | case 0 : ram_max = 4096; data_max = SCA_BITS;  addr = iADR_SCA_LU; brea
;     | k;               // 4K                                                 
;----------------------------------------------------------------------
L234:        
        sti       r0,*+fp(7)
        ldiu      8,r0
        bud       L240
        sti       r0,*+fp(8)
        ldiu      291,r0
        sti       r0,*+fp(6)
;*      Branch Occurs to L240 
	.line	30
;----------------------------------------------------------------------
; 572 | case 1 : ram_max = 256;  data_max = 16;           addr = iADR_ROI_LU; b
;     | reak;            // 256                                                
; 575 | //----- Dither RAM Memory Test                                         
;----------------------------------------------------------------------
L236:        
        sti       r0,*+fp(7)
        ldiu      16,r0
        bud       L240
        sti       r0,*+fp(8)
        ldiu      293,r0
        sti       r0,*+fp(6)
;*      Branch Occurs to L240 
	.line	27
L238:        
        ldi       *-fp(2),r0
        beqd      L234
	nop
	nop
        ldieq     4096,r0
;*      Branch Occurs to L234 
        cmpi      1,r0
        beqd      L236
	nop
	nop
        ldieq     256,r0
;*      Branch Occurs to L236 
L240:        
	.line	34
;----------------------------------------------------------------------
; 576 | for(i=0;i<ram_max;i++){                                                
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(2)
        cmpi      *+fp(7),r0
        bged      L250
	nop
	nop
        ldige     258,r0
;*      Branch Occurs to L250 
	.line	35
;----------------------------------------------------------------------
; 577 | for(j=0;j<data_max;j++){                                               
;----------------------------------------------------------------------
        ldiu      0,r0
L242:        
        sti       r0,*+fp(3)
        cmpi      *+fp(8),r0
        bged      L248
	nop
	nop
        ldige     1,r0
;*      Branch Occurs to L248 
	.line	36
;----------------------------------------------------------------------
; 578 | wtmp = 1 << j;                                                         
;----------------------------------------------------------------------
        ldiu      1,r0
L244:        
        ash       *+fp(3),r0
        sti       r0,*+fp(4)
	.line	37
;----------------------------------------------------------------------
; 579 | WriteEDS( iADR_RAMA, (USHORT) i );              // Load Ram Address    
;----------------------------------------------------------------------
        ldiu      *+fp(2),r1
        push      r1
        ldiu      289,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	38
;----------------------------------------------------------------------
; 580 | WriteEDS( addr, wtmp );                                         // Writ
;     | e DitherRAM DAta                                                       
;----------------------------------------------------------------------
        ldiu      *+fp(4),r0
        push      r0
        ldiu      *+fp(6),r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	39
;----------------------------------------------------------------------
; 581 | rtmp = ReadEDS( addr );                                                
;----------------------------------------------------------------------
        ldiu      *+fp(6),r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(5)
	.line	40
;----------------------------------------------------------------------
; 582 | if( wtmp != rtmp ){                                                    
;----------------------------------------------------------------------
        ldiu      *+fp(4),r0
        cmpi      *+fp(5),r0
        beqd      L246
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L246 
	.line	41
;----------------------------------------------------------------------
; 583 | error ++;                                                              
; 585 | }                               // for j...                            
; 586 | }                                       // for i...                    
;----------------------------------------------------------------------
        ldiu      1,r0
        addi      *+fp(1),r0            ; Unsigned
        sti       r0,*+fp(1)
	.line	35
        ldiu      1,r0
L246:        
        addi      *+fp(3),r0
        sti       r0,*+fp(3)
        cmpi      *+fp(8),r0
        bltd      L244
	nop
	nop
        ldilt     1,r0
;*      Branch Occurs to L244 
	.line	34
        ldiu      1,r0
L248:        
        addi      *+fp(2),r0
        sti       r0,*+fp(2)
        cmpi      *+fp(7),r0
        bltd      L242
	nop
	nop
        ldilt     0,r0
;*      Branch Occurs to L242 
	.line	46
;----------------------------------------------------------------------
; 588 | data = ReadEDS( iADR_CW );                                      // Read
;     |  the Control Word                                                      
;----------------------------------------------------------------------
        ldiu      258,r0
L250:        
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(10)
	.line	47
;----------------------------------------------------------------------
; 589 | data &= ~RAM_ACCESS;                                            // Disa
;     | ble Ram Access                                                         
;----------------------------------------------------------------------
        andn      16,r0
        sti       r0,*+fp(10)
	.line	48
;----------------------------------------------------------------------
; 590 | WriteEDS( iADR_CW, data );                                      // ReWr
;     | ite the Control Word                                                   
; 591 | #if NO_SCA == TRUE                                                     
;----------------------------------------------------------------------
        ldiu      r0,r1
        ldiu      258,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	50
;----------------------------------------------------------------------
; 592 | error = 0;                                                             
; 593 | #endif                                                                 
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
	.line	52
;----------------------------------------------------------------------
; 594 | return( error );                                                       
;----------------------------------------------------------------------
	.line	53
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      12,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	595,000000000h,10


	.sect	 ".text"

	.global	_Dac_Test
	.sym	_Dac_Test,_Dac_Test,47,2,0
	.func	599
;******************************************************************************
;* FUNCTION NAME: _Dac_Test                                                   *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,ar0,fp,ir0,sp,st                              *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 1 Parm + 10 Auto + 0 SOE = 13 words        *
;******************************************************************************
_Dac_Test:
	.sym	_target,-2,4,9,32
	.sym	_error,1,15,1,32
	.sym	_done,2,15,1,32
	.sym	_addr,3,13,1,32
	.sym	_tpsel,4,13,1,32
	.sym	_data,5,13,1,32
	.sym	_rdata,6,13,1,32
	.sym	_comp,7,13,1,32
	.sym	_cmp_data,8,13,1,32
	.sym	_max_loop,9,4,1,32
	.sym	_i,10,4,1,32
	.line	1
;----------------------------------------------------------------------
; 599 | ULONG   Dac_Test( int target )                                         
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      10,sp
	.line	2
	.line	3
;----------------------------------------------------------------------
; 601 | ULONG error = 0;                                                       
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
	.line	4
;----------------------------------------------------------------------
; 602 | ULONG done = 0;                                                        
; 603 | USHORT addr;                                                           
; 604 | USHORT tpsel;                                                          
;----------------------------------------------------------------------
        sti       r0,*+fp(2)
	.line	7
;----------------------------------------------------------------------
; 605 | USHORT data = 0;                                                       
; 606 | USHORT rdata;                                                          
; 607 | USHORT comp;                                                           
; 608 | USHORT cmp_data;                                                       
; 609 | int max_loop;                                                          
; 610 | int i;                                                                 
;----------------------------------------------------------------------
        sti       r0,*+fp(5)
	.line	14
;----------------------------------------------------------------------
; 612 | WriteEDS( iADR_BIT_PEAK1, 0x0 );                                       
;----------------------------------------------------------------------
        ldiu      0,r1
        push      r1
        ldiu      309,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	15
;----------------------------------------------------------------------
; 613 | WriteEDS( iADR_BIT_PEAK2, 0x0 );                                       
;----------------------------------------------------------------------
        ldiu      0,r0
        ldiu      310,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	17
;----------------------------------------------------------------------
; 615 | data = ReadEDS( iADR_CW );                                             
;----------------------------------------------------------------------
        ldiu      258,r0
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(5)
	.line	18
;----------------------------------------------------------------------
; 616 | data &= ~( 3 << 11 );                                           // Clea
;     | r PreAmp Source Bits                                                   
;----------------------------------------------------------------------
        andn      6144,r0
        sti       r0,*+fp(5)
	.line	19
;----------------------------------------------------------------------
; 617 | data &= ~( 1 << 13 );                                           // Set
;     | Input Gain to 1X                                                       
;----------------------------------------------------------------------
        andn      8192,r0
        sti       r0,*+fp(5)
	.line	20
;----------------------------------------------------------------------
; 618 | data |= ( 2 << 11 );                                            // Set
;     | the PreAmp Source to BIT DAC                                           
;----------------------------------------------------------------------
        ldiu      4096,r0
        or        *+fp(5),r0
        sti       r0,*+fp(5)
	.line	23
;----------------------------------------------------------------------
; 621 | tpsel = 0x2  << 12 ;                                            // DAC8
;     | 420 Debug                                                              
;----------------------------------------------------------------------
        ldiu      8192,r0
        sti       r0,*+fp(4)
	.line	24
;----------------------------------------------------------------------
; 622 | tpsel = 0x4 << 12 ;
;     |  // ADS8321 Debug                                                      
;----------------------------------------------------------------------
        ldiu      16384,r0
        sti       r0,*+fp(4)
	.line	25
;----------------------------------------------------------------------
; 623 | tpsel = 0x7 << 12 ;
;     |  // LTC1595 Debug                                                      
;----------------------------------------------------------------------
        ldiu      28672,r0
        sti       r0,*+fp(4)
	.line	26
;----------------------------------------------------------------------
; 624 | tpsel = 0x0 << 12 ;
;     |  // AD9260  Debug                                                      
;----------------------------------------------------------------------
        ldiu      0,r0
	.line	27
;----------------------------------------------------------------------
; 625 | max_loop = 5;                                                          
; 626 | switch( target ){                                                      
;----------------------------------------------------------------------
        bud       L269
        sti       r0,*+fp(4)
        ldiu      5,r0
        sti       r0,*+fp(9)
;*      Branch Occurs to L269 
L254:        
	.line	29
;----------------------------------------------------------------------
; 627 | case 0x0 : addr = iADR_DISC;    tpsel |= ATP_DISC1;             break;
;     |  // Disc 1 Test Point                                                  
;----------------------------------------------------------------------
        ldiu      271,r0
        sti       r0,*+fp(3)
        bud       L271
        ldiu      768,r0
        or        *+fp(4),r0
        sti       r0,*+fp(4)
;*      Branch Occurs to L271 
L255:        
	.line	30
;----------------------------------------------------------------------
; 628 | case 0x1 : addr = iADR_EVCH1;   tpsel |= ATP_DISC2;             break;
;     |  // Disc 2 Test Point                                                  
;----------------------------------------------------------------------
        ldiu      272,r0
        sti       r0,*+fp(3)
        bud       L271
        ldiu      512,r0
        or        *+fp(4),r0
        sti       r0,*+fp(4)
;*      Branch Occurs to L271 
L256:        
	.line	31
;----------------------------------------------------------------------
; 629 | case 0x2 : addr = iADR_EVCH0;   tpsel |= ATP_EVCH;              break;
;     |  // EV/CH Test Point                                                   
;----------------------------------------------------------------------
        ldiu      273,r0
        sti       r0,*+fp(3)
        bud       L272
        ldiu      *+fp(5),r1
        ldiu      258,r0
        push      r1
;*      Branch Occurs to L272 
L257:        
	.line	32
;----------------------------------------------------------------------
; 630 | case 0x3 : addr = iADR_BIT;             tpsel |= ATP_BIT_DAC;   break;
;     |  // BIT DAC Test Point                                                 
;----------------------------------------------------------------------
        ldiu      274,r0
        sti       r0,*+fp(3)
        bud       L271
        ldiu      256,r0
        or        *+fp(4),r0
        sti       r0,*+fp(4)
;*      Branch Occurs to L271 
L258:        
	.line	33
;----------------------------------------------------------------------
; 631 | case 0x4 : addr = iADR_BIT;             tpsel |= ATP_PAMP;      break;
;     |  // Post AMP Test Point X1 Gain                                        
; 632 | case 0x5 :                                                             
;----------------------------------------------------------------------
        ldiu      274,r0
        sti       r0,*+fp(3)
        bud       L271
        ldiu      1024,r0
        or        *+fp(4),r0
        sti       r0,*+fp(4)
;*      Branch Occurs to L271 
L259:        
	.line	35
;----------------------------------------------------------------------
; 633 | max_loop = 3;
;     |                  // Restrict due to Over-Drive                         
;----------------------------------------------------------------------
        ldiu      3,r0
        sti       r0,*+fp(9)
	.line	36
;----------------------------------------------------------------------
; 634 | data |= ( 1 << 13 );
;     |                  // Set Input Gain to 2X                               
;----------------------------------------------------------------------
        ldiu      8192,r0
        or        *+fp(5),r0
        sti       r0,*+fp(5)
	.line	37
;----------------------------------------------------------------------
; 635 | addr = iADR_BIT;                                                       
;----------------------------------------------------------------------
        ldiu      274,r0
        sti       r0,*+fp(3)
	.line	38
;----------------------------------------------------------------------
; 636 | tpsel |= ATP_PAMP;                                                     
;----------------------------------------------------------------------
	.line	39
;----------------------------------------------------------------------
; 637 | break;  // Post AMP Test Point X2 Gain                                 
; 638 | case 0x6 :                                                             
;----------------------------------------------------------------------
        bud       L271
        ldiu      1024,r0
        or        *+fp(4),r0
        sti       r0,*+fp(4)
;*      Branch Occurs to L271 
L260:        
	.line	41
;----------------------------------------------------------------------
; 639 | WriteEDS( iADR_BIT, 0xFFF );                                           
;----------------------------------------------------------------------
        ldiu      4095,r1
        ldiu      274,r0
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	42
;----------------------------------------------------------------------
; 640 | addr = iADR_CGAIN;                                                     
;----------------------------------------------------------------------
        ldiu      266,r0
        sti       r0,*+fp(3)
	.line	43
;----------------------------------------------------------------------
; 641 | tpsel |= ATP_CGAIN;                                                    
;----------------------------------------------------------------------
	.line	44
;----------------------------------------------------------------------
; 642 | break;  // Coarse Gain Out                                             
; 643 | case 0x7 :                                                             
;----------------------------------------------------------------------
        bud       L271
        ldiu      1280,r0
        or        *+fp(4),r0
        sti       r0,*+fp(4)
;*      Branch Occurs to L271 
L261:        
	.line	46
;----------------------------------------------------------------------
; 644 | addr = iADR_BIT;                                                       
;----------------------------------------------------------------------
        ldiu      274,r0
        sti       r0,*+fp(3)
	.line	47
;----------------------------------------------------------------------
; 645 | tpsel |= ATP_BFILTER;                                                  
;----------------------------------------------------------------------
	.line	49
;----------------------------------------------------------------------
; 647 | break;  // Bessell Filter                                              
; 648 | case 0x8 :                                                             
;----------------------------------------------------------------------
        bud       L271
        ldiu      2816,r0
        or        *+fp(4),r0
        sti       r0,*+fp(4)
;*      Branch Occurs to L271 
L262:        
	.line	51
;----------------------------------------------------------------------
; 649 | addr = iADR_BIT;                                                       
;----------------------------------------------------------------------
        ldiu      274,r0
        sti       r0,*+fp(3)
	.line	52
;----------------------------------------------------------------------
; 650 | tpsel |= ATP_AFILTER;                                                  
;----------------------------------------------------------------------
	.line	53
;----------------------------------------------------------------------
; 651 | break;  // Anti-Alias                                                  
; 652 | case 0x9 :                                                             
;----------------------------------------------------------------------
        bud       L271
        ldiu      2048,r0
        or        *+fp(4),r0
        sti       r0,*+fp(4)
;*      Branch Occurs to L271 
L263:        
	.line	55
;----------------------------------------------------------------------
; 653 | addr = iADR_BIT;                                                       
;----------------------------------------------------------------------
        ldiu      274,r0
        sti       r0,*+fp(3)
	.line	56
;----------------------------------------------------------------------
; 654 | tpsel |= ATP_ADCP;                                                     
;----------------------------------------------------------------------
	.line	57
;----------------------------------------------------------------------
; 655 | break;  // ADCIn Positive                                              
; 656 | case 0xA :                                                             
;----------------------------------------------------------------------
        bud       L271
        ldiu      2304,r0
        or        *+fp(4),r0
        sti       r0,*+fp(4)
;*      Branch Occurs to L271 
L264:        
	.line	59
;----------------------------------------------------------------------
; 657 | addr = iADR_BIT;                                                       
;----------------------------------------------------------------------
        ldiu      274,r0
        sti       r0,*+fp(3)
	.line	60
;----------------------------------------------------------------------
; 658 | tpsel |= ATP_ADCN;                                                     
;----------------------------------------------------------------------
	.line	61
;----------------------------------------------------------------------
; 659 | break;  // ADCIn Negative                                              
; 660 | case 0xB :
;     |                                                                        
;     |           // Disc Ev/Ch                                                
;----------------------------------------------------------------------
        bud       L271
        ldiu      2560,r0
        or        *+fp(4),r0
        sti       r0,*+fp(4)
;*      Branch Occurs to L271 
L265:        
	.line	63
;----------------------------------------------------------------------
; 661 | data |= ( 1 << 13 );                                            // Set
;     | Input Gain to 2X                                                       
;----------------------------------------------------------------------
        ldiu      8192,r0
        or        *+fp(5),r0
        sti       r0,*+fp(5)
	.line	64
;----------------------------------------------------------------------
; 662 | data |= 1;
;     |          // Set BIT Mode Enable                                        
;----------------------------------------------------------------------
        ldiu      1,r0
        or        *+fp(5),r0
        sti       r0,*+fp(5)
	.line	65
;----------------------------------------------------------------------
; 663 | WriteEDS( iADR_BIT, 0xFFF );                                           
;----------------------------------------------------------------------
        ldiu      4095,r1
        push      r1
        ldiu      274,r0
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	66
;----------------------------------------------------------------------
; 664 | addr = iADR_DISC_EVCH;                                                 
;----------------------------------------------------------------------
        ldiu      379,r0
        sti       r0,*+fp(3)
	.line	67
;----------------------------------------------------------------------
; 665 | tpsel |= ATP_DISC_EVCH;                                                
;----------------------------------------------------------------------
        ldiu      3584,r0
        or        *+fp(4),r0
	.line	68
;----------------------------------------------------------------------
; 666 | max_loop = 4;                                                          
;----------------------------------------------------------------------
	.line	69
;----------------------------------------------------------------------
; 667 | break;                                                                 
; 668 | case 0xC :
;     |                                                                        
;     |           // Disc Inv                                                  
;----------------------------------------------------------------------
        bud       L271
        sti       r0,*+fp(4)
        ldiu      4,r0
        sti       r0,*+fp(9)
;*      Branch Occurs to L271 
L266:        
	.line	71
;----------------------------------------------------------------------
; 669 | addr = iADR_BIT;                                                       
;----------------------------------------------------------------------
        ldiu      274,r0
        sti       r0,*+fp(3)
	.line	72
;----------------------------------------------------------------------
; 670 | tpsel   |= ATP_DISC_INV;                                               
;----------------------------------------------------------------------
	.line	73
;----------------------------------------------------------------------
; 671 | break;                                                                 
;----------------------------------------------------------------------
        bud       L271
        ldiu      3328,r0
        or        *+fp(4),r0
        sti       r0,*+fp(4)
;*      Branch Occurs to L271 
L267:        
	.line	74
;----------------------------------------------------------------------
; 672 | default :       return( 0xFFFF );
;     |                  break;  // Invalid Case so Exit                       
;----------------------------------------------------------------------
        bud       L419
	nop
	nop
        ldiu      @CL5,r0
;*      Branch Occurs to L419 
L269:        
	.line	28
        ldiu      *-fp(2),ir0
        cmpi      12,ir0
        bhid      L267
	nop
	nop
        ldils     @CL6,ar0
;*      Branch Occurs to L267 
        ldiu      *+ar0(ir0),r0
        bu        r0

	.sect	".text"
SW1:	.word	L254	; 0
	.word	L255	; 1
	.word	L256	; 2
	.word	L257	; 3
	.word	L258	; 4
	.word	L259	; 5
	.word	L260	; 6
	.word	L261	; 7
	.word	L262	; 8
	.word	L263	; 9
	.word	L264	; 10
	.word	L265	; 11
	.word	L266	; 12
	.sect	".text"
;*      Branch Occurs to r0 
L271:        
	.line	76
;----------------------------------------------------------------------
; 674 | WriteEDS( iADR_CW, data );
;     |                                          // ReWrite the Control Word   
;----------------------------------------------------------------------
        ldiu      *+fp(5),r1
        ldiu      258,r0
        push      r1
L272:        
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	77
;----------------------------------------------------------------------
; 675 | WriteEDS( iADR_TP_SEL, tpsel );
;     |                                  // Write the Test Point Select        
;----------------------------------------------------------------------
        ldiu      264,r1
        ldiu      *+fp(4),r0
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	79
;----------------------------------------------------------------------
; 677 | error = 0;                                                             
;----------------------------------------------------------------------
        ldiu      0,r0
        sti       r0,*+fp(1)
	.line	80
;----------------------------------------------------------------------
; 678 | for(i=0;i<max_loop;i++){                                               
; 679 |         switch( i ){                                                   
;----------------------------------------------------------------------
        sti       r0,*+fp(10)
        cmpi      *+fp(9),r0
        bged      L418
	nop
	nop
        ldige     258,r0
;*      Branch Occurs to L418 
        bu        L280
;*      Branch Occurs to L280 
L274:        
	.line	82
;----------------------------------------------------------------------
; 680 | case 0 : data = 0x00; break;                                           
;----------------------------------------------------------------------
        bud       L405
        ldiu      0,r0
        sti       r0,*+fp(5)
        ldiu      *-fp(2),ir0
;*      Branch Occurs to L405 
L275:        
	.line	83
;----------------------------------------------------------------------
; 681 | case 1 : data = 0x3FF; break;                                          
;----------------------------------------------------------------------
        bud       L405
        ldiu      1023,r0
        sti       r0,*+fp(5)
        ldiu      *-fp(2),ir0
;*      Branch Occurs to L405 
L276:        
	.line	84
;----------------------------------------------------------------------
; 682 | case 2 : data = 0x7FF; break;                                          
;----------------------------------------------------------------------
        bud       L405
        ldiu      2047,r0
        sti       r0,*+fp(5)
        ldiu      *-fp(2),ir0
;*      Branch Occurs to L405 
L277:        
	.line	85
;----------------------------------------------------------------------
; 683 | case 3 : data = 0xBFF; break;                                          
;----------------------------------------------------------------------
        bud       L405
        ldiu      3071,r0
        sti       r0,*+fp(5)
        ldiu      *-fp(2),ir0
;*      Branch Occurs to L405 
L278:        
	.line	86
;----------------------------------------------------------------------
; 684 | case 4 : data = 0xFFF; break;                                          
; 687 | switch( target ){                                                      
; 688 | case 0x0 :                                                             
; 689 | case 0x1 :                                                             
; 690 | case 0x2 :                                                             
; 691 | case 0x3 :                                                             
; 692 | default  :                                                             
; 693 |                 switch(i){                                             
;----------------------------------------------------------------------
        bud       L405
        ldiu      4095,r0
        sti       r0,*+fp(5)
        ldiu      *-fp(2),ir0
;*      Branch Occurs to L405 
L280:        
	.line	81
        ldi       *+fp(10),r0
        beq       L274
;*      Branch Occurs to L274 
        cmpi      1,r0
        beq       L275
;*      Branch Occurs to L275 
        cmpi      2,r0
        beq       L276
;*      Branch Occurs to L276 
        cmpi      3,r0
        beq       L277
;*      Branch Occurs to L277 
        cmpi      4,r0
        beq       L278
;*      Branch Occurs to L278 
        bud       L406
	nop
        ldiu      *-fp(2),ir0
        cmpi      12,ir0
;*      Branch Occurs to L406 
L287:        
	.line	96
;----------------------------------------------------------------------
; 694 | case 0 : comp = 0x0E; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      14,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L288:        
	.line	97
;----------------------------------------------------------------------
; 695 | case 1 : comp = 0x2B; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      43,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L289:        
	.line	98
;----------------------------------------------------------------------
; 696 | case 2 : comp = 0x47; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      71,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L290:        
	.line	99
;----------------------------------------------------------------------
; 697 | case 3 : comp = 0x63; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      99,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L291:        
	.line	100
;----------------------------------------------------------------------
; 698 | case 4 : comp = 0x80; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      128,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L293:        
	.line	95
        ldi       *+fp(10),r0
        beq       L287
;*      Branch Occurs to L287 
        cmpi      1,r0
        beq       L288
;*      Branch Occurs to L288 
        cmpi      2,r0
        beq       L289
;*      Branch Occurs to L289 
        cmpi      3,r0
        beq       L290
;*      Branch Occurs to L290 
        cmpi      4,r0
        beq       L291
;*      Branch Occurs to L291 
        bud       L410
        ldiu      *+fp(5),r0
        push      r0
        ldiu      *+fp(3),r0
;*      Branch Occurs to L410 
	.line	102
;----------------------------------------------------------------------
; 700 | break;                                                                 
; 702 | case 0x4 :                                                             
; 703 | switch(i){                                                             
;----------------------------------------------------------------------
L300:        
	.line	106
;----------------------------------------------------------------------
; 704 | case 0 : comp = 0x35; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      53,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L301:        
	.line	107
;----------------------------------------------------------------------
; 705 | case 1 : comp = 0x3C; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      60,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L302:        
	.line	108
;----------------------------------------------------------------------
; 706 | case 2 : comp = 0x43; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      67,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L303:        
	.line	109
;----------------------------------------------------------------------
; 707 | case 3 : comp = 0x4a; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      74,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L304:        
	.line	110
;----------------------------------------------------------------------
; 708 | case 4 : comp = 0x51; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      81,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L306:        
	.line	105
        ldi       *+fp(10),r0
        beq       L300
;*      Branch Occurs to L300 
        cmpi      1,r0
        beq       L301
;*      Branch Occurs to L301 
        cmpi      2,r0
        beq       L302
;*      Branch Occurs to L302 
        cmpi      3,r0
        beq       L303
;*      Branch Occurs to L303 
        cmpi      4,r0
        beq       L304
;*      Branch Occurs to L304 
        bud       L410
        ldiu      *+fp(5),r0
        push      r0
        ldiu      *+fp(3),r0
;*      Branch Occurs to L410 
	.line	112
;----------------------------------------------------------------------
; 710 | break;                                                                 
; 711 | case 0x5 :                                                             
; 712 | switch(i){                                                             
;----------------------------------------------------------------------
L313:        
	.line	115
;----------------------------------------------------------------------
; 713 | case 0 : comp = 0x26; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      38,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L314:        
	.line	116
;----------------------------------------------------------------------
; 714 | case 1 : comp = 0x35; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      53,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L315:        
	.line	117
;----------------------------------------------------------------------
; 715 | case 2 : comp = 0x43; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      67,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L317:        
	.line	114
        ldi       *+fp(10),r0
        beq       L313
;*      Branch Occurs to L313 
        cmpi      1,r0
        beq       L314
;*      Branch Occurs to L314 
        cmpi      2,r0
        beq       L315
;*      Branch Occurs to L315 
        bud       L410
        ldiu      *+fp(5),r0
        push      r0
        ldiu      *+fp(3),r0
;*      Branch Occurs to L410 
	.line	119
;----------------------------------------------------------------------
; 717 | break;                                                                 
; 719 | case 0x6 :                                                             
; 720 | switch(i){                                                             
;----------------------------------------------------------------------
	.line	123
;----------------------------------------------------------------------
; 721 | case 0 : data = 0x0000; comp = 0x45; break;                            
;----------------------------------------------------------------------
L322:        
        bud       L408
        sti       r0,*+fp(5)
        ldiu      69,r0
        sti       r0,*+fp(7)
;*      Branch Occurs to L408 
	.line	124
;----------------------------------------------------------------------
; 722 | case 1 : data = 0x3FFF; comp = 0x49; break;                            
;----------------------------------------------------------------------
L324:        
        bud       L408
        sti       r0,*+fp(5)
        ldiu      73,r0
        sti       r0,*+fp(7)
;*      Branch Occurs to L408 
	.line	125
;----------------------------------------------------------------------
; 723 | case 2 : data = 0x7FFF; comp = 0x4c; break;                            
;----------------------------------------------------------------------
L326:        
        bud       L408
        sti       r0,*+fp(5)
        ldiu      76,r0
        sti       r0,*+fp(7)
;*      Branch Occurs to L408 
	.line	126
;----------------------------------------------------------------------
; 724 | case 3 : data = 0xBFFF; comp = 0x50; break;                            
;----------------------------------------------------------------------
L328:        
        bud       L408
        sti       r0,*+fp(5)
        ldiu      80,r0
        sti       r0,*+fp(7)
;*      Branch Occurs to L408 
	.line	127
;----------------------------------------------------------------------
; 725 | case 4 : data = 0xFFFF; comp = 0x53; break;                            
;----------------------------------------------------------------------
L330:        
        bud       L408
        sti       r0,*+fp(5)
        ldiu      83,r0
        sti       r0,*+fp(7)
;*      Branch Occurs to L408 
L332:        
	.line	122
        ldi       *+fp(10),r0
        beqd      L322
	nop
	nop
        ldieq     0,r0
;*      Branch Occurs to L322 
        cmpi      1,r0
        beqd      L324
	nop
	nop
        ldieq     16383,r0
;*      Branch Occurs to L324 
        cmpi      2,r0
        beqd      L326
	nop
	nop
        ldieq     32767,r0
;*      Branch Occurs to L326 
        cmpi      3,r0
        beqd      L328
	nop
	nop
        ldieq     @CL7,r0
;*      Branch Occurs to L328 
        cmpi      4,r0
        beqd      L330
	nop
	nop
        ldieq     @CL8,r0
;*      Branch Occurs to L330 
        bud       L410
        ldiu      *+fp(5),r0
        push      r0
        ldiu      *+fp(3),r0
;*      Branch Occurs to L410 
	.line	129
;----------------------------------------------------------------------
; 727 | break;                                                                 
; 729 | case 0x7 :                                                             
; 730 | case 0x8 :                                                             
; 731 | switch(i){                                                             
;----------------------------------------------------------------------
L339:        
	.line	134
;----------------------------------------------------------------------
; 732 | case 0 : comp = 0x37; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      55,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L340:        
	.line	135
;----------------------------------------------------------------------
; 733 | case 1 : comp = 0x3e; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      62,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L341:        
	.line	136
;----------------------------------------------------------------------
; 734 | case 2 : comp = 0x45; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      69,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L342:        
	.line	137
;----------------------------------------------------------------------
; 735 | case 3 : comp = 0x4c; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      76,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L343:        
	.line	138
;----------------------------------------------------------------------
; 736 | case 4 : comp = 0x53; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      83,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L345:        
	.line	133
        ldi       *+fp(10),r0
        beq       L339
;*      Branch Occurs to L339 
        cmpi      1,r0
        beq       L340
;*      Branch Occurs to L340 
        cmpi      2,r0
        beq       L341
;*      Branch Occurs to L341 
        cmpi      3,r0
        beq       L342
;*      Branch Occurs to L342 
        cmpi      4,r0
        beq       L343
;*      Branch Occurs to L343 
        bud       L410
        ldiu      *+fp(5),r0
        push      r0
        ldiu      *+fp(3),r0
;*      Branch Occurs to L410 
	.line	140
;----------------------------------------------------------------------
; 738 | break;                                                                 
; 740 | case 0x9 :                                                             
; 741 | switch(i){                                                             
;----------------------------------------------------------------------
L352:        
	.line	144
;----------------------------------------------------------------------
; 742 | case 0 : comp = 0x8A; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      138,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L353:        
	.line	145
;----------------------------------------------------------------------
; 743 | case 1 : comp = 0x83; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      131,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L354:        
	.line	146
;----------------------------------------------------------------------
; 744 | case 2 : comp = 0x7D; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      125,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L355:        
	.line	147
;----------------------------------------------------------------------
; 745 | case 3 : comp = 0x75; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      117,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L356:        
	.line	148
;----------------------------------------------------------------------
; 746 | case 4 : comp = 0x6E; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      110,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L358:        
	.line	143
        ldi       *+fp(10),r0
        beq       L352
;*      Branch Occurs to L352 
        cmpi      1,r0
        beq       L353
;*      Branch Occurs to L353 
        cmpi      2,r0
        beq       L354
;*      Branch Occurs to L354 
        cmpi      3,r0
        beq       L355
;*      Branch Occurs to L355 
        cmpi      4,r0
        beq       L356
;*      Branch Occurs to L356 
        bud       L410
        ldiu      *+fp(5),r0
        push      r0
        ldiu      *+fp(3),r0
;*      Branch Occurs to L410 
	.line	150
;----------------------------------------------------------------------
; 748 | break;                                                                 
; 750 | //                                      switch(i){                     
; 751 | //                                              case 0 : comp = 0x81; b
;     | reak;                                                                  
; 752 | //                                              case 1 : comp = 0x7A; b
;     | reak;                                                                  
; 753 | //                                              case 2 : comp = 0x72; b
;     | reak;                                                                  
; 754 | //                                              case 3 : comp = 0x6B; b
;     | reak;                                                                  
; 755 | //                                              case 4 : comp = 0x64; b
;     | reak;                                                                  
; 756 | //                                      }                              
; 757 | //                                      break;                         
; 759 | case 0xA :                                                             
; 760 | switch(i){                                                             
;----------------------------------------------------------------------
L365:        
	.line	163
;----------------------------------------------------------------------
; 761 | case 0 : comp = 0x71; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      113,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L366:        
	.line	164
;----------------------------------------------------------------------
; 762 | case 1 : comp = 0x78; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      120,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L367:        
	.line	165
;----------------------------------------------------------------------
; 763 | case 2 : comp = 0x7F; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      127,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L368:        
	.line	166
;----------------------------------------------------------------------
; 764 | case 3 : comp = 0x87; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      135,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L369:        
	.line	167
;----------------------------------------------------------------------
; 765 | case 4 : comp = 0x8E; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      142,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L371:        
	.line	162
        ldi       *+fp(10),r0
        beq       L365
;*      Branch Occurs to L365 
        cmpi      1,r0
        beq       L366
;*      Branch Occurs to L366 
        cmpi      2,r0
        beq       L367
;*      Branch Occurs to L367 
        cmpi      3,r0
        beq       L368
;*      Branch Occurs to L368 
        cmpi      4,r0
        beq       L369
;*      Branch Occurs to L369 
        bud       L410
        ldiu      *+fp(5),r0
        push      r0
        ldiu      *+fp(3),r0
;*      Branch Occurs to L410 
	.line	169
;----------------------------------------------------------------------
; 767 | break;                                                                 
; 768 | //                      switch(i){                                     
; 769 | //                              case 0 : comp = 0x68; break;           
; 770 | //                              case 1 : comp = 0x6f; break;           
; 771 | //                              case 2 : comp = 0x76; break;           
; 772 | //                              case 3 : comp = 0x7d; break;           
; 773 | //                              case 4 : comp = 0x85; break;           
; 774 | //                      }                                              
; 775 | //                      break;                                         
; 778 | case 0xb :                                                             
; 779 | switch(i) {                                                            
;----------------------------------------------------------------------
	.line	182
;----------------------------------------------------------------------
; 780 | case 0 : data = 3;      comp = 0x49; break;             // 20eV/CH X1  
;----------------------------------------------------------------------
L378:        
        bud       L408
        sti       r0,*+fp(5)
        ldiu      73,r0
        sti       r0,*+fp(7)
;*      Branch Occurs to L408 
	.line	183
;----------------------------------------------------------------------
; 781 | case 1 : data = 2;      comp = 0x4b; break;             // 10eV/CH /2  
;----------------------------------------------------------------------
L380:        
        bud       L408
        sti       r0,*+fp(5)
        ldiu      75,r0
        sti       r0,*+fp(7)
;*      Branch Occurs to L408 
	.line	184
;----------------------------------------------------------------------
; 782 | case 2 : data = 1;      comp = 0x51; break;             //  5eV/CH /4  
;----------------------------------------------------------------------
L382:        
        bud       L408
        sti       r0,*+fp(5)
        ldiu      81,r0
        sti       r0,*+fp(7)
;*      Branch Occurs to L408 
	.line	185
;----------------------------------------------------------------------
; 783 | case 3 : data = 0;      comp = 0x5f; break;             // 2.5V/CH /8  
;----------------------------------------------------------------------
L384:        
        bud       L408
        sti       r0,*+fp(5)
        ldiu      95,r0
        sti       r0,*+fp(7)
;*      Branch Occurs to L408 
	.line	186
;----------------------------------------------------------------------
; 784 | default : break;                                                       
;----------------------------------------------------------------------
L386:        
	.line	181
        ldi       *+fp(10),r0
        beqd      L378
	nop
	nop
        ldieq     3,r0
;*      Branch Occurs to L378 
        cmpi      1,r0
        beqd      L380
	nop
	nop
        ldieq     2,r0
;*      Branch Occurs to L380 
        cmpi      2,r0
        beqd      L382
	nop
	nop
        ldieq     1,r0
;*      Branch Occurs to L382 
        cmpi      3,r0
        beqd      L384
	nop
	nop
        ldieq     0,r0
;*      Branch Occurs to L384 
        bud       L410
        ldiu      *+fp(5),r0
        push      r0
        ldiu      *+fp(3),r0
;*      Branch Occurs to L410 
	.line	188
;----------------------------------------------------------------------
; 786 | break;                                                                 
; 787 | case 0xC :                                                             
; 788 | switch(i) {                                                            
;----------------------------------------------------------------------
L392:        
	.line	191
;----------------------------------------------------------------------
; 789 | case 0 : comp = 0x52; break;            // 20eV/CH X1                  
;----------------------------------------------------------------------
        bud       L409
        ldiu      82,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L393:        
	.line	192
;----------------------------------------------------------------------
; 790 | case 1 : comp = 0x4b; break;            // 10eV/CH /2                  
;----------------------------------------------------------------------
        bud       L409
        ldiu      75,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L394:        
	.line	193
;----------------------------------------------------------------------
; 791 | case 2 : comp = 0x44; break;            //  5eV/CH /4                  
;----------------------------------------------------------------------
        bud       L409
        ldiu      68,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L395:        
	.line	194
;----------------------------------------------------------------------
; 792 | case 3 : comp = 0x3d; break;            // 2.5V/CH /8                  
;----------------------------------------------------------------------
        bud       L409
        ldiu      61,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
L396:        
	.line	195
;----------------------------------------------------------------------
; 793 | case 4 : comp = 0x36; break;                                           
;----------------------------------------------------------------------
        bud       L409
        ldiu      54,r0
        sti       r0,*+fp(7)
        ldiu      *+fp(5),r0
;*      Branch Occurs to L409 
	.line	196
;----------------------------------------------------------------------
; 794 | default : break;                                                       
;----------------------------------------------------------------------
L398:        
	.line	190
        ldi       *+fp(10),r0
        beq       L392
;*      Branch Occurs to L392 
        cmpi      1,r0
        beq       L393
;*      Branch Occurs to L393 
        cmpi      2,r0
        beq       L394
;*      Branch Occurs to L394 
        cmpi      3,r0
        beq       L395
;*      Branch Occurs to L395 
        cmpi      4,r0
        beq       L396
;*      Branch Occurs to L396 
        bud       L410
        ldiu      *+fp(5),r0
        push      r0
        ldiu      *+fp(3),r0
;*      Branch Occurs to L410 
	.line	198
;----------------------------------------------------------------------
; 796 | break;                                                                 
;----------------------------------------------------------------------
	.line	89
L405:        
        cmpi      12,ir0
L406:        
        bhid      L293
	nop
	nop
        ldils     @CL9,ar0
;*      Branch Occurs to L293 
        ldiu      *+ar0(ir0),r0
        bu        r0

	.sect	".text"
SW2:	.word	L293	; 0
	.word	L293	; 1
	.word	L293	; 2
	.word	L293	; 3
	.word	L306	; 4
	.word	L317	; 5
	.word	L332	; 6
	.word	L345	; 7
	.word	L345	; 8
	.word	L358	; 9
	.word	L371	; 10
	.word	L386	; 11
	.word	L398	; 12
	.sect	".text"
;*      Branch Occurs to r0 
L408:        
	.line	201
;----------------------------------------------------------------------
; 799 | WriteEDS( addr, data );                         // Load The appropriate
;     |  DAC with the Approprite Data                                          
;----------------------------------------------------------------------
        ldiu      *+fp(5),r0
L409:        
        push      r0
        ldiu      *+fp(3),r0
L410:        
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	202
;----------------------------------------------------------------------
; 800 | rdata = ReadBITADC();                           // Read the ADC        
;----------------------------------------------------------------------
        call      _ReadBITADC
                                        ; Call Occurs
        sti       r0,*+fp(6)
	.line	203
;----------------------------------------------------------------------
; 801 | rdata = ReadBITADC();                           // Read the ADC        
;----------------------------------------------------------------------
        call      _ReadBITADC
                                        ; Call Occurs
        sti       r0,*+fp(6)
	.line	204
;----------------------------------------------------------------------
; 802 | rdata = ReadBITADC();                           // Read the ADC        
;----------------------------------------------------------------------
        call      _ReadBITADC
                                        ; Call Occurs
        sti       r0,*+fp(6)
	.line	205
;----------------------------------------------------------------------
; 803 | cmp_data = rdata >> 8;                          // Keep only the 4 uppe
;     | r bits ( Just Range checking since Serial DAC )                        
; 804 | //              if( cmp_data != comp ){                                
;----------------------------------------------------------------------
        lsh       -8,r0
        sti       r0,*+fp(8)
	.line	207
;----------------------------------------------------------------------
; 805 | if( cmp_data > comp ){                                                 
;----------------------------------------------------------------------
        cmpi      *+fp(7),r0
        blsd      L413
	nop
	nop
        ldils     *+fp(8),r0
;*      Branch Occurs to L413 
	.line	208
;----------------------------------------------------------------------
; 806 | rdata = cmp_data - comp;                                               
; 807 | } else {                                                               
;----------------------------------------------------------------------
        bud       L414
        ldiu      *+fp(7),r0
        subri     *+fp(8),r0            ; Unsigned
        sti       r0,*+fp(6)
;*      Branch Occurs to L414 
	.line	210
;----------------------------------------------------------------------
; 808 | rdata = comp - cmp_data;                                               
;----------------------------------------------------------------------
L413:        
        subri     *+fp(7),r0            ; Unsigned
        sti       r0,*+fp(6)
L414:        
	.line	213
;----------------------------------------------------------------------
; 811 | if( rdata > 1 ){                                                       
;----------------------------------------------------------------------
        ldiu      *+fp(6),r0
        cmpi      1,r0
        blsd      L416
	nop
	nop
        ldils     1,r0
;*      Branch Occurs to L416 
	.line	214
;----------------------------------------------------------------------
; 812 | error |= ( 1 << i );                                                   
; 814 | }
;     |          // for i...                                                   
;----------------------------------------------------------------------
        ldiu      1,r0
        ash       *+fp(10),r0
        or        *+fp(1),r0
        sti       r0,*+fp(1)
	.line	80
        ldiu      1,r0
L416:        
        addi      *+fp(10),r0
        sti       r0,*+fp(10)
        cmpi      *+fp(9),r0
        bltd      L280
	nop
	nop
        ldige     258,r0
;*      Branch Occurs to L280 
	.line	218
;----------------------------------------------------------------------
; 816 | data = ReadEDS( iADR_CW );                                             
;----------------------------------------------------------------------
L418:        
        push      r0
        call      _ReadEDS
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(5)
	.line	219
;----------------------------------------------------------------------
; 817 | data &= ~( 3 << 11 );                                   // Clear PreAmp
;     |  Source Bits                                                           
;----------------------------------------------------------------------
        andn      6144,r0
        sti       r0,*+fp(5)
	.line	220
;----------------------------------------------------------------------
; 818 | data &= ~( 1 << 13 );                                   // Set Input Ga
;     | in to 1X                                                               
;----------------------------------------------------------------------
        andn      8192,r0
        sti       r0,*+fp(5)
	.line	221
;----------------------------------------------------------------------
; 819 | WriteEDS( iADR_CW, data );                                             
;----------------------------------------------------------------------
        ldiu      258,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	223
;----------------------------------------------------------------------
; 821 | return( error );                                                       
;----------------------------------------------------------------------
        ldiu      *+fp(1),r0
L419:        
	.line	224
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      12,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	822,000000000h,10


	.sect	 ".text"

	.global	_BIT_SCA_GEN
	.sym	_BIT_SCA_GEN,_BIT_SCA_GEN,32,2,0
	.func	826
;******************************************************************************
;* FUNCTION NAME: _BIT_SCA_GEN                                                *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,fp,sp                                         *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 1 Auto + 0 SOE = 3 words          *
;******************************************************************************
_BIT_SCA_GEN:
	.sym	_data,1,13,1,32
	.line	1
;----------------------------------------------------------------------
; 826 | void BIT_SCA_GEN( void )                                               
; 828 | USHORT data;                                                           
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      1,sp
	.line	4
;----------------------------------------------------------------------
; 829 | data = ReadDPRAM( dADR_SCA_GEN );                                      
;----------------------------------------------------------------------
        ldiu      33,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        sti       r0,*+fp(1)
	.line	5
;----------------------------------------------------------------------
; 830 | WriteEDS( iADR_SCA, data );                                            
;----------------------------------------------------------------------
        ldiu      290,r1
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        subi      2,sp
	.line	6
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      3,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	831,000000000h,1



	.sect	".cinit"
	.field  	1,32
	.field  	_count$1+0,32
	.field  	0,32		; _count$1 @ 0

	.sect	".text"
	.sect	 ".text"

	.global	_BIT_DAC_GEN
	.sym	_BIT_DAC_GEN,_BIT_DAC_GEN,32,2,0
	.func	835
;******************************************************************************
;* FUNCTION NAME: _BIT_DAC_GEN                                                *
;*                                                                            *
;*   Architecture       : TMS320C32                                           *
;*   Calling Convention : Stack Parameter Convention                          *
;*   Function Uses Regs : r0,r1,fp,sp,st                                      *
;*   Regs Saved         :                                                     *
;*   Stack Frame        : Full (w/ debug)                                     *
;*   Total Frame Size   : 2 Call + 0 Parm + 1 Auto + 0 SOE = 3 words          *
;******************************************************************************
_BIT_DAC_GEN:
	.bss	_count$1,1
	.sym	_count,_count$1,13,3,32
	.sym	_target,1,13,1,32
	.line	1
;----------------------------------------------------------------------
; 835 | void BIT_DAC_GEN( void )                                               
; 837 | USHORT target;                                                         
; 838 | static USHORT count = 0;                                               
;----------------------------------------------------------------------
        push      fp
        ldiu      sp,fp
        addi      1,sp
	.line	5
;----------------------------------------------------------------------
; 839 | target = ReadDPRAM( dADR_DAC_GEN );                                    
; 841 | switch( target ){                                                      
;----------------------------------------------------------------------
        ldiu      34,r0
        push      r0
        call      _ReadDPRAM
                                        ; Call Occurs
        subi      1,sp
        bud       L434
        sti       r0,*+fp(1)
        nop
        cmpi      1,r0
;*      Branch Occurs to L434 
	.line	8
;----------------------------------------------------------------------
; 842 | case 1 : WriteEDS( iADR_DISC,  count ); break;                         
;----------------------------------------------------------------------
L426:        
        push      r1
        push      r0
        call      _WriteEDS
                                        ; Call Occurs
        bud       L439
        subi      2,sp
        ldiu      1,r0
        addi      @_count$1+0,r0        ; Unsigned
;*      Branch Occurs to L439 
	.line	9
;----------------------------------------------------------------------
; 843 | case 2 : WriteEDS( iADR_EVCH1, count ); break;                         
;----------------------------------------------------------------------
L428:        
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        bud       L439
        subi      2,sp
        ldiu      1,r0
        addi      @_count$1+0,r0        ; Unsigned
;*      Branch Occurs to L439 
	.line	10
;----------------------------------------------------------------------
; 844 | case 3 : WriteEDS( iADR_BIT,   count ); break;                         
;----------------------------------------------------------------------
L430:        
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        bud       L439
        subi      2,sp
        ldiu      1,r0
        addi      @_count$1+0,r0        ; Unsigned
;*      Branch Occurs to L439 
	.line	11
;----------------------------------------------------------------------
; 845 | case 4 : WriteEDS( iADR_EVCH0, count ); break;                         
;----------------------------------------------------------------------
L432:        
        push      r0
        push      r1
        call      _WriteEDS
                                        ; Call Occurs
        bud       L439
        subi      2,sp
        ldiu      1,r0
        addi      @_count$1+0,r0        ; Unsigned
;*      Branch Occurs to L439 
	.line	12
;----------------------------------------------------------------------
; 846 | default : break;                                                       
;----------------------------------------------------------------------
	.line	7
L434:        
        beqd      L426
	nop
        ldieq     @_count$1+0,r1
        ldieq     271,r0
;*      Branch Occurs to L426 
        cmpi      2,r0
        beqd      L428
	nop
        ldieq     @_count$1+0,r0
        ldieq     272,r1
;*      Branch Occurs to L428 
        cmpi      3,r0
        beqd      L430
	nop
        ldieq     @_count$1+0,r0
        ldieq     274,r1
;*      Branch Occurs to L430 
        cmpi      4,r0
        beqd      L432
	nop
        ldieq     @_count$1+0,r0
        ldieq     273,r1
;*      Branch Occurs to L432 
	.line	14
;----------------------------------------------------------------------
; 848 | count ++;                                                              
;----------------------------------------------------------------------
        ldiu      1,r0
        addi      @_count$1+0,r0        ; Unsigned
L439:        
        sti       r0,@_count$1+0
	.line	15
;----------------------------------------------------------------------
; 849 | count &= 0xFFF;                                                        
;----------------------------------------------------------------------
        ldiu      4095,r0
        and       @_count$1+0,r0
        sti       r0,@_count$1+0
	.line	16
        ldiu      *-fp(1),bk
        ldiu      *fp,fp
        subi      3,sp
        bu        bk
;*      Branch Occurs to bk 
	.endfunc	850,000000000h,1


;******************************************************************************
;* CONSTANT TABLE                                                             *
;******************************************************************************
	.sect	".const"
	.bss	CL1,1
	.bss	CL2,1
	.bss	CL3,1
	.bss	CL4,1
	.bss	CL5,1
	.bss	CL6,1
	.bss	CL7,1
	.bss	CL8,1
	.bss	CL9,1

	.sect	".cinit"
	.field  	9,32
	.field  	CL1+0,32
	.field  	1000000,32
	.field  	SW0,32
	.field  	65535,32
	.field  	268435456,32
	.field  	65535,32
	.field  	SW1,32
	.field  	49151,32
	.field  	65535,32
	.field  	SW2,32

	.sect	".text"
;******************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                              *
;******************************************************************************

	.global	_WriteDPRAM

	.global	_ReadDPRAM

	.global	_WriteGlue

	.global	_ReadGlue

	.global	_WritePCIFifo

	.global	_WriteEDS

	.global	_ReadEDS

	.global	_ReadBITADC

	.global	_WriteEDSFIFO

	.global	_ReadEDSFIFO
